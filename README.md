This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# Development

## Getting Started

### Dockerizer local deploy

After do a normal Dockerizer installation, with folder `/web` to host this app, copy the files in `/web/Dockerizer` to
the Dockerizer folder.

After that, execute `yarn install` to get project dependencies.

Execute the app in dev mode with `yarn dev` and run `launch` in console to open the app in your browser.

### [Deprecated] Deploy alongside with all the Siasar3 suite using Dockerizer
NOT RECOMMENDED, because don't respect the .docker.env file.

install Dockerizer in your workstation and run ([Dockerizer Documentation](https://frontid.github.io/dockerizer/))

```Bash
dk new siasar
cd siasar
git clone git@gitlab.com:siasar3/visit-api.git web
git clone git@gitlab.com:siasar3/nextjs-client.git frontend
```

once you have both repositories cloned you can set the node instance to run the nextjs application

open the `docker-compose.override.yml` and change the node service config for this:

```yaml

  node:
    container_name: ${PROJECT_NAME}_node
    network_mode: traefik_default
    user: "node"
    image: wodby/node:$NODE_TAG
    working_dir: /var/www/html/frontend
    volumes:
      - ./frontend:/var/www/html/frontend
    ports:
      - 3000:3000
    expose:
      - 3000
    command: "npm run dev"
    labels:
      - 'traefik.frontend.rule=Host:${PROJECT_NAME}.front.localhost'

```
make sure to include the expose and the traefik settings. (this will expose http(s)://siasar3.front.localhost, but you can change it for whatever you want)

#### CORS

In order to allow connections from other sites we need to make sure to enable CORS in the Visit Api application. For that, you will need to change the `.env.local` configuration for something like this:

```Bash
CORS_ALLOW_ORIGIN='^https?://(siasar3.localhost|siasar3.front.localhost)?$'
```

#### Run the applications

```Bash
dk start traefik && dk start
```

once it finished deploying all containers you can open http://siasar3.front.localhost

#### NOTE:

you may need to run `npm i` and re run `npm run dev` the first time but once it has all the dependencies installed the nextjs application will run automatically after that.

## Working with app events

### Event types

All event-related structures can be found in the file `/src/utils/eventManager.ts`.

The different types of events are declared in enum `Events`:

```JS
export enum Events {
  // This app enters in maintenance mode. Emitted in api.ts.
  APP_MAINTENANCE = 'app.maintenance',
  // Session Login start.
  LOGIN_START = 'login.start',
  // Session Logout complete.
  LOGIN_COMPLETE = 'login.complete',
  // Session Logout start.
  LOGOUT_START = 'logout.start',
  // Session Logout complete.
  LOGOUT_COMPLETE = 'logout.complete',
  // Add a new i18n literal to remote API.
  I18N_ADD_LITERAL = 'i18n.add.literal',
  // Display status console.
  STATUS_SHOW = 'status.show',
  // Add a new status event.
  STATUS_ADD = 'status.add',
  // A component was mounted.
  COMP_MOUNT = 'comp.mount',
  // a new household survey was succesfully created
  HOUSEHOLD_SURVEY_ADDED = 'household.survey.added',
}
```

Events can send additional information if required, for which additional structures can be created, such as the following:

```JS
export enum StatusEventLevel {
  ERROR = 'error',
  WARNING = 'warning',
  SUCCESS = 'success',
  INFO = 'info',
  DEBUG = 'debug',
}

export interface StatusEvent {
  time?: string;
  level: StatusEventLevel;
  title?: string,
  message: string;
  isPublic: boolean;
}
```

### Send and receive events

To send and receive events, a single object is used in the application, the `eventManager`.

First, those interested in receiving an event must register in the `eventManager` using `on` method, indicating the type of event and a `callback` function that will be invoked when the event occurs.  This callback function can optionally receive the `data` that was passed when sending the event if any.

Then, those who wish to send an event must do so to the `eventManager` using `dispatch` method, indicating the type of event and optionally the data.

```JS
export const eventManager = {
  on(event: Events, callback: (data: any) => any)
  dispatch(event: Events, data: any)
}
```

### Sending status events

The application has 2 components with the purpose of knowing its status, the `StatusConsole` that shows the internal status more suitable for developers, and the `StatusNotifications` that shows messages to the end user. Both display what is known as `StatusEvent`.

To send status events to be seen in the status console and as notifications to the users, create a `StatusEvent` be used as data, and send a `STATUS_ADD` event type by calling the `dispatch` method of the `eventManager`:

- The enum `StatusEventLevel` indicates the severity level of the event available.
- The `title` is optional, while the `message` is mandatory.
- All status events are displayed in the `StatusConsole`, while only those with `isPublic` set to true will be displayed by the `StatusNotifications`.

```JS
import {eventManager, Events, StatusEventLevel, StatusEvent} from "@/utils/eventManager";

// Dispatch status event.
eventManager.dispatch(
    Events.STATUS_ADD,
    {
        level: StatusEventLevel.INFO,
        title: t('Title here'),
        message: t('Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),
        isPublic: true,
    }
);
```

### Sending component mount events


To send a event to notify that a component was mounted (maybe to another components), send a `COMP_MOUNT` event type with `this` as data by calling the `dispatch` method of the `eventManager`:

```JS
class AppTopBar {
  componentDidMount() {
    eventManager.dispatch(Events.COMP_MOUNT, this);
  }
}
```

Interested parties should register their interest in the event, and can do something like the following in the callback to react to the specific component they are interested in:

```JS
class StatusConsole {
  constructor(props: IStatusConsoleProps) { 
    eventManager.on(Events.COMP_MOUNT, (component: Component) => {
      // Check if component is AppTopBar.
      if (component.constructor.name === 'AppTopBar') {
        // React to...
      }
    });
  }
}
```

## Offline zip generated map

- `/` File root
  - `/index.json` Change index
  - `/images/` Image folder
  - `/images/*` Images with name ULID_{old filename}.{extension}
  - `/records/` Records folder.
  - `/records/*.json` Records data with name {ULID}.json

# Git repository

## How to work
We use branches to develop or fix functionalities. These branches are created from issues.

When we have an issue assigned, we must create a merge request with a new branch from the issue page.
While working on issues we must do commits, a minimum of one by day if we are coding.

With each commit, git will run some validations to reduce code mistakes, after that we can do a push to the repository.

When we end the issue resolution we can mark the merge request how Ready and wait, please, You don't must execute the merge.

# Testing
Note that to run tests in local, you must start API server and this app, configured to use the local API.
Launch app with: `yarn build && yarn start`

The app must run on `http://siasar3-client.localhost` to run tests. And before the first Cypress execution we need execute:
`$ echo "{}" > cypress.env.json`

## PWA compatibility
Tested with:
- Android 12L Google API 32 | x86_64
- Android 13.0 Google API 33 | x86_64

## Validate code style
Execute `yarn lint --quiet` to get only errors.

## Test environments
This project uses Jest, to unit tests, and Cypress, to functional tests.

We have three test modes:
- Jest continuous: `yarn testing` that execute tests while we write code.
- Jest: `yarn test` that execute tests one time.
- Cypress: `./cypress.sh` that execute tests one time using Chrome browser. This requires an running API service and the 
app running in production mode. (`yarn build && yarn start`) 

You can execute Cypress manually with other browsers:
- Electron: `docker-compose --env-file .docker.env run e2e-electron`
- Chrome: `docker-compose --env-file .docker.env run e2e-chrome`
- Firefox: `docker-compose --env-file .docker.env run e2e-firefox` , firefox don't save videos. 

You can execute Cypress with only one spec file using `--spec <path-to-spec-file>`, or with several spec files using glob patterns (note that you need to wrap your glob patterns in single quotes to avoid shell expansion):
- with one spec file: `./cypress.sh --spec cypress/integration/dynform/fields/boolean.field.spec.js` 
- with several spec files: `./cypress.sh --spec 'cypress/integration/dynform/fields/*'` 

Also, when you are writing the tests, you may want to run only some of them. To do this, use `it.only()` on the tests you want to run, and  `it.skip()` on the ones you want to ignore:
- execute this test: `it.only('DynForm Field - simple boolean without data', ...`  
- ignore this test: `it.skip('DynForm Field - simple boolean with data', ...`

Note that current tests have a limited coberture.

Test paths:
- `src/__tests__`: Jest tests.
- `cypress/integration`: Cypress tests.
- `cypress/screenshots`: Cypress results screenshots.
- `cypress/videos`: Cypress results videos.

We can create an `cypress.env.json` file to overwrite Cypress environment settings, values in the cypress.json `env` object.

For example:
`# /cypress.env.json`
```json
{
  "test_login_user": "AdminLocalHn",
  "test_login_pass": "123456"
}
```

## Run Cypress in host (local)

Ubuntu global Cypress install, allowing to execute tests manually in the host machine (outside docker):
- sudo apt install npm
- sudo npm install -g  --unsafe-perm=true --allow-root cypress

Installed into: `/usr/local/lib/node_modules/cypress` .
The installation folder is show after install Cypress, you must use the right for your system.

Open Cypress in Global mode: `/usr/local/lib/node_modules/cypress/bin/cypress open  --global`

Then select the project root path inside the Cypress tool.

This project contains a script to launch Cypress tool: `scripts/cypress_open.sh` . We can use it calling outside the Dockerizer folder.

## [Bad practice] DynForm test

Test page: /dynform_test/

To add a field type to the test page add it to `src/dynforms/TestFormSchema.ts`, this file contains the form definition.
You can initialize the form with data updating this line, data is in `dfTestForm.ALL_FIELDS_DATA`:
```
const [formState, setFormState] = useState(getTestFormData(dfTestForm.ALL_FIELDS_DATA));
```
To add test values to forms you can update `src/dynforms/testsForms.ts`.

# Development and Staging environments

https://app-dev.siasar.org

https://app-stg.siasar.org

# Deploy

## Server settings in Debian

1) Create file `/etc/systemd/system/siasar-front.service` with:
```
[Unit]
Description=Siasar Front service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
ExecStart=<app folder>/scripts/start.server.sh

[Install]
WantedBy=multi-user.target
```

2) Reload daemon, enable it and start it:

`systemctl daemon-reload`
`systemctl enable siasar-front.service`
`systemctl start siasar-front.service`

3) Give sudo permissions to the user (if it doesn't have it):

- `sudo visudo`
- add next line:
`<username> ALL=(ALL) NOPASSWD: /usr/sbin/service siasar-front start,/usr/sbin/service siasar-front stop, /usr/sbin/service siasar-front status`

Now you can do `start`, `status` and `stop` the service with the indicated user, for example: `sudo service siasar-front start`
