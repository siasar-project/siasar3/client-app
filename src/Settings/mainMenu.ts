/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {MenuOptionInterface} from "@/objects/MenuOptionInterface";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

/**
 * This file contains the main menu definition.
 *
 * While <code>smIsLoading()</code> is TRUE the menus must be disabled.
 * If a menu option have no children visible, must be hidden.
 */

const menu:MenuOptionInterface = {
    label: 'home',
    url: '/',
    needSession: false,
    permissions: [],
    children: [
        {
            label: 'Home',
            url: '/',
            needSession: false,
        },
        {
            label: 'Dashboard',
            url: '/dashboard',
            needSession: true,
            // hidden: dmIsOfflineMode(),
        },
        {
            label: 'SIASAR Point',
            url: '/point/list',
            permissions: [
                'read public configuration',
            ],
            // hidden: dmIsOfflineMode(),
        },
        {
            label: 'Surveys',
            url: '/surveys',
            permissions: [
                'update inquiry record',
            ],
            hidden: dmIsOfflineMode(),
        },
        {
            label: 'Households',
            url: '/households',
            permissions: [
                'read doctrine householdprocess',
            ],
            // hidden: dmIsOfflineMode(),
        },
        {
            label: 'Validation',
            url: '/validation',
            permissions: [
                'can do validate transition in workflow inquiring',
                'can do calculate transition in workflow pointing',
            ],
            hidden: dmIsOfflineMode(),
        },
        {
            label: 'Messages',
            url: '/mail/list',
            permissions: [
                'can use mailing',
            ],
            hidden: dmIsOfflineMode(),
        },
        {
            label: 'Administration',
            url: '',
            hidden: dmIsOfflineMode(),
            children: [
                {
                    label: 'Users',
                    url: '/admin/users',
                    permissions: [
                        'create doctrine user',
                    ],
                },
                {
                    label: 'Translate',
                    url: '/admin/translate',
                    permissions: [
                        'create doctrine localetarget',
                    ],
                },
                {
                    label: 'Country',
                    url: '/admin/country',
                    permissions: [
                        'update doctrine country',
                    ],
                },
                {
                    label: 'Forms test',
                    url: '/dynform_test',
                    hidden: process.env.NEXT_PUBLIC_ENV !== "development",
                },
                {
                    label: 'Form fields test',
                    url: '/dynform_field_test',
                    hidden: process.env.NEXT_PUBLIC_ENV !== "development",
                },
            ],
        },
    ],
};

/**
 * Get main menu options tree.
 *
 * @returns {MenuOptionInterface}
 */
export default function getMainMenuOptions() {
    return menu;
}
