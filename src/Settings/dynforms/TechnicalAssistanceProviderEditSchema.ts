/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get funderInterventions filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getTechnicalAssistanceProviderEditSchema() {
  let TechnicalAssistanceProviderEditSchema:DynFormStructureInterface = {
    "id": "form.technicalAssistanceProvider.filter",
    "type": "",
    "requires": [],
    "fields": {
      "field_name": {
        "id": "field_name",
        "type": "short_text",
        "label": t("Name"),
        "description": "",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": false,
          "multivalued": false,
          "weight": 0,
          "meta": {  },
          "sort": false,
          "filter": false
        }
      },
      "field_description": {
        "id": "field_description",
        "type": "short_text",
        "label": t("Description"),
        "description": "",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": false,
          "multivalued": false,
          "weight": 0,
          "meta": {  },
          "sort": false,
          "filter": false
        }
      },
      "field_nationalId": {
        "id": "field_nationalId",
        "type": "short_text",
        "label": t("National ID"),
        "description": "",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": false,
          "multivalued": false,
          "weight": 0,
          "meta": {  },
          "sort": false,
          "filter": false
        }
      }
    },
    "title": '2: '+t("Technical assistance provider management"),
    "description": "",
    "endpoint": {
      "collection": {
        "get": true,
        "post": true
      },
      "item": {
        "get": true,
        "put": true,
        "delete": true,
        "patch": true
      }
    },
    "meta": {
      "title": t("Technical assistance provider"),
      "version": "",
      "allow_sdg": false,
      "field_groups": [
        {
          id: '0',
          "title": t("Filters"),
          "children": {
            "0.1": {
              "id": "0.1",
              "title": t("Name"),
              "field_id": "field_name",
              "weight": 0
            },
            "0.2": {
              "id": "0.2",
              "title": t("Description"),
              "field_id": "field_description",
              "weight": 0
            },
            "0.3": {
              "id": "0.3",
              "title": t("National ID"),
              "field_id": "field_nationalId",
              "weight": 0
            }
          },
        },
      ]
    }
  };
  
  return TechnicalAssistanceProviderEditSchema;
}
