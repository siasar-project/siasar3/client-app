/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get surveys filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getOwnSurveysFilterSchema() {
    let OwnSurveysFilterSchema:DynFormStructureInterface = {
        "id": "form.surveys.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_type": {
                "id": "field_type",
                "type": "radio_select",
                "label": t("Survey type"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "form.school": t("Schools"),
                        "form.health.care": t("Health Centre"),
                        "form.tap": t("Technical Assistance Provider (TAP)"),
                        "form.community": t("Community"),
                        "form.wsprovider": t("Water service providers"),
                        "form.wssystem": t("Water supply systems")
                    }
                }
            },
        },
        "title": t("Search own surveys"),
        "description": "",
        "meta": {
            "title": t("Search own surveys"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.0": {
                            "id": "0.0",
                            "title": t("Type"),
                            "field_id": "field_type",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return OwnSurveysFilterSchema;
}
