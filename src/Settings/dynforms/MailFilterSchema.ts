/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get mails filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getMailsFilterSchema() {
    let MailsFilterSchema:DynFormStructureInterface = {
        "id": "form.mails.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_type": {
                "id": "field_type",
                "type": "select",
                "label": t("Messages type"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "all": t("All"),
                        "me": t("Received"),
                        "others": t("Sended")
                    }
                }
            },
            "field_changed": {
                "id": "field_changed",
                "type": "date",
                "label": t("Changed from"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {},
                    "sort": false,
                    "filter": false
                }
            },
            "field_order_changed": {
                id: "field_order_changed",
                type: "select",
                label: t('Order by date'),
                description: '',
                indexable: false,
                internal: false,
                deprecated: false,
                settings: {
                    required: true,
                    multivalued: false,
                    weight: 0,
                    meta: {},
                    sort: false,
                    filter: false,
                    options: {
                        desc: t('Newer first'),
                        asc: t('Older first'),
                    }
                }
            }
        },
        "title": t("Search messages"),
        "description": "",
        "meta": {
            "title": t("Messages"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Type"),
                            "field_id": "field_type",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Changed from"),
                            "field_id": "field_changed",
                            "weight": 0
                        },
                        "0.3": {
                            "id": "0.3",
                            "title": t("Order by date"),
                            "field_id": "field_order_changed",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return MailsFilterSchema;
}
