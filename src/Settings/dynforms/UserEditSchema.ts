/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get user filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getUserEditSchema() {
    let UserEditSchema:DynFormStructureInterface = {
        "id": "form.user.edit",
        "type": "",
        "requires": [],
        "forceIsRequired": true,
        "fields": {
            "field_id": {
                "id": "field_id",
                "type": "ulid",
                "label": t("ID"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                       "disabled": true
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_username": {
                "id": "field_username",
                "type": "short_text",
                "label": t("Username"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                       "autocomplete": false, "focus": true,
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_email": {
                "id": "field_email",
                "type": "mail",
                "label": t("E-mail"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_password": {
               "id": "field_password",
               "type": "new_password",
               "label": t("Password"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": true,
                   "multivalued": false,
                   "weight": 0,
                   "meta": {
                       "autocomplete": false
                   },
                   "sort": false,
                   "filter": false
               }
           },
           "field_roles": {
               "id": "field_roles",
               "type": "radio_user_role",
               "label": t("User roles"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": true,
                   "multivalued": true,
                   "weight": 0,
                   "meta": {  },
                   "sort": false,
                   "filter": false,
                   "options": []
               }
           },
           "field_country": {
               "id": "field_country",
               "type": "country_reference",
               "label": t("User country"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": true,
                   "multivalued": false,
                   "weight": 0,
                   "meta": {
                       "disabled": true
                   },
                   "sort": false,
                   "filter": false
               }
           },
           "field_timezone": {
               "id": "field_timezone",
               "type": "timezone_reference",
               "label": t("User timezone"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": true,
                   "multivalued": false,
                   "weight": 0,
                   "meta": { },
                   "sort": false,
                   "filter": false
               }
           },
           "field_administrative_division": {
               "id": "field_administrative_division",
               "type": "administrative_division_parent_reference",
               "label": t("Administrative Division"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": false,
                   "multivalued": true,
                   "weight": 0,
                   "meta": {  },
                   "sort": false,
                   "filter": false
               }
           },
           "field_language": {
               "id": "field_language",
               "type": "user_language_reference",
               "label": t("User preferred language"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": true,
                   "multivalued": false,
                   "weight": 0,
                   "meta": {  },
                   "sort": false,
                   "filter": false
               }
           },
           "field_active": {
               "id": "field_active",
               "type": "boolean",
               "label": t("Active"),
               "description": "",
               "indexable": false,
               "internal": false,
               "deprecated": false,
               "settings": {
                   "required": true,
                   "multivalued": false,
                   "weight": 0,
                   "meta": {  },
                   "sort": false,
                   "filter": false
               }
           },
       },
       "title": t("Edit User"),
       "description": "",
       "meta": {
           "title": t("Edit User"),
           "version": "",
           "allow_sdg": false,
           "field_groups": [
               {
                   id: '0',
                   "title": t("User"),
                   "children": {
                       "0.1": {
                           "id": "0.1",
                           "title": t("ID"),
                           "field_id": "field_id",
                           "weight": 0
                       },
                       "0.2": {
                           "id": "0.2",
                           "title": t("Username"),
                           "field_id": "field_username",
                           "weight": 0
                       },
                       "0.3": {
                           "id": "0.3",
                           "title": t("E-mail"),
                           "field_id": "field_email",
                           "weight": 0
                       },
                       "0.4": {
                           "id": "0.4",
                           "title": t("Password"),
                           "field_id": "field_password",
                           "weight": 0
                       },
                       "0.5": {
                           "id": "0.5",
                           "title": t("Roles"),
                           "field_id": "field_roles",
                           "weight": 0
                       },
                       "0.6": {
                           "id": "0.6",
                           "title": t("Country"),
                           "field_id": "field_country",
                           "weight": 0
                       },
                       "0.7": {
                           "id": "0.7",
                           "title": t("Timezone"),
                           "field_id": "field_timezone",
                           "weight": 0
                       },
                       "0.8": {
                           "id": "0.8",
                           "title": t("Language"),
                           "field_id": "field_language",
                           "weight": 0
                       },
                       "0.9": {
                           "id": "0.9",
                           "title": t("Administrative Division"),
                           "field_id": "field_administrative_division",
                           "weight": 0
                       },
                       "0.10": {
                           "id": "0.10",
                           "title": t("Active"),
                           "field_id": "field_active",
                           "weight": 0
                       },
                    },
                },
            ]
        }
    };
    
    return UserEditSchema;
}
