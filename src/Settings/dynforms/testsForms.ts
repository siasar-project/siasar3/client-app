/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

const dfEmpty = {};

export enum dfTestForm {
    EMPTY_DATA = 0,
    ALL_FIELDS_DATA = 1,
}

/**
 * Get surveys filter form schema.
 *
 * @param schema {dfTestForm}
 * @returns {DynFormStructureInterface}
 */
export default function getTestFormData(schema: dfTestForm) {
    switch (schema) {
        case dfTestForm.EMPTY_DATA:
            return dfEmpty;
        case dfTestForm.ALL_FIELDS_DATA:
            const dfAllFields = {
                "field_select": { value: "2" },
                "field_select_m": [
                    { value: "2" },
                    { value: "3" },
                ],
                "field_radio": { value: "2" },
                "field_radio_m": [
                    { value: "1" },
                    { value: "2" },
                    { value: "3" },
                    { value: "4" },
                ],
                "field_shorttext": { value: "aaaa" },
                "field_shorttext_m": [
                    { value: "bbbb" },
                    { value: "ccccc" },
                ],
                "field_integer": { value: "6" },
                "field_integer_m": [
                    { value: "7" },
                    { value: "8" },
                ],
                "field_longtext": { value: "0000" },
                "field_longtext_m": [
                    { value: "1111\n111\n1111" },
                    { value: "2222\n222\n2222" },
                    { value: "3333\n333\n3333" },
                ],
                "field_mail": { value: "aaaaa976@gmail.com" },
                "field_mail_m": [
                    { value: "mail1@ejemplo.es" },
                    { value: "mail2@ejemplo.es" },
                    { value: "mail3@ejemplo.es" },
                ],
                "field_decimal": { value: "10" },
                "field_decimal_m": [
                    { value: "10.5" },
                    { value: "25,25" },
                    { value: "800.99" },
                ],
                "field_boolean": { value: "false" },
                "field_boolean1": { value: "true" },
                "field_boolean_m": [
                    { value: "false" },
                    { value: "true" },
                    { value: "false" },
                ],
                "field_date": { value: "2022-04-01", timezone: "UTC" },
                "field_date_m": [
                    { value: "2022-04-02", timezone: "UTC" },
                    { value: "2022-04-03", timezone: "UTC" },
                    { value: "2022-04-04", timezone: "UTC" },
                ],
                "field_phone": { value: "1111111" },
                "field_phone_m": [
                    { value: "2222222" },
                    { value: "3333333" },
                    { value: "4444444" },
                ],
                "field_month_year": { month: "1", year: "2020" },
                "field_month_year_m": [
                    { month: "2", year: "2021" },
                    { month: "3", year: "2022" },
                    { month: "4", year: "2023" },
                ],
            };
            return dfAllFields;
    }
}
