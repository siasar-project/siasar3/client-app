/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get default diameters filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getDefaultDiametersFilterSchema() {
    let DefaultDiametersFilterSchema:DynFormStructureInterface = {
        "id": "form.defaultdiameter.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_value": {
                "id": "field_value",
                "type": "decimal",
                "label": t("Length"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_unit": {
                "id": "field_unit",
                "type": "short_text",
                "label": t("Unit"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "title": '15: '+t("Default diameter management"),
        "description": "",
        "endpoint": {
            "collection": {
                "get": true,
                "post": true
            },
            "item": {
                "get": true,
                "put": true,
                "delete": true,
                "patch": true
            }
        },
        "meta": {
            "title": t("Default diameters"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Value"),
                            "field_id": "field_value",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Unit"),
                            "field_id": "field_unit",
                            "weight": 0
                        }
                    },
                },
            ]
        }
    };

    return DefaultDiametersFilterSchema;
}
