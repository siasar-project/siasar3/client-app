/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get pointTeam edit form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getPointTeamEditSchema() {
    let PointTeamEditSchema:DynFormStructureInterface = {
        "id": "form.pointTeam.edit",
        "type": "",
        "requires": [],
        "forceIsRequired": true,
        "fields": {
            "field_team": {
                "id": "field_team",
                "type": "user_reference",
                "label": t("Team"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
        },
        "title": "",
        "description": "",
        "meta": {
            "title": "Point Team",
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    "id": '0',
                    "title": "",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "ID",
                            "field_id": "field_team",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };

    return PointTeamEditSchema;
}
