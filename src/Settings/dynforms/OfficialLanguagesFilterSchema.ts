/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get officialLanguages filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getOfficialLanguagesFilterSchema() {
    let OfficialLanguagesFilterSchema:DynFormStructureInterface = {
        "id": "form.officialLanguages.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_language": {
                "id": "field_language",
                "type": "short_text",
                "label": t("Language"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
        },
        "title": '22: '+t("Official Language management"),
        "description": "",
        "endpoint": {
            "collection": {
                "get": true,
                "post": true
            },
            "item": {
                "get": true,
                "put": true,
                "delete": true,
                "patch": true
            }
        },
        "meta": {
            "title": t("Official Languages"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Language"),
                            "field_id": "field_language",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return OfficialLanguagesFilterSchema;
}
