/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get functionsCarriedOutWsp filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getFunctionsCarriedOutWspsEditSchema() {
    let FunctionsCarriedOutWspEditSchema:DynFormStructureInterface = {
        "id": "form.functionsCarriedOutWsps.edit",
        "type": "",
        "requires": [],
        "forceIsRequired": true,
        "fields": {
            "field_id": {
                "id": "field_id",
                "type": "ulid",
                "label": t("ID"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_key_index": {
                "id": "field_key_index",
                "type": "short_text",
                "label": t("Key Index"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": { "focus": true },
                    "sort": false,
                    "filter": false
                }
            },
            "field_type": {
                "id": "field_type",
                "type": "short_text",
                "label": t("Type"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
        },
        "title": "",
        "description": "",
        "meta": {
            "title": t("Functions Carried Out by WSP"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Functions Carried Out WSPs"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("ID"),
                            "field_id": "field_id",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Key Index"),
                            "field_id": "field_key_index",
                            "weight": 0
                        },
                        "0.3": {
                            "id": "0.3",
                            "title": t("Type"),
                            "field_id": "field_type",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return FunctionsCarriedOutWspEditSchema;
}
