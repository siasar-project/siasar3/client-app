/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get surveys filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getValidationFilterSchema() {
    let ValidationFilterSchema:DynFormStructureInterface = {
        "id": "form.surveys.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_region": {
                "id": "field_region",
                "type": "administrative_division_parent_reference",
                "label": t("Community"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_changed": {
                "id": "field_changed",
                "type": "date",
                "label": t("Changed from"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {},
                    "sort": false,
                    "filter": false
                }
            },
            "field_order_changed": {
                id: "field_order_changed",
                type: "select",
                label: t('Order by date'),
                description: '',
                indexable: false,
                internal: false,
                deprecated: false,
                settings: {
                    required: true,
                    multivalued: false,
                    weight: 0,
                    meta: {},
                    sort: false,
                    filter: false,
                    options: {
                        desc: t('Newer first'),
                        asc: t('Older first'),
                    }
                }
            },
            "field_validation_type": {
                id: "field_validation_type",
                type: "radio_select",
                label: t("Type to validate"),
                description: "",
                indexable: false,
                internal: false,
                deprecated: false,
                settings: {
                    required: true,
                    multivalued: false,
                    weight: 0,
                    meta: {},
                    sort: false,
                    filter: false,
                    options: {
                        1: t("SIASAR Point"),
                        2: t("Surveys"),
                    },
                },
            }
        },
        "title": t("Validation"),
        "description": "",
        "meta": {
            "title": t("Validation"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Community"),
                            "field_id": "field_region",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Type to validate"),
                            "field_id": "field_validation_type",
                            "weight": 0
                        },
                        "0.4": {
                            "id": "0.4",
                            "title": t("Changed from"),
                            "field_id": "field_changed",
                            "weight": 0
                        },
                        "0.5": {
                            "id": "0.5",
                            "title": t("Order by date"),
                            "field_id": "field_order_changed",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return ValidationFilterSchema;
}
