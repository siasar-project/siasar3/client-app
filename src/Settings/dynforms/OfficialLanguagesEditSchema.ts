/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get officialLanguages filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getOfficialLanguagesEditSchema() {
    let OfficialLanguageEditSchema:DynFormStructureInterface = {
        "id": "form.officialLanguages.edit",
        "type": "",
        "requires": [],
        "forceIsRequired": true,
        "fields": {
            "field_id": {
                "id": "field_id",
                "type": "ulid",
                "label": t("ID"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_language": {
                "id": "field_language",
                "type": "short_text",
                "label": t("Language"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": { "focus": true },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "title": "",
        "description": "",
        "meta": {
            "title": t("Official Languages"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Official Language"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("ID"),
                            "field_id": "field_id",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Language"),
                            "field_id": "field_language",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return OfficialLanguageEditSchema;
}
