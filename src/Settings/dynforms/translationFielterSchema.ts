/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get translations filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getTranslationFilterSchema() {
    let TranslationFilterSchema:DynFormStructureInterface = {
        "id": "translation.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_contains": {
                "id": "field_contains",
                "type": "short_text",
                "label": t("String contains"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_language": {
                "id": "field_language",
                "type": "translatable_language_reference",
                "label": t("Translation language"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false,
                    "options": [],
                }
            },
            "field_search_in": {
                "id": "field_search_in",
                "type": "select",
                "label": t("Search in"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "untranslated": "Untranslated strings",
                        "translated": "Translated strings",
                        "all": "Both translated and untranslated strings",
                    },
                }
            }
        },
        "title": t("Translation tool"),
        "description": "",
        "meta": {
            "title": "",
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("String contains"),
                            "field_id": "field_contains",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Translation language"),
                            "field_id": "field_language",
                            "weight": 0
                        },
                        "0.3": {
                            "id": "0.3",
                            "title": t("Search in"),
                            "field_id": "field_search_in",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return TranslationFilterSchema;
}
