/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get surveys filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getTestFormSchema() {
    let TestFormSchema:DynFormStructureInterface = {
        "id": "form.test.fields",
        "type": "",
        formLevel: 3,
        formSdg: true,
        "requires": [],
        "fields": {
            "field_select": {
                "id": "field_select",
                "type": "select",
                "label": t("Field select"),
                "description": t("Field select"),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1",
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": t("One"),
                        "2": t("Two"),
                        "3": t("Three"),
                        "4": t("Four"),
                    },
                }
            },
            "field_select_m": {
                "id": "field_select_m",
                "type": "select",
                "label": t("Field multivalued select"),
                "description": t("Field multivalued select"),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.2",
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": t("One"),
                        "2": t("Two"),
                        "3": t("Three"),
                        "4": t("Four"),
                    },
                }
            },
            "field_radio": {
                "id": "field_radio",
                "type": "radio_select",
                "label": t("Field radio"),
                "description": t("Field radio"),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.3",
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": t("One"),
                        "2": t("Two"),
                        "3": t("Three"),
                        "4": t("Four"),
                    },
                }
            },
            "field_radio_m": {
                "id": "field_radio_m",
                "type": "radio_select",
                "label": t("Field multivalued radio"),
                "description": t("Field multivalued radio"),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.4",
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": t("One"),
                        "2": t("Two"),
                        "3": t("Three"),
                        "4": t("Four"),
                    },
                }
            },
            "field_shorttext": {
                "id": "field_shorttext",
                "type": "short_text",
                "label": t('Field short text'),
                "description": t('Field short text'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.5",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                },
            },
            "field_shorttext_m": {
                "id": "field_shorttext_m",
                "type": "short_text",
                "label": t('Field multivalued short text'),
                "description": t('Field multivalued short text'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.6",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                },
            },
            "field_integer": {
                "id": "field_integer",
                "type": "integer",
                "label": t('Field integer'),
                "description": t('Field integer'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.7",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                    "min": 0,
                    "max": 1000,
                },
            },
            "field_integer_m": {
                "id": "field_integer_m",
                "type": "integer",
                "label": t('Field multivalued integer'),
                "description": t('Field multivalued integer'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.8",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                    "min": 0,
                    "max": 1000,
                },
            },
            "field_longtext": {
                "id": "field_longtext",
                "type": "long_text",
                "label": t('Field long text'),
                "description": t('Field long text'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.9",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                },
            },
            "field_longtext_m": {
                "id": "field_longtext_m",
                "type": "long_text",
                "label": t('Field multivalued long text'),
                "description": t('Field multivalued long text'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.10",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                },
            },
            "field_mail": {
                "id": "field_mail",
                "type": "mail",
                "label": t('Field mail'),
                "description": t('Field mail'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.11",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                },
            },
            "field_mail_m": {
                "id": "field_mail_m",
                "type": "mail",
                "label": t('Field multivalued mail'),
                "description": t('Field multivalued mail'),
                "indexable": true,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.12",
                    },
                    "sort": true,
                    "filter": true,
                    "max-length": 255,
                },
            },
            "field_decimal": {
                "id": "field_decimal",
                "type": "decimal",
                "label": t('Field decimal'),
                "description": t('Field decimal'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.13",
                    },
                    "sort": false,
                    "filter": false,
                    "min": 0,
                    "max": 1000,
                    "precision": 6,
                    "scale": 2,
                },
            },
            "field_decimal_m": {
                "id": "field_decimal_m",
                "type": "decimal",
                "label": t('Field multivalued decimal'),
                "description": t('Field multivalued decimal'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.14",
                    },
                    "sort": false,
                    "filter": false,
                    "min": 0,
                    "max": 1000,
                    "precision": 6,
                    "scale": 2,
                },
            },
            "field_boolean": {
                "id": "field_boolean",
                "type": "boolean",
                "label": t('Field boolean'),
                "description": t('Field boolean'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.15",
                    },
                    "sort": false,
                    "filter": false,
                    "true_label": t("Allow"),
                    "false_label": t("Disallow"),
                    "show_labels": true,
                },
            },
            "field_boolean1": {
                "id": "field_boolean1",
                "type": "boolean",
                "label": t('Field boolean'),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.15.1",
                    },
                    "sort": false,
                    "filter": false,
                    "true_label": t("Yes"),
                    "false_label": t("No"),
                    "show_labels": true,
                },
            },
            "field_boolean_m": {
                "id": "field_boolean_m",
                "type": "boolean",
                "label": t('Field multivalued boolean'),
                "description": t('Field multivalued boolean'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.16",
                    },
                    "sort": false,
                    "filter": false,
                    "true_label": t("Yes"),
                    "false_label": t("No"),
                    "show_labels": true,
                },
            },
            "field_phone": {
                "id": "field_phone",
                "type": "phone",
                "label": t('Field phone'),
                "description": t('Field phone'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.17",
                    },
                    "sort": false,
                    "filter": false,
                },
            },
            "field_phone_m": {
                "id": "field_phone_m",
                "type": "phone",
                "label": t('Field multivalued phone'),
                "description": t('Field multivalued phone'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.18",
                    },
                    "sort": false,
                    "filter": false,
                },
            },
            "field_date": {
                "id": "field_date",
                "type": "date",
                "label": t('Field date'),
                "description": t('Field date'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.1",
                    },
                    "sort": false,
                    "filter": false,
                },
            },
            "field_date_m": {
                "id": "field_date_m",
                "type": "date",
                "label": t('Field multivalued date'),
                "description": t('Field multivalued date'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.2",
                    },
                    "sort": false,
                    "filter": false,
                },
            },
            "field_month_year": {
                "id": "field_month_year",
                "type": "month_year",
                "label": t('Field month/year'),
                "description": t('Field month/year'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.3",
                    },
                    "sort": false,
                    "filter": false,
                },
            },
            "field_month_year_m": {
                "id": "field_month_year_m",
                "type": "month_year",
                "label": t('Field multivalued month/year'),
                "description": t('Field multivalued month/year'),
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.4",
                    },
                    "sort": false,
                    "filter": false,
                },
            },
        },
        "title": "Test form fields",
        "description": "Use all form field types.",
        "endpoint": {
            "collection": {
                "get": true,
                "post": true
            },
            "item": {
                "get": true,
                "put": true,
                "delete": true,
                "patch": true
            }
        },
        "meta": {
            "title": "Test form fields",
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Basic fields"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Field select"),
                            "field_id": "field_select",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Field multivalued select"),
                            "field_id": "field_select_m",
                            "weight": 0
                        },
                        "0.3": {
                            "id": "0.3",
                            "title": t("Field radio"),
                            "field_id": "field_radio",
                            "weight": 0
                        },
                        "0.4": {
                            "id": "0.4",
                            "title": t("Field multivalued radio"),
                            "field_id": "field_radio_m",
                            "weight": 0
                        },
                        "0.5": {
                            "id": "0.5",
                            "title": t("Field short text"),
                            "field_id": "field_shorttext",
                            "weight": 0
                        },
                        "0.6": {
                            "id": "0.6",
                            "title": t("Field multivalued short text"),
                            "field_id": "field_shorttext_m",
                            "weight": 0
                        },
                        "0.7": {
                            "id": "0.7",
                            "title": t("Field integer"),
                            "field_id": "field_integer",
                            "weight": 0
                        },
                        "0.8": {
                            "id": "0.8",
                            "title": t("Field multivalued integer"),
                            "field_id": "field_integer_m",
                            "weight": 0
                        },
                        "0.9": {
                            "id": "0.9",
                            "title": t("Field long text"),
                            "field_id": "field_longtext",
                            "weight": 0
                        },
                        "0.10": {
                            "id": "0.10",
                            "title": t("Field multivalued long text"),
                            "field_id": "field_longtext_m",
                            "weight": 0
                        },
                        "0.11": {
                            "id": "0.11",
                            "title": t("Field mail"),
                            "field_id": "field_mail",
                            "weight": 0
                        },
                        "0.12": {
                            "id": "0.12",
                            "title": t("Field multivalued mail"),
                            "field_id": "field_mail_m",
                            "weight": 0
                        },
                        "0.13": {
                            "id": "0.13",
                            "title": t("Field decimal"),
                            "field_id": "field_decimal",
                            "weight": 0
                        },
                        "0.14": {
                            "id": "0.14",
                            "title": t("Field multivalued decimal"),
                            "field_id": "field_decimal_m",
                            "weight": 0
                        },
                        "0.15": {
                            "id": "0.15",
                            "title": t("Field boolean"),
                            "field_id": "field_boolean",
                            "weight": 0
                        },
                        "0.15.1": {
                            "id": "0.15.1",
                            "title": t("Field boolean"),
                            "field_id": "field_boolean1",
                            "weight": 0
                        },
                        "0.16": {
                            "id": "0.16",
                            "title": t("Field multivalued boolean"),
                            "field_id": "field_boolean_m",
                            "weight": 0
                        },
                        "0.17": {
                            "id": "0.17",
                            "title": t("Field phone"),
                            "field_id": "field_phone",
                            "weight": 0
                        },
                        "0.18": {
                            "id": "0.18",
                            "title": t("Field multivalued phone"),
                            "field_id": "field_phone_m",
                            "weight": 0
                        },
                    },
                },
                {
                    id: '1',
                    "title": t("Two properties fields"),
                    "children": {
                        "1.1": {
                            "id": "1.1",
                            "title": t("Field date"),
                            "field_id": "field_date",
                            "weight": 0
                        },
                        "1.2": {
                            "id": "1.2",
                            "title": t("Field multivalued date"),
                            "field_id": "field_date_m",
                            "weight": 0
                        },
                        "1.3": {
                            "id": "1.3",
                            "title": t("Field month/year"),
                            "field_id": "field_month_year",
                            "weight": 0
                        },
                        "1.4": {
                            "id": "1.4",
                            "title": t("Field multivalued month/year"),
                            "field_id": "field_month_year_m",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };

    return TestFormSchema;
}
