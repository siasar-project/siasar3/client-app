/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get Household process filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getHHProcessFilterSchema() {
    let HHProcessFilterSchema:DynFormStructureInterface = {
        "id": "form.hhprocess.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_region": {
                "id": "field_region",
                "type": "administrative_division_reference",
                "label": t("Community"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_open": {
                "id": "field_open",
                "type": "radio_select",
                "label": t("Status"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": { },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "A": t("Opened"),
                        "B": t("Closed"),
                        "C": t("Any"),
                    }
                },
            },
        },
        "title": t("Search process"),
        "description": "",
        "meta": {
            "title": t("Process"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Community"),
                            "field_id": "field_region",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Status"),
                            "field_id": "field_open",
                            "weight": 0
                        },
                    },
                },
            ]
        }
    };
    
    return HHProcessFilterSchema;
}
