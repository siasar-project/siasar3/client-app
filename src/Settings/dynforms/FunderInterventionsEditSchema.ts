/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get funderInterventions filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getFunderInterventionsEditSchema() {
    let FunderInterventionEditSchema:DynFormStructureInterface = {
        "id": "form.funderInterventions.edit",
        "type": "",
        "requires": [],
        "forceIsRequired": true,
        "fields": {
            "field_id": {
                "id": "field_id",
                "type": "ulid",
                "label": t("ID"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_name": {
                "id": "field_name",
                "type": "short_text",
                "label": t("Name"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": { "focus": true },
                    "sort": false,
                    "filter": false
                }
            },
            "field_address": {
                "id": "field_address",
                "type": "short_text",
                "label": t("Adress"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_phone": {
                "id": "field_phone",
                "type": "short_text",
                "label": t("Phone"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_contact": {
                "id": "field_contact",
                "type": "short_text",
                "label": t("Contact"),
                "description": '',
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "title": "",
        "description": "",
        "meta": {
            "title": t("Funder Interventions"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Funder Intervention"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("ID"),
                            "field_id": "field_id",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("Name"),
                            "field_id": "field_name",
                            "weight": 0
                        },
                        "0.3": {
                            "id": "0.3",
                            "title": t("Address"),
                            "field_id": "field_address",
                            "weight": 0
                        },
                        "0.4": {
                            "id": "0.4",
                            "title": t("Phone"),
                            "field_id": "field_phone",
                            "weight": 0
                        },
                        "0.5": {
                            "id": "0.5",
                            "title": t("Contact"),
                            "field_id": "field_contact",
                            "weight": 0
                        }
                    },
                },
            ]
        }
    };
    
    return FunderInterventionEditSchema;
}
