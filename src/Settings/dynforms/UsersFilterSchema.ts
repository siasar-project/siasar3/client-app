/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { DynFormStructureInterface } from "@/objects/DynForm/DynFormStructureInterface";
import t from "@/utils/i18n";

/**
 * Get users filter form schema.
 *
 * @returns {DynFormStructureInterface}
 */
export default function getUsersFilterSchema() {
    let SurveysFilterSchema:DynFormStructureInterface = {
        "id": "form.users.filter",
        "type": "",
        "requires": [],
        "fields": {
            "field_username": {
                "id": "field_username",
                "type": "short_text",
                "label": t("Username"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false
                }
            },
            "field_email": {
                "id": "field_email",
                "type": "short_text",
                "label": t("E-mail"),
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {  },
                    "sort": false,
                    "filter": false,
                }
            }
        },
        "title": t("Search Users"),
        "description": "",
        "meta": {
            "title": t("Users"),
            "version": "",
            "allow_sdg": false,
            "field_groups": [
                {
                    id: '0',
                    "title": t("Filters"),
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": t("Username"),
                            "field_id": "field_username",
                            "weight": 0
                        },
                        "0.2": {
                            "id": "0.2",
                            "title": t("E-mail"),
                            "field_id": "field_email",
                            "weight": 0
                        }
                    },
                },
            ]
        }
    }
    return SurveysFilterSchema
}
