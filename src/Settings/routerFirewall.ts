/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {RouteRule} from "@/objects/RouteRule";

/**
 * This file contains firewall routes.
 */

/**
 * The first rule that match is used.
 */
const rules:RouteRule[] = [
    {
        name: "Surveys",
        pattern: /^\/surveys*/,
        needSession: true,
        permissions: [],
        offline: false,
    },
    {
        name: "Households",
        pattern: /^\/households*/,
        needSession: true,
        permissions: [
            'read doctrine householdprocess'
        ],
        offline: true,
    },
    {
        name: "User management",
        pattern: /^\/admin\/users*/,
        needSession: true,
        permissions: [
            'create doctrine user',
        ],
        offline: false,
    },
    {
        name: "Points",
        pattern: /^\/point*/,
        needSession: true,
        permissions: [],
        offline: true,
    },
    {
        name: "Mails",
        pattern: /^\/mail*/,
        needSession: true,
        permissions: [],
        offline: false,
    },
    {
        name: "Country management",
        pattern: /^\/admin\/country/,
        needSession: true,
        permissions: [
            'update doctrine country',
        ],
        offline: false,
    },
    {
        name: "Translation tool",
        pattern: /^\/admin\/translate/,
        needSession: true,
        permissions: [
            'create doctrine localetarget',
        ],
        offline: false,
    },
    {
        name: "Home",
        pattern: /^\/$/,
        needSession: false,
        permissions: [],
        offline: true,
    },
    {
        name: "Login",
        pattern: /^\/login$/,
        needSession: false,
        permissions: [],
        offline: true,
    },
    {
        name: "Test field",
        pattern: /^\/dynform_field_test$/,
        environment: 'development',
        needSession: false,
        permissions: [],
        offline: false,
    },
    {
        name: "Test form",
        pattern: /^\/dynform_test$/,
        environment: 'development',
        needSession: true,
        permissions: [],
        offline: false,
    },
    {
        name: "Household",
        pattern: /^\/household\//,
        needSession: true,
        permissions: [
            'read household record',
        ],
        offline: true,
    },
    {
        name: "Validation",
        pattern: /^\/validation/,
        needSession: true,
        permissions: [
            'can do validate transition in workflow inquiring',
        ],
        offline: false,
    },
    {
        name: "Dashboard",
        pattern: /^\/dashboard/,
        needSession: true,
        permissions: [],
        offline: true,
    },
    {
        name: "Default",
        pattern: /^/,
        needSession: true,
        permissions: [],
        offline: true,
    },
];

/**
 * Get firewall rules.
 *
 * @returns {RouteRule[]}
 */
export default function getFirewallRules() {
    return rules;
}
