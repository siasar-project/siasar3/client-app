/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {createContext, useEffect, useReducer, useState} from "react";
import loadingReducer from "./reducers/loadingReducer";
import {User} from "../object/User";
import {smGetSession, smSetSession} from "@/utils/sessionManager";

interface IContext {
    loading: boolean;
    setLoading: (loading: boolean) => void;
    user: User | (() => Promise<User>) | null;
    // getCurrentToken: () => UserSession | null;
    isUser: () => boolean;
    isDigitizer: () => boolean;
    isValidator: () => boolean;
    isAdmin: () => boolean;
    logout: () => void;
}
export const SiasarContext = createContext<IContext>({} as IContext);

/**
 * SIASAR global context.
 *
 * @param props
 *
 * @constructor
 */
const SiasarContextProvider = (props: any) => {
    const [user, setUser] = useState<User | (() => Promise<User>) | null>(null);
    const [loading, loadingDispatch] = useReducer(loadingReducer, true);

    useEffect(() => {
        /**
         * Set user country.
         */
        async function userCountry() {
            // const user = await api.getCurrentUserWithCountry(smGetSession());
            setUser(user)
        }

        let session = smGetSession();
        setUser(session);

        // If the session haven't a complete country.
        if (session !== null && session?.country?.deep === undefined) {
            userCountry()
        }
    }, [loading]);

    /**
     * Remove the current user.
     */
    const logout = () => {
        smSetSession(null);
    };

    // /**
    //  * Get current user session tokens.
    //  */
    // const getCurrentToken = (): UserSession | null => {
    //     let session = smGetSession();
    //     if (session && session.session && session.session.token) {
    //         return session.session.token;
    //     }
    //
    //     return null;
    // };

    /**
     * Have user session?
     *
     * @returns {boolean}
     */
    const isUser = () => {
        return user !== null;
    };

    /**
     * Have current user an admin role?
     *
     * @returns {boolean}
     */
    const isAdmin = () => {
        if (!user || !("roles" in user && user.roles)) {
            return false;
        }

        return (
            (user !== null && "roles" in user && user?.roles?.includes("ROLE_ADMIN_LOCAL")) ||
            (user !== null && "roles" in user && user?.roles?.includes("ROLE_ADMIN"))
        );
    };

    /**
     * Have current user digitizer role?
     *
     * @returns {boolean}
     */
    const isDigitizer = () => {
        if (!user || !("roles" in user && user.roles)) {
            return false;
        }
        return (user !== undefined && "roles" in user && user?.roles?.includes("ROLE_DIGITIZER")) || isAdmin();
    };

    /**
     * Have current user validator role?
     *
     * @returns {boolean}
     */
    const isValidator = () => {
        if (!user || !("roles" in user && user.roles)) {
            return false;
        }
        return (user !== undefined && "roles" in user && user?.roles?.includes("ROLE_SECTORIAL_VALIDATOR")) || isAdmin();
    };

    /**
     * Display loading animation.
     *
     * @param loading
     * @returns {any}
     */
    const setLoading = (loading: boolean) => {
        return loadingDispatch({
            type: "SET_LOADING",
            loading,
        });
    };

    // /**
    //  * Load countries?
    //  */
    // const fetchCountries = async () =>
    //     api.getCountries().then((payload: any) => {
    //         countriesDispatch({
    //             type: "FETCH_COUNTRIES",
    //             payload,
    //         });
    //     });

    // User session.
    // let preUser = Api.storageGetSession();
    // let user: () => Promise<User> = async () => api.getCurrentUserWithCountry(preUser);

    // if (typeof localStorage !== 'undefined') {
    //     // Load session from local storage.
    //     let aux = localStorage.getItem('session');
    //     if (aux !== null) {
    //         user = JSON.parse(aux);
    //     }
    // }
    // country Context
    // const [countries, countriesDispatch] = useReducer(
    //     countryReducer,
    //     [],
    //     countriesInitializer
    // );
    // useEffect(setCountriesAction(countries), [countries]);

    return (
        <SiasarContext.Provider
            value={{
                user,
                logout,
                // getCurrentToken,
                isUser,
                isAdmin,
                isDigitizer,
                isValidator,
                // countries,
                // fetchCountries,
                loading,
                setLoading,
            }}
        >
            {props.children}
        </SiasarContext.Provider>
    );
};

export default SiasarContextProvider;
