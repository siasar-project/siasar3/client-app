/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {I18nLanguage} from "@/objects/I18nLiterals";

/**
 * Strings in home and login pages only, without session.
 */
const I18N_LITERALS_ES = {
    // Pages.
    "Home": "Inicio",
    "Rural Water and Sanitation Information System": "Sistema de Información de Agua y Saneamiento Rural",
    "SIASAR, the Rural Water and Sanitation Information System, is a joint initiative launched by the governments of Honduras, Nicaragua, and Panamá, that soon expanded to other countries and regions of Latin America, Asia and Africa. The strategic purpose of this initiative is to have a basic, updated, and comparable information on the sustainability of rural water supply, sanitation, and hygiene (WASH) services in a given country. In essence, SIASAR is intended to be a decision support tool for the rural WASH sector, and is defined on the basis of a set of indicators that are aggregated into a reduced number of indices and dimensions.":
        "SIASAR, el Sistema de Información de Agua y Saneamiento Rural, es una iniciativa conjunta de los gobiernos de Honduras, Nicaragua y Panamá, que pronto se extendió a otros países y regiones de América Latina, Asia y África. El propósito estratégico de esta iniciativa es contar con información básica, actualizada y comparable sobre la sostenibilidad de los servicios rurales de agua, saneamiento e higiene (WASH) en un país determinado. En esencia, SIASAR pretende ser una herramienta de apoyo a la toma de decisiones para el sector WASH rural, y se define a partir de un conjunto de indicadores que se agregan en un número reducido de índices y dimensiones.",
    "It can be used to better understand rural water supply and sanitation services in countries which are experiencing low levels of coverage, limited self-sustainability, and a lack of information to guide evidence-based decision-making. The aim is to support planning, coordination, and evaluation of the sector’s stakeholder activities; to monitor coverage, quality and sustainability of rural water supply and sanitation services; and to record performance of technical assistance providers, including their logistical limitations.":
        "Puede utilizarse para comprender mejor los servicios rurales de abastecimiento de agua y saneamiento en países que registran bajos niveles de cobertura, una autosostenibilidad limitada y una falta de información para orientar la toma de decisiones basada en pruebas. El objetivo es apoyar la planificación, coordinación y evaluación de las actividades de las partes interesadas del sector; supervisar la cobertura, calidad y sostenibilidad de los servicios rurales de abastecimiento de agua y saneamiento; y registrar el rendimiento de los prestadores de asistencia técnica, incluidas sus limitaciones logísticas.",
    "Loading...": "Cargando...",
    "Loading translations...": "Cargando traducciones...",
    "Loading system permissions...": "Cargando permisos del sistema...",
    "Loading system roles...": "Cargando roles del sistema...",
    "Loading user country...": "Cargando país del usuario...",
    "Loading administrative divisions...": "Cargando divisiones administrativas...",
    "Loading form structures...": "Cargando estructuras de encuestas...",
    "Loading concentration units...": "Cargando unidades de concentración...",
    "Loading flow units...": "Cargando unidades de caudal...",
    "Loading length units...": "Cargando unidades de longitud...",
    "Loading volume units...": "Cargando unidades de volumen...",
    "Login": "Iniciar sesión",
    "Username": "Nombre de usuario",
    "Password": "Contraseña",
    "Log-in": "Entrar",
    "Invalid Credentials": "Credenciales no válidas",
    // Main menu.
    "Dashboard": "Panel de control",
    "SIASAR Point": "Punto SIASAR",
    "Surveys": "Encuestas",
    "Households": "Hogares",
    "Validation": "Validación",
    "Messages": "Mensajes",
    "Administration": "Administración",
    "Users": "Usuarios",
    "Country": "País",
};
const I18N_LITERALS_PT = {
    // Pages.
    "Home": "Início",
    "Rural Water and Sanitation Information System": "Sistema de Informação sobre Água e Saneamento Rural",
    "SIASAR, the Rural Water and Sanitation Information System, is a joint initiative launched by the governments of Honduras, Nicaragua, and Panamá, that soon expanded to other countries and regions of Latin America, Asia and Africa. The strategic purpose of this initiative is to have a basic, updated, and comparable information on the sustainability of rural water supply, sanitation, and hygiene (WASH) services in a given country. In essence, SIASAR is intended to be a decision support tool for the rural WASH sector, and is defined on the basis of a set of indicators that are aggregated into a reduced number of indices and dimensions.":
        "SIASAR, o Sistema de Informação de Água e Saneamento Rural, é uma iniciativa conjunta dos governos de Honduras, Nicarágua e Panamá, que logo foi estendida a outros países e regiões da América Latina, Ásia e África. O objetivo estratégico desta iniciativa é ter informações básicas, atualizadas e comparáveis sobre a sustentabilidade dos serviços de água, saneamento e higiene rural (WASH) em um determinado país. Em essência, SIASAR pretende ser uma ferramenta de apoio à decisão para o setor de WASH rural, e é definida por um conjunto de indicadores que são agregados em um pequeno número de índices e dimensões.",
    "It can be used to better understand rural water supply and sanitation services in countries which are experiencing low levels of coverage, limited self-sustainability, and a lack of information to guide evidence-based decision-making. The aim is to support planning, coordination, and evaluation of the sector’s stakeholder activities; to monitor coverage, quality and sustainability of rural water supply and sanitation services; and to record performance of technical assistance providers, including their logistical limitations.":
        "Ele pode ser usado para entender melhor os serviços de abastecimento de água e saneamento rural em países com baixos níveis de cobertura, auto-sustentabilidade limitada e falta de informações para orientar a tomada de decisões baseada em evidências. O objetivo é apoiar o planejamento, coordenação e avaliação das atividades das partes interessadas do setor; monitorar a cobertura, qualidade e sustentabilidade dos serviços de abastecimento de água e saneamento rural; e registrar o desempenho dos prestadores de assistência técnica, incluindo suas restrições logísticas.",
    "Loading...": "Carregando...",
    "Loading translations...": "Carregando traduções...",
    "Loading system permissions...": "Carregamento permissões de sistema...",
    "Loading system roles...": "Carregamento das funções do sistema...",
    "Loading user country...": "Carregando o país do usuário...",
    "Loading administrative divisions...": "Carregamento de divisões administrativas...",
    "Loading form structures...": "Carregamento de estruturas de pesquisa...",
    "Loading concentration units...": "Carregamento de unidades de concentração...",
    "Loading flow units...": "Carregando unidades de fluxo...",
    "Loading length units...": "Carregamento de unidades de comprimento...",
    "Loading volume units...": "Carregamento unidades de volume...",
    "Login": "Fazer login",
    "Username": "Nome do usuário",
    "Password": "Senha",
    "Log-in": "Entrar",
    "Invalid Credentials": "Credenciais inválidas",
    // Main menu.
    "Dashboard": "Painel de instrumentos",
    "SIASAR Point": "Ponto SIASAR",
    "Surveys": "Pesquisas",
    "Households": "Domicílios",
    "Validation": "Validação",
    "Messages": "Mensagens",
    "Administration": "Administração",
    "Users": "Usuários",
    "Country": "País",
};

/**
 * Get default translations.
 *
 * @param langcode {string}
 *
 * @returns {I18nLanguage}
 */
export function i18nDefaultLiterals(langcode:string):I18nLanguage {
    if (langcode.startsWith('es')) {
        return {
            id: langcode,
            literals: I18N_LITERALS_ES,
        };
    }
    if (langcode.startsWith('pt')) {
        return {
            id: langcode,
            literals: I18N_LITERALS_PT,
        };
    }

    return {
        id: 'en',
        literals: {},
    };
}
