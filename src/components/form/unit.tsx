/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, { useState, useContext, useEffect } from "react";
import { SiasarUnit } from "@/objects/Generic";
import { Country } from "@/objects/Country";
import { SiasarContext } from "@/context/SiasarContext";
import LoadingField from "./loadingField";
import OutlinedDiv from "../common/OutlinedDiv";
import Grid from "@material-ui/core/Grid";
import {dmGetUnits} from "@/utils/dataManager";

interface Unit {
  symbol: string;
  label: string;
  rate: string;
}

export interface UnitProps {
  defaultValue: SiasarUnit | null;
  classes: any;
  className: any;
  id: string;
  label: string;
  placeholder?: string;
  fullWidth: boolean;
  onChange: Function;
  disabled?: boolean;
  helperText: string;
  optionSettings: {
    country: {
      main: keyof Country;
      units: keyof Country;
    };
    configuration: string;
  };
  settings: {
    classValue: string;
  };
}

type CountryUnits = Exclude<keyof Country, "formLevel">;

/**
 * @param {UnitProps} props
 * @returns {any}
 *   Field structure.
 */
export function UnitField(props: UnitProps) {
  const [defaultUnit, setDefaultUnit] = useState<string | undefined>(undefined);
  const [defaultAmount, setDefaultAmount] = useState<string | undefined>(
    undefined
  );
  const { user } = useContext(SiasarContext);
  const [country, setCountry] = useState<Country | undefined>(undefined);

  const [mainUnit, setMainUnit] = useState<Unit | undefined>(undefined);
  const [definedUnits, setDefinedUnit] = useState<Unit[]>([]);
  const [units, setUnits] = useState<{ [key: string]: Unit }>({});
  const [loading, setLoading] = useState(true);
  const [select, setSelect] = useState<HTMLSelectElement | null>(null);
  const [input, setInput] = useState<HTMLInputElement | null>(null);

  const {
    defaultValue: default_value,
    classes,
    className,
    id,
    label,
    fullWidth,
    onChange,
    disabled,
    settings,
    placeholder,
    optionSettings,
    helperText,
    ...rest
  } = props;

  // on Mount
  useEffect(() => {
    if (default_value) {
      setDefaultAmount(default_value.value);
      setDefaultUnit(default_value.key);
    }
    dmGetUnits(optionSettings.configuration).then(units => {
      setUnits(units.all);
    });
  }, [default_value, optionSettings.configuration/*, getAllUnits*/]);

  //user Loaded
  useEffect(() => {
    if (user && "country" in user && user?.country) {
      setCountry(user.country);
    }
  }, [user]);

  //Country Loaded
  useEffect(() => {
    if (country?.deep && units) {
      const main = units[country[optionSettings.country.main as CountryUnits]];
        const defined = Object.keys(country[optionSettings.country.units]).map(
            (key: string) => units[key]
        );
        setMainUnit(main);
        setDefinedUnit(defined);

    }
  }, [country, optionSettings.country.main, optionSettings.country.units, units]);

  // is loaded
  useEffect(() => {
    if (mainUnit !== undefined && definedUnits.length) {
      setLoading(false);
    }
  }, [mainUnit, definedUnits]);

  /**
   * Subfield change handler.
   *
   * @param {ChangeEventHandler} event
   */
  const handleChange: React.ChangeEventHandler<HTMLSelectElement | HTMLInputElement> =
      (event) => {
    const value = { unit: select?.value, value: input?.value };
    onChange({ target: { id, type: "unit", value } });
  };

  /**
   * Get unit keys.
   *
   * @returns {Array}
   *   Select element compatible array.
   */
  const unitsArray = () => {
    return Object.keys(units).map((key: string) => units[key]);
  };

  if (loading) return <LoadingField label={label} />;

  if (mainUnit === undefined) return <div></div>;

  return (
    <OutlinedDiv
        label={label}
        fullWidth
        helperText={helperText}
        className={(classes.formControl+" "+className)}
    >
      <div style={{ display: "flex" }} className={(classes.formControl+" "+className)}>
        <Grid container spacing={4} alignItems="center" >
          <Grid item xs={6} sm={6}>
            <label htmlFor={'select_'+id}>
              <span className={'label'}>Unit</span>
            </label>
            <br/>
            <select
                id={"select_" + id}
                onChange={handleChange}
                ref={(ref) => setSelect(ref)}
                defaultValue={defaultUnit}
            >
              <optgroup label="Main">
                <option
                    key={`main_${mainUnit.symbol}${mainUnit.label}`}
                    value={mainUnit.label}
                >
                  {mainUnit.label}
                  {('' !== mainUnit.symbol) && (undefined !== mainUnit.symbol) &&
                      (' ('+mainUnit.symbol+')')
                  }
                </option>
              </optgroup>
              <optgroup label="Country">
                {definedUnits.map((item: any) => (
                    <option
                        key={`defined_${item.symbol}${item.label}`}
                        value={item.label}
                    >
                      {item.label}
                      {('' !== item.symbol) && (undefined !== item.symbol) &&
                          (' ('+item.symbol+')')
                      }
                    </option>
                ))}
              </optgroup>
              <optgroup label="All">
                {unitsArray().map((item: any) => (
                    <option
                        key={`all_${item.symbol}${item.label}`}
                        value={item.label}
                    >
                      {item.label}
                      {('' !== item.symbol) && (undefined !== item.symbol) &&
                          (' ('+item.symbol+')')
                      }
                    </option>
                ))}
              </optgroup>
            </select>
          </Grid>
          <Grid item xs={6} sm={6} className="underlined-input">
            <label htmlFor={'ammount_'+id}>
              <span className={'label'}>Amount</span>
            </label>
            <input
                id={"ammount_" + id}
                type="number"
                step="0.01"
                style={{ flex: "1", marginLeft: "5px" }}
                className="MuiInputBase-input"
                onChange={handleChange}
                ref={(ref) => setInput(ref)}
                defaultValue={defaultAmount}
            />
          </Grid>
        </Grid>
      </div>
    </OutlinedDiv>
  );
}
