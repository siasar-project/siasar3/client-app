/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {useEffect, useState} from "react";
import {dmGetUserLanguages} from "@/utils/dataManager";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {TextField} from "@material-ui/core";

interface IProps {
    language?: OptionItem
    id?: string
    name?: string
    onChange?: (event: any, values?: any) => void
    disabled?: boolean
}

interface OptionItem {
    value: string,
    name: string,
}

/**
 * User language field.
 *
 * @param props
 * @constructor
 */
const UserLanguageField = (props: IProps) => {
    const { language, onChange, ...params } = props
    const [ languages, setLanguages ] = useState<Array<OptionItem>>([]);
    const [value, setValue] = useState<OptionItem>({name: "", value: ""});
    const [inputValue, setInputValue] = useState("");

    // Load languages and init value.
    useEffect(() => {
        dmGetUserLanguages().then(remoteLanguages => {
            let options:Array<OptionItem> = [];
            for (let i = 0; i < remoteLanguages.length; i++) {
                let item = {value: '/api/v1/languages/' + remoteLanguages[i].id, name: remoteLanguages[i].name};
                if (language && language.value === '/api/v1/languages/' + remoteLanguages[i].id) {
                    language.name = remoteLanguages[i].name;
                    setValue(remoteLanguages[i]);
                    setInputValue(remoteLanguages[i].name);
                }
                options.push(item);
            }
            setLanguages(options);
        });
    }, []);

    /**
     * Fix form input id.
     *
     * @param event
     * @param values
     */
    function handleOnchange(event: any, values: any) {
        event.target.id = props.id;
        setValue(values);
        if (onChange) {
            onChange(event, values);
        }
    }

    return (
        <Autocomplete
            {...params}
            value={value}
            onChange={handleOnchange}
            onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
            }}
            options={languages}
            getOptionLabel={(option) => option.name || ""}
            getOptionSelected={(option, value) => value && option.value === value.value}
            renderOption={(option) => (
                <>
                    {option.name}
                </>
            )}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label="Language"
                    variant="outlined"
                    inputProps={{
                        ...params.inputProps,
                        autoComplete: "new-password", // disable autocomplete and autofill
                    }}
                />
            )}
        />
    )
}

export default UserLanguageField;
