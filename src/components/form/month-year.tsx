/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, { useState, useEffect } from "react";
import LoadingField from "./loadingField";
import OutlinedDiv from "../common/OutlinedDiv";
import { useField } from "../../hooks/field";
import { validMonth, validYear } from "../../validators/general";
import Grid from "@material-ui/core/Grid";

interface MonthYear {
  month: string;
  year: string;
}

export interface MonthYearProps {
  defaultValue: MonthYear | null;
  classes: any;
  className: any;
  id: string;
  label: string;
  placeholder?: string;
  fullWidth: boolean;
  onChange: Function;
  disabled?: boolean;
  helperText: string;
}

/**
 * Month and year form field.
 *
 * @param {MonthYearProps} props
 * @returns {any}
 *   Field structure.
 */
export function MonthYearField(props: MonthYearProps) {
  const {
    defaultValue,
    classes,
    className,
    id,
    label,
    fullWidth,
    onChange,
    disabled,
    placeholder,
    helperText,
    ...rest
  } = props;

  const [defaultMonth, month, monthError, setMonth] = useField<string>(
    defaultValue?.month ?? "",
    validMonth,
    { start: false, message: "It Should be a valid month" }
  );
  const [defaultYear, year, yearError, setYear] = useField<string>(
    defaultValue?.year ?? "",
    validYear,
    { start: false, message: "It Should be a valid year" }
  );
  const [loading, setLoading] = useState(true);

  const [inputM, setInputM] = useState<HTMLSelectElement | null>(null);
  const [inputY, setInputY] = useState<HTMLInputElement | null>(null);

  // on Mount
  useEffect(() => {
    if (defaultValue) {
      setMonth(defaultValue.month);
      setYear(defaultValue.year);
    }
    setLoading(false);
  }, [defaultValue, setMonth, setYear]);

  /**
   * On subfield changes take data and thrown event.
   */
  const handleChange = () => {
    if (inputY) setYear(inputY.value);
    if (inputM) setMonth(inputM.value);
    const value = {
      month: inputM?.value ?? "",
      year: inputY?.value ?? "",
    };
    if (onChange instanceof Function) {
      onChange({ target: { id, type: "month_year", value } });
    }
  };

  /**
   * Current field help text.
   *
   * @returns {string}
   *   Text to display.
   */
  const getHelperText = () => {
    if (monthError || yearError) {
      if (!monthError) return yearError;
      if (!yearError) return monthError;
      return `${monthError} - ${yearError}`;
    }
    return helperText;
  };

  if (loading) return <LoadingField label={label} />;

  return (
    <OutlinedDiv
      label={label}
      fullWidth
      helperText={getHelperText()}
      error={monthError !== "" || yearError !== ""}
      className={(classes.formControl+" "+className)}
    >
      <div style={{ display: "flex" }} className={(classes.formControl+" "+className)}>
        <Grid container spacing={4} alignItems="center" >
          <Grid item xs={6} sm={6} className="underlined-input">
            <label htmlFor={'select_'+id}>
              <span className={'label'}>Month</span>
            </label>
            <br/>
            <select
                id={"ammount_" + id}
                style={{ flex: "1", marginLeft: "5px" }}
                className="MuiInputBase-input"
                onChange={handleChange}
                ref={(ref) => setInputM(ref)}
                defaultValue={defaultMonth}
            >
              <option value=""> </option>
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">May</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">August</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">december</option>
            </select>
          </Grid>
          <Grid item xs={6} sm={6} className="underlined-input">
            <label htmlFor={'ammount_'+id}>
              <span className={'label'}>Year</span>
            </label>
            <input
                id={"ammount_" + id}
                type="number"
                step="1"
                max={9999}
                min={1000}
                style={{ flex: "5", marginLeft: "5px" }}
                className="MuiInputBase-input"
                onChange={handleChange}
                ref={(ref) => setInputY(ref)}
                defaultValue={defaultYear}
            />
          </Grid>
        </Grid>
      </div>
    </OutlinedDiv>
  );
}
