/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";
import { createStyles, Theme, withStyles, WithStyles } from "@material-ui/core/styles";
import { Component } from "react";
import Markdown from "@/components/common/Markdown";

interface IProps {
    id: string;
    label: string;
    options: string[];
    onChange: any;
    defaultValue: string;
    helperText: string;
    disabled?: boolean;
    className: any;
    multiple: boolean
}

/**
 * Theme update.
 *
 * @param {Theme} theme
 *   Original theme.
 * @returns {Theme}
 *   Updated theme.
 */
const styles = (theme: Theme) => createStyles({
        formControl: {
            width: "100%",
            minWidth: 120,
            margin: theme.spacing(1, 0),
        },
        label: {
            backgroundColor: theme.palette.background.paper,
            padding: theme.spacing(0, 1),
        },
        root: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.paper,
            padding: theme.spacing(8, 0, 6),
        },
        center: {
            margin: 0,
        },
        field: {
            margin: theme.spacing(1, 0),
        },
        subformdata: {
            borderLeftStyle: "dotted",
            borderLeftColor: "blue",
        }
    });

/**
 * Custom select API form field.
 *
 * @returns {any}
 */
class SelectField extends Component<IProps & WithStyles<typeof styles>, any> {

    /**
     * Build form control from properties.
     *
     * @returns {any}
     *   Then control structure.
     */
    render = () => {
        const { id, label, options, classes, onChange, helperText, className, ...rest } = this.props;

        return (
            <FormControl variant="outlined" className={(classes.formControl+" "+className)}>
                <InputLabel className={classes.label} id={id + "-label"}>
                    {label}
                </InputLabel>
                <Select onChange={onChange} id={id} name={id} {...rest}>
                    <MenuItem value="" />
                    {Object.values(options).map((item: any, key: number) => (
                        <MenuItem key={key} value={Object.keys(options)[key]}>
                            {item}
                        </MenuItem>
                    ))}
                </Select>
                { helperText !== '' ? (<Markdown text={helperText}/>) : (<></>) }

            </FormControl>
        );
    };
}

export default withStyles(styles)(SelectField);
