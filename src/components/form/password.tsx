/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { Box } from "@material-ui/core"
import OutlinedDiv from "../common/OutlinedDiv"
import { InputField } from "./input"
import {useState} from "react";

interface IProps {
  id?: string
  onChange: (event: any, values: any) => void
}

/**
 * New password field with validation.
 *
 * @param {IProps} props
 *
 * @constructor
 */
const PasswordField = (props: IProps) => {
  const { id, onChange } = props
  const [error, setError] = useState<boolean>(false)
  const [pwd, setPwd] = useState<string>('');

    /**
     * Update password state and throw errors or events.
     *
     * @param {any} event
     * @param {any} values
     */
  const handleChange = (event: any, values: any) => {
      if (event.target.id === id + '_origin') {
          setPwd(event.target.value)
      }
      else {
          if (event.target.value !== pwd) {
              setError(true)
          }
          else {
              setError(false)
              onChange(event, values)
          }
      }
  }

  return (
      <OutlinedDiv
          label="Password"
      >
        <Box sx={{ display: 'flex', padding: 2 }}>
          <InputField
              id={id + "_origin"}
              type="Password"
              style={{paddingRight: '5px'}}
              onChange={handleChange}
              inputProps={{
                autoComplete: "new-password", // disable autocomplete and autofill
              }}
          />

          <InputField
              disabled={!pwd}
              id={id}
              sx={{ py: 1 }}
              type="Password"
              style={{paddingLeft: '5px'}}
              error={error}
              helperText={error ? "The passwords must be match." : ""}
              onChange={handleChange}
              inputProps={{
                autoComplete: "new-password", // disable autocomplete and autofill
              }}
          />
        </Box>
      </OutlinedDiv>
  )
}

export default PasswordField
