/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import Grid from "@material-ui/core/Grid";
import OutlinedDiv from "../common/OutlinedDiv"
import Loading from "@/components/common/Loading";

interface LoadingProps {
  label: string;
}

/**
 * Loading animation.
 *
 * @param {LoadingProps} props
 * @returns {any}
 *   Loading animation.
 */
export default function LoadingField( props: LoadingProps){
  const { label } = props;
  return (
    <Grid item xs={12} sm={12}>
      <OutlinedDiv  label={label} fullWidth > 
        <Loading />           
      </OutlinedDiv>
    </Grid>

  );

}
