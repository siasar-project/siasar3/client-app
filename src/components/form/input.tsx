/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {TextField} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {isValidInput} from "../../validators/general";
import {useField} from "../../hooks/field";

/**
 * Generic input field.
 *
 * @param {any} props
 * @returns {any}
 *   Structure.
 */
export function InputField(props: any) {
    const {
        id,
        name,
        className,
        label,
        placeholder,
        onChange,
        defaultValue: default_value,
        type,
        helperText,
        ...rest
    } = props;

    const [defaultValue, value, valueError, setValue] = useField<string>(
        default_value,
        isValidInput(type),
        {start: false, message: `It Should be a valid ${type}`}
    );

    /**
     * Update state and throw event.
     *
     * @param {ChangeEventHandler<HTMLInputElement>} event
     */
    const handleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        setValue(event.target.value)
        onChange(event)
    };

    /**
     * Get field help text.
     *
     * @returns {string}
     *   Help text.
     */
    const getHelperText = () => {
        if (valueError !== '') return valueError
        return helperText;
    };

    return (
        <>
            <Grid item xs={12} sm={12}>
                <TextField
                    id={id}
                    label={label}
                    type={type}
                    error={valueError !== ''}
                    helperText={getHelperText()}
                    defaultValue={defaultValue}
                    variant="outlined"
                    className={`form-field ${className}`}
                    fullWidth
                    onChange={handleChange}
                    {...rest}
                />
            </Grid>
        </>
    );
}
