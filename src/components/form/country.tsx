/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { User } from "@/objects/User"
import { Country } from "@/objects/Country"
import {useEffect, useState} from "react"
import { countryToFlag } from "@/utils/country-helpers"
import Autocomplete from "@material-ui/lab/Autocomplete"
import { TextField } from "@material-ui/core"
import api from "@/utils/api";

interface IProps {
    user?: User
    id?: string
    onChange?: (event: any, values?: any) => void
    disabled?: boolean
}

/**
 * Country form field.
 *
 * @param props
 * @constructor
 */
const CountryField = (props: IProps) => {
    const { user, onChange, ...params } = props
    const [countries, setCountries] = useState<Country[]>([])

    useEffect(() => {
        api.getCountries().then(res => {
            setCountries(res)
        })
    }, [])

    return (
        <Autocomplete
            {...params}
            value={user?.country}
            onChange={onChange}
            options={countries}
            getOptionLabel={(option) => option.name || ""}
            getOptionSelected={(option, value) => value && option.code === value.code}
            renderOption={(option) => (
                <>
                    <span>{countryToFlag(option.code)}</span>
                    {option.name} ({option.code})
                </>
            )}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label="Country"
                    variant="outlined"
                    inputProps={{
                        ...params.inputProps,
                        autoComplete: "new-password", // disable autocomplete and autofill
                    }}
                />
            )}
        />
    )
}

export default CountryField
