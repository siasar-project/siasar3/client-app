/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import {ArcElement, Chart as ChartJS, Legend, Tooltip} from 'chart.js';
import {Doughnut} from 'react-chartjs-2';
import t from "@/utils/i18n";
import ChartDataLabels from "chartjs-plugin-datalabels";

ChartJS.register(ArcElement, Tooltip, Legend);

interface Props {
    stats: any
    isLoading: boolean
}

/**
 * Page footer component.
 *
 * @param props {Props}
 *
 * @returns {any}
 *   Footer tags.
 */
export default function AllInquiriesStats(props: Props) {
    const data = {
        labels: [
            t('Draft'),
            t('Finished'),
            t('Validated'),
            t('Locked'),
            t('Removed'),
        ],
        datasets: [
            {
                data: [
                    props.stats.draft,
                    props.stats.finished,
                    props.stats.validated,
                    props.stats.locked,
                    props.stats.removed,
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(255, 159, 64, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(153, 102, 255, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 153, 204, 0.5)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 153, 204, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };

    if (props.isLoading) {
        return (<></>);
    }

    let options = {
        plugins: {
            title: {
                display: true,
                text: props.stats.label,
                font: {
                    size: 18
                }
            },
            datalabels: {
                color: '#000',
                /**
                 * Format segment label.
                 *
                 * @param value
                 *
                 * @returns {string|number}
                 */
                formatter: (value:any) => {
                    if (value === 0) {
                        return "";
                    }
                    return value;
                }
            },
        },
    };

    return (
        <div className={"all-inquiries-stats"}>
            <Doughnut plugins={[ChartDataLabels]} options={options} data={data} />
        </div>
    );
}
