/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {useState} from "react";
import {SystemStatsInterface} from "@/objects/SystemStats";
import {BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title, Tooltip,} from 'chart.js';
import {Bar} from 'react-chartjs-2';
import t from "@/utils/i18n";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

interface Props {
    stats: SystemStatsInterface
    isLoading: boolean
}

/**
 * Page footer component.
 *
 * @param props {Props}
 *
 * @returns {any}
 *   Footer tags.
 */
export default function RecordsByUserStats(props: Props) {
    const [selectedUser, setSelectedUser] = useState<string>("");
    let chartRecordsByTypeData: any;
    const chartRecordsByTypeOptions = {
        plugins: {
            title: {
                display: true,
                text: t('Records by user "@user"', {"@user": selectedUser}),
            },
        },
        responsive: true,
        indexAxis: 'y' as const,
        scales: {
            x: {
                stacked: true,
            },
            y: {
                stacked: true,
            },
        },
    };
    let chartRecordsByTypeLabels: string[] = [];

    /**
     * Get data property about a form.
     *
     * @param label Form label
     * @param type  Data property to get
     *
     * @returns {number}
     */
    const getDataByUserAndLabel = (label: string, type: string) => {
        if (props.stats.by_user && props.stats.by_user[selectedUser]) {
            for (const byUserElementKey in props.stats.by_user[selectedUser].stats) {
                let forms:any = props.stats.by_user[selectedUser];
                for (const formsKey in forms.stats) {
                    if (label === forms.stats[formsKey].label) {
                        return forms.stats[formsKey][type];
                    }
                }
            }
        }

        return 0;
    }

    let first:boolean = true;
    for (const byUserKey in props.stats.by_user) {
        let user = props.stats.by_user[byUserKey];
        // Get labels.
        if (first) {
            first = false;
            for (const formKey in user.stats) {
                chartRecordsByTypeLabels.push(user.stats[formKey].label);
            }
        }
    }

    chartRecordsByTypeData = {
        labels: chartRecordsByTypeLabels,
        datasets: [
            {
                label: t('Removed'),
                data: chartRecordsByTypeLabels.map((label) => {return getDataByUserAndLabel(label, "removed");}),
                backgroundColor: 'rgb(153, 102, 255)',
            },
            {
                label: t('Locked'),
                data: chartRecordsByTypeLabels.map((label) => {return getDataByUserAndLabel(label, "locked");}),
                backgroundColor: 'rgb(53, 162, 235)',
            },
            {
                label: t('Validated'),
                data: chartRecordsByTypeLabels.map((label) => {return getDataByUserAndLabel(label, "validated");}),
                backgroundColor: 'rgb(255, 159, 64)',
            },
            {
                label: t('Finished'),
                data: chartRecordsByTypeLabels.map((label) => {return getDataByUserAndLabel(label, "finished");}),
                backgroundColor: 'rgb(75, 192, 192)',
            },
            {
                label: t('Draft'),
                data: chartRecordsByTypeLabels.map((label) => {return getDataByUserAndLabel(label, "draft");}),
                backgroundColor: 'rgb(255, 99, 132)',
            },
        ],
    };

    /**
     * User changed.
     *
     * @param event
     */
    const handleChangeUser = (event: any) => {
        setSelectedUser(event.target.value);
    }

    if (props.isLoading || !props.stats.by_user) {
        return (<></>);
    }

    let userOptions:any = [];
    for (const byUserKey in props.stats.by_user) {
        userOptions.push(<option key={"user-"+byUserKey} value={byUserKey}>{byUserKey}</option>);
    }
    return (
        <div className={"records-by-user-stats"}>
            <label htmlFor={"users_list"}>{t("Stats by user (Digitizer or validator users only)")}</label>
            <select key={"users_list"} id={"users_list"} onChange={handleChangeUser}>
                <option key={"user-none"} value={""}>{t("Select one")}</option>
                {userOptions}
            </select>
            {"" !== selectedUser ? (
                <Bar options={chartRecordsByTypeOptions} data={chartRecordsByTypeData} />
            ) : (<></>)}
        </div>
    );
}
