/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import {SystemStatsInterface} from "@/objects/SystemStats";
import {BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title, Tooltip,} from 'chart.js';
import {Bar} from 'react-chartjs-2';
import t from "@/utils/i18n";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

interface Props {
    stats: SystemStatsInterface
    isLoading: boolean
}

/**
 * Page footer component.
 *
 * @param props {Props}
 *
 * @returns {any}
 *   Footer tags.
 */
export default function RecordsByTypeAndUserStats(props: Props) {
    let chartRecordsByTypeData: any;
    const chartRecordsByTypeOptions = {
        plugins: {
            title: {
                display: true,
                text: t('Records by type (Digitizer or validator users only)'),
            },
        },
        responsive: true,
        indexAxis: 'y' as const,
        scales: {
            x: {
                stacked: true,
            },
            y: {
                stacked: true,
            },
        },
    };
    let chartRecordsByTypeLabels: string[] = [];

    let first:boolean = true;
    let accumStates:any = {};
    for (const byUserKey in props.stats.by_user) {
        let user = props.stats.by_user[byUserKey];
        // Get labels.
        if (first) {
            first = false;
            for (const formKey in user.stats) {
                chartRecordsByTypeLabels.push(user.stats[formKey].label);
                // Init total accumulators.
                accumStates[user.stats[formKey].label] = {draft: 0, finished: 0, other: 0, total: 0};
            }
        }
        // Accumulate record status.
        for (const formKey in user.stats) {
            accumStates[user.stats[formKey].label].draft += user.stats[formKey].draft;
            accumStates[user.stats[formKey].label].finished += user.stats[formKey].finished;
            accumStates[user.stats[formKey].label].other += user.stats[formKey].other;
            accumStates[user.stats[formKey].label].total += user.stats[formKey].total;
        }
    }
    chartRecordsByTypeData = {
        labels: chartRecordsByTypeLabels,
        datasets: [
            {
                label: t('Other'),
                data: chartRecordsByTypeLabels.map((label) => accumStates[label].other),
                backgroundColor: 'rgb(53, 162, 235)',
            },
            {
                label: t('Finished'),
                data: chartRecordsByTypeLabels.map((label) => accumStates[label].finished),
                backgroundColor: 'rgb(75, 192, 192)',
            },
            {
                label: t('Draft'),
                data: chartRecordsByTypeLabels.map((label) => accumStates[label].draft),
                backgroundColor: 'rgb(255, 99, 132)',
            },
        ],
    };

    if (props.isLoading) {
        return (<></>);
    }

    return (
        <div className={"records-by-type-and-user-stats"}>
            <Bar options={chartRecordsByTypeOptions} data={chartRecordsByTypeData} />
        </div>
    );
}
