/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, { useEffect, useRef, useState } from "react";
import t from "@/utils/i18n";
import { DynFormRecordInterface } from "@/objects/DynForm/DynFormRecordInterface";
import { eventManager, Events, StatusEventLevel } from "@/utils/eventManager";

// ------------------------------ CONSTANTS AND INTERFACES ------------------------------

const AUTOSAVE_CONFIG_KEY = "dynform_autosave_config";
const delayThreshold = { min: 5, max: 30 };

type AutosaveControllerProps = {
    saveDynformPromise: (record: DynFormRecordInterface) => Promise<any>;
    getDynformRecord: () => DynFormRecordInterface;
};

interface IAutosaveConfig {
    enabled: boolean;
    delay: number;
}

const defaultAutosaveConfig: IAutosaveConfig = {
    enabled: false,
    delay: 5,
};

// ------------------------------ HELPER FUNCTIONS ------------------------------

/**
 * returns current AutosaveConfig from localStorage,
 * or creates it and returns defaultAutosaveConfig if none exist yet.
 *
 * @returns {IAutosaveConfig}
 */
const _readConfigFromLocalStorage = () => {
    let savedValue = localStorage.getItem(AUTOSAVE_CONFIG_KEY);
    if (savedValue) {
        return JSON.parse(savedValue) as IAutosaveConfig;
    }

    // if theres no AutosaveConfig in localStorage, create a default one
    localStorage.setItem(AUTOSAVE_CONFIG_KEY, JSON.stringify(defaultAutosaveConfig));
    return defaultAutosaveConfig;
};

/**
 * updates AutosaveConfig in localStorage and returns updated version
 *
 * @param propsToUpdate
 *
 * @returns {IAutosaveConfig}
 */
const _updateAutosaveConfig = (propsToUpdate: Partial<IAutosaveConfig>) => {
    let currentConfig = { ..._readConfigFromLocalStorage(), ...propsToUpdate };
    localStorage.setItem(AUTOSAVE_CONFIG_KEY, JSON.stringify(currentConfig));
    return currentConfig;
};

// ------------------------------ COMPONENT ------------------------------

/**
 * renders the controls for manipulating dynform autosaves
 *
 * @param props
 *
 * @returns {JSX.Element}
 */
const AutosaveController = (props: AutosaveControllerProps) => {
    const autosaveTimer = useRef<NodeJS.Timeout | null>(null);
    
    const [enabled, setEnabled] = useState(defaultAutosaveConfig.enabled);
    const [delayRange, setDelayRange] = useState(defaultAutosaveConfig.delay);
    const [delayInput, setDelayInput] = useState(defaultAutosaveConfig.delay.toString());

    /**
     * on mount, read AutosaveConfig from localStorage, update state vars and start timer
     */
    useEffect(() => {
        let AutosaveConfig = _readConfigFromLocalStorage();

        setEnabled(AutosaveConfig.enabled);
        setDelayRange(AutosaveConfig.delay);
        setDelayInput(AutosaveConfig.delay.toString());

        // on unmount stop any timers
        return clearTimer;
    }, []);

    /**
     * when enabled or delayRange changes,
     * update localStorage and start a new timer.
     * (delayRange only updates when it's a valid value)
     */
    useEffect(() => {
        _updateAutosaveConfig({ enabled, delay: delayRange });
        updateAutosaveTimer();
    }, [enabled, delayRange]);

    /**
     * clear any leftover timers
     */
    const clearTimer = () => {
        if (autosaveTimer.current) {
            clearTimeout(autosaveTimer.current);
        }
    };

    /**
     * start a new timer to check if autosave was enabled elsewhere, 5 mins by default
     */
    const startCheckerTimer = () => {
        let timer = setTimeout(updateComponentValuesWithLocalStorage, defaultAutosaveConfig.delay * 60 * 1000);
        autosaveTimer.current = timer;
    };

    /**
     * clear previous interval if any, and start a new timer if needed
     */
    const updateAutosaveTimer = () => {
        clearTimer();

        // if autosave is not enabled, exit function
        if (!enabled) {
            startCheckerTimer();
            return;
        }

        // otherwise, start a new autosave timer
        let timer = setTimeout(() => {
            props.saveDynformPromise(props.getDynformRecord())
                .then(() => {
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.SUCCESS,
                            title: t('Form saved'),
                            message: t(`changes were saved automatically`),
                            isPublic: true
                        }
                    );
                })
                .catch((error) => {
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.ERROR,
                            title: t('Form could not be saved'),
                            message: t(`something went wrong: ${error}`),
                            isPublic: true
                        }
                    );
                })
                .finally(() => {
                    // once the promises finished, update controls with localStorage values
                    updateComponentValuesWithLocalStorage();
                });
        }, delayRange * 60 * 1000);

        autosaveTimer.current = timer;
    };

    /**
     * reads localStorage and updates state vars accordingly
     */
    const updateComponentValuesWithLocalStorage = () => {
        let AutosaveConfig = _readConfigFromLocalStorage();

        // if nothing changed, restart timers
        if (enabled === AutosaveConfig.enabled && delayRange === AutosaveConfig.delay) {
            updateAutosaveTimer();
            return;
        }

        // else update state vars (causes re-render)
        setEnabled(AutosaveConfig.enabled);
        setDelayRange(AutosaveConfig.delay);
        setDelayInput(AutosaveConfig.delay.toString());
    };

    /**
     * update delay value if posible, both for the number input and the range input
     *
     * @param event
     *
     * @returns {void}
     */
    const updateDelay = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        let value = parseInt(event.target.value);

        // update delayInput, and if valid, delayRange aswell
        setDelayInput(value.toString());
        if (value <= delayThreshold.max && value >= delayThreshold.min) {
            setDelayRange(value);
        }
    };

    return (
        <div className="autosave-container">
            <div>
                <input
                    type="checkbox"
                    className="autosave-checkbox"
                    name="autosave_checkbox"
                    id="autosave_checkbox"
                    checked={enabled}
                    onChange={() => setEnabled(!enabled)}
                />
                <span className="autosave-check-title">{t("Enable autosave ?")}</span>
            </div>

            <input
                type="range"
                className="autosave-delay-range"
                name="autosave_delay_range"
                id="autosave_delay_range"
                min={delayThreshold.min}
                max={delayThreshold.max}
                step={1}
                value={delayRange}
                onChange={updateDelay}
                disabled={!enabled}
            />

            <div>
                {t("save every")}
                <input
                    type="number"
                    className="autosave-delay-input"
                    name="autosave_delay_input"
                    id="autosave_delay_input"
                    min={delayThreshold.min}
                    max={delayThreshold.max}
                    value={delayInput}
                    onChange={updateDelay}
                    disabled={!enabled}
                    // onBlur works as onFocusOut, this resets to last valid value saved
                    onBlur={() => setDelayInput(delayRange.toString())}
                />
                mins.
            </div>
        </div>
    );
};

export default AutosaveController;
