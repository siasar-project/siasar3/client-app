/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {dmGetTechnicalAssistanceProviders} from "@/utils/dataManager/ParametricRead";

/**
 * Component selected status.
 */
interface PropsInterface {
    id: string
}

/**
 * Point status breadcrumb.
 */
export default class TapTitle extends React.Component<PropsInterface> {
    private label:string = '';

    /**
     * Load data before render the field.
     */
    componentDidMount() {
        dmGetTechnicalAssistanceProviders().then((data) => {
            data.filter((item: any) => {
                if (this.props.id === item.id) {
                    this.label = item.name;
                    this.forceUpdate();
                }
            });

            return true;
        });
    }

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        return (
            <h2>{this.label}</h2>
        );
    }
}
