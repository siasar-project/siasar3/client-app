/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ReactElement} from "react";
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import Loading from "../common/Loading";
import {DynFormStructureMetaFieldgroupInterface} from "@/objects/DynForm/DynFormStructureMetaFieldgroupInterface";
import {Accordion} from "react-accessible-accordion";
import t from "@/utils/i18n";
import {DynFormFieldInstanceInterface} from "@/objects/DynForm/DynFormFieldInstanceInterface";
import Field_group from "@/components/DynForm/FieldTypes/field_group";
import AutosaveController from "./AutosaveController";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {DateTime} from "luxon";
import TapTitle from "@/components/DynForm/TapTitle";
import {uniqid} from "locutus/php/misc";
import {smHasPermission} from "@/utils/sessionManager";

/**
 * Form attributes
 */
interface IFormProps {
    structure: DynFormStructureInterface
    /**
     * Is visible a button?
     *
     * @param record {any}
     * @param button {ReactElement}
     * @returns {boolean}
     */
    buttonsVisibility?: Function
    buttons?: Array<ReactElement>
    onSubmit: any
    onChange?: any
    value?: any
    isLoading: boolean
    // If defined and TRUE this form must draw parent groups with accordions.
    withAccordion?: boolean
    checkParentState?: Function
    className?: string
    // if defined, enables autosave
    autosave?: (record: DynFormRecordInterface) => Promise<any>
}

interface IState {
    [key: string]: any
}

/**
 * Dynamic form component.
 *
 * The form will be built from meta.field_groups definitions.
 * The first field group level will be collapsable elements, the next levels
 * will be fieldSets.
 *
 * Field groups must contain any field visible on the property children.
 * The fields will be defined in fields property, and the children are only
 * a reference to that fields.
 */
export class DynForm extends React.Component<IFormProps, IState> {
    private readonly structure: DynFormStructureInterface;
    private fieldInstances: {[key: string]: DynFormFieldInstanceInterface};
    private withAccordion: boolean;
    private timer: NodeJS.Timeout | undefined = undefined;
    // Allow limit the total of error messages to display.
    private cntFieldNotFoundError:number = 0;
    // Form internal ID.
    private uniqueId = DateTime.now().toSeconds();
    private visibilityIgnored = false;

    /**
     * Form component constructor.
     *
     * @param props
     */
    constructor(props: IFormProps) {
        super(props);
        this.structure = props.structure;
        this.fieldInstances = {};

        this.state = this.props.value ?? {};
        // Draw accordions by default.
        this.withAccordion = this.props.withAccordion ?? true;
        // Bind "this" value to handleChange.
        this.handleChange = this.handleChange.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.calculateVisibility = this.calculateVisibility.bind(this);
        this.updateVisibility = this.updateVisibility.bind(this);
        this.applyVisibility = this.applyVisibility.bind(this);
        this.processVisibility = this.processVisibility.bind(this);
        this.checkGroupVisibility = this.checkGroupVisibility.bind(this);
        this.clearTimer = this.clearTimer.bind(this);
        this.getCurrentState = this.getCurrentState.bind(this);
    }

    /**
     * Compute visibility after render.
     */
    componentDidMount(){
        this.updateVisibility();
    }

    /**
     * Compute visibility after update.
     */
    componentDidUpdate(): void {
        this.updateVisibility();
    }

    /**
     * Start timer.
     */
    updateVisibility() {
        this.clearTimer();
        this.timer = setTimeout(this.applyVisibility, 100);
    }

    /**
     * clears dynform visibility timer if needed
     */
     clearTimer() {
        if(this.timer) {
            clearTimeout(this.timer);
        }
    }

    /**
     * Apply visibility field conditions.
     */
    applyVisibility() {
        if (!this.fieldInstances) {
            return;
        }
        let instances = {...this.fieldInstances};
        let unmountedFields: any[] = [];
        // form formLevel defaults to 1.
        const formLevel = this.structure.formLevel || 1;
        
        // check if every field in this.structure.fields loaded
        for (let fieldKey in this.structure.fields) {
            const field = this.structure.fields[fieldKey];

            // skip internal fields, country specific fields and fields with higher sgd levels than formLevel
            if (
                field.internal ||
                field.type === "mutateble" ||
                (field.settings.meta.level && formLevel && field.settings.meta.level > formLevel)
            ) {
                continue;
            }

            // save id for currently unmounted fields
            if (!instances[fieldKey]) {
                unmountedFields.push([field.id, field.type]);
            }
        }
        
        // update fields visibility
        for (let fieldInstancesKey in instances) {
            let field:DynFormFieldInstanceInterface = instances[fieldInstancesKey];
            if (field.type === "field") {
                field.setVisible(this.processVisibility(field));
            }
        }
        
        // update groups visibility (based on fields visibility)
        for (let groupInstanceKey in instances) {
            let group: DynFormFieldInstanceInterface = instances[groupInstanceKey];
            if (group.type === "group") {
                group.setVisible(this.checkGroupVisibility(group));
            }
        }
     
        // if any field is still unmounted, restart timer.
        if (unmountedFields.length > 0) {
            // console.log('unmountedFields', unmountedFields);
            this.updateVisibility();
        }
        // all fields mounted properly, stop timer until handleChange is triggered
        else {
            this.clearTimer();
        }
    }

    /**
     * hides groups with no visible children
     * 
     * @param group {DynFormFieldInstanceInterface}
     * 
     * @returns {boolean}
     */
    checkGroupVisibility(group: DynFormFieldInstanceInterface) {
        let groupProps = group.instance.props;
        let visibleChildren = 0;
        
        for (let childKey in groupProps.definition.children) {
            let childId = groupProps.definition.children[childKey].field_id;
            let child = groupProps.formFields[childId];

            // if child is visible or hasn't mounted yet (!child), increment counter
            if (!child || child.visible) {
                visibleChildren ++;
            }
        }

        // if every child is hidden, return false. else check group conditional
        return (visibleChildren > 0) && this.processVisibility(group);
    }

    /**
     * Handle component state.
     *
     * Important: This must react to changes in any own or subform field.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        // // event.preventDefault();
        // if (!event.target || event.target.dataset.iid != this.uniqueId) {
        //     // A field changed.
        //     if (this.props.onChange) {
        //         this.props.onChange(event);
        //     }
        //     return;
        // }
        // if (event.nativeEvent && event.nativeEvent.submitter && event.nativeEvent.submitter.form.dataset.id !== this.structure.id) {
        //     // Changes from other forms must be ignored.
        //     return;
        // }

        // Handle data changes.
        // console.log("DynForm change");
        this.updateVisibility();

        // update fields that need external data
        for (const key in this.fieldInstances) {
            if (Object.prototype.hasOwnProperty.call(this.fieldInstances, key)) {
                const instance = this.fieldInstances[key];
                if (instance.onFormChange) {
                    instance.onFormChange();
                }
            }
        }

        // Throw on change callback.
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    /**
     * Process field or group conditionals to set visibility.
     *
     * @param field {DynFormFieldInstanceInterface}
     * @returns {boolean}
     */
    processVisibility(field: DynFormFieldInstanceInterface) {
        let visible = true;
        if (!this.visibilityIgnored && field.conditional) {
            if (field.conditional.visible) {
                for (const operator in field.conditional.visible) {
                    visible = this.calculateVisibility(operator, field.conditional.visible[operator], field);
                }
            }
            if (field.conditional.option) {
                let validOptions: {[key: string]: boolean} = {};
                
                // calculate each option's visibility
                for (const opt in field.conditional.option) {
                    for (const operator in field.conditional.option[opt]) {
                        validOptions[opt] = this.calculateVisibility(operator, field.conditional.option[opt][operator], field);;
                    }
                }

                // update options list
                if (field.onVisibilityUpdate) {
                    field.onVisibilityUpdate(validOptions);
                }
            }
        }

        return visible;
    }

    /**
     * Calculate a visibility conditional.
     *
     * @param operator {string} Operator or field path.
     * @param operation {any} list of conditions or literal.
     * @param field {DynFormFieldInstanceInterface}
     *
     * @returns {boolean}
     */
    calculateVisibility(operator: string, operation: any, field: DynFormFieldInstanceInterface):boolean {
        let value: boolean = true;
        // If operator is not an array or object, operator is a field.
        operator = operator.toLowerCase();
        if (operator !== "or" && operator !== "and") {
            // Get field value and use "=" operator.
            // Split field name by points.
            let path = operator.split(".");
            if (path[0] === 'parent') {
                // Find parent form and process.
                if (this.props.checkParentState) {
                    return this.props.checkParentState(operator, operation, path[1]);
                }
            } else {
                // Use field name to get the field visible value and set visibility.
                if (!this.fieldInstances[path[0]]) {
                    this.cntFieldNotFoundError++;
                    if (this.cntFieldNotFoundError < 10) {
                        // Field not found.
                        console.error("Dynform calculateVisibility field '"+path[0]+"' unknown, in ", operator);
                    }
                }
                // if parent is not visible, return FALSE.
                else if (!this.processVisibility(this.fieldInstances[path[0]])) {
                    value = false;
                }
                else {
                    if (this.fieldInstances[path[0]].multivalued) {
                        let values = this.fieldInstances[path[0]].getValue(operator);
                        // If any sub-value is the required value, then condition is TRUE.
                        value = false;
                        for (let i = 0; i < values.length; i++) {
                            let inter = values[i] == operation;
                            if (inter) {
                                value = inter;
                                break;
                            }
                        }
                    } else {
                        value = this.fieldInstances[path[0]].getValue(operator) == operation;
                    }
                }
            }
            return value;
        }

        // If operator is 'and' hold true for default, else is 'or' so hold false for default.
        value = operator === "and";
        for (let i = 0; i < operation.length; i++) {
            let item:any = operation[i];
            switch (operator) {
                case 'or':
                    for (const itemOperator in item) {
                        let resp:boolean = this.calculateVisibility(itemOperator, item[itemOperator], field);
                        value = value || resp;
                    }
                    break;
                case 'and':
                    for (const itemOperator in item) {
                        let resp = this.calculateVisibility(itemOperator, item[itemOperator], field);
                        value = value && resp;
                    }
                    break;
                default:
                    console.error("Dynform calculateVisibility operator '"+operator+"' unknown");
                    break;
            }
        }

        return value;
    }

    /**
     * Handle form content reset.
     *
     * @param event
     */
    handleReset(event: any) {
        if (this.props.onSubmit) {
            this.props.onSubmit({
                id: this.structure.id,
                button: event.nativeEvent.explicitOriginalTarget.value,
                value: this.state
            });
        }
    }

    /**
     * Form submit handler.
     *
     * @param event
     */
    handleSubmit(event: any) {
        event.preventDefault();
        if (event.target.dataset.iid != this.uniqueId) {
            return;
        }
        //: { preventDefault: () => void; }
        if (event.nativeEvent.submitter.form.dataset.id !== this.structure.id) {
            // Submits from other forms must be ignored.
            return;
        }
        if (this.props.onSubmit) {
            let data = this.getCurrentState();
            // Notify that the user want save.
            this.props.onSubmit({
                id: this.structure.id,
                button: event.nativeEvent.submitter.value,
                value: data,
                event: event,
            });
        }
    }

    /**
     * returns an object with the current state of all fields
     * 
     * @returns {any}
     */
    getCurrentState() {
        let data: any = {};
        // Clean internal fields, to not send to API.
        for (const fieldName in this.state) {
            if ("id" !== fieldName) {
                if (this.props.structure.fields[fieldName] && !this.props.structure.fields[fieldName].internal) {
                    data[fieldName] = this.state[fieldName];
                }
            }
        }
        return data;
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        if (this.props.isLoading) {
            return (<Loading key={`${this.props.structure.id}_loading`} />);
        }

        // Build form fields.
        let fields:Array<ReactElement> = [];
        let aGroups:Array<DynFormStructureMetaFieldgroupInterface> = [];
        if (this.structure.meta) {
            aGroups = JSON.parse(JSON.stringify(this.structure.meta.field_groups));
        }
        if (aGroups === undefined || aGroups.length === 0) {
            aGroups = [];
            // Add a default group if not groups defined.
            let nGrp: DynFormStructureMetaFieldgroupInterface = {
                id: "0",
                title: t("Fields"),
                children: {},
            };
            // Add not internal fields to the new group.
            let ind: number = 1;
            for (const fieldsName in this.structure.fields) {
                if (!this.structure.fields[fieldsName].internal) {
                    if (nGrp.children === undefined) {
                        nGrp.children = {};
                    }
                    let catalogId = "0."+ind;
                    nGrp.children[catalogId] = {
                        id: catalogId,
                        title: this.structure.fields[fieldsName].label,
                        field_id: fieldsName,
                    };
                    ind++;
                }
            }
            if (this.structure.meta) {
                this.structure.meta.field_groups = [];
                this.structure.meta.field_groups.push(nGrp);
            }
            aGroups.push(nGrp);
        }
        // Map object to array
        if (aGroups instanceof Object) {
            let aux:Array<DynFormStructureMetaFieldgroupInterface> = [];
            for (const aGroupsKey in aGroups) {
                aux.push(aGroups[aGroupsKey]);
            }
            aGroups = aux;
        }
        // Add internal fields to a new group at the end
        let internalGrp: DynFormStructureMetaFieldgroupInterface = {
            id: '99',
            title: t("META"),
            children: {},
        };
        let indx = 1
        for(const fieldName in this.structure.fields) {            
            if(this.structure.fields[fieldName].internal) {
                this.structure.fields[fieldName].settings.meta = {...this.structure.fields[fieldName].settings.meta, disabled: true}
                if (internalGrp.children === undefined) {
                    internalGrp.children = {};
                }
                let catalogId = internalGrp.id + "." + indx
                internalGrp.children[catalogId] = {
                    id: catalogId,
                    title: this.structure.fields[fieldName].label,
                    field_id: fieldName
                }
                indx++
            }
        }
        if (indx > 1 && !this.structure.isSubForm) {
            aGroups.push(internalGrp);
        }
        if (this.structure.isSubForm) {
            this.withAccordion = false;
        }
        fields = aGroups.map((group) => {
            if (group.field_id !== undefined) {
                let clone: any = JSON.parse(JSON.stringify(group));
                group.field_id = undefined;
                if (!group.children) {
                    group.children = {};
                }
                group.children[clone.id] = clone;
            }
            return (<Field_group
                key={group.id}
                id={group.id}
                structure={this.structure}
                definition={group}
                formData={this.props.value}
                formFields={this.fieldInstances}
                withAccordion={this.withAccordion}
                // Propagate onChange handler to use with fields not standard.
                onChange={this.handleChange.bind(this)}
            />);
        });

        // dynform visible buttons
        let buttons: Array<React.ReactElement> | undefined = [];
        if (this.props.buttons) {
            buttons = this.props.buttons.filter((item: ReactElement) => {
                if (!this.props.buttonsVisibility) {
                    return true;
                }

                return (item && this.props.buttonsVisibility!(this.state, item));
            });
        }
        // form actions
        let formActions = buttons?.length
            ? <div className="form-actions">
                {buttons}
            </div>
            : null;

            let title: any = (<></>);
            if (this.props.structure.meta && this.props.structure.meta.title_field && this.props.structure.meta.title_field != '') {
                // Get referenced field value how title.
                if ('tap_reference' === this.props.structure.fields[this.props.structure.meta.title_field || ''].type) {
                    // TODO generalize to any field type.
                    title = <TapTitle id={this.props.value[this.props.structure.meta.title_field || ''].value}/>;
                } else {
                    title = (<h2>{this.props.value[this.props.structure.meta.title_field || ''].value}</h2>);
                }
            }
        // Form ID fake field.
        let Component = null;
            if (this.props.value.id && '' !== this.props.value.id) {
                Component = React.lazy(
                    () => import('@/components/DynForm/FieldTypes/'+"ulid")
                        .catch(() => ({
                            /**
                             * If not component found, log error to console.
                             *
                             * @returns {ReactElement}
                             */
                            default: () => <>
                                {console.error('['+this.props.structure.id+'] Component "ULID" field type not found.')}
                            </>
                        }))
                );
            }

        return (
            <>
                {smHasPermission('ignore form fields visibility') ? (
                    <label className={'form-item-type-radio'}>
                        <input
                            type={'checkbox'}
                            name={'ignore-visibility'}
                            key={'ignore-visibility'}
                            id={'ignore-visibility'}
                            value={'1'}
                            defaultChecked={false}
                            onChange={(e:any) => {
                                this.visibilityIgnored = !this.visibilityIgnored;
                                this.updateVisibility();
                            }}
                            className="form-check-input"
                            disabled={this.props.isLoading}
                        />
                        <span>{t("Ignore fields visibility.")}</span>
                    </label>
                ) : (<></>)}
                {(!this.structure.isSubForm) ? (
                    <>
                        <h1>{this.structure.title}</h1>
                        {title}
                        {Component ? (
                            <React.Suspense fallback={<Loading />} key={uniqid()} >
                                <Component
                                    id={'n_s'}
                                    structure={this.props.structure}
                                    definition={{
                                        id: "n_s",
                                        type: "ulid",
                                        label: t('S/N'),
                                        description: '',
                                        indexable: false,
                                        internal: true,
                                        deprecated: false,
                                        settings: {
                                            required: false,
                                            multivalued: false,
                                            weight: 0,
                                            meta: {},
                                            sort: true,
                                            filter: true
                                        }
                                    }}
                                    formData={{"value": this.props.value.id}}
                                />
                            </React.Suspense>
                        ) : (<></>)}
                    </>
                ) : (
                    <></>
                )}
                <form
                    key={this.structure.id+"_"+this.uniqueId}
                    data-id={this.structure.id}
                    data-iid={this.uniqueId}
                    className={(this.props.className ? this.props.className : '') + " dynform " + (this.structure.id || "").replace(/\./g, "-")}
                    onSubmit={this.handleSubmit}
                    onChange={this.handleChange}
                    onReset={this.handleReset}
                >
                    {(this.withAccordion) ? (
                        <Accordion allowZeroExpanded allowMultipleExpanded preExpanded={[aGroups[0]?.id ?? 0]}>
                            {fields}
                        </Accordion>
                    ) : (
                        <>
                            {fields}
                        </>
                    )}
                    { formActions }
                </form>
                { 
                    this.props.autosave !== undefined
                        ? <AutosaveController 
                            saveDynformPromise={this.props.autosave} 
                            getDynformRecord={this.getCurrentState} 
                        /> 
                        : null 
                }
            </>
        );
    }
}
