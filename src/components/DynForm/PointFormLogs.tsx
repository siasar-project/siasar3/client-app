/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import t from "@/utils/i18n";
import {dmGetPointFormLogs} from "@/utils/dataManager";
import Pager from "@/components/common/Pager";
import Loading from "@/components/common/Loading";
import {PointFormLog} from "@/objects/PointFormLog";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

interface IPointFormLogsProps {
    pointId: string;
    readonly: boolean;
}

/**
 * Inquiry Form Logs.
 */
export default class PointFormLogs extends React.Component<IPointFormLogsProps> {
    private list: Array<PointFormLog> = [];
    private loading: boolean = true;
    private page: number = 1;
    private listedItems: number = 0;

    private isEditing = false;
    private formState = { field_message: { value: "" }, field_level: { value : "" } }

    /**
     * Pager constructor.
     *
     * @param props
     */
    constructor(props: IPointFormLogsProps) {
        super(props);

        this.getList = this.getList.bind(this);
        this.paginate = this.paginate.bind(this);
        this.renderLogs = this.renderLogs.bind(this);

        // this.handleOpenLogForm = this.handleOpenLogForm.bind(this)
        // this.handleCloseLogForm = this.handleCloseLogForm.bind(this)
        // this.handleAddInquiryFormLog = this.handleAddInquiryFormLog.bind(this)
        // this.handleButtonSubmit = this.handleButtonSubmit.bind(this)
    }

    /**
     * Load form logs before render the component.
     */
    componentDidMount() {
        this.getList();
    }

    /**
     * Load point form logs.
     */
    getList() {
        if (dmIsOfflineMode()) {
            this.list = [];
        }
        dmGetPointFormLogs(this.page, this.props.pointId).then(list => {
            this.list = list;
            this.listedItems = list.length;
            this.loading = false;
            this.forceUpdate();
        });
    }

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    paginate(state: {page: number}) {
        this.page = state.page;
        this.loading = true;
        this.forceUpdate();
        this.getList();
    }

    // /**
    //  * Show modal with new form log dynform
    //  */
    // handleOpenLogForm() {
    //     this.isEditing = true;
    //     this.forceUpdate()
    // }
    //
    // /**
    //  * Show modal with new form log dynform
    //  */
    // handleCloseLogForm() {
    //     this.isEditing = false;
    //     this.forceUpdate()
    // }
    //
    // /**
    //  * handle add new Inquiry form log
    //  */
    // handleAddInquiryFormLog() {
    //     let levelValue = parseInt(this.formState.field_level.value)
    //
    //     dmGetCountry().then((country: any) => {
    //         let newInquiryFormLog: InquiryFormLog = {
    //             country: '/api/v1/countries/' + country.code,
    //             message: this.formState.field_message.value,
    //             level: levelValue,
    //             levelName: levelValue === 300 ? 'Warning' : 'Error',
    //             formId: this.props.formId,
    //             recordId: this.props.recordId,
    //             context: [],
    //             extra: [],
    //         }
    //
    //         dmAddInquiryFormLog(newInquiryFormLog)
    //             .then(() => {
    //                 this.page = 1;
    //                 this.isEditing = false;
    //                 this.formState = { field_message: { value: "" }, field_level: { value : "" } };
    //                 this.getList();
    //             })
    //             .catch(() => {})
    //     })
    // }
    //
    // /**
    //  * Handle component state.
    //  *
    //  * @param event
    //  *
    //  * @see https://reactjs.org/docs/forms.html
    //  */
    //  handleButtonSubmit(event: any) {
    //     this.formState = event.value;
    //     this.handleAddInquiryFormLog()
    // }

    /**
     * Render the logs.
     *
     * @returns {ReactElement}
     */
    renderLogs() {
        if (this.loading) {
            return (<Loading key={'point_form_logs_loading'} />);
        }
        
        let logs = [];
        
        for (let i = 0; i < this.list.length; i++) {
            let created = new Date(this.list[i].created_at ?? '');

            logs.push(
                <tr className={'level-'+this.list[i].level} key={"row-"+i}>
                    <td className="time">{created.toLocaleDateString()} - {created.toLocaleTimeString()}</td>
                    <td className="level">{this.list[i].level_name}</td>
                    <td className="user">{this.list[i].context!.user}</td>
                    <td className="message">{this.list[i].message}</td>
                </tr>
            );
        }
        
        return (
            <table>
                <thead>
                    <tr>
                        <th>{t('Time')}</th>
                        <th>{t('Level')}</th>
                        <th>{t('User')}</th>
                        <th>{t('Message')}</th>
                    </tr>
                </thead>
                <tbody>
                    {logs}
                </tbody>
            </table>
        );
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    render() {
        if (dmIsOfflineMode()) {
            return (<></>);
        }
        return (
            <>
                <div className="point-form-logs">
                    <h2>{t('Logs')}</h2>
                    <Pager page={this.page} itemsAmount={this.listedItems} onChange={this.paginate} isLoading={this.loading}/>
                    {/*<div className="form-actions">*/}
                    {/*    { !this.props.readonly && smHasPermission('create doctrine inquiryformlog') ? (*/}
                    {/*        <button*/}
                    {/*            onClick={this.handleOpenLogForm}*/}
                    {/*            className={"btn btn-secondary float-right"}*/}
                    {/*        ><FontAwesomeIcon icon={faPlusCircle} />&nbsp;{t('Add new log')}</button>*/}
                    {/*    ) : null }*/}
                    {/*</div>*/}
                    {this.renderLogs()}
                </div>
                {/*{ !this.props.readonly && smHasPermission('create doctrine inquiryformlog') ? (*/}
                {/*    <Modal*/}
                {/*        isOpen={this.isEditing}*/}
                {/*        onRequestClose={this.handleCloseLogForm}*/}
                {/*        title={t("New form log")}*/}
                {/*        size={"small"}*/}
                {/*    >*/}
                {/*        <DynForm*/}
                {/*            key={"edit-form"}*/}
                {/*            structure={getInquiryFormLogsEditSchema()}*/}
                {/*            buttons={[<input type="submit" value={t("Save")} key="button-save" className={"btn btn-primary green"}/>]}*/}
                {/*            onSubmit={this.handleButtonSubmit}*/}
                {/*            value={this.formState}*/}
                {/*            isLoading={this.loading}*/}
                {/*            withAccordion={false}*/}
                {/*        />*/}
                {/*    </Modal>*/}
                {/*) : null }*/}
            </>
        );
    }
}
