/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import { dmGetUnits } from "@/utils/dataManager";
import Measure from "./measure";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Length component.
 */
export default class length extends Measure {

  /**
   * constructor.
   *
   * @param props
   */
  constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
    super(props);

    // Add this component to post-process management.
    let id = this.props.definition.id || "";
    if (this.props.formFields) {
      /**
       * Get current visibility usable value from Component.
       *
       * @param path {string} Completed field name path.
       * @returns {any}
       */
      this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
      /**
       * Change visibility to component.
       *
       * @param isVisible {boolean}
       */
      this.props.formFields[id].setVisible = this.setVisible.bind(this);
    }
  }

  /**
   * Load unit before render the field.
   */
  componentDidMount() {
    let unit = "";

    dmGetUnits("system.units.length").then(units => {
      this.loadingOptions = false;
      // Set all units length.
      this.unitList = units.all;
      this.unitCountryList = units.country || [];
      this.unitMain = units.main;

      // Setting initial value.
      if (!this.props.structure.readonly && !this.props.definition.settings.meta.disabled) {
        if (this.state && this.state.unit === "") {
          if (this.unitMain) {
            unit = this.unitMain.id
          } else if (this.unitCountryList.length) {
            unit = this.unitCountryList[0].id
          } else {
            unit = this.unitList[Object.keys(this.unitList)[0]].id
          }

          if (this.isMonoValued()) {
            this.props.formData["unit"] = unit;
          }
          else if (this.isMultivaluedWrapped()) {
            this.props.value["unit"] = unit;
          }
          
          this.setState({['unit']: unit});
        }
      }

      this.forceUpdate();
    });
  }
}
