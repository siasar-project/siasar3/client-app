/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import React from "react";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {dmGetFormRecord, dmSearchFormRecords} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import t from "@/utils/i18n";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

/**
 * Material reference component.
 *
 * @see https://www.digitalocean.com/community/tutorials/react-react-autocomplete
 */
export default class wsprovider_reference extends BaseField {
    // Family class setting.
    protected formType:string = "";
    protected recordPrimaryFieldName = "";
    protected recordSecondaryFieldName = "";
    protected recordTypeLabel = "";
    // Internal state.
    protected currentState: any = {};
    protected currentRecord: any;
    protected loadingValue: boolean = true;
    protected lastInput: string = "";

    /**
     * Flow field constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.formType = "form.wsprovider";
        this.recordPrimaryFieldName = "field_provider_name";
        this.recordSecondaryFieldName = "field_provider_code";
        this.recordTypeLabel = "Water service providers";
        this.currentRecord = null;

        this.currentState = {
            // The component is loading first state.
            starting: true,
            // The active selection's index
            activeSuggestion: 0,
            // The suggestions that match the user's input
            filteredSuggestions: [],
            // Searching suggestions.
            loadingSuggestions: false,
            // Whether or not the suggestion list is shown
            showSuggestions: false,
            // What the user has entered
            userInput: ""
        };
    }

    /**
     * Load unit before render the field.
     */
    componentDidMount() {
        let needLoadRecord:boolean;
        let idToLoad: string;
        if (this.isMultivaluedWrapped()) {
            needLoadRecord = this.props.value.value && this.props.value.value !== "" && this.props.value.value !== undefined;
            idToLoad = this.props.value.value;
        } else {
            needLoadRecord = this.props.formData.value && this.props.formData.value !== "" && this.props.formData.value !== undefined;
            idToLoad = this.props.formData.value;
        }
        if (needLoadRecord) {
            this.loadingValue = true;
            dmGetFormRecord({formId: this.formType, id:idToLoad}).then((record:any) => {
                this.currentRecord = record;
                this.currentState.userInput = record[this.recordPrimaryFieldName].value;
                if (this.recordSecondaryFieldName !== "") {
                    this.currentState.userInput += " / " + record[this.recordSecondaryFieldName].value;
                }
                this.currentState.starting = false;
                this.loadingValue = false;
                this.forceUpdate();
            });
        } else {
            this.loadingValue = false;
            this.currentState.userInput = "";
            this.currentState.starting = false;
            this.forceUpdate();
        }
    }

    /**
     * Value to use as default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", form: ""};
    }

    /**
     * resets value and form props to getDefaultValue()
     * 
     * @param e
     */
    clearInput(e: React.MouseEvent<HTMLButtonElement, MouseEvent>){
        e.preventDefault()
        this.currentState.userInput = ''
        let value = this.getDefaultValue()
        // Is field is multivalued wrapped instance.
        if (this.isMultivaluedWrapped()) {
            this.props.value['value'] = value.value;
            this.props.value['form'] = value.form;
        }
        // If the field is single value.
        else {
            this.props.formData['value'] = value.value;
            this.props.formData['form'] = value.form;
        }
        this.setState(value);
    }

    /**
     * Handle text changes.
     *
     * @param e
     */
    onChange(e:any) {
        // Load suggestions
        const userInput = e.currentTarget.value;

        this.currentState.loadingSuggestions = true;
        this.currentState.userInput = e.currentTarget.value;
        this.setState({'errorClass': ''});
        this.lastInput = userInput;
        dmSearchFormRecords({
            formId: this.formType || "",
            filter: {
                [this.recordPrimaryFieldName]: userInput,
            }
        }, true, 1, dmIsOfflineMode())
            .then((data: any) => {
                if (!data.filter.filter || this.lastInput !== data.filter.filter[this.recordPrimaryFieldName]) {
                    return;
                }
                data = data.data;
                let minResults = Math.min(10, data.length);
                if (dmIsOfflineMode()) {
                    minResults = data.length;
                }
                let allowSugg = [];
                for (let i = 0; i < minResults; i++) {
                    allowSugg.push({
                        id: data[i]["id"],
                        suggestion: data[i][this.recordPrimaryFieldName].value + (this.recordSecondaryFieldName !== "" ? " / " + data[i][this.recordSecondaryFieldName].value : ""),
                        ref: data[i]["field_ulid_reference"].value,
                    });
                }
                this.currentState = {
                    activeSuggestion: 0,
                    filteredSuggestions: allowSugg,
                    loadingSuggestions: false,
                    showSuggestions: true,
                    userInput: this.currentState.userInput,
                };
                this.forceUpdate();
            });
    };

    /**
     * Handle user clicks.
     *
     * @param e
     */
    onClick(e:any) {
        this.currentState = {
            activeSuggestion: 0,
            filteredSuggestions: [],
            loadingSuggestions: false,
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        };
        let value: any = {value: e.currentTarget.dataset.value, form: this.formType};
        // Is field is multivalued wrapped instance.
        if (this.isMultivaluedWrapped()) {
            this.props.value['value'] = value.value;
            this.props.value['form'] = value.form;
        }
        // If the field is single value.
        else {
            this.props.formData['value'] = value.value;
            this.props.formData['form'] = value.form;
        }
        this.setState(value);
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    };

    /**
     * Handle user key downs.
     *
     * @param e
     */
    onKeyDown(e:any) {
        const { activeSuggestion, filteredSuggestions } = this.currentState;

        // User pressed the enter key
        if (e.keyCode === 13) {
            this.currentState.activeSuggestion = 0;
            this.currentState.showSuggestions = false;
            this.currentState.userInput = filteredSuggestions[activeSuggestion];
            this.forceUpdate();
        }
        // User pressed the up arrow
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) {
                return;
            }

            this.currentState.activeSuggestion = activeSuggestion - 1;
            this.forceUpdate();
        }
        // User pressed the down arrow
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }

            this.currentState.activeSuggestion = activeSuggestion + 1;
            this.forceUpdate();
        }
    };

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        const {
            currentState: {
                starting,
                activeSuggestion,
                filteredSuggestions,
                loadingSuggestions,
                showSuggestions,
                userInput,
            }
        } = this;

        if (starting) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        let suggestionsListComponent;

        if (loadingSuggestions) {
            suggestionsListComponent = (<Loading key={`${this.props.definition.id}_loading`} />);
        } else if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                let options:Array<any> = filteredSuggestions.map((suggestion:any, index:number) => {
                    let className;

                    // Flag the active suggestion with a class
                    if (index === activeSuggestion) {
                        className = "suggestion-active";
                    }
                    return (
                        <li
                            key={"suggestion-item-"+this.props.id}
                            id={"suggestion-item-"+this.props.id}
                            className={className}
                            data-value={suggestion.id}
                            onClick={this.onClick.bind(this)}
                        >
                            {suggestion.suggestion}
                        </li>
                    );
                });
                suggestionsListComponent = (
                    <ul key={"suggestion-list-"+this.props.id} className="suggestions">
                        {options}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div key={"suggestion-empty"} className="no-suggestions">
                        <em key={"suggestion-empty-label"}>{t("@recordType not found", {"@recordType": this.recordTypeLabel})}</em>
                    </div>
                );
            }
        }

        return (
            <div className="options-wrapper">
                <div>
                    <input
                        key={"suggestion-input-"+this.props.id}
                        type="text"
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        onChange={this.onChange.bind(this)}
                        onInvalid={this.handleInvalid}
                        onKeyDown={this.onKeyDown.bind(this)}
                        value={userInput}
                        required={this.isForcedIsRequired() && this.isRequired()}
                        aria-required={this.isForcedIsRequired() && this.isRequired()}
                    />
                    {suggestionsListComponent}
                    {dmIsOfflineMode() ? (
                        <div className={"field-description error"}>
                            {t("Enter any value to open the list.")}<br/>
                            {t("In offline mode, this field doesn't filter, and pre-loaded data limit it.")}
                        </div>
                    ) : (<></>)}
                </div>
                <button className="clearInput" onClick={(e) => this.clearInput(e)}>X</button>
            </div>
        );
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        let text = t('Empty');
        if (typeof this.currentState.userInput === 'string' && this.currentState.userInput !== "") {
            text = this.currentState.userInput;
        }
        return (
            <span>{!this.loadingValue ? text : <Loading />}</span>
        );
    }
}
