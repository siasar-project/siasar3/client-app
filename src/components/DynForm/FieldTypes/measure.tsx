/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import { DynFormComponentPropsInterface } from "@/objects/DynForm/DynFormComponentPropsInterface";
import Loading from "@/components/common/Loading";
import t from "@/utils/i18n";

/**
 * Measure base component.
 */
export default class Measure extends BaseField {
  protected loadingOptions:boolean = true;
  protected unitMain: any;
  protected unitCountryList: any;
  protected unitList: any;

  /**
   * Flow field constructor.
   *
   * @param props
   */
  constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
    super(props);

    this.unitMain = undefined
    this.unitCountryList = []
    this.unitList = []

    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  /**
   * Prevent enter form submit.
   *
   * @param event
   */
  handleKeyDown(event:any) {
    if (13 === event.keyCode) {
      event.preventDefault();
    }
  }

  /**
   * Value to use how default.
   *
   * @returns {any}
   */
  getDefaultValue(): any {
    return { value: "", unit: "" }
  }

  /**
   * Get target field property.
   *
   * This is called inside the parent field from the multivalued wrapper.
   *
   * @param event The source event that requires job.
   * @returns {string}
   */
  getValueTargetProperty(event: any): string {
      let id_split = event.target.id.split(`${this.props.id}__${event.target.attributes["data-index"].value}_`)

      if (id_split.length > 1) {
        return id_split[id_split.length - 1]
      }

      return 'value'
  }

  /**
   * Get main field property.
   *
   * @returns {string}
   */
  getValueMainProperty() {
    return "value";
  }

  /**
   * Get list item by Id.
   *
   * @param id {string}
   *
   * @returns {any}
   */
  getListItemById(id:string): any {
    for (let i = 0; i < this.unitList.length; i++) {
      if (id === this.unitList[i].id) {
        return this.unitList[i];
      }
    }

    return null;
  }

  /**
   * Allow to normalize initial field value.
   *
   * @param value The complete state field value.
   * @returns {any}
   */
  formatInitValue(value: any) {
    if ("" !== value.value) {
      let num:Number = value.value;
      if (num.toFixed) {
        value.value = num.toFixed(2);
      }
    }
    return value;
  }

  /**
   * Handle component state.
   *
   * @param event
   *
   * @see https://reactjs.org/docs/forms.html
   */
  handleChange(event: any) {
    let propertyName = "value";
    let value = event.target.value

    if (event.target.id === (this.props.id + "_unit")) {
      propertyName = "unit";
    }

    // if (event.target.id === (this.props.id + "_unknown") && !event.target.checked) {
    //   value = ''
    // }

    if (this.isMonoValued()) {
      this.props.formData[propertyName] = value;
    }
    else if (this.isMultivaluedWrapped()) {
      this.props.value[propertyName] = value;
    }

    this.setState({ [propertyName]: value, 'errorClass': ''});
  }

  /**
   * Render measurement units with country default groups.
   *
   * @returns {Component}
   */
  renderUnitOptions() {
    return (
        <>
          {
            this.unitMain
                ? <optgroup label="Main">
                  {
                    this.unitMain
                        ? <option
                            key={`main_${this.unitMain.symbol}${this.unitMain.id}`}
                            value={this.unitMain.id}
                        >
                          {this.unitMain.label}

                          {('' !== this.unitMain.symbol) && (undefined !== this.unitMain.symbol) && (' (' + this.unitMain.symbol + ')')}
                        </option>
                        : null
                  }
                </optgroup>
                : null
          }
          {
            this.unitCountryList.length
                ? <optgroup label="Country">
                  {
                    Object.values(this.unitCountryList).map((item: any) => (
                        <option
                            key={`country_${item.symbol}${item.id}`}
                            value={item.id}
                        >
                          {item.label}
                          {('' !== item.symbol) && (undefined !== item.symbol) && (' (' + item.symbol + ')')}
                        </option>
                    ))
                  }
                </optgroup>
                : null
          }
          <optgroup label="All">
            {
              Object.values(this.unitList).map((item: any) => (
                  <option
                      key={`all_${item.symbol}${item.id}`}
                      value={item.id}
                  >
                    {item.label}
                    {('' !== item.symbol) && (undefined !== item.symbol) && (' (' + item.symbol + ')')}
                  </option>
              ))
            }
          </optgroup>
        </>
    );
  }

  /**
   * Render component input.
   *
   * @returns {Component}
   */
  renderInput() {
    if (this.loadingOptions) {
      return (<Loading key={`${this.props.definition.id}_loading`} />);
    }

    const { id } = this.props

    // Transform decimal settings to min/max/step values.
    let leftSide = (this.props.definition.settings.precision || 10) - (this.props.definition.settings.scale || 2);
    let rightSide = this.props.definition.settings.scale || 2;
    let maxValue: any = "";
    let minValue: any = 0;
    let minDecimal: any = "1";
    // Calculate maximum to this precision.
    for (let i = 0; i < leftSide; i++) {
      maxValue += "9";
    }
    maxValue = +maxValue;
    // Fix to the set maximum.
    if (maxValue > (this.props.definition.settings.max || 9223372036854775807)) {
      maxValue = (this.props.definition.settings.max || 9223372036854775807);
    }
    // Fix to the set minimum.
    if (minValue < (this.props.definition.settings.min || -9999)) {
      minValue = (this.props.definition.settings.min || -9999);
    }
    // Calculate step from scale.
    for (let i = 0; i < rightSide - 1; i++) {
      minDecimal = "0" + minDecimal;
    }
    // If scale defined.
    if (minDecimal !== "1") {
      // Use how minimum decimal.
      minDecimal = "0." + minDecimal;
    }

    return (
      <>
        <input
            type="number"
            key={id}
            id={id}
            data-index={this.props.definition.settings._multiIndex ?? ''}
            value={this.state.value}
            onChange={this.handleChange}
            onInvalid={this.handleInvalid}
            onKeyDown={this.handleKeyDown}
            min={minValue}
            max={maxValue}
            step={minDecimal}
            required={this.isForcedIsRequired() && this.isRequired()}
            aria-required={this.isForcedIsRequired() && this.isRequired()}
            // disabled={this.state.value === '-9999999'}
            onWheel={(e) => e.currentTarget.blur()}
        />
        <select
          id={id + "_unit"}
          key={id + "_unit"}
          data-index={this.props.definition.settings._multiIndex ?? ''}
          onChange={this.handleChange}
          className={'not-validate'}
          value={this.state.unit}
          // disabled={this.state.value === '-9999999'}
        >
          {this.renderUnitOptions()}
        </select>

        {/*

        // Unknowable field.

        {
          this.props.definition.settings.unknowable
          ? <>
              <input name={id + "_unknown"} type="checkbox" id={id + "_unknown"} value="-9999999" className={`form-check-input`} onChange={this.handleChange} />
              <label htmlFor={id + "_unknown"} className="form-check-label">
                {t('98. Unknown / Not reported')}
              </label>
          </>
          : null
        } */}
      </>
    )
  }

  /**
   * Render component content only, not editable.
   *
   * @returns {Component}
   */
  renderContentOnly() {
    if (this.loadingOptions) {
      return (<Loading key={`${this.props.definition.id}_loading`} />);
    }

    try {
      let label = t("Empty");
      if ("" !== this.state.unit) {
        label = this.unitList[this.state.unit].label;
      }
      let amount:number = Number.parseFloat(this.state.value);
      if (isNaN(amount)) {
        return (
            <span>{label}</span>
        );
      } else {
        return (
            <span>{amount.toFixed(2)} {label}</span>
        );
      }
    } catch (e) {
      console.error(this.props.definition.id, e);
      return (
          <span>ERROR</span>
      );
    }
  }
}
