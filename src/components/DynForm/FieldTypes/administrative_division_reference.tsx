/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import Loading from "@/components/common/Loading";
import { AdministrativeDivision } from "@/objects/AdministrativeDivision";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faXmark } from "@fortawesome/free-solid-svg-icons";
import {smGetSession} from "@/utils/sessionManager";
import {User} from "@/objects/User";
import explode from "locutus/php/strings/explode";
import {implode} from "locutus/php/strings";
import {dmGetUser} from "@/utils/dataManager";
import {
    dmGetAdministrativeDivision,
    dmGetAdministrativeDivisionParents, dmGetAdministrativeDivisions,
    dmGetAdministrativeDivisionTypes
} from "@/utils/dataManager/ParametricRead";

/**
 * Select community component, administrative division of last level.
 */
export default class administrative_division_reference extends BaseField {
    protected options: AdministrativeDivision[] = [];
    protected loadingOptions: boolean = true;
    protected parents: AdministrativeDivision[] = [];
    protected types: any[] = [];
    protected currentUser: User | null = null;
    protected divLimits: any = [];

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
             this.props.formFields[id].getValue = (path: string): any => {
                if (this.isMultivalued()) {
                    let resp: Array<any> = [];
                    for (let i = 0; i < this.props.formData.length; i++) {
                        resp.push(this.getListItemById(this.props.formData[i].value)?.name);
                    }
                    return resp;
                }

                return this.getListItemById(this.state.value)?.name;
            };
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        // Bind "this" value to handlers.
        this.handleRemoveOneParent = this.handleRemoveOneParent.bind(this);
        this.handleRemoveAllParents = this.handleRemoveAllParents.bind(this);
        this.filterByUserLimits = this.filterByUserLimits.bind(this);
    }

    /**
     * Get list item by Id.
     *
     * @param id {string}
     *
     * @returns {any}
     */
     getListItemById(id:string): any {
        for (let i = 0; i < this.options.length; i++) {
            if (id === this.options[i].id) {
                return this.options[i];
            }
        }
        
        return null;
    }

    /**
     * Load data before render the field.
     */
    componentDidMount() {
        // Avoid search parent and options if not be used.
        if (this.isMultivalued()) {
            return;
        }
        let session = smGetSession();
        if (session) {
            dmGetUser(session.id || '').then((user: any) => {
                this.currentUser = user;
                if (this.currentUser?.administrativeDivisions) {
                    for (let i = 0; i < this.currentUser?.administrativeDivisions.length; i++) {
                        dmGetAdministrativeDivision(this.currentUser?.administrativeDivisions[i].id)
                            .then((data: AdministrativeDivision) => {
                                if (!this.divLimits[data.level]) {
                                    this.divLimits[data.level] = [];
                                }
                                this.divLimits[data.level].push(data);
                                dmGetAdministrativeDivisionParents(this.currentUser?.administrativeDivisions[i])
                                    .then(parents => {
                                        for (let j = 0; j < parents.length; j++) {
                                            if (!this.divLimits[parents[j].level]) {
                                                this.divLimits[parents[j].level] = [];
                                            }
                                            this.divLimits[parents[j].level].push(parents[j]);
                                        }
                                    });
                            });
                    }
                }
                // If there is a administrativeDivision select.
                if (this.state && this.state.value !== '') {
                    dmGetAdministrativeDivisionTypes().then(types => {
                        this.types = types;

                        // Search the administrativeDivision to use their data.
                        dmGetAdministrativeDivision(this.state.value).then(administrativeDivision => {
                            if (!administrativeDivision) {
                                // Dispatch status event.
                                eventManager.dispatch(
                                    Events.STATUS_ADD,
                                    {
                                        level: StatusEventLevel.ERROR,
                                        title: t('Administrative division not found'),
                                        message: t('Administrative division "@id" not found.', {"@id": this.state.value}),
                                        isPublic: true,
                                    }
                                );
                            }
                            // Search the administrativeDivision parents.
                            dmGetAdministrativeDivisionParents(administrativeDivision).then(parents => {
                                this.parents = [administrativeDivision, ... parents];

                                // Search the children of the parent (their siblings).
                                dmGetAdministrativeDivisions('', '', administrativeDivision.parent).then(list => {
                                    this.options = list;
                                    this.loadingOptions = false;
                                    this.forceUpdate();
                                });
                            });
                        })
                            .catch(() => {
                                this.loadingOptions = false;
                                this.setState({'value': ''});
                            });
                    });
                }
                else {
                    dmGetAdministrativeDivisionTypes().then(types => {
                        this.types = types;

                        // Search al administrativeDivision of level 1.
                        dmGetAdministrativeDivisions('', '', null).then(list => {
                            this.options = list;
                            this.loadingOptions = false;
                            this.forceUpdate();
                        });
                    });
                }
            });
        }
    }

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: '', class: ''};
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "App\\Entity\\AdministrativeDivision";
    }

    /**
     * Handle component state.
     *
     * @param event
     */
    handleChange(event: any) {
        if (event.target.id !== (this.props.id + "_class")) {
            this.loadingOptions = true;
            this.forceUpdate();
            let id = this.formatValue(event, event.target[this.getValuePropertyName()]);

            // Search the administrativeDivision to use their data.
            dmGetAdministrativeDivision(id).then(data => {
                // Add the selected administrativeDivision to the parents list.
                this.parents.unshift(data);
                // this.parents = [data, ... this.parents];

                // If is last level, then select the administrativeDivision.
                if (data.level === this.types.length) {

                    // Is field is multivalue wrapped instance.
                    if (this.isMultivaluedWrapped()) {
                        this.props.value['value'] = id;
                        this.props.value['class'] = this.getApiClass();
                    } 
                    // If the field is single value.
                    else {
                        this.props.formData['value'] = id;
                        this.props.formData['class'] = this.getApiClass();
                    }

                    this.loadingOptions = false;
                    // This raise the render() (so no need for a forceUpdate() here).
                    this.setState({'value': id, 'errorClass': ''});
                }
                else {
                    // Search the children of the selected administrativeDivision.
                    dmGetAdministrativeDivisions('', '', '/api/v1/administrative_divisions/'+data.id).then(list => {
                        this.options = list;

                        // Is field is multivalued wrapped instance.
                        if (this.isMultivaluedWrapped()) {
                            this.props.value['value'] = '';
                            this.props.value['class'] = '';
                        } 
                        // If the field is single value.
                        else {
                            this.props.formData['value'] = '';
                            this.props.formData['class'] = '';
                        }

                        this.loadingOptions = false;
                        // This raise the render() (so no need for a forceUpdate() here).
                        this.setState({'value': '', 'errorClass': ''});
                    });
                }
            });
        }
    }

    /**
     * Return error message and class.
     *
     * @param event
     * 
     * @returns {any}
     */
    getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must select a %type.', {'%type': this.types[this.parents.length].name});
            errorClass = 'error-required';
        } else {
            message = t('The current option is not valid.');
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }
    
    /**
     * Remove last parent.
     *
     * @param event
     */
    handleRemoveOneParent(event: any) {
        // Remove the last administrativeDivision added to the parent list.
        this.parents.shift();
        this.handleRemoveParent(event);
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    /**
     * Remove all parents.
     *
     * @param event
     */
    handleRemoveAllParents(event: any) {
        // Remove all administrativeDivision in the parent list.
        this.parents = [];
        this.handleRemoveParent(event);
    }

    /**
     * Remove the parent or parents.
     *
     * @param event
     */
    handleRemoveParent(event: any) {
        // this.loadingOptions = true;
        // this.forceUpdate();
        let parentIri: string|null = '';

        if (this.parents.length === 0) {
            parentIri = null;
        } 
        else {
            parentIri = '/api/v1/administrative_divisions/'+this.parents[0].id;
        }

        // Search the children of the last parent if any, otherwise the ones whit level 1.
        dmGetAdministrativeDivisions('', '',  parentIri).then(list => {
            this.options = list;
            // this.loadingOptions = false;

            // Is field is multivalued wrapped instance.
            if (this.isMultivaluedWrapped()) {
                this.props.value['value'] = '';
                this.props.value['class'] = '';
            } 
            // If the field is single value.
            else {
                this.props.formData['value'] = '';
                this.props.formData['class'] = '';
            }

            // This raise the render() (so no need for a forceUpdate() here).
            this.setState({'value': ''});
        });
    }

    /**
     * Filter options by user limits.
     *
     * @param list {AdministrativeDivision[]}
     *
     * @returns {AdministrativeDivision[]}
     */
    filterByUserLimits(list: AdministrativeDivision[]): AdministrativeDivision[] {
        if (!this.props.definition.settings.meta.filtered) {
            // This field don't apply filter.
            return list;
        }

        return list.filter((item:any) => {
            // Filter empty item.
            if (!item) {
                return false;
            }

            // If no limit, allow item.
            if (!this.divLimits[item.level] || this.divLimits[item.level].length === 0) {
                return true;
            }

            // if (1 === item.level) {
            //     for (let i = 0; i < this.divLimits[item.level].length; i++) {
            //         if (this.divLimits[item.level][i].branch === item.branch) {
            //             return true;
            //         }
            //     }
            //
            //     return false;
            // } else {
            //
            // }

            // Search items in limits in the same branch. Allow if the item is allowed in the same branch.
            let inBranch: boolean = false;
            for (let i = 0; i < this.divLimits[item.level].length; i++) {
                let allowedBranch: any[] = explode(',', this.divLimits[item.level][i].branch);
                allowedBranch.pop();
                let itemBranch: any[] = explode(',', item.branch);
                itemBranch.pop();

                if (implode(',', allowedBranch) === implode(',', itemBranch)) {
                    inBranch = true;
                    if (this.divLimits[item.level][i].id === item.id) {
                        return true;
                    }
                }
            }

            return !inBranch;
        });
    }

    /**
     * Render Breadcrumb.
     *
     * @returns {Component}
     */
    renderBreadcrumb() {

        // Build parents breadcrumb.
        let parents = [];
        if (this.parents.length === 0) {
            parents.push(<span key={this.props.definition.id + '-empty'}>{t("Unknown")}</span>);

            return parents;
        }

        // Create a copy of parents array, and reserve it.
        for (let i = this.parents.length - 1; i >= 0; i--) {
            parents.push(
                <span
                    key={this.props.definition.id + '-' + this.parents[i].id + '-' + i}
                    className="parent">
                        {this.parents[i].name}
                    </span>
            );
            parents.push(<span
                key={this.props.definition.id + '-separator-' + this.parents[i].id + '-' + i}
                className="separator"> » </span>);
        }
        // Remove last separator
        parents.pop();

        return parents;
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.loadingOptions) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        let parentsWrapper = (<></>);
        let optionsWrapper = (<></>);

        if (this.parents.length > 0) {
            let parents = this.renderBreadcrumb();

            parents.push(
                <button
                    type="button"
                    className={"dynform-remove-one btn btn-remove-one btn-link"}
                    key={this.props.definition.id+'-remove-one'}
                    id={this.props.definition.id+'-remove-one'}
                    onClick={this.handleRemoveOneParent}
                >
                    <FontAwesomeIcon icon={faAngleLeft} />
                </button>
            );
            
            if (this.parents.length > 1) {
                parents.push(
                    <button
                        type="button"
                        className={"dynform-remove-all btn btn-remove-all btn-link"}
                        key={this.props.definition.id+'-remove-all'}
                        id={this.props.definition.id+'-remove-all'}
                        onClick={this.handleRemoveAllParents}
                    >
                        <FontAwesomeIcon icon={faXmark} />
                    </button>
                );
            }

            let textClass: string = 'error';
            if (this.parents.length === this.types.length) {
                textClass = 'ok';
            }
            parentsWrapper = (
                <div className={"parents "+textClass}>
                    {parents}
                </div>
            )

        }

        // If last level is not selected yet.
        if (this.parents.length < this.types.length) {
            // Build option list.
            let options = [];
            let filteredOptions: AdministrativeDivision[] = this.filterByUserLimits(this.options);
            for (let i = 0; i < filteredOptions.length; i++) {
                options.push(
                    <option
                        key={this.props.definition.id + '-' + filteredOptions[i].id + '-' + this.props.definition.settings._multiIndex}
                        value={filteredOptions[i].id}
                    >
                        {filteredOptions[i].name}
                    </option>
                );
            }

            // Because last level is not selected yet, render <select> as required to force the html5 validation raise if submit in this state.
            optionsWrapper = (
                <div className="options-wrapper">
                    <span
                        key={this.props.definition.id + '-type'}
                        className="type">
                        {this.types[this.parents.length].name}
                    </span>

                    <select
                        key={this.props.id}
                        id={this.props.id}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        value={this.state.value}
                        onChange={this.handleChange}
                        onInvalid={this.handleInvalid}
                        required={this.isForcedIsRequired() && this.isRequired()}
                        aria-required={this.isForcedIsRequired() && this.isRequired()}
                    >
                        <option
                            key={this.props.definition.id + '-_none-' + this.props.definition.settings._multiIndex}
                            value={''}
                        >
                            {t('Select one')}
                        </option>
                        {options}
                    </select>
                </div>
            );
        }

        return (
            <>
                <div className={"field-internal-rows"}>
                    {parentsWrapper}
                    {optionsWrapper}
                </div>
            </>
        )
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <>{this.renderBreadcrumb()}</>
        );
    }
}
