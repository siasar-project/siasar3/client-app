/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import router from "next/router";
import BaseField from "./BaseField";
import {getDestinationQueryParameter} from "@/utils/siasar";

/**
 * renders administrative division name,
 * and a couple buttons to navigate to it's parent community and point
 */
export default class household_process_reference extends BaseField {
    private administrative_division = this.props.formData.meta?.administrative_division;
    private community_record = this.props.formData.meta?.community_record;
    private point_record = this.props.formData.meta?.point_record;

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (!this.props.formData.meta) {
            return <>{t("Unknown")}</>;
        }

        return (
            <>
                <span className="administrative-division-name">{this.administrative_division?.name}</span>
                <button
                    onClick={() => {
                        let destination = getDestinationQueryParameter({value: this.point_record, prefix: "/point/"});
                        router.push(`/form.community/${this.community_record}${destination}`);
                    }}
                    className="btn btn-household-process-reference btn-community"
                >
                    <span className="icon" /> {t("Go to Community")}
                </button>
                <button
                    onClick={() => router.push(`/point/${this.point_record}`)}
                    className="btn btn-household-process-reference btn-point"
                >
                    <span className="icon" /> {t("Go to Point")}
                </button>
            </>
        );
    }
}
