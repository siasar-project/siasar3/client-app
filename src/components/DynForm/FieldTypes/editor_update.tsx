/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {getLocalizedDate, getTimezone} from "@/utils/siasar";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import t from "@/utils/i18n";
import {empty} from "locutus/php/var";
import UserTag from "@/components/common/UserTag";

/**
 * editor_update component.
 *
 * This component display two fields how one.
 */
export default class editor_update_field_type extends BaseField {

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", value1: ""};
    }

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        const {editor_field, update_field} = this.props.definition.settings;
        let editorInstance, editorFieldValue, updateInstance, updateFieldValue;
        if (this.props.formFields) {
            editorInstance = this.props.formFields[editor_field || ""].instance;
            editorFieldValue = editorInstance.getState();

            updateInstance = this.props.formFields[update_field || ""].instance;
            updateFieldValue = updateInstance.getState();
        }
        let list: any[] = [];
        let userTimezone = getTimezone();
        for (let i = 0; i < updateFieldValue.length; i++) {
            let value = updateFieldValue[i];
            // Date.
            let update: any;

            if (value.value === "" || empty(value.value)) {
                update = (<span>{t("Unknown")}</span>);
            } else {
                let dateString = getLocalizedDate(value.value, userTimezone);
                update = (<span>{dateString}&nbsp;{userTimezone}</span>);
            }

            // User.
            let editor = editorFieldValue[i];
            let editorCmp;
            if (editor.value === "") {
                editorCmp = (<><span>{t("Unknown")}</span></>);
            }
            let label = editor.meta.username;
            let gravatar = editor.meta.gravatar;

            if (gravatar !== "") {
                editorCmp = (<UserTag picture_url={gravatar} username={label}/>);
            } else {
                editorCmp = (<span>{label}</span>);
            }

            list.push(
                <li key={this.props.definition.id+'_value_'+i}>
                    {update} {t("by")} {editorCmp}
                </li>
            );
        }

        return (<ul>{list}</ul>);
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return this.renderContentOnly();
    }
}
