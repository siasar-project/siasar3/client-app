/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from 'react';
import t from '@/utils/i18n';
import BaseField from '@/components/DynForm/FieldTypes/BaseField';
import { DynFormComponentPropsInterface } from '@/objects/DynForm/DynFormComponentPropsInterface';
import FieldLabel from '@/components/DynForm/FieldLabel';
import Loading from '@/components/common/Loading';

/**
 * Used to store options values.
 */
interface optionsCollection {
  [key: string]: string;
}

/**
 * Used to manage multiselect values.
 */
interface selectedCollection {
  [key: string]: boolean;
}

/**
 * Select component.
 */
export default class radio_select extends BaseField {
  protected list: any;
  protected options: optionsCollection = {};
  protected originalOptions: optionsCollection = {};
  protected selected: selectedCollection = {};
  protected loadingOptions: boolean = true;

  /**
   * Select constructor.
   *
   * @param props
   */
  constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
    super(props);

    // Add this component to post-process management.
    let id = this.props.definition.id || '';
    if (this.props.formFields) {
      /**
       * Get current visibility usable value from Component.
       *
       * @param path {string} Completed field name path.
       * @returns {any}
       */
      this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
      /**
       * Change visibility to component.
       *
       * @param isVisible {boolean}
       */
      this.props.formFields[id].setVisible = this.setVisible.bind(this);
      /**
       * Update list of available options.
       *
       * @param isVisible {boolean}
       */
      this.props.formFields[id].onVisibilityUpdate = this.updateList.bind(this);
    }

    this.getList = this.getList.bind(this);
  }

  /**
   * Load list before render the field.
   */
  componentDidMount() {
    this.getList().then((list: any) => {
      this.list = list;
      if (list && Object.keys(list).length > 0) {
        for (const key in list) {
          this.options[this.getOptionKey(key, list[key])] = this.getOptionLabel(key, list[key]);
        }
      }

      this.originalOptions = { ...this.options };
      this.loadingOptions = false;
      this.forceUpdate();
    });
  }

  /**
   * Get list to generate the options.
   *
   * @returns {Promise}
   */
  getList() {
    return Promise.resolve(this.props.definition.settings.options);
  }

  /**
   * update list of available options
   */
  updateList(validOptions: { [key: string]: boolean }) {
    this.options = { ...this.originalOptions };

    // remove non-valid options
    Object.keys(this.options).forEach((optionKey: any) => {
      if (validOptions[optionKey] === false) {
        delete this.options[optionKey];
      }
    });

    // reset value if the prev selected option is no longer valid
    if (validOptions[this.getValue()] === false) {
      this.setValue('');
      this.setState(this.getDefaultValue());
    }

    this.forceUpdate();
  }

  /**
   * Get list item by Id.
   *
   * @param id {string}
   *
   * @returns {any}
   */
  getListItemById(id: string): any {
    if (!this.list) {
      return null;
    }

    for (let i = 0; i < this.list.length; i++) {
      if (id === this.list[i].id) {
        return this.list[i];
      }
    }

    return null;
  }

  /**
   * Get option key.
   *
   * @param key {any}
   * @param value {any}
   *
   * @returns {string}
   */
  getOptionKey(key: any, value: any) {
    return key.toString().trim();
  }

  /**
   * Get option label.
   *
   * @param key {any}
   * @param value {any}
   *
   * @returns {string}
   */
  getOptionLabel(key: any, value: any) {
    if (key.startsWith('_')) {
      return value;
    } else {
      return '(' + key.trim() + ') ' + value;
    }
  }

  /**
   * Handle component state.
   *
   * @param event
   *
   * @see https://reactjs.org/docs/forms.html
   */
  handleChange(event: any) {
    if (this.isMonoValued()) {
      super.handleChange(event);
    } else if (this.isMultivalued()) {
      // Update multivalued status.
      this.selected[event.target.value] = !this.selected[event.target.value];
      // Update form data.
      this.props.formData.length = 0;
      for (const optionsKey in this.options) {
        if (this.selected[optionsKey.trim()]) {
          let option = this.getDefaultValue();
          option.value = optionsKey.trim();
          this.props.formData.push(option);
        }
      }
      this.setState({ errorClass: '' });
    }
  }

  /**
   * Render multivalued component.
   *
   * @returns {Component}
   */
  renderMultivalued() {
    if (this.loadingOptions) {
      return <Loading key={`${this.props.definition.id}_loading`} />;
    }

    let catalogId = '';
    if (this.props.definition.settings.meta.catalog_id) {
      catalogId = this.props.definition.settings.meta.catalog_id;
    }
    let labelClassName = '';
    if (this.useLevels() || this.useSdgLevel()) {
      labelClassName = 'level' + (this.props.definition.settings.meta.level ?? 1);
      if (this.props.definition.settings.meta.sdg ?? false) {
        labelClassName = 'levelsdg';
      }
    }
    return (
      <fieldset className={'dynform-multivalued-field' + ' field-type-' + this.constructor.name}>
        <legend className={labelClassName}>
          <FieldLabel
            catalogId={catalogId}
            label={this.props.definition.label}
            isRequired={this.props.definition.settings.required}
          />
        </legend>
        {!this.props.structure.readonly && this.props.definition.description ? (
          <div className={'field-description'}>{this.props.definition.description}</div>
        ) : (
          <></>
        )}
        <div className={'form-field-select form-multivalued-list'}>
          {this.props.structure.readonly || this.props.definition.settings.meta.disabled ? (
            <>{this.renderContentOnly()}</>
          ) : (
            <>{this.renderInput()}</>
          )}
        </div>
      </fieldset>
    );
  }

  /**
   * Render component input.
   *
   * @returns {Component}
   */
  renderInput() {
    if (this.loadingOptions) {
      return <Loading key={`${this.props.definition.id}_loading`} />;
    }

    // Build option list.
    let options = [];
    let index = 1;
    for (const optionsKey in this.options) {
      let currentValue = '';
      if (this.isMultivalued()) {
        // Search the key into values.
        if (this.props.formData) {
          for (let i = 0; i < this.props.formData.length; i++) {
            if (this.props.formData[i]?.value === optionsKey.trim()) {
              currentValue = optionsKey.trim();
              this.selected[optionsKey.trim()] = true;
              break;
            }
          }
        }
      }

      let tooltipText = this.list[optionsKey]?.description;
      let optionContent = (
        <label className={'form-item-type-radio'}>
          <input
            type={this.isMultivalued() ? 'checkbox' : 'radio'}
            name={this.props.definition.id}
            key={this.props.definition.id + '-' + optionsKey + '-' + index++}
            id={this.props.definition.id + '-' + optionsKey + '-' + index++}
            value={optionsKey.trim()}
            checked={this.isMultivalued() ? currentValue === optionsKey.trim() : this.state.value === optionsKey.trim()}
            className="form-check-input"
            onChange={this.handleChange}
          />
          <span>{this.options[optionsKey]}</span>
        </label>
      );

      options.push(
        <li key={this.props.definition.id + '-' + optionsKey + '-' + index++} className={'dynform-item'}>
          {optionsKey.startsWith('_') ? (
            <h3>{this.options[optionsKey]}</h3>
          ) : tooltipText ? (
            <div className="with-tooltip">
              {optionContent}
              <span className="tooltip">{tooltipText}</span>
            </div>
          ) : (
            optionContent
          )}
        </li>
      );
    }

    return (
      <ul className={'dynform-multivalued form-type-radio'}>
        {!this.isRequired() && !this.isMultivalued() ? (
          <li key={this.props.definition.id + '-_none-' + '0'} className={'dynform-item'}>
            <label className={'form-item-type-radio'}>
              <input
                type={this.isMultivalued() ? 'checkbox' : 'radio'}
                name={this.props.definition.id}
                key={this.props.definition.id + '-_none-' + '0'}
                id={this.props.definition.id + '-_none-' + '0'}
                value={''}
                checked={this.state.value === ''}
                className="form-check-input"
                onChange={this.handleChange}
              />
              <span>{t('Unknown')}</span>
            </label>
          </li>
        ) : (
          <></>
        )}
        {options}
      </ul>
    );
  }

  /**
   * Render content.
   *
   * @returns {Component}
   */
  renderContentOnly() {
    if (this.isMultivalued()) {
      return (
        <>
          <ul>
            {this.props.formData.length === 0
              ? t('Empty')
              : this.props.formData.map((option: any) => {
                  return option?.value ? (
                    <li key={'radio_select_' + this.props.definition.id + '_opc_' + option.value}>
                      {this.options[option.value]}
                    </li>
                  ) : (
                    <></>
                  );
                })}
          </ul>
        </>
      );
    }

    return (
      <>{!this.state || this.state.value === '' ? <>{t('Empty')}</> : <span>{this.options[this.state.value]}</span>}</>
    );
  }
}
