/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * New password component.
 *
 * This field doesn't need to represent the initial value.
 */
export default class new_password extends BaseField {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let propertyName = 'new';
        let areSamePasswords = false;
        
        if (event.target.id === (this.props.id + "_new")) {
            areSamePasswords = this.state.again === value;
        } else {
            propertyName = 'again';
            areSamePasswords = this.state.new === value;
        }

        // Check if both inputs are equal, then set or clear the field value.
        if (areSamePasswords) {
            this.setValue(value);
        } else {
            this.setValue('');
        }

        this.setState({[propertyName]: value});
    }
    
    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <>{this.state.value === "" ? (
                <span>{t("Empty")}</span>
            ) : (
                <span>{this.state.value.replace(/./g, "*")}</span>
            )}
            </>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <div className="password-inputs">
                <input
                    type="password"
                    key={this.props.id + "_new"}
                    id={this.props.id +  "_new"}
                    placeholder={t("New password")}
                    data-index={this.props.definition.settings._multiIndex ?? ''}
                    value={this.state.new}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    required={this.isForcedIsRequired() && this.isRequired()}
                    aria-required={this.isForcedIsRequired() && this.isRequired()}
                    autoComplete={this.props.definition.settings.meta.autocomplete ? 'on' : 'new-password'}
                />
                <input
                    type="password"
                    key={this.props.id + '_again'}
                    id={this.props.id + '_again'}
                    placeholder={t("Enter new password again")}
                    data-index={this.props.definition.settings._multiIndex ?? ''}
                    value={this.state.again}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    required={this.isForcedIsRequired() && this.isRequired()}
                    aria-required={this.isForcedIsRequired() && this.isRequired()}
                    autoComplete={this.props.definition.settings.meta.autocomplete ? 'on' : 'new-password'}
                />
                {
                    this.state.new !== this.state.again 
                        ? <span className={"error"}>{t("Passwords are not equal...")}</span> 
                        : null
                }
            </div>
        );
    }
}
