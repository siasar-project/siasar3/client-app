/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";

/**
 * Fake Dynform field component.
 */
export default class fake extends BaseField {
    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <p className={"field-fake"}>
                Field &quot;<span className={"fake-id"}>{this.props.definition.id}</span>&quot; of type &quot;<span className={"fake-type"}>{this.props.definition.type}</span>&quot;
            </p>
        );
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return this.renderInput();
    }
}
