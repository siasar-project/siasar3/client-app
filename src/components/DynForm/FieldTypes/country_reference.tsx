/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import select_reference from "@/components/DynForm/FieldTypes/select_reference";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {dmGetCountries, dmGetFileUrl} from "@/utils/dataManager";
import t from "@/utils/i18n";
import UserTag from "@/components/common/UserTag";
import React from "react";


/**
 * Country reference component.
 */
export default class country_reference extends select_reference {

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.loadingOptions = true;
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "App\\Entity\\Country";
    }

    /**
     * Get list to generate the options.
     *
     * @returns {Promise}
     */
    getList() {
        return dmGetCountries();
    }


    /**
     * Get option key.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionKey(key: any, value: any) {
        return value.code ?? '';
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        return value.name;
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (this.state.value === "") {
            return (<><span>{t("Unknown")}</span></>);
        }

        let label = "";
        let gravatar = "";
        if (this.list) {
            for (let i = 0; i < this.list.length; i++) {
                if (this.state.value === this.list[i].id) {
                    label = this.list[i].name || t("Anonymous");
                    gravatar = dmGetFileUrl(this.list[i].flag.id || "", false);
                    break;
                }
            }
        }

        if (gravatar !== "") {
            return (<span><UserTag picture_url={gravatar} username={label}/></span>);
        }

        return (
            <span>{label}</span>
        );
    }
}
