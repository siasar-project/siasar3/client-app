/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import { AdministrativeDivision } from "@/objects/AdministrativeDivision";
import Loading from "@/components/common/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faXmark } from "@fortawesome/free-solid-svg-icons";
import {
    dmGetAdministrativeDivision,
    dmGetAdministrativeDivisionParents,
    dmGetAdministrativeDivisions,
    dmGetAdministrativeDivisionTypes
} from "@/utils/dataManager/ParametricRead";

/**
 * Select administrative division component.
 */
export default class administrative_division_parent_reference extends BaseField {
    protected options: AdministrativeDivision[] = [];
    protected loadingOptions: boolean = true;
    protected parents: AdministrativeDivision[] = [];
    protected types: any[] = []

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
             this.props.formFields[id].getValue = (path: string): any => {
                if (this.isMultivalued()) {
                    let resp: Array<any> = [];
                    for (let i = 0; i < this.props.formData.length; i++) {
                        resp.push(this.getListItemById(this.props.formData[i].value)?.name);
                    }
                    return resp;
                }

                return this.getListItemById(this.state.value)?.name;
            };
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        // Bind "this" value to handlers.
        this.handleRemoveOneParent = this.handleRemoveOneParent.bind(this);
        this.handleRemoveAllParents = this.handleRemoveAllParents.bind(this);
    }

    /**
     * Get list item by Id.
     *
     * @param id {string}
     *
     * @returns {any}
     */
     getListItemById(id:string): any {
        for (let i = 0; i < this.options.length; i++) {
            if (id === this.options[i].id) {
                return this.options[i];
            }
        }

        return null;
    }

    /**
     * Load data before render the field.
     */
    componentDidMount() {
        // Avoid search parent and options if not be used.
        if (this.isMultivalued()) {
            return;
        }

        // If there is a administrativeDivision select.
        if (this.state && this.state.value !== '') {
            dmGetAdministrativeDivisionTypes().then(types => {
                this.types = types;

                // Search the administrativeDivision to use their data.
                dmGetAdministrativeDivision(this.state.value).then(administrativeDivision => {
                    // Search the administrativeDivision parents.
                    dmGetAdministrativeDivisionParents(administrativeDivision).then(parents => {
                        this.parents = [administrativeDivision, ... parents];

                        // Search the children of the parent (their siblings).
                        dmGetAdministrativeDivisions('', '', '/api/v1/administrative_divisions/'+administrativeDivision.id).then(list => {
                            this.options = list;
                            this.loadingOptions = false;
                            this.forceUpdate();
                        });
                    });
                });
            });
        }
        else {
            dmGetAdministrativeDivisionTypes().then(types => {
                this.types = types;

                // Search al administrativeDivision of level 1.
                dmGetAdministrativeDivisions('', '', null).then(list => {
                    this.options = list;                                   
                    this.loadingOptions = false;
                    this.forceUpdate();
                });
            });
        }
    }

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: '', class: ''};
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "App\\Entity\\AdministrativeDivision";
    }

    /**
     * Handle component state.
     *
     * @param event
     */
    handleChange(event: any) {
        if (event.target.id !== (this.props.id + "_class")) {
            this.loadingOptions = true;
            this.forceUpdate();
            let id = this.formatValue(event, event.target[this.getValuePropertyName()]);

            // Search the administrativeDivision to use their data.
            dmGetAdministrativeDivision(id).then(data => {
                // Add the selected administrativeDivision to the parents list.
                this.parents.unshift(data);
                // this.parents = [data, ... this.parents];

                // If is last level, then select the administrativeDivision.
                if (data.level === this.types.length) {

                    // Is field is multivalue wrapped instance.
                    if (this.isMultivaluedWrapped()) {
                        this.props.value['value'] = id;
                        this.props.value['class'] = this.getApiClass();
                    } 
                    // If the field is single value.
                    else {
                        this.props.formData['value'] = id;
                        this.props.formData['class'] = this.getApiClass();
                    }

                    this.loadingOptions = false;
                    // This raise the render() (so no need for a forceUpdate() here).
                    this.setState({'value': id, 'errorClass': ''});
                }
                else {
                    // Search the children of the selected administrativeDivision.
                    dmGetAdministrativeDivisions('', '', '/api/v1/administrative_divisions/'+data.id).then(list => {
                        this.options = list;

                        // Is field is multivalue wrapped instance.
                        if (this.isMultivaluedWrapped()) {
                            this.props.value['value'] = data.id;
                            this.props.value['class'] = this.getApiClass();
                        }
                        // If the field is single value.
                        else {
                            this.props.formData['value'] = data.id;
                            this.props.formData['class'] = this.getApiClass();
                        }

                        this.loadingOptions = false;
                        // This raise the render() (so no need for a forceUpdate() here).
                        this.setState({'value': data.id, 'errorClass': ''});
                    });
                }
            });
        }
    }

    /**
     * Return error message and class.
     *
     * @param event
     * 
     * @returns {any}
     */
    getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must select a %type.', {'%type': this.types[this.parents.length].name});
            errorClass = 'error-required';
        } else {
            message = t('The current option is not valid.');
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }
    
    /**
     * Remove last parent.
     *
     * @param event
     */
    handleRemoveOneParent(event: any) {
        // Remove the last administrativeDivision added to the parent list.
        this.parents.shift();
        this.handleRemoveParent(event);
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    /**
     * Remove all parents.
     *
     * @param event
     */
    handleRemoveAllParents(event: any) {
        // Remove all administrativeDivision in the parent list.
        this.parents = [];
        this.handleRemoveParent(event);
    }

    /**
     * Remove the parent or parents.
     *
     * @param event
     */
    handleRemoveParent(event: any) {
        // this.loadingOptions = true;
        // this.forceUpdate();
        let parentIri: string|null = '';
        let currentId = '';

        if (this.parents.length === 0) {
            parentIri = null;
        } 
        else {
            parentIri = '/api/v1/administrative_divisions/'+this.parents[0].id;
            currentId = this.parents[0].id;
        }

        // Search the children of the last parent if any, otherwise the ones whit level 1.
        dmGetAdministrativeDivisions('', '',  parentIri).then(list => {
            this.options = list;
            // this.loadingOptions = false;

            // Is field is multivalued wrapped instance.
            if (this.isMultivaluedWrapped()) {
                this.props.value['value'] = currentId;
            } 
            // If the field is single value.
            else {
                this.props.formData['value'] = currentId;
            }

            // This raise the render() (so no need for a forceUpdate() here).
            this.setState({'value': currentId});
        });
    }

    /**
     * Render Breadcrumb.
     *
     * @returns {Component}
     */
    renderBreadcrumb() {

        // Build parents breadcrumb.
        let parents = [];
        if (this.parents.length === 0) {
            parents.push(<span key={this.props.definition.id + '-empty'}>{t("Unknown")}</span>);

            return parents;
        }

        // Create a copy of parents array, and reserve it.
        for (let i = this.parents.length - 1; i >= 0; i--) {
            parents.push(
                <span
                    key={this.props.definition.id + '-' + this.parents[i].id + '-' + i}
                    className="parent">
                        {this.parents[i].name}
                    </span>
            );
            parents.push(<span
                key={this.props.definition.id + '-separator-' + this.parents[i].id + '-' + i}
                className="separator"> » </span>);
        }
        // Remove last separator
        parents.pop();

        return parents;
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.loadingOptions) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        let parentsWrapper = (<></>);
        let optionsWrapper = (<></>);

        if (this.parents.length > 0) {
            let parents = this.renderBreadcrumb();

            parents.push(
                <button
                    type="button"
                    className={"dynform-remove-one btn btn-remove-one btn-link"}
                    key={this.props.definition.id+'-remove-one'}
                    id={this.props.definition.id+'-remove-one'}
                    onClick={this.handleRemoveOneParent}
                >
                    <FontAwesomeIcon icon={faAngleLeft} />
                </button>
            );
            
            if (this.parents.length > 1) {
                parents.push(
                    <button
                        type="button"
                        className={"dynform-remove-all btn btn-remove-all btn-link"}
                        key={this.props.definition.id+'-remove-all'}
                        id={this.props.definition.id+'-remove-all'}
                        onClick={this.handleRemoveAllParents}
                    >
                        <FontAwesomeIcon icon={faXmark} />
                    </button>
                );
            }

            parentsWrapper = (
                <div className="parents">
                    {parents}
                </div>
            )

        }

        // If last level is not selected yet.
        if (this.parents.length < this.types.length) {
            // Build option list.
            let options = [];

            for (const optionsKey in this.options) {
                options.push(
                    <option
                        key={this.props.definition.id + '-' + this.options[optionsKey].id + '-' + this.props.definition.settings._multiIndex}
                        value={this.options[optionsKey].id}
                    >
                        {this.options[optionsKey].name}
                    </option>
                );
            }

            // Because last level is not selected yet, render <select> as required to force the html5 validation raise if submit in this state.
            optionsWrapper = (
                <div className="options-wrapper">
                    <span
                        key={this.props.definition.id + '-type'}
                        className="type">
                        {this.types[this.parents.length].name}
                    </span>

                    <select
                        key={this.props.id}
                        id={this.props.id}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        value={this.state.value}
                        onChange={this.handleChange}
                        onInvalid={this.handleInvalid}
                        required={this.isForcedIsRequired() && this.isRequired()}
                        aria-required={this.isForcedIsRequired() && this.isRequired()}
                    >
                        <option
                            key={this.props.definition.id + '-_none-' + this.props.definition.settings._multiIndex}
                            value={''}
                        >
                            {t('Select one')}
                        </option>
                        {options}
                    </select>
                </div>
            );
        }

        return (
            <>
                <div className={"field-internal-rows"}>
                    {parentsWrapper}
                    {optionsWrapper}
                </div>
            </>
        )
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <>{this.renderBreadcrumb()}</>
        );
    }
}
