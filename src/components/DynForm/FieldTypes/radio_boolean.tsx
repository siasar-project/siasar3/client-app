/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import boolean_field_type from "@/components/DynForm/FieldTypes/boolean";
import t from "@/utils/i18n";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Boolean component.
 *
 * This component expose "1"/"0" values but internally use booleans.
 */
export default class radio_boolean_field_type extends boolean_field_type {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }
    }

    /**
     * Get current visibility usable value from Component.
     *
     * @param path {string} Completed field name path.
     * @returns {any}
     */
    getVisibilityValue(path: string): any {
        return super.getVisibilityValue(path);
    }

    /**
     * Get the value property name in this component.
     *
     * Used in "multivalued" wrapper.
     *
     * ex. to "select" input the property name is "value".
     * ex. to "checkbox" input the property name is "checked".
     *
     * @returns {string}
     */
    getValuePropertyName() {
        return "value";
    }

    /**
     * Allow change value type in multivalued if required.
     *
     * @param event The source event that require the format.
     * @param value
     * @returns {any}
     */
    formatValue(event: any, value: any) {
        return value;
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        super.handleChange(event);
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <>
                <ul>
                    <li>
                        <label>
                            <input
                                type={'radio'}
                                name={this.props.id}
                                key={this.props.id + "-yes"}
                                id={this.props.id + "-yes"}
                                data-index={this.props.definition.settings._multiIndex ?? undefined}
                                value={'1'}
                                defaultChecked={this.state.value === '1'}
                                className="form-check-input"
                                onChange={this.handleChange}
                            />
                            {this.props.definition.settings.show_labels ? (
                                <span>{this.props.definition.settings.true_label}</span>
                            ) : (<span>{t('Yes')}</span>)}
                        </label>
                    </li>
                    <li>
                        <label>
                            <input
                                type={'radio'}
                                name={this.props.id}
                                key={this.props.id + "-no"}
                                id={this.props.id + "-no"}
                                data-index={this.props.definition.settings._multiIndex ?? undefined}
                                value={'0'}
                                defaultChecked={this.state.value === '0'}
                                className="form-check-input"
                                onChange={this.handleChange}
                            />
                            {this.props.definition.settings.show_labels ? (
                                <span>{this.props.definition.settings.false_label}</span>
                            ) : (<span>{t('No')}</span>)}
                        </label>
                    </li>
                </ul>
            </>
        );
    }
}
