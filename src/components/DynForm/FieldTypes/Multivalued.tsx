/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import t from "@/utils/i18n";
import FieldLabel from "@/components/DynForm/FieldLabel";
import Loading from "@/components/common/Loading";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons/faPlusCircle";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";

/**
 * Component state.
 */
interface MultivaluedStateInterface {
    count: number
}

/**
 * Multivalued DynForm component.
 */
export default class Multivalued extends React.Component<DynFormComponentPropsInterface, MultivaluedStateInterface> {

    /**
     * Multivalued component.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // We must synchronize internal and external values.
        // External state: Used to update main form state.
        if (this.props.formData.length === 0) {
            // Internal state: Used to manage component state.
            this.state = { count: 0 };
        } else {
            this.state = { count: this.props.formData.length };
        }

        // Bind "this" value to handlers.
        this.handleChange = this.handleChange.bind(this);
        this.handleAddChild = this.handleAddChild.bind(this);
        this.handleRemoveChild = this.handleRemoveChild.bind(this);
        this.formatValue = this.formatValue.bind(this);
        this.getValuePropertyName = this.getValuePropertyName.bind(this);
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        // Size content array.
        if (this.props.formData.length < this.state.count) {
            for (let i = this.props.formData.length; i < this.state.count; i++) {
                this.props.formData.push({});
            }
        }
    }

    /**
     * Allow change value type in multivalued if required.
     *
     * Execute callback or return default value.
     *
     * @param event The source event that require the format.
     * @param value
     * @returns {any}
     */
    formatValue(event: any, value: any) {
        if (this.props.onFormatValue) {
            return this.props.onFormatValue(event, value);
        }

        return value;
    }

    /**
     * Get target field property.
     *
     * @param event The source event that requires job.
     * @returns {string}
     */
    getValueTargetProperty(event: any) {
        if (this.props.onGetValueTargetProperty) {
            return this.props.onGetValueTargetProperty(event);
        }

        return "value";
    }

    /**
     * Get the value property name in this component.
     *
     * ex. to select the property name is "value".
     * ex. to checkbox the property name is "checked".
     *
     * @returns {string}
     */
    getValuePropertyName() {
        if (this.props.onGetValuePropertyName) {
            return this.props.onGetValuePropertyName();
        }

        return "value";
    }

    /**
     * Value to use as default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        if (this.props.onGetDefaultValue) {
            return this.props.onGetDefaultValue();
        }
        return {};
    }

    /**
     * Get main field property.
     *
     * @returns {string}
     */
    getValueMainProperty() {
        if (this.props.onGetValueMainProperty) {
            return this.props.onGetValueMainProperty();
        }
        return "value";
    }

    /**
     * Add a new child.
     *
     * @param event {any}
     */
    handleAddChild = (event: any) => {
        event.preventDefault();
        this.props.formData.push(this.getDefaultValue());
        this.setState({count: this.state.count + 1});
    }

    /**
     * Remove a new child.
     *
     * @param event {any}
     */
    handleRemoveChild = (event: any) => {
        let index = event.currentTarget.dataset.index;
        let oldCollection = JSON.parse(JSON.stringify(this.props.formData));
        this.props.formData.length = 0;
        for (let i = 0; i < oldCollection.length; i++) {
            if (i != index) {
                // Update content data and count.
                this.props.formData.push(JSON.parse(JSON.stringify(oldCollection[i])));
            }
        }
        // Update state.
        this.setState({count: this.props.formData.length});
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    /**
     * Render a child.
     *
     * @param Component
     * @param i
     * @param value
     * @param subDefinition
     * @returns {ReactElement}
     */
    renderChild(Component: any, i:number, value: any, subDefinition: any) {
        return (
            <li key={'li-'+this.props.definition.id+'-'+i} className={"dynform-item field-"+(this.props.definition.id+'__'+i)}>
                <Component
                    key={this.props.definition.id+'-'+i}
                    id={this.props.definition.id+'__'+i}
                    index={i}
                    value={value}
                    structure={this.props.structure}
                    definition={subDefinition}
                    formData={this.state}
                    onChange={this.props.onChange}
                />
                {this.props.structure.readonly || this.props.definition.settings.meta.disabled ? null : (
                    <button
                        type="button"
                        className={"dynform-remove btn btn-link"}
                        key={this.props.definition.id+i+'-remove'}
                        id={this.props.definition.id+i+'-remove'}
                        data-index={i}
                        onClick={this.handleRemoveChild}
                    ><FontAwesomeIcon icon={faTrash} /></button>
                )}
            </li>
        );
    }

    /**
     * Process currant state to simplify rendering.
     *
     * @returns {any}
     */
    prepareRender() {
        const children = [];
        // Fake field component.
        const FakeComponent = React.lazy(
            () => import('@/components/DynForm/FieldTypes/'+'fake')
                .catch(() => ({
                    /**
                     * If not component found, log error to console.
                     *
                     * @returns {ReactElement}
                     */
                    default: () => <>{console.error('['+this.props.structure.id+'] Component "fake" field type not found.')}</>
                }))
        );
        for (let i = 0; i < this.state.count; i += 1) {
            const Component = React.lazy(
                () => import('@/components/DynForm/FieldTypes/'+this.props.definition.type)
                    .catch(() => ({
                        /**
                         * If not component found, log error to console.
                         *
                         * @returns {ReactElement}
                         */
                        default: () => <>
                            {console.error('['+this.props.structure.id+'] Component "'+this.props.definition.type+'" field type not found.')}
                            <FakeComponent
                                key={this.props.definition.id+'-'+i}
                                id={this.props.definition.id+'__'+i}
                                index={i}
                                value={value}
                                structure={this.props.structure}
                                definition={subDefinition}
                                formData={this.state}
                            />
                        </>
                    }))
            );
            let subDefinition = JSON.parse(JSON.stringify(this.props.definition));
            subDefinition.settings.multivalued = false;
            subDefinition.settings._inMultivalued = true;
            subDefinition.settings._multiIndex = i;
            let value = this.getDefaultValue();
            if (this.props.formData.length > i) {
                value = this.props.formData[i];
            }
            children.push(
                <React.Suspense fallback={<Loading />}>
                    {this.renderChild(Component, i, value, subDefinition)}
                </React.Suspense>
            );
        }

        let catalogId = "";
        if (this.props.definition.settings.meta.catalog_id) {
            catalogId = this.props.definition.settings.meta.catalog_id;
        }

        let labelClassName = "";
        if (this.props.structure.formLevel || this.props.structure.formSdg) {
            labelClassName = "level"+(this.props.definition.settings.meta.level ?? 1);
            if (this.props.definition.settings.meta.sdg ?? false) {
                labelClassName = "levelsdg";
            }
        }

        return {
            labelClassName: labelClassName,
            catalogId: catalogId,
            children: children,
        };
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        let processed = this.prepareRender();
        return (
            <fieldset className={"dynform-multivalued-field"}>
                <legend className={processed.labelClassName}>
                    <FieldLabel
                        catalogId={processed.catalogId}
                        displayId={this.props.definition.settings.meta.displayId ?? processed.catalogId !== ''}
                        label={this.props.definition.label}
                        isRequired={this.props.definition.settings.required}
                    />
                </legend>
                <div className="dynform-field">
                    {!this.props.structure.readonly ? (
                        <div className={'field-description'}>
                            {this.props.definition.description}
                        </div>
                    ) : (<></>)}
                    <div className={'form-multivalued-list'}>
                        <ul onChange={this.handleChange} className={processed.labelClassName}>
                            {this.props.structure.readonly && processed.children.length === 0 ? (      
                                <>{t("Empty")}</>
                            ) : (
                                processed.children.length 
                                    ? processed.children
                                    : null
                            )}
                        </ul>
                    </div> 
                    <div className={"dynform-add"}>
                        {this.props.structure.readonly || this.props.definition.settings.meta.disabled? (
                            <></>
                        ) : (
                            <button
                                type="button"
                                className={"btn btn-primary"}
                                key={this.props.definition.id+'-add'}
                                onClick={this.handleAddChild}
                            ><FontAwesomeIcon icon={faPlusCircle} />{t('Add')}</button>
                        )}
                    </div>
                </div>
            </fieldset>
        );
    }
}
