/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import Loading from "@/components/common/Loading";

/**
 * Used to store options values.
 */
 interface optionsCollection {
    [key: string]: string
}

/**
 * Select component.
 */
export default class select extends BaseField {
    protected list: any;
    protected options: optionsCollection = {};
    protected originalOptions: optionsCollection = {};
    protected loadingOptions: boolean = true;

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
            /**
             * Update list of available options.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].onVisibilityUpdate = this.updateList.bind(this);
        }

        this.getList = this.getList.bind(this);
    }

    /**
     * Load list before render the field.
     */
     componentDidMount() { 
        if (this.isMultivalued()) {
            return;
        }

        this.getList().then((list: any) => {
            this.list = list;
            if (list && Object.keys(list).length > 0) {
                for (const key in list) {
                    this.options[this.getOptionKey(key, list[key])] = this.getOptionLabel(key, list[key]);
                }
            }

            this.originalOptions = {...this.options};
            this.loadingOptions = false;

            // If the field es a single value or a multivalue wrapped instance, set the state and internal (in props) value.
            if (!this.props.structure.readonly && !this.props.definition.settings.meta.disabled) {
                // If is required, and value is empty, and options is not empty, set the first options value.
                // if (this.isRequired() && this.state.value === '' && Object.keys(this.options).length > 0) {
                //     let value = Object.keys(this.options)[0];
                //     // If the first option is a group, then get the next item that should be a valid option.
                //     if (value.startsWith("_")) {
                //         value = Object.keys(this.options)[1];
                //     }
                //     this.setValue(value);
                //     this.setState({'value': value.trim()});
                // }
                // else {
                    this.forceUpdate();
                // }
            } 
            // Is a readonly or disabled field.
            else {
                this.forceUpdate();
            }
        });
    }

    /**
     * Get list to generate the options.
     * 
     * @returns {Promise}
     */
    getList() {
        return Promise.resolve(this.props.definition.settings.options)
    }

    /**
     * update list of available options
     */
    updateList(validOptions: {[key: string]: boolean}) {
        this.options = {...this.originalOptions};

        // remove non-valid options
        Object.keys(this.options).forEach((optionKey: any) => {
            if (validOptions[optionKey.trim()] === false) {
                delete this.options[optionKey];
            }
        })
        // reset value if the prev selected option is no longer valid
        if (validOptions[this.getValue()] === false) {
            this.setValue("");
            this.setState(this.getDefaultValue());
        }

        this.forceUpdate();
    }

    /**
     * Get option key.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionKey(key: any, value: any) {
        return key;
    }
    
    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        if (key.startsWith("_")) {
            return value;
        }
        else {
            return '(' + key.trimEnd() + ') ' + value;
        }
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.loadingOptions) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        // Build option list.
        let options = [];
        for (const optionsKey in this.options) {
            if (optionsKey.startsWith("_")) {
                options.push(
                    <optgroup
                        key={this.props.definition.id + '-' + optionsKey + '-' + this.props.definition.settings._multiIndex}
                        label={this.options[optionsKey]}
                    />
                );
            } else {
                options.push(
                    <option
                        key={this.props.definition.id + '-' + optionsKey + '-' + this.props.definition.settings._multiIndex}
                        value={optionsKey.trimEnd()}
                    >
                        {this.options[optionsKey]}
                    </option>
                );
            }
        }

        return (
            <select
                key={this.props.id}
                id={this.props.id}
                data-index={this.props.definition.settings._multiIndex ?? ''}
                value={this.state.value}
                onChange={this.handleChange}
                onInvalid={this.handleInvalid}
                required={this.isForcedIsRequired() && this.isRequired()}
                aria-required={this.isForcedIsRequired() && this.isRequired()}
            >
                {/*!this.isRequired() ?*/
                    // Add empty option to non required fields.
                    (
                        <option
                            key={this.props.definition.id + '-_none-' + this.props.definition.settings._multiIndex}
                            value={''}
                        >
                            {t('Select one')}
                        </option>
                    )/* : (<></>)*/
                }
                {options}
            </select>
        );
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        let trimedOptionKeys: {[key: string]: string} = {};
        Object.keys(this.options).forEach(key => {
            trimedOptionKeys[key.trimEnd()] = this.options[key];
        });

        return (
            <>
                {!this.state || this.state.value === "" ? (
                    <>{t('Unknown')}</>
                ) : (
                    <span>{trimedOptionKeys[this.state.value]}</span>
                )}
            </>
        );
    }
}
