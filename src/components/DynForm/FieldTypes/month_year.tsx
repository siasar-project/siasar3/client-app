/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * month_year component.
 *
 * @see https://www.tutorialesprogramacionya.com/htmlya/html5/temarios/descripcion.php?cod=181
 */
export default class month_year extends BaseField {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {month: ""+((new Date()).getMonth() + 1), year: ""+((new Date()).getFullYear())};
    }

    /**
     * Get target field property.
     *
     * This is called inside the parent field from the multivalued wrapper.
     *
     * @param event The source event that requires job.
     * @returns {string}
     */
    getValueTargetProperty(event: any) {
        let id_split = event.target.id.split(`${this.props.id}__${event.target.attributes["data-index"].value}_`)

        return id_split[id_split.length - 1]
    }

    /**
     * Get main field property.
     *
     * @returns {string}
     */
    getValueMainProperty() {
        return "month";
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        let propertyName = "year";
        if (event.target.id !== (this.props.id + "_year")) {
            propertyName = "month";
        }

        if (this.isMonoValued()) {
            this.props.formData[propertyName] = event.target.value;
        }
        else if (this.isMultivaluedWrapped()) {
            this.props.value[propertyName] = event.target.value;
        }
        
        this.setState({[propertyName]: event.target.value, 'errorClass': ''});
    }

    /**
     * Get month name.
     *
     * @param id {string}
     *
     * @returns {string}
     */
    getMonthName(id: string) {
        switch (id) {
            case "1": return t("January");
            case "2": return t("February");
            case "3": return t("March");
            case "4": return t("April");
            case "5": return t("May");
            case "6": return t("June");
            case "7": return t("July");
            case "8": return t("August");
            case "9": return t("September");
            case "10": return t("October");
            case "11": return t("November");
            case "12": return t("December");
            default: return t("Unknown");
        }
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <span>{this.getMonthName(this.state.month)} / {this.state.year}</span>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <div className={"composed-field"}>
                <div className={"composed-field-block"}>
                    <label htmlFor={this.props.id + "_month"}>{t("Month")}</label>
                    <select
                        key={this.props.id + "_month"}
                        id={this.props.id + "_month"}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        value={this.state.month}
                        onChange={this.handleChange}
                        onInvalid={this.handleInvalid}
                        required={this.isForcedIsRequired() && this.isRequired()}
                        aria-required={this.isForcedIsRequired() && this.isRequired()}
                    >
                        {this.isRequired() ? (
                            <option value="">{t("Select one")}</option>
                        ) : (
                            <></>
                        )}
                        <option value="1">{t("January")}</option>
                        <option value="2">{t("February")}</option>
                        <option value="3">{t("March")}</option>
                        <option value="4">{t("April")}</option>
                        <option value="5">{t("May")}</option>
                        <option value="6">{t("June")}</option>
                        <option value="7">{t("July")}</option>
                        <option value="8">{t("August")}</option>
                        <option value="9">{t("September")}</option>
                        <option value="10">{t("October")}</option>
                        <option value="11">{t("November")}</option>
                        <option value="12">{t("December")}</option>
                    </select>
                </div>
                <div className={"composed-field-block"}>
                    <label htmlFor={this.props.id + "_year"}>{t("Year")}</label>
                    <input
                        type="number"
                        key={this.props.id + "_year"}
                        id={this.props.id + "_year"}
                        step="1"
                        max={9999}
                        min={1000}
                        value={this.state.year}
                        onChange={this.handleChange}
                        onInvalid={this.handleInvalid}
                        onKeyDown={this.handleKeyDown}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        required={this.isForcedIsRequired() && this.isRequired()}
                        aria-required={this.isForcedIsRequired() && this.isRequired()}
                        onWheel={(e) => e.currentTarget.blur()}
                    />
                </div>
            </div>
        );
    }
}
