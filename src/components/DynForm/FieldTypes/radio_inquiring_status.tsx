/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import t from "@/utils/i18n";
import radio_select from "@/components/DynForm/FieldTypes/radio_select";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Radio inquiring status component.
 */
export default class radio_inquiring_status extends radio_select {

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

       
    }

    /**
     * Get list to generate the options.
     *
     * @returns {Promise}
     */
    getList() {
        return Promise.resolve({
            draft: t('Draft'),
            finished: t('Finished'),
            validated: t('Validated'),
            locked: t('Locked'),
            removed: t('Removed'),
        });
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        return value;
    } 
}
