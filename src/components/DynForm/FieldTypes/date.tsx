/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import { getLocalizedDate, getRelativeDate, getTimezone } from "@/utils/siasar";
import {empty} from "locutus/php/var";

/**
 * Date component.
 *
 * @see https://www.tutorialesprogramacionya.com/htmlya/html5/temarios/descripcion.php?cod=181
 */
export default class date_field_type extends BaseField {
    protected userTimezone = getTimezone();
    protected relativeDate = this.props.formData.value || this.props.value;

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        if (this.isMonoValued()) {
            if(this.props.formData.value && this.props.formData.timezone) {
                this.relativeDate = getRelativeDate(this.props.formData.value, this.props.formData.timezone, this.userTimezone);
            }
        } else if (this.isMultivaluedWrapped()) {
            if(this.props.value.value && this.props.value.timezone) {
                this.relativeDate = getRelativeDate(this.props.value.value, this.props.value.timezone, this.userTimezone);
            }
        }

        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Return error message and class.
     *
     * @param event
     * 
     * @returns {any}
     */
    getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must have a date.');
            errorClass = 'error-required';
        } else {
            message = t('The current value is not valid, must be a date.');
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }
    
    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: '', timezone: ''};
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (this.relativeDate === "" || empty(this.relativeDate)) {
            return (<span>{t("Unknown")}</span>);
        }

        let dateString = getLocalizedDate(this.relativeDate, this.userTimezone);

        return (
            <span>
                {dateString}&nbsp;{this.userTimezone}
            </span>
        );
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        value = getRelativeDate(value, this.userTimezone, this.userTimezone)
        this.relativeDate = value;
        this.setValue(value);
        this.setState({
            [this.getValueMainProperty()]: value,
            timezone: getTimezone(),
            'errorClass': ''
        });
    }

    /**
     * Set value.
     *
     * @param value {any}
     */
    setValue(value: any) {
        if (this.isMonoValued()) {
            this.props.formData[this.getValueMainProperty()] = value;
            this.props.formData.timezone = this.userTimezone;
        }
        else if (this.isMultivaluedWrapped()) {
            this.props.value[this.getValueMainProperty()] = value;
            this.props.value.timezone = this.userTimezone;
        }
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <input
                type="date"
                key={this.props.id}
                id={this.props.id}
                data-index={this.props.definition.settings._multiIndex ?? ''}
                value={this.relativeDate ? this.relativeDate.split(' ')[0] : ""}
                onChange={this.handleChange}
                onInvalid={this.handleInvalid}
                onKeyDown={this.handleKeyDown}
                required={this.isForcedIsRequired() && this.isRequired()}
                aria-required={this.isForcedIsRequired() && this.isRequired()}
            />
        );
    }
}
