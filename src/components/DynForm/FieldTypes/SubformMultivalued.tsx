/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import FieldLabel from "@/components/DynForm/FieldLabel";
import Multivalued from "@/components/DynForm/FieldTypes/Multivalued";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons/faPlusCircle";

/**
 * Subform Multivalued DynForm component.
 */
export default class SubformMultivalued extends Multivalued {

    protected requestedOpenModal = false;

    /**
     * After remove the referenced record, remove the row.
     *
     * @param props
     */
    handleOnRemove(props:DynFormComponentPropsInterface) {
        let oldCollection = JSON.parse(JSON.stringify(this.props.formData));
        this.props.formData.length = 0;
        for (let i = 0; i < oldCollection.length; i++) {
            if (i != props.index) {
                // Update content data and count.
                this.props.formData.push(JSON.parse(JSON.stringify(oldCollection[i])));
            }
        }
        // Update state.
        this.setState({count: this.props.formData.length});
        if (this.props.onChange) {
            this.props.onChange({field: this.props.definition.id});
        }
        this.forceUpdate();
    }

    /**
     * Add a new child.
     *
     * @param event {any}
     */
    handleAddChild = (event: any) => {
        event.preventDefault();
        this.props.formData.push(this.getDefaultValue());
        this.requestedOpenModal = true;
        this.setState({count: this.state.count + 1});
    }

    /**
     * Render a child.
     *
     * @param Component
     * @param i
     * @param value
     * @param subDefinition
     * @returns {ReactElement}
     */
    renderChild(Component: any, i:number, value: any, subDefinition: any) {
        return (
            <>
                <Component
                    key={this.props.definition.id+'-'+i}
                    id={this.props.definition.id+'__'+i}
                    index={i}
                    value={value}
                    structure={this.props.structure}
                    definition={subDefinition}
                    formData={this.state}
                    onRemove={this.handleOnRemove.bind(this)}
                    onChange={this.props.onChange}
                    triggerOpenModal={this.requestedOpenModal && i === this.state.count - 1}
                    formFields={this.props.formFields}
                />
                {/*<button*/}
                {/*    type="button"*/}
                {/*    className={"dynform-remove btn btn-remove btn-link"}*/}
                {/*    key={this.props.definition.id+i+'-remove'}*/}
                {/*    id={this.props.definition.id+i+'-remove'}*/}
                {/*    data-index={i}*/}
                {/*    onClick={this.handleRemoveChild}*/}
                {/*>&nbsp;</button>*/}
            </>
        );
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        let processed = this.prepareRender();
        this.requestedOpenModal = false;
        return (
            <fieldset className={"dynform-multivalued-field"}>
                <legend className={processed.labelClassName}>
                    <FieldLabel
                        catalogId={processed.catalogId}
                        displayId={this.props.definition.settings.meta.displayId ?? processed.catalogId !== ''}
                        label={this.props.definition.label}
                        isRequired={this.props.definition.settings.required}
                    />
                </legend>
                {!this.props.structure.readonly ? (
                    <div className={'field-description'}>
                        {this.props.definition.description}
                    </div>
                ) : (<></>)}
                <div className={'form-multivalued-list'}>
                        <table>
                            <thead>
                                {this.props.onRenderHeader()}
                            </thead>
                            <tbody>
                                {processed.children}
                            </tbody>
                        </table>
                </div>
                {!this.props.structure.readonly && !this.props.definition.settings.meta.disabled ? (
                    <div className={"dynform-add"}>
                        <button
                            type="button"
                            className={"btn btn-primary"}
                            key={this.props.definition.id+'-add'}
                            onClick={this.handleAddChild}
                        ><FontAwesomeIcon icon={faPlusCircle} />{t('Add')}</button>
                    </div>
                ) : (<></>)}
            </fieldset>
        );
    }
}
