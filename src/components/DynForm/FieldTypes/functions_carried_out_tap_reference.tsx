/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import radio_reference from "@/components/DynForm/FieldTypes/radio_reference";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import t from "@/utils/i18n";
import {dmGetFunctionsCarriedOutTaps} from "@/utils/dataManager/ParametricRead";

/**
 * TAP reference component.
 */
export default class functions_carried_out_tap_reference extends radio_reference {

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = (path: string): any => {
                if (this.isMultivalued()) {
                    let resp: Array<any> = [];
                    for (let i = 0; i < this.props.formData.length; i++) {
                        resp.push(this.getListItemById(this.props.formData[i].value)?.keyIndex);
                    }
                    return resp;
                }

                return this.getListItemById(this.state.value)?.keyIndex;
            };
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.loadingOptions = true;
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "App\\Entity\\FunctionsCarriedOutTap";
    }

    /**
     * Get list to generate the options.
     *
     * @returns {Promise}
     */
    getList() {
        return dmGetFunctionsCarriedOutTaps();
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        let label = value.type;
        if (['99'].includes(value.keyIndex)) {
            label = t(label);
        }
        return '(' + value.keyIndex + ') ' + label;
    }
}
