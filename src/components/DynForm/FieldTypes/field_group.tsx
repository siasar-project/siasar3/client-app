/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ReactElement} from "react";
import {DynFormStructureMetaFieldgroupInterface} from "@/objects/DynForm/DynFormStructureMetaFieldgroupInterface";
import {DynFormStructureFieldInterface} from "@/objects/DynForm/DynFormStructureFieldInterface";
import {AccordionItem, AccordionItemButton, AccordionItemHeading, AccordionItemPanel} from "react-accessible-accordion";
import Markdown from "@/components/common/Markdown";
import FieldLabel from "@/components/DynForm/FieldLabel";
import {DynFormStructureFieldGroupInterface} from "@/objects/DynForm/DynFormStructureFieldGroupInterface";
import {
    DynFormFieldInstanceInterface,
    DynFormFieldInstanceTypes
} from "@/objects/DynForm/DynFormFieldInstanceInterface";
import Loading from "@/components/common/Loading";

/**
 * Field group component.
 */
export default class Field_group extends React.Component<DynFormStructureFieldGroupInterface> {

    private showLabel: boolean = true;
    private withAccordion: boolean = false;

    /**
     * Field group constructor.
     *
     * @param props
     */
    constructor(props: DynFormStructureFieldGroupInterface | Readonly<DynFormStructureFieldGroupInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        let instance: DynFormFieldInstanceInterface = {
            id: id,
            instance: this,
            type: DynFormFieldInstanceTypes.Group,
            conditional: this.props.definition.conditional,
            visible: true,
            multivalued: false,
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            getValue: (path:string) => { return "" },
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            setVisible: this.setVisible.bind(this),
        }
        if (this.props.formFields) {
            this.props.formFields[this.getFormVisibilityId()] = instance;
        }
        if (this.props.withAccordion) {
            this.withAccordion = this.props.withAccordion;
        }

        // Bind "this" value to handler.
        this.handleAccordionHeadingClick = this.handleAccordionHeadingClick.bind(this);
    }

    /**
     * Get the form visibility ID.
     *
     * @returns {string}
     */
    getFormVisibilityId() {
        return "field_group_"+(this.props.definition.id || "");
    }

    /**
     * Change visibility to component.
     *
     * @param isVisible {boolean}
     */
    setVisible(isVisible: boolean)
    {
        if (this.props.formFields) {
            if (this.props.formFields[this.getFormVisibilityId()].visible !== isVisible) {
                this.props.formFields[this.getFormVisibilityId()].visible = isVisible;
                this.forceUpdate();
            }
        }
    }

    /**
     * Is this field visible from props formFields?
     *
     * @returns {boolean}
     */
    isFormFieldsVisible() {
        if (this.props.formFields) {
            return this.props.formFields[this.getFormVisibilityId()].visible;
        }
        return true;
    }

    /**
     * Handle AccordionItem onClick.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
     handleAccordionHeadingClick(event: any) {
        setTimeout(() => {
            // Here the event.target could be the accordion__button, title or catalog-id (depending on where you click); but how their parent (accordion__heading) is sticky (floating) we can't use their offsetTop.
            // Instead we need to use the firts accordion__button parent that have static position (group) to get the offsetTop of AccordionItem, for this reason it use the function .closest(), to search the nearest parent with the class group
            
            window.scrollTo({
                top: event.target.closest('.group').offsetTop,
    
                behavior: "smooth",
            }); 
            
        }, 400);
        
        // update fields on group toggle
        for (const key in this.props.formFields) {
            if (Object.prototype.hasOwnProperty.call(this.props.formFields, key)) {
                const instance = this.props.formFields[key];
                if (instance.onGroupToggle) {
                    instance.onGroupToggle();
                }
            }
        }
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        if (!this.isFormFieldsVisible()) {
            return (<></>);
        }
        // Detect my current ID.
        let catalogId = "";
        if (this.props.definition.id) {
            catalogId = this.props.definition.id;
        }
        // Hidde field by form level.
        if ((this.props.structure.formLevel || 1) < (this.props.definition.level || 1)) {
            return (<></>);
        }
        // Is this an SDG field in a non-SDG form?
        if (this.props.definition.sdg) {
            if ((undefined !== this.props.structure.meta.allow_sdg && !this.props.structure.meta.allow_sdg) || !this.props.structure.formSdg) {
                return (<></>);
            }
        }
        // hide label and accordion if it has only one field_group as child
        if (this.props.structure.meta.field_groups.length === 1) {
            this.showLabel = false;
            this.withAccordion = false;
        }
        // Render group.
        let fields:Array<ReactElement> = [];
        let aFields:Array<DynFormStructureFieldInterface | DynFormStructureMetaFieldgroupInterface> = [];
        // Find fields referred in group.
        for (const fieldKey in this.props.definition.children) {
            let element: DynFormStructureMetaFieldgroupInterface = this.props.definition.children[fieldKey];
            // If element is defined.
            if (element) {
                // If element is a field.
                if (element.field_id) {
                    aFields.push(this.props.structure.fields[element.field_id]);
                    // Is a field with children?
                    if (element.children && Object.keys(element.children).length > 0) {
                        const fieldMetaInfo = this.props.structure.fields[element.field_id].settings.meta;
                        const newFieldGroup: DynFormStructureMetaFieldgroupInterface = {
                            ...element,
                            type: "field_group",
                            level: fieldMetaInfo.level,
                            sdg: fieldMetaInfo.sdg,
                            showLabel: false,
                        }
                        aFields.push(newFieldGroup);
                    }
                } else {
                    // Element is a field group.
                    element.type = "field_group";
                    aFields.push(element);
                }
            }
        }
        // Fake field component.
        const FakeComponent = React.lazy(
            () => import('@/components/DynForm/FieldTypes/'+'fake')
                .catch(() => ({
                    /**
                     * If not component found, log error to console.
                     *
                     * @returns {ReactElement}
                     */
                    default: () => <>{console.error('['+this.props.structure.id+'] Component "fake" field type not found.')}</>
                }))
        );
        // Render detected fields.
        fields = aFields.map((field) => {
            if ("deprecated" in field && field.deprecated) {
                return (<></>);
            }
            const Component = React.lazy(
                () => import('@/components/DynForm/FieldTypes/'+field.type)
                    .catch(() => ({
                        /**
                         * If not component found, log error to console.
                         *
                         * @returns {ReactElement}
                         */
                        default: () => <>
                            {console.error('['+this.props.structure.id+'] Component "'+field.type+'" field type not found.')}
                            <FakeComponent
                                key={field.id}
                                id={field.id}
                                structure={this.props.structure}
                                definition={field}
                                formData={isGroup ? this.props.formData : this.props.formData[field.id]}
                                formFields={this.props.formFields}
                            />
                        </>
                    }))
            );
            // If the field have field.settings then is a field, not a group, can have a value.
            let isGroup: boolean = ("field_group" === field.type ?? '');
            if (!isGroup) {
                if (undefined === this.props.formData[field.id]) {
                    if ("settings" in field && field.settings.multivalued) {
                        this.props.formData[field.id] = [];
                    } else {
                        // The object <code>{value: false}</code> is a mark to known that the field is not initialized.
                        this.props.formData[field.id] = {value: false};
                    }
                }
            }
            return (
                <React.Suspense 
                    fallback={<Loading />}
                    key={field.id}
                >
                    <Component
                        id={field.id}
                        structure={this.props.structure}
                        definition={field}
                        formData={isGroup ? this.props.formData : this.props.formData[field.id]}
                        formFields={this.props.formFields}
                        onChange={this.props.onChange}
                    />
                </React.Suspense>
            );
        });

        // We don't attach onClick to AccordionItemButton because it prevents another handler from executing on it.
        // But it doesn't prevent bubbling, so we can attach onClick to AccordionItemHeading.
        if (this.withAccordion) {
            return (
                <>
                    <AccordionItem key={catalogId} uuid={catalogId} className={'accordion__item group group-'+catalogId.split('.').join('-')} >
                        <AccordionItemHeading onClick={this.handleAccordionHeadingClick}>
                            <AccordionItemButton>
                                <FieldLabel
                                    catalogId={catalogId}
                                    label={this.props.definition.title}
                                    isRequired={false}
                                    displayId={this.props.structure.meta.field_groups.length >= 2}
                                />
                            </AccordionItemButton>
                        </AccordionItemHeading>
                        <AccordionItemPanel>
                            <div key={"block_" + catalogId} className="field-block-wrapper field-group-description">
                                {!this.props.structure.readonly && this.props.definition.help && "" != this.props.definition.help ? (
                                    <Markdown text={this.props.definition.help}/>
                                ) : (<></>)}
                                { this.props.definition.id === '99' && this.props.structure.meta.version !== '' ? (
                                    <div className={"version"}>{this.props.structure.meta.version}</div>
                                ): (<></>)}
                                { this.props.definition.id === '99' && this.props.structure.description !== '' && this.props.structure.isSubForm ? (
                                    <Markdown text={this.props.structure.description}/>
                                ): (<></>)}
                                {fields}
                            </div>
                        </AccordionItemPanel>
                    </AccordionItem>
                </>
            );
        }

        let labelClassName = "";
        if (this.props.structure.formLevel !== undefined || this.props.structure.formSdg) {
            labelClassName = "group-legend level"+(this.props.definition.level ?? 1);
            if (this.props.definition.sdg ?? false) {
                labelClassName = "group-legend levelsdg";
            }
        }

        // Apply definition show label setting.
        if (this.props.definition.showLabel !== undefined && !this.props.definition.showLabel) {
            this.showLabel = false;
        }

        let groupClasses = "group group-" + catalogId.split('.').join('-');
        if (!this.showLabel || this.props.structure.isSubForm) {
            groupClasses += " hidden";
        }

        return (
            <>
                <fieldset className={groupClasses}>
                    {(this.showLabel /*&& this.props.structure.isSubForm !== true*/) ? (
                        <legend className={labelClassName}>
                            <FieldLabel
                                catalogId={catalogId}
                                label={this.props.definition.title}
                                isRequired={false}
                            />
                        </legend>
                    ) : (<></>)}
                    {!this.props.structure.readonly && this.props.definition.help && "" != this.props.definition.help ? (
                        <div className={"field-group-description"}>
                            <Markdown text={this.props.definition.help}/>
                        </div>
                    ) : (<></>)}
                    { this.props.definition.id === '99' && this.props.structure.meta.version !== '' ? (
                        <div className={"version"}>{this.props.structure.meta.version}</div>
                    ): (<></>)}
                    { this.props.definition.id === '99' && this.props.structure.description !== '' && this.props.structure.isSubForm ? (
                        <Markdown text={this.props.structure.description}/>
                    ): (<></>)}
                    {fields}
                </fieldset>
            </>
        );
    }
}
