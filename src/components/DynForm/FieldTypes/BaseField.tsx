/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import Multivalued from "@/components/DynForm/FieldTypes/Multivalued";
import FieldLabel from "@/components/DynForm/FieldLabel";
import Markdown from "@/components/common/Markdown";
import {
    DynFormFieldInstanceInterface,
    DynFormFieldInstanceTypes
} from "@/objects/DynForm/DynFormFieldInstanceInterface";
import t from "@/utils/i18n";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";

/**
 * Component state.
 *
 * If a field is multivalued will build a multivalued component that
 * add any number of this type subfields.
 *
 * Ex. components of multivalued of select:
 *   select -> multivalued -> [select, ...]
 */
export interface DefaultFieldStateInterface {
    [key: string]: string
}

/**
 * Base field component.
 *
 * This adds support to Multivalued fields and custom render section.
 */
export default class BaseField extends React.Component<DynFormComponentPropsInterface, DefaultFieldStateInterface> {

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);
        this.state = this.getDefaultValue();

        if (this.isMultivaluedWrapped()) {
            this.props.definition.id = this.props.id;
        }
        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            if (!this.props.formFields[id]) {
                let instance:DynFormFieldInstanceInterface = {
                    id: id,
                    instance: this,
                    type: DynFormFieldInstanceTypes.Field,
                    conditional: this.props.definition.settings.meta?.conditional,
                    visible: true,
                    multivalued: this.props.definition.settings.multivalued,

                    // NOTE!!!: To work, getValue and setVisible must be declared in each heritage component.

                    /**
                     * Get current visibility usable value from Component.
                     *
                     * @param path {string} Completed field name path.
                     * @returns {any}
                     */
                    getValue: this.getVisibilityValue.bind(this),
                    /**
                     * Change visibility to component.
                     *
                     * @param isVisible {boolean}
                     */
                    setVisible: this.setVisible.bind(this),
                    /**
                     * Parent form was changed.
                     */
                    onFormChange: () => {},
                    /**
                     * Parent form has recalculated visibility.
                     */
                    onVisibilityUpdate: () => {},
                    /**
                     * A parent form group state was changed.
                     */
                    onGroupToggle: () => {},
                }
                this.props.formFields[id] = instance;
            }
        }

        // We must synchronize internal and external values.
        // Internal state: Used to manage component state.
        if (this.isMultivalued()) {
            // Value come from multivalued component.
            this.state = this.props.formData;
        } else if (this.isMultivaluedWrapped()) {
            // Value come from multivalued component.
            // External state: Used to update main form state.
            if (!this.props.formData[this.getValueMainProperty()]) {
                let def:{[key: string]: any} = this.getDefaultValue();
                for (const defKey in def) {
                    this.props.formData[defKey] = def[defKey];
                }
            }
            // Value come from form.
            this.state = this.formatInitValue(this.props.value);
        } else {
            // External state: Used to update main form state.
            if (!this.props.formData || !this.props.formData[this.getValueMainProperty()]) {
                // Delete default properties.
                if (this.props.formData) {
                    for (const propsKey in this.props.formData) {
                        delete this.props.formData[propsKey];
                    }
                }
                // Update response object.
                let def:{[key: string]: any} = this.getDefaultValue();
                for (const defKey in def) {
                    this.props.formData[defKey] = def[defKey];
                }
            }
            // Value come from form.
            this.state = this.formatInitValue(this.props.formData);
        }

        // Bind "this" value to handlers.
        this.handleChange = this.handleChange.bind(this);
        this.handleInvalid = this.handleInvalid.bind(this);
        this.getError = this.getError.bind(this);        
        this.formatValue = this.formatValue.bind(this);
        this.formatInitValue = this.formatInitValue.bind(this);
        this.getDefaultValue = this.getDefaultValue.bind(this);
        this.getValueTargetProperty = this.getValueTargetProperty.bind(this);
        this.isFormFieldsVisible = this.isFormFieldsVisible.bind(this);
        this.setVisible = this.setVisible.bind(this);
        this.getVisibilityValue = this.getVisibilityValue.bind(this);
        this.getValue = this.getValue.bind(this);
        this.setValue = this.setValue.bind(this);
        this.renderContentOnly = this.renderContentOnly.bind(this);
        this.consoleLog = this.consoleLog.bind(this);
    }

    /**
     * Get current visibility usable value from Component.
     *
     * @param path {string} Completed field name path.
     * @returns {any}
     */
    getVisibilityValue(path: string): any {
        if (this.isMultivalued()) {
            let resp: Array<any> = [];
            for (let i = 0; i < this.props.formData.length; i++) {
                resp.push(this.props.formData[i].value);
            }
            return resp;
        }

        return this.state.value;
    }

    /**
     * Change visibility to component.
     *
     * @param isVisible {boolean}
     */
    setVisible(isVisible: boolean) {
        if (this.props.formFields) {
            if (this.props.formFields[this.props.definition.id || ""].visible !== isVisible) {
                this.props.formFields[this.props.definition.id || ""].visible = isVisible;
                if (this.state) {
                    this.forceUpdate();
                }
            }
        }
    }

    /**
     * Is this field visible from props formFields?
     *
     * @returns {boolean}
     */
    isFormFieldsVisible() {
        if (this.props.formFields && this.props.formFields[this.props.definition.id || ""]) {
            return this.props.formFields[this.props.definition.id || ""].visible;
        }
        return true;
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        this.setValue(value);
        this.setState({[this.getValueMainProperty()]: value, 'errorClass': ''});
    }

    /**
     * Handle component invalid state.
     *
     * @param event
     */
    handleInvalid(event: any) {
        event.preventDefault();
        let error = this.getError(event);

        // If the title is not given, use the default title.
        if (error.title === '') {
            error.title = (this.props.definition.settings.meta.catalog_id ?? '') + ' ' + this.props.definition.label;
        }

        // Dispatch status event.
        eventManager.dispatch(
            Events.STATUS_ADD,
            {
                level: StatusEventLevel.ERROR,
                title: error.title,
                message: error.message,
                isPublic: true,
            }
        );

        this.setState({'errorClass': error.class});
    }

    /**
     * Return error title, message and css class.
     *
     * @param event
     * 
     * @returns {any}
     */
    getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must have a value.');
            errorClass = 'error-required';
        } else {
            message = t('The current value is not valid.');
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }

    /**
     * Allow change value type in multivalued if required.
     *
     * @param event The source event that require the format.
     * @param value
     * @returns {any}
     */
    formatValue(event: any, value: any) {
        return value;
    }

    /**
     * Allow to normalize initial field value.
     *
     * @param value The complete state field value.
     * @returns {any}
     */
    formatInitValue(value: any) {
        return value;
    }

    /**
     * Get the value property name in this component.
     *
     * ex. to select the property name is "value".
     * ex. to checkbox the property name is "checked".
     *
     * @returns {string}
     */
    getValuePropertyName() {
        return "value";
    }

    /**
     * Get main field property.
     *
     * @returns {string}
     */
    getValueMainProperty() {
        return "value";
    }

    /**
     * Get target field property.
     *
     * Used only in multivalued.
     *
     * @param event The source event that requires job.
     * @returns {string}
     */
    getValueTargetProperty(event: any) {
        return "value";
    }

    /**
     * Value to use as default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        // Assign to a variable is required to overwrite this method.
        let value: any = {[this.getValueMainProperty()]: ''};
        return value;
    }

    /**
     * Get current component state copy.
     *
     * @returns {any}
     */
    getState() {
        return JSON.parse(JSON.stringify(this.state));
    }

    /**
     * Get current value.
     *
     * @returns {any}
     */
    getValue() {
        if (this.isMonoValued()) {
            return this.props.formData[this.getValueMainProperty()];
        }
        else if (this.isMultivaluedWrapped()) {
            return this.props.value[this.getValueMainProperty()];
        }
    }

    /**
     * Set value.
     *
     * @param value {any}
     */
    setValue(value: any) {
        if (this.isMonoValued()) {
            this.props.formData[this.getValueMainProperty()] = value;
        }
        else if (this.isMultivaluedWrapped()) {
            this.props.value[this.getValueMainProperty()] = value;
        }
    }

    /**
     * Is this field monovalued?
     *
     * @returns {boolean}
     */
    isMonoValued() {
        return !this.isMultivalued() && !this.isMultivaluedWrapped();
    }

    /**
     * Is this field multivalued?
     *
     * @returns {boolean}
     */
    isMultivalued() {
        return this.props.definition.settings.multivalued;
    }

    /**
     * Is this field multivalued?
     *
     * @returns {boolean}
     */
    isMultivaluedWrapped() {
        return this.props.definition.settings._inMultivalued;
    }

    /**
     * Get multivalued wrapped index.
     *
     * @returns {number}
     */
    getMultivaluedIndex() {
        return this.props.definition.settings._multiIndex ?? 0;
    }

    /**
     * Is this field required?
     *
     * @returns {boolean}
     */
    isRequired() {
        return this.props.definition.settings.required;
    }

    /**
     * Need this form force respect Required attribute.
     *
     * @returns {boolean}
     */
    isForcedIsRequired() {
        return this.props.structure.forceIsRequired || false;
    }

    /**
     * Use this form levels?
     *
     * @returns {boolean}
     */
    useLevels() {
        return this.props.structure.formLevel || false;
    }

    /**
     * Get form level.
     *
     * @returns {number}
     */
    getFormLevel() {
        return this.props.structure.formLevel || 1;
    }

    /**
     * Use this form SDG level?
     *
     * @returns {boolean}
     */
    useSdgLevel() {
        return this.props.structure.formSdg || false;
    }

    /**
     * Get field description.
     *
     * @returns {string}
     */
    getDescription() {
        return this.props.definition.description;
    }

    /**
     * Render multivalued component.
     *
     * @returns {Component}
     */
    renderMultivalued() {
        return (
            <Multivalued
                key={this.props.definition.id}
                id={this.props.definition.id}
                structure={this.props.structure}
                definition={this.props.definition}
                formData={this.props.formData}
                // Parent field methods.
                onFormatValue={this.formatValue}
                onGetValuePropertyName={this.getValuePropertyName}
                onGetDefaultValue={this.getDefaultValue}
                onGetValueMainProperty={this.getValueMainProperty}
                onGetValueTargetProperty={this.getValueTargetProperty}
                onChange={this.props.onChange}
            />
        );
    }

    /**
     * Render field label.
     *
     * @returns {Component | null}
     */
    renderFieldLabel() {
        // Multivalued fields use one description for all subfields.
        if (this.isMultivaluedWrapped()) {
            return null
        }

        let catalogId = "";
        if (this.props.definition.settings.meta.catalog_id) {
            catalogId = this.props.definition.settings.meta.catalog_id;
        }

        let labelClassName = "";
        if (this.useLevels() || this.useSdgLevel()) {
            labelClassName = "level"+(this.props.definition.settings.meta.level ?? 1);
            if (this.props.definition.settings.meta.sdg ?? false) {
                labelClassName = "levelsdg";
            }
        }

        return (
            <div className={'field-label'}>
                <label className={labelClassName}>
                    <FieldLabel 
                        catalogId={catalogId} 
                        displayId={this.props.definition.settings.meta.displayId ?? catalogId !== ''}
                        label={this.props.definition.label} 
                        isRequired={this.isRequired()}
                    />
                </label>
            </div>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (<div>Todo: Render component input.</div>);
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <p className={"field-fake"}>
                Field &quot;<span className={"fake-id"}>{this.props.definition.id}</span>&quot; of type &quot;<span className={"fake-type"}>{this.props.definition.type}</span>&quot; <strong>don&apos;t render on read only mode</strong>.
            </p>
        );
    }

    /**
     * Render component description.
     *
     * @returns {Component | null}
     */
    renderDescription() {
        // Multivalued fields use one description for all subfields.
        // if field is disabled or inside a readonly dynform, it should have no description.
        if (
            this.isMultivaluedWrapped() ||
            this.props.structure.readonly ||
            this.props.definition.settings.meta.disabled ||
            this.props.definition.description === ""
        ) {
            return null
        }

        return (
            <div className={'field-description'}>
                <Markdown text={this.props.definition.description}/>
            </div>
        );
    }

    /**
     * Is this component visible?
     *
     * @returns {boolean}
     */
    isVisible() {
        if (!this.isFormFieldsVisible()) {
            return false;
        }
        // API want hidden this field?
        if (undefined !== this.props.definition.settings.meta.hidden && this.props.definition.settings.meta.hidden) {
            return false;
        }
        // Hidde field by form level.
        if ((this.props.structure.formLevel || 1) < (this.props.definition.settings.meta.level || 1)) {
            return false;
        }
        // Is this an SDG field in a non-SDG form?
        if (this.props.definition.settings.meta.sdg) {
            if ((undefined !== this.props.structure.meta.allow_sdg && !this.props.structure.meta.allow_sdg) || !this.props.structure.formSdg) {
                return false;
            }
        }
        return true;
    }

    /**
     * Debug console log filtering by catalog ID.
     *
     * @param msg
     * @param limitId
     */
    consoleLog(msg:string, limitId: string = '') {
        if (limitId !== '' && limitId !== this.props.definition.settings.meta.catalog_id) {
            return;
        }
        console.log(this.props.definition.settings.meta.catalog_id, msg);
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        if (!this.isVisible()) {
            return (<></>);
        }
        // Render multivalued.
        if (this.isMultivalued()) {
            return this.renderMultivalued();
        }
        // Render single value.

        let classes = 'form-field-' + this.props.definition.type + ' ' + (this.state.errorClass ?? '')
        if (this.isMultivalued()) {
            classes += " field-amount-multi ";
        } else {
            classes += " field-amount-single ";
        }
        if(this.isMonoValued()) {
            classes = classes + 'dynform-field'
        }

        return (
            <div className={classes}>
                {this.renderFieldLabel()}
                <div className={'field-input'}>
                    {this.props.structure.readonly || this.props.definition.settings.meta.disabled ? (
                        <>{this.renderContentOnly()}</>
                    ) : (
                        <>{this.renderInput()}</>
                    )}
                </div>
                {this.renderDescription()}
            </div>
        );
    }
}
