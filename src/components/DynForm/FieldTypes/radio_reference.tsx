/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import radio_select from "@/components/DynForm/FieldTypes/radio_select";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * TAP reference component.
 */
export default class radio_reference extends radio_select {

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        this.setApiClass = this.setApiClass.bind(this);
    }

    /**
     * Value to use as default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", class: ""};
    }
    
    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "";
    }

    /**
     * Get list to generate the options.
     * 
     * @returns {Promise}
     */
    getList() {
        return Promise.resolve([]);
    }

    /**
     * Get option key.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionKey(key: any, value: any) {
        return value.id ?? '';
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        return "";
    }

    /**
     * Set api class.
     */
    setApiClass() {
        let apiClass = this.getValue() === '' ? '' : this.getApiClass();

        if (this.isMonoValued()) {
            this.props.formData['class'] = apiClass;
        } 
        else if (this.isMultivalued()) {
            this.props.value['class'] = apiClass;
        }
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        if (this.isMonoValued()) {
            super.handleChange(event);
            this.setApiClass();
        } else if (this.isMultivalued()) {
            // Update multivalued status.
            this.selected[event.target.value] = !this.selected[event.target.value];
            // Update form data.
            this.props.formData.length = 0;
            for (const optionsKey in this.options) {
                if (this.selected[optionsKey]) {
                    let option = this.getDefaultValue();
                    option.value = optionsKey;
                    option.class = this.getApiClass();
                    this.props.formData.push(option);
                }
            }
            this.setState({'errorClass': ''});
        }
    }

    /**
     * Get current visibility usable value from Component.
     *
     * @param path {string} Completed field name path.
     * @returns {any}
     */
     getVisibilityValue(path: string): any {
        if (this.isMultivalued()) {
            let resp: Array<any> = [];
            for (let i = 0; i < this.props.formData.length; i++) {
                resp.push(this.list.find((item:any) => item.id === this.props.formData[i].value).keyIndex);
            }
            return resp;
        }

        if (this.state.value !== '') {
            return this.list.find((item:any) => item.id === this.state.value).keyIndex;
        }
        else {
            return this.state.value
        }
    }
}
