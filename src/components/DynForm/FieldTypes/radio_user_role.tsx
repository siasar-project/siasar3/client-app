/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { DynFormComponentPropsInterface } from "@/objects/DynForm/DynFormComponentPropsInterface";
import { dmGetRoles } from "@/utils/dataManager";
import radio_select from "./radio_select";

/**
 * User roles radio select
 */
export default class radio_user_role extends radio_select {

    /**
     * Select constructor.
     *
     * @param props
     */
     constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);

            this.getList = this.getList.bind(this);
        }
    }

    /**
     * Load list before render the field.
     */
    componentDidMount() {
        this.getList().then((list: any) => {
            this.list = list;
            
            if (list && Object.keys(list).length > 0) {
                for (const key in list) {
                    this.options[this.getOptionKey(key, list[key])] = this.getOptionLabel(key, list[key].label);
                }
            }

            this.loadingOptions = false;
            this.forceUpdate();
        });
    }

    /**
     * Get list to generate the options.
     *
     * @returns {Promise}
     */
     getList() {
        return dmGetRoles().then(list => {
            let mappedList: {[key: string]: any} = {};
            list.filter(role => role.id !== 'ROLE_ADMIN' && role.id !== 'ROLE_USER')
                .forEach(option => { mappedList[option.id] = option; });
            return mappedList;
        });
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
     getOptionLabel(key: any, value: any) {
        return value;
    }
}
