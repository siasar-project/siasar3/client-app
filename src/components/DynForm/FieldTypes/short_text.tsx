/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import Markdown from "@/components/common/Markdown";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Short text component.
 */
export default class short_text extends BaseField {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <>{this.state.value === "" ? (
                <span>{t("Empty")}</span>
            ) : (
                <Markdown text={this.state.value}/>
            )}
            </>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <input
                type="text"
                key={this.props.id}
                autoFocus={this.props.definition.settings.meta?.focus ?? false}
                id={this.props.id}
                data-index={this.props.definition.settings._multiIndex ?? ''}
                value={this.state.value}
                onChange={this.handleChange}
                onInvalid={this.handleInvalid}
                onKeyDown={this.handleKeyDown}
                required={this.isForcedIsRequired() && this.isRequired()}
                aria-required={this.isForcedIsRequired() && this.isRequired()}
                autoComplete={this.props.definition.settings.meta.autocomplete ? 'on' : 'new-password'}
            />
        );
    }
}
