/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import Pill from "@/components/common/Pill";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import {getTimezone} from "@/utils/siasar";
import router from "next/router";
import {uniqid} from "locutus/php/misc";


/**
 * Ulid component.
 */
export default class point_link extends BaseField {
  protected userTimezone = getTimezone();

  /**
   * constructor.
   *
   * @param props
   */
  constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
    super(props);

    // Add this component to post-process management.
    let id = this.props.definition.id || "";
    if (this.props.formFields) {
      /**
       * Get current visibility usable value from Component.
       *
       * @param path {string} Completed field name path.
       * @returns {any}
       */
      this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
      /**
       * Change visibility to component.
       *
       * @param isVisible {boolean}
       */
      this.props.formFields[id].setVisible = this.setVisible.bind(this);
    }
  }


  /**
   * Get Division name and path.
   *
   * @param meta {AdmDivisionMetaInterface}
   * @returns {string}
   */
  getAdministrativeDivisionBreadcrumb(meta: AdmDivisionMetaInterface) {
    if (!meta) {
      return "";
    }

    let resp = meta.name;
    if (meta.parent) {
      resp = this.getAdministrativeDivisionBreadcrumb(meta.parent) + " / " + resp;
    }
    return resp;
  }

  /**
   * Render component content only, not editable.
   *
   * @returns {Component}
   */
  renderContentOnly() {
    if (this.state.value === "") {
      return (
          <span>{t("Unknown")}</span>
      );
    }

    let data:any = this.state.meta;
// console.log('this.state.meta.updated', getLocalizedDate(data.updated, this.userTimezone));
    let divs: Array<any> = [];
    divs = data.administrative_division
        .map((div:any) => {
          return this.getAdministrativeDivisionBreadcrumb(div.meta);
        })
        .filter((elem: any, index: any, self: string | any[]) => {
          return index === self.indexOf(elem);
        });

    return (
        <>
            <div>
                <div className={"history-administrative-division"}>
                    <span className={"label"}>{t('Community')}:</span>&nbsp;
                    {divs.map((div: string) => {
                        return (<span className={"value"} key={uniqid()}>- {div}. </span>);
                    })}
                </div>
                <div className={"history-creation-date"}>
                    <span className={"label"}>{t('Updated')}:</span>&nbsp;
                    <span className={"value"}>{data.updated}</span>&nbsp;
                </div>
                <Pill title={"ID"} color={"red"} isUlid={true} id={this.state.value}/>
                <button
                    onClick={() => {
                        router.push(`/point/${this.state.value}`)
                            .then(() => {
                                window.location.reload();
                            });
                    }}
                    className="btn btn-household-process-reference btn-point btn-sep-top"
                >
                    <span className="icon"/> {t("Go to Point")}
                </button>
            </div>
        </>
    );
  }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return this.renderContentOnly();
        // return (
        //   <input
        //     type="text"
        //     key={this.props.id}
        //     id={this.props.id}
        //     data-index={this.props.definition.settings._multiIndex ?? ''}
        //     value={this.state.value}
        //     disabled
        //     />
        // )
    }
}
