/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { DynFormComponentPropsInterface } from "@/objects/DynForm/DynFormComponentPropsInterface";
import dynamic from "next/dynamic";
import BaseField from "./BaseField";
import React from "react";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import t from "@/utils/i18n";

/**
 * Map component.
 */
export default class map_point extends BaseField {
    readonly allowedValues: {[key: string]: number[]} = {
        latitude: [-90, 90],
        longitude: [-180, 180]
    }

    private groupToggleIndicator: number;
    private location: [number, number] = [0, 0];
    private MapWithNoSSR = dynamic(() => import("../../Map/LeafletMap"), { ssr: false });

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);
        
        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
            /**
             * update lat and lon values on dynform handleChange
             */
            this.props.formFields[id].onFormChange = this.getCoordinates.bind(this);
            /**
             * invalidate map size on field_group handleAccordionHeadingClick
             */
            this.props.formFields[id].onGroupToggle = this.invalidateMapSize.bind(this);
        }
        
        this.getCoordinates();
        this.groupToggleIndicator = Date.now();
        
        // this.handleChangeLocation = this.handleChangeLocation.bind(this);
        this.getCoordinates = this.getCoordinates.bind(this);
        this.invalidateMapSize = this.invalidateMapSize.bind(this);
    }

    /**
     * updates groupToggleIndicator value to trigger a map update
     */
    invalidateMapSize() {
        this.groupToggleIndicator = Date.now();
        this.forceUpdate();
    }

    /**
     * read coordinates from the specified fields in map definition
     */
    getCoordinates() {        
        const {lat_field, lon_field} = this.props.definition.settings;

        if(!lat_field || !lon_field ||!this.props.formFields) {
            this.location = [0, 0];
            this.forceUpdate();
            return;
        }

        let latitude = this.props.formFields[lat_field].instance.getValue() ?? 0;
        let longitude = this.props.formFields[lon_field].instance.getValue() ?? 0;

        // use previous values if new ones are not valid
        if(isNaN(latitude) || latitude === "") {
            latitude = this.location[0];
        }
        if (isNaN(longitude) || longitude === "") {
            longitude = this.location[1];
        }

        // clamp coords values
        if (latitude < this.allowedValues.latitude[0]) {
            latitude = this.allowedValues.latitude[0];
        }
        if (latitude > this.allowedValues.latitude[1]) {
            latitude = this.allowedValues.latitude[1];
        }
        if (longitude < this.allowedValues.longitude[0]) {
            longitude = this.allowedValues.longitude[0];
        }
        if (longitude > this.allowedValues.longitude[1]) {
            longitude = this.allowedValues.longitude[1];
        }
        
        this.location = [latitude, longitude];
        this.forceUpdate();
    }
    
    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (dmIsOfflineMode()) {
            return (<p>{t('Offline mode is not supported.')}</p>);
        }

        return (
            <this.MapWithNoSSR
                coordinates={this.location}
                groupToggleIndicator={this.groupToggleIndicator}
                zoom={12}
            />
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return this.renderContentOnly();
    }
}
