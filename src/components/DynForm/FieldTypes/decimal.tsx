/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Short text component.
 */
export default class decimal extends BaseField {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Return error message and class.
     *
     * @param event
     * 
     * @returns {any}
     */
    getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must have a value.');
            errorClass = 'error-required';
        } else {
            message = t('The current value is not valid, must be a decimal between %min and %max', {'%min': event.target.min, '%max': event.target.max});
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (this.state.value === "") {
            return (
                <span>{t("Unknown")}</span>
            );
        }

        return (
            <span>{this.state.value}</span>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        // Transform decimal settings to min/max/step values.
        let leftSide = (this.props.definition.settings.precision || 10) - (this.props.definition.settings.scale || 2);
        let rightSide = this.props.definition.settings.scale || 2;
        let maxValue:any = "";
        let minDecimal:any = "1";
        // Calculate maximum to this precision.
        for (let i = 0; i < leftSide; i++) {
            maxValue += "9";
        }
        maxValue = +maxValue;
        // Fix to the set maximum.
        if (maxValue > (this.props.definition.settings.max || 9223372036854775807)) {
            maxValue = (this.props.definition.settings.max || 9223372036854775807);
        }
        // Calculate step from scale.
        for (let i = 0; i < rightSide - 1 ; i++) {
            minDecimal = "0" + minDecimal;
        }
        // If scale defined.
        if (minDecimal !== "1") {
            // Use how minimum decimal.
            minDecimal = "0."+minDecimal;
        }

        return (
            <input
                type="number"
                key={this.props.id}
                id={this.props.id}
                data-index={this.props.definition.settings._multiIndex ?? ''}
                value={this.state.value}
                onChange={this.handleChange}
                onInvalid={this.handleInvalid}
                onKeyDown={this.handleKeyDown}
                min={this.props.definition.settings.min}
                max={maxValue}
                step={minDecimal}
                required={this.isForcedIsRequired() && this.isRequired()}
                aria-required={this.isForcedIsRequired() && this.isRequired()}
                onWheel={(e) => e.currentTarget.blur()}
            />
        );
    }
}
