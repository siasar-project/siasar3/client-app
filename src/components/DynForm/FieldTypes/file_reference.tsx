/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {dmRemoveFile, dmUploadFile, dmGetFileUrl, dmGetCountry} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import FieldLabel from "@/components/DynForm/FieldLabel";
import t from "@/utils/i18n";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import {formatBytes} from "@/utils/siasar";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

/**
 * File reference component.
 */
export default class file_reference extends BaseField {
    protected loadingFiles:boolean = false;

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        // Bind "this" value to handlers.
        this.handleRemoveFile = this.handleRemoveFile.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.renderButtons = this.renderButtons.bind(this);
    }

    /**
     * Get field description.
     *
     * @returns {string}
     */
    getDescription() {
        if (this.props.definition.settings.maximum_file_size) {
            return this.props.definition.description + " " + t(
                "Maximum upload file size: @value",
                {'@value': formatBytes(this.props.definition.settings.maximum_file_size)}
            );
        }
        return this.props.definition.description;
    }

    /**
     * Return error message and class.
     *
     * @param event
     * 
     * @returns {any}
     */
    getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must upload a file.');
            errorClass = 'error-required';
        } else {
            message = t('The file uploaded is not valid.');
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }
    
    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", class: "", filename: ""};
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
     getApiClass() {
        return "App\\Entity\\File";
    }

    /**
     * Get target field property.
     *
     * This is called inside the parent field from the multivalued wrapper.
     *
     * @param event The source event that requires job.
     * @returns {string}
     */
    getValueTargetProperty(event: any) {
        if (event.target.id !== `${this.props.id}[${event.target.attributes["data-index"].value}]_class`) {
            return "value";
        }
        return "class";
    }

    /**
     * Get main field property.
     *
     * @returns {string}
     */
    getValueMainProperty() {
        return "value";
    }

    /**
     * Handle component state.
     *
     * @param event
     */
    handleChange(event: any) {
        if (event.target.id !== (this.props.id + "_class")) {
            this.loadingFiles = false;

            if (event.target.files && event.target.files.length > 0) {
                const fileList = event.target.files;

                dmGetCountry().then((country:any) => {
                    this.loadingFiles = true;
                    this.forceUpdate();

                    const filePromises = Array.from(fileList).map((file:any) => {
                        dmUploadFile("/api/v1/countries/" + country.code, file).then((fileData) => {
                            this.loadingFiles = false;

                            // Is field is multivalue add entire file to formData array.
                            if (this.isMultivalued()) {
                                // Get default of field.
                                let newFile = this.getDefaultValue();
                                newFile.value = fileData.id;
                                newFile.class = this.getApiClass();
                                newFile.filename = fileData.fileName;
                                this.props.formData.push(newFile);
                                this.setState({count: this.props.formData.length, 'errorClass': ''});
                            } 
                            // If the field is single value set individual attributes to formData object.
                            else {
                                this.props.formData['value'] = fileData.id;
                                this.props.formData['class'] = this.getApiClass();
                                this.props.formData['filename'] = fileData.fileName;
                                this.setState({value: fileData.id, filename: fileData.fileName, 'errorClass': ''});
                            }

                            // return fileData;
                        }).catch(function(e) {
                            console.error('file_reference>>handleChange: ' + e.message);
                        });      
                    });

                    Promise.all(filePromises).then(() => {
                        this.loadingFiles = false;
                        this.forceUpdate();
                    });                  
                });
            }
        } else {
            this.props.formData["class"] = event.target.value;
            this.setState({["class"]: event.target.value, 'errorClass': ''});
        }
    }

    /**
     * Remove the file.
     *
     * @param event
     */
    handleRemoveFile(event: any) {
        let id = event.target.dataset.id;
        let self = this;
        if (!id) {
            id = event.target.parentNode.dataset.id;
        }
        if (!id) {
            id = event.target.parentNode.parentNode.dataset.id;
        }
        dmRemoveFile(id).then(() => {
            self.removeItem(id, event);
        }).catch(function(e) {
            console.error('file_reference>>handleRemoveFile: ' + e.message);
            // If this component have a not valid ID remove it.
            self.removeItem(id, event);
        });
    }

    /**
     * Remove item ID.
     *
     * @param id
     * @param event
     */
    removeItem(id: string, event: any) {
        if (this.isMultivalued()) {
            let oldCollection = JSON.parse(JSON.stringify(this.props.formData));
            this.props.formData.length = 0;

            for (let i = 0; i < oldCollection.length; i++) {
                if (oldCollection[i].value != id) {
                    // Update content data and count.
                    this.props.formData.push(JSON.parse(JSON.stringify(oldCollection[i])));
                }
            }

            this.setState({count: this.props.formData.length});
        } else {
            this.props.formData['value'] = '';
            this.props.formData['class'] = '';
            this.props.formData['filename'] = '';
            delete this.props.formData.meta;
            this.setState({value: '', filename: ''});
        }
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    /**
     * Get content preview of the file.
     * 
     * @param file
     * @param i
     * 
     * @returns {Component}
     */
    renderFilePreview(file:any, i: number) {
        return (
            <span
                key={this.props.definition.id + '-' + i}
                id={this.props.definition.id + '-' + i}
            >
                {file.filename}{file.meta?.size ? ' ('+formatBytes(file.meta.size)+')' : ''}
            </span>
        );
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        let files = this.isMultivalued() ? this.props.formData : [this.props.formData];
        let isEmpty = this.isMultivalued() ? (this.props.formData.length === 0) : (this.state.value === '');
        let fileItems = [];
        if (isEmpty) {
            return (
                <ul className={"dynform-multivalued"}>
                    <li key={this.props.definition.id + '-empty'} className={"dynform-field dynform-item"}>
                        {t("Without files.")}
                    </li>
                </ul>
            );
        }

        // If is multivalued always show the fileItems (if any), or if is single value only when not is empty.
        if (this.isMultivalued() || this.state.value !== '') {
            for (let i = 0; i < files.length; i++) {
                let file = files[i];

                fileItems.push(
                    <li key={this.props.definition.id + '-' + i} className={"dynform-field dynform-item"}>
                        {!dmIsOfflineMode() ? (
                            <a href={dmGetFileUrl(file.value)} target="_blank" rel="noreferrer">
                                {this.renderFilePreview(file, i)}
                            </a>
                        ) : (
                            <>
                                {this.renderFilePreview(file, i)}
                            </>
                        )}
                    </li>
                );
            }
        }

        return (
            <ul className={"dynform-multivalued"}>
                {fileItems}
            </ul>
        );
    }

    /**
     * Render multivalued component.
     *
     * @returns {Component}
     */
    renderMultivalued() {
        let catalogId = "";
        if (this.props.definition.settings.meta.catalog_id) {
            catalogId = this.props.definition.settings.meta.catalog_id;
        }
        let labelClassName = "";
        if (this.useLevels() || this.useSdgLevel()) {
            labelClassName = "level"+(this.props.definition.settings.meta.level ?? 1);
            if (this.props.definition.settings.meta.sdg ?? false) {
                labelClassName = "levelsdg";
            }
        }
        return (
            <fieldset className={"dynform-multivalued-field" + ' ' + (this.state.errorClass ?? '')}>
                <legend className={labelClassName}>
                    <FieldLabel
                        catalogId={catalogId}
                        displayId={this.props.definition.settings.meta.displayId ?? catalogId !== ''}
                        label={this.props.definition.label}
                        isRequired={this.props.definition.settings.required}
                    />
                </legend>
                {!this.props.structure.readonly ? (
                    <div className={'field-description'}>
                        {this.getDescription()}
                    </div>
                ) : (<></>)}
                <div className={'form-field-'+this.props.definition.type+' form-multivalued-list'}>
                    {this.props.structure.readonly || this.props.definition.settings.meta.disabled ? (
                        <>{this.renderContentOnly()}</>
                    ) : (
                        <>{this.renderInput()}</>
                    )}
                </div>
            </fieldset>
        );
    }

    /**
     * render relevant buttons for each file
     * 
     * @param file
     * @param i
     * @param count
     * 
     * @returns {JSX.Element}
     */
    renderButtons(file:any, i: number, count: number) {

        let buttons = [
            <button
                type="button"
                className={"dynform-remove btn btn-link"}
                key={this.props.definition.id + i + '-remove'}
                id={this.props.definition.id + i + '-remove'}
                data-id={file.value}
                onClick={this.handleRemoveFile}
            >
                <FontAwesomeIcon icon={faTrash} size="lg"/>
            </button>
        ]

        return buttons
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.loadingFiles) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        // Build file input, only accept list based in allow_extension setting.
        let fileAccept = this.props.definition.settings.allow_extension?.map(ext => '.'+ext).join(", ") ?? '*';

        // Build files list.
        let files = this.isMultivalued() ? this.props.formData : [this.props.formData];
        let fileItems = [];

        // If is multivalued always show the fileItems (if any), or if is single value only when not is empty.
        if (this.isMultivalued() || this.state.value !== '') {
            for (let i = 0; i < files.length; i++) {
                let file = files[i];
                
                fileItems.push(
                    <li key={this.props.definition.id + '-' + i} className={"dynform-item"}>
                        {!dmIsOfflineMode() ? (
                            <a href={dmGetFileUrl(file.value)} target="_blank" rel="noreferrer">
                                {this.renderFilePreview(file, i)}
                            </a>
                        ) : (
                            <>{this.renderFilePreview(file, i)}</>
                        )}

                        <div className="image-buttons">
                            {this.renderButtons(file, i, files.length)}
                        </div>
                    </li>
                );
            }
        } 

        // Render the component.
        return (
            <>
                <ul className={"dynform-multivalued"}>
                    {fileItems}
                </ul>

                { // If is multivalued always show the input, or if is single value only when is empty.
                this.isMultivalued() || this.state.value === '' ? 
                (
                    <input
                        type="file"
                        key={this.props.id+'-add'}
                        id={this.props.id+'-add'}
                        className={"dynform-add"}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        accept={fileAccept}
                        multiple={this.isMultivalued()}
                        onChange={this.handleChange}
                        onInvalid={this.handleInvalid}
                        required={this.isForcedIsRequired() && this.isRequired() && (this.isMultivalued() && files.length === 0 || this.state.value === '')}
                        aria-required={this.isForcedIsRequired() && this.isRequired() && (this.isMultivalued() && files.length === 0 || this.state.value === '')}
                    />
                ) : (<></>)}
            </>
        );           
    }
}
