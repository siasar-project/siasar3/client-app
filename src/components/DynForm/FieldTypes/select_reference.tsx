/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import select from "@/components/DynForm/FieldTypes/select";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * TAP reference component.
 */
export default class select_reference extends select {
    
    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        this.setApiClass = this.setApiClass.bind(this);
    }

    /**
     * Value to use as default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", class: ""};
    }
    
    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "";
    }

    /**
     * Get list to generate the options.
     * 
     * @returns {Promise}
     */
    getList() {
        return Promise.resolve([]);
    }

    /**
     * Get list item by Id.
     *
     * @param id {string}
     *
     * @returns {any}
     */
    getListItemById(id:string): any {
        for (let i = 0; i < this.list.length; i++) {
            if (id === this.list[i].id) {
                return this.list[i];
            }
        }

        return null;
    }

    /**
     * Get option key.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionKey(key: any, value: any) {
        return value.id ?? '';
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        return "";
    }

    /**
     * Set value.
     *
     * @param value {any}
     */
    setValue(value: any) {
        super.setValue(value);
        this.setApiClass();
    }

    /**
     * Set api class.
     */
    setApiClass() {
        let apiClass = this.getValue() === '' ? '' : this.getApiClass();

        if (this.isMonoValued()) {
            this.props.formData['class'] = apiClass;
        } 
        else if (this.isMultivaluedWrapped()) {
            this.props.value['class'] = apiClass;
        }
    }
}
