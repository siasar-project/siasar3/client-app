/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ReactElement} from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {User} from "@/objects/User";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {dmGetSafeUsers} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import UserTag from "@/components/common/UserTag";

/**
 * User reference component.
 *
 * @see https://www.tutorialesprogramacionya.com/htmlya/html5/temarios/descripcion.php?cod=181
 */
export default class user_reference extends BaseField {
    private userList: Array<User>;
    private loadingUsers:boolean = true;

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.userList = [];
    }

    /**
     * Load users before render the field.
     */
    componentDidMount() {
        dmGetSafeUsers().then((list:any) => {
            this.userList = list;
            this.loadingUsers = false;

            // If the field es a single value or a multivalue wrapped instance, set the state and internal (in props) value.
            if (this.isMonoValued() || this.isMultivaluedWrapped()) {
                let value = '';

                // If is required, and value is empty, and lis is not empty, set the first list value.
                if (this.isRequired() && this.state.value === '' && list.length > 0) {
                    value =  list[0].id ?? '';
                } 
                // If not is required, set current value.                
                else {
                    value = this.state.value;
                }

                this.setValue(value);

                // This raise the render() (so no need for a forceUpdate() here).
                this.setState({'value': value});  // esto lanza el render del componente, .
            } 
            // Is a wrapper field.
            else {
                this.forceUpdate();
            }
        });
    }

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", class: ""};
    }

    /**
     * Get target field property.
     *
     * This is called inside the parent field from the multivalued wrapper.
     *
     * @param event The source event that requires job.
     * @returns {string}
     */
    getValueTargetProperty(event: any) {
        if (event.target.id !== `${this.props.id}[${event.target.attributes["data-index"].value}]_class`) {
            return "value";
        }
        return "class";
    }

    /**
     * Get main field property.
     *
     * @returns {string}
     */
    getValueMainProperty() {
        return "value";
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        let propertyName = "class";
        if (event.target.id !== (this.props.id + "_class")) {
            propertyName = "value";
        }

        this.setValue(event.target.value);
        this.setState({[propertyName]: event.target.value, 'errorClass': ''});
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "App\\Entity\\User";
    }

    /**
     * Set value.
     * 
     * @param value {any}
     */
    setValue(value: any) {
        super.setValue(value);

        let apiClass = value === '' ? '' : this.getApiClass();

        if (this.isMonoValued()) {
            this.props.formData['class'] = apiClass;
        } 
        else if (this.isMultivaluedWrapped()) {
            this.props.value['class'] = apiClass;
        }
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (this.state.value === "") {
            return (<><span>{t("Unknown")}</span></>);
        }

        let label = "";
        let gravatar = "";
        for (let i = 0; i < this.userList.length; i++) {
            if (this.state.value === this.userList[i].id) {
                label = this.userList[i].username || t("Anonymous");
                gravatar = this.userList[i].gravatar || "";
                break;
            }
        }

        if (gravatar !== "") {
            return (<UserTag picture_url={gravatar} username={label}/>);
        }

        return (
            <span>{label}</span>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.loadingUsers) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        // Build options.
        let selectOptions:ReactElement[] = this.userList.map((user:User) => {
            return (
                <option key={"user_option_"+user.id} value={user.id}>
                    {user.username}
                </option>
            );
        });

        // Render the component.
        return (
            <select
                key={this.props.id + "_value"}
                id={this.props.id + "_value"}
                data-index={this.props.definition.settings._multiIndex ?? ''}
                value={this.state.value}
                onChange={this.handleChange}
                onInvalid={this.handleInvalid}
                required={this.isForcedIsRequired() && this.isRequired()}
                aria-required={this.isForcedIsRequired() && this.isRequired()}
            >
                {!this.isRequired() ? (
                    <option key={"user_option_none"} value="">
                        {t("Select one")}
                    </option>
                ) : (
                    <></>
                )}
                {selectOptions}
            </select>
        );
    }
}
