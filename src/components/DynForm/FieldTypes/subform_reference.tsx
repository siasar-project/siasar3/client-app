/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ReactElement} from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {
    dmGetFormRecord,
    dmGetFormStructure
} from "@/utils/dataManager";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import Loading from "@/components/common/Loading";
import Modal from "@/components/common/Modal";
import t from "@/utils/i18n";
import {DynForm} from "@/components/DynForm/DynForm";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {DynFormStructureFieldInterface} from "@/objects/DynForm/DynFormStructureFieldInterface";
import {stringToBoolean} from "@/utils/siasar";
import SubformMultivalued from "@/components/DynForm/FieldTypes/SubformMultivalued";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons/faPlusCircle";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import {dmAddFormRecord, dmRemoveFormRecord, dmUpdateFormRecord} from "@/utils/dataManager/FormRecordWrite";

/**
 * Subform component.
 */
export default class subform_reference extends BaseField {
    private isLoading: boolean = true;
    private isEditing: boolean = false;
    private isRemoving: boolean = false;
    private subformStructure: DynFormStructureInterface | undefined;
    private formEditState: DynFormRecordInterface;

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
            /**
             * update dynform preview on parent change.
             */
            this.props.formFields[id].onFormChange = this.updatePreview.bind(this);
        }

        this.formEditState = {
            formId: this.props.definition.settings.subform || "",
            id: this.props.formData[this.getValueMainProperty()],
            data: {},
        };

        // If field is multivalued wrapped instance.
        if (this.isMultivaluedWrapped()) {
            this.formEditState.id = this.props.value['value'];
        }

        this.checkParentState = this.checkParentState.bind(this);
        this.updatePreview = this.updatePreview.bind(this);
    }

    /**
     * updates preview with parent data, triggers forceUpdate()
     */
    updatePreview() {
        // skip multivalued subforms and currently editing subforms
        if (this.isEditing || this.isMultivalued()) {
            return;
        }

        // todo This update generates blinking in WSProvider 1.12 field while writing.
        // this.forceUpdate();
    }

    /**
     * Get sub form structure before render the field.
     */
    componentDidMount() {
        // let fId = this.props.definition.settings.subform || "";
        dmGetFormStructure(this.formEditState.formId)
            .then((structure: DynFormStructureInterface) => {
                structure.formLevel = this.props.structure.formLevel;  
                structure.formSdg = this.props.structure.formSdg;
                structure.isSubForm = true;

                this.subformStructure = structure;
                if (typeof this.formEditState.id === "string" && this.formEditState.id !== "") {
                    return this.loadRecord();
                }
                if(this.props.triggerOpenModal) {
                    this.isEditing = true;
                }
                this.isLoading = false;
                this.forceUpdate();
            })
            .catch(() => {
                this.isLoading = false;
                this.forceUpdate();
            });
    }

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "", form: this.props.definition.settings.subform};
    }

    /**
     * Handle close edit modal request.
     */
    handleRequestCloseEditing() {
        if ((!this.state.value || this.state.value === "") && this.props.onRemove) {
            this.props.onRemove(this.props);
        }
        this.isEditing = false;
        // this.forceUpdate();
        this.isLoading = true;
        this.loadRecord();
    }

    /**
     * Handle close remove modal request.
     */
    handleRequestCloseRemoving() {
        this.isRemoving = false;
        this.forceUpdate();
    }

    /**
     * Handle add modal request.
     */
    handleRequestAdd() {
        this.isEditing = true;
        this.forceUpdate();
    }

    /**
     * Handle remove modal request.
     */
    handleRequestRemove() {
        this.isLoading = true;
        this.isRemoving = false;
        this.forceUpdate();
        dmRemoveFormRecord(this.formEditState)
            .then(() => {
                this.removeItem(this.formEditState);
            })
            .catch(() => {
                this.removeItem(this.formEditState);
                this.isLoading = false;
                this.forceUpdate();
            });
    }

    /**
     * Remove a item.
     *
     * @param formEditState
     */
    removeItem(formEditState: DynFormRecordInterface) {
        formEditState.id = "";
        formEditState.data = {};
        // Is field is multivalued wrapped instance.
        if (this.isMultivaluedWrapped()) {
            this.props.value['value'] = "";
        }
        // If the field is single value.
        else {
            this.props.formData['value'] = "";
        }
        this.setState({value: ""});
        this.isLoading = false;
        this.forceUpdate();
        if (this.props.onRemove) {
            this.props.onRemove(this.props);
        }
        if (this.props.onChange) {
            this.props.onChange({});
        }
    }

    /**
     * Load current form record from remote.
     *
     * @returns {Promise}
     */
    loadRecord() {
        return dmGetFormRecord(this.formEditState, false)
            .then((resp:DynFormRecordInterface) => {
                this.formEditState.data = resp;
                // Update internal field status.
                this.setState({value: resp.id || ""});
                // Update DynForm field value.
                // Is field is multivalued wrapped instance.
                if (this.isMultivaluedWrapped()) {
                    this.props.value['value'] = resp.id;
                }
                // If the field is single value.
                else {
                    this.props.formData['value'] = resp.id;
                }
                this.isLoading = false;
                this.forceUpdate();
            })
            .catch(() => {
                this.removeItem(this.formEditState);
                this.isLoading = false;
                this.forceUpdate();
            });
    }

    /**
     * Save current subform record.
     *
     * @returns {Promise}
     */
    saveRecord() {
        if (this.formEditState.id === "") {
            // Add new record.
            return dmAddFormRecord(this.formEditState)
                .then((resp:DynFormRecordInterface) => {
                    // Update internal record wrapper.
                    this.formEditState.id = resp.id;
                    // And now, end edition.
                    this.isEditing = false;
                    // Update from server with autogenerated values.
                    return dmGetFormRecord(this.formEditState)
                        .then((resp:DynFormRecordInterface) => {
                            this.formEditState.data = resp;
                            // Update internal field status.
                            this.setState({value: resp.id || ""});
                            // Update DynForm field value.
                            // Is field is multivalued wrapped instance.
                            if (this.isMultivaluedWrapped()) {
                                this.props.value['value'] = resp.id;
                            }
                            // If the field is single value.
                            else {
                                this.props.formData['value'] = resp.id;
                            }
                            this.isLoading = false;
                            this.forceUpdate();
                            if (this.props.onChange) {
                                this.props.onChange({});
                            }
                        });
                })
                .catch(() => {
                    this.isLoading = false;
                    this.forceUpdate();
                });
        } else {
            // Update record.
            return dmUpdateFormRecord(this.formEditState)
                .then(resp => {
                    this.isLoading = false;
                    this.forceUpdate();
                    if (this.props.onChange) {
                        this.props.onChange({});
                    }
                    this.isEditing = false;
                })
                .catch(() => {
                    this.isLoading = false;
                    this.forceUpdate();
                });
        }
    }

    /**
     * Handle modal submit.
     *
     * @param event
     */
    handleButtonSubmitEditing(event: any) {
        switch (event.button) {
            case t("Save"):
                if (event.id === this.subformStructure?.id) {
                    event.event.preventDefault();
                    // Edition must end after save the record, if it's ok.
                    // this.isEditing = false;
                    this.isLoading = true;
                    this.formEditState.data = event.value;
                    if (!this.formEditState.id) {
                        this.formEditState.id = "";
                    }
                    this.saveRecord()
                        .then(() => {
                            this.isEditing = false;
                            this.forceUpdate();
                        });
                }
                break;
            default:
                console.log("Unknown button: ", event.button);
                break;
        }
    }

    /**
     * If value is a object return the value property, else return parameter value.
     *
     * @param value
     * @returns {any}
     */
    plainValue(value: any) {
        if (value.value !== undefined) {
            return value.value;
        }
        return value;
    }

    /**
     * Format a field value to the cell string content.
     *
     * @param fieldId Field ID.
     * @param value   Object with the value property.
     * @returns {string}
     */
    formatCell(fieldId: string, value: any) {
        let fieldDef = this.subformStructure?.fields[fieldId];
        switch (fieldDef?.type) {
            case "date":
                if (value.value === undefined || value.timezone === undefined) {
                    return "";
                }
                return value.value+" "+value.timezone;
            case "radio_boolean":
            case "boolean":
                if (stringToBoolean(this.plainValue(value))) {
                    return fieldDef?.settings.true_label || t("Yes");
                } else {
                    return fieldDef?.settings.false_label || t("No");
                }
            case "integer":
            case "decimal":
            case "long_text":
            case "short_text":
            case "phone":
            case "ulid":
                return this.plainValue(value) || "";
            default:
                console.log('Type not found: ', fieldDef?.type);
                return this.plainValue(value) || "";
        }
    }

    /**
     * Render multivalued component.
     *
     * @returns {Component}
     */
    renderMultivalued() {
        if (this.isLoading) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }
        return (
            <SubformMultivalued
                key={this.props.definition.id}
                id={this.props.definition.id}
                structure={this.props.structure}
                definition={this.props.definition}
                formData={this.props.formData}
                // Parent field methods.
                formFields={this.props.formFields}
                onFormatValue={this.formatValue}
                onGetValuePropertyName={this.getValuePropertyName}
                onGetDefaultValue={this.getDefaultValue}
                onGetValueMainProperty={this.getValueMainProperty}
                onGetValueTargetProperty={this.getValueTargetProperty}
                onRenderHeader={this.renderRowHeader.bind(this)}
                onChange={this.props.onChange}
            />
        );
    }

    /**
     * Render a header row.
     *
     * @returns {Array<DynFormStructureFieldInterface | undefined>}
     */
    renderRowHeader() {
        return (<></>);
    }

    /**
     * are the buttons visible ?
     * 
     * @returns {boolean}
     */
    buttonsVisibility() {
        return (
            this.state.value === "" || 
            (!this.props.structure.readonly && !this.props.definition.settings.meta.disabled)
        );
    }

    /**
     * Render a value row.
     *
     * @param colFields
     * @returns {Array<DynFormStructureFieldInterface | undefined>}
     */
    renderRow(colFields: Array<DynFormStructureFieldInterface | undefined>) {
        let preview = JSON.parse(JSON.stringify(this.subformStructure));
        preview.readonly = true;

        if (this.isLoading) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        let buttons = [
            <button
                type="button"
                value={t("Edit")}
                key="button-add"
                className={"btn btn-primary green"}
                onClick={this.handleRequestAdd.bind(this)}>
                <FontAwesomeIcon icon={faEdit} />{t("Edit")}
            </button>,
            <button
                type="button"
                value={t("Remove")}
                key="button-remove"
                className={"btn btn-primary btn-red"}
                onClick={() => {
                    this.isRemoving = true;
                    this.forceUpdate();
                }}
            >
                <FontAwesomeIcon icon={faTrash} />{t("Remove")}
            </button>
        ]

        return (
            <tr>
                <td>
                    {this.subformStructure && this.state.value !== "" ? (
                        <DynForm
                            key={"edit-form-"+this.subformStructure.id}
                            structure={preview}
                            buttons={buttons}
                            buttonsVisibility={this.buttonsVisibility.bind(this)}
                            onSubmit={this.handleButtonSubmitEditing.bind(this)}
                            value={this.formEditState.data}
                            isLoading={this.isLoading}
                            withAccordion={false}
                            checkParentState={this.checkParentState}
                        />
                    ) : (<span>{t("Empty")}</span>)}
                    {!this.props.structure.readonly && !this.props.definition.settings.meta.disabled? (
                        <>
                            {this.state.value !== "" ? (<></>) : (
                                <button
                                    type="button"
                                    value={"+"}
                                    key="button-add"
                                    className={"btn btn-primary green"}
                                    onClick={this.handleRequestAdd.bind(this)}
                                >
                                    <FontAwesomeIcon icon={faPlusCircle} />
                                </button>
                            )}
                            {this.renderModal()}
                        </>
                    ) : (<></>)}
                </td>
            </tr>
        );
    }

    /**
     * checks parent state for conditional
     * 
     * @param operator {string} Operator or field path.
     * @param operation {any} list of conditions or literal.
     * @param path {string}
     * 
     * @returns {boolean}
     */
    checkParentState(operator: string, operation: any, path: string) {
        let value = false;

        if (this.props.formFields) {
            if (this.props.formFields[path].multivalued) {
                let values = this.props.formFields?.[path].instance.getValue(operator);
                // If any sub-value is the required value, then condition is TRUE.
                for (let i = 0; i < values.length; i++) {
                    let inter = values[i] == operation;
                    if (inter) {
                        value = inter;
                        break;
                    }
                }
            } else {
                value = this.props.formFields?.[path].instance.getValue(operator) == operation;
            }
        }

        return value;
    }

    /**
     * Render required modals.
     *
     * @returns {ReactElement}
     */
    renderModal() {
        let editButtons: ReactElement[] = [
            (<input type="submit" value={t("Save")} key="button-save" className={"btn btn-primary green"}/>),
            (<input type="button" value={t("Cancel")} key="button-cancel" className={"btn btn-primary btn-red"} onClick={this.handleRequestCloseEditing.bind(this)}/>),
        ];
        return (<>
            {/* Edit record modal */}
            <Modal
                isOpen={this.isEditing}
                onRequestClose={this.handleRequestCloseEditing.bind(this)}
                title={this.state.value !== "" ? (
                    t("Edit record")
                ) : (
                    t("New record")
                )}
                size={"large"}
                footMessage={
                    this.state.value !== "" ? (
                        t("Remember that the edited data will be automatically saved in the system. This data can be edited as many times as needed.")
                    ) : (
                        ""
                    )
                }
            >
                {this.subformStructure ? (
                    <DynForm
                        key={"edit-form-"+this.subformStructure.id}
                        structure={this.subformStructure}
                        buttons={editButtons}
                        onSubmit={this.handleButtonSubmitEditing.bind(this)}
                        value={this.formEditState.data}
                        isLoading={this.isLoading}
                        withAccordion={true}
                        checkParentState={this.checkParentState}
                    />
                ) : (<></>)}
            </Modal>
            {/* Remove record modal */}
            <Modal
                isOpen={this.isRemoving}
                onRequestClose={this.handleRequestCloseRemoving.bind(this)}
                title={t("Remove record")}
                size={"small"}
            >
                {t("Remove this record?")}
                <div className={"modal-actions"}>
                    <input
                        type="button"
                        value={t("Ok")}
                        key="button-remove"
                        className={"btn btn-primary btn-red"}
                        onClick={this.handleRequestRemove.bind(this)}
                    />
                    <input
                        type="button"
                        value={t("Cancel")}
                        key="button-cancel-remove"
                        className={"btn btn-primary green"}
                        onClick={this.handleRequestCloseRemoving.bind(this)}
                    />
                </div>
            </Modal>
        </>);
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.isLoading) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        // Init column names and formatters.
        let colFieldsNames: Array<string> = this.subformStructure?.meta.table_fields || [];
        let colFields: Array<DynFormStructureFieldInterface | undefined> = colFieldsNames.map((fieldName:string) => {
            return this.subformStructure?.fields[fieldName];
        });

        return (
            <>
                {this.isMultivaluedWrapped() ? (
                    <>{this.renderRow(colFields)}</>
                ) : (
                    <table>
                        <thead>
                            {this.renderRowHeader()}
                        </thead>
                        <tbody>
                            {this.renderRow(colFields)}
                        </tbody>
                    </table>
                )}
            </>
        );
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        if (!this.isVisible()) {
            return (<></>);
        }
        if (this.isMultivalued()) {
            return this.renderMultivalued();
        }
        if (this.isMultivaluedWrapped()){
            return this.renderInput();
        }

        return (
            <div className={'dynform-field form-field-'+this.props.definition.type}>
                {this.renderFieldLabel()}
                {!this.props.structure.readonly ? (
                    <>{this.renderDescription()}</>
                ) : (<></>)}
                <div className={'field-input'}>
                    {this.renderInput()}
                </div>
            </div>
        );    
    }
}
