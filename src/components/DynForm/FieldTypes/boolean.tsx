/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import Switch from "@/components/common/Switch";
import {stringToBoolean} from "@/utils/siasar";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import t from "@/utils/i18n";

/**
 * Boolean component.
 *
 * THis component expose "1"/"0" values but internally use booleans.
 */
export default class boolean_field_type extends BaseField {
    internalValue = false;

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {value: "0"};
    }

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        // Normalize state.
        if (!this.isMultivalued()) {
            if (this.isMultivaluedWrapped()) {
                this.state = {value: stringToBoolean(this.state.value) ? "1" : "0"};
            } else {
                this.props.formData.value = stringToBoolean(this.props.formData.value) ? "1" : "0";
            }
         } else {
            if (this.props.formData) {
                for (let i = 0; i < this.props.formData.length; i++) {
                    this.props.formData[i].value = stringToBoolean(this.props.formData[i].value) ? "1" : "0";
                }
            }
        }
    }

    /**
     * Allow change value type in multivalued if required.
     *
     * @param event The source event that require the format.
     * @param value
     * @returns {any}
     */
    formatValue(event: any, value: any) {
        return value ? "1" : "0";
    }

    /**
     * Get the value property name in this component.
     *
     * Used in "multivalued" wrapper.
     *
     * ex. to "select" input the property name is "value".
     * ex. to "checkbox" input the property name is "checked".
     *
     * @returns {string}
     */
    getValuePropertyName() {
        return "checked";
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        return (
            <span>
                {this.state.value === "1" ? (
                    <>{this.props.definition.settings.show_labels ? (
                        <>{this.props.definition.settings.true_label}</>
                    ) : (<>{t('Yes')}</>)}</>
                ) : (
                    <>{this.props.definition.settings.show_labels ? (
                        <>{this.props.definition.settings.false_label}</>
                    ) : (<>{t('No')}</>)}</>
                )}
            </span>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (typeof this.state.value !== "boolean") {
            this.internalValue = stringToBoolean(this.state.value);
            this.state = {value: this.formatValue(null, this.internalValue)};
        }
        return (
            <>
                {this.props.definition.settings.show_labels ? (
                    <span className={"true-label"}>{this.props.definition.settings.false_label}</span>
                ) : (<></>)}
                <span className={"switch"}>
                    <Switch
                        key={this.props.id}
                        id={this.props.structure.id.replace('.','_')+'_'+this.props.id}
                        data-index={this.props.definition.settings._multiIndex ?? undefined}
                        //value={this.isMultivaluedWrapped() ? (this.props.value.value ?? false) : this.internalValue}
                        value={stringToBoolean(this.state.value)}
                        onChange={this.handleChange}
                    />
                </span>
                {this.props.definition.settings.show_labels ? (
                    <span className={"false-label"}>{this.props.definition.settings.true_label}</span>
                ) : (<></>)}
            </>
        );
    }
}
