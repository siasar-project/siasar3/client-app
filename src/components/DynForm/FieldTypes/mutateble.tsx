/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import {dmGetCountry} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import { DynFormComponentPropsInterface } from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Mutateble component.
 */
export default class mutateble extends BaseField {
    private mutatebleProps: any = {};
    private loading:boolean = true;

    /**
     * Mutateble field constructor.
     *
     * @param props
     */
     constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        // Bind functions
        this.isVisible = this.isVisible.bind(this);
        this.setVisible = this.setVisible.bind(this);
        this.hasDefinedMutation = this.hasDefinedMutation.bind(this);
    }

    /**
     * Load country before render the field.
     */
    componentDidMount() {
        dmGetCountry().then((country:any) => {
            if (country.mutatebleFields[this.props.structure.id]) {
                this.mutatebleProps = country.mutatebleFields[this.props.structure.id][this.props.definition.id] ?? {};
            }
            this.loading = false;
            this.setVisible(this.hasDefinedMutation());
            this.forceUpdate();
        });
    }    

    /**
     * checks if the field has a defined mutation, and it's type is not disabled 
     * 
     * @returns {boolean}
     */
    hasDefinedMutation() {
        return (this.mutatebleProps.mutate_to && this.mutatebleProps.mutate_to !== "disabled");
    }
    
    /**
     * Change visibility to component.
     *
     * @param isVisible {boolean}
     */
    setVisible(isVisible: boolean) {
        if (!this.hasDefinedMutation()) {
            if (this.props.formFields) {
                this.props.formFields[this.props.definition.id || ""].visible = false;
                this.forceUpdate();
            }
        }
        else {
            super.setVisible(isVisible);
        }
    }

    /**
     * Is this component visible?
     *
     * @returns {boolean}
     */
    isVisible() {
        if (!this.hasDefinedMutation()) {
            return false;
        }
        return super.isVisible();
    }

    /**
     * Render component.
     *
     * @returns {ReactElement}
     */
    render() {
        if (this.loading) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        const FakeComponent = React.lazy(
            () => import('@/components/DynForm/FieldTypes/'+'fake')
                .catch(() => ({
                    /**
                     * If not component found, log error to console.
                     *
                     * @returns {ReactElement}
                     */
                    default: function fakeComponentLoadError(): any { return (<>{console.error('['+this.props.structure.id+'] Component "fake" field type not found.')}</>);}
                }))
        );
        
        let definition = JSON.parse(JSON.stringify(this.props.definition));
        let value = this.getValue();
        let mutate_to = 'fake';
        let types = ['disabled', 'boolean', 'radio_boolean', 'select', 'radio_select', 'decimal', 'integer', 'long_text', 'short_text'];

        if (Object.keys(this.mutatebleProps).length > 0 && types.includes(this.mutatebleProps.mutate_to ?? '') ) {
            mutate_to = this.mutatebleProps.mutate_to;
            definition.type = mutate_to;
            definition.label = this.mutatebleProps.label ?? '';
            definition.description = this.mutatebleProps.description ?? '';

            switch (mutate_to) {
                case 'disabled':
                    return <></>;
                case 'boolean':
                case 'radio_boolean':
                    definition.settings.true_label = this.mutatebleProps.true_label ?? '';
                    definition.settings.false_label = this.mutatebleProps.false_label ?? '';
                    definition.settings.show_labels = this.mutatebleProps.show_labels ?? false;                    
                    break;
                case 'select':
                case 'radio_select':
                    definition.settings.options = this.mutatebleProps.options ?? {};
                    break;
                default:
                    break;
            }
        }

        if ("fake" === mutate_to) {
            return <></>;
        }

        const Component = React.lazy(
            () => import('@/components/DynForm/FieldTypes/'+mutate_to)
                .catch(() => ({
                    /**
                     * If not component found, log error to console.
                     *
                     * @returns {ReactElement}
                     */
                    default: function componentLoadError(): any {
                        return (
                            <>
                                {console.error('['+this.props.structure.id+'] Component "'+this.props.definition.type+'" field type not found.')}
                                <FakeComponent
                                    key={this.props.definition.id}
                                    id={this.props.definition.id}
                                    value={value}
                                    structure={this.props.structure}
                                    definition={definition}
                                    formData={this.props.formData}
                                />
                            </>
                        );
                    }
                }))
        );

        return (
            <React.Suspense fallback={<Loading />}>
                <Component
                    key={this.props.definition.id}
                    id={this.props.definition.id}
                    value={value}
                    structure={this.props.structure}
                    definition={definition}
                    formData={this.props.formData}
                />
            </React.Suspense>
        )
    }
}
