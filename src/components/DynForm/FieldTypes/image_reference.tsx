/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import file_reference from "@/components/DynForm/FieldTypes/file_reference";
import {dmGetFileUrl} from "@/utils/dataManager";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleLeft, faAngleRight, faAnglesLeft} from "@fortawesome/free-solid-svg-icons";
import {formatBytes} from "@/utils/siasar";
import ImageOffline from "@/components/common/ImageOffline";
import React from "react";

/**
 * File reference component.
 *
 * @see https://www.tutorialesprogramacionya.com/htmlya/html5/temarios/descripcion.php?cod=181
 */
export default class image_reference extends file_reference {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }
    }

    /**
     * shift images positions in array
     * 
     * @param index 
     * @param destination 
     */
    rearrangeImage(index:number, destination: number) {
        if (destination === 0) {
            let oldImages = [...this.props.formData]
            this.props.formData.length = 0
            this.props.formData.push(
                oldImages.find((_, i) => i === index),
                ...oldImages.filter((_, i) => i !== index)
            )
        }
        else {
            let temp = this.props.formData[destination]
            this.props.formData[destination] = this.props.formData[index]
            this.props.formData[index] = temp
        }

        this.forceUpdate()
    }

    /**
     * render relevant buttons for each file
     * 
     * @param file
     * @param i
     * @param count
     * 
     * @returns {JSX.Element}
     */
    renderButtons(file:any, i: number, count: number) {

        let buttons = super.renderButtons(file, i, count)
        
        if (this.isMultivalued()) {
            if(i !== 0) {
                buttons.push(
                    <button 
                        key={this.props.definition.id+'-first'}
                        type="button"
                        className="btn btn-link"
                        onClick={() => this.rearrangeImage(i, 0)}
                    >
                        <FontAwesomeIcon icon={faAnglesLeft} size="lg"/>
                    </button>,
                    <button 
                        key={this.props.definition.id+'-left'}
                        type="button"
                        className="btn btn-link"
                        onClick={() => this.rearrangeImage(i, i - 1)}
                    >
                        <FontAwesomeIcon icon={faAngleLeft} size="lg"/>
                    </button>
                )
            }
            if(i !== count - 1) {
                buttons.push(
                    <button 
                        key={this.props.definition.id+'-right'}
                        type="button"
                        className="btn btn-link"
                        onClick={() => this.rearrangeImage(i, i + 1)}
                    >
                        <FontAwesomeIcon icon={faAngleRight} size="lg"/>
                    </button>
                )
            }
        }

        return buttons
    }

    /**
     * Get content preview of the file.
     * 
     * @param file
     * @param i
     * 
     * @returns {Component}
     */
    renderFilePreview(file:any, i: number) {
        return (
            <div className="image-preview">
                <ImageOffline
                    src={dmGetFileUrl(file.value, false)}
                    alt={file.filename}
                    id={this.props.definition.id + "-" + i}
                    width={'100%'}
                    height={'100%'}
                />
                {file.meta?.size ?
                (
                    <small className="dynform-filesize">
                        {formatBytes(file.meta.size)}
                    </small>
                ) : (<></>)}
            </div>
        );
    }
}
