/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Short text component.
 */
export default class mail extends BaseField {

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Return error message and class.
     *
     * @param event
     * 
     * @returns {any}
     */
     getError(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let message: string;
        let errorClass: string;

        if (this.isForcedIsRequired() && this.isRequired() && (value === '' || value === null || value === undefined)) {
            message = t('Must have a email.');
            errorClass = 'error-required';
        } else {
            message = t('The current value is not valid, must be an email.');
            errorClass = 'error-not-valid';
        }

        return {title: '', message: message, class: errorClass};
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        if (this.state.value === "") {
            return (
                <span>{t("Unknown")}</span>
            );
        }

        return (
            <span>{this.state.value}</span>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        return (
            <input
                type="email"
                key={this.props.id}
                id={this.props.id}
                data-index={this.props.definition.settings._multiIndex ?? ''}
                value={this.state.value}
                onChange={this.handleChange}
                onInvalid={this.handleInvalid}
                onKeyDown={this.handleKeyDown}
                required={this.isForcedIsRequired() && this.isRequired()}
                aria-required={this.isForcedIsRequired() && this.isRequired()}
            />
        );
    }
}
