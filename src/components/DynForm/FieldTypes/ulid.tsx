/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import Pill from "@/components/common/Pill";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";

/**
 * Ulid component.
 */
export default class ulid extends BaseField {

  /**
   * constructor.
   *
   * @param props
   */
  constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
    super(props);

    // Add this component to post-process management.
    let id = this.props.definition.id || "";
    if (this.props.formFields) {
      /**
       * Get current visibility usable value from Component.
       *
       * @param path {string} Completed field name path.
       * @returns {any}
       */
      this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
      /**
       * Change visibility to component.
       *
       * @param isVisible {boolean}
       */
      this.props.formFields[id].setVisible = this.setVisible.bind(this);
    }
  }

  /**
   * Render component content only, not editable.
   *
   * @returns {Component}
   */
  renderContentOnly() {
    if (this.state.value === "") {
      return (
          <span>{t("Unknown")}</span>
      );
    }

    return (
        <Pill title={"ID"} color={"red"} isUlid={true} id={this.state.value} />
    );
  }

  /**
   * Render component input.
   *
   * @returns {Component}
   */
  renderInput() {
    return this.renderContentOnly();
    // return (
    //   <input
    //     type="text"
    //     key={this.props.id}
    //     id={this.props.id}
    //     data-index={this.props.definition.settings._multiIndex ?? ''}
    //     value={this.state.value}
    //     disabled
    //     />
    // )
  }
}
