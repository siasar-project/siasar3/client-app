/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ReactElement} from "react";
import BaseField from "@/components/DynForm/FieldTypes/BaseField";
import t from "@/utils/i18n";
import {Currency} from "@/objects/Currency";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import Loading from "@/components/common/Loading";
import {dmGetCurrencies} from "@/utils/dataManager/ParametricRead";

/**
 * currency component.
 *
 * @see https://www.tutorialesprogramacionya.com/htmlya/html5/temarios/descripcion.php?cod=181
 */
export default class currency extends BaseField {
    private currencyList: Array<Currency>;
    private loadingCurrencies:boolean = true;

    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
             this.props.formFields[id].getValue = (path: string): any => {
                if (this.isMultivalued()) {
                    let resp: Array<any> = [];
                    for (let i = 0; i < this.props.formData.length; i++) {
                        resp.push(this.getListItemById(this.props.formData[i].value)?.keyIndex);
                    }
                    return resp;
                }

                return this.getListItemById(this.state.value)?.keyIndex;
            };
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.currencyList = [];

        // Bind "this" value to handlers.
        this.getAmount = this.getAmount.bind(this);
        this.setAmount = this.setAmount.bind(this);
        this.setApiClass = this.setApiClass.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    /**
     * Prevent enter form submit.
     *
     * @param event
     */
    handleKeyDown(event:any) {
        if (13 === event.keyCode) {
            event.preventDefault();
        }
    }

    /**
     * Get list item by Id.
     *
     * @param id {string}
     *
     * @returns {any}
     */
    getListItemById(id:string): any {
        for (let i = 0; i < this.currencyList.length; i++) {
            if (id === this.currencyList[i].id) {
                return this.currencyList[i];
            }
        }

        return null;
    }

    /**
     * Load currencies before render the field.
     */
      componentDidMount() {
        if (this.isMultivalued()) {
            return;
        }
    
        dmGetCurrencies().then(list => {
            this.currencyList = list;
            this.loadingCurrencies = false;

            // If the field es a single value or a multivalue wrapped instance, set the state and internal (in props) value.
            if (!this.props.structure.readonly && !this.props.definition.settings.meta.disabled) {
                // If is required, and value is empty, and list is not empty, set the first list value.
                if (this.state.value === '' && list.length > 0) {
                    let value = list[0].id ?? '';
                    this.setValue(value);
                    this.setState({'value': value});
                } 
                else {
                    this.forceUpdate();
                }
            } 
            // Is a readonly or disabled field.
            else {
                this.forceUpdate();
            }
        });
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        let value = this.formatValue(event, event.target[this.getValuePropertyName()]);
        let propertyName = 'value';

        if (event.target.id === (this.props.id + "_value")) {
            this.setValue(value);
        } else {
            value = Number.parseFloat(value).toFixed(2);
            propertyName = 'amount';
            this.setAmount(value);
        }

        this.setApiClass();

        this.setState({[propertyName]: value, 'errorClass': ''});
    }

    /**
     * Value to use how default.
     *
     * @returns {any}
     */
    getDefaultValue() {
        return {amount: "0.00", value: "", class: this.getApiClass()};
    }

    /**
     * Get target field property.
     *
     * This is called inside the parent field from the multivalued wrapper.
     *
     * @param event The source event that requires job.
     * @returns {string}
     */
    getValueTargetProperty(event: any) {
        let id_split = event.target.id.split(`${this.props.id}__${event.target.attributes["data-index"].value}_`)

        return id_split[id_split.length - 1]
    }

    /**
     * Get main field property.
     *
     * @returns {string}
     */
    getValueMainProperty() {
        return "value";
    }

    /**
     * Value to use as Class.
     *
     * @returns {string}
     */
    getApiClass() {
        return "App\\Entity\\Currency";
    }

    /**
     * Get current amount.
     * 
     * @returns {any}
     */
    getAmount() {
        if (this.isMonoValued()) {
            return this.props.formData['amount'];
        } 
        else if (this.isMultivaluedWrapped()) {
            return this.props.value['amount'];
        }
    }

    /**
     * Set amount.
     * 
     * @param amount {any}
     */
    setAmount(amount: any) {
        if (this.isMonoValued()) {
            this.props.formData['amount'] = amount;
        } 
        else if (this.isMultivaluedWrapped()) {
            this.props.value['amount'] = amount;
        }
    }

    /**
     * Set api class.
     */
    setApiClass() {
        let apiClass = this.getValue() === '' ? '' : this.getApiClass();

        if (this.isMonoValued()) {
            this.props.formData['class'] = apiClass;
        } 
        else if (this.isMultivaluedWrapped()) {
            this.props.value['class'] = apiClass;
        }
    }

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    renderContentOnly() {
        let label = "";
        for (let i = 0; i < this.currencyList.length; i++) {
            if (this.state.value === this.currencyList[i].id) {
                label = this.currencyList[i].name;
                break;
            }
        }
        if (this.state.amount === "" && this.state.value === "") {
            return (<><span>{t("Unknown")}</span></>);
        }

        let amount:number = Number.parseFloat(this.state.amount);
        return (
            <div className={"composed-field"}>
                <div className={"composed-field-block"}>
                    <span>{amount.toFixed(2)}</span>
                </div>&nbsp;
                <div className={"composed-field-block"}>
                    {label}
                </div>
            </div>
        );
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        if (this.loadingCurrencies) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        // Build options.
        let options:ReactElement[] = this.currencyList.map((currency:Currency) => {
            return (<option key={"currency_option_"+currency.id} value={currency.id}>{currency.name}</option>);
        });

        // Render the component.
        let amount:number = Number.parseFloat(this.state.amount);
        return (
            <div className={"composed-field"}>
                <div className={"composed-field-block"}>
                    <label htmlFor={this.props.id + "_amount"}>{t("Amount")}</label>
                    <input
                        type="number"
                        key={this.props.id + "_amount"}
                        id={this.props.id + "_amount"}
                        // step="0.01"
                        // max={999999999999}
                        // min={0}
                        value={amount}
                        onChange={this.handleChange}
                        onInvalid={this.handleInvalid}
                        onKeyDown={this.handleKeyDown}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        required={this.isForcedIsRequired() && this.isRequired()}
                        aria-required={this.isForcedIsRequired() && this.isRequired()}
                        onWheel={(e) => e.currentTarget.blur()}
                    />
                </div>
                <div className={"composed-field-block"}>
                    <label htmlFor={this.props.id + "_value"}>{t("Currency")}</label>
                    <select
                        key={this.props.id + "_value"}
                        id={this.props.id + "_value"}
                        data-index={this.props.definition.settings._multiIndex ?? ''}
                        value={this.state.value}
                        onChange={this.handleChange}
                        className={'not-validate'}
                    >
                        {options}
                    </select>
                </div>
            </div>
        );
    }
}
