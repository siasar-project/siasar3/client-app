/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {dmGetUnits} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import t from "@/utils/i18n";
import Measure from "@/components/DynForm/FieldTypes/measure";
import Modal from "@/components/common/Modal";
import {DynFormComponentPropsInterface} from "@/objects/DynForm/DynFormComponentPropsInterface";
import {dmGetDefaultDiameters} from "@/utils/dataManager/ParametricRead";

/**
 * Short text component.
 */
export default class diameter extends Measure {
    private showModal = false;
    private options: { [key: string]: string } = {};
    private selected: string = '';

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
             this.props.formFields[id].getValue = (path: string): any => {
                if (this.isMultivalued()) {
                    let resp: Array<any> = [];
                    for (let i = 0; i < this.props.formData.length; i++) {
                        resp.push(this.getListItemById(this.props.formData[i].value)?.unit);
                    }
                    return resp;
                }

                return this.getListItemById(this.state.value)?.unit;
            };
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }
    }

    /**
     * Load materials before render the field.
     */
    componentDidMount() {
        let unit = "";

        dmGetDefaultDiameters().then(list => {
            for (let i = 0; i < list.length; i++) {
                this.options[list[i].value+'-'+list[i].unit] = list[i].value + ' ' + list[i].unit;
            }

            dmGetUnits("system.units.length").then(units => {
                this.loadingOptions = false;
                // Set all units length.
                this.unitList = units.all;
                this.unitCountryList = units.country || [];
                this.unitMain = units.main;

                // Setting initial value.
                if (!this.props.structure.readonly && !this.props.definition.settings.meta.disabled) {
                    if (this.state && this.state.unit === "") {
                        if (this.unitMain) {
                            unit = this.unitMain.id
                        } else if (this.unitCountryList.length) {
                            unit = this.unitCountryList[0].id
                        } else {
                            unit = this.unitList[Object.keys(this.unitList)[0]].id
                        }

                        if (this.isMonoValued()) {
                            this.props.formData["unit"] = unit;
                        }
                        else if (this.isMultivaluedWrapped()) {
                            this.props.value["unit"] = unit;
                        }

                        this.setState({['unit']: unit});
                    }
                }

                this.forceUpdate();
            });
        });
    }

    /**
     * @param event
     */
    selectDiameter = (event: any) => {
        event.preventDefault();
        const multiIndex = this.props.definition.settings._multiIndex ?? -1;

        this.selected = event.target.value;
        this.showModal = false;

        // Setting option value.
        let valueSplit = this.selected.split('-');
        this.props.formData['value'] = valueSplit[0];
        this.props.formData['unit'] = valueSplit[1];
        this.setState({ ['unit']: valueSplit[1], ['value']: valueSplit[0] });

        if (multiIndex >= 0) {
            this.props.value['value'] = valueSplit[0];
            this.props.value['unit'] = valueSplit[1];
        }

        this.forceUpdate();
        if (this.props.onChange) {
            this.props.onChange(event);
        }
    }

    /**
     * Close default diameter modal.
     */
    handleDefaultClose() {
        this.showModal = false;
        this.forceUpdate();
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        const { id } = this.props
        const multiIndex = this.props.definition.settings._multiIndex ?? -1

        if (this.loadingOptions) {
            return (<Loading key={`${this.props.definition.id}_loading`} />);
        }

        // Build option list.
        let options = [];
        let index = 1;
        for (const optionsKey in this.options) {
            options.push(
                <li key={this.props.definition.id + '-' + optionsKey + '-' + (index++)} className={"dynform-field dynform-item"}>
                    <button
                        name={this.props.definition.id}
                        data-index={multiIndex ?? ''}
                        key={this.props.definition.id + '-' + optionsKey + '-' + (index++)}
                        id={
                            multiIndex < 0 ? this.props.definition.id + '-option-' + optionsKey  : this.props.definition.id + '-option-' + optionsKey + '__' + multiIndex
                        }
                        value={optionsKey}
                        className="form-check-input btn btn-link"
                        onClick={this.selectDiameter.bind(this)}
                    >
                        {this.options[optionsKey]}
                    </button>
                </li>
            );
        }

        return (
            <>
                <button
                    id={multiIndex >= 0
                        ? `button-toggle-diameters__${multiIndex}`
                        : "button-toggle-diameters"
                    }
                    data-index={multiIndex ?? ''}
                    onClick={(event) => {
                        event.preventDefault();
                        this.showModal = !this.showModal;
                        this.forceUpdate();
                }}>
                    {t('Select default diameter')}
                </button>
                {/* Edit record modal */}
                <Modal
                    isOpen={this.showModal}
                    onRequestClose={this.handleDefaultClose.bind(this)}
                    title={t("Select default diameter")}
                    size={"small"}
                >
                    <div className={
                        multiIndex >= 0
                            ? `modal-diameter__${multiIndex}`
                            : "modal-diameter"
                    }>
                        <ul className={"dynform-multivalued"}>
                            {options}
                        </ul>
                    </div>
                </Modal>
                <input
                    type="number"
                    key={id}
                    id={id}
                    data-index={this.props.definition.settings._multiIndex ?? ''}
                    value={this.state.value}
                    onChange={this.handleChange}
                    onInvalid={this.handleInvalid}
                    min={this.props.definition.settings.min}
                    required={this.isForcedIsRequired() && this.isRequired()}
                    aria-required={this.isForcedIsRequired() && this.isRequired()}
                    // disabled={true}
                    onWheel={(e) => e.currentTarget.blur()}
                />
                <select
                    id={id + "_unit"}
                    key={id + "_unit"}
                    data-index={this.props.definition.settings._multiIndex ?? ''}
                    onChange={this.handleChange}
                    className={'not-validate'}
                    value={this.state.unit}
                    /*disabled={true}*/
                >
                    {this.renderUnitOptions()}
                </select>
            </>
        );
    }
}
