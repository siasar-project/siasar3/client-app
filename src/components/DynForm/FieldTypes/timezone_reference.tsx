/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { DynFormComponentPropsInterface } from "@/objects/DynForm/DynFormComponentPropsInterface";
import select from "./select";

// Property 'supportedValuesOf' does not exist on type 'typeof Intl'.ts(2339)
// https://github.com/microsoft/TypeScript/issues/49231#issuecomment-1137251612
declare namespace Intl {
    type Key = 'calendar' | 'collation' | 'currency' | 'numberingSystem' | 'timeZone' | 'unit';

    function supportedValuesOf(input: Key): string[];
}

/**
 * timezone select reference component.
 *
 * @see https://www.tutorialesprogramacionya.com/htmlya/html5/temarios/descripcion.php?cod=181
 */
export default class timezone_reference extends select {
    /**
     * Select constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.loadingOptions = true;
    }

    /**
     * Get list to generate the options.
     *
     * @returns {Promise}
     */
    getList() {
        let timezones: {[key: string]: string} = {};
        Intl.supportedValuesOf('timeZone').forEach((timezone:any) => {
           timezones[timezone] = timezone;
        });
        return Promise.resolve(timezones);
    }

    /**
     * Get option label.
     *
     * @param key {any}
     * @param value {any}
     *
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        return value;
    }
}
