/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import Loading from "@/components/common/Loading";
import { DynFormComponentPropsInterface } from "@/objects/DynForm/DynFormComponentPropsInterface";
import { dmGetCountry } from "@/utils/dataManager";
import select_reference from "./select_reference";
import LinearProgressWithLabel from "@/components/common/LinearProgressWithLabel";
import React from "react";
import {smHasPermission} from "@/utils/sessionManager";
import {dmGetTranslationProgress} from "@/utils/dataManager/Translation";

/**
 * translatable language reference component.
 */
export default class translatable_language_reference extends select_reference {
    protected progress: any = null;

    /**
     * translatable language constructor.
     *
     * @param props
     */
    constructor(props: DynFormComponentPropsInterface | Readonly<DynFormComponentPropsInterface>) {
        super(props);

        // Add this component to post-process management.
        let id = this.props.definition.id || "";
        if (this.props.formFields) {
            /**
             * Get current visibility usable value from Component.
             *
             * @param path {string} Completed field name path.
             * @returns {any}
             */
            this.props.formFields[id].getValue = this.getVisibilityValue.bind(this);
            /**
             * Change visibility to component.
             *
             * @param isVisible {boolean}
             */
            this.props.formFields[id].setVisible = this.setVisible.bind(this);
        }

        this.loadingOptions = true;

        this.getTranslationProgress.bind(this);
    }

    /**
     * Component did mount
     */
    componentDidMount() {
        super.componentDidMount();
        this.getTranslationProgress();
    }

    /**
     * Get list to generate the options.
     *
     * @returns {Promise}
     */
    getList() {
        return dmGetCountry().then((res: any) => {
            let translatableLanguages = res.languages.filter((lang: any) => {
                if (smHasPermission("all permissions")) {
                    // A super admin can translate any language.
                    return true;
                }
                return lang.translatable;
            });
            return translatableLanguages;
        });
    }

    /**
     * Get option key.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionKey(key: any, value: any) {
        return value.isoCode ?? '';
    }

    /**
     * Get option label.
     * 
     * @param key {any}
     * @param value {any}
     * 
     * @returns {string}
     */
    getOptionLabel(key: any, value: any) {
        return value.name;
    }

    /**
     * fetch translation progress for selected language
     */
    getTranslationProgress() {
        if (!this.state.value) {
            this.progress = null;
            return;
        }

        if (this.progress?.iso_code === this.state.value) {
            return;
        }
        dmGetTranslationProgress(this.state.value)
            .then(res => {
                this.progress = {...res, iso_code: this.state.value};
                this.forceUpdate();
            })
            .catch((info) => {
                this.progress = null;
                this.forceUpdate();
            });
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        this.progress = null;
        super.handleChange(event);
        this.getTranslationProgress();
    }

    /**
     * Render component input.
     *
     * @returns {Component}
     */
    renderInput() {
        let detailedInfo: {[key: string]: number} = {};
        if (this.progress) {
            detailedInfo = {
                translated: this.progress.translated,
                untranslated: this.progress.untranslated,
                total: this.progress.total,
            };
        } else {
            this.getTranslationProgress();
        }

        return (
            <>
                { super.renderInput() }
                { !this.progress ? (
                    <Loading /> 
                ) :(
                    <ProgressBar value={this.progress.progress} detailedInfo={detailedInfo} />
                ) }
            </>
        );
    }
}

/**
 * renders a progress bar 
 * 
 * @param props {any}
 * @param props.value {number}
 * @param props.detailedInfo {any}
 * 
 * @returns {Jsx.Element}
 */
const ProgressBar = ({value = 100, detailedInfo}: {value: number, detailedInfo?: {[key: string]: number}}) => {
    if (!detailedInfo) {
        detailedInfo = {
            translated: 0,
            untranslated: 0,
            total: 0,
        };
    }
    return (
        <>
            <div>
                <LinearProgressWithLabel
                    value={value}
                    variant="determinate"
                    color="secondary"
                    label={detailedInfo['translated']+"/"+detailedInfo['total']}
                />
            </div>
        </>
    )
};
