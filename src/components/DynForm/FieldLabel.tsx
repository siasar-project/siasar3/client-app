/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {empty} from "locutus/php/var";

interface FieldLabelInterface {
    catalogId: string
    label: string
    isRequired: boolean
    displayId?: boolean
}

/**
 * Field label.
 */
export default class FieldLabel extends React.Component<FieldLabelInterface> {

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props: FieldLabelInterface) {
        super(props);
    }

    /**
     * Render inquiry card.
     *
     * @returns {ReactElement}
     */
    render() {
        return (
            <>
                {
                    (this.props.displayId === undefined || this.props.displayId) && 
                    (this.props.catalogId === "0" || !empty(this.props.catalogId)) 
                        ? (<span className={"catalog-id"}>{this.props.catalogId}</span>)
                        : (<></>)
                }
                {
                    this.props.isRequired ?
                        (<span className={"required"}>*</span>) :
                        (<></>)
                }
                <span className={"title"}>{this.props.label}</span>
            </>
        );
    }
}
