/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {ReactElement, useEffect, useState} from "react";
import Pager from "@/components/common/Pager";
import CardContainer from "@/components/Cards/CardContainer";
import {AccordionItemPanel} from "react-accessible-accordion";
import t from "@/utils/i18n";
import {faCalculator, faFilter, faFolderTree, faPlusCircle, faXmark} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import router from "next/router";
import {smHasPermission} from "@/utils/sessionManager";
import {eventManager, Events, HouseholdEvents, StatusEventLevel} from "@/utils/eventManager";
import Modal from "@/components/common/Modal";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import {dmAddHouseholdSurvey, dmCloseHouseholdProcess, dmGetHouseholdProcesses} from "@/utils/dataManager/Household";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {DynForm} from "@/components/DynForm/DynForm";
import getHHProcessFilterSchema from "../../Settings/dynforms/HHProcessFilterSchema";

/**
 * renders card container and pager for household processes
 *
 * @returns {JSX.Elelment}
 */
export default function HouseholdProcessCards() {

    // List state.
    const [householdProcesses, setHouseholdProcesses] = useState([])
    // Pager state.
    const [processesPage, setProcessesPage] = useState(1);
    // Is loading, used to init data load.
    const [isLoadingProcesses, setIsLoadingProcesses] = useState(false);
    // close process modal state
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [processToClose, setProcessToClose] = useState()
    // used to dynamically change card buttons
    const [filterByProcessID, setFilterByProcessID] = useState(lcmGetItem('household_process_filter', null));
    // Filter state.
    const [filterState, setFilterState] = useState(lcmGetItem(
        'hhprocess_filter',
        {
            "field_region": {"value": ""},
            "field_open": {"value": "A"},
        }
    ));
    const [listedItems, setListedItems] = useState(0);

    /**
     * load household processes.
     */
    useEffect(() => {
        if (isLoadingProcesses) {
            // Load householdProcesses list.
            let filterOpen:boolean|undefined = true;
            if ("C" === filterState.field_open.value) {
                filterOpen = undefined;
            }
            else {
                filterOpen = ("A" === filterState.field_open.value);
            }
            dmGetHouseholdProcesses(processesPage, filterOpen, filterState.field_region.value).then((data) => {
                setHouseholdProcesses(data);
                setListedItems(data.length);
            })
        }
    }, [isLoadingProcesses])

    /**
     * processes loaded.
     */
    useEffect(() => {
        setIsLoadingProcesses(false);
    }, [householdProcesses])

    /**
     * processesPage changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (processesPage === 0) {
            // Page 0 is not valid, must be 1.
            setProcessesPage(1);
        } else {
            setIsLoadingProcesses(true);
        }
    }, [processesPage])

    /**
     * Save filter state.
     */
    useEffect(() => {
        lcmSetItem('hhprocess_filter', filterState);
    }, [filterState])

    /**
     * Handle component state.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFilterState(event.value);
                setProcessesPage(0);
                break;
            case t("Reset"):
                // todo Implement form reset compatibility.
                setFilterState({"field_region": {"value": ""}, "field_open": {"value": "A"}});
                break;
            case t("Refresh"):
                // Do nothing
                break;
        }
        // Force update.
    }

    /**
     * Update processes page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginateProcesses = (state: { page: number }) => {
        setProcessesPage(state.page);
    }

    /**
     * add survey to process and refresh user households
     *
     * @param data {any}
     */
    const handleAddHouseholdSurvey = (data: any) => {
        dmAddHouseholdSurvey(data.id).then(res => {
            // Unset loading.
            eventManager.dispatch(
                Events.HOUSEHOLD_EVENT,
                { event: HouseholdEvents.SURVEY_ADDED, processId: data.id }
            );
            // Alert user.
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.SUCCESS,
                    title: t('Household survey successfully created'),
                    message: t(`a new household survey for process @division has been created`, {"@division": data.administrativeDivision.name}),
                    isPublic: true
                }
            );
        })
    }

    /**
     * filter household process cards by parent process
     *
     * @param data {any}
     */
    const handleFilterSurveys = (data: any) => {
        if(filterByProcessID === data.id) {
            setFilterByProcessID(null);
            lcmSetItem('household_process_filter', null);
        } else {
            setFilterByProcessID(data.id);
            lcmSetItem('household_process_filter', data.id);
        }
        eventManager.dispatch(
            Events.HOUSEHOLD_EVENT,
            {
                event: HouseholdEvents.FILTER_SURVEYS,
                processId: data.id,
                processMeta: data.administrativeDivision.meta,
            }
        );
    }

    // Form buttons.
    let filterButtons: ReactElement[] = [
        (<input type="submit" value={t("Filter")} key="button-filter" className={"btn btn-primary"} disabled={isLoadingProcesses}/>),
    ]

    /**
     * close process and refetch processes
     *
     * @param data {any}
     */
    const handleCloseProcess = (data: any) => {
        dmCloseHouseholdProcess(data.id).then(res => {
            setIsLoadingProcesses(true)
            setIsModalOpen(false)
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.SUCCESS,
                    title: t('Household process closed correctly'),
                    message: t("Household process @process has been closed and will no longer receive new surveys", {"@process": data.administrativeDivision.name}),
                    isPublic: true
                }
            )
            // trigger update for household surveys
            eventManager.dispatch(
                Events.HOUSEHOLD_EVENT,
                { event: HouseholdEvents.PROCESS_CLOSED, processId: data.id }
            );
        }).catch((info) => console.log(info));
    }

    return (
        <>
            <AccordionItemPanel>
                <div className="filters-wrapper">
                    <DynForm
                        key={"filter-form"}
                        className={"filters-wrapper-form"}
                        structure={getHHProcessFilterSchema()}
                        buttons={filterButtons}
                        onSubmit={handleButtonSubmit}
                        value={filterState}
                        isLoading={false}
                        withAccordion={false}
                    />
                </div>
                <Pager page={processesPage} itemsAmount={listedItems} onChange={paginateProcesses} isLoading={isLoadingProcesses}/>
                <CardContainer
                    enableList
                    cardType={"CardHouseholdProcess"}
                    cards={householdProcesses}
                    isLoading={isLoadingProcesses}
                    buttons={[
                        {
                            id: 'btnFilterSurveys',
                            label: t('Remove filter'),
                            className: 'btn-primary btn-red',
                            /**
                             * is this button visible ?
                             *
                             * @param data {any}
                             *
                             * @returns {boolean}
                             */
                            show: (data: any) => {
                                if (!filterByProcessID) return false;
                                if (filterByProcessID !== data.id) return false;
                                return smHasPermission('read doctrine householdprocess')
                            },
                            icon: <FontAwesomeIcon icon={faXmark}/>,
                            onClick: handleFilterSurveys
                        },
                        {
                            id: 'btnFilterSurveys',
                            label: t('Filter surveys'),
                            className: 'btn-primary btn-tertiary',
                            /**
                             * is this button visible ?
                             *
                             * @param data {any}
                             *
                             * @returns {boolean}
                             */
                            show: (data: any) => {
                                if (filterByProcessID === data.id) return false;
                                return smHasPermission('read doctrine householdprocess')
                            },
                            icon: <FontAwesomeIcon icon={faFilter}/>,
                            onClick: handleFilterSurveys
                        },
                        {
                            id: 'btnAddHousehold',
                            label: t('Add Household'),
                            className: 'btn-primary',
                            /**
                             * is this button visible ?
                             *
                             * @param data {any} Process data.
                             *
                             * @returns {boolean}
                             */
                            show: (data: any): boolean => {
                                if (!data.open) {
                                    return false;
                                }
                                return smHasPermission('read doctrine householdprocess');
                            },
                            icon: <FontAwesomeIcon icon={faPlusCircle}/>,
                            onClick: handleAddHouseholdSurvey
                        },
                        {
                            id: 'btnCloseProcess',
                            label: t('Close Process'),
                            className: 'btn-primary btn-secondary',
                            /**
                             * is this button visible ?
                             *
                             * @param data {any} Process data.
                             *
                             * @returns {boolean}
                             */
                            show: (data: any): boolean => {
                                if (!data.open) {
                                    return false;
                                }
                                return !dmIsOfflineMode() && smHasPermission('update doctrine householdprocess');
                            },
                            icon: <FontAwesomeIcon icon={faCalculator}/>,
                            /**
                             * toggle modal
                             *
                             * @param process
                             *
                             * @returns {void}
                             */
                            onClick: (process: any) => {
                                setProcessToClose(process)
                                setIsModalOpen(true)
                            }
                        },
                        {
                            id: 'btnRedirectToPoint',
                            label: t('Go to point'),
                            className: 'btn-primary btn-tertiary',
                            /**
                             * is this button visible ?
                             *
                             * @returns {boolean}
                             */
                            show: () => smHasPermission('read point record'),
                            icon: <FontAwesomeIcon icon={faFolderTree}/>,
                            /**
                             * toggle modal
                             *
                             * @param process
                             *
                             * @returns {void}
                             */
                            onClick: (process: any) => {
                                router.push(`/point/${process.point}/`)
                            }
                        },
                    ]}
                />
            </AccordionItemPanel>

            {/* close process modal */}
            <Modal
                title={t("Close household process")}
                isOpen={isModalOpen}
                onRequestClose={() => setIsModalOpen(false)}
                size={"small"}
            >
                <div className="card-serveys-actions">
                    {t("Are you sure?")}
                    <div className="card-administrative-division-actions">
                        <button type="button" className="btn btn-primary green"
                                onClick={() => setIsModalOpen(false)}>{t("Cancel")}</button>
                        <button type="button" className="btn btn-primary btn-red" onClick={() => handleCloseProcess(processToClose)}>{t("Close Process")}</button>
                    </div>
                </div>
            </Modal>
        </>
    )
}
