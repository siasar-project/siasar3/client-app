/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {useEffect, useState} from "react";
import Pager from "@/components/common/Pager";
import CardContainer from "@/components/Cards/CardContainer";
import {AccordionItemPanel} from "react-accessible-accordion";
import t from "@/utils/i18n";
import {faCalculator, faEdit, faEye} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import router from "next/router";
import {eventManager, Events, HouseholdEvents, StatusEventLevel} from "@/utils/eventManager";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import {dmGetUserHouseholds} from "@/utils/dataManager/Household";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {smHasPermission} from "@/utils/sessionManager";
import Modal from "@/components/common/Modal";
import {dmRemoveFormRecord} from "@/utils/dataManager/FormRecordWrite";

/**
 * Renders card container and pager for household surveys
 *
 * @returns {JSX.Element}
 */
export default function HouseholdSurveyCards() {
    // List state.
    const [userHouseholds, setUserHouseholds] = useState([])
    // Pager state.
    const [userPage, setUserPage] = useState(1)
    // Is loading, used to init data load.
    const [isLoadingHouseholds, setIsLoadingHouseholds] = useState(false);
    // if not null, show only surveys belonging to this process
    const [filterByProcessID, setFilterByProcessID] = useState(lcmGetItem('household_process_filter', null));
    const [processMeta, setProcessMeta] = useState<AdmDivisionMetaInterface | null>(null);
    // Only display my households.
    const [displayOnlyMine, setDisplayOnlyMine] = useState<boolean>(lcmGetItem('households_owned_filter', true));
    // remove household modal state
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [householdToRemove, setHouseholdToRemove] = useState()
    const [listedItems, setListedItems] = useState(0);

    eventManager.on(
        Events.HOUSEHOLD_EVENT,
        (data) => {
            switch (data.event) {
                default:
                    console.log("SOMETHING WENT WRONG");
                    break;

                case HouseholdEvents.SURVEY_ADDED:
                case HouseholdEvents.SURVEY_DELETED:
                case HouseholdEvents.PROCESS_CLOSED:
                    setIsLoadingHouseholds(true);
                    break;

                case HouseholdEvents.FILTER_SURVEYS:
                    if (filterByProcessID === data.processId) {
                        setFilterByProcessID(null)
                        lcmSetItem('household_process_filter', null);
                        setProcessMeta(null)
                        setIsLoadingHouseholds(true);
                    } else {
                        setFilterByProcessID(data.processId);
                        lcmSetItem('household_process_filter', data.processId);
                        setProcessMeta(data.processMeta)
                        setIsLoadingHouseholds(true);
                    }
                    break;
            }
        }
    )

    useEffect(() => {
        return () => {eventManager.remove(Events.HOUSEHOLD_EVENT)}
    }, [])

    /**
     * load user households.
     */
    useEffect(() => {
        if (isLoadingHouseholds) {
            // Load user households list.
            dmGetUserHouseholds(userPage, filterByProcessID ?? "", displayOnlyMine).then((data) => {
                setUserHouseholds(data);
                setListedItems(data.length);
            })
        }
    }, [isLoadingHouseholds])

    /**
     * user households loaded.
     */
    useEffect(() => {
        setIsLoadingHouseholds(false);
    }, [userHouseholds])

    /**
     * userPage changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (userPage === 0) {
            // Page 0 is not valid, must be 1.
            setUserPage(1);
        } else {
            setIsLoadingHouseholds(true);
        }
    }, [userPage])

    /**
     * Update user households from pager.
     *
     * @param state
     * @param state.page
     */
    const paginateHouseholds = (state: { page: number }) => {
        setUserPage(state.page);
    }

    /**
     * redirect to household/{processId}/{recordId}
     *
     * @param data
     */
    const handleViewItem = (data: any) => {
        router.push(`/household/${data.field_household_process.value}/${data.id}`)
    }

    /**
     * redirect to household/{processId}/{recordId}/edit
     *
     * @param data
     */
    const handleEditItem = (data: any) => {
        router.push(`/household/${data.field_household_process.value}/${data.id}/edit`)
    }

    /**
     * Get Division name and path.
     *
     * @param meta {AdmDivisionMetaInterface}
     * @returns {string}
     */
    const getAdministrativeDivisionBreadcrumb = (meta: AdmDivisionMetaInterface) => {
        let resp = meta.name;
        if (meta.parent) {
            resp = getAdministrativeDivisionBreadcrumb(meta.parent) + " / " + resp;
        }
        return resp;
    }

    // let surveys: any[] = userHouseholds;
    // if (filterByProcessID) {
    //     surveys = surveys.filter(sur => sur.field_household_process.value === filterByProcessID)
    // }

    /**
     * Remove household
     *
     * @param data {any}
     */
    const handleRemoveHouseHold = (data: any) => {
        const ref = {
            formId: 'form.community.household',
            id: data.id,
            data: data
        };
        setIsModalOpen(false);
        eventManager.dispatch(
            Events.STATUS_ADD,
            {
                level: StatusEventLevel.WARNING,
                title: t('Removing household...'),
                message: '',
                isPublic: true
            }
        );
        dmRemoveFormRecord(ref).then((res: any) => {
            setIsLoadingHouseholds(true);
            // trigger update for household surveys
            eventManager.dispatch(
                Events.HOUSEHOLD_EVENT,
                { event: HouseholdEvents.SURVEY_DELETED, processId: data.field_household_process.value }
            );
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.SUCCESS,
                    title: t('Household removed'),
                    message: '',
                    isPublic: true
                }
            );
        }).catch((info: any) => console.log(info));
        // dmCloseHouseholdProcess(data.id).then(res => {
        //     setIsLoadingProcesses(true)
        //     setIsModalOpen(false)
        //     eventManager.dispatch(
        //         Events.STATUS_ADD,
        //         {
        //             level: StatusEventLevel.SUCCESS,
        //             title: t('Household process closed correctly'),
        //             message: t("Household process @process has been closed and will no longer receive new surveys", {"@process": data.administrativeDivision.name}),
        //             isPublic: true
        //         }
        //     )
        //     // trigger update for household surveys
        //     eventManager.dispatch(
        //         Events.HOUSEHOLD_EVENT,
        //         { event: HouseholdEvents.PROCESS_CLOSED, processId: data.id }
        //     );
        // }).catch((info) => console.log(info));
    }

    return (
        <AccordionItemPanel>
            { processMeta && <h3>{getAdministrativeDivisionBreadcrumb(processMeta)}</h3> }
            <label className={'form-item-type-radio'}>
                <input
                    type={'checkbox'}
                    name={'show-all'}
                    key={'show-all'}
                    id={'show-all'}
                    value={"1"}
                    checked={displayOnlyMine}
                    defaultChecked={true}
                    onChange={(e:any) => {
                        let value:boolean = !displayOnlyMine;
                        lcmSetItem('households_owned_filter', value);
                        setDisplayOnlyMine(value);
                        setUserPage(0);
                    }}
                    className="form-check-input"
                    disabled={isLoadingHouseholds}
                />
                <span>{t("Display only my households.")}</span>
            </label>
            <Pager page={userPage} itemsAmount={listedItems} onChange={paginateHouseholds} isLoading={isLoadingHouseholds} />
            <CardContainer
                enableList
                cardType={"CardHouseholdSurvey"}
                cards={userHouseholds}
                isLoading={isLoadingHouseholds}
                buttons={[
                    {
                        id: 'btnView',
                        label: t('View'),
                        className: 'btn-tertiary',
                        /**
                         * is this button visible ?
                         *
                         * @param data {any} Process data.
                         *
                         * @returns {boolean}
                         */
                        show: (data: any): boolean => true,
                        icon: <FontAwesomeIcon icon={faEye} />,
                        onClick: handleViewItem
                    },
                    {
                        id: 'btnEdit',
                        label: t('Edit'),
                        className: 'btn-primary',
                        /**
                         * is this button visible ?
                         *
                         * @param data {any}
                         *
                         * @returns {boolean}
                         */
                        show: (data: any): boolean => {
                            if (!data.process.is_open) {
                                return false;
                            }
                            return data.field_finished === '0' || data.field_finished.value === '0';
                        },
                        icon: <FontAwesomeIcon icon={faEdit} />,
                        onClick: handleEditItem
                    },
                    {
                        id: 'btnRemoveHousehold',
                        label: t('Remove'),
                        className: 'btn-primary btn-red',
                        /**
                         * is this button visible ?
                         *
                         * @param data {any} Process data.
                         *
                         * @returns {boolean}
                         */
                        show: (data: any): boolean => {
                            if (!data.process.is_open) {
                                return false;
                            }
                            return !dmIsOfflineMode() && smHasPermission('delete household record');
                        },
                        icon: <FontAwesomeIcon icon={faCalculator}/>,
                        /**
                         * toggle modal
                         *
                         * @param process
                         *
                         * @returns {void}
                         */
                        onClick: (process: any) => {
                            setHouseholdToRemove(process)
                            setIsModalOpen(true)
                        }
                    }
                ]}
            />

            {/* remove household modal */}
            <Modal
                title={t("Remove household")}
                isOpen={isModalOpen}
                onRequestClose={() => setIsModalOpen(false)}
                size={"small"}
            >
                <div className="card-serveys-actions">
                    {t("Are you sure?")}
                    <div className="card-administrative-division-actions">
                        <button type="button" className="btn btn-primary green"
                                onClick={() => setIsModalOpen(false)}>{t("Cancel")}</button>
                        <button type="button" className="btn btn-primary btn-red" onClick={() => handleRemoveHouseHold(householdToRemove)}>{t("Remove household")}</button>
                    </div>
                </div>
            </Modal>
        </AccordionItemPanel>
    )
}
