/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import * as React from "react";

interface ReferenceProps {
    languages: any;
    data: any;
    onChange: any;
}

/**
 * @param {ReferenceProps} props
 * @returns {any}
 */
export function LanguageSetting(props: ReferenceProps) {
    const {languages, data, onChange} = props;

    /**
     * Render all languages as options.
     *
     * @returns {React.Component}
     */
    const renderOptions = () => {
        let options: any[] = [];

        for (var key in languages) {
            options.push(
                <option key={languages[key].id} value={languages[key].id}>
                    {t(languages[key].language)}
                </option>);
        }

        return options;
    }

    return (
        <>
            <div className={"country_main_language"}>
                <label className={'country_main_language'}>{t('Select the main language')}</label>
                <select
                    id={'country_main__language'}
                    value={data?.id}
                    onChange={onChange}
                >
                {renderOptions()}
                </select>
            </div>
        </>
    )
}
