/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import * as React from "react";

interface ReferenceProps {
    unit_id: string;
    data: any;
    onChange: any;
}

/**
 * @param {ReferenceProps} props
 * @returns {any}
 */
export function UnitSetting(props: ReferenceProps) {
    const {unit_id, data, onChange} = props;

    /**
     * Render all unit as options.
     *
     * @returns {React.Component}
     */
    const renderOptions = () => {
        let options: any[] = [];
        let allUnits = data['all'];

        for (var key in allUnits) {
            options.push(
                <option key={key} value={allUnits[key].id}>
                    {allUnits[key].symbol ? '['+allUnits[key].symbol+'] ' : ''}{t(allUnits[key].label)}
                </option>);
        }

        return options;
    }

    /** 
     * Render all unit as options.
     *
     * @returns {React.Component}
     */
    const renderCheckboxes = () => {
        let checkboxes: any[] = [];
        let allUnits = data['all'];
        let countryUnits = data['country'];
        for (var key in allUnits) {
            checkboxes.push(
                <label 
                    key={'country__other__' + unit_id + '__' + key + '__label'} 
                    className="unit">
                    <input 
                        type={"checkbox"}
                        key={'country__other__' + unit_id + '__' + key} 
                        id={'country__other__' + unit_id + '__' + key}
                        checked={countryUnits.filter((item:any) => {
                            if (item) {
                                return item.id === key;
                            }

                            return false;
                        }).length !== 0}

                        onChange={onChange}
                    />
                    <span>{allUnits[key].symbol ? '['+allUnits[key].symbol+'] ' : ''}{t(allUnits[key].label)}</span>
                </label>
            );            
        }

        return checkboxes;
    }
    return (
        <>
            <div className={"country_main_unit " + unit_id}>
                <label className={'country_main_unit ' + unit_id}>
                    {t('Select a main unit')}
                </label>
                <select
                    id={'country__main__' + unit_id}
                    value={data.main?.id}
                    onChange={onChange}
                >
                {renderOptions()}
                </select>
            </div>

            <br />

            <div className={"country_others " + unit_id}>
                <label className={'country_others ' + unit_id}>
                    {t('Select other units')}
                </label>
                <fieldset>
                    <div className="units-wrapper">
                        {renderCheckboxes()}
                    </div>
                </fieldset>
            </div>
        </>
    )
}
