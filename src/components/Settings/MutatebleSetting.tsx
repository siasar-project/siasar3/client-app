/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";

const fieldTypeList = {
    disabled: {label: "Disabled"},
    boolean: {label: "Boolean"},
    radio_boolean: {label: "Radio Boolean"},
    select: {label: "Select"},
    radio_select: {label: "Radio Select"},
    decimal: {label: "Decimal"},
    integer: {label: "Integer"},
    long_text: {label: "Long Text"},
    short_text: {label: "Short Text"},
};

interface ReferenceProps {
    form_id: string;
    fieldsMutateble: any;
    formStructure: any;
    data: any;
    onChange: any; 
    onInvalid: any;
}

/**
 * @param {ReferenceProps} props
 * @returns {any}
 */
export function MutatebleSetting(props: ReferenceProps) {
    const {form_id, fieldsMutateble, formStructure, data, onChange, onInvalid} = props;

    /**
     * Render props for all field type.
     *
     * @returns {React.Component}
     */
    const renderFieldTypeOptions = () => {
        let options: any[] = [];

        for (var key in fieldTypeList) {
            options.push(<option key={key} value={key}>{t(fieldTypeList[key as keyof typeof fieldTypeList].label)}</option>);
        }
        return options;
    }

    /**
     * Render depend props for boolean field type.
     *
     * @param field_id
     * @returns {React.Component}
     */
    const renderProps = (field_id: string) => {
        let field_type = data[field_id].mutate_to;

        return (
            <>
                <div className="prop">
                    <label key={form_id + "__" + field_id + "__label_label"}>
                        {t('Label')}
                    </label>
                    <input
                        type="text"
                        id={form_id + "__" + field_id + "__label"}
                        onChange={onChange}
                        onInvalid={onInvalid}
                        required={true}
                        aria-required={true}
                        value={data[field_id].label ?? ''}
                    />
                </div>
                <div className="prop">
                    <label key={form_id + "__" + field_id + "__description_label"}>
                        {t('Description')}
                    </label>
                    <input
                        type="text"
                        id={form_id + "__" + field_id + "__description"}
                        onChange={onChange}
                        value={data[field_id].description ?? ''}
                    />
                </div>
                {['boolean', 'radio_boolean'].includes(field_type) ? (
                    renderBooleanProps(field_id)
                ) : (<></>) }
                {['select', 'radio_select'].includes(field_type) ? (
                    renderSelectProps(field_id)
                ) : (<></>) }
            </>
        )
    }

    /**
     * Render depend props for boolean field type.
     *
     * @param field_id
     * @returns {React.Component}
     */
    const renderBooleanProps = (field_id: string) => {
        return (
            <>
                <div className="prop">
                    <label key={form_id + "__" + field_id + "__true_label"}>
                        {t('True label')}
                    </label>
                    <input
                        type="text"
                        id={form_id + "__" + field_id + "__true_label"}
                        onChange={onChange}
                        value={data[field_id].true_label ?? ''}
                    />
                </div>
                <div className="prop">
                    <label key={form_id + "__" + field_id + "__false_label"}>
                        {t('False label')}
                    </label>
                    <input
                        type="text"
                        id={form_id + "__" + field_id + "__false_label"}
                        onChange={onChange}
                        value={data[field_id].false_label ?? ''}
                    />
                </div>
                <div className="prop">
                    <label key={form_id + "__" + field_id + "__show_labels_label"} className={'show_labels'}>
                        {t('Show option label')}
                    </label>
                    <input 
                        type={"checkbox"}
                        key={form_id + "__" + field_id + "__show_labels"}
                        id={form_id + "__" + field_id + "__show_labels"}
                        checked={data[field_id].show_labels ?? false}
                        onChange={onChange}
                    />
                    <span />
                </div>
            </>
        );
    }

    /**
     * Render depend prop for select field type.
     *
     * @param field_id
     * @returns {React.Component}
     */
    const renderSelectProps = (field_id: string): JSX.Element => {
        let fields: any[] = [];
        let options = data[field_id]['options_key_label'];

        for (let i = 0; i < options.length; i++) {
            fields.push(
                <>
                    <div className="key">
                        <label key={form_id + "__" + field_id + "__option_key_label_" + i}>
                            {t("Key")}
                        </label>
                        <input
                            type="text"
                            key={form_id + "__" + field_id + "__option_key__" + i}
                            id={form_id + "__" + field_id + "__option_key__" + i}
                            onChange={onChange}
                            onInvalid={onInvalid}
                            required={true}
                            aria-required={true}
                            value={options[i]?.key}
                        />
                    </div>
                    <div className="label">
                        <label key={form_id + "__" + field_id + "__option_value_label_" + i}>
                            {t("Label")}
                        </label>
                        <input
                            type="text"
                            key={form_id + "__" + field_id + "__option_value__" + i}
                            id={form_id + "__" + field_id + "__option_value__" + i}
                            onChange={onChange}
                            onInvalid={onInvalid}
                            required={true}
                            aria-required={true}
                            value={options[i]?.label}
                        />
                    </div>
                    <div className="buttons">
                        <button 
                            key={form_id + "__" + field_id + "__option_remove__" + i} 
                            id={form_id + "__" + field_id + "__option_remove__" + i} 
                            onClick={onChange}
                            className={'btn btn-primary btn-red'}
                        >
                            {t('Remove option')}
                        </button>
                    </div>
                </>
            )
        }

        return (
            <>
                <div className="prop">
                    <label key={form_id + "__" + field_id + "__options_label"}>
                        {t("Options")}
                    </label>
                    <div className="options-wrapper">
                        { fields.map((item, key) => (
                            <div className="field-option" key={form_id + "__" + field_id + '__option__' + key}>
                                {item}
                            </div>
                        ))}
                    </div>
                    <div>
                        <button 
                            key={form_id + "__" + field_id + "__add"} 
                            id={form_id + "__" + field_id + "__add"} 
                            onClick={onChange}
                            className={'btn btn-primary'}
                        ><FontAwesomeIcon icon={faPlusCircle} />
                            {t('Add new option')}
                        </button>
                    </div>
                </div>
            </>
        )
    }

    /**
     * return.
     */
  
    return (
        <div className={"mutatebles-fields-group"}>
            <h3 className={"title"}>{t('Mutatebles fields')}</h3>
            <ul className="country-mutateble-list">
                {fieldsMutateble.map((field_id: string, key: number) => {
                    let field_struct = formStructure.fields[field_id];
                    // Get value.
                    let field_type = 'disabled';
                    if (data && field_id in data) {
                        field_type = data[field_id]['mutate_to']
                    }
                    return (
                        <li key={form_id + '__' + field_id} className={'country-mutateble-list-li'}>
                            <label key={form_id + '__' + field_id + '__mutate_to_label'}>
                                {field_struct?.label} ({field_struct.settings.multivalued ? t("Multiple values") : "Single value"})
                            </label>
                            <select
                                id={form_id + '__' + field_id + '__mutate_to'}
                                value={field_type}
                                onChange={onChange}
                            >
                                {renderFieldTypeOptions()}
                            </select>

                            {field_type !== 'disabled' ? (
                                <fieldset>
                                    <div className="props-wrapper">
                                        {renderProps(field_id)}
                                    </div>
                                </fieldset>
                            ) : (<></>) }
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}
