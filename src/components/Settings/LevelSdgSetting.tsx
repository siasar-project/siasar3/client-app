/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import * as React from "react";

interface ReferenceProps {
    form_id: string;
    formStructure: any;
    data: any;
    onChange: any;
}

/**
 * @param props
 * @returns {React.Component}
 */
export function LevelSdgSetting(props: ReferenceProps) {
    const {form_id, formStructure, data, onChange} = props;

    return (
        <>
            <div className="level">
                <label className={'country_level_label'}>
                    {t('Select a level')}
                </label>
                <select
                    id={'country_level__' + form_id}
                    value={data.level}
                    onChange={onChange}
                >
                    <option value={"1"}>{t("Level 1: Standard")}</option>
                    <option value={"2"}>{t("Level 2: Intermediate")}</option>
                    <option value={"3"}>{t("Level 3: Advanced")}</option>
                </select>
            </div>

            { formStructure.meta.allow_sdg ? (
                    <div className="sdg">
                        <label className={'country_sdg_label input-type-check'}>
                            <input 
                                type={"checkbox"}
                                key={'country_sdg__' + form_id}
                                id={'country_sdg__' + form_id} 
                                // value={label}
                                checked={data.sdg}
                                onChange={onChange}
                            />
                            <span>{t('Have SDG')}</span>
                        </label>
                        
                    </div>
            ) : ( <></> )}
        </>
    )
}
