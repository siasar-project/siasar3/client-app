/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import t from "@/utils/i18n";
import {PointInterface} from "@/objects/PointInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import Modal from "@/components/common/Modal"
import {DynForm} from "@/components/DynForm/DynForm"
import getPointTeamEditSchema from "../../Settings/dynforms/PointTeamEditSchema";
import ButtonsManager from "@/utils/buttonsManager";
import {eventManager, Events} from "@/utils/eventManager";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {dmUpdateFormPoint} from "@/utils/dataManager/FormRecordWrite";

/**
 * Component status.
 */
 interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point edit team.
 */
export default class PointEditTeam  extends React.Component<PropsInterface> {
    private formState: any;
    private loading = false;
    private isOpen = false;


    // /**
    //  * Constructor.
    //  *
    //  * @param props
    //  */
    // constructor(props:any) {
    //     super(props);

        
    // }

    /**
     * Open dialog to edit point team.
     */
    handleOpenModal() {
        this.formState = {field_team: (this.props.point.team ?? [])};

        this.isOpen = true;
        this.forceUpdate();
    }

    /**
     * Close modal with the edit point team.
     */
    handleCloseModal() {
        this.isOpen = false;
        this.forceUpdate()
    }
    
    /**
     * Handle component state.
     *
     * @param event
     */
    handleButtonSubmit(event: any) {
        this.loading = true;
        this.forceUpdate();

        dmUpdateFormPoint(
            {
                formId: "form.point",
                id: this.props.point.id, 
                data: this.formState,
            })
            .then((data: any) => {
                // Update point prop with team data.
                this.props.point.team = this.formState.field_team;
                this.isOpen = false;
                this.loading = false;
                eventManager.dispatch(Events.POINT_TEAM_CHANGED, data.data);
                this.forceUpdate();
            })
            .catch(() => {
                this.loading = false;
                this.forceUpdate();
            });
    }

    private formButtons = [
        <input type="submit" value={t("Save")} key="button-save-dialog" className={"btn btn-primary green"}/>,
        <input type="button" value={t("Cancel")} key="button-cancel-dialog" className={"btn btn-primary btn-red"} onClick={this.handleCloseModal.bind(this)}/>,
    ]

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    render() {
        if (!this.props.buttons.haveButton('btn-edit-team'))
        {
            this.props.buttons.addButton({
                id: 'btn-edit-team',
                icon: <FontAwesomeIcon icon={faEdit} />,
                label: t("Edit Team"),
                className: "btn-primary",
                onClick: this.handleOpenModal.bind(this),
                /**
                 * Is this button visible?
                 *
                 * @returns {boolean}
                 */
                show: () => {return !dmIsOfflineMode();},
            });
        }

        return (            
            <Modal
                isOpen={this.isOpen} 
                onRequestClose={this.handleCloseModal.bind(this)}
                title={t("Edit Point Team")}
                size={"small"}
            >
                <DynForm
                    key={"edit-form"}
                    structure={getPointTeamEditSchema()}
                    buttons={this.formButtons}
                    onSubmit={this.handleButtonSubmit.bind(this)}
                    value={this.formState}
                    isLoading={this.loading}
                    withAccordion={false}
                /> 
            </Modal> 
        );
    }
}
