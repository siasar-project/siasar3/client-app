/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import t from "@/utils/i18n";
import {PointInterface} from "@/objects/PointInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faScroll} from "@fortawesome/free-solid-svg-icons";
import Modal from "@/components/common/Modal"
import {DynForm} from "@/components/DynForm/DynForm"
import ButtonsManager from "@/utils/buttonsManager";
import {dmGetPointHistory} from "@/utils/dataManager";
import getPointHistorySchema from "../../Settings/dynforms/PointHistorySchema";

/**
 * Component status.
 */
 interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point edit team.
 */
export default class PointHistory extends React.Component<PropsInterface> {
    private formState: any;
    private loading = false;
    private isOpen = false;

    /**
     * Hide loading.
     */
    componentDidMount() {
        dmGetPointHistory(this.props.point.id || '')
            .then((list:any) => {
                let values = list.map((point:any) => {
                    return {value: point.id, meta: point};
                });
                this.formState = {field_points: values};
                this.loading = false;
            })
            .catch(() => {
                this.loading = false;
            });
        this.loading = true;
    }

    /**
     * Open dialog to edit point team.
     */
    handleOpenModal() {
        // this.formState = {field_team: (this.props.point.team ?? [])};

        this.isOpen = true;
        this.forceUpdate();
    }

    /**
     * Close modal with the edit point team.
     */
    handleCloseModal() {
        this.isOpen = false;
        this.forceUpdate()
    }

    private formButtons = [
        <input type="button" value={t("Close")} key="button-cancel-dialog" className={"btn btn-primary btn-red"} onClick={this.handleCloseModal.bind(this)}/>,
    ]

    /**
     * Render component content only, not editable.
     *
     * @returns {Component}
     */
    render() {
        if (this.loading) {
            return <></>;
        }

        if (!this.props.buttons.haveButton('btn-history'))
        {
            this.props.buttons.addButton({
                id: 'btn-history',
                icon: <FontAwesomeIcon icon={faScroll} />,
                label: t("History"),
                className: "btn-primary float-right",
                onClick: this.handleOpenModal.bind(this),
            });
        }

        return (            
            <Modal
                isOpen={this.isOpen} 
                onRequestClose={this.handleCloseModal.bind(this)}
                title={t("Point History")}
                size={"large"}
            >
                <DynForm
                    key={"edit-form"}
                    structure={getPointHistorySchema()}
                    buttons={this.formButtons}
                    onSubmit={() => {}}
                    value={this.formState}
                    isLoading={this.loading}
                    withAccordion={false}
                /> 
            </Modal> 
        );
    }
}
