/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import t from "@/utils/i18n";
import {PointInterface} from "@/objects/PointInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faDivide} from "@fortawesome/free-solid-svg-icons";
import Modal from "@/components/common/Modal"
import ButtonsManager from "@/utils/buttonsManager";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import Loading from "@/components/common/Loading";
import ActionButtons from "../Buttons/ActionButtons";
import {dmReplanPoint} from "@/utils/dataManager/FormRecordWrite";

/**
 * Component status.
 */
 interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point edit team.
 */
export default class PointDefusion extends React.Component<PropsInterface> {
    private loading = false;
    private isOpen = false;
    private saving = false;
    // Action buttons.
    private modalButtons: ButtonsManager = new ButtonsManager;
    // Option list control.
    private freeCommunities: any[] = [];
    private freeWSProviders: any[] = [];
    private freeWSSystems: any[] = [];
    private linkedIds: string[] = [];

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: PropsInterface | Readonly<PropsInterface>) {
        super(props);

        // Modal bottoms.
        this.modalButtons.addButton({
            id: 'button-cancel-dialog',
            icon: <></>,
            label: t("Cancel"),
            className: "btn btn-primary btn-red",
            onClick: this.handleCloseModal.bind(this),
        });
        this.modalButtons.addButton({
            id: 'button-re-plan-dialog',
            icon: <FontAwesomeIcon icon={faDivide} />,
            label: t("Re-plan"),
            className: "btn btn-primary green",
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {
                let list = this.getSelectedIds();
                this.saving = true;
                this.forceUpdate();
                dmReplanPoint(this.props.point.id || "", list)
                    .then((data: any) => {
                        window.location.reload();
                    })
                    .catch((reason) => {
                        this.saving = false;
                        this.forceUpdate();
                    });
            },
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                let list = this.getSelectedItems();
                if (list.freeCommunities.length === 0) {
                    return false;
                }

                if (list.freeCommunities.length > 0 || list.freeWSProviders.length > 0 || list.freeWSSystems.length > 0) {
                    return this.props.point.community?.length !== list.freeCommunities.length;
                }

                return false;
            }
        });

        if (!this.props.buttons.haveButton('btn-re-plan'))
        {
            // Additional point page button.
            this.props.buttons.addButton({
                id: 'btn-re-plan',
                icon: <FontAwesomeIcon icon={faDivide} />,
                label: t("Re-plan"),
                className: "btn-primary float-right",
                onClick: this.handleOpenModal.bind(this),
                /**
                 * Is this button visible?
                 *
                 * @param data {any} Source data.
                 * @param btn {CardButtonsInterface} Current button.
                 *
                 * @returns {boolean}
                 */
                show: (data: any, btn: CardButtonsInterface) => {
                    return ("digitizing" === this.props.point.status_code?.toString());
                }
            });
        }

        if (this.props.point.inquiry_relations) {
            // Filter systems.
            this.freeWSSystems.length = 0;
            for (let i = 0; i < this.props.point.inquiry_relations?.systems.length; i++) {
                let system = this.props.point.inquiry_relations?.systems[i];
                if (system?.communities.length > 0 || system?.providers.length > 0) {
                    for (let j = 0; j < system?.communities.length; j++) {
                        this.linkedIds.push(system?.communities[j]);
                    }
                    for (let j = 0; j < system?.providers.length; j++) {
                        this.linkedIds.push(system?.providers[j]);
                    }
                } else {
                    system.selected = false;
                    this.freeWSSystems.push(system);
                }
            }
            // Filter communities.
            this.freeCommunities.length = 0;
            for (let i = 0; i < this.props.point.inquiry_relations?.communities.length; i++) {
                let community = this.props.point.inquiry_relations?.communities[i];
                let matches = this.linkedIds.filter((elem: any, index: any, self: string | any[]) => {
                    return elem === community?.id;
                });
                if (matches.length <= 0) {
                    community.selected = false;
                    this.freeCommunities.push(community);
                }
            }
            // Filter providers.
            this.freeWSProviders.length = 0;
            for (let i = 0; i < this.props.point.inquiry_relations?.providers.length; i++) {
                let provider = this.props.point.inquiry_relations?.providers[i];
                let matches = this.linkedIds.filter((elem: any, index: any, self: string | any[]) => {
                    return elem === provider?.id;
                });
                if (matches.length <= 0) {
                    provider.selected = false;
                    this.freeWSProviders.push(provider);
                }
            }
        }

        this.getSelectedItems.bind(this);
    }

    /**
     * Get selected item.
     *
     * @returns {{freeCommunities: string[], freeWSProviders: string[], freeWSSystems: string[]}}
     */
    getSelectedItems() {
        let list:{freeCommunities: string[], freeWSProviders: string[], freeWSSystems: string[]} = {
            freeCommunities: [],
            freeWSProviders: [],
            freeWSSystems: [],
        };
        for (let i = 0; i < this.freeCommunities.length; i++) {
            let item = this.freeCommunities[i];
            if (item.selected) {
                list.freeCommunities.push(item);
            }
        }
        for (let i = 0; i < this.freeWSProviders.length; i++) {
            let item = this.freeWSProviders[i];
            if (item.selected) {
                list.freeWSProviders.push(item);
            }
        }
        for (let i = 0; i < this.freeWSSystems.length; i++) {
            let item = this.freeWSSystems[i];
            if (item.selected) {
                list.freeWSSystems.push(item);
            }
        }

        return list;
    }

    /**
     * Get selected item IDs.
     *
     * @returns {{freeCommunities: string[], freeWSProviders: string[], freeWSSystems: string[]}}
     */
    getSelectedIds() {
        let list:{"form.community": string[], "form.wsprovider": string[], "form.wssystem": string[]} = {
            "form.community": [],
            "form.wsprovider": [],
            "form.wssystem": [],
        };
        for (let i = 0; i < this.freeCommunities.length; i++) {
            let item = this.freeCommunities[i];
            if (item.selected) {
                list["form.community"].push(item.id);
            }
        }
        for (let i = 0; i < this.freeWSProviders.length; i++) {
            let item = this.freeWSProviders[i];
            if (item.selected) {
                list["form.wsprovider"].push(item.id);
            }
        }
        for (let i = 0; i < this.freeWSSystems.length; i++) {
            let item = this.freeWSSystems[i];
            if (item.selected) {
                list["form.wssystem"].push(item.id);
            }
        }

        return list;
    }

    /**
     * Open dialog to edit point team.
     */
    handleOpenModal() {
        // this.formState = {field_team: (this.props.point.team ?? [])};
        this.isOpen = true;
        this.forceUpdate();
    }

    /**
     * Close modal with the edit point team.
     */
    handleCloseModal() {
        this.isOpen = false;
        this.forceUpdate()
    }

    /**
     * Handle checkbox changes.
     *
     * @param event
     */
    handleOptionChange(event: any) {
        let list:any[];
        switch (event.target.dataset.list) {
            case 'freeCommunities':
                list = this.freeCommunities;
                break;
            case 'freeWSProviders':
                list = this.freeWSProviders;
                break;
            case 'freeWSSystems':
                list = this.freeWSSystems;
                break;
            default:
                console.log('UNKNOWN event.target.dataset.list !!');
                return;
        }
        for (let i = 0; i < list.length; i++) {
            let item = list[i];
            if (item.id === event.target.dataset.id) {
                item.selected = !item.selected;
                break;
            }
        }
        this.forceUpdate();
    }

    /**
     * Transform a list of graph nodes to components.
     *
     * @param type The list name to use.
     * @param list The list to use.
     *
     * @returns {Array}
     */
    mapOptionList(type:string, list: any[]) {
        return list.map((item, key) => {
            return (
                <li className={type} key={"chk_" + item.id}>
                    <label className={'re-plan-input-type-check'}>
                        <input
                            type="checkbox"
                            name={"chk_" + item.id}
                            checked={item.selected}
                            onChange={this.handleOptionChange.bind(this)}
                            className="form-check-input"
                            data-id={item.id}
                            data-list={type}
                            disabled={this.saving}
                        />
                        <span className={type}> </span><span>{item.label}</span>
                    </label>
                </li>
            );
        })
    }

    /**
     * Render point content list with checkboxes.
     *
     * @returns {Component}
     */
    render() {
        if (this.loading) {
            return <></>;
        }

        return (            
            <Modal
                isOpen={this.isOpen} 
                onRequestClose={this.handleCloseModal.bind(this)}
                title={t("Point re-plan")}
                size={"large"}
            >
                <div className={"modal-re-plan"}>
                    <div>{t('Re-plan a point allow move inquiries into a new SIASAR point.')}</div>
                    <div>{t("Communities")}</div>
                    <ul>
                        {this.mapOptionList('freeCommunities', this.freeCommunities)}
                    </ul>
                    <div>{t("Water systems")}</div>
                    <ul>
                        {this.mapOptionList('freeWSSystems', this.freeWSSystems)}
                    </ul>
                    <div>{t("Water service providers")}</div>
                    <ul>
                        {this.mapOptionList('freeWSProviders', this.freeWSProviders)}
                    </ul>
                </div>
                {this.saving ? (
                    <Loading key={'loading_record'} />
                ) : (
                    <ActionButtons buttons={this.modalButtons} isLoading={this.props.isLoading}/>
                )}
            </Modal>
        );
    }
}
