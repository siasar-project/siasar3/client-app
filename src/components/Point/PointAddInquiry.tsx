/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import {PointInterface} from "@/objects/PointInterface";
import router from "next/router";
import {getDestinationQueryParameter} from "@/utils/siasar";
import {dmAddWspToPoint, dmAddWSystemToPoint} from "@/utils/dataManager/FormRecordWrite";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
}

/**
 * Point status breadcrumb.
 */
export default class PointAddInquiry extends React.Component<PropsInterface> {
    protected lockWS:boolean = false;
    protected lockWSP:boolean = false;

    /**
     * Add a new water service provider.
     */
    handleAddWSP() {
        if (this.lockWSP) {
            return;
        }
        this.lockWSP = true;
        dmAddWspToPoint(this.props.point.id || "", {"field_provider_type": "1"})
            .then((res: any) => {
                let destination = getDestinationQueryParameter({value: this.props.point.id, prefix: "/point/"});
                router.push(`/form.wsprovider/${res.id}/edit${destination}`);
            })
            .catch(() => {
                this.lockWSP = false;
            });
    }

    /**
     * Add a new water system.
     */
    handleAddSystem() {
        if (this.lockWS) {
            return;
        }
        this.lockWS = true;
        dmAddWSystemToPoint(this.props.point.id || "", {"field_have_distribution_network": true})
            .then((res: any) => {
                let destination = getDestinationQueryParameter({value: this.props.point.id, prefix: "/point/"});
                router.push(`/form.wssystem/${res.id}/edit${destination}`);
            })
            .catch(() => {
                this.lockWS = false;
            });
    }

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        return (
            <fieldset className={"point-add-inquiry-wrapper"}>
                <legend>
                    <span className={"title"}>{t("Add inquiry")}</span>
                </legend>
                <div className="form-actions">
                    <button
                        type="button"
                        key="button-add-wsp"
                        className={"btn button-service-provider"}
                        value={t("Service provider")}
                        disabled={this.lockWSP}
                        onClick={this.handleAddWSP.bind(this)}>
                        <span>{t("Add Service provider")}</span>
                    </button>
                    <button
                        type="button"
                        key="button-add-system"
                        className={"btn button-water-system"}
                        value={t("Water system")}
                        disabled={this.lockWS}
                        onClick={this.handleAddSystem.bind(this)}>
                        <span>{t("Add Water system")}</span>
                    </button>
                </div>
            </fieldset>
        );
    }
}
