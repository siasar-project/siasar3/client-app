/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import {PointInterface} from "@/objects/PointInterface";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faHouse} from "@fortawesome/free-solid-svg-icons";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
}

/**
 * Point status breadcrumb.
 */
export default class PointInformation extends React.Component<PropsInterface> {

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        let admDiv = this.props.point.administrative_division;
        let community = admDiv ? admDiv[0].name : '';
        let updated = new Date(this.props.point.updated ?? '');

        return (
            <div className={"point-information-wrapper"}>
                <div className="community">
                    <FontAwesomeIcon icon={faHouse} size="1x"/>
                    <span className="value">{community}</span>
                </div>
                <div className="updated">
                    <span className="label">{t('Updated')}</span>
                    <span className="value">{updated.toLocaleDateString()} - {updated.toLocaleTimeString()}</span>
                </div>
            </div>
        );
    }
}
