/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {PointInterface} from "@/objects/PointInterface";
import React from "react";
import PointEditTeam from "@/components/Point/PointEditTeam";
import ActionButtons from "@/components/Buttons/ActionButtons";
import ButtonsManager from "@/utils/buttonsManager";
import dynamic from "next/dynamic";
import PointListInquiries from "@/components/Point/PointListInquiries";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point page body in reviewing status.
 */
export default class PointBodyReviewing extends React.Component<PropsInterface> {
    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        /**
         * Resolver "ReferenceError: self is not defined".
         *
         * @see https://stackoverflow.com/a/66100185/6385708
         */
        const PointGraph = dynamic(() => import('@/components/Graph/PointGraph'), {
            ssr: false
        });

        return (
            <>
                <PointGraph
                    data={this.props.point.inquiry_relations || {communities: [], systems: [], providers: []}}
                    lockDiagram={true}
                    lockZoom={true}/>
                <PointListInquiries point={this.props.point} simple={false} isLoading={this.props.isLoading}/>
                <PointEditTeam point={this.props.point} isLoading={this.props.isLoading} buttons={this.props.buttons}/>
                <ActionButtons buttons={this.props.buttons} isLoading={this.props.isLoading}/>
            </>
        );
    }
}
