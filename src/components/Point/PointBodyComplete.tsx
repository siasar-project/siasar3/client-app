/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {PointInterface} from "@/objects/PointInterface";
import React from "react";
import PointEditTeam from "@/components/Point/PointEditTeam";
import ActionButtons from "@/components/Buttons/ActionButtons";
import ButtonsManager from "@/utils/buttonsManager";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point page body in complete status.
 */
export default class PointBodyComplete extends React.Component<PropsInterface> {
    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        return (
            <>
                <PointEditTeam point={this.props.point} buttons={this.props.buttons} isLoading={this.props.isLoading}/>
                <ActionButtons buttons={this.props.buttons} isLoading={this.props.isLoading}/>
            </>
        );
    }
}
