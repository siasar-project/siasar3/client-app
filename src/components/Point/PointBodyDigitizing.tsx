/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {PointInterface} from "@/objects/PointInterface";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faA} from "@fortawesome/free-solid-svg-icons";
import PointAddInquiry from "@/components/Point/PointAddInquiry";
import dynamic from "next/dynamic";
import {PointGraphInterface} from "@/objects/Graph/PointGraphInterface";
import PointListInquiries from "@/components/Point/PointListInquiries";
import PointEditTeam from "@/components/Point/PointEditTeam";
import ActionButtons from "@/components/Buttons/ActionButtons";
import ButtonsManager from "@/utils/buttonsManager";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point page body in planning status.
 */
export default class PointBodyDigitizing extends React.Component<PropsInterface> {
    // Point graph example.
    protected data: PointGraphInterface = {
        communities: [
            {id: "com1", label: "Community 1 Community 1 Community 1", color: 'white', icon: <FontAwesomeIcon icon={faA} color="red" size="lg"/>},
            {id: "com2", label: "Community 2", icon: <FontAwesomeIcon icon={faA} color="blue" size="lg"/>},
            // {id: "com3", label: "Community 3", icon: <FontAwesomeIcon icon={faA} color="gray" size="lg"/>},
            // {id: "com4", label: "Community 4"},
            // {id: "com5", label: "Community 5"},
            // {id: "com6", label: "Community 6", icon: <FontAwesomeIcon icon={faA} color="blue" size="lg"/>},
        ],
        systems: [
            {id: "sys1", label: "System 1", communities: ["com1", "com2"], providers: ["pro1", "pro2"]},
            // {id: "sys2", label: "System 2", communities: ["com2", "com3"], providers: ["pro1"]},
            // {id: "sys3", label: "System 3", communities: ["com2", "com4", "com6"], providers: ["pro1", "pro3"]},
            // {id: "sys4", label: "System 4", communities: ["com1", "com3", "com6"], providers: ["pro2", "pro3"]},
        ],
        providers: [
            {id: "pro1", label: "Provider 1"},
            {id: "pro2", label: "Provider 2"},
            // {id: "pro3", label: "Provider 3"},
        ],
    };

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        /**
         * Resolver "ReferenceError: self is not defined".
         *
         * @see https://stackoverflow.com/a/66100185/6385708
         */
        const PointGraph = dynamic(() => import('@/components/Graph/PointGraph'), {
            ssr: false
        });

        return (
            <>
                <PointAddInquiry point={this.props.point}/>
                <PointGraph
                    data={this.props.point.inquiry_relations || {communities: [], systems: [], providers: []}}
                    lockDiagram={true}
                    lockZoom={true}/>
                <PointListInquiries point={this.props.point} simple={false} isLoading={this.props.isLoading}/>
                <PointEditTeam point={this.props.point} buttons={this.props.buttons} isLoading={this.props.isLoading}/>
                <ActionButtons buttons={this.props.buttons} isLoading={this.props.isLoading}/>
            </>
        );
    }
}
