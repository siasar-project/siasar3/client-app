/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */


import {PointInterface} from "@/objects/PointInterface";
import React from "react";
import CardContainer from "@/components/Cards/CardContainer";
import t from "@/utils/i18n";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEye} from "@fortawesome/free-solid-svg-icons/faEye";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import router from "next/router";
import {dmGetPointInquiries} from "@/utils/dataManager";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import Modal from "@/components/common/Modal"
import {PointStatus} from "@/objects/PointStatus";
import {getDestinationQueryParameter} from "@/utils/siasar";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {
    dmRemoveCommunity,
    dmRemoveCommunityHouseholds,
    dmRemoveWSProvider,
    dmRemoveWSSystem
} from "@/utils/dataManager/FormRecordWrite";
import {faUsers} from "@fortawesome/free-solid-svg-icons";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
    simple: boolean
    isLoading: boolean
}

/**
 * Point page body in planning status.
 */
export default class PointListInquiries extends React.Component<PropsInterface, any> {
    protected urlDestination = router.query.destination;
    protected isLoading: boolean = true;
    protected listItems: any[] = [];
    // Record item to remove.
    protected itemFormId: string = "";
    // Record item type to remove.
    protected itemFormType: string = "";
    // Point where remove the item.
    protected formId: any = "";

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props:any) {
        super(props);

        this.state = {
            isOpen: false,
            isOpenRemoveHouseholds: false,
            title: "",
            message: "",
        };
    }

    /**
     * Open the Modal for confirmation message
     */
    showModal() {
        this.setState({ isOpen: true });
    };

    /**
     * Open the Modal for confirmation message
     */
    showModalRemoveHouseholds() {
        this.setState({ isOpenRemoveHouseholds: true });
    };

    /**
     * Close the Modal
     */
    hideModal(){
        this.setState({ isOpen: false });
    };

    /**
     * Close the Modal
     */
    hideModalRemoveHouseholds(){
        this.setState({ isOpenRemoveHouseholds: false });
    };

    /**
     * Modal's tittle
     * 
     * @param title 
     */
    confirmationTitle(title: string){
        this.setState({ title : title});
    }

    /**
     * Modal's message
     *
     * @param message
     */
    confirmationMessage(message: string){
        this.setState({ message : message});
    }

    /**
     * Return true if the record have any relation with other record in this point.
     *
     * @param record The record data.
     *
     * @returns {boolean}
     */
    hasReferences(record: any):boolean {
        switch (record.type) {
            case 'form.community':
                for (const systemsKey in this.props.point.inquiry_relations?.systems) {
                    let system:any = this.props.point.inquiry_relations?.systems[parseInt(systemsKey)];
                    for (let i = 0; i < system?.communities.length; i++) {
                        if (system?.communities[i] === record.id) {
                            return true;
                        }
                    }
                }
                break;
            case 'form.wssystem':
                for (const systemsKey in this.props.point.inquiry_relations?.systems) {
                    let system:any = this.props.point.inquiry_relations?.systems[parseInt(systemsKey)];
                    if (system?.id === record.id) {
                        return !(system?.providers.length === 0 && system?.communities.length === 0);
                    }
                }
                break;
            case 'form.wsprovider':
                for (const systemsKey in this.props.point.inquiry_relations?.systems) {
                    let system:any = this.props.point.inquiry_relations?.systems[parseInt(systemsKey)];
                    for (let i = 0; i < system?.providers.length; i++) {
                        if (system?.providers[i] === record.id) {
                            return true;
                        }
                    }
                }
                break;
        }

        return false;
    }

    /**
     * Hide loading.
     */
    componentDidMount() {
        dmGetPointInquiries(this.props.point.id || '')
            .then((list) => {
                this.listItems = list;
                this.isLoading = false;
                this.forceUpdate();
                this.formId = this.props.point.id;
            })
            .catch(() => {
                this.isLoading = false;
            });
    }

    /**
     * Handle view item.
     *
     * @param data Item data to view.
     */
    handleViewItem(data:any) {
        let subDestination = "/point/" + this.props.point.id + getDestinationQueryParameter({value: this.urlDestination});
        let destination = getDestinationQueryParameter({value: subDestination, uriEncode: true});
        router.push(`/${data.type}/${data.id}${destination}`);
    }

    /**
     * Handle edit item.
     *
     * @param data Item data to edit.
     */
    handleEditItem(data:any) {
        let subDestination = "/point/" + this.props.point.id + getDestinationQueryParameter({value: this.urlDestination});
        let destination = getDestinationQueryParameter({value: subDestination, uriEncode: true});
        router.push(`/${data.type}/${data.id}/edit${destination}`);
    }

    /**
     * Handle remove item.
     * 
     * @param event Item data to remove.
     */
    handleRemoveItem(event : any) {

        // Community ID.
        let sid : string = this.itemFormId;
        // Form type.
        let sType : any = this.itemFormType;
        // Point ID.
        let pid : any = this.formId;

        switch (sType) {
            case 'form.community':
                dmRemoveCommunity(pid, sid).then((ok: any) => {this.reloadAndAlert(ok);});
                break;
            case 'form.wssystem':
                dmRemoveWSSystem(pid, sid).then((ok: any) => {this.reloadAndAlert(ok);});
                break;
            case 'form.wsprovider':
                dmRemoveWSProvider(pid, sid).then((ok: any) => {this.reloadAndAlert(ok);});
                break;
        }
    }

    /**
     * Handle remove item.
     *
     * @param event Item data to remove.
     */
    handleRemoveHouseholds(event : any) {
        // Community ID.
        let sid : string = this.itemFormId;
        // Form type.
        let sType : any = this.itemFormType;
        // Point ID.
        let pid : any = this.formId;

        switch (sType) {
            case 'form.community':
                dmRemoveCommunityHouseholds(pid, sid).then((ok: any) => {this.reloadAndAlert(ok);});
                break;
        }
    }

    /**
     * After remove reload page and dispatch message event about the remove action.
     *
     * @param ok {boolean}
     */
    reloadAndAlert(ok: boolean) {
        // Force the update
        window.location.reload()

        if (ok) {
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.SUCCESS,
                    title: t('Removed'),
                    message: '',
                    isPublic: true,
                }

            );
        }
    }

    /**
     * Render inquiry cards.
     *
     * @returns {React.ReactElement}
     */
    renderCards() {
        return (
            <React.StrictMode>
                <CardContainer
                    enableList
                    cardType={"CardInquiry"}
                    cards={this.listItems}
                    isLoading={this.isLoading}
                    buttons={[
                        {
                            id: 'btnView',
                            label: t("View"),
                            className: "btn-tertiary",
                            /**
                             * Is this button visible?
                             *
                             * @param data {any} Source data.
                             * @param btn {CardButtonsInterface} Current button.
                             *
                             * @returns {boolean}
                             */
                            show: (data: any, btn: CardButtonsInterface) => {return true},
                            icon: <FontAwesomeIcon icon={faEye} />,
                            onClick: this.handleViewItem.bind(this)
                        },
                        {
                            id: 'btnEdit',
                            label: t("Edit"),
                            className: "btn-primary",
                            /**
                             * Is this button visible?
                             *
                             * @param data {any} Source data.
                             * @param btn {CardButtonsInterface} Current button.
                             *
                             * @returns {boolean}
                             */
                            show: (data: any, btn: CardButtonsInterface) => {
                                if (dmIsOfflineMode()) {
                                    if (data.field_status !== "draft") {
                                        return false;
                                    }
                                }
                                return (
                                    PointStatus.calculating.toLowerCase() !== this.props.point.status_code &&
                                    PointStatus.calculated.toLowerCase() !== this.props.point.status_code);
                            },
                            icon: <FontAwesomeIcon icon={faEdit} />,
                            onClick: this.handleEditItem.bind(this)
                        },
                        {
                            id: 'btnRemove',
                            label: t("Remove"),
                            className: "btn-primary btn-red",
                            /**
                             * Is this button visible?
                             *
                             * @param data {any} Source data.
                             *
                             * @returns {boolean}
                             */
                            show: (data: any) => {
                                if (dmIsOfflineMode()) {
                                    return false;
                                }
                                // We can remove drafts only.
                                if (data.field_status !== "draft") {
                                    return false;
                                }
                                // The point requires one community at least.
                                if (data.type === 'form.community') {
                                    if (this.props.point && this.props.point.inquiry_relations) {
                                        if (this.props.point.inquiry_relations?.communities?.length <= 1) {
                                            return false;
                                        }
                                    }
                                }
                                // Records with references can't be removed.
                                return !this.hasReferences(data);
                            },
                            icon: <FontAwesomeIcon icon={faTrash} />,
                            /**
                             * On mouse click.
                             *
                             * @param item {any}
                             */
                            onClick: (item:any) => {
                                this.itemFormId = item.id;
                                this.itemFormType = item.type;
                                this.confirmationTitle(t('Remove @id with label "@label"', {"@id": item.id, "@label": item.type_title}));
                                this.confirmationMessage(t('Remove @id with label "@label"', {"@id": item.id, "@label": item.type_title}));
                                this.showModal();
                            }
                        },
                        {
                            id: 'btnRemoveHouseholds',
                            label: t("Change to Without households"),
                            className: "btn-primary btn-purple",
                            /**
                             * Is this button visible?
                             *
                             * @param data {any} Source data.
                             *
                             * @returns {boolean}
                             */
                            show: (data: any) => {
                                if (dmIsOfflineMode()) {
                                    return false;
                                }
                                if ('draft' !== data.field_status) {
                                    return false;
                                }
                                if (undefined !== data.field_have_households) {
                                    return data.field_have_households;
                                }
                                return false;
                            },
                            icon: <FontAwesomeIcon icon={faUsers} />,
                            /**
                             * On mouse click.
                             *
                             * @param item {any}
                             */
                            onClick: (item:any) => {
                                this.itemFormId = item.id;
                                this.itemFormType = item.type;
                                this.confirmationTitle(t('Remove households from @id with label "@label"', {"@id": item.id, "@label": item.type_title}));
                                this.confirmationMessage(
                                    t('Change the community survey to not collect household data.') + '\n' +
                                    t('This change deletes the associated household surveys.')
                                );
                                this.showModalRemoveHouseholds();
                            }
                        }
                    ]}
                />
            </React.StrictMode>
        );
    }

    /**
     * Render inquiry as rows.
     *
     * @returns {React.ReactElement}
     */
    renderRows() {
        let items = [];
        for (let i = this.listItems.length - 1; i >= 0; i--) {
            let title:string = this.listItems[i].field_title;
            if (title === '') {
                title = this.listItems[i].field_region.meta.name;
            }
            items.push(
                <tr className={'type-'+this.listItems[i].type} key={"row-"+i}>
                    <td className="type">{this.listItems[i].type_title}</td>
                    <td className="status">{this.listItems[i].field_status}</td>
                    <td className="title">{title}</td>
                </tr>
            );
        }

        return (
            <table>
                <thead>
                <tr>
                    <th>{t('Type')}</th>
                    <th>{t('Status')}</th>
                    <th>{t('Title')}</th>
                </tr>
                </thead>
                <tbody>
                    {items}
                </tbody>
            </table>
        );
    }

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        return (
            <>
            <fieldset className={"point-list-inquiry-wrapper"}>
                <legend>
                    <span className={"title"}>{t("Inquiries")}</span>
                </legend>
                { this.props.simple ?
                    this.renderRows()
                :
                    this.renderCards()
                }
            </fieldset>
            <Modal
                title={this.state.title}
                onRequestClose={this.hideModal.bind(this)}
                isOpen={this.state.isOpen}
            >
                <div className="surveys-modal">
                    <span>{this.state.message}</span>
                    <span>{t('Are you sure?')}</span>

                    <div className="card-serveys-actions">
                        <button type="button" className="btn btn-primary" onClick={() =>this.hideModal()}>{t("Cancel")}</button>
                        <button type="button" className="btn btn-primary btn-red" onClick={() => this.handleRemoveItem(this.itemFormId)}>{t("Remove")}</button>
                    </div>
                </div>
            </Modal>
            <Modal
                title={this.state.title}
                onRequestClose={this.hideModalRemoveHouseholds.bind(this)}
                isOpen={this.state.isOpenRemoveHouseholds}
            >
                <div className="surveys-modal">
                    <div><pre>{this.state.message}</pre></div>
                    <span>{t('Are you sure?')}</span>
                    <span>{t('This changes can\'t be reverted.')}</span>

                    <div className="card-serveys-actions">
                        <button type="button" className="btn btn-primary btn-green" onClick={() =>this.hideModalRemoveHouseholds()}>{t("Cancel")}</button>
                        <button type="button" className="btn btn-primary btn-red" onClick={() => this.handleRemoveHouseholds(this.itemFormId)}>{t("Remove")}</button>
                    </div>
                </div>
            </Modal>
            </>
        );
    }
}
