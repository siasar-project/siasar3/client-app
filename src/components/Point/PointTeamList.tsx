/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import {PointInterface} from "@/objects/PointInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPeopleGroup} from "@fortawesome/free-solid-svg-icons";
import {eventManager, Events} from "@/utils/eventManager";
import UserTag from "@/components/common/UserTag";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
}

/**
 * Point status breadcrumb.
 */
export default class PointTeamList extends React.Component<PropsInterface> {
    private team:any = [];

    /**
     * Constructor
     *
     * @param props
     * @param context
     */
    constructor(props: PropsInterface, context: any) {
        super(props, context);
        this.team = this.props.point.team;
        eventManager.on(Events.POINT_TEAM_CHANGED, (data) => {
            this.team = data.team;
            this.forceUpdate();
        });
    }

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        let members:any[] = [];
        if (this.team) {
            for (let i = 0; i < this.team.length; i++) {
                let meta: any = this.team[i];
                members.push(<li className={"team-item"}>
                    <UserTag picture_url={meta.meta.gravatar} username={meta.meta.username}/>
                </li>);
            }
            if (0 === this.team.length) {
                members.push(<li className={"team-item"}>
                    {t('No team members found')}
                </li>);
            }
        }

        return (
            <div className={"point-team-list-wrapper"}>
                <div className="list">
                    <FontAwesomeIcon icon={faPeopleGroup} size="1x"/>
                    <span className="value">{t("Team")}</span>
                    <ul className={"team-list"}>
                        {members}
                    </ul>
                </div>
            </div>
        );
    }
}
