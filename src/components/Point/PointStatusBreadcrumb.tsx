/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import {PointStatus} from "@/objects/PointStatus";

/**
 * Component selected status.
 */
interface PropsInterface {
    status: PointStatus;
}

/**
 * Point status breadcrumb.
 */
export default class PointStatusBreadcrumb extends React.Component<PropsInterface> {

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        return (
            <div className={"point-status-breadcrumb-bar"}>
                {Object.entries(PointStatus).map((item: string[]) => {
                    if (item[0] === 'notdefined') {
                        return null;
                    }

                    return (
                        <span
                            className={"point-status-breadcrumb-item"+(this.props.status === item[0] ? " active" : "")}
                            key={item[0]}
                            id={"psbi-"+item[0]}>
                            {t(item[1])}
                        </span>
                    )
                })}
            </div>
        );
    }
}
