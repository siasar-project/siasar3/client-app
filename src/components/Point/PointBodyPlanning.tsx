/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {PointInterface} from "@/objects/PointInterface";
import React from "react";
import t from "@/utils/i18n";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faDownload} from "@fortawesome/free-solid-svg-icons";
import router from "next/router";
import PointListInquiries from "@/components/Point/PointListInquiries";
import ActionButtons from "@/components/Buttons/ActionButtons";
import ButtonsManager from "@/utils/buttonsManager";
import {dmUpdateFormPoint} from "@/utils/dataManager/FormRecordWrite";

/**
 * Component selected status.
 */
interface PropsInterface {
    point: PointInterface
    buttons: ButtonsManager
    isLoading: boolean
}

/**
 * Point page body in planning status.
 */
export default class PointBodyPlanning extends React.Component<PropsInterface> {
    // chksStatus
    protected chkStates = {chk1: false, chk2: false};

    /**
     * Handle visibility of button.
     * 
     * @returns {boolean}
     */
    handleVisibility() {
        return this.chkStates.chk1 && this.chkStates.chk2;
    }

    /**
     * Handle update button click.
     */
    handleUpdate() {
        dmUpdateFormPoint(
            {
                formId: "form.point",
                id: this.props.point.id, data: {field_chk_to_plan: {value: true}}
            })
            .then(() => {
                // Change state to digitalizing.
                dmUpdateFormPoint(
                    {
                        formId: "form.point",
                        id: this.props.point.id, data: {field_status: {value: "digitizing"}}
                    })
                    .then(() => {
                        // Reload page.
                        router.push('/point/list');
                    })
                    .catch(() => {});
            })
            .catch(() => {});
    }

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        if (!this.props.buttons.haveButton('btn-update'))
        {
            this.props.buttons.addButton({
                id: 'btn-update',
                icon: <FontAwesomeIcon icon={faDownload} />,
                label: t("Scheduled"),
                className: "btn-primary green",
                show: this.handleVisibility.bind(this),
                onClick: this.handleUpdate.bind(this),
            });
        }

        return (
            <>
                <div>
                    <ul className={"list-reset"}>
                        <li>
                            <label className={'form-item-type-radio'}>
                                <input
                                    type={'checkbox'}
                                    name={'chk1'}
                                    key={'chk1'}
                                    id={'chk1'}
                                    value={'1'}
                                    onChange={() => {
                                        this.chkStates.chk1 = !this.chkStates.chk1;
                                        this.forceUpdate();
                                    }}
                                    className="form-check-input"
                                />
                                <span>{t("Inform the community about the objective and scope of the survey")}</span>
                            </label>
                        </li>
                        <li>
                            <label className={'form-item-type-radio'}>
                                <input
                                    type={'checkbox'}
                                    name={'chk2'}
                                    key={'chk2'}
                                    id={'chk2'}
                                    value={'2'}
                                    onChange={() => {
                                        this.chkStates.chk2 = !this.chkStates.chk2;
                                        this.forceUpdate();
                                    }}
                                    className="form-check-input"
                                />
                                <span>{t("Schedule the field visit with the people to be interviewed")}</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <PointListInquiries point={this.props.point} simple={true} isLoading={this.props.isLoading}/>
                <ActionButtons buttons={this.props.buttons} isLoading={this.props.isLoading}/>
            </>
        );
    }
}
