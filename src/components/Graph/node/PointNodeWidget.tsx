/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import * as React from 'react';
import * as _ from 'lodash';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';
import { PointNodeModel } from '@/components/Graph/node/PointNodeModel';
import { PointPortLabel } from '@/components/Graph/port/PointPortLabelWidget';
import styled from '@emotion/styled';

namespace S {
	export const Node = styled.div<{ background: string; selected: boolean }>`
		background-color: ${(p) => p.background};
		font-family: sans-serif;
		color: white;
		border: solid 2px black;
		overflow: hidden;
		font-size: 11px;
		border: solid 2px ${(p) => (p.selected ? 'rgb(0,192,255)' : 'black')};
		width: 250px;
	`;

	export const Title = styled.div`
		background: transparent;
		color: black;
		font-size: 14px;
		display: flex;
		white-space: nowrap;
		justify-items: center;
	`;

	export const TitleIcon = styled.div`
		flex-grow: 0;
		margin: 5px 5px;
	`;

	export const TitleName = styled.div`
		flex-grow: 1;
		margin: 5px 5px;
	`;

	export const Ports = styled.div`
		position: absolute;
		top: calc(50% - 10px);
		left: 0;
		width: 100%;
		z-index: -1;
		display: flex;
		background-image: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2));
	`;

	export const PortsContainer = styled.div`
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		height: 20px;

		&:first-of-type {
			margin-right: 10px;
		}

		&:only-child {
			margin-right: 0px;
		}
	`;
}

export interface PointNodeProps {
	node: PointNodeModel;
	engine: DiagramEngine;
}

/**
 * Point node that models the PointNodeModel. It creates two columns
 * for both all the input ports on the left, and the output ports on the right.
 */
export class PointNodeWidget extends React.Component<PointNodeProps> {
	/**
	 * generatePort
	 * 
	 * @param port {any}
	 * 
	 * @returns {any}
	 */
	generatePort = (port:any) => {
		return <PointPortLabel engine={this.props.engine} port={port} key={port.getID()} />;
	};

	/**
	 * render
	 * 
	 * @returns {any}
	 */
	render() {
		return (
			<S.Node
				data-default-node-name={this.props.node.getOptions().name}
				selected={this.props.node.isSelected()}
				background={this.props.node.getOptions().color ?? ''}
			>
				<S.Title>
					{this.props.node.getOptions().icon ? <S.TitleIcon>{this.props.node.getOptions().icon}</S.TitleIcon> : <></>}
					<S.TitleName>{this.props.node.getOptions().name}</S.TitleName>
				</S.Title>
				<S.Ports>
					<S.PortsContainer>{_.map(this.props.node.getInPorts(), this.generatePort)}</S.PortsContainer>
					<S.PortsContainer>{_.map(this.props.node.getOutPorts(), this.generatePort)}</S.PortsContainer>
				</S.Ports>
			</S.Node>
		);
	}
}
