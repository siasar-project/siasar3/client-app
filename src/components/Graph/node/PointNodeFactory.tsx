/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import * as React from 'react';
import { PointNodeModel } from '@/components/Graph/node/PointNodeModel';
import { PointNodeWidget } from '@/components/Graph/node/PointNodeWidget';
import { AbstractReactFactory } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';

/**
 * PointNodeFactory
 */
export class PointNodeFactory extends AbstractReactFactory<PointNodeModel, DiagramEngine> {

	/**
	 * constructor
	 */
	constructor() {
		super('default');
	}

	/**
	 * generateReactWidget
	 *
	 * @param event {any}
	 * 
	 * @returns {any}
	 */
	generateReactWidget(event:any): JSX.Element {
		return <PointNodeWidget engine={this.engine} node={event.model} />;
	}

	/**
	 * generateModel
	 * 
	 * @param event {any}
	 * 
	 * @returns {any}
	 */
	generateModel(event:any): PointNodeModel {
		return new PointNodeModel();
	}
}
