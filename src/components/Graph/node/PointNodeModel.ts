/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import * as _ from 'lodash';
import { NodeModel, NodeModelGenerics, PortModelAlignment } from '@projectstorm/react-diagrams-core';
import { PointPortModel } from '@/components/Graph/port/PointPortModel';
import { BasePositionModelOptions, DeserializeEvent } from '@projectstorm/react-canvas-core';
import { ReactElement } from 'react';

/**
 * PointNodeModelOptions
 */
export interface PointNodeModelOptions extends BasePositionModelOptions {
	id?: string;
	name?: string;
	color?: string;
	icon?: ReactElement;
}

/**
 * PointNodeModelGenerics
 */
export interface PointNodeModelGenerics extends NodeModelGenerics {
	OPTIONS: PointNodeModelOptions;
}

/**
 * PointNodeModel
 */
export class PointNodeModel extends NodeModel<PointNodeModelGenerics> {
	protected portsIn: PointPortModel[];
	protected portsOut: PointPortModel[];

	/**
	 * constructor
	 * 
	 * @param name {string} 
	 * @param color {string}
	 */
	constructor(name: string, color: string);

	/**
	 * constructor
	 * 
	 * @param options {PointNodeModelOptions}
	 */	
	constructor(options?: PointNodeModelOptions);

	/**
	 * constructor
	 * 
	 * @param options {any}
	 * @param color {string}
	 */	
	constructor(options: any = {}, color?: string) {
		if (typeof options === 'string') {
			options = {
				name: options,
				color: color
			};
		}
		super({
			type: 'default',
			name: 'Untitled',
			color: 'rgb(0,192,255)',
			...options
		});
		this.portsOut = [];
		this.portsIn = [];
	}

	/**
	 * doClone
	 * 
	 * @param lookupTable {{}}
	 * @param clone {any}
	 */
	doClone(lookupTable: {}, clone: any): void {
		clone.portsIn = [];
		clone.portsOut = [];
		super.doClone(lookupTable, clone);
	}

	/**
	 * removePort
	 * 
	 * @param port {PointPortModel}
	 */
  removePort(port: PointPortModel): void {
		super.removePort(port);
		if (port.getOptions().in) {
			this.portsIn.splice(this.portsIn.indexOf(port), 1);
		} else {
			this.portsOut.splice(this.portsOut.indexOf(port), 1);
		}
	}

	/**
	 * addPort
	 * 
	 * @param port {T}
	 *
	 * @returns {any}
	 */
  addPort<T extends PointPortModel>(port: T): T {
		super.addPort(port);
		if (port.getOptions().in) {
			if (this.portsIn.indexOf(port) === -1) {
				this.portsIn.push(port);
			}
		} else {
			if (this.portsOut.indexOf(port) === -1) {
				this.portsOut.push(port);
			}
		}
		return port;
	}

	/**
	 * addInPort
	 * 
	 * @param label {string}
	 * @param after {boolean}
	 * 
	 * @returns {any}
	 */
	addInPort(label: string, after = true): PointPortModel {
		const p = new PointPortModel({
			in: true,
			name: label,
			label: label,
			alignment: PortModelAlignment.LEFT
		});
		if (!after) {
			this.portsIn.splice(0, 0, p);
		}
		return this.addPort(p);
	}

	/**
	 * addOutPort
	 * 
	 * @param label {string}
	 * @param after {boolean}
	 * 
	 * @returns {any}
	 */
	addOutPort(label: string, after = true): PointPortModel {
		const p = new PointPortModel({
			in: false,
			name: label,
			label: label,
			alignment: PortModelAlignment.RIGHT
		});
		if (!after) {
			this.portsOut.splice(0, 0, p);
		}
		return this.addPort(p);
	}

	/**
	 * deserialize
	 * 
	 * @param event {DeserializeEvent}
	 */
	deserialize(event: DeserializeEvent<this>) {
		super.deserialize(event);
		this.options.name = event.data.name;
		this.options.color = event.data.color;
		this.portsIn = _.map(event.data.portsInOrder, (id) => {
			return this.getPortFromID(id);
		}) as PointPortModel[];
		this.portsOut = _.map(event.data.portsOutOrder, (id) => {
			return this.getPortFromID(id);
		}) as PointPortModel[];
	}

	/**
	 * serialize
	 * 
	 * @returns {any}
	 */
	serialize(): any {
		return {
			...super.serialize(),
			name: this.options.name,
			color: this.options.color,
			portsInOrder: _.map(this.portsIn, (port) => {
				return port.getID();
			}),
			portsOutOrder: _.map(this.portsOut, (port) => {
				return port.getID();
			})
		};
	}

	/**
	 * getInPorts
	 *
	 * @returns {any}
	 */
	getInPorts(): PointPortModel[] {
		return this.portsIn;
	}

	/**
	 * getOutPorts
	 *
	 * @returns {any}
	 */
	getOutPorts(): PointPortModel[] {
		return this.portsOut;
	}
}
