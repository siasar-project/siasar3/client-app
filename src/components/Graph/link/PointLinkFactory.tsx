/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import * as React from 'react';
import { PointLinkModel } from '@/components/Graph/link/PointLinkModel';
import { DefaultLinkWidget } from '@projectstorm/react-diagrams-defaults';
import styled from '@emotion/styled';
import { AbstractReactFactory } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';
import { css, keyframes } from '@emotion/react';

namespace S {
	export const Keyframes = keyframes`
		from {
			stroke-dashoffset: 24;
		}
		to {
			stroke-dashoffset: 0;
		}
	`;

	const selected = css`
		animation: ${Keyframes} 0.5s linear infinite;
	`;

	export const Path = styled.path<{ selected: boolean }>`
		stroke-dasharray: 3, 2;
		animation: ${Keyframes} 1.5s linear infinite;
		${(p) => p.selected && selected};
		fill: none;
		pointer-events: auto;
	`;
}

/**
 * PointLinkFactory
 */
export class PointLinkFactory<Link extends PointLinkModel = PointLinkModel> extends AbstractReactFactory<Link, DiagramEngine> {

	/**
	 * constructor
	 * 
	 * @param type {any}
	 */
	constructor(type = 'point') {
		super(type);
	}

	/**
	 * generateReactWidget
	 *
	 * @param event {any}
	 * 
	 * @returns {any}
	 */
	generateReactWidget(event:any): JSX.Element {
		return <DefaultLinkWidget link={event.model} diagramEngine={this.engine} />;
	}

	/**
	 * generateModel
	 *
	 * @param event {any}
	 * 
	 * @returns {any}
	 */
	generateModel(event:any): Link {
		return new PointLinkModel() as Link;
	}

	/**
	 * generateLinkSegment
	 *
	 * @param model {Link}
	 * @param selected {boolean}
	 * @param path {string}
	 * 
	 * @returns {any}
	 */
	generateLinkSegment(model: Link, selected: boolean, path: string) {
		return (
			<S.Path
				selected={selected}
				stroke={selected ? model.getOptions().selectedColor : model.getOptions().color}
				strokeWidth={model.getOptions().width}
				d={path}
			/>
		);
	}
}
