/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {
	LabelModel,
	LinkModel,
	LinkModelGenerics,
	LinkModelListener,
	PortModel,
	PortModelAlignment
} from '@projectstorm/react-diagrams-core';
import { DefaultLabelModel } from '@projectstorm/react-diagrams-defaults';
import { BezierCurve } from '@projectstorm/geometry';
import { BaseEntityEvent, BaseModelOptions, DeserializeEvent } from '@projectstorm/react-canvas-core';

/**
 * PointLinkModelListener
 */
export interface PointLinkModelListener extends LinkModelListener {
	//@ts-ignore;
	colorChanged?(event: BaseEntityEvent<PointLinkModel> & { color: null | string	}): void;
	//@ts-ignore;
	widthChanged?(event: BaseEntityEvent<PointLinkModel> & { width: 0 | number }): void;
}

/**
 * PointLinkModelOptions
 */
export interface PointLinkModelOptions extends BaseModelOptions {
	width?: number;
	color?: string;
	selectedColor?: string;
	curvyness?: number;
	type?: string;
	testName?: string;
}

/**
 * PointLinkModelGenerics
 */
export interface PointLinkModelGenerics extends LinkModelGenerics {
	LISTENER: PointLinkModelListener;
	OPTIONS: PointLinkModelOptions;
}

/**
 * PointLinkModel
 */
export class PointLinkModel extends LinkModel<PointLinkModelGenerics> {

	/**
	 * constructor
	 * 
	 * @param options {PointLinkModelOptions} 
	 */
	constructor(options: PointLinkModelOptions = {}) {
		super({
			type: 'point',
			width: options.width || 3,
			color: options.color || '#287FB8',
			selectedColor: options.selectedColor || 'rgb(0,192,255)',
			curvyness: 50,
			...options
		});
	}

	/**
	 * constructor
	 * 
	 * @param port {PortModel}
	 * 
	 * @returns {[number, number]}
	 */	
	calculateControlOffset(port: PortModel): [number, number] {
		let curvyness = this.options.curvyness ?? 0;
		if (port.getOptions().alignment === PortModelAlignment.RIGHT) {
			return [curvyness, 0];
		} else if (port.getOptions().alignment === PortModelAlignment.LEFT) {
			return [-curvyness, 0];
		} else if (port.getOptions().alignment === PortModelAlignment.TOP) {
			return [0, -curvyness];
		}
		return [0, curvyness];
	}

	/**
	 * constructor
	 * 
	 * @returns {string | undefined}
	 */	
	getSVGPath(): string | undefined {
		if (this.points.length == 2) {
			const curve = new BezierCurve();
			curve.setSource(this.getFirstPoint().getPosition());
			curve.setTarget(this.getLastPoint().getPosition());
			curve.setSourceControl(this.getFirstPoint().getPosition().clone());
			curve.setTargetControl(this.getLastPoint().getPosition().clone());

			if (this.sourcePort) {
				curve.getSourceControl().translate(...this.calculateControlOffset(this.getSourcePort()));
			}

			if (this.targetPort) {
				curve.getTargetControl().translate(...this.calculateControlOffset(this.getTargetPort()));
			}
			return curve.getSVGCurve();
		}
	}

	/**
	 * serialize
	 * 
	 * @returns {any}
	 */	
	serialize() {
		return {
			...super.serialize(),
			width: this.options.width,
			color: this.options.color,
			curvyness: this.options.curvyness,
			selectedColor: this.options.selectedColor
		};
	}

	/**
	 * deserialize
	 * 
	 * @param event {DeserializeEvent}
	 */	
	deserialize(event: DeserializeEvent<this>) {
		super.deserialize(event);
		this.options.color = event.data.color;
		this.options.width = event.data.width;
		this.options.curvyness = event.data.curvyness;
		this.options.selectedColor = event.data.selectedColor;
	}
	
	/**
	 * addLabel
	 * 
	 * @param label {LabelModel | string}
	 * 
	 * @returns {any}
	 */	
	addLabel(label: LabelModel | string) {
		if (label instanceof LabelModel) {
			return super.addLabel(label);
		}
		let labelOb = new DefaultLabelModel();
		labelOb.setLabel(label);
		return super.addLabel(labelOb);
	}

	/**
	 * setWidth
	 * 
	 * @param width {number}
	 */	
	setWidth(width: number) {
		this.options.width = width;
		this.fireEvent({ width }, 'widthChanged');
	}

	/**
	 * setColor
	 * 
	 * @param color {string}
	 */	
	setColor(color: string) {
		this.options.color = color;
		this.fireEvent({ color }, 'colorChanged');
	}
}
