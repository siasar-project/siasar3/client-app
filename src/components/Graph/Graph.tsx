/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import createEngine, { DiagramEngine, DiagramModel, NodeModel, LinkModel } from '@projectstorm/react-diagrams';
import * as React from 'react';
import { CanvasWidget } from '@projectstorm/react-canvas-core';
import { GraphInterface } from "@/objects/Graph/GraphInterface";
import Loading from "@/components/common/Loading";

interface IGraphProps {
    data: GraphInterface;
    lockDiagram?: boolean;
    lockZoom?: boolean;
}

interface IGraphState {
}

/**
 * Graph.
 */
class Graph extends React.Component<IGraphProps, IGraphState> {
    protected engine: DiagramEngine;
    protected diagram: DiagramModel;
    protected nodes: {[id: string]: NodeModel} = {};
    protected links: {[id: string]: LinkModel} = {};
    protected loading: boolean = true;
    protected classNames: string;

    /**
     * Init component.
     *
     * @param props
     */
    constructor(props: IGraphProps) {        
        super(props);

        // create an instance of the graph engine, locking the zoom for default if not indicated.
        this.engine = createEngine({
            registerDefaultPanAndZoomCanvasAction: false,
            registerDefaultZoomCanvasAction: !(this.props.lockZoom ?? true)
        });
        this.diagram = new DiagramModel();
        this.classNames = "";

        this.configurate = this.configurate.bind(this);
        this.buildNodesAndLinks = this.buildNodesAndLinks.bind(this);
        this.buildDiagram = this.buildDiagram.bind(this);
    }

    /**
     * Configure and load data before render the graph.
     */
    componentDidMount() {
        this.configurate();
        this.buildNodesAndLinks();
        this.buildDiagram();

        this.loading = false;
        this.forceUpdate();
    }

    /**
     * Configurate all components of graph.
     */
    configurate() {
        // Lock nodes, ports and links por default if not indicated.
        if (this.props.lockDiagram ?? true) {
            this.diagram.setLocked(true);
        }
    }

    /**
     * Create nodes and links.
     */
    buildNodesAndLinks() {   
    }

    /**
     * Add nodes and links to the diagram.
     */
    buildDiagram() {
        this.diagram.addAll(...Object.values(this.nodes), ...Object.values(this.links));
        this.engine.setModel(this.diagram);
    }

    /** 
     * Render the status notifications container.
     * 
     * @returns {ReactElement}
     */
    render() {
        if (this.loading) {
            return (<Loading key={'graph_loading'} />);
        }

        return (
            <div className={"graph-wrapper " + this.classNames ?? ""}>
                <CanvasWidget engine={this.engine} />
            </div>
        );
    }
}

export default Graph;
