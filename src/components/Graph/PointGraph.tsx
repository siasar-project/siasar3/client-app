/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import Graph from "@/components/Graph/Graph";
import { PointGraphInterface } from "@/objects/Graph/PointGraphInterface";
import { PointNodeModel } from "@/components/Graph/node/PointNodeModel";
import { PointLinkModel } from "@/components/Graph/link/PointLinkModel";
import { PointNodeFactory } from "@/components/Graph/node/PointNodeFactory";
import { PointPortFactory } from "@/components/Graph/port/PointPortFactory";
import { PointLinkFactory } from "@/components/Graph/link/PointLinkFactory";
import intval from "locutus/php/var/intval";

/**
 * Graph.
 */
class PointGraph extends Graph {

    /**
     * Configurate all components of graph.
     */
    configurate() {
        super.configurate();

        // Register factories.
        this.engine.getPortFactories().registerFactory(new PointPortFactory());
        this.engine.getNodeFactories().registerFactory(new PointNodeFactory());
        this.engine.getLinkFactories().registerFactory(new PointLinkFactory());
    }

    /**
     * Create nodes and links of the graph.
     */
    buildNodesAndLinks() {
        let data = this.props.data as PointGraphInterface;
        //TODO to take full control of node render, make GraphNodeModel instead to use PointNodeModel.
        // see here https://projectstorm.gitbook.io/react-diagrams/customizing/nodes

        const communityIndexMap = data.communities.reduce((map: { [key: string]: any }, community, index) => {
            map[community.id] = index;
            return map;
        }, {});

        const sortedSystems = data.systems.sort((a, b) => {
            const aFirstCommunityIndex = a.communities.length
                ? Math.min(...a.communities.map(c => communityIndexMap[c]))
                : Infinity;
            const bFirstCommunityIndex = b.communities.length
                ? Math.min(...b.communities.map(c => communityIndexMap[c]))
                : Infinity;

            if (aFirstCommunityIndex !== bFirstCommunityIndex) {
                return aFirstCommunityIndex - bFirstCommunityIndex;
            }

            const aTotalRelations = a.communities.length + a.providers.length;
            const bTotalRelations = b.communities.length + b.providers.length;

            return bTotalRelations - aTotalRelations;
        });

        const systemIndexMap = sortedSystems.reduce((map: { [key: string]: any }, system, index) => {
            map[system.id] = index;
            return map;
        }, {});

        const sortedProviders = data.providers.sort((a, b) => {
            const aFirstSystemIndex = data.systems.some(system => system.providers.includes(a.id))
                ? Math.min(...data.systems
                    .filter(system => system.providers.includes(a.id))
                    .map(system => systemIndexMap[system.id])
                )
                : Infinity;
            const bFirstSystemIndex = data.systems.some(system => system.providers.includes(b.id))
                ? Math.min(...data.systems
                    .filter(system => system.providers.includes(b.id))
                    .map(system => systemIndexMap[system.id])
                )
                : Infinity;

            return aFirstSystemIndex - bFirstSystemIndex;
        });

        const sortedCommunities = data.communities.sort((a, b) => {
            const aSystems = data.systems.filter(system => system.communities.includes(a.id)).length;
            const bSystems = data.systems.filter(system => system.communities.includes(b.id)).length;

            if (aSystems === 1 && bSystems !== 1) {
                return -1;
            } else if (aSystems !== 1 && bSystems === 1) {
                return 1;
            }

            const aSystemIndex = data.systems.some(system => system.communities.includes(a.id))
                ? Math.min(...data.systems
                    .filter(system => system.communities.includes(a.id))
                    .map(system => systemIndexMap[system.id])
                )
                : Infinity;
            const bSystemIndex = data.systems.some(system => system.communities.includes(b.id))
                ? Math.min(...data.systems
                    .filter(system => system.communities.includes(b.id))
                    .map(system => systemIndexMap[system.id])
                )
                : Infinity;

            return aSystemIndex - bSystemIndex;
        });

        // Create communities nodes.
        for (let i = 0; i < data.communities.length; i++) {
            let nodeData = data.communities[i];

            const node = new PointNodeModel({
                id: nodeData.id,
                // Limit labels to 30 characters.
                name: (nodeData.label.length > 30 ? nodeData.label.substr(0, 30) + '...' : nodeData.label),
                color: nodeData.color ?? 'white',
                icon: nodeData.icon,
            });

            // node.width = 400;
            // node.height = 10;
            node.setPosition(30, i * 80 + 30);

            this.nodes[nodeData.id] = node;
        }

        // Create providers nodes.
        for (let i = 0; i < data.providers.length; i++) {
            let nodeData = data.providers[i];
            let providerNodeColor = 'white';
            if (nodeData["f1.4"] && (nodeData["f1.4"] === "3" || nodeData["f1.4"] === "4")) {
                providerNodeColor = 'lightblue'
            }

            let label = (nodeData.label.length > 30 ? nodeData.label.substr(0, 30) + '...' : nodeData.label);
            const node = new PointNodeModel(label, providerNodeColor);

            node.setPosition(800, i * 80 + 30);

            this.nodes[nodeData.id] = node;
        }

        // Create systems nodes.
        for (let i = 0; i < data.systems.length; i++) {
            let nodeData = data.systems[i];

            let label = (nodeData.label.length > 30 ? nodeData.label.substr(0, 30) + '...' : nodeData.label);
            const node = new PointNodeModel(label, nodeData.color ?? 'white');

            node.setPosition(420, i * 80 + 30);

            this.nodes[nodeData.id] = node;

            let portCommunity = node.addInPort('communities');
            let portProvider = node.addOutPort('providers');

            // Create links with communities nodes.
            for (let j = 0; j < nodeData.communities.length; j++) {
                let otherNodeId = nodeData.communities[j];
                let otherNode = this.nodes[otherNodeId] as PointNodeModel;

                let otherPort = otherNode.getPort('systems') ?? otherNode.addOutPort('systems');
                const link = portCommunity.link<PointLinkModel>(otherPort);
                // link.addLabel('link info');

                this.links[nodeData.id + '-' + otherNodeId] = link;
            }

            // Create links with providers nodes.
            for (let j = 0; j < nodeData.providers.length; j++) {
                let otherNodeId = nodeData.providers[j];
                let otherNode = this.nodes[otherNodeId] as PointNodeModel;

                if (otherNode) {
                    let otherPort = otherNode.getPort('systems') ?? otherNode.addInPort('systems');
                    const link = portProvider.link<PointLinkModel>(otherPort);
                    // link.addLabel('link info');

                    this.links[nodeData.id + '-' + otherNodeId] = link;
                }
            }
        }

        // Set classes to limit component height.
        let rows = Math.max(data.communities.length, data.systems.length, data.providers.length);
        rows = Math.min(rows, intval(process.env.NEXT_PUBLIC_POINT_GRAPH_AMPLITUDE));
        this.classNames = "point-graph-wrapper rows-" + rows;
    }
}

export default PointGraph;
