/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {
	LinkModel,
	PortModel,
	PortModelAlignment,
	PortModelGenerics,
	PortModelOptions
} from '@projectstorm/react-diagrams-core';
import { PointLinkModel } from '@/components/Graph/link/PointLinkModel';
import { AbstractModelFactory, DeserializeEvent } from '@projectstorm/react-canvas-core';

export interface PointPortModelOptions extends PortModelOptions {
	label?: string;
	in?: boolean;
	type?: string;
}

export interface PointPortModelGenerics extends PortModelGenerics {
	OPTIONS: PointPortModelOptions;
}

/**
 * PointPortModel
 */
export class PointPortModel extends PortModel<PointPortModelGenerics> {
	/**
	 * constructor
	 * 
	 * @param isIn {boolean}
	 * @param name {string}
	 * @param label {string}
	 */
	constructor(isIn: boolean, name?: string, label?: string);

	/**
	 * constructor
	 * 
	 * @param options {PointPortModelOptions}
	 */
	constructor(options: PointPortModelOptions);

	/**
	 * constructor
	 * 
	 * @param options {PointPortModelOptions | boolean}
	 * @param name {string}
	 * @param label {string}
	 */
	constructor(options: PointPortModelOptions | boolean, name?: string, label?: string) {
		if (!!name) {
			options = {
				in: !!options,
				name: name,
				label: label
			};
		}
		options = options as PointPortModelOptions;
		super({
			label: options.label || options.name,
			alignment: options.in ? PortModelAlignment.LEFT : PortModelAlignment.RIGHT,
			type: 'default',
			...options
		});
	}

	/**
	 * deserialize
	 * 
	 * @param event {DeserializeEvent}
	 */
	deserialize(event: DeserializeEvent<this>) {
		super.deserialize(event);
		this.options.in = event.data.in;
		this.options.label = event.data.label;
	}

	/**
	 * serialize
	 * 
	 * @returns {any}
	 */
	serialize() {
		return {
			...super.serialize(),
			in: this.options.in,
			label: this.options.label
		};
	}

	/**
	 * link
	 * 
	 * @param port {PortModel} 
	 * @param factory {AbstractModelFactory}
	 * 
	 * @returns {any}
	 */
	link<T extends LinkModel>(port: PortModel, factory?: AbstractModelFactory<T>): T {
		let link = this.createLinkModel(factory);
		link.setSourcePort(this);
		link.setTargetPort(port);
		return link as T;
	}

	/**
	 * canLinkToPort
	 * 
	 * @param port {PortModel}
	 * 
	 * @returns {any}
	 */
	canLinkToPort(port: PortModel): boolean {
		if (port instanceof PointPortModel) {
			return this.options.in !== port.getOptions().in;
		}
		return true;
	}

	/**
	 * createLinkModel
	 * 
	 * @param factory {AbstractModelFactory}
	 * 
	 * @returns {any}
	 */
	createLinkModel(factory?: AbstractModelFactory<LinkModel>): LinkModel {
		let link = super.createLinkModel();
		if (!link && factory) {
			return factory.generateModel({});
		}
		return link || new PointLinkModel();
	}
}
