/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { PointPortModel } from '@/components/Graph/port/PointPortModel';
import { AbstractModelFactory } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';

/**
 * PointPortFactory
 */
export class PointPortFactory extends AbstractModelFactory<PointPortModel, DiagramEngine> {
	/**
	 * constructor
	 */
	constructor() {
		super('default');
	}

	/**
	 * generateModel
	 * 
	 * @returns {any}
	 */
	generateModel(): PointPortModel {
		return new PointPortModel({
			name: 'unknown'
		});
	}
}
