/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { MapContainer, Marker, TileLayer, useMap, useMapEvents } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet-defaulticon-compatibility";
import { useEffect, useState } from "react";

type Props = { 
    coordinates: [number, number]
    groupToggleIndicator: number
    zoom?: number
};

/**
 * Render map component. import dynamically with no SSR.
 *
 * @param props {Props}
 *
 * @returns {JSX.Element}
 */
const Map = (props: Props) => {
    const [zoom, setZoom] = useState(props.zoom ?? 5);
    const {coordinates, groupToggleIndicator} = props;

    return (
        <div className="map-container">
            <MapContainer
                center={coordinates}
                zoom={zoom}
                scrollWheelZoom={false}
                style={{ width: "100%", height: "100%", zIndex: 0 }}
            >
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <MapViewController
                    zoom={zoom}
                    coordinates={coordinates}
                    groupToggleIndicator={groupToggleIndicator}
                />
                <InteractionsController setZoom={setZoom} />
                <Marker position={coordinates} />
            </MapContainer>
        </div>
    );
};

export default Map;

interface MapViewControllerInterface { 
    zoom: number,
    coordinates: [number, number],
    groupToggleIndicator: number,
}

/**
 * centers map when coordinates or zoom changes
 *
 * @param props {any}
 *
 * @returns {null}
 */
const MapViewController = (props: MapViewControllerInterface) => {
    const { zoom, coordinates, groupToggleIndicator } = props;

    const map = useMap();

    useEffect(() => {
        map.invalidateSize();
    }, [groupToggleIndicator]);

    useEffect(() => {
        map.flyTo(coordinates, zoom);
    }, [coordinates]);

    return null;
};

interface InteractionsController {
    setZoom: Function,
}

/**
 * listen to map interactions
 *
 * @param props {any}
 *
 * @returns {null}
 */
const InteractionsController = (props: InteractionsController) => {
    const { setZoom } = props;

    const map = useMapEvents({
        /**
         * update zoom level
         *
         * @param event
         */
        zoomend: (event) => {
            setZoom(event.target._zoom);
        },
    });

    return null;
};
