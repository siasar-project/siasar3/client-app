/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import DescriptionOutlinedIcon from "@material-ui/icons/DescriptionOutlined";
import BrokenImageOutlinedIcon from "@material-ui/icons/BrokenImageOutlined";
import Tooltip from "@material-ui/core/Tooltip";

interface StatusIconProps {
  status: string;
  fontSize?: "small" | "inherit" | "default" | "large" | "medium";
}

/**
 * Status icon component.
 *
 * @param root0
 * @param root0.status
 * @returns {any}
 *   Structure.
 */
export default function StatusIcon({ status, ...rest }: StatusIconProps) {
  switch (status) {
    case "draft":
      return (
        <Tooltip title={status}>
          <DescriptionOutlinedIcon {...rest} />
        </Tooltip>
      );
    default:
      return (
        <Tooltip title={status}>
          <BrokenImageOutlinedIcon {...rest} />
        </Tooltip>
      );
  }
}
