/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from 'react';
import {isNumber} from "lodash";

const INTERVAL_MAX_D = 0.39;
const INTERVAL_MAX_C = 0.69;
const INTERVAL_MAX_B = 0.89;
const INTERVAL_MAX_A = 1.0;

interface IndicatorItemProps {
    value?: number;
}

/**
 * Get char about the indicator value.
 *
 * @param value
 *
 * @returns {string}
 */
function getIndicatorChar(value:number): string {
    if (value < 0.0 || !isNumber(value)) {
        return '';
    }
    if (value <= INTERVAL_MAX_D) {
        return 'D';
    }
    if (value <= INTERVAL_MAX_C) {
        return 'C';
    }
    if (value <= INTERVAL_MAX_B) {
        return 'B';
    }

    return 'A';
}

/**
 * Get indicator CSS class.
 *
 * @param value
 *
 * @returns {string}
 */
function getIndicatorColorClass(value:number): string {
    if (value < 0.0 || !isNumber(value)) {
        return '';
    }
    if (value <= INTERVAL_MAX_D) {
        return 'indicator_d';
    }
    if (value <= INTERVAL_MAX_C) {
        return 'indicator_c';
    }
    if (value <= INTERVAL_MAX_B) {
        return 'indicator_b';
    }

    return 'indicator_a';
}

/** 
 * Form indicator icon component
 *
 * @param props
 * @constructor
 */
export default function IndicatorIcon(props: IndicatorItemProps) {
    if (!props.value && 0 != props.value) {
        return null;
    }
    let indicator:string = getIndicatorChar(props.value);
    let color:string = getIndicatorColorClass(props.value);

    if ('' === indicator) {
        return null;
    }

    return (
        <span className={"indicator_icon " + color} title={`${props.value}`}>{indicator}</span>
    );
};
