/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons/faEnvelope";
import {Badge} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import {dmUnreadMailCount} from "@/utils/dataManager";
import {smIsValidSession} from "@/utils/sessionManager";
import router from "next/router";

/**
 * UnreadMailCounter component.
 *
 * @returns {any}
 *   Structure.
 */
export default function UnreadMailCounter() {
    // Unread mails.
    const [unreadMails, setUnreadMails] = useState(0);

    /**
     * Update unread mark alert.
     */
    const getUnreadMailCount = () => {
        if(smIsValidSession()) {
            dmUnreadMailCount()
                .then((data: any) => {
                    setUnreadMails(data);
                    setTimeout(getUnreadMailCount, 300000);
                })
                .catch((info: any) => {});
        } else {
            setTimeout(getUnreadMailCount, 300000);
        }
    }

    /**
     * Go to message page.
     */
    const mailClick = () => {
        router.push('/mail/list/');
    }

    useEffect(() => {
        getUnreadMailCount();
    }, [])

    return (
        <>
            {unreadMails > 0 ? (
                <div className={"unreal-mails"}>
                    <Badge badgeContent={unreadMails} color="error" className={"unread-mails"} onClick={mailClick}>
                        <FontAwesomeIcon icon={faEnvelope} size={"2x"} bounce />
                    </Badge>
                </div>
            ) : (<></>)}
        </>
    );
}
