/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {strlen} from "locutus/php/strings";
import substr from "locutus/php/strings/substr";
import t from "@/utils/i18n";
import Modal from "@/components/common/Modal"
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCopy} from "@fortawesome/free-solid-svg-icons";

export interface PillInterface {
    id: string
    title: string | JSX.Element
    color?: any
    isUlid?: boolean
}

/**
 * Pill Component.
 */
export default class Pill extends React.Component<PillInterface> {
    private isOpen = false;

    /**
     * Open dialog with the info.
     */
    handleOpenModal() {
        if (!this.props.isUlid) {
            return;
        }

        this.isOpen = true;
        this.forceUpdate();
    }

    /**
     * Close modal with the info.
     */
    handleCloseModal() {
        this.isOpen = false;
        this.forceUpdate()
    }

    /**
     * Handle copy to clipboard.
     */
    handleCopyToClipboard() {
        if ('clipboard' in navigator) {
            navigator.clipboard.writeText(this.props.id);
        } else {
            document.execCommand('copy', true, this.props.id);
        }

        eventManager.dispatch(
            Events.STATUS_ADD,
            {
                level: StatusEventLevel.INFO,
                title: t('Copy ID'),
                message: t('"@id" was copied to clipboard.', {'@id': this.props.id}),
                isPublic: true,
            }
        );
    }

    /**
     * Render pill.
     *
     * @returns {ReactElement}
     */
    render() {

        let id = this.props.id;
        if (this.props.isUlid) {
            id = substr(id, strlen(id) - 6);
        }

        let classes = "pill-style"
        if (this.props.title === t("Status")) { classes += " status-field" }

        return (
            <>
                <div className={classes}>
                    <div className={"pill-content "+(this.props.color ?? '')}>
                        <span className={"pill-label"}>{ this.props.title }</span>
                        <span className={"pill-value"}>
                            <span className={"w-copy"} onClick={this.handleOpenModal.bind(this)}>{ id } </span>
                            { this.props.isUlid ? (
                                <button
                                    type="button"
                                    className="copy"
                                    title={t("Copy ID")}
                                    onClick={this.handleCopyToClipboard.bind(this)}
                                >
                                    <FontAwesomeIcon icon={faCopy}/>
                                </button>
                            ) : null }
                        </span>
                    </div>
                </div>
                { this.isOpen ? (
                    <Modal
                        isOpen={this.isOpen}
                        onRequestClose={this.handleCloseModal.bind(this)}
                        title={t("View more information")}
                        size={"small"}
                    >
                        {t('The completed value is "@id".', {'@id': this.props.id})}<br/><br/>

                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={this.handleCopyToClipboard.bind(this)}
                        >
                            <FontAwesomeIcon icon={faCopy}/> {t('Copy this value to the clipboard')}
                        </button>
                    </Modal>
                ) : null}
            </>
        );
    }
}
