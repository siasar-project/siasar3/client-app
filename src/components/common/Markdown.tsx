/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { marked } from "marked";

interface MarkdownProps {
  text?: string;
}

/**
 * Render a Markdown text into HTML.
 *
 * @param text
 * @returns {object}
 *   Renderer tags.
 */
const parseText = (text='') => {
  const rawMarkup = marked.parse(text);
  return { __html: rawMarkup };
}

/**
 * Markdown component.
 *
 * @param root0
 * @param root0.text
 * @returns {any}
 *   Structure.
 */
export default function Markdown({ text }: MarkdownProps) {
  
  if(text){
    return (<div className="markdown" dangerouslySetInnerHTML={parseText(text)} />);
  }

  return (<></>);
}
