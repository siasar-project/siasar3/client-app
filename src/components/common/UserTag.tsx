/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from 'react';
import AvatarOffline from "@/components/common/AvatarOffline";

interface UserTagProps {
    picture_url: string;
    username: string;
}

/**
 * User with picture.
 *
 * @param props {UserTagProps}
 *
 * @constructor
 */
const UserTag = (props: UserTagProps) => {
    return (
        <span className={"user-with-gravatar"}>
          <AvatarOffline src={props.picture_url} alt={props.username} />
            &nbsp;<span className="username">{props.username}</span>
        </span>
    )
};

export default UserTag;
