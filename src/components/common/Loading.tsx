/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from 'react';

/** 
 * Loading component
 * 
 * @constructor
 */
const Loading = () => {
    return (
        <div className='drop-loading'>
            <div
                className='circle1'
            />

            <div
                className='circle2'
            />

            <div
                className='circle3'
            />

            <div
                className='circle4'
            />

            <svg className='drop' data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 109.6 175.6"><path d="M456.72,286.19c-13.35-16.86-29.53-31.48-45.09-46.59a108.92,108.92,0,0,0-8.31-8.21l-.07-.07h0c-3.9-3.47-6.66-5.4-6.36-4.82s2.95,10,3.07,10.43c6.63,25.29-2.65,47.12-15.14,68.48C378.89,315.54,373,326,369.3,337.05c-10.65,31.77,6.53,59.3,39.07,64.22,27.74,4.19,52.09-10.06,62.11-36.64C481.34,335.84,474.73,308.93,456.72,286.19Z" transform="translate(-366.15 -226.4)"/></svg> 
        </div>

    );
};

export default Loading;
