/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

// Base code generated with chatGPT from OpenAI.
import React, {useEffect, useState} from 'react';
import {Avatar} from "@material-ui/core";
import {dmLoadImage} from "@/utils/dataManager";

interface CustomImageProps {
    src: string | any;
    alt: string;
    className?: string;
}

/**
 * Offline compatible image component.
 *
 * @param src
 * @param src.src
 * @param src.alt
 * @param src.className
 *
 * @constructor
 */
const AvatarOffline: React.FC<CustomImageProps> = ({ src, alt, className }: CustomImageProps) => {
    const [imageData64, setImageData64] = useState<string>("");
    const [update, setUpdate] = useState<Boolean>();

    useEffect(() => {
        dmLoadImage(src)
            .then((base64Image: string) => {
                setImageData64(base64Image);
                setUpdate(!update);
            });
    }, []);

    if ("" === imageData64) {
        return <></>;
    }
    return <Avatar className={className} src={imageData64} alt={alt} />;
};

export default AvatarOffline;
