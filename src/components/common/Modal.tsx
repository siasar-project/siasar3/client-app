/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import ReactModal from "react-modal";

interface State {
    isOpen: boolean
    title: string
    onRequestClose?: any
    size?: 'small' | 'default' | 'large'
    footMessage?:string
}

/**
 * Modal component.
 */
export default class Modal extends React.Component<State, State> {

    private modalSize: 'small' | 'default' | 'large' = 'default';

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props: State) {
        super(props);

        if (this.props.size) {
            this.modalSize = this.props.size;
        }

        this.handleRequestClose = this.handleRequestClose.bind(this);
    }

    /**
     * Handle close modal request.
     */
    handleRequestClose() {
        if (this.props.onRequestClose) {
            this.props.onRequestClose();
        }
    }

    /**
     * Render pager.
     *
     * @returns {ReactElement}
     */
    render() {
        return (
            <ReactModal
                isOpen={this.props.isOpen}
                shouldCloseOnOverlayClick={false}
                // overlayClassName={"card-edit-item-overlay"}
                // className={"card-edit-item"}
                ariaHideApp={false}
                onRequestClose={this.handleRequestClose}
                className={`modal-content ${this.modalSize}_size`}
                style={{
                    overlay: {
                        backgroundColor: ''
                    }
                }}
            >
                <div className="modal-header">
                    <div className="modal-title">
                        <h2>{this.props.title}</h2>
                        { this.props.onRequestClose ? (
                            <input type="button" key="button-modal-close" className={"btn btn-close"} value={"X"} onClick={this.handleRequestClose}/>
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
                <div className="modal-body">
                    {this.props.footMessage ? (
                        <div className={"modal-footer"}>{this.props.footMessage}</div>
                    ) : (
                        <></>
                    )}
                    {this.props.children}
                </div>
            </ReactModal>
        );
    }
}
