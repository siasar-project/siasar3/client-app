/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import TextField from "@material-ui/core/TextField";

/**
 * Subcomponent.
 *
 * @param root
 * @param root.inputRef
 * @param root.other
 * @constructor
 */
const InputComponent = ({ inputRef, ...other }: any) => <div {...other} />;

/**
 * Outlined component.
 *
 * @param root
 * @param root.children
 * @param root.label
 * @param root.className
 * @param root.helperText
 * @param root.error
 * @constructor
 */
const OutlinedDiv = ({ children, label, className, helperText, error}:any) => {

  return (
    <TextField
      variant="outlined"
      label={label}
      className={className}
      error={error}
      helperText={helperText}
      style={{marginBottom: '8px', marginTop: '8px'}}
      multiline
      fullWidth
      InputLabelProps={{ shrink: true }}
      InputProps={{
        inputComponent: InputComponent
      }}
      inputProps={{ children: children }}
    />
  );
};
export default OutlinedDiv;
