/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

// Base code generated with chatGPT from OpenAI.
import React, {useEffect, useState} from 'react';
import NextImage from 'next/image';
import {dmLoadImage} from "@/utils/dataManager";

interface CustomImageProps {
    src: string | any;
    alt: string;
    width?: string
    height?: string
    id?: string
}

/**
 * Offline compatible image component.
 *
 * @param src
 * @param src.src
 * @param src.alt
 * @param src.width
 * @param src.height
 * @param src.id
 *
 * @constructor
 */
const ImageOffline: React.FC<CustomImageProps> = ({ src, alt, width, height, id }: CustomImageProps) => {
    const [imageData64, setImageData64] = useState<string>("");
    const [update, setUpdate] = useState<Boolean>();

    useEffect(() => {
        loadImage(src);
    }, []);

    useEffect(() => {
        loadImage(src);
    }, [src]);

    /**
     * Load image and update component.
     *
     * @param src
     */
    const loadImage = (src:string) => {
        dmLoadImage(src)
            .then((base64Image: string) => {
                if (base64Image) {
                    setImageData64(base64Image);
                }
                setUpdate(!update);
            });
    }

    if ("" === imageData64) {
        return <></>;
    }
    let sizes = "(min-width: 75em) 33vw, (min-width: 48em) 50vw, 100vw";
    let layout:any = "fill";
    let widthValue = undefined;
    let heightValue = undefined;
    let unoptimized = false;
    if (width || height) {
        sizes = "";
        layout = undefined;
        unoptimized = true;
        if (width) {
            widthValue = width;
        }
        if (height) {
            heightValue = height;
        }
    }

    return <NextImage
        id={id}
        src={imageData64}
        alt={alt}
        sizes={sizes}
        layout={layout}
        width={widthValue}
        height={heightValue}
        unoptimized={unoptimized}
    />;
};

export default ImageOffline;
