/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import Link from "next/link";

interface OptionalLinkProps {
    condition?: Boolean;
    href: any;
    children: any;
}

/**
 * If condition is True display a link, else only display a label.
 *
 * @param root
 * @param root.condition
 * @param root.href
 * @param root.children
 *
 * @constructor
 */
export default function OptionalLink({ condition, href, children }: OptionalLinkProps) {
    return condition ? (<Link href={href} passHref>{children}</Link>) : (<span>{children}</span>);
}
