/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ErrorInfo} from "react";

interface MyStateInterface {
    hasError: boolean
}

/**
 * Error handler component.
 *
 * todo See https://catchjs.com/Docs/React
 *
 * @see https://reactjs.org/blog/2017/07/26/error-handling-in-react-16.html
 */
export class ErrorBoundary extends React.Component<any, MyStateInterface> {
    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props: MyStateInterface | Readonly<MyStateInterface>) {
        super(props);
        this.state = { hasError: false };
    }

    /**
     * Error hook.
     *
     * @param error
     * @param info
     */
    componentDidCatch(error: Error, info: ErrorInfo) {
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        // logErrorToMyService(error, info);
    }

    /**
     * Renderer.
     *
     * @returns {Element}
     */
    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h1>Something went wrong.</h1>;
        }
        return this.props.children;
    }
}
