/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { ChangeEvent, ReactNode } from "react";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Markdown from "@/components/common/Markdown";

interface Option {
  value: string;
  label: string;
}

interface SelectProps {
  label: string;
  options: Option[];
  helperText?: string;
  error?: boolean;
  disabled?: boolean;
  className?: string;
  onChange?: (
    event: ChangeEvent<{ name?: string | undefined; value: unknown }>,
    child: ReactNode
  ) => void;
  value?: string;
}

/**
 * Custom select component.
 *
 * @param root0
 * @param root0.label
 * @param root0.options
 * @param root0.helperText
 * @param root0.onChange
 * @param root0.className
 * @param root0.error
 * @param root0.disabled
 * @param root0.value
 * @returns {any}
 *   Structure.
 */
export default function OurSelect({
  label,
  options,
  helperText,
  onChange,
  className,
  error,
  disabled,
  value,
}: SelectProps) {
  return (
    <FormControl
      variant="outlined"
      className={className}
      error={error ?? false}
    >
      <InputLabel>{label}</InputLabel>
      <Select
        onChange={onChange}
        label={label}
        disabled={disabled}
        value={value}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </Select>
      <Markdown text={helperText} />
    </FormControl>
  );
}
