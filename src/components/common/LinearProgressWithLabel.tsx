/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import * as React from 'react';
import PropTypes from 'prop-types';
import LinearProgress, { LinearProgressProps } from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

/**
 * Internal component.
 *
 * @param props
 * @constructor
 */
function LinearProgressWithLabel(props: JSX.IntrinsicAttributes & LinearProgressProps) {
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ width: '100%', mr: 1 }}>
                <LinearProgress {...props} />
            </Box>
            <Box sx={{ minWidth: 35 }}>
                <Typography variant="body2">{`${Math.round(
                    props.value ?? 0,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

LinearProgressWithLabel.propTypes = {
    /**
     * The value of the progress indicator for the determinate and buffer variants.
     * Value between 0 and 100.
     */
    value: PropTypes.number.isRequired,
};

/**
 * Linear progress with value label.
 *
 * @param rest
 * @constructor
 */
export default function LinearWithValueLabel({...rest}) {
    return (
        <Box sx={{ width: '100%' }}>
            <LinearProgressWithLabel {...rest} />
            <Typography variant="body2">{rest.label ?? ''}</Typography>
            <Typography variant="body2">{rest.sublabel ?? ''}</Typography>
        </Box>
    );
}
