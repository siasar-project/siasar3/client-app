/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from 'react';
import t from "@/utils/i18n";
import ActionButtons from "@/components/Buttons/ActionButtons";
import ButtonsManager from "@/utils/buttonsManager";
import {dmGetParametricTemplateUrl, dmUploadImportParametric} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import Modal from "@/components/common/Modal";
import {faFileImport, faUndo} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

/**
 * Component props.
 */
interface PropsInterface {
    entityName:string
    title: string
}

/**
 * State interface.
 */
interface StateInterface {
    file: any
}

/**
 * Import parametric component.
 */
export default class ImportParametric extends  React.Component<PropsInterface, StateInterface> {
    // Action buttons.
    private buttons: ButtonsManager = new ButtonsManager;
    private modalButtons: ButtonsManager = new ButtonsManager;
    private isOpen: boolean = false;
    private isLoading: boolean = false;
    private apiResponse: string = '';

    /**
     * constructor.
     *
     * @param props
     */
    constructor(props: PropsInterface | Readonly<PropsInterface>) {
        super(props);

        this.state = {
            file: null,
        };

        // Component bottoms.
        this.buttons.addButton({
            id: 'button-reset-dialog',
            type: "reset",
            icon: <FontAwesomeIcon icon={faUndo} />,
            label: t("Reset"),
            className: "btn btn-primary btn-red",
            /**
             * Is this button visible?
             *
             * @returns {boolean}
             */
            show: () => {
                return !this.isLoading;
            }
        });
        this.buttons.addButton({
            id: 'button-import-dialog',
            type: "submit",
            icon: <FontAwesomeIcon icon={faFileImport} />,
            label: t("Import"),
            className: "btn btn-primary green",
            /**
             * Is this button visible?
             *
             * @returns {boolean}
             */
            show: () => {
                return !this.isLoading;
            }
        });

        // Modal bottoms.
        this.modalButtons.addButton({
            id: 'button-cancel-dialog',
            icon: <></>,
            label: t("Close"),
            className: "btn btn-primary green",
            onClick: this.handleCloseModal.bind(this),
        });

        this.handleChange = this.handleChange.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Handle component state.
     *
     * @param event
     */
    handleChange(event: any) {
        if (event.target.files && event.target.files.length > 0) {
            this.setState({file: event.target.files[0]});
        }
    }

    /**
     * Handle component reset state.
     *
     * @param event
     */
    handleReset(event: any) {
        this.setState({file: null});
    }

    /**
     * Form submit handler.
     *
     * @param event
     */
    handleSubmit(event: any) {
        event.preventDefault();
        this.isLoading = true;
        this.forceUpdate();
        dmUploadImportParametric(this.props.entityName, this.state.file)
            .then((data) => {
                this.apiResponse = data;
                this.isOpen = true;
                this.isLoading = false;
                this.forceUpdate();
            })
            .catch((reason) => {
                this.apiResponse = reason.response.data ?? reason.response.data?.message ?? reason.response.statusText;
                this.isOpen = true;
                this.isLoading = false;
                this.forceUpdate();
            });
    }

    /**
     * Close modal with the edit point team.
     */
    handleCloseModal() {
        this.isOpen = false;
        this.forceUpdate()
    }

    /**
     * Render pager.
     *
     * @returns {ReactElement}
     */
    render() {
        return (
            <>
                <fieldset>
                    <div>{t("@entityName importer", {"@entityName": this.props.title})}</div>
                    <a href={dmGetParametricTemplateUrl(this.props.entityName, true)} target={"_blank"} rel="noreferrer">{t('Download template')}</a>
                    {this.isLoading ? (
                        <Loading />
                    ) : (
                        <form
                            key={"importer-form"}
                            className={"importer-form"}
                            encType="multipart/form-data"
                            onSubmit={this.handleSubmit}
                            onReset={this.handleReset}
                        >
                            <input
                                type="file"
                                key={'import-file-input'}
                                id={'import-file-input'}
                                className={"import-file-input"}
                                accept={'application/csv'}
                                multiple={false}
                                onChange={this.handleChange}
                            />
                            <ActionButtons buttons={this.buttons} isLoading={false}/>
                        </form>
                    )}
                </fieldset>
                <Modal
                    isOpen={this.isOpen}
                    onRequestClose={this.handleCloseModal.bind(this)}
                    title={t("Parametric import response")}
                    size={"large"}
                >
                    <div className={"modal-import"}>
                        <pre>{this.apiResponse}</pre>
                    </div>
                    {<ActionButtons buttons={this.modalButtons} isLoading={false}/>}
                </Modal>
            </>
        );
    }
}
