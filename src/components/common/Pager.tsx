/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";

interface State {
    page?: number
    onChange?: any
    isLoading?: boolean
    itemsAmount?: number
}

/**
 * Generic pager.
 */
export default class Pager extends  React.Component<State, State> {

    /**
     * Pager constructor.
     *
     * @param props
     */
    constructor(props: State) {
        super(props);

        this.goFirst = this.goFirst.bind(this);
        this.goPrevious = this.goPrevious.bind(this);
        this.goNext = this.goNext.bind(this);
    }

    /**
     * Go previous page.
     */
    goPrevious() {
        let current: number = this.props.page !== undefined ? this.props.page : 1;
        let newState = {
            page: current - 1,
        };

        if (newState.page < 1) {
            newState = {
                page: 1,
            };
        }
        this.setState(newState);
        if (this.props.onChange) {
            this.props.onChange(newState);
        }
    }

    /**
     * Go next page.
     */
    goFirst() {
        this.setState({page: 1});
        if (this.props.onChange) {
            this.props.onChange({page: 1});
        }
    }

    /**
     * Go next page.
     */
    goNext() {
        let current: number = this.props.page !== undefined ? this.props.page : 1;
        this.setState({page: current + 1});
        if (this.props.onChange) {
            this.props.onChange({page: current + 1});
        }
    }

    /**
     * Render pager.
     *
     * @returns {ReactElement}
     */
    render() {
        let prevDisabled = (this.props.isLoading || this.props.page === 1);
        let nextDisabled = (this.props.isLoading) || (this.props.itemsAmount === 0);

        return (
            <div className={"pager-container"}>
                <ul>
                    <li><button className={"pager-btn btn first"} onClick={this.goFirst} disabled={prevDisabled}>{t("First")}</button></li>
                    <li><button className={"pager-btn btn previous"} onClick={this.goPrevious} disabled={prevDisabled}>{t("Previous")}</button></li>
                    <li><span className={"current-page"}>{t("Page @page", {'@page': this.props.page})}</span></li>
                    <li><button className={"pager-btn btn next"} onClick={this.goNext} disabled={nextDisabled}>{t("Next")}</button></li>
                </ul>
            </div>
        );
    }
}
