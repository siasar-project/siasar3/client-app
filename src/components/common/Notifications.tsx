/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import * as React from "react";
import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";

const columns: GridColDef[] = [
  {
    field: "fullName",
    headerName: "Full name",
    description: "This column has a value getter and is not sortable.",
    sortable: false,
    width: 160,
    /**
     * Get cell value.
     *
     * @param params
     * @returns {any}
     *   Add last name to first name.
     */
    valueGetter: (params: GridValueGetterParams) =>
      `${params.row.firstName || ""} ${
          params.row.lastName || ""
      }`,
  },
  {
    field: "notification",
    headerName: "Notification",
    width: 240,
    editable: true,
  },
];

const rows = [
  {
    id: 1,
    lastName: "Snow",
    firstName: "Jon",
    age: 35,
    notification: "lorem it ..",
  },
  {
    id: 2,
    lastName: "Lannister",
    firstName: "Cersei",
    age: 42,
    notification: "Itsum lagae ..",
  },
  {
    id: 3,
    lastName: "Lannister",
    firstName: "Jaime",
    age: 45,
    notification: "Indo mine to ..",
  },
];

/**
 * Dummy table.
 *
 * @returns {any}
 *   Structure.
 */
export default function Notifications() {
  return (
    <div style={{ height: 300, width: "100%" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={3}
        rowsPerPageOptions={[3]}
        checkboxSelection
        disableSelectionOnClick
      />
    </div>
  );
}
