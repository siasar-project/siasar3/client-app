/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
/**
 * @see https://upmostly.com/tutorials/build-a-react-switch-toggle-component
 */
import React from 'react';

interface switchInterface {
    id: string
    className?: string
    value: boolean,
    onChange: any
    "data-index"? : number
}

/**
 * Toggle/Switch component.
 *
 * @param props
 * @constructor
 */
const Switch = (props: switchInterface) => {
    return (
        <div className={"react-switch-item " +(props.value ? "state-on" : "") + " " + (props.className ?? "")}>
            <input
                checked={props.value}
                onChange={props.onChange}
                className="react-switch-checkbox"
                id={props.id}
                data-index={props["data-index"]}
                type="checkbox"
            />
            <label
                className={"react-switch-label " + (props.value ? "react-switch-label-on" : "") + " " + (props.className ?? "")}
                htmlFor={props.id}
            >
                <span className={`react-switch-button`} />
            </label>
        </div>
    );
};

export default Switch;
