/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import {ButtonInterface} from "@/objects/Buttons/ButtonInterface";
import ButtonsManager from "@/utils/buttonsManager";

interface ActionButtonsInterface {
  buttons: ButtonsManager
  isLoading: boolean
}

/**
 * Actions Buttons renderer.
 */
export default class ActionButtons extends React.Component<ActionButtonsInterface> {
  /**
   * Render.
   *
   * @returns {React.ReactElement}
   */
  render() {
    let buttons: any = this.props.buttons.getButtons().map((btn: ButtonInterface) => {
      if (btn.show === undefined || btn.show()) {
          let bType: any = "button";
          if (btn.type) {
            bType = btn.type;
          }
          return (
            <button key={btn.id} id={btn.id} type={bType} className={"btn "+btn.className} onClick={btn.onClick || (() => {})} disabled={this.props.isLoading}>
                {btn.icon}{btn.label}
            </button>
          );
      }
      return null;
    });
    
    if (buttons.length === 0) {
      return null;
    }

    return (
      <div className="form-actions">
        {buttons}
      </div>
    );
  }
}
