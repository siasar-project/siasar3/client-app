/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { confirmationFlow } from "@/utils/confirmationFlow";
import {
    dmGetCountry, dmGetFormRecord,
    dmSearchFormRecords,
} from "@/utils/dataManager";
import { eventManager, Events, StatusEventLevel } from "@/utils/eventManager";
import t from "@/utils/i18n";
import router from "next/router";
import { useEffect, useState } from "react";
import { useMachine } from "react-robot";
import Modal from "../common/Modal";
import {getDestinationQueryParameter} from "@/utils/siasar";
import {dmGetTechnicalAssistanceProviders} from "@/utils/dataManager/ParametricRead";
import {dmAddFormRecord} from "@/utils/dataManager/FormRecordWrite";

type Props = {
    formRedirectURL?: string;
    disabled?: boolean
};

/**
 * renders 3 buttons for creating a new record (form.school, form.health.care, form.tap)
 * this component returns 3 separate button elements, to be position by it's parent.
 *
 * @param props {Props}
 *
 * @returns {JSX.Element}
 */
const BasicCreationButtons = (props: Props) => {
    // add optional query param to redirect from dynform
    const urlTemplate = getDestinationQueryParameter({value: props.formRedirectURL});

    // List state.
    const [currentCountry, setCurrentCountry] = useState(null);
    // TAPs
    const [taps, setTaps] = useState<any[]>([]);
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(true);
    // Confirmation flow.
    const [confirmationTitle, setConfirmationTitle] = useState("Remove");
    const [confirmationMessage, setConfirmationMessage] = useState("Message");
    const [itemToRemove, setItemToRemove] = useState();
    const [tapSelected, setTapSelected] = useState();
    const [currentConfirmationState, updateConfirmationState] = useMachine(confirmationFlow);

    useEffect(() => {
        dmGetCountry().then((country: any) => {
            dmGetTechnicalAssistanceProviders().then((taps) => {
                setCurrentCountry(country);
                setTaps(taps);
                setIsLoading(false);
            });
        });
    }, []);

    /**
     * Render TAPs select
     *
     * @returns {Component}
     */
    const renderTapsSelect = () => {
        // Build option list.
        let options = [];
        for (const optionsKey in taps) {
            let item: any = taps[optionsKey];
            options.push(
                <option key={"item-" + item.id} value={item.id}>
                    ({item.id}) {item.name}
                </option>
            );
        }

        return (
            <select key={"taps"} id={"taps"} value={tapSelected} onChange={handleTapsChange}>
                {" "}
                {options}
            </select>
        );
    };

    /**
     * Handle Taps select state.
     *
     * @param event
     */
    const handleTapsChange = (event: any) => {
        setTapSelected(event.target.value);
    };

    /**
     * Handle add new item.
     *
     * @param event
     */
    const handleAddItem = (event: any) => {
        if (currentCountry !== null) {
            let button: any = event.target;
            // If we click button text, we must change the target.
            if (undefined === event.target.value) {
                button = event.target.parentElement;
            }
            if (button.value === "form.tap") {
                if (taps.length > 0) {
                    setConfirmationTitle(t("TAP parametric"));
                    setConfirmationMessage(t("Choose the TAP on which you are going to work"));
                    setItemToRemove(undefined);
                    setTapSelected(taps[0].id);
                    updateConfirmationState("begin");
                } else {
                    eventManager.dispatch(Events.STATUS_ADD, {
                        level: StatusEventLevel.WARNING,
                        title: t("Add TAP"),
                        message: "There are no TAPs defined yet.",
                        isPublic: true,
                    });
                }
            } else {
                let data: any = {};
                switch (button.value) {
                    case "form.school":
                        data["field_main_source_is_water_available_at_school"] = true;
                        break;
                    case "form.health.care":
                        data["field_drinking_water_available"] = true;
                        break;
                }
                setIsLoading(true);
                dmAddFormRecord({ formId: button.value, data: data })
                    .then((data) => {
                        dmGetFormRecord({formId: button.value, id:data.id})
                            .then((newRecord: any) => {
                                router.push(`/${button.value}/${data.id}/edit${urlTemplate}`);
                            });
                    })
                    .catch(() => {
                        setIsLoading(false);
                    });
            }
        }
    };

    /**
     * Handle add new form.tap item.
     */
    const handleAddTapItem = () => {
        dmSearchFormRecords(
            {
                formId: "form.tap",
                filter: {
                    field_provider_name: tapSelected,
                },
            },
            true,
            1,
            false
        ).then((list: any) => {
            list = list.data;
            let found = null;
            if (list) {
                for (let i = 0; i < list.length; i++) {
                    if (["draft", "finished"].includes(list[i].field_status.value)) {
                        found = list[i];
                        break;
                    }
                }
            }

            if (found) {
                router.push(`/form.tap/${found.id}/edit${urlTemplate}`);
            } else {
                setIsLoading(true);
                dmAddFormRecord({
                    formId: "form.tap",
                    data: {
                        field_provider_name: {
                            value: tapSelected,
                            class: "App\\Entity\\TechnicalAssistanceProvider",
                        },
                    },
                })
                    .then((data) => {
                        router.push(`form.tap/${data.id}/edit${urlTemplate}`);
                    })
                    .catch(() => {
                        setIsLoading(false);
                    });
            }

            updateConfirmationState("confirm");
        });
    };

    return (
        <>
            <button
                type="submit"
                value={"form.tap"}
                className={"btn button-tap"}
                onClick={handleAddItem}
                key="button-add-tap"
                disabled={isLoading || props.disabled}
            >
                <span>{t("Add TAP")}</span>
            </button>

            <button
                type="submit"
                value={"form.health.care"}
                className={"btn button-health-care"}
                onClick={handleAddItem}
                key="button-add-health-care"
                disabled={isLoading || props.disabled}
            >
                <span>{t("Add Health Centre")}</span>
            </button>

            <button
                type="submit"
                value={"form.school"}
                className={"btn button-schools"}
                onClick={handleAddItem}
                key="button-add-school"
                disabled={isLoading || props.disabled}
            >
                <span>{t("Add Schools")}</span>
            </button>

            <Modal
                title={confirmationTitle}
                onRequestClose={() => updateConfirmationState("cancel")}
                isOpen={currentConfirmationState.name === "confirming"}
            >
                <div className="surveys-modal">
                    <span>{confirmationMessage}</span>
                    {!itemToRemove ? renderTapsSelect() : ""}
                    <div className="card-serveys-actions">
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => updateConfirmationState("cancel")}
                        >
                            {t("Cancel")}
                        </button>
                        <button type="button" className="btn btn-primary green" onClick={handleAddTapItem}>
                            {t("Add")}
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default BasicCreationButtons;
