/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {ReactElement} from 'react'
import t from '@/utils/i18n';
import CardItemBase from './CardItemBase'
import {SystemRole, userHasPermission} from '@/utils/permissionsManager';
import Modal from "@/components/common/Modal"
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {smGetSession, smHasPermission} from "@/utils/sessionManager";
import {dmGetFileUrl, dmInvalidateUsers, dmUpdateUser} from "@/utils/dataManager"
import {empty} from "locutus/php/var";
import ImageOffline from "@/components/common/ImageOffline";
import AvatarOffline from "@/components/common/AvatarOffline";

/**
 * User card
 */
export default class CardUser extends CardItemBase {
    private isOpenModal = false;
    private isCurrentUser: boolean = smGetSession()?.id === this.props.data.id;

    /**
     * Open dialog.
     */
    handleOpenModal() {
        let isAdmin = userHasPermission(this.props.data, "all permissions");
        if (isAdmin && !smHasPermission('all permissions')) {
            return;
        }

        if (smHasPermission('update doctrine user')) {
            if (this.isCurrentUser) {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.WARNING,
                        title: t('Change user status'),
                        message: t('It is not possible to change the status of the logged in account.'),
                        isPublic: true,
                    }
                );
            } else {
                this.isOpenModal = true;
                this.forceUpdate();
            }
        }
    }

    /**
     * Close modal.
     */
    handleCloseModal() {
        this.isOpenModal = false;
        this.forceUpdate()
    }

    /**
     * Handle change user status.
     */
    handleChangeStatus() {
        const updatedUser: any = {
            active: !this.props.data.active,
        };
        dmUpdateUser(this.props.data.id, updatedUser)
            .then(resp => {
                dmInvalidateUsers().then(() => {
                    this.props.data.active = !this.props.data.active;

                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.INFO,
                            title: t('Change user status'),
                            message: t('The user status was changed to "@status".', {'@status': (this.props.data.active ? 'active' : 'inactive')}),
                            isPublic: true,
                        }
                    );

                    this.isOpenModal = false;
                    this.forceUpdate()
                });
            })
            .catch(() => {
                this.isOpenModal = false;
                this.forceUpdate()
            });
    }

    /**
     * Renders a user card
     * 
     * @returns {JSX.Element}
     */
    render() {
        let flag:ReactElement|null = null;
        if (!empty(this.props.data.countryFlagId)) {
            let flagUrl:string = dmGetFileUrl(this.props.data.countryFlagId || "", false)
            let label = t('Country flag');
            flag = (
                <span className="country-flag">
                    <ImageOffline
                        src={flagUrl}
                        alt={label}
                        width={'16px'}
                        height={'11px'}
                    />
                </span>
            );
        }
        return (
            <>
                <div className="card-item card-user">
                    <div className="card-item-image">
                        {flag}
                        { this.props.data.active ? (
                            <div className="user-tag active" onClick={this.handleOpenModal.bind(this)}><span>{t('Active')}</span></div>
                        ) : (
                            <div className="user-tag inactive" onClick={this.handleOpenModal.bind(this)}><span>{t('Inactive')}</span></div>
                        )}
                        <div className="card-user-details">
                            <AvatarOffline className='avatar' src={this.props.data.gravatar} alt={t('User profile picture')} />
                            <span className="username">{this.props.data.username}</span>
                            <span className="email">{this.props.data.email}</span>
                        </div>
                    </div>
                    <div className='card-item-container'>
                        <div className="card-item-content">
                            <div className="card-user-roles">
                                <span className="roles-title">{t('Roles')}</span>
                                <ul className='roles-list'>
                                    {
                                        this.props.data.roles
                                            .filter((role: SystemRole) => role.id !== 'ROLE_ADMIN' && role.id !== 'ROLE_USER')
                                            .map((role: SystemRole) => (<li key={role.id} className='role'>{role.label}</li>))
                                    }
                                </ul>
                            </div>
                        </div>
                        {this.renderActions('card-user')}
                    </div>
                </div>
                { this.isOpenModal ? (
                    <Modal
                        isOpen={this.isOpenModal}
                        onRequestClose={this.handleCloseModal.bind(this)}
                        title={t("Change user status")}
                        size={"small"}
                    >
                        <p>{t('The user is @status.', {'@status': (this.props.data.active ? 'active' : 'inactive')})}</p>
                        <p>{t('Do you like change their status to "@status"?', {'@status': (this.props.data.active ? 'inactive' : 'active')})}</p>
                        <div className="form-actions">
                            <button type="button" className="btn btn-primary green" onClick={this.handleChangeStatus.bind(this)}>{t("Change")}</button>
                            <button type="button" className="btn btn-primary btn-red" onClick={this.handleCloseModal.bind(this)}>{t("Cancel")}</button>
                        </div>
                    </Modal>
                ) : null}
            </>            
        )
    }    
}
