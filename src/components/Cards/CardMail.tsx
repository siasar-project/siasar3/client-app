/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import CardItemBase from "@/components/Cards/CardItemBase";
import {getLocalizedDate, getRelativeDate, getTimezone} from "@/utils/siasar";
import {faHouseFlag} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import t from "@/utils/i18n";
import {faEnvelopeOpen} from "@fortawesome/free-solid-svg-icons/faEnvelopeOpen";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons/faEnvelope";
import Pill from "@/components/common/Pill";
import UserTag from "@/components/common/UserTag";

/**
 * Inquiry card.
 */
export default class CardMail extends CardItemBase {

    /**
     * Render inquiry card.
     *
     * @returns {React.Element}
     */
    render() {
        let userTimezone = getTimezone();
        let relativeDate = getRelativeDate(this.props.data.field_changed.value, this.props.data.field_changed.timezone, userTimezone);
        let luxonDate = getLocalizedDate(relativeDate, userTimezone);

        // TODO: The form read mark field can be changed by various users, and need other implementation.
        return (
            <div className="card-item card-mail-service">
                <div className="card-item-container">
                    <div className="card-item-content card-mail-service-content">
                        <h4>{
                            (this.props.data.field_global.value === "1") ?
                                (<><FontAwesomeIcon icon={faHouseFlag} /><span> </span></>) :
                                (<></>)
                        }{ this.props.data.field_subject.value }</h4>
                        <div>
                            <strong>{t("Remitter")}</strong>
                            <div>
                                <UserTag picture_url={this.props.data.field_from.meta.gravatar} username={this.props.data.field_from.meta.username}/>
                            </div>
                        </div>
                        <div>
                            {(this.props.data.field_is_read.value === "1") ?
                                (<Pill title={<FontAwesomeIcon icon={faEnvelopeOpen} />} color={"green"} id={t("Read")}/>) :
                                (<Pill title={<FontAwesomeIcon icon={faEnvelope} />} color={"red"} id={t("Unread")}/>)}
                        </div>
                        <div>
                            <Pill title={t("Responses")} color={"green"} id={this.props.data.responses}/>
                        </div>
                        <div>
                            <span className={"title"}>{t("Changed:")}</span>&nbsp;
                            <span className={"label"}>{ relativeDate }</span>
                        </div>
                    </div>
                    {this.props.data.defaultParametric || this.renderActions("card-mail-service-actions")}
                </div>
            </div>
        );
    }
}
