/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React, {ReactElement} from "react";
import t from "@/utils/i18n";
import Loading from "@/components/common/Loading";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faThLarge } from "@fortawesome/free-solid-svg-icons";
import { eventManager, Events } from "@/utils/eventManager";


interface CardContainerInterface {
    cards: Array<any>
    cardType: string
    isLoading: boolean
    buttons: CardButtonsInterface[]
    enableList?: boolean
    forceList?: boolean
}

/**
 * Card container.
 */
export default class CardContainer extends React.Component<CardContainerInterface, CardContainerInterface> {

    protected cardsLayout: 'list' | 'grid' = this.props.forceList ? 'list' : 'grid'

    /**
     * listen to cards-container-layout chages 
     * 
     * @param props {any}
     */
    constructor(props: any) {
        super(props);

        this.updateLayout = this.updateLayout.bind(this);
        this.toggleLayout = this.toggleLayout.bind(this);

        eventManager.on(Events.CARDS_LAYOUT_CHANGED, this.updateLayout);
    }

    /**
     * once mounted, check localStorage for layout value
     */
    componentDidMount(): void {
        this.updateLayout();
    }

    /**
     * check if valued changed and trigger rerender if necessary
     */
    updateLayout() {
        let prevLayout = localStorage.getItem('card-container-layout');

        if (
            !prevLayout ||
            this.cardsLayout === prevLayout ||
            (prevLayout !== 'list' && prevLayout !== 'grid')
        ) {
            return;
        }

        this.cardsLayout = prevLayout;
        this.forceUpdate();
    }

    /**
     * toggle value in local storage between 'list' and 'grid'
     */
    toggleLayout() {
        let prevLayout = localStorage.getItem('card-container-layout');
        let newLayout = prevLayout === 'list' ? 'grid' : 'list';
        localStorage.setItem('card-container-layout', newLayout);
        
        eventManager.dispatch(Events.CARDS_LAYOUT_CHANGED);
    }

    /**
     * Render inquiry card list.
     *
     * @returns {ReactElement}
     */
    render() {
        let cardItems:Array<ReactElement> = [];
        let aCardItems:Array<any> = this.props.cards;
        const Component = React.lazy(
            () => import('@/components/Cards/'+this.props.cardType)
                .catch(() => ({
                    /**
                     * If not component found, log error to console.
                     *
                     * @returns {ReactElement}
                     */
                    default: () => <>{console.error('[CardContainer] Component "'+this.props.cardType+'" card type not found.')}</>
                }))
        );
        cardItems = aCardItems.map((card) => {
            let classes = "row-item"
            if (this.props.forceList || (this.props.enableList && this.cardsLayout === "list")) {
                classes += " list-override"
            }

            return (
                <div key={"row-item--"+card.id} className={classes}>
                    <Component key={card.id} data={card} buttons={this.props.buttons}/>
                </div>
            );
        });

        if (this.props.isLoading) {
            return <Loading />;
        } else if (this.props.cards.length === 0) {
            return <span className={'empty-message'}>{t("No content found.")}</span>;
        }

        return (
            <React.Suspense fallback={<Loading />}>
                {
                    this.props.enableList && !this.props.forceList
                        ? <div className="layout-buttons">
                            <button
                                type="button"
                                onClick={ this.toggleLayout.bind(this) }
                                className="btn btn-primary visibility-tablet"
                            >
                                <FontAwesomeIcon 
                                    icon={ this.cardsLayout === 'list' ? faThLarge : faList } 
                                />
                            </button>
                        </div>
                        : null
                }
                <div className="cards-container">
                    {cardItems}
                </div>
            </React.Suspense>
        );
    }
}
