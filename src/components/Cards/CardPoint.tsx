/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import CardItemBase from "@/components/Cards/CardItemBase";
import Pill from "@/components/common/Pill";
import {truncateWithEllipsis} from "@/utils/siasar";
import {EllipsisPositionEnum} from "@/objects/EllipsisPositionEnum";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import defaultPic from "../../../public/default.png";
import ImageOffline from "@/components/common/ImageOffline";

/**
 * Point card.
 */
export default class CardPoint extends CardItemBase {

    /**
     * Get Division name and path.
     *
     * @param meta {AdmDivisionMetaInterface}
     * @returns {string}
     */
    getAdministrativeDivisionBreadcrumb(meta: AdmDivisionMetaInterface) {
        let resp = meta.name;
        if (meta.parent) {
            resp = this.getAdministrativeDivisionBreadcrumb(meta.parent) + " / " + resp;
        }
        return resp;
    }

    /**
     * Render Point card.
     *
     * @returns {ReactElement}
     */
    render() {
        let breadcrumbRoot:any = this.props.data.administrative_division[0].meta.parent;
        let title:string = this.props.data.administrative_division[0].meta.name;
        let headerContent = truncateWithEllipsis(this.getAdministrativeDivisionBreadcrumb(breadcrumbRoot), 122, EllipsisPositionEnum.Middle)

        // Allow API image debug.
        let xdebug = '';
        if (process.env.NEXT_PUBLIC_SIASAR3_API_DEBUG === '1') {
            xdebug = 'XDEBUG_SESSION=siasar';
        }

        return (
            <div className="card-item card-point">
                <div className="card-item-image">
                    { headerContent ? <div className="card-header"><span>{ headerContent }</span></div> : null }
                    <ImageOffline
                        src={
                            this.props.data.image ? this.props.data.image : defaultPic.src
                        }
                        alt={this.props.data.field_title || 'Image'}
                    />
                </div>
                <div className="card-item-container">
                    <div className="card-item-content card-point-content">
                        <h2 className={'card-title'}>{ title }</h2>
                        <Pill title={t("Status")} color={"green"} id={this.props.data.status} />
                        <Pill title={t("Version")} color={"red"} id={this.props.data.version} />
                        <div className={"card-item-alerts"}>
                            <div className={"error"}>
                                {t(
                                    "Errors: @value",
                                    {"@value":  this.props.data.notes.errors !== -1 ? this.props.data.notes.errors : '--'}
                                )}
                            </div>
                            <div className={"warning"}>
                                {t(
                                    "Warnings: @value",
                                    {"@value":  this.props.data.notes.warnings !== -1 ? this.props.data.notes.warnings : '--'}
                                )}
                            </div>
                        </div>
                    </div>
                {this.renderActions("card-point-actions")}
                </div>
            </div>
        );
    }
}
