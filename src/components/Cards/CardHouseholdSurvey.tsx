/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {EllipsisPositionEnum} from "@/objects/EllipsisPositionEnum";
import t from "@/utils/i18n";
import {truncateWithEllipsis} from "@/utils/siasar";
import Pill from "../common/Pill";
import CardItemBase from "./CardItemBase";
import Loading from "../common/Loading";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import defaultPic from "../../../public/default.png";
import React from "react";
import UserTag from "@/components/common/UserTag";
import ImageOffline from "@/components/common/ImageOffline";

/**
 * Household Survey card
 */
export default class CardHouseholdSurvey extends CardItemBase {

    private isLoading: boolean = true

    /**
     * Load data before render the field.
     */
    componentDidMount() {
        this.isLoading = false;
        this.forceUpdate()
    }

    /**
     * Get Division name and path.
     *
     * @param meta {AdmDivisionMetaInterface}
     * @returns {string}
     */
    getAdministrativeDivisionBreadcrumb(meta: AdmDivisionMetaInterface) {
        let resp = meta.name;
        if (meta.parent) {
            resp = this.getAdministrativeDivisionBreadcrumb(meta.parent) + " / " + resp;
        }
        return resp;
    }

    /**
     * Render Household Survey card.
     *
     * @returns {ReactElement}
     */
    render() {
        if (this.isLoading) {
            return (<Loading key={`${this.props.data.id}_loading`} />);
        }

        // Allow API image debug.
        let xdebug = '';
        if (process.env.NEXT_PUBLIC_SIASAR3_API_DEBUG === '1') {
            xdebug = 'XDEBUG_SESSION=siasar';
        }

        let title: string = this.props.data.field_title;
        let breadcrumbRoot: any = this.props.data.field_community.meta;
        let headerContent = truncateWithEllipsis(this.getAdministrativeDivisionBreadcrumb(breadcrumbRoot), 122, EllipsisPositionEnum.Middle)

        if (!title) {
            title = this.props.data.field_community.meta.name;
            breadcrumbRoot = this.props.data.field_community.meta.parent;
        }

        return (
            <div className="card-item card-household">
                <div className="card-item-image">
                    { headerContent ? <div className="card-header"><span>{ headerContent }</span></div> : null }
                    <ImageOffline
                        src={
                            this.props.data.image ? this.props.data.image : defaultPic.src
                        }
                        alt={this.props.data.field_title || 'Image'}
                    />
                    <div className={"card-badge "/* + this.props.data.type.replace(/\./gi, "_")*/ }><span>{this.props.data.field_household_identifier ?? ''}</span></div>
                </div>
                <div className="card-item-container">
                    <div className="card-item-content card-content">
                        <h2>{title}</h2>
                        <Pill title={t("ID")} color={"red"} isUlid={true} id={this.props.data.id} />
                        <Pill 
                            title={t("Status")} 
                            color={"green"} 
                            id={!!parseInt(this.props.data.field_finished) ? t('Finished') : t('Draft')} 
                        />
                        <UserTag picture_url={this.props.data.field_interviewer.meta.gravatar} username={this.props.data.field_interviewer.meta.username}/>
                    </div>
                    {this.renderActions("card-actions")}
                </div>
            </div>
        );
    }
}


