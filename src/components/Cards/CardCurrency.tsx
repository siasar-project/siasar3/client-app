/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import CardItemBase from "@/components/Cards/CardItemBase";
import Pill from "@/components/common/Pill";

/**
 * Currency card.
 */
export default class CardCurrency extends CardItemBase {

    /**
     * Render inquiry card.
     *
     * @returns {ReactElement}
     */
    render() {
        return (
            <div className="card-item card-currency">
                <div className="card-item-container">
                    <div className="card-item-content card-currency-content">
                        <h4>{ this.props.data.name }</h4>
                        <p>{ this.props.data.address }</p>
                        <p>{ this.props.data.phone }</p>
                        <p>{ this.props.data.contact }</p>
                        <Pill title={t("ID")} color={"red"} isUlid={true} id={this.props.data.id} />
                    </div>
                    {this.props.data.defaultParametric || this.renderActions("card-currency-actions")}
                </div>
            </div>
        );
    }
}
