/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import {CardItemInterface} from "@/objects/Cards/CardItemInterface";

/**
 * Inquiry card.
 */
export default class CardItemBase extends React.Component<CardItemInterface, any> {

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props: CardItemInterface) {
        super(props);
    }
    
    /**
     * Render card actions.
     *
     * @param cardClass {string}
     *
     * @returns {ReactElement}
     */
    renderActions(cardClass: string) {
        let buttons: any = this.props.buttons.map((btn) => {
            if (btn.show(this.props.data, btn)) {
                return (
                    <button key={btn.id} id={btn.id} type="button" className={"btn "+btn.className} onClick={() => {
                        btn.onClick(this.props.data);
                    }}>
                        {btn.icon}<span className="btn-text">{btn.label}</span>
                    </button>
                );
            }
            return null;
        });
        return (
            <div className={"card-item-actions "+cardClass}>
                {buttons}
            </div>
        );
    }
    
    /**
     * Render inquiry card.
     *
     * @returns {ReactElement}
     */
    render() {
        return (
            <div className="card-item">
                <p>Card Item Base</p>
            </div>
        );
    }
}
