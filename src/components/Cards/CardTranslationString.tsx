/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { ChangeEvent } from 'react';
import CardItemBase from './CardItemBase';

/**
 * Translation string card
 */
export default class CardTranslationString extends CardItemBase {
    /**
     * setup state
     * 
     * @param props {any}
     */
    constructor(props: any) {
        super(props);

        this.state = {source: this.props.data.source, target: this.props.data.target};

        this.handleChange = this.handleChange.bind(this);
    }

    /**
     * update state and props
     * 
     * @param e {ChangeEvent<HTMLInputElement>}
     */
    handleChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState((prevState: any) => ({...prevState, target: e.target.value}))
        this.props.data.target = e.target.value;
    }

    /**
     * Renders a translation string card
     * 
     * @returns {JSX.Element}
     */
    render() {
        let classes = "card-item card-translation";
        if (this.props.data.translated) {
            classes += " translated";
        }

        return (
            <div className={classes}>
                <div className='card-item-container'>
                    <div className="translation-input">
                        <div className="source-string">{this.state.source}</div>
                        <input 
                            type="text"
                            name="translation_string"
                            id="translation_string"
                            className='translation_string'
                            value={this.state.target}
                            onChange={this.handleChange}
                        />
                    </div>
                    {this.renderActions('card-translation')}
                </div>
            </div>
        );
    }
};
