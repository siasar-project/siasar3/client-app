/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import Loading from "../common/Loading";
import Pill from "../common/Pill";
import CardItemBase from "./CardItemBase";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import {eventManager, Events, HouseholdEvents} from "@/utils/eventManager";
import {truncateWithEllipsis} from "@/utils/siasar";
import {EllipsisPositionEnum} from "@/objects/EllipsisPositionEnum";
import defaultPic from "../../../public/default.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faEdit, faHouse} from "@fortawesome/free-solid-svg-icons";
import ImageOffline from "@/components/common/ImageOffline";
import React from "react";

/**
 * Household Processes card
 */
export default class CardHouseholdProcess extends CardItemBase {

    private isLoading: boolean = true
    private breadcrumb: any[] = []

    private finishedHousehold: string
    private draftHousehold: string
    private totalHousehold: string
    private communityHouseholds: string

    /**
     * initialize household values and subscribe to event
     * 
     * @param props {any}
     */
    constructor(props: any) {
        super(props)

        this.finishedHousehold = this.props.data.finishedHousehold
        this.draftHousehold = this.props.data.draftHousehold
        this.totalHousehold = this.props.data.totalHousehold
        this.communityHouseholds = this.props.data.communityHouseholds

        eventManager.on(
            Events.HOUSEHOLD_EVENT,
            (data) => {
                if (data.event !== HouseholdEvents.SURVEY_ADDED) return;
                if (data.processId !== this.props.data.id) return;

                this.draftHousehold = (parseInt(this.draftHousehold) + 1).toString()
                this.totalHousehold = (parseInt(this.totalHousehold) + 1).toString()
                
                this.forceUpdate()
            }
        )
        eventManager.on(
            Events.HOUSEHOLD_EVENT,
            (data) => {
                if (data.event !== HouseholdEvents.SURVEY_DELETED) return;
                if (data.processId !== this.props.data.id) return;

                this.draftHousehold = (parseInt(this.draftHousehold) - 1).toString()
                this.totalHousehold = (parseInt(this.totalHousehold) - 1).toString()

                this.forceUpdate()
            }
        )
    }

    /**
     * Load data before render the field.
     */
    componentDidMount() {
        this.isLoading = false;
        this.forceUpdate()
    }

    /**
     * Get Division name and path.
     *
     * @param meta {AdmDivisionMetaInterface}
     * @returns {string}
     */
    getAdministrativeDivisionBreadcrumb(meta: AdmDivisionMetaInterface) {
        let resp = meta.name;
        if (meta.parent) {
            resp = this.getAdministrativeDivisionBreadcrumb(meta.parent) + " / " + resp;
        }
        return resp;
    }

    /**
     * Render Household Processes card.
     *
     * @returns {ReactElement}
     */
    render() {
        if (this.isLoading) {
            return (<Loading key={`${this.props.data.id}_loading`} />);
        }

        // Allow API image debug.
        let xdebug = '';
        if (process.env.NEXT_PUBLIC_SIASAR3_API_DEBUG === '1') {
            xdebug = 'XDEBUG_SESSION=siasar';
        }

        let title: string = this.props.data.field_title;
        let breadcrumbRoot: any = this.props.data.administrativeDivision.meta;
        let headerContent = truncateWithEllipsis(this.getAdministrativeDivisionBreadcrumb(breadcrumbRoot), 122, EllipsisPositionEnum.Middle)

        if (!title) {
            title = this.props.data.administrativeDivision.meta.name;
            breadcrumbRoot = this.props.data.administrativeDivision.meta.parent;
        }

        return (
            <div className="card-item card-household">
                <div className="card-item-image">
                    { headerContent ? <div className="card-header"><span>{ headerContent }</span></div> : null }
                    <ImageOffline
                        src={
                            this.props.data.image ? this.props.data.image : defaultPic.src
                        }
                        alt={this.props.data.field_title || 'Image'}
                    />
                </div>
                <div className="card-item-container">
                    <div className="card-item-content card-content">
                        <h2>{title}</h2>
                        <Pill title={t("ID")} color={"red"} isUlid={true} id={this.props.data.id} />
                        <div className="process-status">
                            <Pill title={<FontAwesomeIcon icon={faEdit}/>} color={"orange"} id={this.draftHousehold} />
                            <Pill title={<FontAwesomeIcon icon={faCheck}/>} color={"green"} id={this.finishedHousehold} />
                            <Pill title={<FontAwesomeIcon icon={faHouse}/>} color={"blue"} id={this.totalHousehold + " / " + this.communityHouseholds} />
                        </div>
                    </div>
                    {this.renderActions("card-actions")}
                </div>
            </div>
        );
    }
}

