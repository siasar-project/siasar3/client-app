/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import React from "react";
import t from "@/utils/i18n";
import CardItemBase from "@/components/Cards/CardItemBase";
import Pill from "@/components/common/Pill";
import {getLuxonDate, getRelativeDate, getTimezone, truncateWithEllipsis} from "@/utils/siasar";
import {EllipsisPositionEnum} from "@/objects/EllipsisPositionEnum";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import defaultPic from '../../../public/default.png'
import IndicatorIcon from "@/components/common/IndicatorIcon";
import ImageOffline from "@/components/common/ImageOffline";

/**
 * Inquiry card.
 */
export default class CardInquiry extends CardItemBase {
    protected lastUpdate = this.props.data.field_editors_update[this.props.data.field_editors_update.length - 1];

    /**
     * Get Division name and path.
     *
     * @param meta {AdmDivisionMetaInterface}
     * @returns {string}
     */
    getAdministrativeDivisionBreadcrumb(meta: AdmDivisionMetaInterface) {
        if (!meta) {
            return "";
        }

        let resp = meta.name;

        // if (meta.parent) {
        if ((meta.parent && !Array.isArray(meta.parent))|| (Array.isArray(meta.parent) && meta.parent.length > 0)) {
            resp = this.getAdministrativeDivisionBreadcrumb(meta.parent) + " / " + resp;
        }
        return resp;
    }

    /**
     * returns appropriate pill color depending on date of creation
     * 
     * @returns {"green" | "orange" | "red"}
     */
    getAntiquityColor() {
        let userTimezone = getTimezone();
        let relativeDate = getRelativeDate(this.lastUpdate.value, this.lastUpdate.timezone, userTimezone);
        let luxonDate = getLuxonDate(relativeDate, userTimezone);
        if (undefined === luxonDate) { return "green"; }
        let yearDiff = Math.trunc(Math.abs(luxonDate.diffNow('years').years));

        if (yearDiff < 1) { return "green"; }
        if (yearDiff < 3) { return "orange"; }
        return "red";
    }

    /**
     * Render inquiry card.
     *
     * @returns {ReactElement}
     */
    render() {
        // Allow API image debug.
        let xdebug = '';
        if (process.env.NEXT_PUBLIC_SIASAR3_API_DEBUG === '1') {
            xdebug = 'XDEBUG_SESSION=siasar';
        }

        let title: any = this.props.data.field_title;
        let breadcrumbRoot: any = this.props.data.field_region.meta;
        let headerContent = truncateWithEllipsis(this.getAdministrativeDivisionBreadcrumb(breadcrumbRoot), 122, EllipsisPositionEnum.Middle)

        if (title === '') {
            if (this.props.data.field_region.meta) {
                title = this.props.data.field_region.meta.name;
                breadcrumbRoot = this.props.data.field_region.meta.parent;
            }
        } else if (title && typeof title === 'object') {
            title = title.meta.name;
        }

        // ✅ Capitalize first letter
        const status = this.props.data.field_status;
        const statusLabel = status.charAt(0).toUpperCase() + status.slice(1);
        // Card error or warning background.
        let alertClass = '';
        if (this.props.data.logs) {
            if (this.props.data.logs.errors > 0) {
                alertClass = 'card-error';
            } else if (this.props.data.logs.warnings > 0) {
                alertClass = 'card-warning';
            }
        }

        let hhPill = false;
        let hhLabel = '';
        if (undefined !== this.props.data.field_have_households) {
            hhPill = true;
            hhLabel = (this.props.data.field_have_households) ? t("With households") : t("Without households");
        }
        return (
            <div className={"card-item card-inquiry " + alertClass}>
                <div className="card-item-image">
                    {headerContent ? <div className="card-header"><span>{headerContent}</span></div> : null}
                    <ImageOffline
                        src={
                            this.props.data.field_image && this.props.data.field_image[0] && this.props.data.field_image[0].meta.size ?
                                this.props.data.field_image[0].meta.url :
                                defaultPic.src
                        }
                        alt={this.props.data.field_title || 'Image'}
                    />
                    <div className={"card-badge " + this.props.data.type.replace(/\./gi, "_") }><span>{this.props.data.type_title}</span></div>
                </div>
                <div className="card-item-container">
                    <div className="card-item-content card-content">
                        <h2 className={'card-title'}><IndicatorIcon value={this.props.data.indicator_value}/>{title}</h2>
                        <Pill title={t("Status")} color={"green"} id={t(statusLabel)}/>
                        <Pill title={t("ID")} color={"green"} isUlid={true} id={this.props.data.field_ulid_reference}/>
                        <Pill title={t("S/N")} color={"red"} isUlid={true} id={this.props.data.id}/>
                        <Pill title={t("Chg.")} color={this.getAntiquityColor()} id={this.lastUpdate.value} />
                        {hhPill ? <Pill title={t("H.")} color={"purple"} id={hhLabel} /> : null }
                    </div>
                    {this.renderActions("card-actions")}
                </div>
            </div>
        );
    }
}
