/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {Component, ReactElement} from "react";
import Image, {ImageLoader} from "next/image";
import {User} from "@/objects/User";
import {AppBar} from "@material-ui/core";
import Link from "next/link";
import Typography from "@material-ui/core/Typography";
import {smGetSession, smHasPermission, smIsLoading, smIsValidSession} from "@/utils/sessionManager";
import OptionalLink from "@/components/common/OptionalLink";
import Switch from "@/components/common/Switch";
import t from "@/utils/i18n";
import getMainMenuOptions from "../../Settings/mainMenu";
import {MenuOptionInterface} from "@/objects/MenuOptionInterface";
import {eventManager, Events} from "@/utils/eventManager";
import defaultLogo from "../../../public/siasar_logo_blue.png";
import UnreadMailCounter from "@/components/common/UnreadMailCounter";
import OfflineBottom from "@/components/common/OfflineBottom";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import UserTag from "@/components/common/UserTag";

interface IAppProps {
}

interface IAppState {
    user?: User;
    anchorEl: any;
    mainMenuToggle: boolean;
}

/**
 * Image loader.
 *
 * @param root
 * @param {string} root.src
 *   Source URL.
 * @param {number} root.width
 *   Image width.
 * @param {number|undefined} root.quality
 *   Image compression quality.
 *
 * @returns {string}
 *   Image path.
 */
const myLoader: ImageLoader = ({src, width, quality}) => {
    return `${src}?w=${width}&q=${quality || 75}`;
};

/**
 * Main menu bar.
 */
class AppTopBar extends Component<IAppProps, IAppState> {
    protected isMaintenanceMode:boolean = false;

    /**
     * Component did mount
     */
    componentDidMount() {
        // Notify that this component did mount.
        eventManager.dispatch(Events.COMP_MOUNT, this);
    }

    /**
     * Init component.
     *
     * @param props
     */
    constructor(props: IAppProps) {
        super(props);

        this.state = {
            anchorEl: null,
            mainMenuToggle: false,
        };

        // Bind "this" value to handleChange.
        this.handleChange = this.handleChange.bind(this);

        /**
         * If the API set maintenance mode lock the user.
         */
        eventManager.on(Events.APP_MAINTENANCE, (data) => {
            this.isMaintenanceMode = true;
            this.forceUpdate();
        });

    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    handleChange(event: any) {
        this.setState({mainMenuToggle: event.target.checked});
    }

    /**
     * Build main menu options components tree
     *
     * @param menuOptions {Array<MenuOptionInterface>}
     *   Menu options data.
     * @returns {Array<ReactElement>}
     *   Menu options components.
     */
    buildMenuOptions(menuOptions: MenuOptionInterface[] | undefined): ReactElement[] {
        let menuOptionsResult = [];

        if (menuOptions !== undefined) {
            for (let i = 0; i < menuOptions.length; i++) {
                let menuOption = menuOptions[i];

                // Validate environment.
                if (menuOption.hidden) {
                    continue;
                }
                // If need session or have permissions, check to have a valid session.
                if ((menuOption.needSession === true || menuOption.permissions !== undefined && menuOption.permissions.length > 0) && !smIsValidSession()) {
                    continue;
                }
                // If have permissions, check them.
                if (menuOption.permissions !== undefined) {
                    let passPermissions = true;

                    for (let j = 0; j < menuOption.permissions.length; j++) {
                        if (passPermissions && !smHasPermission(menuOption.permissions[j])) {
                            passPermissions = false;
                        }
                    }

                    if (!passPermissions) {
                        continue;
                    }
                }

                let item = null;
                let itemChildren = this.buildMenuOptions(menuOption.children);

                // If have a valid url, build as OptionalLink. Can adicionally have children.
                if (menuOption.url !== undefined && menuOption.url !== "") {
                    item = (
                        <li key={i} className="menu-item">
                            <OptionalLink condition={!menuOption.disabled && !smIsLoading()} href={menuOption.url}>{t(menuOption.label)}</OptionalLink>
                            {itemChildren.length > 0 ? (
                                <ul className="menu">
                                    {itemChildren}
                                </ul>
                            ) : (
                                ""
                            )}
                        </li>
                    );
                } else if (itemChildren.length > 0) {     // Only render if have children, as Typography.
                    item = (
                        <li key={i} className="menu-item expander">
                            <Typography color={"textSecondary"}>{t(menuOption.label)}</Typography>
                            <ul className="menu">
                                {itemChildren}
                            </ul>
                        </li>
                    );
                }

                // Add the item to render if applicable.
                if (item) {
                    menuOptionsResult.push(item);
                }
            }
        }

        return menuOptionsResult;
    }

    /**
     * Render menu options.
     *
     * @returns {any}
     *   Menu structure.
     */
    render() {
        let mainMenuOptions = getMainMenuOptions();
        let menuOptions = this.buildMenuOptions(mainMenuOptions.children);
        let username_block = (<></>);
        let username_mobile_block_top = (<></>);
        let username_mobile_block_bottom = (<></>);

        if (!smIsValidSession()) {
            username_block = (
                <div className={'user-links'}>
                    <Link href={"/login"} passHref>
                        <a className={'login'}>{t("Log-in")}</a>
                    </Link>
                </div>
            );
        } else {
            let user = smGetSession();
            username_block = (
                <div className={'userMenu only-desktop'}>
                    <div className={'userMenu-info'}>
                        <UserTag picture_url={user?.gravatar || ""} username={user?.username || ""}/>
                    </div>
                    {dmIsOfflineMode() ? (<></>) : (
                        <div className="user-links">
                            <Link href={"/logout"} passHref>
                                <a className={'logout'}>{t("Logout")}</a>
                            </Link>
                        </div>
                    )}
                </div>
            );
            username_mobile_block_top = (
                <div className={'userMenu menuTop only-mobile'}>
                    <div className={'userMenu-wrapper'}>
                        <UserTag picture_url={user?.gravatar || ""} username={user?.username || ""}/>
                    </div>
                </div>
            );
            username_mobile_block_bottom = (
                <div className={'userMenu menubottom only-mobile'}>
                    <div className="user-links">
                        <Link href={"/logout"} passHref>
                            <a className={'logout'}>{t("Logout")}</a>
                        </Link>
                    </div>
                </div>
            );
        }

        return (
            <>
                <div>
                    {this.isMaintenanceMode ? (<div className={"maintenance-mode-label"}>{t('Maintenance mode')}</div>) : (<></>)}
                    <AppBar position={"static"}>
                        <div className={'header-top'}>
                            <div className="site-logo">
                                <Link href="/" passHref>
                                    <a>
                                        <Image
                                            src={defaultLogo}
                                            unoptimized={true}
                                            width={''}
                                            height={''}
                                            layout="intrinsic"
                                            alt="SIASAR Logo"
                                            className={'logo'}
                                        />
                                    </a>
                                </Link>
                            </div>
                            {dmIsOfflineMode() ? (<></>) : (
                                <UnreadMailCounter/>
                            )}
                            <OfflineBottom/>
                            {username_block}
                            <div className={'only-mobile'}>
                                <div className="menu-toggle">
                                    <Switch
                                        key="main-menu-toggle"
                                        id="main-menu-togle"
                                        className="main-menu-toggle"
                                        value={this.state.mainMenuToggle}
                                        onChange={this.handleChange}
                                    />
                                    <div className="menu-icon" >
                                        <div className="menu-line line-1"></div>
                                        <div className="menu-line line-2"></div>
                                        <div className="menu-line line-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="top-toolbar" className={'top-toolbar' + (this.state.mainMenuToggle ? " visible" : "")}>
                            {username_mobile_block_top}
                            <ul className={"menu main-menu"}>
                                {menuOptions}
                            </ul>
                            {username_mobile_block_bottom}
                        </div>
                    </AppBar>
                </div>
            </>
        );
    }
}

export default AppTopBar;
