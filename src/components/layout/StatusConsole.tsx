/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {Component} from "react";
import t from "@/utils/i18n";
import {eventManager, Events, StatusEvent, StatusEventLevel} from "@/utils/eventManager";
import Draggable, {ControlPosition, DraggableEvent, DraggableData} from 'react-draggable';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEraser} from "@fortawesome/free-solid-svg-icons/faEraser";
import {cmClearCache} from "@/utils/cacheManager";
import {smGetSession, smSetSession} from "@/utils/sessionManager";
import {decodeHtmlEntities} from "@/utils/siasar";
import {faUnlink} from "@fortawesome/free-solid-svg-icons";
import offlineDataRepository from "@/utils/offlineDataRepository";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

interface IStatusConsoleProps {
    classes?: string;
}

interface IStatusConsoleState {
    isVisible: boolean;
    position: ControlPosition;
    length: number;
}

/**
 * Status Console.
 */
class StatusConsole  extends Component<IStatusConsoleProps, IStatusConsoleState> {
    protected events: StatusEvent[] = [];

    /**
     * Init component.
     *
     * @param props
     */
    constructor(props: IStatusConsoleProps) {        
        super(props);

        this.state = {
            isVisible: false,
            position: {x: 0, y: 0},
            length: 0,
        };
        
        // Bind "this" value to handlers.
        this.addEvent = this.addEvent.bind(this);
        this.showConsole = this.showConsole.bind(this);
        this.hideConsole = this.hideConsole.bind(this);
        this.handleStop = this.handleStop.bind(this);

        // Register callback function in event manager dispatcher.
        eventManager.on(Events.STATUS_ADD, (event: StatusEvent) => {
            this.addEvent(event);
        });

        // Display status console.
        eventManager.on(Events.STATUS_SHOW, (event: StatusEvent) => {
            this.showConsole();
        });

        // Register callback function in event manager dispatcher to know when AppTopBar component did mount.
        eventManager.on(Events.COMP_MOUNT, (component: Component) => {
            // Check if component is AppTopBar.
            if (component.constructor.name === 'AppTopBar') {
                // Register callback function on 3 clicks in trigger element, to show the status console.
                let triggerElement = document.querySelector('.MuiAvatar-root');
                
                // Check if trigger element exists.
                if (triggerElement !== null) {
                    triggerElement.addEventListener('click', (ev: any) => {
                        // If event have 3 clicks.
                        if (ev.detail === 3) {
                            this.showConsole();
                        }
                    });
                }
            }
        });

        // Add current online mode at start.
        eventManager.dispatch(Events.STATUS_ADD, {
            level: (dmIsOfflineMode() ? StatusEventLevel.WARNING : StatusEventLevel.SUCCESS),
            title: t("Mode"),
            message: t("Working in @mode mode", {"@mode": (dmIsOfflineMode() ? "Offline" : "Online")}),
            isPublic: false,
        });
    }

    /**
     * Add a event.
     * 
     * @param event {StatusEvent}
     */
    addEvent(event: StatusEvent) {
        // Add the current time (hh:mm:ss) to event.
        event.time = new Date().toISOString().slice(11, 19);
        event.message = decodeHtmlEntities(event.message);
        this.events.push(event);

        // Remove the first event (older) until keep only the last 100.
        while (this.events.length > 100) {
            this.events.shift();
        }

        this.setState({length: this.events.length});
    }

    /**
     * Show the status console.
     */
    showConsole() {
        this.setState({isVisible: true});
    }

    /**
     * Hide the status console.
     */
    hideConsole() {
        this.setState({isVisible: false});
    }

    /**
     * Hold position.
     * 
     * @param e {DraggableEvent}
     * @param data {DraggableData}
     */
    handleStop(e: DraggableEvent, data: DraggableData) {
        this.setState({position: {x: data.x, y: data.y}});
    }

    /**
     * Force session to expire.
     */
    handleExpireSession() {
        let session = smGetSession();
        if (session && session?.session?.refresh_token_expire) {
             session.session.refresh_token_expire = 0;
        }
        smSetSession(session);
    }

    /**
     * Display in console log all offline items saved.
     */
    handleOfflineItemLogs() {
        offlineDataRepository.getAllItems()
            .then((items) => {
                console.log(items);
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.INFO,
                        title: t("Offline items"),
                        message: t("You can see offline items in the console log"),
                        isPublic: true,
                    }
                );
            });
    }
    
    /**
     * Render the events.
     *
     * @returns {ReactElement}
     */
    renderEvents() {        
        // Build the event rows in reverse order (newer events first).
        let events = [];
        for (let i = this.events.length - 1; i >= 0; i--) {
            let message = this.events[i].message;

            events.push(
                <tr className={'level-'+this.events[i].level} key={"row-"+i}>
                    <td className="time">{this.events[i].time}</td>
                    <td className="is-public">{this.events[i].isPublic ? t('Yes') : t('No')}</td>
                    <td className="level">{this.events[i].level}</td>
                    <td className="message">
                        {this.events[i].title ?
                            (<><strong>{this.events[i].title}</strong><br/></>)
                        : ('')}
                        {message}
                    </td>
                    {/* <td className="actions"></td> */}
                </tr>
            );
        }

        return (
            <>
                <div className={"status-console-toolbar"}>
                    <button
                        type="button"
                        key="button-clear"
                        className={"btn"}
                        onClick={() => {
                            this.events.length = 0;
                            this.setState({length: 0});
                        }}
                        >
                        <FontAwesomeIcon icon={faEraser} /> {t("Clear")}
                    </button>
                    {!dmIsOfflineMode() ? (
                        <>
                            <button
                                type="button"
                                key="button-cache-clear"
                                className={"btn"}
                                onClick={() => {
                                    cmClearCache();
                                }}
                            >
                                <FontAwesomeIcon icon={faEraser} /> {t("Cache clear")}
                            </button>
                            <button
                                type="button"
                                key="button-expire-session"
                                className={"btn"}
                                onClick={this.handleExpireSession}
                            >
                                <FontAwesomeIcon icon={faEraser} /> {t("Expire Session")}
                            </button>
                        </>
                    ) : (<></>)}
                    <button
                        type="button"
                        key="button-log-offline-items"
                        className={"btn"}
                        onClick={this.handleOfflineItemLogs}
                    >
                        <FontAwesomeIcon icon={faUnlink}/> {t("Offline items")}
                    </button>
                </div>
                <table>
                    <thead>
                    <tr>
                        <th>{t('Time')}</th>
                        <th>{t('Pub.')}</th>
                        <th>{t('Level')}</th>
                        <th>{t('Message')}</th>
                        {/* <th>{t('Actions')}</th> */}
                    </tr>
                    </thead>
                    <tbody>
                    {events}
                    </tbody>
                </table>
            </>
        );
    }

    /**
     * Render the console.
     *
     * @returns {ReactElement}
     */
    renderConsole() {
        return (
            <Draggable
                handle=".status-console-handle"
                defaultPosition={{x: 0, y: 0}}
                position={this.state.position}
                onStop={this.handleStop}
                >
                <div className={"status-console "+this.props.classes}>
                    <div className="status-console-handle">
                        {t('Status Console')}
                        <button onClick={this.hideConsole}>X</button>
                    </div>
                    <div className="status-console-body">
                        {this.renderEvents()}
                    </div>
                </div>
            </Draggable>
        )
    }

    /** 
     * Render the status console.
     * 
     * @returns {ReactElement}
     */
    render() {
        return (
            <>
                {this.state.isVisible ? 
                    this.renderConsole()
                : '' }
            </>
        );
    }
}

export default StatusConsole;
