/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {eventManager, Events} from "@/utils/eventManager";
import {useEffect, useState} from "react";
import {ApiVersionInterface} from "@/objects/ApiVersionInterface";
import {dmGetApiVersion, dmGetCurrentHost} from "@/utils/dataManager/Api";

/**
 * Page footer component.
 *
 * @returns {any}
 *   Footer tags.
 */
export default function Footer() {
    // API Version.
    const [apiVer, setApiVer] = useState({
        title: "",
        description: "",
        version: "---",
        terms_of_service: "",
        licence: "",
    });
    // This app version.
    let version = 'dev';
    if (process.env.NEXT_PUBLIC_APP_VERSION) {
        version = process.env.NEXT_PUBLIC_APP_VERSION;
    }
    const [retries, setRetries] = useState(0); 
    const [currentHost, setCurrentHost] = useState('');

    /**
     * Get API version to display.
     */
    useEffect( () =>{
        dmGetApiVersion()
            .then((data: ApiVersionInterface) => {
                setApiVer(data);
            })
            .catch(() => {
                if (retries < 5) {
                    setTimeout(() => {
                        setRetries(retries + 1);
                    }, 5000);
                }
            });
    },[retries])

    useEffect( () =>{
        setTimeout(updateHostString, 1000);
    },[])

    /**
     * Update host message.
     */
    const updateHostString = () => {
        // console.log('updateHostString', dmGetCurrentHost());
        setCurrentHost(dmGetCurrentHost());
        setTimeout(updateHostString, 1000);
    }

    /**
     * Open status console.
     */
    const showConsole = () => {
        // Dispatch status event.
        eventManager.dispatch(Events.STATUS_SHOW, {});
    };

    return (
        <footer className={"footer"}>
            <div>SIASAR Client v. {version} ·|· API v. {apiVer.version} ·|· <a onClick={showConsole}>Console</a></div>
            <div>Connected to: {currentHost}</div>
        </footer>
    )
}
