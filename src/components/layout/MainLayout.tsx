/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, { Component } from 'react'
import {createStyles, Theme, WithStyles, withStyles} from '@material-ui/core/styles';
import {Fab} from "@material-ui/core"
import Head from 'next/head'
import AppTopBar from "./AppTopBar";
// import { SiasarContext } from "../../context/SiasarContext";
import ScrollToTop from './ScrollToTop';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

/**
 * Theme extension.
 *
 * @param {Theme} theme
 *   Theme to update.
 * @returns {Theme}
 *   Updated theme.
 */
const styles = (theme: Theme) => createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
})

interface IProps { }
interface IState { }

/**
 * Main HTML structure.
 */
class MainLayout extends Component<IProps & WithStyles<typeof styles>, IState> {

    // static contextType = SiasarContext;

    /**
     * Hide loading.
     */
    componentDidMount() {
        // this.context.setLoading(false);
    }

    /**
     * Return HTML structure.
     *
     * @returns {any}
     *   HTML structure.
     */
    render() {
        const { children, classes } = this.props
        // const { loading } = this.context;

        return <>
            <Head>
                <meta charSet='utf-8' />
                <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
                <meta name='viewport' content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=2' />
                {/*<meta name='description' content='Description' />*/}
                {/*<meta name='keywords' content='Keywords' />*/}
                <link rel='manifest' href='/manifest.json' />
                <link href='/icon/favicon-16x16.png' rel='icon' type='image/png' sizes='16x16' />
                <link href='/icon/favicon-32x32.png' rel='icon' type='image/png' sizes='32x32' />
                <link rel='apple-touch-icon' href='/apple-icon.png'></link>
                <meta name='theme-color' content='#317EFB' />
            </Head>

            <AppTopBar/>
            { children }
            <ScrollToTop {...this.props}>
              <Fab color="secondary" size="small" aria-label="scroll back to top">
                <KeyboardArrowUpIcon />
              </Fab>
            </ScrollToTop>
            {/*<Backdrop*/}
            {/*  className={classes.backdrop}*/}
            {/*  // open={loading}*/}
            {/*>*/}
            {/*  <CircularProgress color="inherit" />*/}
            {/*</Backdrop>*/}
        </>
    }
}

export default withStyles(styles)(MainLayout);
