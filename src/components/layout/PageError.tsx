/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import Link from "next/link";
import t from "@/utils/i18n";

interface PropsInterface {
    title?: string;
    status: number;
    message: string;
    link?: {
        href: string,
        label: string,
    }
}

/**
 * Error page render.
 */
export default class PageError extends React.Component<PropsInterface> {

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        return (
            <>
                { this.props.title ? (
                    <h1>{this.props.title}</h1>
                ) : (<></>)} 
                <h2>{this.props.status} - {this.props.message}</h2>
                <Link href={this.props.link?.href ?? "/"}>
                    <a>{this.props.link?.label ?? t("Go back home")}</a>
                </Link>
            </>
        );
    }
}
