/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {Component} from "react";
// import t from "@/utils/i18n";
import {eventManager, Events, StatusEvent, StatusEventLevel} from "@/utils/eventManager";
import {Store, ReactNotifications, iNotification} from 'react-notifications-component'
import {decodeHtmlEntities} from "@/utils/siasar";

interface IStatusNotificationsProps {
    classes?: string;
}

interface IStatusNotificationsState {
    isVisible: boolean;
}

/**
 * Status Notifications.
 */
class StatusNotifications  extends Component<IStatusNotificationsProps, IStatusNotificationsState> {
    /**
     * Init component.
     *
     * @param props
     */
    constructor(props: IStatusNotificationsProps) {        
        super(props);

        this.state = {
            isVisible: true,
        };
        
        // Bind "this" value to handlers.
        this.addEvent = this.addEvent.bind(this);

        // Register callback function in event manager dispatcher.
        eventManager.on(Events.STATUS_ADD, (event: StatusEvent) => {
            this.addEvent(event);
        });
    }

    /**
     * Add a event.
     * 
     * @param event {StatusEvent}
     */
    addEvent(event: StatusEvent) {
        // Check if event is public.
        if (!event.isPublic) {
            return;
        }

        let notification: iNotification = {
            title: event.title,
            message: decodeHtmlEntities(event.message),
            insert: "bottom",
            container: "bottom-right",
            animationIn: ["animate__animated", "animate__jackInTheBox"],
            animationOut: ["animate__animated", "animate__zoomOut"],
            dismiss: {
              duration: 5000,
              onScreen: true,
              pauseOnHover: true,
            },
        };

        // Map status level to appropriated notification type.
        switch (event.level) {
            case StatusEventLevel.ERROR:
                notification.type = 'danger';
                break;
            case StatusEventLevel.WARNING:
                notification.type = 'warning';
                break;
            case StatusEventLevel.SUCCESS:
                notification.type = 'success';
                break;
            case StatusEventLevel.INFO:
                notification.type = 'info';
                break;
            case StatusEventLevel.DEBUG:
                notification.type = 'default';
                break;
        }

        Store.addNotification(notification);
    }
    
    /**
     * Show the status notifications.
     */
    showNotifications() {
        this.setState({isVisible: true});
    }

    /**
     * Hide the status notifications.
     */
    hideNotifications() {
        this.setState({isVisible: false});
    }

    /** 
     * Render the status notifications container.
     * 
     * @returns {ReactElement}
     */
    render() {
        return (
            <>
                {this.state.isVisible ? 
                    <ReactNotifications />
                : '' }
            </>
        );
    }
}

export default StatusNotifications;
