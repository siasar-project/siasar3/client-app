/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import {
    smIsLoading,
    smGetRefreshTokenExpire,
    smGetSession,
    smHasPermission,
    smHaveSession
} from "@/utils/sessionManager";
import router from "next/router";
import getFirewallRules from "../../Settings/routerFirewall";
import {RouteRule} from "@/objects/RouteRule";
import {User} from "@/objects/User";
import PageError from "@/components/layout/PageError";
import t from "@/utils/i18n";
import {Container} from "@material-ui/core";
import MainLayout from "@/components/layout/MainLayout";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

/**
 * Router firewall to validate user permissions before render.
 */
export default class RouterFirewall extends React.Component {

    /**
     * Validate user access limited by rule.
     *
     * @param user {User | null}
     * @param rule {RouteRule}
     *
     * @returns {boolean}
     */
    canAccess(user: User | null, rule: RouteRule) {
        // Require opened session?
        let allow = (rule.needSession || false) ? (!!user) : true;
        // Validate environment.
        if (rule.environment) {
            allow = allow && rule.environment === process.env.NEXT_PUBLIC_ENV;
        }
        // Validate permissions.
        if (rule.permissions && rule.permissions.length > 0) {
            for (let i = 0; i < rule.permissions.length; i++) {
                allow = allow && smHasPermission(rule.permissions[i]);
                if (!allow) {
                    break;
                }
            }
        }

        return allow;
    }

    /**
     * Render.
     *
     * @returns {React.ReactElement}
     */
    render() {
        // Get session.
        let session = smGetSession();
        let username = "Anonymous";
        if (session) {
            username = session.username ?? username;
        }
        // Get path.
        let path = router.pathname;
        let allowAccess = false;
        let match:RouteRule | undefined = undefined;
        let error = {status: 403, title: t("Forbidden"), message: t('You do not have access to this page.')};

        // If login was partially loaded, or we have session and refresh token has expired.
        if (!dmIsOfflineMode() && (path !== '/logout' && (smIsLoading() || (smHaveSession() && smGetRefreshTokenExpire() < (Date.now() / 1000)))) ){
            if (smIsLoading()) {
                error.title =  t('Session invalid');
                error.message = t('Invalid session. User redirected to logout.');
            } else {
                error.title = t('Session expired');
                error.message = t('Expired session. User redirected to logout.');
            }
            // Redirect to logout page.
            router.push('/logout');
        }
        else {
            let rules:RouteRule[] = getFirewallRules();
            let offline: boolean = dmIsOfflineMode();

            for (let i = 0; i < rules.length; i++) {
                if (process.env.NEXT_PUBLIC_DEBUG_FIREWALL === '1') console.log("Math rule?", rules[i].name);
                if (path.match(rules[i].pattern)) {
                    match = rules[i];
                    if (process.env.NEXT_PUBLIC_DEBUG_FIREWALL === '1') console.log("Rule matched", rules[i].name);
                    allowAccess = this.canAccess(session, match) && (rules[i].offline || !offline);
                    break;
                }
            }
        }

        if (!allowAccess) {
            // Dispatch status event.
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.DEBUG,
                    title: error.title,
                    message: error.message,
                    isPublic: false,
                }
            );
        }

        // Render.
        return (
            <>
                {(process.env.NEXT_PUBLIC_DEBUG_FIREWALL === '1') ? (
                    <h4>FIREWALL [{process.env.NEXT_PUBLIC_ENV} / {username}] - {match ? match.name : '!'} {"=> {"} {path} | {allowAccess ? "Allowed" : "Forbidden"}</h4>
                ) : (<></>)}
                {allowAccess ? (
                    <>{this.props.children}</>
                ) : (
                    <MainLayout>
                        <div className={'error-page'}>
                            <Container>
                                <PageError status={error.status} title={error.title} message={error.message}/>
                            </Container>
                        </div>
                    </MainLayout>
                )}
            </>
        );
    }
}
