/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {useEffect, useState} from "react";
import {SystemStatsInterface} from "@/objects/SystemStats";
import {BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title, Tooltip,} from 'chart.js';
import Loading from "@/components/common/Loading";
import RecordsByUserStats from "@/components/Stats/RecordsByUserStats";
import AllInquiriesStats from "@/components/Stats/AllInquiriesStats";
import PointsStats from "@/components/Stats/PointsStats";
import HHProcessStats from "@/components/Stats/HHProcessStats";
import {dmGetStats} from "@/utils/dataManager/Api";
import t from "@/utils/i18n";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

/**
 * Page footer component.
 *
 * @returns {any}
 *   Footer tags.
 */
export default function SystemStats() {
    const [isLoading, setIsLoading] = useState(true);
    const [stats, setStats] = useState<SystemStatsInterface>({});

    /**
     * update record list
     */
    useEffect(() => {
        if (!isLoading) {
            return;
        }

        dmGetStats()
            .then((res:any) => {
                setStats(res);
                setIsLoading(false);
            });
    }, [isLoading]);

    if (isLoading) {
        return (<Loading key={`stats_loading`} />);
    }

    let inquiriesStats: any = [];
    if (stats.points) {
        inquiriesStats.push(<PointsStats stats={stats.points} isLoading={isLoading}/>);
    }
    if (stats.inquiries) {
        for (const inquiriesKey in stats.inquiries) {
            inquiriesStats.push(<AllInquiriesStats stats={stats.inquiries[inquiriesKey]} isLoading={isLoading} />);
        }
    }
    if (stats.hhprocess) {
        inquiriesStats.push(<HHProcessStats stats={stats.hhprocess} isLoading={isLoading}/>);
    }

    return (
        <div className={"system-stats"}>
            {stats.inquiries ? (
                <div>
                    <div>{t('Click on a legend item to toggle its visibility and focus on specific data.')}</div>
                    <div className={"all-inquiries-stats-container"}>
                    {inquiriesStats}
                    </div>
                </div>
            ) : (<></>)}
            <RecordsByUserStats stats={stats} isLoading={isLoading} />
        </div>
    )
}
