/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {User} from "@/objects/User";
import {SystemPermission} from "@/utils/permissionsManager";
import {stringToBoolean} from "../utils/siasar";
import Api from "../utils/api";
import {db} from "../utils/db";
import {cmClearCache} from "@/utils/cacheManager";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {dmGetUser} from "@/utils/dataManager";
import {dmGetAdministrativeDivision} from "@/utils/dataManager/ParametricRead";
import {AdministrativeDivision} from "@/objects/AdministrativeDivision";
import {startsWith} from "lodash";

/**
 * Server alive tests.
 *
 * This interval generates a session refresh if required.
 */
setInterval(() => {
    if (smHaveSession()) {
        // todo This call generate a server side error:
        //  "error - unhandledRejection: Invalid session!" that we need find how to stop.
        if (process.env.NEXT_PUBLIC_SIASAR3_API_AUTOSESSIONALIVE === '1') {
            if (!dmIsOfflineMode()) {
                Api.pingServer().then(() => true).catch(err => true);
            }
        }
    }
}, 20 * 1000);

/**
 * Are we working in server rendering?
 */
export const isServerSideRendering = typeof window === 'undefined'

/**
 * Get stored user session.
 *
 * @returns {User | null}
 *   Get stored user session.
 */
export function smGetSession():User | null {
    let session = null;
    if (typeof localStorage !== 'undefined') {
        // Load session from local storage.
        let aux = localStorage.getItem('session');
        if (aux !== null) {
            session = JSON.parse(aux);
        }
    }

    return session;
}

/**
 * Get current username.
 *
 * @returns {string}
 *   Active account name or Anonymous.
 */
export function smGetSessionName(): string {
    let user = smGetSession();
    let name = 'Anonymous';
    if (!!user) {
        name = user.username ?? name;
    }

    return name;
}

/**
 * Get current user id.
 *
 * @returns {string}
 *   Active account id or empty.
 */
export function smGetSessionId(): string {
    let user = smGetSession();
    let id = '';
    if (!!user) {
        id = user.id ?? id;
    }

    return id;
}

/**
 * Get current avatar url.
 *
 * @returns {string}
 *   Active account avatar url or empty string.
 */
export function smGetSessionAvatar(): string {
    let user = smGetSession();
    let avatarUrl = '';
    if (!!user) {
        avatarUrl = user.gravatar ?? avatarUrl;
    }

    return avatarUrl;
}

/**
 * Get current user language.
 *
 * By default use browser language.
 *
 * @returns {string}
 *   Selected user language.
 */
export function smGetSessionLanguage(): string {
    let user = smGetSession();

    return user?.language ?? "";     
}

/**
 * Store user session.
 *
 * @param value User object.
 */
export function smSetSession(value: any): void {
    localStorage.setItem('session', JSON.stringify(value));
}

/**
 * Remove stored user session.
 */
export function smClearSession(): void {
    try {
        // Empty local storage.
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            // Don't remove strings untranslated.
            if (key && !key.startsWith('i18n_strings.untranslated')) {
                localStorage.removeItem(key);
                i--;
            }
        }
        // Clear current host.
        localStorage.removeItem('current_host');
        // Empty local indexedDB.
        db.documents.clear();
        // Clear cache.
        cmClearCache();
    } catch (e) {
        // Nothing to do.
    }
}

/**
 * Is the current user session valid?
 *
 * If the session is expired return false.
 *
 * @returns {boolean}
 */
export function smIsValidSession(): boolean {
    let response = false;
    let session = smGetSession();
    if (session && session?.session?.token) {
        // Validate session.
        response = !!session?.session?.token;
    }

    return response;
}

/**
 * Have we a user session?
 *
 * @returns {boolean}
 *   True if we have a session token, valid or not.
 */
export function smHaveSession(): boolean {
    return (smGetToken() !== '');
}

/**
 * Get current session token.
 *
 * @returns {string}
 */
export function smGetToken(): string {
    let session = smGetSession();
    if (session && session?.session?.token) {
        return session?.session?.token;
    }

    return '';
}

/**
 * Get current refresh token.
 *
 * @returns {string}
 */
export function smGetRefreshToken(): string {
    let session = smGetSession();
    if (session && session?.session?.refresh_token) {
        return session?.session?.refresh_token;
    }

    return '';
}

/**
 * Get current refresh token expire.
 *
 * @returns {number}
 */
 export function smGetRefreshTokenExpire(): number {
    let session = smGetSession();
    if (session && session?.session?.refresh_token_expire) {
        return session?.session?.refresh_token_expire;
    }

    return 0;
}

/**
 * Have the user this permission?
 *
 * @param id {string}
 *   Permission Id.
 * @returns {boolean}
 *   True if the user has the permission.
 */
export function smHasPermission(id:string): boolean {
    if ("all permissions" !== id) {
        if (smHasPermission("all permissions")) {
            return true;
        }
    }

    let user = smGetSession();
    for (let i=0; i < (user?.permissions.length ?? 0); ++i) {
        let permission: SystemPermission | undefined = user?.permissions[i];
        if (permission && permission.id === id) {
            return true;
        }
    }
    return false;
}

/**
 * Is this user inside the selected region?
 *
 * @param id {string} The selected region ID, if empty the user is in, return TRUE.
 *
 * @returns {Promise} Revolve to TRUE if the user is in the reference region.
 */
export function smUserInRegion(id:string): Promise<any> {
    if ("" === id || smHasPermission('all administrative divisions')) {
        return new Promise((resolve) => resolve(true));
    }
    return dmGetAdministrativeDivision(id)
        .then((requiredAdministrativeDivision: AdministrativeDivision) => {
            let requiredBranch = requiredAdministrativeDivision.branch;
            // Take each division of the user and see if the required division begins with one of the user.
            let session = smGetSession();
            if (session) {
                return dmGetUser(session.id || '').then((user: any) => {
                    if (user?.administrativeDivisions) {
                        let middlePromise2 = [];
                        for (let i = 0; i < user?.administrativeDivisions.length; i++) {
                            middlePromise2.push(
                                dmGetAdministrativeDivision(user?.administrativeDivisions[i].id)
                                    .then((data: AdministrativeDivision) => {
                                        return startsWith(requiredBranch, data.branch);
                                    })
                            );
                        }
                        return Promise.all(middlePromise2)
                            .then((results: boolean[]) => {
                                if (0 === results.length) {
                                    // If the user don't have any administrative division limit, it is a all A.DD.
                                    return true;
                                }
                                // results is an array of booleans corresponding to the resolutions of each promise.
                                // We check if any element is true with the .some() method.
                                return results.some(result => result);
                            });
                    }
                });
            } else {
                return new Promise((resolve) => resolve(false));
            }
        });
}

/**
 * Add a new user permission.
 *
 * @param permission {SystemPermission}
 */
export function smAddPermissionToUser(permission: SystemPermission) {
    if (!smHasPermission(permission.id)) {
        let user = smGetSession();
        user?.permissions.push(permission);
        smSetSession(user);
    }
}

/**
 * Is the system loading context?
 *
 * @returns {boolean}
 */
export function smIsLoading(): boolean {
    try {
        return stringToBoolean(localStorage.getItem('loading_session'));
    } catch (e) {
        return false;
    }
}

/**
 * Set loading context status.
 *
 * @param loading {boolean}
 */
export function smSetLoading(loading:boolean): void {
    if (!loading) {
        localStorage.removeItem('loading_session');
        return;
    }
    localStorage.setItem('loading_session', (loading ? 'true' : 'false'));
}
