/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {ButtonInterface} from "@/objects/Buttons/ButtonInterface";

/**
 * Buttons Manager.
 */
 export default class ButtonsManager  {
  protected buttons: ButtonInterface[] = [];
  protected isLoading: boolean = false;

  /**
   * Constructor.
   */
  constructor() {
    this.haveButton = this.haveButton.bind(this);
    this.addButton = this.addButton.bind(this);
    this.getButtons = this.getButtons.bind(this);
  }

  /**
   * Check id the button exists.
   * 
   * @param id {string}
   * 
   * @returns {boolean}
   */
  haveButton(id: string) {
    // Find is button already exists.
    return this.buttons.findIndex(b => id === b.id) >= 0;
  }

  /**
   * Add button.
   * 
   * @param button {ButtonInterface}
   * @param atEnd {boolean}
   */
  addButton(button: ButtonInterface, atEnd: boolean = false) {
    // Add the button if not found.
    if (!this.haveButton(button.id)) {
      if (atEnd) {
        this.buttons.push(button);
      } else {
        this.buttons.unshift(button);
      }
    }
  }

  /**
   * Return the buttons.
   * 
   * @returns {ButtonInterface[]}
   */
  getButtons() {
    return this.buttons;
  }
}
