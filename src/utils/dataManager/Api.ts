/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {ApiVersionInterface} from "@/objects/ApiVersionInterface";
import {cmGetItem, cmSetItem} from "@/utils/cacheManager";
import api from "@/utils/api";
import {CACHE_APP_VERSION} from "@/utils/dataManager";
import Api from "@/utils/api";
import {HostAvailable} from "@/objects/HostAvailable";
import {SystemStatsInterface} from "@/objects/SystemStats";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import t from "@/utils/i18n";

/**
 * Get API version.
 *
 * @returns {Promise<ApiVersionInterface>}
 */
export function dmGetApiVersion(): Promise<ApiVersionInterface> {
    if (dmIsOfflineMode()) {
        return new Promise((resolve) => resolve({
            title: t('Offline'),
            description: '',
            version: t('Offline'),
            terms_of_service: '',
            licence: '',
        }));
    }
    return cmGetItem(CACHE_APP_VERSION)
        .then((ver: ApiVersionInterface) => {
            if (ver) {
                return new Promise((resolve) => resolve(ver));
            }
            return api.getVersion()
                .then((data: ApiVersionInterface) => {
                    cmSetItem(CACHE_APP_VERSION, data);
                    return data as ApiVersionInterface;
                });
        });
}

/**
 * Get current host.
 *
 * @returns {string}
 */
export function dmGetCurrentHost(): string {
    if (dmIsOfflineMode()) {
        return t('Offline');
    }

    return Api.getCurrentHost();
}

/**
 * Change current host.
 *
 * @param host {string} The new host.
 */
export function dmMoveToHost(host: string) {
    if (dmIsOfflineMode()) {
        return;
    }

    Api.moveToHost(host);
}

/**
 * Get available host list.
 *
 * @returns {HostAvailable[]}
 */
export function dmGetAvailableHosts(): HostAvailable[] {
    if (dmIsOfflineMode()) {
        return [];
    }

    return Api.getAvailableHosts();
}

/**
 * Get system stats.
 *
 * @returns {Promise}
 */
export function dmGetStats() {
    if (dmIsOfflineMode()) {
        return new Promise((resolve) => resolve({}));
    }

    return new Promise((resolve) => {
        return Api.getStats()
            .then((res:SystemStatsInterface) => {
                return resolve(res);
            })
            .catch(reason => {
                // Else, return nothing.
                return resolve([]);
            });
    });
}
