/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import api from "@/utils/api";
import {InquiryStatusEnum} from "@/objects/InquiryStatusEnum";
import {smGetSession} from "@/utils/sessionManager";
import {dmGetEmptyFormRecord, dmGetFormStructure, dmGetUser, dmOfflineIntercept} from "@/utils/dataManager";
import {db} from "@/utils/db";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import offlineDataRepository from "@/utils/offlineDataRepository";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {dmClearFormRecord} from "@/utils/dataManager/FormRecordWrite";
import {getTimezone} from "@/utils/siasar";
import {dmGetAdministrativeDivision, dmGetAdministrativeDivisionMeta} from "@/utils/dataManager/ParametricRead";
import {User} from "@/objects/User";

/**
 * closes the specified process, preventing more household questionnaires to be created
 *
 * @param processId {string}
 *
 * @returns {Promise}
 */
export function dmCloseHouseholdProcess(processId:string) {
    if (!dmOfflineIntercept('dmCloseHouseholdProcess')) {
        return api.closeHouseholdProcess(processId)
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * creates a new household questionnaire for the given process
 *
 * @param processId {string}
 *
 * @returns {Promise}
 */
export function dmAddHouseholdSurvey(processId: string) {
    if (!dmOfflineIntercept('dmAddHouseholdSurvey')) {
        return api.addHouseholdSurvey(processId)
    } else {
        return dmGetHouseholdProcess(processId)
            .then((process) => {
                return dmGetAdministrativeDivision(process.administrativeDivision.id)
                    .then((division) => {
                        return dmGetAdministrativeDivisionMeta(division)
                            .then((regionMeta) => {
                                let user: User|null = smGetSession();
                                // $newRecord['field_household_process'] = $householdProcess;
                                // $newRecord['field_interviewer'] = $this->sessionService->getUser();
                                // $newRecord['field_community'] = $householdProcess->getAdministrativeDivision();
                                // $newRecord['field_date'] = new DateTime();
                                // $newId = $form->insert($newRecord);
                                let now:Date = new Date();
                                let record: DynFormRecordInterface = {
                                    formId: "form.community.household",
                                    data: {
                                        id: offlineDataRepository.getNewUlid(),
                                        field_household_process: {value: processId, class: "App\\Entity\\HouseholdProcess"},
                                        field_interviewer: {
                                            value: user?.id,
                                            class: "App\\Entity\\User",
                                            "meta": {
                                                "username": user?.username,
                                                "gravatar": user?.gravatar
                                            },
                                        },
                                        field_community: {
                                            value: process.administrativeDivision.id,
                                            class: "App\\Entity\\AdministrativeDivision",
                                            meta: regionMeta,
                                        },
                                        field_date: {
                                            // Use current date.
                                            "value": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                            "timezone": getTimezone(),
                                            "meta": {
                                                "real": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                            }
                                        }
                                    },
                                };
                                // Create and save the new record.
                                return offlineDataRepository.updateFormRecord(record.data.id, record.formId, record.data)
                                    .then((offlineRecord:any) => {
                                        return dmGetFormStructure(record.formId)
                                            .then((structure:any) => {
                                                return dmGetEmptyFormRecord(record.formId)
                                                    .then((emptyRecord: any) => {
                                                        // Copy all data fields to the empty record.
                                                        for (const dataKey in record.data) {
                                                            emptyRecord[dataKey] = record.data[dataKey];
                                                        }
                                                        emptyRecord = dmClearFormRecord(structure, emptyRecord);
                                                        return db.addFormRecord(emptyRecord)
                                                            .then(() => {
                                                                return emptyRecord;
                                                            });
                                                    });
                                            });
                                    });
                            });
                    });
            });
    }
}

/**
 * Get a household process by id.
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmGetHouseholdProcess(id: string) {
    if (!dmIsOfflineMode()) {
        return api.getHouseholdProcess(id.toString());
    } else {
        // Allow get local copy of household process.
        return db.getHouseholdProcess(id.toString());
    }
}

/**
 * Retrieve a list of the user households
 *
 * @param page
 * @param processId
 * @param onlyMine
 * @param status
 *
 * @returns {Promise}
 */
export function dmGetUserHouseholds(
    page: number = -1,
    processId: string = "",
    onlyMine: boolean = true,
    status: string = InquiryStatusEnum.Undefined,
) {
    let parameters: {
        page?: number,
        record_type?: string,
        field_household_process?: string,
        field_interviewer?: string,
        field_status?: string,
        field_region?: string
    } = {};
    if (status !== InquiryStatusEnum.Undefined) {
        parameters.field_status = status.toString();
    }
    if (processId !== "") {
        parameters.field_household_process = processId;
    }
    if (onlyMine) {
        let session = smGetSession();
        if (session) {
            parameters.field_interviewer = session.id;
        }
    }
    if (!dmIsOfflineMode()) {
        return api.getUserHouseholds(page, parameters).then(data => data);
    } else {
        // Allow return a local copy of households.
        return db.getFormRecords(page, parameters).then((data: any) => {
            return dmGetFormStructure('form.community.household')
                .then((structure) => {
                    // Complete divisions meta.
                    let middlePromises = [];
                    for (let i = 0; i < data.length; i++) {
                        let record = data[i];
                        for (const fieldName in structure.fields) {
                            let fieldType = structure.fields[fieldName].type;
                            switch (fieldType) {
                                case 'administrative_division_reference':
                                    middlePromises.push(dmGetAdministrativeDivision(record[fieldName].value)
                                        .then((division) => {
                                            return dmGetAdministrativeDivisionMeta(division)
                                                .then((divisionMeta) => {
                                                    record[fieldName].meta = divisionMeta;
                                                });
                                        }));
                                    break;
                                case 'user_reference':
                                    middlePromises.push(dmGetUser(record[fieldName].value)
                                        .then((user) => {
                                            record[fieldName].meta = {
                                                "username": user?.username,
                                                "gravatar": user?.gravatar,
                                            };
                                        }));
                                    break;
                            }
                        }
                    }
                    return Promise.all(middlePromises)
                        .then(() => {
                            return data;
                        });
                });
        });
    }
}

/**
 * Retrieve a list of household processes
 *
 * @param page
 * @param open
 * @param field_region
 *
 * @returns {Promise}
 */
export function dmGetHouseholdProcesses(
    page: number = -1,
    open: boolean | undefined = undefined,
    field_region: string | undefined = undefined
) {
    let parameters: { open?: boolean, administrativeDivision?: string } = {};
    if (undefined !== open) {
        parameters.open = open;
    }
    if (field_region) {
        parameters.administrativeDivision = field_region;
    }

    if (!dmIsOfflineMode()) {
        return api.getHouseholdProcesses(page, open, field_region).then(data => data);
    } else {
        // Retrieve local list of household processes
        return db.getHouseholdPrecesses({page: page}).then(data => data);
    }
}
