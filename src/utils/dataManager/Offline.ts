/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {isNull} from "lodash";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import router from "next/router";

const LOCAL_CONFIGURATION_OFFLINE_STATUS:string = "offline_mode";
let inOfflineMode:boolean = false;

/**
 * Change offline mode status.
 *
 * @param value {boolean}
 */
export function dmSetOfflineMode(value: boolean) {
    inOfflineMode = value;
    lcmSetItem(LOCAL_CONFIGURATION_OFFLINE_STATUS, value);
}

/**
 * Are we in offline mode?
 *
 * @returns {boolean};
 */
export function dmIsOfflineMode(): boolean {
    let offlineStatus: any = lcmGetItem(LOCAL_CONFIGURATION_OFFLINE_STATUS, null);
    if (!isNull(offlineStatus)) {
        return offlineStatus;
    }

    return inOfflineMode;
}

/**
 * If offline mode then go home.
 *
 * @returns {void}
 */
export function dmOfflineRedirection() {
    if (dmIsOfflineMode()) {
        router.push('/');
    }
}
