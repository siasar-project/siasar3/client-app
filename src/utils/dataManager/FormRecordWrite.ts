/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import Api from "@/utils/api";
import api from "@/utils/api";
import {
    dmGetCountry,
    dmGetEmptyFormRecord,
    dmGetFormRecord,
    dmGetFormStructure,
    dmGetPoint,
    dmOfflineIntercept
} from "@/utils/dataManager";
import {ReplanPointInterface} from "@/objects/ReplanPointInterface";
import offlineDataRepository from "@/utils/offlineDataRepository";
import {db} from "@/utils/db";
import {uniqid} from "locutus/php/misc";
import {smGetSession} from "@/utils/sessionManager";
import {User} from "@/objects/User";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import t from "@/utils/i18n";
import {
    dmGetAdministrativeDivision,
    dmGetAdministrativeDivisionMeta,
    dmGetTechnicalAssistanceProvider
} from "@/utils/dataManager/ParametricRead";
import {getTimezone} from "@/utils/siasar";
import {StructureField} from "@/objects/Structure";

/**
 * Clear record from API.
 *
 * Prepare the record to send it to API again.
 *
 * @param structure {DynFormStructureInterface}
 * @param record {any}
 *
 * @returns {any}
 */
export function dmClearFormRecord(structure: DynFormStructureInterface, record: any) {
    // Expand single property values & remove meta properties..
    for (const dataKey in record) {
        if (structure.fields[dataKey]) {
            // Expand value.
            switch (typeof record[dataKey]) {
                case "string":
                case "boolean":
                case "number":
                    record[dataKey] = {value: record[dataKey]};
                    break;
            }
            // Boolean value to string 0/1.
            switch (structure.fields[dataKey].type) {
                case "boolean":
                case "radio_boolean":
                    if (typeof record[dataKey].value === "boolean") {
                        record[dataKey].value = (record[dataKey].value) ? '1' : '0';
                    }
                    break;
            }
            if (record[dataKey] === null) {
                if (structure.fields[dataKey].settings.multivalued) {
                    record[dataKey] = [];
                } else {
                    record[dataKey] = {value: ""};
                }
            }
            if(Array.isArray(record[dataKey])){
                record[dataKey] = record[dataKey].map((val:any) => {
                    if (typeof val === 'string') {
                        return {value: val};
                    }

                    for (const valKey in val) {
                        if (val[valKey] === "00000000000000000000000000") {
                            val[valKey] = "";
                        }
                    }
                    return val;
                });
            }
            // Remove Nil ULIDs from value property.
            if (record[dataKey].value && record[dataKey].value === "00000000000000000000000000") {
                record[dataKey].value = "";
            }
            // Remove meta.
            let excludedFieldTypes = ['user_reference', 'file_reference', 'image_reference', 'household_process_reference'];
            if (record[dataKey]?.meta !== undefined && !excludedFieldTypes.includes(structure.fields[dataKey].type)) {
                delete record[dataKey].meta;
            }
        }
        // if (dataKey !== 'id' && dataKey !== 'form') {
        //
        // }
    }
    return record;
}

/**
 * Add DynForm record.
 *
 * @param record
 * @param record.formId DynForm ID.
 * @param record.data API record object.
 *
 * @returns {Promise}
 */
export function dmAddFormRecord(record: DynFormRecordInterface) {
    record.data = dmClearFormRecordToUpdate(record.data);
    if (!dmIsOfflineMode()) {
        return Api.addFormRecord(record.formId, record.data);
    } else {
        record.data.id = offlineDataRepository.getNewUlid();
        // Create and save the new record.
        return offlineDataRepository.updateFormRecord(record.data.id, record.formId, record.data)
            .then((offlineRecord:any) => {
                return dmGetFormStructure(record.formId)
                    .then((structure:any) => {
                        return dmGetEmptyFormRecord(record.formId)
                            .then((emptyRecord: any) => {
                                // Copy all data fields to the empty record.
                                for (const dataKey in record.data) {
                                    emptyRecord[dataKey] = record.data[dataKey];
                                }
                                emptyRecord = dmClearFormRecord(structure, emptyRecord);
                                return db.addFormRecord(emptyRecord)
                                    .then(() => {
                                        let resp: DynFormRecordInterface = {
                                            id: emptyRecord.id,
                                            formId: emptyRecord.form,
                                            data: emptyRecord,
                                        };
                                        if ("App\\Forms\\InquiryFormManager" !== structure.type) {
                                            return resp;
                                        }
                                        let crossPromise;
                                        if (structure.meta.point_associated) {
                                            crossPromise = dmCreatePointInquiryIndex(emptyRecord);
                                        } else {
                                            crossPromise = dmCreateInquiryIndex(emptyRecord);
                                        }
                                        // InquiryFormManager records need index records.
                                        return crossPromise.then(() => {
                                            return dmCreateInquiryIndex(emptyRecord)
                                                .then((newIndexRecord) => {
                                                    return db.addFormIndexRecord(newIndexRecord)
                                                        .then((newIndexRecord) => {
                                                            return resp;
                                                        });
                                                });
                                        });
                                    });
                            });
                    });
            });
    }
}

/**
 * Update DynForm record.
 *
 * @param record
 * @param record.formId DynForm ID.
 * @param record.data API record object.
 *
 * @returns {Promise}
 */
export function dmUpdateFormRecord(record: DynFormRecordInterface) {
    if (!dmIsOfflineMode()) {
        record.data = dmClearFormRecordToUpdate(record.data);
        return Api.updateFormRecord(record.formId, record.id || "", record.data)
            .then((res:any) => {
                return dmGetFormStructure(record.formId).then((structure: DynFormStructureInterface) => {
                    let data = res.data;
                    return dmClearFormRecord(structure, data);
                });
            });
    } else {
        // Update form index record.
        record.data.form = record.formId;
        record.data.id = record.id;
        // Complete record data with default one.
        return dmCompleteFormRecordToUpdate(record.data, record.formId)
            .then((recordData) => {
                return dmGetFormStructure(record.formId).then((structure: DynFormStructureInterface) => {
                    record.data = dmClearFormRecord(structure, record.data);
                    // Update form record.
                    return db.updateFormRecord(recordData)
                        .then(() => {
                            // Add offline change mark
                            return offlineDataRepository.updateFormRecord(record.id || "", record.formId, recordData)
                                .then(() => {
                                    switch (structure.type) {
                                        case "App\\Forms\\HouseholdFormManager":
                                            break;
                                        case "App\\Forms\\InquiryFormManager":
                                            return recordData;
                                        default:
                                            // InquiryFormManager records need index records.
                                            let crossPromise;
                                            if (structure.meta.point_associated) {
                                                crossPromise = dmCreatePointInquiryIndex(recordData);
                                            } else {
                                                crossPromise = dmCreateInquiryIndex(recordData);
                                            }
                                            return crossPromise.then((newIndexRecord) => {
                                                return db.updateFormIndexRecord(newIndexRecord)
                                                    .then(() => {
                                                        return recordData;
                                                    });
                                            });
                                    }
                                });
                        });
                });
            });
    }
}

/**
 * Complete a form record fields.
 *
 * In offline mode, before update a form record, we need have all fields.
 * If not defined, we need add default values.
 *
 * @param record
 * @param formType {string}
 *
 * @returns {Promise}
 */
function dmCompleteFormRecordToUpdate(record: any, formType: string):Promise<any> {
    return dmGetFormStructure(formType)
        .then((structure) => {
            return dmGetEmptyFormRecord(formType)
                .then((emptyRecord) => {
                    let middlePromise:any[] = [];
                    for (const emptyRecordKey in emptyRecord) {
                        if ("id" !== emptyRecordKey && "form" !== emptyRecordKey) {
                            let fieldDefinition:StructureField = structure.fields[emptyRecordKey];
                            // Set data if not exist.
                            if (!record[emptyRecordKey]) {
                                record[emptyRecordKey] = emptyRecord[emptyRecordKey];
                            }
                            // Add required meta data.
                            switch (fieldDefinition.type) {
                                case "administrative_division_reference":
                                    if (!record[emptyRecordKey].meta) {
                                        middlePromise.push(
                                            dmGetAdministrativeDivision(record[emptyRecordKey].value)
                                                .then((division) => {
                                                    if (division) {
                                                        return dmGetAdministrativeDivisionMeta(division)
                                                            .then((regionMeta) => {
                                                                record[emptyRecordKey].meta = regionMeta;
                                                            });
                                                    } else {
                                                        record[emptyRecordKey].meta = [];
                                                        return null;
                                                    }
                                                })
                                        );
                                    }
                                    break;
                            }
                        }
                    }
                    return Promise.all(middlePromise)
                        .then(() => {
                            return record;
                        });
                });
        });
}

/**
 * Clone DynForm record.
 *
 * @param record
 * @param record.formId DynForm ID.
 * @param record.data API record object.
 *
 * @returns {Promise}
 */
export function dmCloneFormRecord(record: DynFormRecordInterface) {
    if (!dmOfflineIntercept('dmCloneFormRecord')) {
        record.data = {};
        return Api.cloneFormRecord(record.formId, record.id || "");
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove DynForm record.
 *
 * @param record
 * @param record.formId DynForm ID.
 * @param record.id API record ID.
 *
 * @returns {Promise}
 */
export function dmRemoveFormRecord(record: DynFormRecordInterface) {
    if (!dmIsOfflineMode()) {
        return Api.removeFormRecord(record.formId, record.id || "");
    } else {
        // Load record.
        return dmGetFormRecord(record)
            .then((data:any) => {
                if (data) {
                    // If record exist, mark how deleted.
                    offlineDataRepository.removeFormRecord(record.id || "", record.formId)
                        .then(() => {
                            return dmGetFormStructure(record.formId)
                                .then((structure) => {
                                    // Remove record from database.
                                    return db.invalidateFormRecord(record.id || "", record.formId)
                                        .then(() => {
                                            // If is an inquiry form , remove form index record.
                                            if ("App\\Forms\\InquiryFormManager" !== structure.type) {
                                                return null;
                                            }
                                            // InquiryFormManager records need index records that we need remove too.
                                            return offlineDataRepository.removeFormRecord(record.id || "", record.formId)
                                                .then(() => null);
                                        });
                                });
                        });
                }
            });
    }
}

/**
 * Update DynForm record.
 *
 * @param record
 * @param record.formId DynForm ID.
 * @param record.data API record object.
 *
 * @returns {Promise}
 */
export function dmUpdateFormPoint(record: DynFormRecordInterface) {
    if (!dmIsOfflineMode()) {
        record.data = dmClearFormRecordToUpdate(record.data);
        return Api.updateFormPoint(record.id || "", record.data);
    } else {
        return db.updatePoint(record.data)
            .then(() => {
                return record;
            });
    }
}

/**
 * Clone DynForm record.
 *
 * Clone a calculated point to a new point.
 *
 * Note: This method don't have offline mode.
 *
 * @param record
 * @param record.id record ID.
 *
 * @returns {Promise}
 */
export function dmCloneFormPoint(record: DynFormRecordInterface) {
    if (!dmIsOfflineMode()) {
        return Api.cloneFormPoint(record.id || "");
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Remove form point
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmRemoveFormPoint(id: string) {
    if (!dmOfflineIntercept('dmRemoveFormPoint')) {
        return Api.removeFormPoint(id);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Re-plan SIASAR point
 *
 * @param id {string}
 * @param data {ReplanPointInterface} Any init form data.
 *
 * @returns {Promise}
 */
export function dmReplanPoint(id: string, data: ReplanPointInterface) {
    if (!dmOfflineIntercept('dmReplanPoint')) {
        return Api.replanPoint(id, data);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Add a new SIASAR Point in this division.
 *
 * @param division {string} The new division IRI.
 * @returns {Promise}
 */
export function dmAddPoint(division:string) {
    if (!dmOfflineIntercept('dmAddPoint')) {
        return api.addFormPoints(division);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Add a new WSP to SIASAR Point.
 *
 * @param id {string} Point ID.
 * @param data {any} Any init form data.
 * @returns {Promise}
 */
export function dmAddWspToPoint(id: string, data: any) {
    if (!data) {
        data = {};
    }
    if (!dmIsOfflineMode()) {
        return api.addFormWspToPoint(id, data);
    } else {
        data.id = offlineDataRepository.getNewUlid();
        data.form = 'form.wsprovider';
        // Create and save the new record.
        return offlineDataRepository.updateFormRecord(data.id, data.form, data)
            .then((item:any) => {
                // Find Point and add the new WSSystem record.
                return dmGetPoint(id)
                    .then((point:any) => {
                        // { id: "01GM8YB6JY2TZM85CG3XWWSMWM", name: "Sistema de La Flor", meta: [] }
                        return dmGetFormStructure(item.form)
                            .then((structure:any) => {
                                return dmGetEmptyFormRecord(item.form)
                                    .then((emptyRecord: any) => {
                                        // Copy all data fields to the empty record.
                                        for (const dataKey in data) {
                                            emptyRecord[dataKey] = data[dataKey];
                                        }
                                        let newRecord = dmClearFormRecord(structure, emptyRecord);
                                        // It requires save a record form index before.
                                        return dmCreatePointInquiryIndex(newRecord)
                                            .then((newPointInquiryIndex) => {
                                                return db.addFormIndexRecord(newPointInquiryIndex)
                                                    .then(() => {
                                                        return db.addFormRecord(newRecord)
                                                            .then(() => {
                                                                point.wsp.push({ id: newRecord.id, name: newRecord[structure.meta.title_field].value, meta: [] });
                                                                // Save updated point.
                                                                return offlineDataRepository.updateFormRecord(point.id, 'form.point', point)
                                                                    .then(() => {
                                                                        return dmUpdateFormPoint({id: point.id, data: point, formId: 'form.point'})
                                                                            .then(() => {
                                                                                return item;
                                                                            });
                                                                    });
                                                            });
                                                    });
                                            });
                                    });
                            });
                    });
            });
    }
}

/**
 * Add a new Water system to SIASAR Point.
 *
 * @param id {string} Point ID.
 * @param data {any} Any init form data.
 * @returns {Promise}
 */
export function dmAddWSystemToPoint(id: string, data: any) {
    if (!data) {
        data = {};
    }
    if (!dmIsOfflineMode()) {
        return api.addFormWSystemToPoint(id, data);
    } else {
        data.id = offlineDataRepository.getNewUlid();
        data.form = 'form.wssystem';
        // Create and save the new record.
        return offlineDataRepository.updateFormRecord(data.id, data.form, data)
            .then((item:any) => {
                // Find Point and add the new WSSystem record.
                return dmGetPoint(id)
                    .then((point:any) => {
                        // { id: "01GM8YB6JY2TZM85CG3XWWSMWM", name: "Sistema de La Flor", meta: [] }
                        return dmGetFormStructure(item.form)
                            .then((structure:any) => {
                                return dmGetEmptyFormRecord(item.form)
                                    .then((emptyRecord: any) => {
                                        // Copy all data fields to the empty record.
                                        for (const dataKey in data) {
                                            emptyRecord[dataKey] = data[dataKey];
                                        }
                                        let newRecord = dmClearFormRecord(structure, emptyRecord);
                                        // It requires save a record form index before.
                                        return dmCreatePointInquiryIndex(newRecord)
                                            .then((newPointInquiryIndex) => {
                                                return db.addFormIndexRecord(newPointInquiryIndex)
                                                    .then(() => {
                                                        return db.addFormRecord(newRecord)
                                                            .then(() => {
                                                                point.wsystem.push({ id: newRecord.id, name: newRecord[structure.meta.title_field].value, meta: [] });
                                                                // Save updated point.
                                                                return offlineDataRepository.updateFormRecord(point.id, 'form.point', point)
                                                                    .then(() => {
                                                                        return dmUpdateFormPoint({id: point.id, formId: point.type, data: point})
                                                                            .then(() => {
                                                                                return item;
                                                                            });
                                                                    });
                                                            });
                                                    });
                                            });
                                    });
                            });
                    });
            });
    }
}

/**
 * Get empty form index record to points.
 *
 * Note: To use in offline mode.
 *
 * @param refData
 *
 * @returns {Promise}
 */
export function dmCreatePointInquiryIndex(refData:any) {
    let user: User|null = smGetSession();
    let now:Date = new Date();
    // Complete and fix data.
    return dmGetFormStructure(refData.form)
        .then((structure:any) => {
            let titleField:string = structure.meta.title_field;
            return dmGetCountry()
                .then((country: any) => {
                    return dmGetAdministrativeDivision(refData.field_region.value)
                        .then((division) => {
                            return dmGetAdministrativeDivisionMeta(division)
                                .then((regionMeta) => {
                                    return {
                                        "id": refData.id,
                                        "type": refData.form || refData.type,
                                        "type_title": t(structure.title),
                                        "field_reference": uniqid(),
                                        "field_ulid_reference": offlineDataRepository.getNewUlid(),
                                        "field_validated": "0",
                                        "field_deleted": "0",
                                        "field_status": "draft",
                                        "field_country": {
                                            "value": country.code,
                                            "class": "App\\Entity\\Country",
                                            "meta": {
                                                "name": country.name,
                                                "flag": country.flag,
                                                "formLevel": country.formLevel
                                            }
                                        },
                                        "field_region": {
                                            "value": refData.field_region.value,
                                            "class": "App\\Entity\\AdministrativeDivision",
                                            "meta": regionMeta
                                        },
                                        "field_creator": {
                                            "value": user?.id,
                                            "class": "App\\Entity\\User",
                                            "meta": {
                                                "username": user?.username,
                                                "gravatar": user?.gravatar
                                            }
                                        },
                                        "field_editors": [
                                            {
                                                "value": user?.id,
                                                "class": "App\\Entity\\User",
                                                "meta": {
                                                    "username": user?.username,
                                                    "gravatar": user?.gravatar
                                                }
                                            }
                                        ],
                                        "field_editors_update": [
                                            {
                                                // Use current date.
                                                "value": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                "timezone": getTimezone(),
                                                "meta": {
                                                    "real": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                }
                                            }
                                        ],
                                        "field_image": null,
                                        "field_title": refData[titleField].value,
                                        "indicator": "NOT CALCULATED",
                                        "indicator_value": -3,
                                        "logs": {
                                            "errors": 0,
                                            "warnings": 0
                                        }
                                    }
                                });
                        });
                });
        });
}

/**
 * Get empty form index record.
 *
 * Note: To use in offline mode.
 *
 * @param refData
 *
 * @returns {Promise}
 */
export function dmCreateInquiryIndex(refData:any) {
    let user: User|null = smGetSession();
    let now:Date = new Date();
    // Complete and fix data.
    return dmGetFormStructure(refData.form)
        .then((structure:any) => {
            // Get index record title.
            let titleField:string = structure.meta.title_field;
            let fieldTitle = refData[titleField].value;
            let crossPromise: Promise<any>;
            // Here, we have two Promise ways. Both return the same data type to allow to unify then in one.
            if ((refData.form || refData.type) === 'form.tap') {
                // Use the TAP name how index record title.
                crossPromise = dmGetTechnicalAssistanceProvider(fieldTitle)
                    .then((tap) => {
                        return new Promise((resolve) => resolve(tap.name));
                    });
            } else {
                // Otherwise, use the standard title.
                crossPromise = new Promise((resolve) => resolve(fieldTitle));
            }
            return crossPromise.then((title) => {
                fieldTitle = title;
                return dmGetCountry()
                    .then((country: any) => {
                        return dmGetAdministrativeDivision(refData.field_region.value)
                            .then((division) => {
                                return dmGetAdministrativeDivisionMeta(division)
                                    .then((regionMeta) => {
                                        return {
                                            "id": refData.id,
                                            "type": refData.form || refData.type,
                                            "type_title": t(structure.title),
                                            "field_reference": uniqid(),
                                            "field_ulid_reference": offlineDataRepository.getNewUlid(),
                                            "field_validated": "0",
                                            "field_deleted": "0",
                                            "field_status": "draft",
                                            "field_country": {
                                                "value": country.code,
                                                "class": "App\\Entity\\Country",
                                                "meta": {
                                                    "name": country.name,
                                                    "flag": country.flag,
                                                    "formLevel": country.formLevel
                                                }
                                            },
                                            "field_region": {
                                                "value": refData.field_region.value,
                                                "class": "App\\Entity\\AdministrativeDivision",
                                                "meta": regionMeta
                                            },
                                            "field_created": {
                                                // Use current date.
                                                "value": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                "timezone": getTimezone(),
                                                "meta": {
                                                    "real": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                }
                                            },
                                            "field_changed": {
                                                // Use current date.
                                                "value": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                "timezone": getTimezone(),
                                                "meta": {
                                                    "real": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                }
                                            },
                                            "field_creator": {
                                                "value": user?.id,
                                                "class": "App\\Entity\\User",
                                                "meta": {
                                                    "username": user?.username,
                                                    "gravatar": user?.gravatar
                                                }
                                            },
                                            "field_editors": [
                                                {
                                                    "value": user?.id,
                                                    "class": "App\\Entity\\User",
                                                    "meta": {
                                                        "username": user?.username,
                                                        "gravatar": user?.gravatar
                                                    }
                                                }
                                            ],
                                            "field_editors_update": [
                                                {
                                                    // Use current date.
                                                    "value": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                    "timezone": getTimezone(),
                                                    "meta": {
                                                        "real": `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`,
                                                    }
                                                }
                                            ],
                                            "field_image": null,
                                            "field_title": fieldTitle,
                                            "point": "",
                                            "point_status": "",
                                            "indicator": "NOT CALCULATED",
                                            "indicator_value": -3,
                                            "logs": {
                                                "errors": 0,
                                                "warnings": 0
                                            }
                                        }
                                    });
                            });
                    });
            });
        });
}

/**
 * Delete a Water Supply System from Siasar Point
 *
 * @param pid {string} Point Record ID.
 * @param sid {string} WSS ID.
 * @returns {Promise}
 */
export function dmRemoveWSSystem(pid: string, sid: string) {
    if (!dmOfflineIntercept('dmRemoveWSSystem')) {
        return Api.removeWSSystem(pid, sid);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Delete a Community from Siasar Point
 *
 * @param pid {string} Point Record ID.
 * @param sid {string} Community ID.
 * @returns {Promise}
 */
export function dmRemoveCommunity(pid: string, sid: string) {
    if (!dmOfflineIntercept('dmRemoveCommunity')) {
        return Api.removeCommunity(pid, sid);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Delete a Community households from Siasar Point community.
 *
 * @param pid {string} Point Record ID.
 * @param sid {string} Community ID.
 * @returns {Promise}
 */
export function dmRemoveCommunityHouseholds(pid: string, sid: string) {
    if (!dmOfflineIntercept('dmRemoveCommunityHouseholds')) {
        return Api.removeCommunityHouseholds(pid, sid);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Delete a Water Service Provider from Siasar Point
 *
 * @param pid {string} Point Record ID.
 * @param wspid {string} WSP ID.
 * @returns {Promise}
 */
export function dmRemoveWSProvider(pid: string, wspid: string) {
    if (!dmOfflineIntercept('dmRemoveWSProvider')) {
        return Api.removeWSProvder(pid, wspid);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Clear form record data to send to API.
 *
 * @param data {any}
 * @returns {any}
 */
export function dmClearFormRecordToUpdate(data: any) {
    for (const fieldName in data) {
        if (Array.isArray(data[fieldName])) {
            for (let i = 0; i < data[fieldName].length; i++) {
                dmClearFormRecordDataToUpdate(data[fieldName][i]);
            }
        } else {
            dmClearFormRecordDataToUpdate(data[fieldName]);
        }
    }
    delete data["form"];
    return data;
}

/**
 * Clear a form field value to send to API.
 *
 * @param data
 */
function dmClearFormRecordDataToUpdate(data: any) {
    // All object properties must have a value.
    let withValue = true;
    for (const property in data) {
        // Remove meta properties, it's not a field property.
        if (property === "meta") {
            delete data["meta"];
        }
        // Detect any empty property.
        else if (!data[property] || data[property] === "") {
            withValue = false;
        }
    }
    if (!withValue) {
        // Clean all properties.
        for (const property in data) {
            data[property] = "";
        }
    }
}
