/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {db} from "@/utils/db";
import {dmGetCountry, dmOfflineIntercept, localCountryParametricQuerier} from "@/utils/dataManager";
import {smGetSession, smSetSession} from "@/utils/sessionManager";
import {TechnicalAssistanceProvider} from "@/objects/TechnicalAssistanceProvider";
import api from "@/utils/api";
import {TreatmentTechnologyAerationOxidation} from "@/objects/TreatmentTechnologyAerationOxidation";
import {TreatmentTechnologyDesalination} from "@/objects/TreatmentTechnologyDesalination";
import {TreatmentTechnologyFiltration} from "@/objects/TreatmentTechnologyFiltration";
import {GeographicalScope} from "@/objects/GeographicalScope";
import {FunctionsCarriedOutTap} from "@/objects/FunctionsCarriedOutTap";
import {FunctionsCarriedOutWsp} from "@/objects/FunctionsCarriedOutWsp";
import {TypeIntervention} from "@/objects/TypeIntervention";
import {CommunityService} from "@/objects/CommunityService";
import {Currency} from "@/objects/Currency";
import {ProgramIntervention} from "@/objects/ProgramIntervention";
import {TypologyChlorinationInstallation} from "@/objects/TypologyChlorinationInstallation";
import {InterventionStatus} from "@/objects/InterventionStatus";
import {TypeHealthcareFacility} from "@/objects/TypeHealthcareFacility";
import {TypeTap} from "@/objects/TypeTap";
import {WaterQualityInterventionType} from "@/objects/WaterQualityInterventionType";
import {TreatmentTechnologyCoagFloccu} from "@/objects/TreatmentTechnologyCoagFloccu";
import {TreatmentTechnologySedimentation} from "@/objects/TreatmentTechnologySedimentation";
import {OfficialLanguage} from "@/objects/OfficialLanguage";
import {AdministrativeDivision} from "@/objects/AdministrativeDivision";
import {str_replace} from "locutus/php/strings";
import {SpecialComponent} from "@/objects/SpecialComponent";
import {Ethnicity} from "@/objects/Ethnicity";
import {DisinfectingSubstance} from "@/objects/DisinfectingSubstance";
import {DistributionMaterial} from "@/objects/DistributionMaterial";
import {DefaultDiameter} from "@/objects/DefaultDiameter";
import {Material} from "@/objects/Material";
import {PumpType} from "@/objects/PumpType";
import {InstitutionIntervention} from "@/objects/InstitutionIntervention";
import {WaterQualityEntity} from "@/objects/WaterQualityEntity";
import {FunderIntervention} from "@/objects/FunderIntervention";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

/**
 * Hold a reference to current promise to allow just one run at once.
 */
let dmGetAdministrativeDivisionsPromise: Promise<any> = Promise.resolve();

/**
 * Get technical assitance providers.
 *
 * @param name
 * @param description
 * @param nationalId
 * @param page
 * @returns {Promise}
 */
export function dmGetTechnicalAssistanceProviders(name: string = "", description: string = "", nationalId: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.technicalAssistanceProviders) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.technicalAssistanceProviders,
            /**
             * Compare two TechnicalAssistanceProviders by name string.
             *
             * @param first {TechnicalAssistanceProvider}
             * @param second {TechnicalAssistanceProvider}
             * @returns {number}
             */
            itemComparer: (first: TechnicalAssistanceProvider, second: TechnicalAssistanceProvider) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}, {
                value: description,
                propertyName: "description"
            }, {value: nationalId, propertyName: "nationalId"}]
        });
    } else {
        if (!dmOfflineIntercept('dmGetTechnicalAssistanceProviders')) {
            // Get from API all TechnicalAssistanceProviders.
            return api.getTechnicalAssistanceProviders().then((data: any) => {
                if (session && session.country) {
                    // The TechnicalAssistanceProviders must be into the user session data.
                    session.country.technicalAssistanceProviders = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTechnicalAssistanceProviders(name, description, nationalId, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}


/**
 * Get technical assitance provider.
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmGetTechnicalAssistanceProvider(id: string): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.technicalAssistanceProviders) {
        // Get from user session.
        for (let i = 0; i < session?.country?.technicalAssistanceProviders.length; i++) {
            let tap = session?.country?.technicalAssistanceProviders[i];
            if (tap.id === id) {
                return new Promise((resolve) => resolve(tap));
            }
        }
    } else {
        if (!dmIsOfflineMode()) {
            // Get from remote API.
            return api.getTechnicalAssistanceProvider(id);
        }
    }

    // We don't know any TAPs.
    return new Promise((resolve) => resolve(null));
}

/**
 *
 * @param name
 * @param page
 * @returns {Promise}
 */
export function dmGetTreatmentTechnologyAerationOxidation(name: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.treatmentTechnologyAerationOxidation) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.treatmentTechnologyAerationOxidation,
            /**
             * Compare two TreatmentTechnologyAerationOxidation by name string.
             *
             * @param first {TreatmentTechnologyAerationOxidation}
             * @param second {TreatmentTechnologyAerationOxidation}
             * @returns {number}
             */
            itemComparer: (first: TreatmentTechnologyAerationOxidation, second: TreatmentTechnologyAerationOxidation) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}]
        });
    } else {
        if (!dmOfflineIntercept('dmGetTreatmentTechnologyAerationOxidation')) {
            // Get from API all treatment technology filtration.
            return api.getTreatmentTechnologyAerationOxidation().then((data: any) => {
                if (session && session.country) {
                    // The Treatment technology filtration must be into the user session data.
                    session.country.treatmentTechnologyAerationOxidation = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTreatmentTechnologyAerationOxidation(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 *
 * @param name
 * @param page
 * @returns {Promise}
 */
export function dmGetTreatmentTechnologyDesalination(name: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.treatmentTechnologyDesalination) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.treatmentTechnologyDesalination,
            /**
             * Compare two TreatmentTechnologyDesalination by name string.
             *
             * @param first {TreatmentTechnologyDesalination}
             * @param second {TreatmentTechnologyDesalination}
             * @returns {number}
             */
            itemComparer: (first: TreatmentTechnologyDesalination, second: TreatmentTechnologyDesalination) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}]
        });
    } else {
        if (!dmOfflineIntercept('dmGetTreatmentTechnologyDesalination')) {
            // Get from API all treatment technology filtration.
            return api.getTreatmentTechnologyDesalination().then((data: any) => {
                if (session && session.country) {
                    // The Treatment technology filtration must be into the user session data.
                    session.country.treatmentTechnologyDesalination = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTreatmentTechnologyDesalination(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 *
 * @param name
 * @param page
 * @returns {Promise}
 */
export function dmGetTreatmentTechnologyFiltration(name: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.treatmentTechnologyFiltration) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.treatmentTechnologyFiltration,
            /**
             * Compare two TreatmentTechnologyFiltration by name string.
             *
             * @param first {TreatmentTechnologyFiltration}
             * @param second {TreatmentTechnologyFiltration}
             * @returns {number}
             */
            itemComparer: (first: TreatmentTechnologyFiltration, second: TreatmentTechnologyFiltration) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}]
        });
    } else {
        if (!dmOfflineIntercept('dmGetTreatmentTechnologyFiltration')) {
            // Get from API all treatment technology filtration.
            return api.getTreatmentTechnologyFiltration().then((data: any) => {
                if (session && session.country) {
                    // The Treatment technology filtration must be into the user session data.
                    session.country.treatmentTechnologyFiltration = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTreatmentTechnologyFiltration(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 *
 * @param keyIndex
 * @param name
 * @param page
 * @returns {Promise}
 */
export function dmGetGeographicalScope(keyIndex: string = "", name: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.geographicalScopes) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.geographicalScopes,
            /**
             * Compare two geographical scope by type string.
             *
             * @param first {GeographicalScope}
             * @param second {GeographicalScope}
             * @returns {number}
             */
            itemComparer: (first: GeographicalScope, second: GeographicalScope) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: keyIndex, propertyName: "keyIndex"}, {value: name, propertyName: "name"}]
        });
    } else {
        if (!dmOfflineIntercept('dmGetGeographicalScope')) {
            // Get from API all geographical scope.
            return api.getGeographicalScope().then((data: any) => {
                if (session && session.country) {
                    // The geographical scope must be into the user session data.
                    session.country.geographicalScopes = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetGeographicalScope(keyIndex, name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 *
 * @param keyIndex
 * @param type
 * @param page
 * @returns {Promise}
 */
export function dmGetFunctionsCarriedOutTaps(keyIndex: string = "", type: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.functionsCarriedOutTap) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.functionsCarriedOutTap,
            /**
             * Compare two FunctionCarriedOutTap by type string.
             *
             * @param first {FunctionCarriedOutTap}
             * @param second {FunctionCarriedOutTap}
             * @returns {number}
             */
            itemComparer: (first:FunctionsCarriedOutTap, second: FunctionsCarriedOutTap) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: keyIndex, propertyName: "keyIndex"}, {value: type, propertyName: "type"}]
        });
    } else {
        if (!dmOfflineIntercept('dmGetFunctionsCarriedOutTaps')) {
            // Get from API all functions carried out by TAP.
            return api.getFunctionsCarriedOutTaps().then((data: any) => {
                if (session && session.country) {
                    // The FunctionCarriedOutTap must be into the user session data.
                    session.country.functionsCarriedOutTap = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetFunctionsCarriedOutTaps(keyIndex, type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get functionsCarriedOutWsps list.
 *
 * @param type {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetFunctionsCarriedOutWsps(type: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.functionsCarriedOutWsps) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.functionsCarriedOutWsps,
            /**
             * Compare two functionsCarriedOutWsps by type string.
             *
             * @param first {FunctionsCarriedOutWsp}
             * @param second {FunctionsCarriedOutWsp}
             * @returns {number}
             */
            itemComparer: (first:FunctionsCarriedOutWsp, second: FunctionsCarriedOutWsp) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: type, propertyName: "type"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetFunctionsCarriedOutWsps')) {
            // Get from API all functionsCarriedOutWsps.
            return api.getFunctionsCarriedOutWsps().then((data: any) => {
                if (session && session.country) {
                    // The functionsCarriedOutWsps must be into the user session data.
                    session.country.functionsCarriedOutWsps = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetFunctionsCarriedOutWsps(type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get typeInterventions list.
 *
 * @param type {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetTypeInterventions(type: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.typeInterventions) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.typeInterventions,
            /**
             * Compare two typeInterventions by type string.
             *
             * @param first {TypeIntervention}
             * @param second {TypeIntervention}
             * @returns {number}
             */
            itemComparer: (first: TypeIntervention, second: TypeIntervention) => first.type.toLocaleLowerCase() > second.type.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: type, propertyName: "type"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetTypeInterventions')) {
            // Get from API all typeInterventions.
            return api.getTypeInterventions().then((data: any) => {
                if (session && session.country) {
                    // The typeInterventions must be into the user session data.
                    session.country.typeInterventions = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTypeInterventions(type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get communityServices list.
 *
 * @param type {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetCommunityServices(type: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.communityServices) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.communityServices,
            /**
             * Compare two communityServices by type string.
             *
             * @param first {CommunityService}
             * @param second {CommunityService}
             * @returns {number}
             */
            itemComparer: (first: CommunityService, second: CommunityService) => first.type.toLocaleLowerCase() > second.type.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: type, propertyName: "type"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetCommunityServices')) {
            // Get from API all communityServices.
            return api.getCommunityServices().then((data: any) => {
                if (session && session.country) {
                    // The communityServices must be into the user session data.
                    session.country.communityServices = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetCommunityServices(type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get currencies list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetCurrencies(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.currencies) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.currencies,
            /**
             * Compare two currencies by name string.
             *
             * @param first {Currency}
             * @param second {Currency}
             * @returns {number}
             */
            itemComparer: (first:Currency, second: Currency) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetCurrencies')) {
            // Get from API all currencies.
            return api.getCurrencies().then((data: any) => {
                if (session && session.country) {
                    // The currencies must be into the user session data.
                    session.country.currencies = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetCurrencies(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get typeInterventions list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetProgramInterventions(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.programInterventions) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.programInterventions,
            /**
             * Compare two ProgramInterventions by type string.
             *
             * @param first {ProgramIntervention}
             * @param second {ProgramIntervention}
             * @returns {number}
             */
            itemComparer: (first:ProgramIntervention, second: ProgramIntervention) => (first.name || "").toLocaleLowerCase() > (second.name || "").toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetProgramInterventions')) {
            // Get from API all ProgramInterventions.
            return api.getProgramInterventions().then((data: any) => {
                if (session && session.country) {
                    // The ProgramInterventions must be into the user session data.
                    session.country.programInterventions = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetProgramInterventions(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get typologyChlorinationInstallations list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetTypologyChlorinationInstallations(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.typologyChlorinationInstallations) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.typologyChlorinationInstallations,
            /**
             * Compare two typologyChlorinationInstallations by name string.
             *
             * @param first {TypologyChlorinationInstallation}
             * @param second {TypologyChlorinationInstallation}
             * @returns {number}
             */
            itemComparer: (first:TypologyChlorinationInstallation, second: TypologyChlorinationInstallation) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetTypologyChlorinationInstallations')) {
            // Get from API all typologyChlorinationInstallations.
            return api.getTypologyChlorinationInstallations().then((data: any) => {
                if (session && session.country) {
                    // The typologyChlorinationInstallations must be into the user session data.
                    session.country.typologyChlorinationInstallations = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTypologyChlorinationInstallations(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get interventionStatuses list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetInterventionStatuses(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.interventionStatuses) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.interventionStatuses,
            /**
             * Compare two interventionStatuses by name string.
             *
             * @param first {InterventionStatus}
             * @param second {InterventionStatus}
             * @returns {number}
             */
            itemComparer: (first:InterventionStatus, second: InterventionStatus) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetInterventionStatuses')) {
            // Get from API all interventionStatuses.
            return api.getInterventionStatuses().then((data: any) => {
                if (session && session.country) {
                    // The interventionStatuses must be into the user session data.
                    session.country.interventionStatuses = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetInterventionStatuses(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get typeHealthcareFacilities list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetTypeHealthcareFacilities(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.typeHealthcareFacilities) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.typeHealthcareFacilities,
            /**
             * Compare two typeHealthcareFacilities by name string.
             *
             * @param first {TypeHealthcareFacility}
             * @param second {TypeHealthcareFacility}
             * @returns {number}
             */
            itemComparer: (first:TypeHealthcareFacility, second: TypeHealthcareFacility) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetTypeHealthcareFacilities')) {
            // Get from API all typeHealthcareFacilities.
            return api.getTypeHealthcareFacilities().then((data: any) => {
                if (session && session.country) {
                    // The typeHealthcareFacilities must be into the user session data.
                    session.country.typeHealthcareFacilities = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTypeHealthcareFacilities(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get typeTaps list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetTypeTaps(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.typeTaps) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.typeTaps,
            /**
             * Compare two typeTaps by name string.
             *
             * @param first {TypeTap}
             * @param second {TypeTap}
             * @returns {number}
             */
            itemComparer: (first:TypeTap, second: TypeTap) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetTypeTaps')) {
            // Get from API all typeTaps.
            return api.getTypeTaps().then((data: any) => {
                if (session && session.country) {
                    // The typeTaps must be into the user session data.
                    session.country.typeTaps = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTypeTaps(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get waterQualityInterventionTypes list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetWaterQualityInterventionTypes(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.waterQualityInterventionTypes) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.waterQualityInterventionTypes,
            /**
             * Compare two waterQualityInterventionTypes by name string.
             *
             * @param first {WaterQualityInterventionType}
             * @param second {WaterQualityInterventionType}
             * @returns {number}
             */
            itemComparer: (first:WaterQualityInterventionType, second: WaterQualityInterventionType) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetWaterQualityInterventionTypes')) {
            // Get from API all waterQualityInterventionTypes.
            return api.getWaterQualityInterventionTypes().then((data: any) => {
                if (session && session.country) {
                    // The waterQualityInterventionTypes must be into the user session data.
                    session.country.waterQualityInterventionTypes = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetWaterQualityInterventionTypes(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get treatmentTechnologyCoagFloccus list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetTreatmentTechnologyCoagFloccus(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.treatmentTechnologyCoagFloccus) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.treatmentTechnologyCoagFloccus,
            /**
             * Compare two treatmentTechnologyCoagFloccus by name string.
             *
             * @param first {TreatmentTechnologyCoagFloccu}
             * @param second {TreatmentTechnologyCoagFloccu}
             * @returns {number}
             */
            itemComparer: (first:TreatmentTechnologyCoagFloccu, second: TreatmentTechnologyCoagFloccu) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetTreatmentTechnologyCoagFloccus')) {
            // Get from API all treatmentTechnologyCoagFloccus.
            return api.getTreatmentTechnologyCoagFloccus().then((data: any) => {
                if (session && session.country) {
                    // The treatmentTechnologyCoagFloccus must be into the user session data.
                    session.country.treatmentTechnologyCoagFloccus = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTreatmentTechnologyCoagFloccus(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get treatmentTechnologySedimentations list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetTreatmentTechnologySedimentations(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.treatmentTechnologySedimentations) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.treatmentTechnologySedimentations,
            /**
             * Compare two treatmentTechnologySedimentations by name string.
             *
             * @param first {TreatmentTechnologySedimentation}
             * @param second {TreatmentTechnologySedimentation}
             * @returns {number}
             */
            itemComparer: (first:TreatmentTechnologySedimentation, second: TreatmentTechnologySedimentation) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetTreatmentTechnologySedimentations')) {
            // Get from API all treatmentTechnologySedimentations.
            return api.getTreatmentTechnologySedimentations().then((data: any) => {
                if (session && session.country) {
                    // The treatmentTechnologySedimentations must be into the user session data.
                    session.country.treatmentTechnologySedimentations = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetTreatmentTechnologySedimentations(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get officialLanguages list.
 *
 * @param language {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetOfficialLanguages(language: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.officialLanguages) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.officialLanguages,
            /**
             * Compare two officialLanguages by language string.
             *
             * @param first {OfficialLanguage}
             * @param second {OfficialLanguage}
             * @returns {number}
             */
            itemComparer: (first:OfficialLanguage, second: OfficialLanguage) => first.language.toLocaleLowerCase() > second.language.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: language, propertyName: "language"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetOfficialLanguages')) {
            // Get from API all officialLanguages.
            return api.getOfficialLanguages().then((data: any) => {
                if (session && session.country) {
                    // The officialLanguages must be into the user session data.
                    session.country.officialLanguages = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetOfficialLanguages(language, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get main official_language.
 *
 * @returns {Promise}
 */
export function dmGetMainOfficialLanguage(): Promise<any> {
    let session = smGetSession();

    return new Promise((resolve) => resolve(session?.country?.mainOfficialLanguage));
}

/**
 * Get AdministrativeDivisionType list.
 *
 * @returns {Promise}
 */
export function dmGetAdministrativeDivisionTypes(): Promise<any> {
    return dmGetCountry().then((country: any) => {
        return country?.divisionTypes
    });
}

/**
 * Get AdministrativeDivision list.
 *
 * @param name {string}
 * @param code {string}
 * @param parent {string|null}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetAdministrativeDivisions(name: string = '', code: string = '', parent: string|null = '', page: number = -1): Promise<any> {
    // Hold the current promise to be use inside this method.
    let currentPromise = dmGetAdministrativeDivisionsPromise;

    // Create a new promise and hold as the new current promise.
    dmGetAdministrativeDivisionsPromise = new Promise(resolve => {
        // Wait for the current promise finished.
        currentPromise.then(() => {
            // Query if we have preloaded data in database.
            db.getAdministrativeDivisions('', '', parent, -1).then((data: any) => {
                if (data.length > 0) {
                    // We have preloaded data.
                    db.getAdministrativeDivisions(name, code, parent, page).then((data: any) => {
                        resolve(data);
                    });
                } else {
                    // Never call API in offline mode.
                    if (!dmOfflineIntercept('dmGetAdministrativeDivisions')) {
                        // We need call API to preload data, or not exist API data.
                        // Get from API the AdministrativeDivisions.
                        api.getAdministrativeDivisions('', '', parent).then((dataApi: any) => {
                            // If Api return empty, then there is no data.
                            if (dataApi.length === 0) {
                                resolve([]);
                            }
                            // Add all AdministrativeDivisions at once to db.
                            db.addAdministrativeDivisions(dataApi).then(() => {
                                // Now, we have all data into database, query again.
                                db.getAdministrativeDivisions(name, code, parent, page).then((data: any) => {
                                    resolve(data);
                                });
                                // resolve(dmGetAdministrativeDivisions(name, code, parent, page));
                            });
                        });
                    }
                }
            });
        });
    });

    return dmGetAdministrativeDivisionsPromise;
}

/**
 * Load administrative divisions.
 *
 * @param countryCode {string}
 * @param stepCallback {Function|null}
 *
 * @returns {Promise}
 */
export function dmGetAllAdministrativeDivisions(countryCode: string, stepCallback: Function|null = null): Promise<any> {
    if (!dmOfflineIntercept('dmGetAllAdministrativeDivisions')) {
        return api.getAllAdministrativeDivisions(countryCode, 1, [], stepCallback).then((data: any) => {
            // Save administrative divisions locally.
            console.log('Divisions loaded:', data.length);
            db.transaction('rw', db.documents, () => {
                if (stepCallback) {
                    stepCallback(0);
                }
                for (let i = 0; i < data.length; i++) {
                    let item = db._addAdministrativeDivision(data[i]);
                    db.documents.add(item);
                }
            });
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Get AdministrativeDivision.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmGetAdministrativeDivision(id: string): Promise<any> {
    if ("" === id) {
        return new Promise((resolve) => resolve(undefined));
    }
    return db.getAdministrativeDivision(id).then((data: any) => {
        if (data) {
            return data;
        }
        if (!dmIsOfflineMode()) {
            // Get from API the AdministrativeDivision.
            return api.getAdministrativeDivision(id)?.then((dataApi: any) => {
                // If Api return empty, then there is no data.
                if (!dataApi) {
                    return Promise.resolve(null);
                }

                // Get AdministrativeDivisions with same parent from API, and add all at once to DB.
                return dmGetAdministrativeDivisions('', '', dataApi.parent).then(() => {
                    // Return the AdministrativeDivision.
                    return dataApi;
                });
            });
        } else {
            return new Promise((resolve) => resolve(null));
        }
    });
}

/**
 * Get administrative division parents
 *
 * @param administrativeDivision {AdministrativeDivision}
 * @returns {Promise}
 */
export function dmGetAdministrativeDivisionParents(administrativeDivision: AdministrativeDivision): Promise<any> {
    if (!administrativeDivision || !administrativeDivision.parent) {
        return Promise.resolve([]);
    }

    let parentId = str_replace("/api/v1/administrative_divisions/", "", administrativeDivision.parent);

    return dmGetAdministrativeDivision(parentId).then((parent: AdministrativeDivision) => {
        return dmGetAdministrativeDivisionParents(parent).then(list => {
            return [parent, ... list];
        });
    });
}

/**
 * Generate an administrative division meta by A.D.
 *
 * @param {AdministrativeDivision|null} division
 *
 * @returns {Promise}
 */
export function dmGetAdministrativeDivisionMeta(division: AdministrativeDivision|null): Promise<any> {
    return new Promise((resolve) => {
        if (!division) {
            let meta:any = [];
            resolve(null);
        } else {
            return dmGetAdministrativeDivision(division.parent)
                .then((parent) => {
                    return dmGetAdministrativeDivisionMeta(parent)
                        .then((parentMeta) => {
                            let meta = {
                                "name": division.name,
                                "code": division.code,
                                "parent_id": parent ? parent.id : null,
                                "parent": parentMeta,
                            };
                            resolve(meta);
                        });
                });
        }
    });
}

/**
 * Get Special Components list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetSpecialComponents(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.specialComponents) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.specialComponents,
            /**
             * Compare two SpecialComponents by type string.
             *
             * @param first {SpecialComponent}
             * @param second {SpecialComponent}
             * @returns {number}
             */
            itemComparer: (first:SpecialComponent, second: SpecialComponent) => first.keyIndex.toLocaleLowerCase() > second.keyIndex.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetSpecialComponents')) {
            // Get from API all SpecialComponent.
            return api.getSpecialComponents().then((data: any) => {
                if (session && session.country) {
                    // The Special Component must be into the user session data.
                    session.country.specialComponents = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetSpecialComponents(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get Ethnicities list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetEthnicities(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.ethnicities) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.ethnicities,
            /**
             * Compare two Ethnicities by type string.
             *
             * @param first {Ethnicity}
             * @param second {Ethnicity}
             * @returns {number}
             */
            itemComparer: (first:Ethnicity, second: Ethnicity) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetEthnicities')) {
            // Get from API all Ethnicities.
            return api.getEthnicities().then((data: any) => {
                if (session && session.country) {
                    // The Ethnicity must be into the user session data.
                    session.country.ethnicities = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetEthnicities(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get DisinfectingSubstances list.
 *
 * @param name {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetDisinfectingSubstances(name: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.disinfectingSubstances) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.disinfectingSubstances,
            /**
             * Compare two DisinfectingSubstances by type string.
             *
             * @param first {DisinfectingSubstance}
             * @param second {DisinfectingSubstance}
             * @returns {number}
             */
            itemComparer: (first:DisinfectingSubstance, second: DisinfectingSubstance) => (first.name || "").toLocaleLowerCase() > (second.name || "").toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetDisinfectingSubstances')) {
            // Get from API all DisinfectingSubstances.
            return api.getDisinfectingSubstances().then((data: any) => {
                if (session && session.country) {
                    // The DisinfectingSubstances must be into the user session data.
                    session.country.disinfectingSubstances = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetDisinfectingSubstances(name, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get Distribution materials list.
 *
 * @param type {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetDistributionMaterials(type: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.distributionMaterials) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.distributionMaterials,
            /**
             * Compare two Distribution materials by type string.
             *
             * @param first {DistributionMaterial}
             * @param second {DistributionMaterial}
             * @returns {number}
             */
            itemComparer: (first:DistributionMaterial, second: DistributionMaterial) => first.type.toLocaleLowerCase() > second.type.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: type, propertyName: "type"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetDistributionMaterials')) {
            // Get from API all Distribution materials.
            return api.getDistributionMaterials().then((data: any) => {
                if (session && session.country) {
                    // The Distribution materials must be into the user session data.
                    session.country.distributionMaterials = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetDistributionMaterials(type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get Default Diameters list.
 *
 * @param value {number}
 * @param unit {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetDefaultDiameters(value: string = "", unit: string = "", page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.defaultDiameters) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.defaultDiameters,
            /**
             * Compare two DefaultDiameters by type string.
             *
             * @param first {DefaultDiameter}
             * @param second {DefaultDiameter}
             * @returns {number}
             */
            itemComparer: (first:DefaultDiameter, second: DefaultDiameter) => first.value > second.value ? -1 : 1,
            filter: [{value: value, propertyName: "value"}, {value: unit, propertyName: "unit"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetDefaultDiameters')) {
            // Get from API all DefaultDiameters.
            return api.getDefaultDiameters().then((data: any) => {
                if (session && session.country) {
                    // The DefaultDiameters must be into the user session data.
                    session.country.defaultDiameters = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetDefaultDiameters(value, unit, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get materials list.
 *
 * @param type {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetMaterials(type: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.materials) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.materials,
            /**
             * Compare two materials by type string.
             *
             * @param first {Material}
             * @param second {Material}
             * @returns {number}
             */
            itemComparer: (first: Material, second: Material) => first.type.toLocaleLowerCase() > second.type.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: type, propertyName: "type"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetMaterials')) {
            // Get from API all materials.
            return api.getMaterials().then((data: any) => {
                if (session && session.country) {
                    // The materials must be into the user session data.
                    session.country.materials = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetMaterials(type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get pumpTypes list.
 *
 * @param type {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetPumpTypes(type: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.pumpTypes) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.pumpTypes,
            /**
             * Compare two pumpTypes by type string.
             *
             * @param first {PumpType}
             * @param second {PumpType}
             * @returns {number}
             */
            itemComparer: (first: PumpType, second: PumpType) => first.type.toLocaleLowerCase() > second.type.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: type, propertyName: "type"}],
        });
    } else {
        if (!dmOfflineIntercept('dmGetPumpTypes')) {
            // Get from API all pumpTypes.
            return api.getPumpTypes().then((data: any) => {
                if (session && session.country) {
                    // The pumpTypes must be into the user session data.
                    session.country.pumpTypes = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetPumpTypes(type, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get institutionInterventions list.
 *
 * @param name {string}
 * @param contact {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetInstitutionInterventions(name: string = '', contact: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.institutionInterventions) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.institutionInterventions,
            /**
             * Compare two institutionInterventions by name string.
             *
             * @param first {InstitutionIntervention}
             * @param second {InstitutionIntervention}
             * @returns {number}
             */
            itemComparer: (first: InstitutionIntervention, second: InstitutionIntervention) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}, {value: contact, propertyName: "contact"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetInstitutionInterventions')) {
            // Get from API all institutionInterventions.
            return api.getInstitutionInterventions().then((data: any) => {
                if (session && session.country) {
                    // The institutionInterventions must be into the user session data.
                    session.country.institutionInterventions = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetInstitutionInterventions(name, contact, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get waterQualityEntities list.
 *
 * @param name {string}
 * @param contact {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetWaterQualityEntities(name: string = '', contact: string = '', page: number = 1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.waterQualityEntities) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.waterQualityEntities,
            /**
             * Compare two waterQualityEntities by name string.
             *
             * @param first {WaterQualityEntity}
             * @param second {WaterQualityEntity}
             * @returns {number}
             */
            itemComparer: (first: WaterQualityEntity, second: WaterQualityEntity) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}, {value: contact, propertyName: "contact"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetWaterQualityEntities')) {
            // Get from API all waterQualityEntities.
            return api.getWaterQualityEntities().then((data: any) => {
                if (session && session.country) {
                    // The waterQualityEntities must be into the user session data.
                    session.country.waterQualityEntities = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetWaterQualityEntities(name, contact, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}

/**
 * Get funderInterventions list.
 *
 * @param name {string}
 * @param contact {string}
 * @param page {number}
 * @returns {Promise}
 */
export function dmGetFunderInterventions(name: string = '', contact: string = '', page: number = -1): Promise<any> {
    let session = smGetSession();
    if (session && session?.country?.funderInterventions) {
        return localCountryParametricQuerier(page, {
            list: session?.country?.funderInterventions,
            /**
             * Compare two funderInterventions by name string.
             *
             * @param first {FunderIntervention}
             * @param second {FunderIntervention}
             * @returns {number}
             */
            itemComparer: (first: FunderIntervention, second: FunderIntervention) => first.name.toLocaleLowerCase() > second.name.toLocaleLowerCase() ? -1 : 1,
            filter: [{value: name, propertyName: "name"}, {value: contact, propertyName: "contact"}],

        });
    } else {
        if (!dmOfflineIntercept('dmGetFunderInterventions')) {
            // Get from API all funderInterventions.
            return api.getFunderInterventions().then((data: any) => {
                if (session && session.country) {
                    // The funderInterventions must be into the user session data.
                    session.country.funderInterventions = data;
                    smSetSession(session);
                    // Now, it's in session, we must apply filter.
                    data = dmGetFunderInterventions(name, contact, page);
                }
                return data;
            });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    }
}
