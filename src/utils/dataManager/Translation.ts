/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import api from "@/utils/api";
import {i18nGetCurrentLanguage, i18nGetStrings, i18nSetStrings} from "@/utils/i18n";
import {dmOfflineIntercept} from "@/utils/dataManager";

/**
 * filter translations
 *
 * @param iso_code {string}
 * @param search_in {string}
 * @param contains {string}
 * @param page {number}
 *
 * @returns {Promise}
 */
export function dmSearchTranslationStrings(
    iso_code: string,
    search_in: 'all' | 'untranslated' | 'translated',
    contains: string,
    page: number | undefined = undefined
) {
    if (!dmOfflineIntercept('dmSearchTranslationStrings')) {
        return api.searchTranslationStrings(iso_code, search_in, contains, page);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * save a new translation
 *
 * @param iso_code {string}
 * @param source {string}
 * @param target {string}
 *
 * @returns {Propmise}
 */
export function dmSaveTranslationString(iso_code: string, source: string, target: string) {
    if (!dmOfflineIntercept('dmSaveTranslationString')) {
        return api.saveTranslationString(iso_code, source, target);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * get transtalion progress
 *
 * @param iso_code {string}
 *
 * @returns {Promise}
 */
export function dmGetTranslationProgress(iso_code: string) {
    if (!dmOfflineIntercept('dmGetTranslationProgress')) {
        return api.getTranslationProgress(iso_code);
    } else {
        return new Promise((resolve) => resolve({
            "iso_code": iso_code,
            "n_translated": 0,
            "n_untranslated": 0,
            "percentage": 0
        }));
    }
}

/**
 * Get translation string collection.
 *
 * @param lang {string | null | undefined}
 *
 * @returns {Array<I18nLanguage> | null}
 */
export function dmGetI18nStrings(lang?: string) {
    return new Promise((resolve) => {
        let language: string = lang ?? i18nGetCurrentLanguage();
        const defaultLang = {
            id: language,
            literals: {},
        };
        let currentLiterals: any = i18nGetStrings(language);
        if (Object.keys(currentLiterals.literals).length > 0) {
            return resolve(currentLiterals);
        }

        if (!dmOfflineIntercept('dmGetI18nStrings')) {
            return api.getI18n(language)
                .then((res) => {
                    i18nSetStrings(res);
                    return resolve(res);
                })
                .catch(reason => {
                    // Else, return default collection.
                    i18nSetStrings(defaultLang);
                    return resolve(defaultLang);
                });
        } else {
            return new Promise((resolve) => resolve([]));
        }
    });
}
