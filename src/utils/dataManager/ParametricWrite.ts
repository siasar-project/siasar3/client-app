/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {db} from "@/utils/db";
import {dmInvalidateInSession, dmOfflineIntercept} from "@/utils/dataManager";
import Api from "@/utils/api";
import {FunctionsCarriedOutWsp} from "@/objects/FunctionsCarriedOutWsp";
import {TypeIntervention} from "@/objects/TypeIntervention";
import {CommunityService} from "@/objects/CommunityService";
import {Currency} from "@/objects/Currency";
import {ProgramIntervention} from "@/objects/ProgramIntervention";
import {TypologyChlorinationInstallation} from "@/objects/TypologyChlorinationInstallation";
import {InterventionStatus} from "@/objects/InterventionStatus";
import {TypeHealthcareFacility} from "@/objects/TypeHealthcareFacility";
import {TypeTap} from "@/objects/TypeTap";
import {WaterQualityInterventionType} from "@/objects/WaterQualityInterventionType";
import {TreatmentTechnologyCoagFloccu} from "@/objects/TreatmentTechnologyCoagFloccu";
import {TreatmentTechnologySedimentation} from "@/objects/TreatmentTechnologySedimentation";
import {OfficialLanguage} from "@/objects/OfficialLanguage";
import {SpecialComponent} from "@/objects/SpecialComponent";
import {InstitutionIntervention} from "@/objects/InstitutionIntervention";
import {WaterQualityEntity} from "@/objects/WaterQualityEntity";
import {FunderIntervention} from "@/objects/FunderIntervention";

/**
 * Alert internal AdministrativeDivisions changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @param parent {string|null} AdministrativeDivision parent property.
 *
 * @returns {Promise}
 */
export function dmInvalidateAdministrativeDivisions(parent: string|null) {
    return db.invalidateAdministrativeDivisions(parent);
}

/**
 * Alert internal AdministrativeDivisions changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateAllAdministrativeDivisions() {
    return db.invalidateAllAdministrativeDivisions();
}

/**
 * Alert internal InstitutionInterventions changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateInstitutionInterventions() {
    return dmInvalidateInSession("institutionInterventions");
}

/**
 * Alert internal WaterQualityEntities changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateWaterQualityEntities() {
    return dmInvalidateInSession("waterQualityEntities");
}

/**
 * Alert internal FunderInterventions changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateFunderInterventions() {
    return dmInvalidateInSession("funderInterventions");
}

/**
 * Alert internal special components changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateSpecialComponents() {
    return dmInvalidateInSession("specialComponents");
}

/**
 * Alert internal Ethnicities changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateEthnicities() {
    return dmInvalidateInSession("ethnicities");
}

/**
 * Alert internal DisinfectingSubstances changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateDisinfectingSubstances() {
    return dmInvalidateInSession("disinfectingSubstances");
}

/**
 * Alert internal Distribution Materials changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateDistributionMaterials() {
    return dmInvalidateInSession("distributionMaterials");
}

/**
 * Alert internal Default Diameter changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateDefaultDiameters() {
    return dmInvalidateInSession("defaultDiameters");
}

/**
 * Alert internal Storage infrastructure materials changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateMaterials() {
    return dmInvalidateInSession("materials");
}

/**
 * Alert internal PumpTypes changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidatePumpTypes() {
    return dmInvalidateInSession("pumpTypes");
}

/**
 * Alert internal OfficialLanguages changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateOfficialLanguages() {
    return dmInvalidateInSession("officialLanguages");
}

/**
 * Alert internal Currencies changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateCurrencies() {
    return dmInvalidateInSession("currencies");
}

/**
 * Alert internal ProgramInterventions changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateProgramInterventions() {
    return dmInvalidateInSession("programInterventions");
}

/**
 * Alert internal TypologyChlorinationInstallations changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTypologyChlorinationInstallations() {
    return dmInvalidateInSession("typologyChlorinationInstallations");
}

/**
 * Alert internal InterventionStatuses changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateInterventionStatuses() {
    return dmInvalidateInSession("interventionStatuses");
}

/**
 * Alert internal TypeHealthcareFacilities changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTypeHealthcareFacilities() {
    return dmInvalidateInSession("typeHealthcareFacilities");
}

/**
 * Alert internal TypeTaps changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTypeTaps() {
    return dmInvalidateInSession("typeTaps");
}

/**
 * Alert internal WaterQualityInterventionTypes changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateWaterQualityInterventionTypes() {
    return dmInvalidateInSession("waterQualityInterventionTypes");
}

/**
 * Alert internal TreatmentTechnologyCoagFloccus changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTreatmentTechnologyCoagFloccus() {
    return dmInvalidateInSession("treatmentTechnologyCoagFloccus");
}

/**
 * Alert internal TreatmentTechnologySedimentations changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTreatmentTechnologySedimentations() {
    return dmInvalidateInSession("treatmentTechnologySedimentations");
}

/**
 * Alert internal TypeInterventions changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTypeInterventions() {
    return dmInvalidateInSession("typeInterventions");
}

/**
 * Alert internal CommunityServices changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateCommunityServices() {
    return dmInvalidateInSession("communityServices");
}

/**
 * Alert internal TechnicalAssistanceProviders changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTechnicalAssistanceProviders() {
    return dmInvalidateInSession("technicalAssistanceProviders");
}

/**
 * Alert internal treatment technology filtration changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTreatmentTechnologyAerationOxidation() {
    return dmInvalidateInSession("treatmentTechnologyAerationOxidation");
}

/**
 * Alert internal treatment technology filtration changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTreatmentTechnologyDesalination() {
    return dmInvalidateInSession("treatmentTechnologyDesalination");
}

/**
 * Alert internal treatment technology filtration changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateTreatmentTechnologyFiltration() {
    return dmInvalidateInSession("treatmentTechnologyFiltration");
}

/**
 * Alert internal geographical scope changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateGeographicalScope() {
    return dmInvalidateInSession("geographicalScopes");
}

/**
 * Alert internal Materials changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateFunctionCarriedOutTap() {
    return dmInvalidateInSession("functionsCarriedOutTap");
}

/**
 * Alert internal FunctionsCarriedOutWsps changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
export function dmInvalidateFunctionsCarriedOutWsps() {
    return dmInvalidateInSession("functionsCarriedOutWsps");
}

/**
 * Add a new country TechnicalAssistanceProviders TechnicalAssistanceProviders.
 *
 * @param country {string}.
 * @param name {string}.
 * @param description {string}.
 * @param nationalId {string}.
 * @returns {Promise}
 */
export function dmAddTechnicalAssistanceProviders(country: string, name: string, description: string, nationalId: string) {
    if (!dmOfflineIntercept('dmAddTechnicalAssistanceProviders')) {
        return Api.addTechnicalAssistanceProviders({country, name, description, nationalId})
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country TechnicalAssistanceProviders.
 *
 * @param id {string} TechnicalAssistanceProviders TechnicalAssistanceProviders ID.
 * @param name {string} TechnicalAssistanceProviders TechnicalAssistanceProviders name property.
 * @param description
 * @param nationalId
 * @returns {Promise}
 */
export function dmUpdateTechnicalAssistanceProviders(id: string, name: string, description: string, nationalId: string) {
    if (!dmOfflineIntercept('dmUpdateTechnicalAssistanceProviders')) {
        return Api.updateTechnicalAssistanceProviders({id, name, description, nationalId});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country TechnicalAssistanceProviders.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTechnicalAssistanceProviders(id: string) {
    if (!dmOfflineIntercept('dmRemoveTechnicalAssistanceProviders')) {
        return Api.removeTechnicalAssistanceProviders(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country treatment technology filtration.
 *
 * @param country {string}.
 * @param name {string}.
 * @returns {Promise}
 */
export function dmAddTreatmentTechnologyAerationOxidation(country: string, name: string) {
    if (!dmOfflineIntercept('dmAddTreatmentTechnologyAerationOxidation')) {
        return Api.addTreatmentTechnologyAerationOxidation({country, name})
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country treatment technology filtration.
 *
 * @param id {string} TreatmentTechnologyAerationOxidation TreatmentTechnologyAerationOxidation ID.
 * @param name {string} TreatmentTechnologyAerationOxidation TreatmentTechnologyAerationOxidation name property.
 * @returns {Promise}
 */
export function dmUpdateTreatmentTechnologyAerationOxidation(id: string, name: string) {
    if (!dmOfflineIntercept('dmUpdateTreatmentTechnologyAerationOxidation')) {
        return Api.updateTreatmentTechnologyAerationOxidation({id, name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country treatment technology filtration.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTreatmentTechnologyAerationOxidation(id: string) {
    if (!dmOfflineIntercept('dmRemoveTreatmentTechnologyAerationOxidation')) {
        return Api.removeTreatmentTechnologyAerationOxidation(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country treatment technology filtration.
 *
 * @param country {string}.
 * @param name {string}.
 * @returns {Promise}
 */
export function dmAddTreatmentTechnologyDesalination(country: string, name: string) {
    if (!dmOfflineIntercept('dmAddTreatmentTechnologyDesalination')) {
        return Api.addTreatmentTechnologyDesalination({country, name})
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country treatment technology filtration.
 *
 * @param id {string} TreatmentTechnologyDesalination TreatmentTechnologyDesalination ID.
 * @param name {string} TreatmentTechnologyDesalination TreatmentTechnologyDesalination name property.
 * @returns {Promise}
 */
export function dmUpdateTreatmentTechnologyDesalination(id: string, name: string) {
    if (!dmOfflineIntercept('dmUpdateTreatmentTechnologyDesalination')) {
        return Api.updateTreatmentTechnologyDesalination({id, name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country treatment technology filtration.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTreatmentTechnologyDesalination(id: string) {
    if (!dmOfflineIntercept('dmRemoveTreatmentTechnologyDesalination')) {
        return Api.removeTreatmentTechnologyDesalination(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country treatment technology filtration.
 *
 * @param country {string}.
 * @param name {string}.
 * @returns {Promise}
 */
export function dmAddTreatmentTechnologyFiltration(country: string, name: string) {
    if (!dmOfflineIntercept('dmAddTreatmentTechnologyFiltration')) {
        return Api.addTreatmentTechnologyFiltration({country, name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country treatment technology filtration.
 *
 * @param id {string} TreatmentTechnologyFiltration TreatmentTechnologyFiltration ID.
 * @param name {string} TreatmentTechnologyFiltration TreatmentTechnologyFiltration name property.
 * @returns {Promise}
 */
export function dmUpdateTreatmentTechnologyFiltration(id: string, name: string) {
    if (!dmOfflineIntercept('dmUpdateTreatmentTechnologyFiltration')) {
        return Api.updateTreatmentTechnologyFiltration({id, name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country treatment technology filtration.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTreatmentTechnologyFiltration(id: string) {
    if (!dmOfflineIntercept('dmRemoveTreatmentTechnologyFiltration')) {
        return Api.removeTreatmentTechnologyFiltration(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country geographical scope.
 *
 * @param country {string}.
 * @param keyIndex {string}.
 * @param name {string}.
 * @returns {Promise}
 */
export function dmAddGeographicalScope(country: string, keyIndex: string, name: string) {
    if (!dmOfflineIntercept('dmAddGeographicalScope')) {
        return Api.addGeographicalScope({ country, keyIndex, name });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country geographical scope.
 *
 * @param id {string} geographical scope ID.
 * @param keyIndex {string} geographical scope key index.
 * @param name {string} geographical scope name property.
 * @returns {Promise}
 */
export function dmUpdateGeographicalScope(id: string, keyIndex: string, name: string) {
    if (!dmOfflineIntercept('dmUpdateGeographicalScope')) {
        return Api.updateGeographicalScope({ id, keyIndex, name });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country geographical scope.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveGeographicalScope(id: string) {
    if (!dmOfflineIntercept('dmRemoveGeographicalScope')) {
        return Api.removeGeographicalScope(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country function carried out by TAP.
 *
 * @param country {string}.
 * @param keyIndex {string}.
 * @param type {string}.
 * @returns {Promise}
 */
export function dmAddFunctionCarriedOutTap(country: string, keyIndex: string,  type: string) {
    if (!dmOfflineIntercept('dmAddFunctionCarriedOutTap')) {
        return Api.addFunctionCarriedOutTap({country, keyIndex, type});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country FunctionCarriedOutTap.
 *
 * @param id {string} Function carried out by TAP ID.
 * @param keyIndex {string} Function carried out by TAP key index.
 * @param type {string} Function carried out by TAP  type property.
 * @returns {Promise}
 */
export function dmUpdateFunctionCarriedOutTap(id: string, keyIndex: string, type: string) {
    if (!dmOfflineIntercept('dmUpdateFunctionCarriedOutTap')) {
        return Api.updateFunctionCarriedOutTap({id, keyIndex, type});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country FunctionCarriedOutTap.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveFunctionCarriedOutTap(id: string) {
    if (!dmOfflineIntercept('dmRemoveFunctionCarriedOutTap')) {
        return Api.removeFuncionCarriedOutTap(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country functionsCarriedOutWsp.
 *
 * @param country {string} Owner country IRI.
 * @param functionsCarriedOutWsp
 * @param functionsCarriedOutWsp.keyIndex {string} FunctionsCarriedOutWsp keyIndex property.
 * @param functionsCarriedOutWsp.type {string} FunctionsCarriedOutWsp type property.
 * @returns {Promise}
 */
export function dmAddFunctionsCarriedOutWsp(country: string, functionsCarriedOutWsp: FunctionsCarriedOutWsp) {
    if (!dmOfflineIntercept('dmAddFunctionsCarriedOutWsp')) {
        return Api.addFunctionsCarriedOutWsp({
            country: country,
            keyIndex: functionsCarriedOutWsp.keyIndex,
            type: functionsCarriedOutWsp.type,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country functionsCarriedOutWsp.
 *
 * @param functionsCarriedOutWsp
 * @param functionsCarriedOutWsp.id {string} FunctionsCarriedOutWsp ID.
 * @param functionsCarriedOutWsp.keyIndex {string} FunctionsCarriedOutWsp keyIndex property.
 * @param functionsCarriedOutWsp.type {string} FunctionsCarriedOutWsp type property.
 * @returns {Promise}
 */
export function dmUpdateFunctionsCarriedOutWsp(functionsCarriedOutWsp: FunctionsCarriedOutWsp) {
    if (!dmOfflineIntercept('dmUpdateFunctionsCarriedOutWsp')) {
        return Api.updateFunctionsCarriedOutWsp({
            id: functionsCarriedOutWsp.id ?? '',
            keyIndex: functionsCarriedOutWsp.keyIndex,
            type: functionsCarriedOutWsp.type,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country functionsCarriedOutWsp.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveFunctionsCarriedOutWsp(id: string) {
    if (!dmOfflineIntercept('dmRemoveFunctionsCarriedOutWsp')) {
        return Api.removeFunctionsCarriedOutWsp(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country typeIntervention.
 *
 * @param country {string} Owner country IRI.
 * @param instance {TypeIntervention} TypeIntervention object.
 * @returns {Promise}
 */
export function dmAddTypeIntervention(country: string, instance: TypeIntervention) {
    if (!dmOfflineIntercept('dmAddTypeIntervention')) {
        return Api.addTypeIntervention({country: country, type: instance.type, description: instance.description || ""});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country typeIntervention.
 *
 * @param id {string} TypeIntervention ID.
 * @param instance {TypeIntervention} TypeIntervention object.
 * @returns {Promise}
 */
export function dmUpdateTypeIntervention(id: string, instance: TypeIntervention) {
    if (!dmOfflineIntercept('dmUpdateTypeIntervention')) {
        return Api.updateTypeIntervention({
            id: id,
            type: instance.type,
            description: instance.description || ""
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country typeIntervention.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTypeIntervention(id: string) {
    if (!dmOfflineIntercept('dmRemoveTypeIntervention')) {
        return Api.removeTypeIntervention(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country communityService.
 *
 * @param country {string} Owner country IRI.
 * @param instance {CommunityService} CommunityService object.
 * @returns {Promise}
 */
export function dmAddCommunityService(country: string, instance: CommunityService) {
    if (!dmOfflineIntercept('dmAddCommunityService')) {
        return Api.addCommunityService({country: country, type: instance.type});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country communityService.
 *
 * @param id {string} CommunityService ID.
 * @param instance {CommunityService} CommunityService object.
 * @returns {Promise}
 */
export function dmUpdateCommunityService(id: string, instance: CommunityService) {
    if (!dmOfflineIntercept('dmUpdateCommunityService')) {
        return Api.updateCommunityService({ id: id, type: instance.type });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country communityService.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveCommunityService(id: string) {
    if (!dmOfflineIntercept('dmRemoveCommunityService')) {
        return Api.removeCommunityService(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country currency.
 *
 * @param country {string} Owner country IRI.
 * @param currency
 * @param currency.name {string} Currency name property.
 * @returns {Promise}
 */
export function dmAddCurrency(country: string, currency: Currency) {
    if (!dmOfflineIntercept('dmAddCurrency')) {
        return Api.addCurrency({
            country: country,
            name: currency.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country currency.
 *
 * @param currency
 * @param currency.id {string} Currency ID.
 * @param currency.name {string} Currency name property.
 * @returns {Promise}
 */
export function dmUpdateCurrency(currency: Currency) {
    if (!dmOfflineIntercept('dmUpdateCurrency')) {
        return Api.updateCurrency({ id: currency.id ?? '', name: currency.name });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country currency.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveCurrency(id: string) {
    if (!dmOfflineIntercept('dmRemoveCurrency')) {
        return Api.removeCurrency(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country ProgramIntervention.
 *
 * @param country {string} Owner country IRI.
 * @param instance {ProgramIntervention} ProgramIntervention object.
 * @returns {Promise}
 */
export function dmAddProgramIntervention(country: string, instance:ProgramIntervention) {
    if (!dmOfflineIntercept('dmAddProgramIntervention')) {
        return Api.addProgramIntervention({country: country, name: instance.name || "", description: instance.description || ""});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country ProgramIntervention.
 *
 * @param id {string} ProgramIntervention ID.
 * @param instance {ProgramIntervention} ProgramIntervention object.
 * @returns {Promise}
 */
export function dmUpdateProgramIntervention(id: string, instance:ProgramIntervention) {
    if (!dmOfflineIntercept('dmUpdateProgramIntervention')) {
        return Api.updateProgramIntervention({
            id: id, name: instance.name || "",
            description: instance.description || ""
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country ProgramIntervention.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveProgramIntervention(id: string) {
    if (!dmOfflineIntercept('dmRemoveProgramIntervention')) {
        return Api.removeProgramIntervention(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country typologyChlorinationInstallation.
 *
 * @param country {string} Owner country IRI.
 * @param typologyChlorinationInstallation
 * @param typologyChlorinationInstallation.keyIndex {string} TypologyChlorinationInstallation keyIndex property.
 * @param typologyChlorinationInstallation.name {string} TypologyChlorinationInstallation name property.
 * @returns {Promise}
 */
export function dmAddTypologyChlorinationInstallation(country: string, typologyChlorinationInstallation: TypologyChlorinationInstallation) {
    if (!dmOfflineIntercept('dmAddTypologyChlorinationInstallation')) {
        return Api.addTypologyChlorinationInstallation({
            country: country,
            keyIndex: typologyChlorinationInstallation.keyIndex,
            name: typologyChlorinationInstallation.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country typologyChlorinationInstallation.
 *
 * @param typologyChlorinationInstallation
 * @param typologyChlorinationInstallation.id {string} TypologyChlorinationInstallation ID.
 * @param typologyChlorinationInstallation.keyIndex {string} TypologyChlorinationInstallation keyIndex property.
 * @param typologyChlorinationInstallation.name {string} TypologyChlorinationInstallation name property.
 * @returns {Promise}
 */
export function dmUpdateTypologyChlorinationInstallation(typologyChlorinationInstallation: TypologyChlorinationInstallation) {
    if (!dmOfflineIntercept('dmUpdateTypologyChlorinationInstallation')) {
        return Api.updateTypologyChlorinationInstallation({
            id: typologyChlorinationInstallation.id ?? '',
            keyIndex: typologyChlorinationInstallation.keyIndex,
            name: typologyChlorinationInstallation.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country typologyChlorinationInstallation.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTypologyChlorinationInstallation(id: string) {
    if (!dmOfflineIntercept('dmRemoveTypologyChlorinationInstallation')) {
        return Api.removeTypologyChlorinationInstallation(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country interventionStatus.
 *
 * @param country {string} Owner country IRI.
 * @param interventionStatus
 * @param interventionStatus.keyIndex {string} InterventionStatus keyIndex property.
 * @param interventionStatus.name {string} InterventionStatus name property.
 * @returns {Promise}
 */
export function dmAddInterventionStatus(country: string, interventionStatus: InterventionStatus) {
    if (!dmOfflineIntercept('dmAddInterventionStatus')) {
        return Api.addInterventionStatus({
            country: country,
            keyIndex: interventionStatus.keyIndex,
            name: interventionStatus.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country interventionStatus.
 *
 * @param interventionStatus
 * @param interventionStatus.id {string} InterventionStatus ID.
 * @param interventionStatus.keyIndex {string} InterventionStatus keyIndex property.
 * @param interventionStatus.name {string} InterventionStatus name property.
 * @returns {Promise}
 */
export function dmUpdateInterventionStatus(interventionStatus: InterventionStatus) {
    if (!dmOfflineIntercept('dmUpdateInterventionStatus')) {
        return Api.updateInterventionStatus({
            id: interventionStatus.id ?? '',
            keyIndex: interventionStatus.keyIndex,
            name: interventionStatus.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country interventionStatus.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveInterventionStatus(id: string) {
    if (!dmOfflineIntercept('dmRemoveInterventionStatus')) {
        return Api.removeInterventionStatus(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country typeHealthcareFacility.
 *
 * @param country {string} Owner country IRI.
 * @param typeHealthcareFacility
 * @param typeHealthcareFacility.keyIndex {string} TypeHealthcareFacility keyIndex property.
 * @param typeHealthcareFacility.name {string} TypeHealthcareFacility name property.
 * @returns {Promise}
 */
export function dmAddTypeHealthcareFacility(country: string, typeHealthcareFacility: TypeHealthcareFacility) {
    if (!dmOfflineIntercept('dmAddTypeHealthcareFacility')) {
        return Api.addTypeHealthcareFacility({
            country: country,
            keyIndex: typeHealthcareFacility.keyIndex,
            name: typeHealthcareFacility.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country typeHealthcareFacility.
 *
 * @param typeHealthcareFacility
 * @param typeHealthcareFacility.id {string} TypeHealthcareFacility ID.
 * @param typeHealthcareFacility.keyIndex {string} TypeHealthcareFacility keyIndex property.
 * @param typeHealthcareFacility.name {string} TypeHealthcareFacility name property.
 * @returns {Promise}
 */
export function dmUpdateTypeHealthcareFacility(typeHealthcareFacility: TypeHealthcareFacility) {
    if (!dmOfflineIntercept('dmUpdateTypeHealthcareFacility')) {
        return Api.updateTypeHealthcareFacility({
            id: typeHealthcareFacility.id ?? '',
            keyIndex: typeHealthcareFacility.keyIndex,
            name: typeHealthcareFacility.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country typeHealthcareFacility.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTypeHealthcareFacility(id: string) {
    if (!dmOfflineIntercept('dmRemoveTypeHealthcareFacility')) {
        return Api.removeTypeHealthcareFacility(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country typeTap.
 *
 * @param country {string} Owner country IRI.
 * @param typeTap
 * @param typeTap.keyIndex {string} TypeTap keyIndex property.
 * @param typeTap.name {string} TypeTap name property.
 * @returns {Promise}
 */
export function dmAddTypeTap(country: string, typeTap: TypeTap) {
    if (!dmOfflineIntercept('dmAddTypeTap')) {
        return Api.addTypeTap({
            country: country,
            keyIndex: typeTap.keyIndex,
            name: typeTap.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country typeTap.
 *
 * @param typeTap
 * @param typeTap.id {string} TypeTap ID.
 * @param typeTap.keyIndex {string} TypeTap keyIndex property.
 * @param typeTap.name {string} TypeTap name property.
 * @returns {Promise}
 */
export function dmUpdateTypeTap(typeTap: TypeTap) {
    if (!dmOfflineIntercept('dmUpdateTypeTap')) {
        return Api.updateTypeTap({
            id: typeTap.id ?? '',
            keyIndex: typeTap.keyIndex,
            name: typeTap.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country typeTap.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTypeTap(id: string) {
    if (!dmOfflineIntercept('dmRemoveTypeTap')) {
        return Api.removeTypeTap(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country waterQualityInterventionType.
 *
 * @param country {string} Owner country IRI.
 * @param waterQualityInterventionType
 * @param waterQualityInterventionType.keyIndex {string} WaterQualityInterventionType keyIndex property.
 * @param waterQualityInterventionType.name {string} WaterQualityInterventionType name property.
 * @returns {Promise}
 */
export function dmAddWaterQualityInterventionType(country: string, waterQualityInterventionType: WaterQualityInterventionType) {
    if (!dmOfflineIntercept('dmAddWaterQualityInterventionType')) {
        return Api.addWaterQualityInterventionType({
            country: country,
            keyIndex: waterQualityInterventionType.keyIndex,
            name: waterQualityInterventionType.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country waterQualityInterventionType.
 *
 * @param waterQualityInterventionType
 * @param waterQualityInterventionType.id {string} WaterQualityInterventionType ID.
 * @param waterQualityInterventionType.keyIndex {string} WaterQualityInterventionType keyIndex property.
 * @param waterQualityInterventionType.name {string} WaterQualityInterventionType name property.
 * @returns {Promise}
 */
export function dmUpdateWaterQualityInterventionType(waterQualityInterventionType: WaterQualityInterventionType) {
    if (!dmOfflineIntercept('dmUpdateWaterQualityInterventionType')) {
        return Api.updateWaterQualityInterventionType({
            id: waterQualityInterventionType.id ?? '',
            keyIndex: waterQualityInterventionType.keyIndex,
            name: waterQualityInterventionType.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country waterQualityInterventionType.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveWaterQualityInterventionType(id: string) {
    if (!dmOfflineIntercept('dmRemoveWaterQualityInterventionType')) {
        return Api.removeWaterQualityInterventionType(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country treatmentTechnologyCoagFloccu.
 *
 * @param country {string} Owner country IRI.
 * @param treatmentTechnologyCoagFloccu
 * @param treatmentTechnologyCoagFloccu.name {string} TreatmentTechnologyCoagFloccu name property.
 * @returns {Promise}
 */
export function dmAddTreatmentTechnologyCoagFloccu(country: string, treatmentTechnologyCoagFloccu: TreatmentTechnologyCoagFloccu) {
    if (!dmOfflineIntercept('dmAddTreatmentTechnologyCoagFloccu')) {
        return Api.addTreatmentTechnologyCoagFloccu({
            country: country,
            name: treatmentTechnologyCoagFloccu.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country treatmentTechnologyCoagFloccu.
 *
 * @param treatmentTechnologyCoagFloccu
 * @param treatmentTechnologyCoagFloccu.id {string} TreatmentTechnologyCoagFloccu ID.
 * @param treatmentTechnologyCoagFloccu.name {string} TreatmentTechnologyCoagFloccu name property.
 * @returns {Promise}
 */
export function dmUpdateTreatmentTechnologyCoagFloccu(treatmentTechnologyCoagFloccu: TreatmentTechnologyCoagFloccu) {
    if (!dmOfflineIntercept('dmUpdateTreatmentTechnologyCoagFloccu')) {
        return Api.updateTreatmentTechnologyCoagFloccu({
            id: treatmentTechnologyCoagFloccu.id ?? '',
            name: treatmentTechnologyCoagFloccu.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country treatmentTechnologyCoagFloccu.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTreatmentTechnologyCoagFloccu(id: string) {
    if (!dmOfflineIntercept('dmRemoveTreatmentTechnologyCoagFloccu')) {
        return Api.removeTreatmentTechnologyCoagFloccu(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country treatmentTechnologySedimentation.
 *
 * @param country {string} Owner country IRI.
 * @param treatmentTechnologySedimentation
 * @param treatmentTechnologySedimentation.name {string} TreatmentTechnologySedimentation name property.
 * @returns {Promise}
 */
export function dmAddTreatmentTechnologySedimentation(country: string, treatmentTechnologySedimentation: TreatmentTechnologySedimentation) {
    if (!dmOfflineIntercept('dmAddTreatmentTechnologySedimentation')) {
        return Api.addTreatmentTechnologySedimentation({
            country: country,
            name: treatmentTechnologySedimentation.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country treatmentTechnologySedimentation.
 *
 * @param treatmentTechnologySedimentation
 * @param treatmentTechnologySedimentation.id {string} TreatmentTechnologySedimentation ID.
 * @param treatmentTechnologySedimentation.name {string} TreatmentTechnologySedimentation name property.
 * @returns {Promise}
 */
export function dmUpdateTreatmentTechnologySedimentation(treatmentTechnologySedimentation: TreatmentTechnologySedimentation) {
    if (!dmOfflineIntercept('dmUpdateTreatmentTechnologySedimentation')) {
        return Api.updateTreatmentTechnologySedimentation({
            id: treatmentTechnologySedimentation.id ?? '',
            name: treatmentTechnologySedimentation.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country treatmentTechnologySedimentation.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveTreatmentTechnologySedimentation(id: string) {
    if (!dmOfflineIntercept('dmRemoveTreatmentTechnologySedimentation')) {
        return Api.removeTreatmentTechnologySedimentation(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country officialLanguage.
 *
 * @param country {string} Owner country IRI.
 * @param officialLanguage
 * @param officialLanguage.language {string} OfficialLanguage language property.
 * @returns {Promise}
 */
export function dmAddOfficialLanguage(country: string, officialLanguage: OfficialLanguage) {
    if (!dmOfflineIntercept('dmAddOfficialLanguage')) {
        return Api.addOfficialLanguage({
            country: country,
            language: officialLanguage.language
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country officialLanguage.
 *
 * @param officialLanguage
 * @param officialLanguage.id {string} OfficialLanguage ID.
 * @param officialLanguage.language {string} OfficialLanguage language property.
 * @returns {Promise}
 */
export function dmUpdateOfficialLanguage(officialLanguage: OfficialLanguage) {
    if (!dmOfflineIntercept('dmUpdateOfficialLanguage')) {
        return Api.updateOfficialLanguage({
            id: officialLanguage.id ?? '',
            language: officialLanguage.language
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country officialLanguage.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveOfficialLanguage(id: string) {
    if (!dmOfflineIntercept('dmRemoveOfficialLanguage')) {
        return Api.removeOfficialLanguage(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country AdministrativeDivision.
 *
 * @param country {string} Owner country IRI.
 * @param name {string} AdministrativeDivision name property.
 * @param code {string} AdministrativeDivision code property.
 * @param parent {string} AdministrativeDivision parent IRI property.
 * @returns {Promise}
 */
export function dmAddAdministrativeDivision(country: string, name: string, code: string, parent: string) {
    if (!dmOfflineIntercept('dmAddAdministrativeDivision')) {
        return Api.addAdministrativeDivision({country: country, name: name, code: code, parent: parent});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country AdministrativeDivision.
 *
 * @param id {string} AdministrativeDivision ID.
 * @param name {string} AdministrativeDivision name property.
 * @param code {string} AdministrativeDivision code property.
 * @param parent {string} AdministrativeDivision parent IRI property.
 * @returns {Promise}
 */
export function dmUpdateAdministrativeDivision(id: string, name: string, code: string, parent: string) {
    if (!dmOfflineIntercept('dmUpdateAdministrativeDivision')) {
        return Api.updateAdministrativeDivision({id: id, name: name, code: code, parent: parent});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country AdministrativeDivision.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveAdministrativeDivision(id: string) {
    if (!dmOfflineIntercept('dmRemoveAdministrativeDivision')) {
        return Api.removeAdministrativeDivision(id)
            .then(() => { return true});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country Special Component.
 *
 * @param country {string} Owner country IRI.
 * @param specialComponent
 * @param specialComponent.keyIndex {string} SpecialComponent keyIndex property.
 * @param specialComponent.name {string} SpecialComponent name property.
 * @returns {Promise}
 */
export function dmAddSpecialComponent(country: string, specialComponent: SpecialComponent) {
    if (!dmOfflineIntercept('dmAddSpecialComponent')) {
        return Api.addSpecialComponent({
            country: country,
            keyIndex: specialComponent.keyIndex,
            name: specialComponent.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country SpecialComponent.
 *
 * @param specialComponent
 * @param specialComponent.id {string} TypeTap ID.
 * @param specialComponent.keyIndex {string} TypeTap keyIndex property.
 * @param specialComponent.name {string} TypeTap name property.
 * @returns {Promise}
 */
export function dmUpdateSpecialComponent(specialComponent: SpecialComponent) {
    if (!dmOfflineIntercept('dmUpdateSpecialComponent')) {
        return Api.updateSpecialComponent({
            id: specialComponent.id ?? '',
            keyIndex: specialComponent.keyIndex,
            name: specialComponent.name,
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country SpecialComponent.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveSpecialComponent(id: string) {
    if (!dmOfflineIntercept('dmRemoveSpecialComponent')) {
        return Api.removeSpecialComponent(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country Ethnicity.
 *
 * @param country {string} Owner country IRI.
 * @param name {string} Ethnicity name property.
 * @returns {Promise}
 */
export function dmAddEthnicity(country: string, name: string) {
    if (!dmOfflineIntercept('dmAddEthnicity')) {
        return Api.addEthnicity({country: country, name: name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country Ethnicity.
 *
 * @param id {string} Ethnicity ID.
 * @param name {string} Ethnicity name property.
 * @returns {Promise}
 */
export function dmUpdateEthnicity(id: string, name: string) {
    if (!dmOfflineIntercept('dmUpdateEthnicity')) {
        return Api.updateEthnicity({id: id, name: name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country Ethnicity.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveEthnicity(id: string) {
    if (!dmOfflineIntercept('dmRemoveEthnicity')) {
        return Api.removeEthnicity(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country DisinfectingSubstance.
 *
 * @param country {string} Owner country IRI.
 * @param keyIndex {string} DisinfectingSubstance keyIndex property.
 * @param name {string} DisinfectingSubstance name property.
 * @returns {Promise}
 */
export function dmAddDisinfectingSubstance(country: string, keyIndex: string, name: string) {
    if (!dmOfflineIntercept('dmAddDisinfectingSubstance')) {
        return Api.addDisinfectingSubstance({country: country, keyIndex: keyIndex, name: name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country DisinfectingSubstance.
 *
 * @param id {string} DisinfectingSubstance ID.
 * @param keyIndex {string} DisinfectingSubstance keyIndex property.
 * @param name {string} DisinfectingSubstance name property.
 * @returns {Promise}
 */
export function dmUpdateDisinfectingSubstance(id: string, keyIndex: string, name: string) {
    if (!dmOfflineIntercept('dmUpdateDisinfectingSubstance')) {
        return Api.updateDisinfectingSubstance({id: id, keyIndex: keyIndex, name: name});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country DisinfectingSubstance.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveDisinfectingSubstance(id: string) {
    if (!dmOfflineIntercept('dmRemoveDisinfectingSubstance')) {
        return Api.removeDisinfectingSubstance(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country Distribution material.
 *
 * @param country {string} Owner country IRI.
 * @param keyIndex {string} Distribution Material keyIndex property.
 * @param typeName {string} Distribution Material type property.
 * @returns {Promise}
 */
export function dmAddDistributionMaterial(country: string, keyIndex: string, typeName: string) {
    if (!dmOfflineIntercept('dmAddDistributionMaterial')) {
        return Api.addDistributionMaterial({country: country, keyIndex: keyIndex, type: typeName});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country Distribution material.
 *
 * @param id {string} Distribution Material ID.
 * @param keyIndex {string} Distribution Material keyIndex property.
 * @param typeName {string} Distribution Material type property.
 * @returns {Promise}
 */
export function dmUpdateDistributionMaterial(id: string, keyIndex: string, typeName: string) {
    if (!dmOfflineIntercept('dmUpdateDistributionMaterial')) {
        return Api.updateDistributionMaterial({id: id, keyIndex: keyIndex, type: typeName});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country Distribution material.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveDistributionMaterial(id: string) {
    if (!dmOfflineIntercept('dmRemoveDistributionMaterial')) {
        return Api.removeDistributionMaterial(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country Default Diameter.
 *
 * @param country {string} Owner country IRI.
 * @param value {string} DefaultDiameter value property.
 * @param unit {string} DefaultDiameter unit property.
 * @returns {Promise}
 */
export function dmAddDefaultDiameter(country: string, value: string, unit: string) {
    if (!dmOfflineIntercept('dmAddDefaultDiameter')) {
        return Api.addDefaultDiameter({country: country, value: value, unit: unit});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country Default Diameter.
 *
 * @param id {string} Default Diameter ID.
 * @param value {string} Default Diameter value property.
 * @param unit {string} Default Diameter unit property.
 * @returns {Promise}
 */
export function dmUpdateDefaultDiameter(id: string, value: string, unit: string) {
    if (!dmOfflineIntercept('dmUpdateDefaultDiameter')) {
        return Api.updateDefaultDiameter({id: id, value: value, unit: unit});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country Default Diameter.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveDefaultDiameter(id: string) {
    if (!dmOfflineIntercept('dmRemoveDefaultDiameter')) {
        return Api.removeDefaultDiameter(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country material.
 *
 * @param country {string} Owner country IRI.
 * @param keyIndex {string} Material keyIndex property.
 * @param typeName {string} Material type property.
 * @returns {Promise}
 */
export function dmAddMaterial(country: string, keyIndex: string, typeName: string) {
    if (!dmOfflineIntercept('dmAddMaterial')) {
        return Api.addMaterial({country: country, keyIndex: keyIndex, type: typeName});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country material.
 *
 * @param id {string} Material ID.
 * @param keyIndex {string} Material keyIndex property.
 * @param typeName {string} Material type property.
 * @returns {Promise}
 */
export function dmUpdateMaterial(id: string, keyIndex: string, typeName: string) {
    if (!dmOfflineIntercept('dmUpdateMaterial')) {
        return Api.updateMaterial({id: id, keyIndex: keyIndex, type: typeName});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country material.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveMaterial(id: string) {
    if (!dmOfflineIntercept('dmRemoveMaterial')) {
        return Api.removeMaterial(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country pumpType.
 *
 * @param country {string} Owner country IRI.
 * @param typeName {string} PumpType type property.
 * @returns {Promise}
 */
export function dmAddPumpType(country: string, typeName: string) {
    if (!dmOfflineIntercept('dmAddPumpType')) {
        return Api.addPumpType({country: country, type: typeName});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country pumpType.
 *
 * @param id {string} PumpType ID.
 * @param typeName {string} PumpType type property.
 * @returns {Promise}
 */
export function dmUpdatePumpType(id: string, typeName: string) {
    if (!dmOfflineIntercept('dmUpdatePumpType')) {
        return Api.updatePumpType({id: id, type: typeName});
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country pumpType.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemovePumpType(id: string) {
    if (!dmOfflineIntercept('dmRemovePumpType')) {
        return Api.removePumpType(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country institutionIntervention.
 *
 * @param country {string} Owner country IRI.
 * @param institutionIntervention
 * @param institutionIntervention.name {string} InstitutionIntervention name property.
 * @param institutionIntervention.address {string} InstitutionIntervention address property.
 * @param institutionIntervention.phone {string} InstitutionIntervention phone property.
 * @param institutionIntervention.contact {string} InstitutionIntervention contact property.
 * @returns {Promise}
 */
export function dmAddInstitutionIntervention(country: string, institutionIntervention: InstitutionIntervention) {
    if (!dmOfflineIntercept('dmAddInstitutionIntervention')) {
        return Api.addInstitutionIntervention({
            country: country,
            name: institutionIntervention.name,
            address: institutionIntervention.address ?? '',
            phone: institutionIntervention.phone ?? '',
            contact: institutionIntervention.contact ?? ''
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country institutionIntervention.
 *
 * @param institutionIntervention
 * @param institutionIntervention.id {string} InstitutionIntervention ID.
 * @param institutionIntervention.name {string} InstitutionIntervention name property.
 * @param institutionIntervention.address {string} InstitutionIntervention address property.
 * @param institutionIntervention.phone {string} InstitutionIntervention phone property.
 * @param institutionIntervention.contact {string} InstitutionIntervention contact property.
 * @returns {Promise}
 */
export function dmUpdateInstitutionIntervention(institutionIntervention: InstitutionIntervention) {
    if (!dmOfflineIntercept('dmUpdateInstitutionIntervention')) {
        return Api.updateInstitutionIntervention({
            id: institutionIntervention.id ?? '',
            name: institutionIntervention.name,
            address: institutionIntervention.address ?? '',
            phone: institutionIntervention.phone ?? '',
            contact: institutionIntervention.contact ?? ''
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country institutionIntervention.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveInstitutionIntervention(id: string) {
    if (!dmOfflineIntercept('dmRemoveInstitutionIntervention')) {
        return Api.removeInstitutionIntervention(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country waterQualityEntity.
 *
 * @param country {string} Owner country IRI.
 * @param waterQualityEntity
 * @param waterQualityEntity.name {string} WaterQualityEntity name property.
 * @param waterQualityEntity.address {string} WaterQualityEntity address property.
 * @param waterQualityEntity.phone {string} WaterQualityEntity phone property.
 * @param waterQualityEntity.contact {string} WaterQualityEntity contact property.
 * @returns {Promise}
 */
export function dmAddWaterQualityEntity(country: string, waterQualityEntity: WaterQualityEntity) {
    if (!dmOfflineIntercept('dmAddWaterQualityEntity')) {
        return Api.addWaterQualityEntity({
            country: country,
            name: waterQualityEntity.name,
            address: waterQualityEntity.address ?? '',
            phone: waterQualityEntity.phone ?? '',
            contact: waterQualityEntity.contact ?? ''
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country waterQualityEntity.
 *
 * @param waterQualityEntity
 * @param waterQualityEntity.id {string} WaterQualityEntity ID.
 * @param waterQualityEntity.name {string} WaterQualityEntity name property.
 * @param waterQualityEntity.address {string} WaterQualityEntity address property.
 * @param waterQualityEntity.phone {string} WaterQualityEntity phone property.
 * @param waterQualityEntity.contact {string} WaterQualityEntity contact property.
 * @returns {Promise}
 */
export function dmUpdateWaterQualityEntity(waterQualityEntity: WaterQualityEntity) {
    if (!dmOfflineIntercept('dmUpdateWaterQualityEntity')) {
        return Api.updateWaterQualityEntity({
            id: waterQualityEntity.id ?? '',
            name: waterQualityEntity.name,
            address: waterQualityEntity.address ?? '',
            phone: waterQualityEntity.phone ?? '',
            contact: waterQualityEntity.contact ?? ''
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country waterQualityEntity.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveWaterQualityEntity(id: string) {
    if (!dmOfflineIntercept('dmRemoveWaterQualityEntity')) {
        return Api.removeWaterQualityEntity(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new country funderIntervention.
 *
 * @param country {string} Owner country IRI.
 * @param funderIntervention
 * @param funderIntervention.name {string} FunderIntervention name property.
 * @param funderIntervention.address {string} FunderIntervention address property.
 * @param funderIntervention.phone {string} FunderIntervention phone property.
 * @param funderIntervention.contact {string} FunderIntervention contact property.
 * @returns {Promise}
 */
export function dmAddFunderIntervention(country: string, funderIntervention: FunderIntervention) {
    if (!dmOfflineIntercept('dmAddFunderIntervention')) {
        return Api.addFunderIntervention({
            country: country,
            name: funderIntervention.name,
            address: funderIntervention.address ?? '',
            phone: funderIntervention.phone ?? '',
            contact: funderIntervention.contact ?? ''
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Update a country funderIntervention.
 *
 * @param funderIntervention
 * @param funderIntervention.id {string} FunderIntervention ID.
 * @param funderIntervention.name {string} FunderIntervention name property.
 * @param funderIntervention.address {string} FunderIntervention address property.
 * @param funderIntervention.phone {string} FunderIntervention phone property.
 * @param funderIntervention.contact {string} FunderIntervention contact property.
 * @returns {Promise}
 */
export function dmUpdateFunderIntervention(funderIntervention: FunderIntervention) {
    if (!dmOfflineIntercept('dmUpdateFunderIntervention')) {
        return Api.updateFunderIntervention({
            id: funderIntervention.id ?? '',
            name: funderIntervention.name,
            address: funderIntervention.address ?? '',
            phone: funderIntervention.phone ?? '',
            contact: funderIntervention.contact ?? ''
        });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Remove a country funderIntervention.
 *
 * @param id {string}
 * @returns {Promise}
 */
export function dmRemoveFunderIntervention(id: string) {
    if (!dmOfflineIntercept('dmRemoveFunderIntervention')) {
        return Api.removeFunderIntervention(id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}
