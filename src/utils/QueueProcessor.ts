/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {loadStateStep} from "@/objects/loadStateStep";
import t from "@/utils/i18n";

/**
 * Queue processor.
 *
 * Require loadStateStep jobs.
 * The first job must have the 'start' key.
 *
 * @param updater
 *
 * @constructor
 */
const QueueProcessor = (updater: Function) => {
    let sublabel: string = "";
    // Load state: Must be a key in loadAppDataState.
    let loadState = "wait";
    let loadAppDataState:Array<loadStateStep> = [
        {key: 'wait', label: "",
            /**
             * Queue waiting to start.
             *
             * @param queueContext {any}
             */
            action: (queueContext:any) => {}},
    ];

    /**
     * Update progress bar state value.
     *
     * @returns {number}
     *   Current progress percent.
     */
    const getLoaderPercent = () => {
        let index = loadAppDataStateIndex(loadState);
        return ((100 * (index ?? 0)) / (loadAppDataState.length - 1));
    }

    /**
     * Update progress bar label value.
     *
     * @returns {string}
     *   Current progress label.
     */
    const getLoaderLabel = () => {
        let index = loadAppDataStateIndex(loadState);
        return t(loadAppDataState[(index ?? 0)].label);
    }

    /**
     * Set sub-label content.
     *
     * @param msg
     */
    const setLoaderSublabel = (msg: string) => {
        // Only to debug.
        // if ("" !== msg) {
        //     console.log('setLoaderSublabel', msg);
        // }
        sublabel = msg;
        if (updater) {
            updater();
        }
    }

    /**
     * Get progress bar sub-label value.
     *
     * @returns {string}
     *   Current progress sub-label.
     */
    const getLoaderSublabel = () => {
        return sublabel;
    }

    /**
     * Start queue process.
     */
    const init = () => {
        let index = loadAppDataStateIndex("wait");
        if (index !== 0) {
            return;
        }

        loadState = 'start';
        step();
    }

    /**
     * Get queue context functions.
     *
     * @returns {Object}
     */
    const getContext = () => {
        return {
            setLoaderSublabel,
        };
    }

    /**
     * Add a new job to queue.
     *
     * @param job
     */
    const add = (job: loadStateStep) => {
        loadAppDataState.push(job);
    }

    /**
     * Default task response.
     *
     * @returns {Promise}
     */
    const defaultTaskResponse = (): Promise<any> => {
        return new Promise((resolve) => {
            resolve(true);
        });
    }

    /**
     * Get the load state index by key.
     *
     * @param key {string}
     * @returns {number | undefined}
     */
    const loadAppDataStateIndex = (key: string) => {
        for (let i = 0; i < loadAppDataState.length; ++i) {
            if (loadAppDataState[i].key === key) {
                return i;
            }
        }

        return undefined;
    }

    /**
     * Data load process.
     */
    const step = () => {
        let index = loadAppDataStateIndex(loadState);
        if (index === undefined) {
            // All data loaded
            return;
        }
        setLoaderSublabel("");
        switch (loadState) {
            case "wait":
                return;
            case "stop":
                console.warn("STOP job reached !!");
                return;
            default:
                if (index < loadAppDataState.length) {
                    let job = loadAppDataState[(index ?? 0)];
                    if (job && typeof job.action !== "undefined") {
                        job.action(getContext()).then(() => {
                            let newIndex = (index ?? 0) + 1;
                            let nextJob = loadAppDataState[newIndex];
                            if (nextJob) {
                                loadState = loadAppDataState[newIndex].key;
                                step();
                            }
                        });
                    }
                }
                break;
        }
        if (updater) {
            updater();
        }
    };

    return {
        getLoaderPercent,
        getLoaderLabel,
        init,
        add,
        getLoaderSublabel,
        defaultTaskResponse,
    };
}

export default QueueProcessor;
