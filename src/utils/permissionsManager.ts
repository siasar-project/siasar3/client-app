/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {smGetSession, smSetSession} from "@/utils/sessionManager";

const STORAGE_SYSTEM_PERMISSIONS_KEY = 'system_permissions';
const STORAGE_SYSTEM_ROLES_KEY = 'system_roles';

export interface SystemPermission {
    id: string,
    description: string,
    group: string,
    label: string,
    security_warning: boolean,
}

export interface SystemRole {
    id: string,
    description: string,
    label: string,
    permissions: Array<SystemPermission>,
}

/**
 * Load list from local storage.
 *
 * @returns {Object}
 */
function getPermissions(): Array<SystemPermission> {
    let data = localStorage.getItem(STORAGE_SYSTEM_PERMISSIONS_KEY) ?? '[]';
    return JSON.parse(data);
}

/**
 * Load list from local storage.
 *
 * @returns {Object}
 */
export function getRoles(): Array<SystemRole> {
    let data = localStorage.getItem(STORAGE_SYSTEM_ROLES_KEY) ?? '[]';
    return JSON.parse(data);
}

/**
 * Save list in local storage.
 *
 * @param list {Array<SystemPermission>}
 *   Complete system permissions list.
 */
function setPermissions(list: Array<SystemPermission>) {
    let data = JSON.stringify(list);
    localStorage.setItem(STORAGE_SYSTEM_PERMISSIONS_KEY, data);
}

/**
 * Save list in local storage.
 *
 * @param list {Array<SystemRole>}
 *   Complete system roles list.
 */
function setRoles(list: Array<SystemRole>) {
    let data = JSON.stringify(list);
    localStorage.setItem(STORAGE_SYSTEM_ROLES_KEY, data);
}

/**
 * Exist the permission id?
 *
 * @param id {string}
 *   Permission id.
 * @returns {boolean}
 *   True if the system permission is defined.
 */
function existSystemPermission(id: string): boolean {
    return (getSystemPermission(id) !== null);
}

/**
 * Exist the role id?
 *
 * @param id {string}
 *   Role id.
 * @returns {boolean}
 *   True if the system role is defined.
 */
function existSystemRole(id: string): boolean {
    return (getSystemRole(id) !== null);
}

/**
 * Get permission by id.
 *
 * @param id {string}
 *   Permission id.
 * @returns {SystemPermission | null}
 *   The system permission.
 */
export function getSystemPermission(id: string): SystemPermission | null {
    let list:Array<SystemPermission> = getPermissions();
    for (let i = 0; i < list.length; ++i) {
        let permission = list[i];
        if (permission.id === id) {
            return permission;
        }
    }

    return null;
}

/**
 * Get role by id.
 *
 * @param id {string}
 *   Role id.
 * @returns {SystemRole | null}
 *   The system role.
 */
export function getSystemRole(id: string): SystemRole | null {
    let list:Array<SystemRole> = getRoles();
    for (let i = 0; i < list.length; ++i) {
        let role = list[i];
        if (role.id === id) {
            return role;
        }
    }

    return null;
}

/**
 * Remove a system permission.
 *
 * @param id
 */
function removeSystemPermission(id: string) {
    let list:Array<SystemPermission> = getPermissions();
    let newList:Array<SystemPermission> = [];
    for (let i = 0; i < list.length; ++i) {
        if (list[i].id !== id) {
            newList.push(list[i]);
        }
    }
    setPermissions(newList);
}

/**
 * Remove a system role.
 *
 * @param id
 */
function removeSystemRole(id: string) {
    let list:Array<SystemRole> = getRoles();
    let newList:Array<SystemRole> = [];
    for (let i = 0; i < list.length; ++i) {
        if (list[i].id !== id) {
            newList.push(list[i]);
        }
    }
    setRoles(newList);
}

/**
 * Add a new system permission.
 *
 * @param permission {SystemPermission}
 *   The system permission.
 */
export function addSystemPermission(permission: SystemPermission) {
    if (existSystemPermission(permission.id)) {
        // Remove old permission.
        removeSystemPermission(permission.id);
    }
    // Add the new permission.
    let list:Array<SystemPermission> = getPermissions();
    list.push(permission);
    setPermissions(list);
}

/**
 * Add a new system role.
 *
 * @param role {SystemRole}
 */
export function addSystemRole(role: SystemRole) {
    if (existSystemRole(role.id)) {
        // Remove old Role.
        removeSystemRole(role.id);
    }
    // Add the new role.
    let list:Array<SystemRole> = getRoles();
    list.push(role);
    setRoles(list);
}

/**
 * Password generator by Shani Kehati.
 *
 * @see https://stackoverflow.com/a/64767873/6385708
 */
const Allowed = {
    Uppers: "ABCDEFGHJKLMNPRSTUVWXYZ",
    Lowers: "abcdefghijkmnopqrstuvwxyz",
    Numbers: "23456789",
    Symbols: "!@#$%&",
    // Symbols: "!#%+:=?@",
}

/**
 * Generate random char.
 *
 * @param str
 *
 * @returns {string}
 */
function getRandomCharFromString(str: string): string {
    return str.charAt(Math.floor(Math.random() * str.length));
}

/**
 * Generate a new password string.
 *
 * @param {number} length Default to 10, and have at least one upper, one lower, one number and one symbol.
 *
 * @returns {string}
 */
export function generatePassword(length: number = 10): string {
    if (length < 6) {
        throw new Error('Password must be at least 6 characters long')
    }

    let pwd = "";
    // pwd will have at least one upper
    pwd += getRandomCharFromString(Allowed.Uppers);
    // pwd will have at least one lower
    pwd += getRandomCharFromString(Allowed.Lowers);
    // pwd will have at least one number
    pwd += getRandomCharFromString(Allowed.Numbers);
    // pwd will have at least one symbol
    pwd += getRandomCharFromString(Allowed.Symbols);
    for (let i = pwd.length; i < length; i++) {
        // fill the rest of the pwd with random characters
        pwd += getRandomCharFromString(Object.values(Allowed).join(''));
    }

    // scramble password
    let arr = pwd.split('');
    pwd = "";
    while (arr.length) {
        let indx = Math.floor(Math.random() * arr.length);
        pwd += arr[indx];
        arr.splice(indx, 1);
    }

    return pwd
}



/**
 * Have the user the permission?
 *
 * @param user {User}
 * @param permission {String}
 *
 * @returns {boolean}
 */
export function userHasPermission(user: any, permission: string) {
    for (let role = 0; role < user.roles.length || 0; role++) {
        let roleItem = user.roles[role];
        for (let perm = 0; perm < roleItem.permissions.length || 0; perm++) {
            let permItem = roleItem.permissions[perm];
            if (permItem.id === permission) {
                return true;
            }
        }
    }

    return false;
}

/**
 * Refresh current user permissions by system roles.
 */
export function updateSessionPermissions() {
    let session = smGetSession();
    let systemRoles = getRoles();

    systemRoles.map((role, index) => {
        if (session?.roles?.includes(role.id)) {
            role.permissions.map((permission, index) => {
                session?.permissions.push(permission);
            });
        }
    });

    smSetSession(session);
}
