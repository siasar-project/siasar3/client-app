/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {ulid} from "ulid";
import {db} from "@/utils/db";

export interface OfflineItemInterface {
    // Added or updated ULID.
    id: string
    // Form record type or data type.
    type: string
    // Is this data deleted?
    deleted: boolean
}

/**
 * Offline data pack repository.
 *
 * This class use indexDB to remember data generated or update in offline mode.
 */
export default class offlineDataRepository {
    /**
     * Binary file offline item.
     */
    static OFFLINE_TYPE_FILE: string = 'binary_file';
    /**
     * Internal record key prefix.
     */
    static OFFLINE_DATA_PREFIX: string = 'offline_data_item_';

    /**
     * Annotate the change.
     *
     * @param id
     * @param type
     * @param data
     *
     * @returns {Promise}
     */
    static updateFormRecord(id:string, type: string, data:any):Promise<any> {
        let item: OfflineItemInterface = {id:id, type:type, deleted: false};
        return db.getKey(this.OFFLINE_DATA_PREFIX + id)
            .then((oldItem) => {
                if (!oldItem) {
                    // Is a new form record.
                    return db.addKey(this.OFFLINE_DATA_PREFIX + id, item)
                        .then(() => {
                            return data;
                        });
                }
                return data;
            });
    }

    /**
     * Annotate the remove.
     *
     * @param id
     * @param type
     *
     * @returns {Promise}
     */
    static removeFormRecord(id:string, type: string):Promise<any> {
        let item: OfflineItemInterface = {id:id, type:type, deleted: true};
        // Is a new form record.
        return db.getKey(this.OFFLINE_DATA_PREFIX + id)
            .then((oldItem) => {
                if (!oldItem) {
                    // No exist previous mark.
                    return db.addKey(this.OFFLINE_DATA_PREFIX + id, item);
                }
                // Update previous mark.
                return db.updateKey(this.OFFLINE_DATA_PREFIX + id, item);
            });
    }

    /**
     * Is this form record removed?
     *
     * @param id
     *
     * @returns {Promise}
     */
    static isRemovedFormRecord(id:string):Promise<any> {
        return db.getKey(this.OFFLINE_DATA_PREFIX + id)
            .then((oldItem) => {
                if (!oldItem) {
                    return false;
                }

                return oldItem.deleted;
            });
    }

    /**
     * Get all offline marks.
     *
     * @returns {Promise}
     */
    static getAllItems(): Promise<any> {
        return db.getKeyThatStartBy(this.OFFLINE_DATA_PREFIX);
    }

    /**
     * Generated a new ULID.
     *
     * @returns {string}
     */
    static getNewUlid(): string {
        return ulid();
    }
}
