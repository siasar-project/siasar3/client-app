/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Local configuration manager over local storage.
 * 
 * @see https://www.sohamkamani.com/javascript/localstorage-with-ttl-expiry/
 */

const LOCAL_CONFIG_MANAGER_PREFIX = 'localCM_';

/**
 * Add item to local configuration.
 * 
 * @param key {string}
 * @param value {any}
 */
export function lcmSetItem(key: string, value: any) {
    const item = {
        value: value,
    }
    localStorage.setItem(LOCAL_CONFIG_MANAGER_PREFIX + key, JSON.stringify(item));
}

/**
 * Get item from the local configuration if the key exists.
 * 
 * @param key {string}
 * @param def {any}
 *
 * @returns {any}
 */
export function lcmGetItem(key: string, def: any = {})  {
    if (typeof localStorage === 'undefined') {
        return def;
    }

    const itemStr = localStorage.getItem(LOCAL_CONFIG_MANAGER_PREFIX + key);
    // if the item doesn't exist, return default.
    if (!itemStr) {
        return def;
    }
    const item = JSON.parse(itemStr);
    return item.value
}

/**
 * Remove item if the key exists.
 * 
 * @param key {string}
 */
export function lcmRemoveItem(key: string) {
    localStorage.removeItem(LOCAL_CONFIG_MANAGER_PREFIX + key);
}

/**
 * Remove all local configuration items.
 */
export function lcmClear() {
    // Empty cache.
    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i);
        // Don't remove strings untranslated.
        if (key && key.startsWith(LOCAL_CONFIG_MANAGER_PREFIX)) {
            localStorage.removeItem(key);
            i--;
        }
    }
}
