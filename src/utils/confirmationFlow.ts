/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Confirmation dialog workflow.
 *
 * @see https://daveceddia.com/react-confirmation-modal-state-machine/
 */

import { createMachine, state, transition, invoke } from 'robot3';

/**
 * Nothing to do.
 */
const action = async () => {
    // call an API to delete something
}

const confirmationFlow = createMachine({
    initial: state(
        transition('begin', 'confirming')
    ),
    confirming: state(
        transition('confirm', 'loading'),
        transition('cancel', 'initial'),
    ),
    loading: invoke(action,
        transition('done', 'initial'),
        transition('error', 'confirming')
    )
});

export { confirmationFlow };
