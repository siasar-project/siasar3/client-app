/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {db} from "@/utils/db";
import Api from "@/utils/api";
import api, {API_PAGE_SIZE} from "@/utils/api";
import {getRoles, SystemRole} from "@/utils/permissionsManager";
import {smGetSession, smIsValidSession, smSetSession} from "@/utils/sessionManager";
import {cmGetItem, cmRemoveItem, cmSetItem} from "@/utils/cacheManager";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {InquiryStatusEnum} from "@/objects/InquiryStatusEnum";
import {AdministrativeDivision} from "@/objects/AdministrativeDivision";
import {getFormTypes} from "@/utils/formTypes";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import {Country} from "@/objects/Country";
import {User} from "@/objects/User";
import {InquiryFormLog} from "@/objects/InquiryFormLog";
import {dmGetAdministrativeDivision} from "@/utils/dataManager/ParametricRead";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {dmGetCurrentHost} from "@/utils/dataManager/Api";
import {blobToBase64} from "@/utils/siasar";
import {dmClearFormRecord} from "@/utils/dataManager/FormRecordWrite";
import offlineDataRepository from "@/utils/offlineDataRepository";
import t from "@/utils/i18n";
import {FileOfflineWrapper} from "@/objects/FileOfflineWrapper";

/**
 * Cache key constants.
 */
const CACHE_USER_LIST:string = "users";
const CACHE_USER_SAFE_LIST:string = "safe_users";
const CACHE_COUNTRIES_LIST:string = "countries";
const CACHE_MUTATEBLES:string = "mutatebles";
export const CACHE_APP_VERSION:string = "app_version";
const CACHE_PREFIX_FORM_STRUCTURE:string = "form_structure_";
const CACHE_PREFIX_FORM_EMPTY_RECORD:string = "form_empty_record_";

/**
 * I18n add literal.
 */
eventManager.on(Events.I18N_ADD_LITERAL, (data) => {
    // Send to API.
    if (!dmIsOfflineMode() && smIsValidSession()) {
        Api.addI18nLiteral(data.literal);
    }
});

/**
 * If we are in offline mode output error in console, and return true.
 *
 * Used to detect calls to API in offline mode. Ex.
 * <code>
 *     if (!dmOfflineIntercept('dmExample')) {
 *         // Call to API.
 *     } else {
 *         // Return an empty data collection.
 *     }
 * </code>
 *
 * @param methodName
 *
 * @returns {boolean}
 */
export function dmOfflineIntercept(methodName:string):boolean {
    if (dmIsOfflineMode()) {
        console.warn('Call to API in offline mode in: ', methodName);
        return true;
    }

    return false;
}

/**
 * Dummy response used how default response in offline mode.
 */
export const dummyAxioResponse = {
    data: {},
    status: 200,
    statusText: 'OK',
    headers: {},
    config: {},
    request: null,
};

/**
 * Get system units.
 *
 * @param key {string}
 *   A system unit key.
 *
 * @returns {PromiseExtended}
 *   System units.
 *
 * @see <API Server Address>/api/v1/docs#operations-tag-Configuration
 */
export function dmGetUnits(key: string) {
    return db.documents
        .where("keys")
        .equals(key)
        .toArray()
        .then((data) => {
            if (data.length === 0) {
                if (!dmOfflineIntercept('dmGetUnits')) {
                    return Api.getUnits(key)
                        .then(data => {
                            // Save cache data.
                            // db.documents.add({
                            //     ulid: '',
                            //     type: 'system.units',
                            //     keys: key,
                            //     data: data[0],
                            // });
                            db.addSystemUnits(data[0], key);

                            return getCountryUnits(data[0], key);
                        })
                        .catch((info) => {
                            // Dispatch status event.
                            eventManager.dispatch(
                                Events.STATUS_ADD,
                                {
                                    level: StatusEventLevel.ERROR,
                                    title: "API getUnits error: "+key,
                                    message: info.response.data.detail,
                                    isPublic: true,
                                }
                            );
                        });
                } else {
                    return [];
                }
            }

            return getCountryUnits(data[0].data, key);
        });
}

/**
 * @param units
 * @param key
 *
 * @returns {Promise}
 */
function getCountryUnits(units: any[], key: string): { [key: string]: any; } {
    let session = smGetSession();
    let countryUnit = null;
    let mainUnit = null;
    let unitList: any = {};
    unitList["all"] = units;

    if (session?.country) {
        let tmp: any = [];
        switch (key) {
            case 'system.units.flow':
                countryUnit = session.country?.unitFlows;
                tmp = [];
                for (let i = 0; i < countryUnit.length; i++) {
                    tmp[countryUnit[i]] = true;
                }
                countryUnit = tmp;
                mainUnit = session.country?.mainUnitFlow;
                break;
            case 'system.units.length':
                countryUnit = session.country?.unitLengths;
                tmp = [];
                for (let i = 0; i < countryUnit.length; i++) {
                    tmp[countryUnit[i]] = true;
                }
                countryUnit = tmp;
                mainUnit = session.country?.mainUnitLength;
                break
            case 'system.units.volume':
                countryUnit = session.country?.unitVolumes;
                tmp = [];
                for (let i = 0; i < countryUnit.length; i++) {
                    tmp[countryUnit[i]] = true;
                }
                countryUnit = tmp;
                mainUnit = session.country?.mainUnitVolume;
                break
            case 'system.units.concentration':
                countryUnit = session.country?.unitConcentrations;
                tmp = [];
                for (let i = 0; i < countryUnit.length; i++) {
                    tmp[countryUnit[i]] = true;
                }
                countryUnit = tmp;
                mainUnit = session.country?.mainUnitConcentration;
                break
            default:
        }

        // Add unit id as unit information.
        for (var key1 in units) {
            units[<any>key1].id = key1;
        }

        // Main unit.
        unitList["main"] = units[<any>mainUnit];

        // Country units list.
        unitList["country"] = [];
        for (const countryUnitKey in countryUnit) {
            unitList["country"][countryUnitKey] = units[<any>countryUnitKey];
        }
        unitList["country"] = Object.keys(countryUnit!).map((key: string) => units[<any>key]);
    }

    return new Promise((resolve) => resolve(unitList));
}

/**
 * Get user languages, used to translate strings.
 *
 * @returns {Promise}
 */
export function dmGetUserLanguages() {
    return db.documents
        .where("keys")
        .equals('system.languages')
        .toArray()
        .then((data) => {
            if (data.length === 0) {
                if (!dmOfflineIntercept('dmGetUserLanguages')) {
                    return Api.getUserLanguages()
                        .then(data => {
                            db.documents.add({
                                ulid: '',
                                type: 'system.languages',
                                keys: 'system.languages',
                                data: data,
                            });
                            return data;
                        })
                        .catch((info) => {
                            // Dispatch status event.
                            eventManager.dispatch(
                                Events.STATUS_ADD,
                                {
                                    level: StatusEventLevel.ERROR,
                                    title: "API getUserLanguages error",
                                    message: info.response.data.detail,
                                    isPublic: true,
                                }
                            );
                        });
                } else {
                    return [];
                }
            }
            return data[0].data;
        });
}

/**
 * Get page size.
 *
 * @returns {number}
 */
export function dmGetPageSize() {
    return API_PAGE_SIZE;
}

/**
 * Get user by id.
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmGetUser(id: string) {
    // Try to load from cache.
    return cmGetItem(CACHE_USER_LIST)
        .then((users: any) => {
            if (users) {
                for (let i = 0; i < users.length; i++) {
                    let user = users[i];
                    if (user.id === id) {
                        return new Promise((resolve) => resolve(user));
                    }
                }
            }
            // Else, try API, if we can do it.
            if (!dmOfflineIntercept('dmGetUser')) {
                return api.getUser(id.toString());
            } else {
                return new Promise((resolve) => resolve({}));
            }
        });
}

/**
 * update user by id.
 *
 * @param id {string}
 * @param user {User}
 *
 * @returns {Promise}
 */
 export function dmUpdateUser(id: string, user: User) {
    if (!dmOfflineIntercept('dmUpdateUser')) {
        return api.updateUser(id.toString(), user);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * create a new user
 *
 * @param user {User}
 *
 * @returns {Promise}
 */
 export function dmAddUser(user: User) {
    if (!dmOfflineIntercept('dmAddUser')) {
        return api.addUser(user);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Get system roles.
 *
 * @returns {Promise}
 */
export function dmGetRoles(): Promise<Array<SystemRole>> {
    return new Promise((resolve) => resolve(getRoles()));
}

/**
 * Get user list.
 *
 * @param username {string}
 * @param email {string}
 * @param page {number}
 *
 * @returns {Promise}
 */
export function dmGetUsers(username: string = '', email: string = '', page: number = 1) {
    return cmGetItem(CACHE_USER_LIST)
        .then((users) => {
            if (users) {
                return localCountryParametricQuerier(page, {
                    list: users,
                    /**
                     * Compare two Users by type string.
                     *
                     * @param first {any}
                     * @param second {any}
                     * @returns {number}
                     */
                    itemComparer: (first:any, second:any) => first.username.toLocaleLowerCase() > second.username.toLocaleLowerCase() ? -1 : 1,
                    filter: [{value: username, propertyName: "username"}, {value: email, propertyName: "email"}],
                });
            } else {
                if (!dmOfflineIntercept('dmGetUsers')) {
                    // Get from API all users.
                    return api.getUsers().then((data: any) => {
                        // The users must be into the cache.
                        cmSetItem(CACHE_USER_LIST, data);
                        // Avoid recursive call (dmGetUsers(...)) because cache can be disabled, so cmGetItem() to return null always.
                        data = localCountryParametricQuerier(page, {
                            list: data,
                            /**
                             * Compare two Users by type string.
                             *
                             * @param first {any}
                             * @param second {any}
                             * @returns {number}
                             */
                            itemComparer: (first:any, second:any) => first.username.toLocaleLowerCase() > second.username.toLocaleLowerCase() ? -1 : 1,
                            filter: [{value: username, propertyName: "username"}, {value: email, propertyName: "email"}],
                        });
                        return data;
                    });
                } else {
                    return new Promise((resolve) => resolve([]));
                }
            }
        });
}

/**
 * Get safe user list.
 *
 * @param username {string}
 * @param email {string}
 * @param page {number} Deprecated parameter.
 *
 * @returns {Promise}
 */
export function dmGetSafeUsers(username: string = '', email: string = '', page: number = 1) {
    return cmGetItem(CACHE_USER_SAFE_LIST)
        .then((users) => {
            if (users) {
                return localCountryParametricQuerier(-1, {
                    list: users,
                    /**
                     * Compare two Users by type string.
                     *
                     * @param first {any}
                     * @param second {any}
                     * @returns {number}
                     */
                    itemComparer: (first:any, second:any) => first.username.toLocaleLowerCase() > second.username.toLocaleLowerCase() ? -1 : 1,
                    filter: [{value: username, propertyName: "username"}],
                });
            } else {
                if (!dmOfflineIntercept('dmGetSafeUsers')) {
                    // Get from API all users.
                    return api.getUserList().then((data: any) => {
                        // The users must be into the cache.
                        cmSetItem(CACHE_USER_SAFE_LIST, data);
                        // Avoid recursive call (dmGetUsers(...)) because cache can be disabled, so cmGetItem() to return null always.
                        data = localCountryParametricQuerier(-1, {
                            list: data,
                            /**
                             * Compare two Users by type string.
                             *
                             * @param first {any}
                             * @param second {any}
                             * @returns {number}
                             */
                            itemComparer: (first:any, second:any) => first.username.toLocaleLowerCase() > second.username.toLocaleLowerCase() ? -1 : 1,
                            filter: [{value: username, propertyName: "username"}],
                        });
                        return data;
                    });
                } else {
                    return new Promise((resolve) => resolve([]));
                }
            }
        });
}

/**
 * Query data from a country parametric stored in localStorage.
 *
 * IMPORTANT: Internal use only.
 *
 * @param page Required page to get.
 * @param context Algorithm to query localStorage.
 * @param context.list Item list.
 * @param context.itemComparer Compare two items and return (first > second ? -1 : 1).
 * @param context.filter Filter list with values and property names to compare with.
 * @returns {Promise}
 */
export function localCountryParametricQuerier(page: number = -1, context: {
    list: Array<any>,
    itemComparer: Function,
    filter: Array<{ value: string, propertyName: string }>
}) {
    return new Promise((resolve) => {
        let result: Array<any> = [];
        let offset: number = (page - 1) * dmGetPageSize();
        if (context.list) {
            let counter: number = 0;
            let index: number = 0;
            let sortedItems = context.list.sort((first, second) => 0 - context.itemComparer(first, second));
            for (let i = 0; i < sortedItems.length; i++) {
                let item: any = context.list[i];
                let match: boolean = true;
                for (let j = 0; j < context.filter.length; j++) {
                    if (match && context.filter[j].value !== '') {
                        // Implicit number to boolean casting.
                        match = String(item[context.filter[j].propertyName]).toLowerCase().includes(String(context.filter[j].value).toLowerCase());
                    }
                }
                if (match) {
                    if (page !== -1 && index < offset) {
                        index++;
                    } else {
                        result.push(item);
                        counter++;
                        if (page !== -1 && counter >= dmGetPageSize()) {
                            break;
                        }
                    }
                }
            }
        }
        return resolve(result);
    });
}

/**
 * Get country list.
 *
 * @returns {Promise}
 */
export function dmGetCountries() {
    return cmGetItem(CACHE_COUNTRIES_LIST)
        .then((countries) => {
            if (countries) {
                return Promise.resolve(countries);
            } else {
                if (!dmOfflineIntercept('dmGetCountries')) {
                    // Get from API all countries.
                    return api.getCountries().then((data: any) => {
                        // The countries must be into the cache.
                        cmSetItem(CACHE_COUNTRIES_LIST, data);
                        return data;
                    });
                } else {
                    return new Promise((resolve) => resolve([]));
                }
            }
        });
}

/**
 * Get country.
 *
 * Get country from session, or null if we don't have a session.
 *
 * @returns {Promise}
 */
export function dmGetCountry() {
    let session = smGetSession();
    if (session && session?.country) {
        return new Promise((resolve) => resolve(session?.country));
    }

    return new Promise((resolve) => resolve(null));
}

/**
 * Update country.
 *
 * @param code
 * @param data
 * @returns {Promise}
 */
export function dmUpdateCountry(code: string, data: any) {
    let session = smGetSession();
    if (session) {
        if (!dmOfflineIntercept('dmUpdateCountry')) {
            return Api.updateCountry(code, data).then(data => {
                return dmInvalidateCountry().then(() => {
                    return Api.getCountry(code).then((data: any) => {
                        // The user country must be into the user session data.
                        if (session?.country) {
                            session.country = data;
                        }
                        smSetSession(session);

                        return new Promise((resolve) => resolve(session?.country));
                    });
                });
            });
        } else {
            return new Promise((resolve) => resolve(session?.country));
        }
    }

    return new Promise(resolve => resolve(null))
}

/**
 * Invalidate Country property.
 *
 * @returns {Promise<boolean>}
 */
 function dmInvalidateCountry() {
    return new Promise<boolean>((resolve) => {
        let session = smGetSession();
        if (session) {
            let country: Country | null = session.country ? session.country : null;
            if (country) {
                country = null;
                smSetSession(session);
                return resolve(false);
            }
        }
        return resolve(true);
    });
}

/**
 * Remove file by id.
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmRemoveFile(id: string): Promise<any> {
    if (!dmIsOfflineMode()) {
        return api.removeFile(id);
    } else {
        cmRemoveItem("image_" + encodeURIComponent(dmGetFileUrl(id, false)));
        return offlineDataRepository.removeFormRecord(id, offlineDataRepository.OFFLINE_TYPE_FILE);
    }
}

/**
 * Upload file
 *
 * @param country
 * @param file
 * @returns {Promise}
 */
export function dmUploadFile(country: string, file: File): Promise<any> {
    if (!dmIsOfflineMode()) {
        return api.uploadFile(country, file);
    } else {
        // Save a local copy to upload when we can't get API access.
        const reader = new FileReader();

        return new Promise((resolve, reject) => {
            /**
             * On reader load file.
             *
             * @returns {any}
             */
            reader.onload = () => {
                const base64Image = reader.result as string;
                const newUlid = offlineDataRepository.getNewUlid();
                const imageWrapper: FileOfflineWrapper = {
                        id: newUlid,
                        fileName: file.name,
                        content: base64Image,
                    };
                cmSetItem("image_" + encodeURIComponent(dmGetFileUrl(imageWrapper.id, false)), imageWrapper);
                return offlineDataRepository.updateFormRecord(imageWrapper.id, offlineDataRepository.OFFLINE_TYPE_FILE, imageWrapper)
                    .then(() => {
                        resolve(imageWrapper);
                    });
            };

            /**
             * On error reading file.
             */
            reader.onerror = () => {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.ERROR,
                        title: t("File loading"),
                        message: t("File load error"),
                        isPublic: true,
                    }
                );
            };

            reader.readAsDataURL(file);
        });
    }
}

/**
 * Get file url by id.
 *
 * @param id {string}
 * @param withBaseUrl {boolean}
 *
 * @returns {string}
 */
export function dmGetFileUrl(id: string, withBaseUrl: boolean = true): string {
    // This method don't call to the remote API.
    return api.getFileUrl(id, withBaseUrl);
}

/**
 * Get file name.
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmGetFileName(id: string): Promise<any> {
    return cmGetItem("image_" + encodeURIComponent(dmGetFileUrl(id, false)))
        .then((imageWrapper: FileOfflineWrapper | null) => {
            return imageWrapper?.fileName;
        });
}

/**
 * Get file content.
 *
 * @param id {string}
 *
 * @returns {Promise}
 */
export function dmGetFileContent(id: string): Promise<any> {
    return dmLoadImage(dmGetFileUrl(id, false));
}

/**
 * Get file content.
 *
 * @param id {string}
 * @param contentType {string}
 * @param sliceSize {number}
 *
 * @returns {Promise}
 *
 * @author ChatGPT, by OpenAI, with human help.
 */
export function dmGetFileBlobContent(id: string, contentType: string = '', sliceSize: number = 512): Promise<any> {
    return dmGetFileContent(id)
        .then((b64Data) => {
            const encodedString = b64Data.replace(/^data:(.*;)?base64,/, '');
            return window.atob(encodedString);
        });
}

/**
 * Load image from remote or local storage.
 *
 * @param src {string}
 * @param quality {number}
 *
 * @returns {Promise}
 */
export function dmLoadImage(src: string, quality: number = 75): Promise<any> {
    return cmGetItem("image_" + encodeURIComponent(src))
        .then((imageWrapper: FileOfflineWrapper | null) => {
            let b64: string | null = null;
            if (imageWrapper) {
                b64 = imageWrapper.content;
            }
            if (typeof b64 === "string") {
                return new Promise((resolve) => resolve(b64));
            }
            if (!dmIsOfflineMode()) {
                let apiSrc = src;
                // Is this image a remote one or a local resource?
                if (src.indexOf('api/v1') !== -1) {
                    // The image is a remove one.
                    if (!quality) {
                        quality = 75;
                    }
                    if (src && src !== '') {
                        let queryParams: string[] = [];
                        // Allow API image debug.
                        let xdebug = '';
                        if (process.env.NEXT_PUBLIC_SIASAR3_API_DEBUG === '1') {
                            xdebug = 'XDEBUG_SESSION=siasar';
                        }
                        if (xdebug !== '') {
                            queryParams.push(xdebug);
                        }
                        queryParams.push('width=' + 640);
                        queryParams.push('quality=' + quality);
                        apiSrc = dmGetCurrentHost() + src;
                        if (queryParams.length > 0) {
                            apiSrc = apiSrc + "?" + queryParams.join('&');
                        }
                    }
                }

                return fetch(apiSrc)
                    .then((response: Response) => {
                        return response.blob()
                            .then((blob: Blob) => {
                                return blobToBase64(blob)
                                    .then((b64) => {
                                        // Cache images in online mode.
                                        let imageWrapper: FileOfflineWrapper = {
                                            id: '',
                                            fileName: '',
                                            content: b64,
                                        };
                                        cmSetItem("image_" + encodeURIComponent(src), imageWrapper);
                                        return b64;
                                    });
                            });
                    });
            } else {
                return new Promise((resolve) => resolve(null));
            }
        });
}

/**
 * Get parametric template url.
 *
 * @param entityName {string}
 * @param withBaseUrl {boolean}
 *
 * @returns {string}
 */
export function dmGetParametricTemplateUrl(entityName: string, withBaseUrl: boolean = true): string {
    if (!dmOfflineIntercept('dmGetParametricTemplateUrl')) {
        return api.getParametricTemplateUrl(entityName, withBaseUrl);
    } else {
        return '';
    }
}

/**
 * Massive parametric import.
 *
 * @param entityName
 * @param file
 *
 * @returns {Promise}
 */
export function dmUploadImportParametric(entityName: string, file: File): Promise<any> {
    if (!dmOfflineIntercept('dmUploadImportParametric')) {
        return api.uploadImportParametric(entityName, file);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Get inquiry list.
 *
 * @param params
 * @param params.page {number} Default 1.
 * @param params.formType {string}
 * @param params.status {string}
 * @param params.division {string}
 * @param params.user_id {string}
 * @param params.changed {string}
 * @param orders
 * @param orders.changed {string}
 * @returns {Promise}
 */
export function dmGetInquires(
    params: {
        page?: number,
        formType?: string | string[],
        status?: string[],
        division?: string,
        user_id?: string,
        changed?: string
    } =
        {page: -1, formType: "", status: [], division: "", user_id: "", changed: ""},
    orders: {changed: string} = {changed: ''}
) {
    // let parameters: { record_type?: string | string[], field_status?: string[], field_region?: string, field_creator?: string, order?: {field_changed?: string} } = {};
    let parameters: { [key: string]: any } = {};
    if (params.formType !== "") {
        parameters.record_type = params.formType;
    }
    if (params.status && params.status.length > 0) {
        parameters.field_status = params.status;
    }
    if (params.division && params.division !== "") {
        parameters.field_region = params.division;
    }
    if (params.user_id !== "") {
        parameters.field_creator = params.user_id;
    }
    if (params.changed && params.changed !== "") {
        parameters.field_changed = params.changed;
    }

    if (orders.changed && orders.changed !== "") {
        if (!parameters.order) {
            parameters.order = {};
        }
        parameters["order[field_changed]"] = orders.changed;
    }

    if (!dmIsOfflineMode()) {
        return Api.getFormInquires(params.page, parameters);
    } else {
        // Use local storage to get list.
        return db.getFormIndexRecords(params);
    }
}

/**
 * Get points list.
 *
 * @param page {number} Default 1.
 * @param status {string} To query by multiple values separate it with commas, like st1,st2,st3
 * @param division {string}
 * @param changed {string}
 * @param orders
 * @param orders.changed {string}
 * @returns {Promise}
 */
export function dmGetPoints(
    page: number = -1,
    status: string = InquiryStatusEnum.Undefined,
    division: string = "",
    changed: string = "",
    orders: {changed: string} = {changed: ''}
) {
    let parameters: { [key: string]: any } = {};
    if (status !== InquiryStatusEnum.Undefined) {
        parameters.status = status.toString();
    }
    return dmGetCountry().then((country: any) => {
        if (!country) {
            return [];
        }

        if ("" !== changed) {
            parameters.field_changed = changed;
        }
        if (orders.changed) {
            if (!parameters.order) {
                parameters.order = {};
            }
            parameters["order[field_changed]"] = orders.changed;
        }

        if (division !== "") {
            return dmGetAdministrativeDivision(division).then((data:AdministrativeDivision) => {
                if (data.level == country.divisionTypes.length) {
                    parameters.region = '/api/v1/administrative_divisions/'+division;
                } else {
                    parameters.parent = '/api/v1/administrative_divisions/'+division;
                }

                if (!dmIsOfflineMode()) {
                    return Api.getFormPoints('/api/v1/countries/'+country.code, page, parameters);
                } else {
                    return db.getFormPoints('/api/v1/countries/'+country.code, page, parameters);
                }
            });
        } else {
            if (!dmIsOfflineMode()) {
                return Api.getFormPoints('/api/v1/countries/'+country.code, page, parameters);
            } else {
                return db.getFormPoints('/api/v1/countries/'+country.code, page, parameters);
            }
        }
    })
}

/**
 * Get point history.
 *
 * @param pid {string} Point ID.
 * @param page {number} Default 1.
 * @param orders
 * @param orders.changed {string}
 * @returns {Promise}
 */
export function dmGetPointHistory(
    pid: string,
    page: number = 1,
    orders: {changed: string} = {changed: ''}
) {
    let parameters: { [key: string]: any } = {};
    if (orders.changed) {
        if (!parameters.order) {
            parameters.order = {};
        }
        parameters["order[field_changed]"] = orders.changed;
    }

    if (!dmOfflineIntercept('dmGetPointHistory')) {
        return Api.getFormPointHistory(pid, page, parameters);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Get a SIASAR Point.
 *
 * @param id {string} Point ID.
 * @returns {Promise}
 */
export function dmGetPoint(id:string) {
    if (!dmIsOfflineMode()) {
        return api.getFormPoint(id);
    } else {
        return db.getFormPoint(id);
    }
}

/**
 * Get a SIASAR Point inquiries.
 *
 * @param id {string} Point ID.
 * @returns {Promise}
 */
export function dmGetPointInquiries(id:string) {
    if (!dmIsOfflineMode()) {
        return api.getFormPointInquiries(id);
    } else {
        return db.getFormPointInquiries(id);
    }
}

/**
 * Get form types list.
 *
 * @returns {Promise}
 */
export function dmGetFormTypes() {
    return new Promise((resolve) => resolve(getFormTypes()));
}

/**
 * Get form structure.
 *
 * @param id
 * @returns {Promise}
 */
export function dmGetFormStructure(id: string = "") {
    return cmGetItem(CACHE_PREFIX_FORM_STRUCTURE+id)
        .then((structure) => {
            if (structure) {
                return Promise.resolve(structure);
            } else {
                if (!dmOfflineIntercept(`dmGetFormStructure "${id}"`)) {
                    // Get from API the structure.
                    return api.getStructure(id).then((data: any) => {
                        // The structure must be into the cache.
                        cmSetItem(CACHE_PREFIX_FORM_STRUCTURE+id, data);
                        return data;
                    });
                } else {
                    return new Promise((resolve) => resolve(dummyAxioResponse.data));
                }
            }
        });
}

/**
 * Get form empty record.
 *
 * @param formId
 * @returns {Promise}
 */
export function dmGetEmptyFormRecord(formId: string = "") {
    return cmGetItem(CACHE_PREFIX_FORM_EMPTY_RECORD + formId)
        .then((emptyRecord) => {
            if (emptyRecord) {
                return Promise.resolve(emptyRecord);
            } else {
                if (!dmOfflineIntercept(`dmGetEmptyFormRecord(${formId})`)) {
                    // Get from API the structure.
                    return dmGetFormStructure(formId)
                        .then((structure: DynFormStructureInterface) => {
                            return api.getEmptyFormRecord(formId).then((data: any) => {
                                // Update boolean fields to '0'/'1' values.
                                data = dmClearFormRecord(structure, data);
                                // The structure must be into the cache.
                                cmSetItem(CACHE_PREFIX_FORM_EMPTY_RECORD + formId, data);
                                return data;
                            });
                        });
                } else {
                    return new Promise((resolve) => resolve(dummyAxioResponse.data));
                }
            }
        });
}

/**
 * Search DynForm record.
 *
 * @param record
 * @param record.formId {string} Id of the form.
 * @param record.id {string} Id of the record.
 * @param record.filter.field_provider_name {string} Id of the provider.
 * @param record.filter.field_provider_code {string} Code of the provider.
 * @param record.order {Object} Order settings.
 * @param unique {boolean} Return only one record by ULID reference field.
 * @param page {number} Page to load.
 * @param autoPagination {boolean} True, default, to get all response pages.
 *
 * @returns {Promise}
 */
export function dmSearchFormRecords(record: DynFormRecordInterface, unique: boolean = false, page:number = 1, autoPagination: boolean = true) {
    let filter:any = {};
    for (const filterKey in record.filter) {
        filter[filterKey] = record.filter[filterKey];
    }
    if (record.filter?.field_provider_code) {
        filter.field_provider_code = record.filter.field_provider_code;
    }
    if (record.filter?.field_provider_name) {
        filter.field_provider_name = record.filter.field_provider_name;
    }
    return dmGetFormStructure(record.formId)
        .then((formStr:DynFormStructureInterface) => {
            let likeFieldFilters = ['short_text', 'long_text'];
            let gtFieldFilters = ['date'];
            for (const filterKey in record.filter) {
                if (likeFieldFilters.includes(formStr.fields[filterKey].type)) {
                    filter[filterKey+'[like]'] = record.filter[filterKey];
                    filter[filterKey] = undefined;
                } else if (gtFieldFilters.includes(formStr.fields[filterKey].type)) {
                    filter[filterKey+'[>=]'] = record.filter[filterKey];
                    filter[filterKey] = undefined;
                }
            }
            let order:any = {};
            for (const filterKey in record.order) {
                order[filterKey] = record.order[filterKey];
            }

            if (!dmIsOfflineMode()) {
                return Api.searchFormRecords(
                    {
                        form_id: record.formId,
                        unique: unique,
                        filter: filter,
                        order: order,
                        page: page,
                        autopagination: autoPagination,
                    }
                ).then((res:any) => {
                    if (res.length > 0) {
                        return dmGetFormStructure(record.formId).then((structure: DynFormStructureInterface) => {
                            for (let i = 0; i < res.length; i++) {
                                res[i] = dmClearFormRecord(structure, res[i]);
                            }

                            return {data:res, filter: record};
                        });
                    }

                    return {data:[], filter: record};
                });
            } else {
                return db.searchFormRecords(
                    {
                        form_id: record.formId,
                        unique: unique,
                        filter: filter,
                        order: order,
                        page: page,
                        autopagination: autoPagination,
                        record: record,
                    });
            }
        });
}

/**
 * Get DynForm record.
 *
 * @param record
 * @param record.formId
 * @param record.id
 * @param isPublic
 *
 * @returns {Promise}
 */
export function dmGetFormRecord(record: DynFormRecordInterface, isPublic: boolean = true) {
    if (!dmIsOfflineMode()) {
        return Api.getFormRecord(record.formId, record.id).then((res:any) => {
            return dmGetFormStructure(record.formId).then((structure: DynFormStructureInterface) => {
                if (res.length > 0) {
                    let data = res[0];
                    return dmClearFormRecord(structure, data);
                }
            });
        })
            .catch((info:any) => {
                return Api.displayErrorMessage(info, isPublic);
            });
    } else {
        return db.getFormRecord(record.id || "")
            .then((res:any) => {
                return dmGetFormStructure(record.formId).then((structure: DynFormStructureInterface) => {
                        // If the record is marked how deleted, don't return it.
                        return offlineDataRepository.isRemovedFormRecord(record.id || "")
                            .then((removed) => {
                                if (!removed) {
                                    return dmClearFormRecord(structure, res);
                                }
                            });
                    });
            });
    }
}

/**
 * Get form record history.
 *
 * @param form_id
 * @param record_id
 *
 * @returns {Promise}
 */
export function dmGetHistoryFormRecord(form_id: string, record_id: string) {
    if (!dmOfflineIntercept('dmGetHistoryFormRecord')) {
        return api.getHistoryFormRecord(form_id, record_id);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Get unread mail count.
 *
 * @returns {Promise}
 */
export function dmUnreadMailCount() {
    if (!dmOfflineIntercept('dmUnreadMailCount')) {
        return Api.getUnreadMailCount().then((res:any) => {
            return res;
        });
    } else {
        return new Promise((resolve) => resolve(0));
    }
}

/**
 * Get inquiry form logs.
 *
 * @param page {number} Default 1.
 * @param formId {string}
 * @param recordId {string}
 * @param level {string}
 * @param levelName {string}
 * @param message {string}
 * @returns {Promise}
 */
 export function dmGetInquiryFormLogs(
    page: number = -1,
    formId: string = "",
    recordId: string = "",
    level: number = -1,
    levelName: string = "",
    message: string = ""
) {
    let parameters: { formId?: string, recordId?: string, level?: number, levelName?: string, message?: string } = {};
    if (formId !== "") {
        parameters.formId = formId;
    }
    if (recordId !== "") {
        parameters.recordId = recordId;
    }
    if (level !== -1) {
        parameters.level = level;
    }
    if (levelName !== "") {
        parameters.levelName = levelName;
    }
    if (message !== "") {
        parameters.message = message;
    }

    if (!dmOfflineIntercept('dmGetInquiryFormLogs')) {
        return Api.getInquiryFormLogs(page, parameters);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Add a new inquiry form log
 *
 * @param inquiryFormLog {InquiryFormLog}
 *
 * @returns {Promise}
 */
export function dmAddInquiryFormLog(inquiryFormLog: InquiryFormLog) {
    if (!dmOfflineIntercept('dmAddInquiryFormLog')) {
        return Api.addInquiryFormLog(inquiryFormLog);
    } else {
        return new Promise((resolve) => resolve(null));
    }
}

/**
 * Get inquiry form logs.
 *
 * @param page {number} Default 1.
 * @param pointId {string}
 * @param level {string}
 * @param levelName {string}
 * @param message {string}
 * @returns {Promise}
 */
export function dmGetPointFormLogs(
    page: number = -1,
    pointId: string = "",
    level: number = -1,
    levelName: string = "",
    message: string = ""
) {
    let parameters: { point?: string, level?: number, levelName?: string, message?: string } = {};
    if (pointId !== "") {
        parameters.point = pointId;
    }
    if (level !== -1) {
        parameters.level = level;
    }
    if (levelName !== "") {
        parameters.levelName = levelName;
    }
    if (message !== "") {
        parameters.message = message;
    }

    if (!dmIsOfflineMode()) {
        return Api.getPointFormLogs(page, parameters);
    } else {
        return new Promise((resolve) => resolve([]));
    }
}

/**
 * Invalidate a Session Country list property.
 *
 * @param property {keyof Country} Property to invalidate.
 *
 * @returns {Promise<boolean>}
 */
export function dmInvalidateInSession(property: keyof Country) {
    return new Promise<boolean>((resolve) => {
        let session = smGetSession();
        if (session) {
            let country: Country | null = session.country ? session.country : null;
            if (country && country[property]) {
                country[property] = undefined;
                smSetSession(session);
                return resolve(false);
            }
        }
        return resolve(true);
    });
}

/**
 * Alert internal Users changes.
 *
 * This Must be used after adding, removing or updates.
 *
 * @returns {Promise}
 */
 export function dmInvalidateUsers() {
    return new Promise<boolean>((resolve) => {
        cmRemoveItem(CACHE_USER_LIST);
        return resolve(true);
    });
}

/**
 * @returns {Promise}
 */
export function dmGetMutatebles() {
    if (!dmOfflineIntercept('dmGetMutatebles')) {
        return cmGetItem(CACHE_MUTATEBLES)
            .then((cache) => {
                if (!cache) {
                    return Api.getMutatebles()
                        .then((data) => {
                            cmSetItem(CACHE_MUTATEBLES, data);
                            return data;
                        });
                }
                return new Promise((resolve) => resolve(cache));
            });
    } else {
        return new Promise((resolve) => resolve([]));
    }
}
