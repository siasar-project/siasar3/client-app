/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import strtr from "locutus/php/strings/strtr";
import htmlspecialchars from "locutus/php/strings/htmlspecialchars";
import {I18nLanguage} from "@/objects/I18nLiterals";
import {isServerSideRendering, smGetSessionLanguage} from "../utils/sessionManager";
import {I18nOptions} from "@/objects/I18nOptions";
import {eventManager, Events} from "../utils/eventManager";
import {i18nDefaultLiterals} from "../translations/literals";

const STORAGE_I18N_STRINGS = 'i18n_strings';
const I18N_LANGUAGE_DEFAULT = 'en';
let currentLanguage: string = "";

const detectBrowserLanguage = require('detect-browser-language');

/**
 * Get the current language based in session, browser, or by default.
 * 
 * @returns {string}
 */
export function i18nGetCurrentLanguage() {
    if (currentLanguage === "") {
        currentLanguage = smGetSessionLanguage();

        if (currentLanguage === "") {
            if (!isServerSideRendering) {
                currentLanguage = detectBrowserLanguage().split('-').join('_');
           } else {
                currentLanguage = I18N_LANGUAGE_DEFAULT;
           }   
        }
    }

    return currentLanguage;
}

/**
 * Set the current language explicitly.
 * 
 * @param language {string}
 */
 export function i18nSetCurrentLanguage(language: string) {
    currentLanguage = language;
}

/**
 * Set current language to none.
 */
 export function i18nInvalidateCurrentLanguage() {
    currentLanguage = ""; 
}

/**
 * Load list from local storage.
 *
 * @param language {string} Language ID.
 *
 * @returns {I18nLanguage}
 */
export function i18nGetStrings(language: string): I18nLanguage {
    let data:string | null = null;
    if (!isServerSideRendering) {
        data = localStorage.getItem(STORAGE_I18N_STRINGS + '.' + language);
    }

    if (!data) {
        return {
            id: language,
            literals:{}
        };
    }
    return JSON.parse(data);
}

/**
 * Save list to local storage.
 *
 * @param language {I18nLanguage} Language ID.
 */
export function i18nSetStrings(language: I18nLanguage) {
    let data = JSON.stringify(language);
    localStorage.setItem(STORAGE_I18N_STRINGS + '.' + language.id, data);
}

/**
 * Load list from local storage.
 *
 * @param language {string} Language ID.
 *
 * @returns {string[]}
 */
 export function i18nGetStringsUntranslated(language: string): string[] {
    let data:string | null = null;

    if (!isServerSideRendering) {
        data = localStorage.getItem(STORAGE_I18N_STRINGS + '.untranslated.' + language);
    }

    if (!data) {
        return []
    }

    return JSON.parse(data);
}

/**
 * Save list to local storage.
 *
 * @param language {I18nLanguage} Language ID.
 * @param literals {string[]} list of strings untranslated.
 */
export function i18nSetStringsUntranslated(language: string, literals: string[]) {
    let data = JSON.stringify(literals);
    localStorage.setItem(STORAGE_I18N_STRINGS + '.untranslated.' + language, data);
}

/**
 * Scape HTML special characters.
 *
 * @param text {string}
 *   Text to scape.
 *
 * @returns {string}
 *   String escaped.
 *   <code>escapeHtml('Kip\'s <b>evil</b> "test" code\'s here');</code>
 *   Returns: <code>Kip&#039;s &lt;b&gt;evil&lt;/b&gt; &quot;test&quot; code&#039;s here</code>
 *
 * @see https://stackoverflow.com/a/4835406/6385708
 */
export function escapeHtml(text: string) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

/**
 * Replaces placeholders in a string with values.
 *
 * @param string {string}
 *   A string containing placeholders. The string itself is expected to be
 *   safe and correct HTML. Any unsafe content must be in $args and
 *   inserted via placeholders.
 * @param args {Object}
 *   An associative array of replacements. Each array key should be the same
 *   as a placeholder in $string. The corresponding value should be a string
 *   or an object that implements
 *   \App\Tools\MarkupInterface. The value replaces the
 *   placeholder in $string. Sanitization and formatting will be done before
 *   replacement. The type of sanitization and formatting depends on the first
 *   character of the key:
 *   - @variable: When the placeholder replacement value is:
 *     - A string, the replaced value in the returned string will be sanitized
 *       using \Drupal\Component\Utility\Html::escape().
 *     - A MarkupInterface object, the replaced value in the returned string
 *       will not be sanitized.
 *     - A MarkupInterface object cast to a string, the replaced value in the
 *       returned string be forcibly sanitized using
 *       \Drupal\Component\Utility\Html::escape().
 * @code
 *         $this->placeholderFormat('This will force HTML-escaping of the replacement value: @text', ['@text' => (string) $safe_string_interface_object));
 * @endcode
 *     Use this placeholder as the default choice for anything displayed on
 *     the site, but not within HTML attributes, JavaScript, or CSS. Doing so
 *     is a security risk.
 *   - %variable: Use when the replacement value is to be wrapped in <em>
 *     tags.
 *     A call like:
 * @code
 *       $string = "%output_text";
 *       $arguments = ['%output_text' => 'text output here.'];
 *       $this->placeholderFormat($string, $arguments);
 * @endcode
 *     makes the following HTML code:
 * @code
 *       <em class="placeholder">text output here.</em>
 * @endcode
 *     As with @variable, do not use this within HTML attributes, JavaScript,
 *     or CSS. Doing so is a security risk.
 *   - :variable: Return value is escaped with
 *     \Drupal\Component\Utility\Html::escape() and filtered for dangerous
 *     protocols using UrlHelper::stripDangerousProtocols(). Use this when
 *     using the "href" attribute, ensuring the attribute value is always
 *     wrapped in quotes:
 * @code
 *     // Secure (with quotes):
 *     $this->placeholderFormat('<a href=":url">@variable</a>', [':url' => $url, '@variable' => $variable]);
 *     // Insecure (without quotes):
 *     $this->placeholderFormat('<a href=:url>@variable</a>', [':url' => $url, '@variable' => $variable]);
 * @endcode
 *     When ":variable" comes from arbitrary user input, the result is secure,
 *     but not guaranteed to be a valid URL (which means the resulting output
 *     could fail HTML validation). To guarantee a valid URL, use
 *     Url::fromUri($user_input)->toString() (which either throws an exception
 *     or returns a well-formed URL) before passing the result into a
 *     ":variable" placeholder.
 *
 * @returns {string}
 *   A formatted HTML string with the placeholders replaced.
 *
 * @ingroup sanitization
 *
 * @see \Drupal\Core\StringTranslation\TranslatableMarkup
 * @see \Drupal\Core\StringTranslation\PluralTranslatableMarkup
 * @see \Drupal\Component\Utility\Html::escape()
 * @see \Drupal\Component\Utility\UrlHelper::stripDangerousProtocols()
 * @see \Drupal\Core\Url::fromUri()
 */
export function i18nPlaceholderFormat(string: string, args: any): string {
    // Transform arguments before inserting them.
    for (const key in args) {
        let value: any = args[key];
        if (!value) {
            value = '';
        }
        switch (key.charAt(0)) {
            case ':':
                // Strip URL protocols that can be XSS vectors.
                // todo This do nothing.
                break;
            case '@':
                // Escape if the value is not an object from a class that implements
                // \App\Tools\MarkupInterface, for example strings will
                // be escaped.
                // Strings that are safe within HTML fragments, but not within other
                // contexts, may still be an instance of
                // \App\Tools\MarkupInterface, so this placeholder type
                // must not be used within HTML attributes, JavaScript, or CSS.
                args[key] = htmlspecialchars(value, ['ENT_QUOTES', 'ENT_SUBSTITUTE'], 'UTF-8');
                break;

            case '%':
                // Similarly to @, escape non-safe values. Also, add wrapping markup
                // in order to render as a placeholder. Not for use within attributes,
                // per the warning above about
                // \App\Tools\MarkupInterface and also due to the
                // wrapping markup.
                args[key] = '`' + escapeHtml(value) + '`';
                break;

            default:
                // No replacement possible therefore we can discard the argument.
                args[key] = null;
                break;
        }
    }

    return strtr(string, args);
}

/**
 * Translates a string to the current language or to a given language.
 *
 * See \Drupal\Core\StringTranslation\TranslatableMarkup::__construct() for
 * important security information and usage guidelines.
 *
 * In order for strings to be localized, make them available in one of the
 * ways supported by the
 *
 * @link https://www.drupal.org/node/322729 Localization API #endlink. When
 * possible, use the \Drupal\Core\StringTranslation\StringTranslationTrait
 * $this->t(). Otherwise create a new
 * \Drupal\Core\StringTranslation\TranslatableMarkup object.
 *
 * @param text {string}
 *   A string containing the English text to translate.
 * @param args {Object}
 *   (optional) An associative array of replacements to make after
 *   translation. Based on the first character of the key, the value is
 *   escaped and/or themed. See
 *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
 *   details.
 * @param options {Object}
 *   (optional) An associative array of additional options, with the following
 *   elements:
 *   - 'langcode' (defaults to the current language): A language code, to
 *     translate to a language other than what is used to display the page.
 *   - 'context' (defaults to the empty context): The context the source
 *     string belongs to. See the
 * @link i18n Internationalization topic #endlink for more information
 *     about string contexts.
 *
 * @return {string}
 *   An object that, when cast to a string, returns the translated string.
 *
 * @see  \Drupal\Component\Render\FormattableMarkup::placeholderFormat()
 * @see  \Drupal\Core\StringTranslation\TranslatableMarkup::__construct()
 *
 * @ingroup sanitization
 */

/**
 * Translate string.
 *
 * @param text
 * @param args
 * @param options
 * @returns {string}
 */
export default function t(text:string, args:object = {}, options:I18nOptions = {}): string
{
    let resp:string = text;
    let language:string;

    language = options.langcode ?? i18nGetCurrentLanguage();
    if (process.env.NEXT_PUBLIC_DEBUG_I18N === '1') {
        resp = language + '_' + resp;
    }

    if (language !== I18N_LANGUAGE_DEFAULT) {
        // Get translated strings.
        let i18nStrings = i18nGetStrings(language);
        // Search string in translated.
        if (i18nStrings.literals && i18nStrings.literals[text]) {
            resp = i18nStrings.literals[text];
        } else {
            // Try with default collection.
            if (i18nDefaultLiterals(language)?.literals[text]) {
                resp = i18nDefaultLiterals(language)?.literals[text];
            } else if (!isServerSideRendering) {
                // Get untranslated strings.
                let i18nUntranslated = i18nGetStringsUntranslated(language);
                // Search string in untranslated.
                if (i18nUntranslated.indexOf(text) === -1) {
                    i18nUntranslated.push(text);
                    i18nSetStringsUntranslated(language, i18nUntranslated)
                    // Send untranslated literal to remote API server, using events.
                    eventManager.dispatch(Events.I18N_ADD_LITERAL, {literal: text});
                }
            }
        }
    } else {
        resp = text;
    }

    // Apply replacements.
    resp = i18nPlaceholderFormat(resp, args);

    // If debugging i18n, add [] to the string to differentiate it from the unprocessed strings (i.e. when t() function was not used).
    if (process.env.NEXT_PUBLIC_DEBUG_I18N === '1') {
        resp = '[' + resp + ']';
    }

    return resp;
}

/**
 * Debug console log filtering by ID.
 *
 * @param msg {string} Message to display.
 * @param limitId {string} Dynamic string to validate.
 * @param requiredId {string} Static string with validate.
 */
function consoleLog(msg:string, limitId: string = '', requiredId: string = '') {
    if (requiredId === '') {
        return;
    }
    if (limitId !== '' && limitId !== requiredId) {
        return;
    }
    console.log(`[${limitId}]`, msg);
}
