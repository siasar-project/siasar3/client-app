/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {EllipsisPositionEnum} from "@/objects/EllipsisPositionEnum";
import substr from "locutus/php/strings/substr";
import {empty} from "locutus/php/var";
import {DateTime} from "luxon";
import {smGetSession, smHasPermission} from "./sessionManager";
import {DestinationSettingsInterface} from "@/objects/DestinationSettingsInterface";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import router from "next/router";

/**
 * Parse string to boolean.
 *
 * @param str {string | null | undefined}
 *   Boolean representation.
 * @returns {boolean}
 *   Boolean value.
 *
 * @see https://stackoverflow.com/a/71129626/6385708
 */
export const stringToBoolean = (str: string | null | undefined) => {
    if (!str) {
        return false
    }
    if (typeof str === "string") {
        return !["0", "false", "no", "n", "null", "undefined", "nil"].includes(str.toLowerCase().trim())
    }
    return Boolean(str)
}

/**
 * Truncate text with ellipsis
 * 
 * @param str {string}
 * @param limit {number}
 * @param position {EllipsisPositionEnum | null}
 * @returns {string}
 */
export const truncateWithEllipsis = (str: string, limit: number, position?: EllipsisPositionEnum | null) => {
    if (empty(str)) {
        return str;
    }

    if (!position) {
        position = EllipsisPositionEnum.Right;
    }

    if (limit < 20) {
        limit = 20;
    }

    let start: string = '';
    let end: string = '';
    let length: number = str.length;
    let len: number;

    if (length > limit) {
        switch (position) {
            case EllipsisPositionEnum.Left:
                len = limit-3;
                return "..." + substr(str, length - len );
            case EllipsisPositionEnum.Right:
                return substr(str, 0, limit-3) + "...";
            case EllipsisPositionEnum.Middle:
                len = limit / 2 - 3;
                start = substr(str, 0, len);
                end = substr(str, length-len);
                return start + ' ... ' + end;
        }
    }
    return str;
}

/**
 * format bytes to human readable
 *
 * @param bytes {number}
 * @param decimals {number}
 *
 * @returns {string}
 */
export const formatBytes = (bytes: number, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

/**
 * Decode HTML entities.
 *
 * @param html
 *
 * @returns {string}
 *
 * @see https://stackoverflow.com/a/42182294/6385708
 */
export const decodeHtmlEntities = (html: string) => {
    let txt:any = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

/**
 * creates a new luxon date object from a given string and optional timezone
 * 
 * @param originalDate {any}
 * @param originalTimezone {string}
 * 
 * @returns {DateTime}
 */
export const getLuxonDate = (originalDate: any, originalTimezone: string = "UTC") => {
    if (!originalDate) {
        return originalDate;
    }
    let date = originalDate.split(" ")[0];
    let time = originalDate.split(" ")[1] || 
        new Date().toLocaleTimeString("UTC", {hour: "numeric", minute: "numeric", second: "numeric", hour12: false});
    let objectDate = {
        year: date.split("-")[0], month: date.split("-")[1], day: date.split("-")[2],
        hour: time.split(":")[0], minute: time.split(":")[1], second: time.split(":")[2],
    };

    if ("" === objectDate.year) {
        return undefined;
    }

    let luxonDate = DateTime.fromObject(objectDate, {zone: originalTimezone});
    return luxonDate;
}

/**
 * returns time and date formatted to the user's timezone, in the format formRecords use
 * 
 * @param originalDate {any}
 * @param originalTimezone {string}
 * @param userTimezone {string}
 * 
 * @returns {string}
 */
export const getRelativeDate = (
    originalDate: any,
    originalTimezone: string,
    userTimezone: string
) => {
    let luxonDate = getLuxonDate(originalDate, originalTimezone);

    if (undefined === luxonDate) {
        return "";
    }

    luxonDate = luxonDate.setZone(userTimezone);
    return luxonDate.toFormat("yyyy-LL-dd TT");
}

/**
 * returns time and date formatted to the user's timezone, and stringyfied to the user's timezone locale
 * 
 * @param originalDate {any}
 * @param originalTimezone {string}
 * 
 * @returns {string}
 */
export const getLocalizedDate = (originalDate: any, originalTimezone: string) => {
    const locale = getLocale();
    const IntlOptions: Intl.DateTimeFormatOptions = {
        year: "numeric", month: "numeric", day: "numeric",
        hour: "numeric", minute: "numeric", second: "numeric",
        hour12: false,
    };

    let luxonDate = getLuxonDate(originalDate, originalTimezone);
    if (undefined === luxonDate) {
        return "";
    }
    let luxonStringFormatted = luxonDate.toLocaleString(IntlOptions, { locale });
    return luxonStringFormatted;
};

/**
 * returns user's timezone if it exists, and the browser's timezone otherwise
 * 
 * @returns {string}
 */
export const getTimezone = () => {
    let userSession = smGetSession();
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    if (userSession?.timezone) {
        timezone = userSession.timezone;
    }

    return timezone;
};

/**
 * returns user's locale, derived from session language (default: es)
 * 
 * @returns {string}
 */
export const getLocale = () => {
    let userSession = smGetSession();
    let locale = "es";

    if (userSession?.language) {
        locale = userSession.language.split("_")[0];
    }

    return locale;
};

/**
 * Is the current user in the point team?
 *
 * @param point
 *
 * @returns {true}
 */
export const AmIInPointTeam = (point: any) => {
    if (smHasPermission('all point teams')) {
        return true;
    }

    let session = smGetSession();
    for (let i = 0; i < point.team.length; i++) {
        if (point.team[i].value === session?.id) {
            return true;
        }
    }
    return false;
}

/**
 * Build the destination query param with chain start if required.
 *
 * @param params
 * @param params.glue {string} Add this glue character to response. '?' by default.
 * @param params.uriEncode {boolean} Encode value with URL encoder. False how default.
 * @param params.prefix {string} Prefix destination URL with this. Empty by default.
 * @param params.value {string} Destination value.
 *
 * @returns {string}
 */
export const getDestinationQueryParameter = (params: DestinationSettingsInterface) => {
    let response: string;
    let glue = params.glue || "?";
    let encode = params.uriEncode || false;
    let prefix = params.prefix || "";

    if ("" === params.value || undefined === params.value) {
        response = "";
    } else {
        response = glue + 'destination=' + (encode ? encodeURIComponent(prefix + params.value) : prefix + params.value);
    }

    return response;
}

/**
 * Redirect application to another page.
 *
 * @param destination Context preferred destination page.
 * @param onlineDefault Default online destination page.
 * @param offlineDefault Default offline destination page.
 */
export const routeTo = (destination:string | string[] | undefined, onlineDefault:string, offlineDefault:string) => {
    if (dmIsOfflineMode()) {
        // Check if offline mode and destination contains "/point".
        if (destination && typeof destination === 'string' && destination.indexOf('/point') !== -1) {
            router.push(destination);
        } else {
            router.push(offlineDefault);
        }
    } else {
        // Redirect to destination or default online page.
        router.push(destination as string || onlineDefault);
    }
}

/**
 * File to base64 image.
 *
 * @param blob {Blob}
 *
 * @returns {Promise}
 */
export const blobToBase64 = (blob: Blob): Promise<string> => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        /**
         * On load complete.
         */
        reader.onloadend = () => {
            resolve(reader.result as string);
        };
        /**
         * On load error.
         *
         * @param error
         */
        reader.onerror = (error) => {
            reject(error);
        };
    });
}
