/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {db} from "@/utils/db";

/**
 * Cache manager over local storage.
 * 
 * @see https://www.sohamkamani.com/javascript/localstorage-with-ttl-expiry/
 */

const CACHE_PREFIX = 'cache_';

/**
 * Add item to cache.
 * 
 * @param key {string}
 * @param value {any}
 * @param ttl {number}
 *
 * @returns {Promise}
 */
export function cmSetItem(key: string, value: any, ttl: number =  3600) {
    if (process.env.NEXT_PUBLIC_CACHE_DISABLED === '1') {
        return;
    }

    const now = new Date();

    // `item` is an object which contains the original value
    // as well as the time when it's supposed to expire
    const item = {
        value: value,
        expiry: Math.floor(now.getTime() / 1000) + ttl,
    }

    return db.addKey(CACHE_PREFIX + key, JSON.stringify(item));
}

/**
 * Get item from the cache if the key exists.
 * 
 * @param key {string}
 * 
 * @returns {Promise}
 */
export function cmGetItem(key: string)  {
    if (process.env.NEXT_PUBLIC_CACHE_DISABLED === '1') {
        return new Promise((resolve) => resolve(null));
    }
    return db.getKey(CACHE_PREFIX + key)
        .then((itemStr) => {
            // if the item doesn't exist, return null
            if (!itemStr) {
                return null;
            }
            const item = JSON.parse(itemStr);

            // Are we in offline mode?
            if (!dmIsOfflineMode()) {
                // No, allow to expire cache items.
                const now = new Date();
                // compare the expiry time of the item with the current time
                if (Math.floor(now.getTime() / 1000) > item.expiry) {
                    // If the item is expired, delete the item from storage
                    // and return null
                    return db.invalidateKey(CACHE_PREFIX + key);
                }
            }

            return item.value
        });
}

/**
 * Remove item if the key exists.
 * 
 * @param key {string}
 *
 * @returns {Promise}
 */
export function cmRemoveItem(key: string) {
    return db.invalidateKey(CACHE_PREFIX + key);
}

/**
 * Remove all cache items.
 *
 * @returns {Promise}
 */
export function cmClearCache() {
    // Empty cache.
    return db.invalidateAllKeys();
}
