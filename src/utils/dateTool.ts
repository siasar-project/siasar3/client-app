/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Get date how timestamp in seconds.
 *
 * @param date {Date | undefined} Date
 * @returns {number}
 *   Timestamp.
 */
export function dateToTimestamp(date: Date | undefined): number {
    if (date === undefined) {
        date = new Date();
    }
    return Math.floor(date.getTime() / 1000);
}

/**
 * Get current date in seconds timestamp.
 *
 * @returns {number}
 *   Timestamp.
 */
export function dateNow() {
    return dateToTimestamp(new Date());
}

/**
 * Get date object from timestamp.
 *
 * @param timestamp number Timestamp in seconds.
 * @returns {Date}
 */
export function timestampToDate(timestamp: number) {
    let currentTimeInSeconds=Math.floor(timestamp);
    let date = new Date();
    date.setTime(currentTimeInSeconds * 1000);

    return date;
}
