/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

const STORAGE_FORM_TYPES = 'form_types';

export interface formType {
    allow_sdg: boolean,
    description: string,
    id: string,
    path: string,
    title: string,
    type: string,
}

/**
 * Load list from local storage.
 *
 * @returns {Object}
 */
export function getFormTypes(): Array<formType> {
    let data = localStorage.getItem(STORAGE_FORM_TYPES) ?? '[]';
    return JSON.parse(data);
}

/**
 * Save list in local storage.
 *
 * @param list {Array<formType>}
 *   Complete form types list.
 */
function setFormTypes(list: Array<formType>) {
    let data = JSON.stringify(list);
    localStorage.setItem(STORAGE_FORM_TYPES, data);
}

/**
 * Exist the form type id?
 *
 * @param id {string}
 *   Form type id.
 * @returns {boolean}
 *   True if the form type is defined.
 */
function existFormType(id: string): boolean {
    return (getFormType(id) !== null);
}

/**
 * Remove a system form type.
 *
 * @param id
 */
function removeFormType(id: string) {
    let list:Array<formType> = getFormTypes();
    let newList:Array<formType> = [];
    for (let i = 0; i < list.length; ++i) {
        if (list[i].id !== id) {
            newList.push(list[i]);
        }
    }
    setFormTypes(newList);
}

/**
 * Get form type by id.
 *
 * @param id {string}
 *   Form type id.
 * @returns {formType | null}
 *   The form type.
 */
export function getFormType(id: string): formType | null {
    let list:Array<formType> = getFormTypes();
    for (let i = 0; i < list.length; ++i) {
        let formDef = list[i];
        if (formDef.id === id) {
            return formDef;
        }
    }

    return null;
}

/**
 * Add a new system form type.
 *
 * @param formDef {formType}
 */
export function addFormType(formDef: formType) {
    if (existFormType(formDef.id)) {
        // Remove old form type.
        removeFormType(formDef.id);
    }
    // Add the new form type.
    let list:Array<formType> = getFormTypes();
    list.push(formDef);
    setFormTypes(list);
}
