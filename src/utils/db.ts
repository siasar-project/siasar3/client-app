/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import Dexie, {Table} from 'dexie';
import {dmGetPageSize} from "@/utils/dataManager";
import {str_replace} from "locutus/php/strings";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import t from "@/utils/i18n";

export interface Document {
    id?: number;
    ulid: string;
    type: string;
    // Keywords to filtering.
    keys: string;
    data: any;
}

/**
 * Database layer.
 *
 * Used by data manager and punctual code to persist data.
 *
 * @see dataManager
 */
export class Database extends Dexie {
    // Record types const.
    public static readonly DB_TYPE_DIVISION = "administrative_division";
    public static readonly DB_TYPE_POINT = "point";
    public static readonly DB_TYPE_FORM_RECORD = "form_record";
    public static readonly DB_TYPE_FORM_RECORD_INDEX = "form_index_record";
    public static readonly DB_TYPE_FREE_KEY = "free_key";
    public static readonly DB_TYPE_HOUSEHOLD_PROCESS = "household_process";
    public static readonly DB_TYPE_SYSTEM_UNITS = "system.units";

    documents!: Table<Document>;

    /**
     * Database constructor.
     */
    constructor() {
        super('siasarApp');
        // todo Validate that we don't have duplicated data.
        // this.version(2)
        //     .stores({
        //         // Primary key and indexed props.
        //         documents: '++id, &ulid, type, keys'
        //     });
        this.version(1)
            .stores({
                // Primary key and indexed props.
                documents: '++id, ulid, type, keys'
            });
    }

    /**
     * Alert internal data changes.
     *
     * This Must be used after adding, removing or updates.
     *
     * @param type {string}
     *
     * @returns {Promise}
     */
    _invalidateAllDataByType(type: string) {
        return this.transaction('readwrite', db.documents, () => {
            let query = db.documents;
            let where = query.where("type").equals(type);

            return where.delete().then(() => {
                return Promise.resolve([]);
            });
        });
    }

    /**
     * Get administrative divisions.
     *
     * @param name {string}
     * @param code {string}
     * @param parent {string|null}
     * @param page {number}
     *
     * @returns {Promise<any>}
     */
    getAdministrativeDivisions = (name: string = '', code: string = '', parent: string|null = '', page: number = -1): Promise<any> => {
        return this.transaction('readonly', db.documents, () => {
            // The parent filter is always used initially to build the query. So if passed empty, null is used.
            if (parent === '') {
                parent = null;
            } else {
                parent = str_replace('/api/v1/administrative_divisions/', '', parent);
            }
            // Filter by more significating index first, because only one index can be used in "and" queries.
            let query = db.documents.where('keys').equals('parent_'+parent);
            // Later filter with JS callback function by type to ensure correct row.
            query.and(row => row.type === Database.DB_TYPE_DIVISION);
            // Optionally filter with JS callback function by name.
            if (name !== '') {
                // Ignore text case.
                name = name.toLowerCase();
                // Ignore text again here, and to use indexOf() to simulate a SQL like comparator.
                query.and(row => row.data.name.toLowerCase().indexOf(name) !== -1);
            }
            // Optionally filter with JS callback function by code.
            if (code.toLowerCase !== undefined && code !== '' && code !== null && code !== undefined) {
                // Ignore text case.
                code = code.toLowerCase();
                // Ignore text again here, and to use indexOf() to simulate a SQL like comparator.
                query.and(row => row.data.code && row.data.code.toLowerCase().indexOf(code) !== -1);
            }

            // Perform filter and sort by name.
            return query.sortBy('data.name').then(items => {
                // if paging is required and there are elements.
                let resp:Array<any> = [];
                if (page !== -1) {
                    if (items.length > 0) {
                        let pageSize = dmGetPageSize();
                        let itemsCount = items.length;
                        let offset: number = (page - 1) * pageSize;
                        // items = items.slice(offset, offset + dmGetPageSize());
                        for (let i = 0; i < itemsCount; i++) {
                            if (resp.length >= pageSize) {
                                break;
                            }
                            if (i >= offset) {
                                resp.push(items[i]);
                            }
                        }
                    }
                } else {
                    resp = items;
                }

                return Promise.resolve(resp.map(item => item.data));
            });
        });
    }

    /**
     * Get administrative division.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    getAdministrativeDivision = (id: string): Promise<any> => {
        if (!id) {
            return new Promise<any>((resolve) => resolve(null));
        }
        const params: Record<string,any> = {
            ulid: id,
            // Redundant filter, the ID is unique.
            type: Database.DB_TYPE_DIVISION,
        };

        return db.documents.where(params).first(item => item?.data);
    }

    /**
     * Build the object to insert a administrative division.
     *
     * @param data
     *
     * @returns {any}
     */
    _addAdministrativeDivision = (data: any): any => {
        if (!data) {
            return undefined;
        }
        return {
            ulid: data.id,
            type: Database.DB_TYPE_DIVISION,
            keys: 'parent_'+(data.parent ? str_replace("/api/v1/administrative_divisions/", "", data.parent) : 'null'),
            data: data,
        };
    }

    /**
     * Add one administrative division.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    addAdministrativeDivision = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = db._addAdministrativeDivision(data);
            return db._addDbItem(item);
        });
    }

    /**
     * Add many administrative divisions at once.
     *
     * @param data {Array<any>}
     *
     * @returns {Promise<any>}
     */
    addAdministrativeDivisions = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let items = [];
            for (let i = 0; i < data.length; i++) {
                items.push(db._addAdministrativeDivision(data[i]));
            }
            return db.documents.bulkAdd(items);
        });
    }

    /**
     * Alert internal AdministrativeDivisions changes.
     *
     * This Must be used after adding, removing or updates.
     * 
     * @param parent {string|null} AdministrativeDivision parent property.
     *
     * @returns {Promise}
     */
    invalidateAdministrativeDivisions(parent: string|null) {
        return this.transaction('readwrite', db.documents, () => {
            let query = db.documents;
            let where = query.where("type").equals(Database.DB_TYPE_DIVISION);

            if (parent && parent !== '') {
                where.and(document => {
                    return document.keys === 'parent_' + parent;
                });
            } else {
                where.and(document => {
                    return document.keys === 'parent_null';
                });
            }
            return where.delete().then(() => {
                return Promise.resolve([]);
            });
        });
    }

    /**
     * Alert internal AdministrativeDivisions changes.
     *
     * This Must be used after adding, removing or updates.
     *
     * @returns {Promise}
     */
    invalidateAllAdministrativeDivisions() {
        return db._invalidateAllDataByType(Database.DB_TYPE_DIVISION);
    }

    /**
     * Get one point item to write in database.
     *
     * @param data {any}
     *
     * @returns {any}
     */
    _addPoint = (data: any): any => {
        return {
            ulid: data.id,
            type: Database.DB_TYPE_POINT,
            keys: Database.DB_TYPE_POINT + '_' + data.administrative_division[0].id,
            data: data,
        };
    }

    /**
     * Add one point.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    addPoint = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = db._addPoint(data);
            return db._addDbItem(item);
        });
    }

    /**
     * Update one form point.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    updatePoint = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item:Document = this._addPoint(data);
            return this._updateDbItem(item, item.type);
        });
    }

    /**
     * Get point.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    getFormPoint = (id: string): Promise<any> => {
        const params: Record<string,any> = {
            ulid: id,
            // Redundant filter, the ID is unique.
            type: Database.DB_TYPE_POINT,
        };
        return db.documents.where(params).first(item => item?.data);
    }

    /**
     * Get form data.
     *
     * @param country {string} Country ID, code, without IRI.
     * @param page {number} List page number, default 1.
     * @param parameters {any}
     * @param allPages {boolean}
     *
     * @returns {Promise}
     */
    getFormPoints = (country: string, page: number = 1, parameters: any = {}, allPages: boolean = false): Promise<any> => {
        // Load data from database using filters...
        return this.transaction('readonly', db.documents, () => {
            // Filter by more significating index first, because only one index can be used in "and" queries.
            let query = db.documents.where('type').equals(Database.DB_TYPE_POINT);

            // Perform filter and sort by name.
            return query.sortBy('data.updated').then(items => {
                // if paging is required and there are elements.
                let resp:Array<any> = [];
                if (page !== -1) {
                    if (items.length > 0) {
                        let pageSize = dmGetPageSize();
                        let itemsCount = items.length;
                        let offset: number = (page - 1) * pageSize;
                        // items = items.slice(offset, offset + dmGetPageSize());
                        for (let i = 0; i < itemsCount; i++) {
                            if (resp.length >= pageSize) {
                                break;
                            }
                            if (i >= offset) {
                                resp.push(items[i]);
                            }
                        }
                    }
                } else {
                    resp = items;
                }

                return Promise.resolve(resp.map(item => item.data));
            });
        });
    }

    /**
     * Get SIASAR Point Inquiries.
     *
     * @param id {string} Point ID.
     *
     * @returns {Promise}
     */
    getFormPointInquiries = (id: string) => {
        return db.getFormPoint(id)
            .then((point: any) => {
                let middlePromise3 = [];
                let records: DynFormRecordInterface[] = [];
                // community: Array [ "01GM8XH6PZAPH3DKN5NZDG1957" ]
                for (let j = 0; j < point['community'].length; j++) {
                    let recordId = point['community'][j];
                    // Add form record.
                    middlePromise3.push(db.getFormIndexRecord(recordId)
                        .then((data: any) => {
                            records.push(data);
                        })
                    );
                }
                // wsystem: Array [ {…} ]
                // 0: Object { id: "01GM8YB6JY2TZM85CG3XWWSMWM", name: "Sistema de La Flor", meta: [] }
                for (let j = 0; j < point['wsystem'].length; j++) {
                    let recordId = point['wsystem'][j].id;
                    // Add form record.
                    middlePromise3.push(db.getFormIndexRecord(recordId)
                        .then((data: any) => {
                            records.push(data);
                        })
                    );
                }
                // wsp: Array [ {…} ]
                // 0: Object { id: "01GM8XHWMDPEYKNZWN7XAPPRMD", name: "Junta de Agua de La Flor", meta: [] }
                for (let j = 0; j < point['wsp'].length; j++) {
                    let recordId = point['wsp'][j].id;
                    // Add form record.
                    middlePromise3.push(db.getFormIndexRecord(recordId)
                        .then((data: any) => {
                            records.push(data);
                        })
                    );
                }

                return Promise
                    .all(middlePromise3)
                    .then(() => {
                        return records;
                    });
            });
    }

    /**
     * Alert internal form points changes.
     *
     * This Must be used after adding, removing or updates.
     *
     * @returns {Promise}
     */
    invalidateAllFormPoints() {
        return db._invalidateAllDataByType(Database.DB_TYPE_POINT);
    }

    /**
     * Build a new item to insert.
     *
     * @param data
     *
     * @returns {Document}
     */
    _addFormRecord = (data: any): Document => {
        return {
            ulid: data.id,
            type: Database.DB_TYPE_FORM_RECORD,
            keys: Database.DB_TYPE_FORM_RECORD + '_' + data.form,
            data: data,
        };
    }

    /**
     * Add one form record.
     *
     * IMPORTANT: Data must have id & form properties. Where form is the form type.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    addFormRecord = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = this._addFormRecord(data);
            return db._addDbItem(item);
        }).catch((reason) => {
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.ERROR,
                    title: t('Add Error'),
                    message: data.id + " => " + reason,
                    isPublic: true,
                }
            );
        });
    }

    /**
     * Update one form record.
     *
     * IMPORTANT: Data must have id & form properties. Where form is the form type.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    updateFormRecord = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item:Document = this._addFormRecord(data);
            return this._updateDbItem(item, item.type);
        });
    }

    /**
     * Get form record.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    getFormRecord = (id: string): Promise<any> => {
        const params: Record<string,any> = {
            ulid: id,
            // Redundant filter, the ID is unique.
            type: Database.DB_TYPE_FORM_RECORD,
        };
        return db.documents.where(params).first((item) => {
            return item?.data;
        });
    }

    /**
     * Get inquiry list.
     *
     * @param page
     * @param params
     *
     * @returns {Promise}
     */
    getFormRecords = (page: number = 1, params: any = {}) => {
        // let parameters: { record_type?: string | string[], field_status?: string[], field_region?: string, field_creator?: string, order?: {field_changed?: string} } = {};
        params.record_type = ["form.community.household"];
        // Find record list and return it.
        return this.transaction('readonly', db.documents, () => {
            // Filter by more significant index first, because only one index can be used in "and" queries.
            let query = db.documents.where('type').equals(Database.DB_TYPE_FORM_RECORD);
            // Later filter with JS callback function by type to ensure correct row.
            if (Array.isArray(params.record_type) && params.record_type.length > 0) {
                if (params.record_type) {
                    let typeConditions = params.record_type.map((keys:any) => Database.DB_TYPE_FORM_RECORD+'_' + keys);
                    if (typeConditions.length > 0) {
                        let combinedConditions = {
                            type: Database.DB_TYPE_FORM_RECORD,
                            or: typeConditions.map((condition:any) => ({keys: condition}))
                        };
                        query.and(row => {
                            return row.type === combinedConditions.type &&
                                combinedConditions.or.some((condition:any) => row.keys === condition.keys);
                        });
                    }
                }
            }

            if (typeof params.field_household_process !== "undefined" && params.field_household_process !== "") {
                query.and(row => {
                    return row.data.field_household_process.value === params.field_household_process;
                });
            }
            if (typeof params.field_interviewer !== "undefined" && params.field_interviewer !== "") {
                query.and(row => {
                    return row.data.field_interviewer.value === params.field_interviewer;
                });
            }
            // if (params.formType !== "") {
            //     query.and(row => row.keys === DB_TYPE_FORM_RECORD_INDEX+'_'+params.formType);
            // }
            // // Optionally filter with JS callback function by name.
            // if (name !== '') {
            //     // Ignore text case.
            //     name = name.toLowerCase();
            //     // Ignore text again here, and to use indexOf() to simulate a SQL like comparator.
            //     query.and(row => row.data.name.toLowerCase().indexOf(name) !== -1);
            // }
            // // Optionally filter with JS callback function by code.
            // if (code.toLowerCase !== undefined && code !== '' && code !== null && code !== undefined) {
            //     // Ignore text case.
            //     code = code.toLowerCase();
            //     // Ignore text again here, and to use indexOf() to simulate a SQL like comparator.
            //     query.and(row => row.data.code && row.data.code.toLowerCase().indexOf(code) !== -1);
            // }

            // Perform filter and sort by name.
            return query.sortBy('data.field_changed').then(items => {
                items = items.reverse();
                // if paging is required and there are elements.
                let resp:Array<any> = [];
                if (page !== -1) {
                    if (!page) {
                        page = -1;
                    }
                    if (items.length > 0) {
                        let pageSize = dmGetPageSize();
                        let itemsCount = items.length;
                        let offset: number = (page - 1) * pageSize;
                        // items = items.slice(offset, offset + dmGetPageSize());
                        for (let i = 0; i < itemsCount; i++) {
                            if (resp.length >= pageSize) {
                                break;
                            }
                            if (i >= offset) {
                                resp.push(items[i]);
                            }
                        }
                    }
                } else {
                    resp = items;
                }

                return Promise.resolve(resp.map(item => item.data));
            });
        });
    }

    /**
     * Alert internal form recors changes.
     *
     * This Must be used after adding, removing or updates.
     *
     * @returns {Promise}
     */
    invalidateAllFormRecords() {
        return db._invalidateAllDataByType(Database.DB_TYPE_FORM_RECORD);
    }

    /**
     * Alert internal data changes.
     *
     * @param id {string}
     * @param type {string}
     *
     * @returns {Promise}
     */
    invalidateFormRecord(id: string, type: string) {
        return this.transaction('readwrite', db.documents, () => {
            let query = db.documents;
            let where = query
                .where("type").equals(type)
                .and((x: Document) => x.ulid === id);

            return where.delete().then(() => {
                return Promise.resolve([]);
            });
        });
    }

    /**
     * Get one point item to write in database.
     *
     * @param data {any}
     *
     * @returns {any}
     */
    _addFormIndexRecord = (data: any): any => {
        return {
            ulid: data.id,
            type: Database.DB_TYPE_FORM_RECORD_INDEX,
            keys: Database.DB_TYPE_FORM_RECORD_INDEX + '_' + data.type,
            data: data,
        };
    }

    /**
     * Add system unit list.
     *
     * @param data {any}
     * @param type {string}
     *
     * @returns {Promise<any>}
     */
    addSystemUnits = (data: any, type: string): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = {
                ulid: Database.DB_TYPE_SYSTEM_UNITS + '.' + type,
                type: Database.DB_TYPE_SYSTEM_UNITS,
                keys: Database.DB_TYPE_SYSTEM_UNITS + '.' + type,
                data: data,
            };

            return db.documents.add(item);
        });
    }

    /**
     * Add one form inquiry index record.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    addFormIndexRecord = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = db._addFormIndexRecord(data);
            return db._addDbItem(item);
        });
    }

    /**
     * Get inquiry list.
     *
     * @param params
     * @param params.page {number} Default 1.
     * @param params.formType {string}
     * @param params.status {string}
     * @param params.user_id {string}
     *
     * @returns {Promise}
     */
    getFormIndexRecords = (
        params: {
            page?: number,
            formType?: string | string[],
            status?: string[],
            user_id?: string,
        } =
            {page: -1, formType: "", status: [], user_id: ""}
    ) => {
        // let parameters: { record_type?: string | string[], field_status?: string[], field_region?: string, field_creator?: string, order?: {field_changed?: string} } = {};

        // Find record list and return it.
        return this.transaction('readonly', db.documents, () => {
            // Filter by more significant index first, because only one index can be used in "and" queries.
            let query = db.documents.where('type').equals(Database.DB_TYPE_FORM_RECORD_INDEX);
            // Later filter with JS callback function by type to ensure correct row.
            if (Array.isArray(params.formType) && params.formType.length > 0) {
                if (params.formType) {
                    let typeConditions = params.formType.map(type => Database.DB_TYPE_FORM_RECORD_INDEX+'_' + type);
                    if (typeConditions.length > 0) {
                        let combinedConditions = {
                            type: Database.DB_TYPE_FORM_RECORD_INDEX,
                            or: typeConditions.map(condition => ({keys: condition}))
                        };
                        query.and(row => {
                            return row.type === combinedConditions.type &&
                                combinedConditions.or.some(condition => row.keys === condition.keys);
                        });
                    }
                }
            }

            // if (params.formType !== "") {
            //     query.and(row => row.keys === DB_TYPE_FORM_RECORD_INDEX+'_'+params.formType);
            // }
            // // Optionally filter with JS callback function by name.
            // if (name !== '') {
            //     // Ignore text case.
            //     name = name.toLowerCase();
            //     // Ignore text again here, and to use indexOf() to simulate a SQL like comparator.
            //     query.and(row => row.data.name.toLowerCase().indexOf(name) !== -1);
            // }
            // // Optionally filter with JS callback function by code.
            // if (code.toLowerCase !== undefined && code !== '' && code !== null && code !== undefined) {
            //     // Ignore text case.
            //     code = code.toLowerCase();
            //     // Ignore text again here, and to use indexOf() to simulate a SQL like comparator.
            //     query.and(row => row.data.code && row.data.code.toLowerCase().indexOf(code) !== -1);
            // }

            // Perform filter and sort by name.
            return query.sortBy('data.field_changed').then(items => {
                // if paging is required and there are elements.
                let resp:Array<any> = [];
                if (params.page !== -1) {
                    if (!params.page) {
                        params.page = -1;
                    }
                    if (items.length > 0) {
                        let pageSize = dmGetPageSize();
                        let itemsCount = items.length;
                        let offset: number = (params.page - 1) * pageSize;
                        // items = items.slice(offset, offset + dmGetPageSize());
                        for (let i = 0; i < itemsCount; i++) {
                            if (resp.length >= pageSize) {
                                break;
                            }
                            if (i >= offset) {
                                resp.push(items[i]);
                            }
                        }
                    }
                } else {
                    resp = items;
                }

                return Promise.resolve(resp.map(item => item.data));
            });
        });
    }

    /**
     * Get form index record.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    getFormIndexRecord = (id: string): Promise<any> => {
        const params: Record<string,any> = {
            ulid: id,
            // Redundant filter, the ID is unique.
            type: Database.DB_TYPE_FORM_RECORD_INDEX,
        };
        return db.documents.where(params).first(item => item?.data);
    }

    /**
     * Update one form index record.
     *
     * IMPORTANT: Data must have id & form properties. Where form is the form type.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    updateFormIndexRecord = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item:Document = this._addFormIndexRecord(data);
            return this._updateDbItem(item, item.type);
        });
    }

    /**
     * Alert internal form index records changes.
     *
     * This Must be used after adding, removing or updates.
     *
     * @returns {Promise}
     */
    invalidateAllFormIndexRecords() {
        return db._invalidateAllDataByType(Database.DB_TYPE_FORM_RECORD_INDEX);
    }

    /**
     * Add one key data.
     *
     * @param key {string}
     * @param data {any}
     *
     * @returns {Document}
     */
    _addKey = (key: string, data: any): Document => {
        return {
            ulid: key,
            type: Database.DB_TYPE_FREE_KEY,
            keys: Database.DB_TYPE_FREE_KEY+'_'+key,
            data: data,
        };
    }

    /**
     * Add one key data.
     *
     * @param key {string}
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    addKey = (key: string, data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = this._addKey(key, data);
            return db._addDbItem(item);
        });
    }

    /**
     * Update one key.
     *
     * @param key {string}
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    updateKey = (key: string, data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item:Document = this._addKey(key, data);
            return this._updateDbItem(item, item.type);
        });
    }

    /**
     * Get key data.
     *
     * @param key {string}
     *
     * @returns {Promise<any>}
     */
    getKey = (key: string): Promise<any> => {
        const params: Record<string,any> = {
            ulid: key,
            // Redundant filter, the ID is unique.
            type: Database.DB_TYPE_FREE_KEY,
        };
        return db.documents.where(params).first(item => item?.data);
    }

    /**
     * Get key data that start with.
     *
     * @param keyStart {string}
     *
     * @returns {Promise<any>}
     */
    getKeyThatStartBy = (keyStart: string): Promise<any> => {
        return db.documents
            .where("ulid")
            .startsWith(keyStart)
            .toArray()
            .then((items) => {
                let resp = [];
                for (let i = 0; i < items.length; i++) {
                    let data = items[i];
                    resp.push(data.data);
                }

                return resp;
            });
    }

    /**
     * Invalidate key data.
     *
     * @param key {string}
     *
     * @returns {Promise}
     */
    invalidateKey(key: string) {
        return this.transaction('readwrite', db.documents, () => {
            let query = db.documents;
            let where = query.where("ulid").equals(key);

            return where.delete().then(() => {
                return Promise.resolve([]);
            });
        });
    }

    /**
     * Invalidate all key datas.
     *
     * @returns {Promise}
     */
    invalidateAllKeys() {
        return db._invalidateAllDataByType(Database.DB_TYPE_FREE_KEY);
    }

    /**
     * Get one household process item to write in database.
     *
     * @param data {any}
     *
     * @returns {any}
     */
    _addHouseholdProcess = (data: any): any => {
        return {
            ulid: data.id,
            type: Database.DB_TYPE_HOUSEHOLD_PROCESS,
            keys: Database.DB_TYPE_HOUSEHOLD_PROCESS+'_'+data.id,
            data: data,
        };
    }

    /**
     * Add one household process.
     *
     * @param data {any}
     *
     * @returns {Promise<any>}
     */
    addHouseholdProcess = (data: any): Promise<any> => {
        return this.transaction('rw', db.documents, () => {
            let item = db._addHouseholdProcess(data);
            return db._addDbItem(item);
        });
    }

    /**
     * Get household process list.
     *
     * @param params
     * @param params.page {number} Default 1.
     * @param params.administrativeDivision {string}
     *
     * @returns {Promise}
     */
    getHouseholdPrecesses = (
        params: {
            page?: number,
            administrativeDivision?: string,
        } =
            {page: -1}
    ) => {
        // let parameters: { record_type?: string | string[], field_status?: string[], field_region?: string, field_creator?: string, order?: {field_changed?: string} } = {};

        // Find record list and return it.
        return this.transaction('readonly', db.documents, () => {
            // Filter by more significant index first, because only one index can be used in "and" queries.
            let query = db.documents.where('type').equals(Database.DB_TYPE_HOUSEHOLD_PROCESS);

            // Perform filter and sort by name.
            return query.sortBy('data.id').then(items => {
                // if paging is required and there are elements.
                let resp:Array<any> = [];
                if (params.page !== -1) {
                    if (!params.page) {
                        params.page = -1;
                    }
                    if (items.length > 0) {
                        let pageSize = dmGetPageSize();
                        let itemsCount = items.length;
                        let offset: number = (params.page - 1) * pageSize;
                        // items = items.slice(offset, offset + dmGetPageSize());
                        for (let i = 0; i < itemsCount; i++) {
                            if (resp.length >= pageSize) {
                                break;
                            }
                            if (i >= offset) {
                                resp.push(items[i]);
                            }
                        }
                    }
                } else {
                    resp = items;
                }

                return Promise.resolve(resp.map(item => item.data));
            });
        });
    }

    /**
     * Get household process.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    getHouseholdProcess = (id: string): Promise<any> => {
        const params: Record<string,any> = {
            ulid: id,
            // Redundant filter, the ID is unique.
            type: Database.DB_TYPE_HOUSEHOLD_PROCESS,
        };
        return db.documents.where(params).first(item => item?.data);
    }

    /**
     * Alert internal form index records changes.
     *
     * This Must be used after adding, removing or updates.
     *
     * @returns {Promise}
     */
    invalidateAllHouseholdProcesses() {
        return db._invalidateAllDataByType(Database.DB_TYPE_HOUSEHOLD_PROCESS);
    }

    /**
     * Search DynForm record.
     *
     * @param parameters
     * @param parameters.form_id Form type Id.
     * @param parameters.unique {boolean} Get unique records.
     * @param parameters.filter
     * @param parameters.order
     * @param parameters.page {number} First page to get, if -1 then don't download other pages.
     * @param parameters.autopagination
     * @param parameters.prevData
     * @param parameters.record Querier original filter.
     *
     * @returns {Promise}
     */
    searchFormRecords(
        parameters: {
            form_id: string,
            unique?: boolean,
            filter?: any,
            order?: any,
            page?: number,
            autopagination?: boolean,
            prevData?: any,
            record: DynFormRecordInterface,
        }
    ) {
        // Default parameters.
        if (typeof parameters.unique === "undefined") {parameters.unique = false;}
        if (typeof parameters.filter === "undefined") {parameters.filter = {};}
        if (typeof parameters.order === "undefined") {parameters.order = {};}
        if (typeof parameters.page === "undefined") {parameters.page = 1;}
        if (typeof parameters.autopagination === "undefined") {parameters.autopagination = false;}
        if (typeof parameters.prevData === "undefined") {parameters.prevData = [];}
        let query = db.documents;
        let where = query.where("type").equals(Database.DB_TYPE_FORM_RECORD);
        where.and(document => {
            return document.keys === Database.DB_TYPE_FORM_RECORD + '_' + parameters.form_id;
        });
        switch (parameters.form_id) {
            case 'form.tap':
            case 'form.community':
            case 'form.health.care':
            case 'form.school':
            case 'form.wsprovider':
            case 'form.wssystem':
                if (!parameters.filter["field_deleted"]) {
                    where.and(document => {
                        return document.data.field_deleted.value === "0" || document.data.field_deleted.value === false;
                    });
                }
                break;
        }

        for (const filterKey in parameters.filter) {
            where.and(document => {
                if (!parameters.filter[filterKey]) {
                    return true;
                }
                if (!document.data[filterKey]) {
                    return true;
                }
                // todo Allow filter with operator "like". Ex. filter name "field_provider_name[like]".
                return document.data[filterKey].value === parameters.filter[filterKey].toString();
            });
        }

        let lastSort;
        for (const filterKey in parameters.order) {
            lastSort = where.sortBy('data.'+filterKey);
        }
        if (!lastSort) {
            lastSort = where.sortBy('data.id');
        }

        // Perform filter and sort by name.
        return lastSort.then(items => {
            // if paging is required and there are elements.
            let data:Array<any> = [];
            if (!parameters.autopagination) {
                if (items.length > 0) {
                    let pageSize = dmGetPageSize();
                    let itemsCount = items.length;
                    let offset: number = ((parameters.page || 1) - 1) * pageSize;
                    for (let i = 0; i < itemsCount; i++) {
                        if (data.length >= pageSize) {
                            break;
                        }
                        if (i >= offset) {
                            data.push(items[i]);
                        }
                    }
                }
            } else {
                data = items;
            }
            let resp = {data: data.map(item => item.data), filter: parameters.record};
            return Promise.resolve(resp);
        });
    }

    /**
     * Add data to indexDB doing generic actions.
     *
     * @param item
     *
     * @returns {Promise}
     */
    _addDbItem(item: Document) {
         let msgId:string = `[${item.ulid} / ${item.keys}]`;

        return db.documents
            .add(item)
            .then((index) => {
                return item;
            })
            .catch((reason:any) => {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.ERROR,
                        title: t('Add Error'),
                        message: msgId + " " + reason,
                        isPublic: true,
                    }
                );
            });
    }

    /**
     * Update database item.
     *
     * @param newItem
     * @param type
     *
     * @returns {Promise}
     */
    _updateDbItem = (newItem: any, type: string): Promise<any> => {
        const params: Record<string,any> = {
            ulid: newItem.ulid,
            // Redundant filter, the ID is unique.
            type: type,
        };

        return db.documents
            .where(params)
            .first(item => item)
            .then((oldItemBlock:any) => {
                if (oldItemBlock) {
                    oldItemBlock.data = newItem.data;
                    return db.documents.put(oldItemBlock)
                        .then((resp) => {
                            return newItem.data;
                        });
                } else {
                    console.warn('[_updateDbItem] Record not found:', newItem);
                    return newItem.data;
                }
            });
    }

}

export const db = new Database();
