/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Use an enum to keep track of events similar to action types set as variables in redux.
 * Force to know all possible events in this application.
 */
export enum Events {
    // This app enters in maintenance mode. Emitted in api.ts.
    APP_MAINTENANCE = 'app.maintenance',
    // Session Login start.
    LOGIN_START = 'login.start',
    // Session Logout complete.
    LOGIN_COMPLETE = 'login.complete',
    // Add a new i18n literal to remote API.
    I18N_ADD_LITERAL = 'i18n.add.literal',
    // Display status console.
    STATUS_SHOW = 'status.show',
    // Add a new status event.
    STATUS_ADD = 'status.add',
    // A component was mounted.
    COMP_MOUNT = 'comp.mount',
    // a new household survey was succesfully created
    HOUSEHOLD_EVENT = 'household.event',
    // Cards container layout changed
    CARDS_LAYOUT_CHANGED = 'cards.layout.changed',
    // Cards container layout changed
    POINT_TEAM_CHANGED = 'point.team.changed',
}

export enum HouseholdEvents {
    SURVEY_ADDED = "survey.added",
    SURVEY_DELETED = "survey.deleted",
    PROCESS_CLOSED = "process.closed",
    FILTER_SURVEYS = "filter.surveys",
}

/**
 * Use an enum to keep track of status event levels.
 * Force to know all possible status events levels in this application.
 */
export enum StatusEventLevel {
    ERROR = 'error',
    WARNING = 'warning',
    SUCCESS = 'success',
    INFO = 'info',
    DEBUG = 'debug',
}

export interface StatusEvent {
    time?: string;
    level: StatusEventLevel;
    title?: string,
    message: string;
    isPublic: boolean;
}

/**
 * Allow use events to manage states.
 *
 * @see https://lolahef.medium.com/react-event-emitter-9a3bb0c719
 */
export const eventManager = {
    _events: {},
    /**
     * Throw an event.
     *
     * @param event
     * @param data
     */
    dispatch(event: Events, data?: any) {
        if (!this._events[event]) return;
        this._events[event].forEach((callback: (arg0: any) => any) => callback(data))
    },

    /**
     * Register a new event handler.
     *
     * @param event {Events}
     *   Event id.
     * @param callback {(data: any) => any}
     *   Event callback function.
     */
    on(event: Events, callback: (data: any) => any) {
        if (!this._events[event]) this._events[event] = [];
        this._events[event].push(callback);
    },

    /**
     * Unregister event handler.
     *
     * @param event {Events}
     *   Event id.
     */
    remove(event: Events) {
        if (!this._events[event]) return;
        delete this._events[event];
    }
}
