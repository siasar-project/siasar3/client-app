/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import axiosBase from "axios";
import router from "next/router";
import {UserLogin} from "@/objects/User";
import {AdministrativeDivision} from "@/objects/AdministrativeDivision";
import {SchoolInterface} from "@/objects/School";
import {JwtHelper} from "../utils/JwtHelper";
import {
    smClearSession,
    smGetRefreshToken,
    smGetSession,
    smGetToken,
    smIsValidSession,
    smSetSession
} from "../utils/sessionManager";
import {eventManager, Events, StatusEventLevel} from '../utils/eventManager';
import t from "@/utils/i18n";
import {InquiryFormLog} from "@/objects/InquiryFormLog";
import {HostAvailable} from "@/objects/HostAvailable";
import {ReplanPointInterface} from "@/objects/ReplanPointInterface";

let BASE_URL = process.env.NEXT_PUBLIC_SIASAR3_API_HOST;
// Get host from localStorage.
let storedHost = typeof window !== 'undefined' ? localStorage.getItem('current_host') : null;
if (storedHost) {
    BASE_URL = storedHost;
}
export const API_PAGE_SIZE:number = 24;

const axios = axiosBase.create({
    baseURL: BASE_URL,
});

/**
 * Disable API cache.
 *
 * Axios' calls don't must add catch's.
 *
 * @see https://thewebdev.info/2021/11/18/how-to-disable-browser-cache-with-javascript-axios/
 */
axios.defaults.headers = {
    'Cache-Control': 'no-cache',
    'Pragma': 'no-cache',
    'Expires': '0',
    'accept': 'application/json',
};

/**
 * Update each query to add authentication.
 */
axios.interceptors.request.use(
function (config) {
    // Use alternative API URL if defined.
    config.baseURL = Api.getCurrentHost();

    // If active, add debug mode to API calls.
    // To update you must rebuild the app.
    if (process.env.NEXT_PUBLIC_SIASAR3_API_DEBUG === '1') {
        if (!config.params) {
            config.params = {};
        }
        config.params["XDEBUG_SESSION"] = `siasar`;
    }

    // Add session token to config.
    if (!smIsValidSession()) {
        // Session expired or not valid.
        Promise.reject('Invalid session!');
    }
    const token = smGetToken();

    if (config.url === Api.URL_LOGIN) {
        return config;
    }
    if (token && token !== "") {
        config["headers"]["Authorization"] = `Bearer ${token}`;
    }
    config["headers"]["Content-Type"] = "application/json";
    return config;
},
function (error) {
    // Cancel query.
    return Promise.reject(error);
}
);

/**
 * Preprocess any API response and try to recover API errors.
 */
axios.interceptors.response.use(
function (response) {
    // Allow the current response.
    return response;
},
function (error) {
    const originalRequest = error.config;
    if (error?.response?.status === 503) {
        // Maintenance mode.
        eventManager.dispatch(Events.APP_MAINTENANCE, {});
        console.log('The site is in maintenance mode.');
        return Promise.reject('The site is in maintenance mode.');
    }
    // Is an error status in the login end-point?
    if (error?.response?.status === 401 && originalRequest.url === Api.URL_LOGIN) {
        // Yes, go to login page.
        smClearSession();
        router.push('/login');
        return;
    }
    // Is it the first query error?
    if (error?.response?.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;
        return Api.login(undefined, originalRequest)
            .catch((error) => {
                return Promise.reject(error);
            });
    }
    return Promise.reject(error);
}
);

/**
 * Remote API client.
 */
class Api {
    static SCHOOL_ID = "form.schools";
    static SCHOOL_CONTACT_ID = "form.school.contacts";
    static FORM_WSSYSTEM_ID = "form.wssystem";
    static FORM_WSPROVIDER_ID = "form.wsprovider";
    static FORM_COMMUNITY_ID = "form.community";

    static URL_PING = "/api/v1/configuration/ping";
    static URL_I18N = "/api/v1/configuration/i18n/";
    static URL_I18N_ADD = "/api/v1/configuration/i18n/add";
    static URL_STATS = "/api/v1/configuration/stats";
    static URL_PARAMETRIC_TEMPLATE = "/api/v1/configuration/templates";
    static URL_PARAMETRIC_IMPORT = "/api/v1/configuration/import";
    static URL_USERS = "/api/v1/users";
    static URL_LOGIN = "/api/v1/users/login";
    static URL_FILES = "/api/v1/files";
    static URL_FORM_DATA = "/api/v1/form/data/";
    static URL_USERS_ME = "/api/v1/users/me";
    static URL_MATERIALS = "/api/v1/materials";
    static URL_FUNCTIONS_CARRIED_OUT_TAPS = "/api/v1/functions_carried_out_taps";
    static URL_FUNCTIONS_CARRIED_OUT_WSPS = "/api/v1/functions_carried_out_wsps";
    static URL_GEOGRAPHICAL_SCOPE = "/api/v1/geographical_scopes";
    static URL_SPECIAL_COMPONENTS = "/api/v1/special_components";
    static URL_DEFAULT_DIAMETERS = "/api/v1/default_diameters";
    static URL_DISTRIBUTION_MATERIAL = "/api/v1/distribution_materials";
    static URL_USERS_IN_COUNTRY = "/api/v1/users/country/";
    static URL_FORM_STRUCTURE = "/api/v1/form/structure";
    static URL_FORM_STRUCTURE_EMPTY = "/api/v1/form/structure/empty";
    static URL_FORM_INQUIRES = "/api/v1/form/data/inquiries";
    static URL_FORM_POINTS = "/api/v1/form/data/form.points";
    static URL_INQUIRY_FORM_LOGS = "/api/v1/inquiry_form_logs";
    static URL_POINT_FORM_LOGS = "/api/v1/point_logs";
    static URL_COUNTRIES = "/api/v1/countries";
    static URL_ETHNICITIES = "/api/v1/ethnicities";
    static URL_ADMINISTRATIVE_DIVISIONS = "/api/v1/administrative_divisions";
    static URL_ADMINISTRATIVE_DIVISIONS_BY_COUNTRY = "/api/v1/administrative_divisions/country/";
    static URL_ADMINISTRATIVE_DIVISION_TYPES = "/api/v1/administrative_division_types";
    static URL_INSTITUTION_INTERVENTIONS = "/api/v1/institution_interventions";
    static URL_WATER_QUALITY_ENTITIES = "/api/v1/water_quality_entities";
    static URL_TYPE_INTERVENTIONS = "/api/v1/type_interventions";
    static URL_COMMUNITY_SERVICES = "/api/v1/community_services";
    static URL_TREATMENT_TECHNOLOGY_COAG_FLOCCUS = "/api/v1/treatment_technology_coag_floccus";
    static URL_TREATMENT_TECHNOLOGY_SEDIMENTATIONS = "/api/v1/treatment_technology_sedimentations";
    static URL_DISINFECTING_SUBSTANCE = "/api/v1/disinfecting_substances";
    static URL_FUNDER_INTERVENTIONS = "/api/v1/funder_interventions";
    static URL_PROGRAM_INTERVENTIONS = "/api/v1/program_interventions";
    static URL_CURRENCIES = "/api/v1/currencies";
    static URL_TYPOLOGY_CHLORINATION_INSTALLATIONS = "/api/v1/typology_chlorination_installations";
    static URL_TYPE_TAPS = "/api/v1/type_taps";
    static URL_WATER_QUALITY_INTERVENTION_TYPES = "/api/v1/water_quality_intervention_types";
    static URL_TYPE_HEALTHCARE_FACILITIES = "/api/v1/type_healthcare_facilities";
    static URL_INTERVENTION_STATUSES = "/api/v1/intervention_statuses";
    static URL_PUMP_TYPES = "/api/v1/pump_types";
    static URL_TECHNICAL_ASSISTANCE_PROVIDERS = "/api/v1/technical_assistance_providers";
    static URL_CONFIGURATION = "/api/v1/configuration";
    static URL_OFFICIAL_LANGUAGES = "/api/v1/official_languages";
    static URL_USER_LANGUAGES = "/api/v1/languages";
    static URL_SYSTEM_ROLES = "/api/v1/configuration/system.role";
    static URL_SYSTEM_PERMISSIONS = "/api/v1/configuration/system.permissions";
    static URL_TREATMENT_TECHNOLOGY_FILTRATION = "/api/v1/treatment_technology_filtrations"
    static URL_TREATMENT_TECHNOLOGY_AERATION_OXIDATION = "/api/v1/treatment_technology_aera_oxidas"
    static URL_TREATMENT_TECHNOLOGY_DESALINATION = "/api/v1/treatment_technology_desalinations"
    static URL_HOUSEHOLD_PROCESSES = "/api/v1/household_processes"
    static URL_MUTATEBLE="/api/v1/configuration/mutatebles";
    static URL_VERSION="/api/v1/configuration/version";

    /**
     * Get pluralized name.
     *
     * To forms URLs only add a "s" at end.
     *
     * @param str
     * @returns {string}
     */
    static getPlural = (str: string) => {
        if (str.startsWith("form.")) return str+"s";
        if (
            str.endsWith("s") ||
            str.endsWith("sh") ||
            str.endsWith("ch") ||
            str.endsWith("x") ||
            str.endsWith("z")
        ) {
            return str + "es";
        } else if (str.endsWith("y")) {
            return str.substring(0, str.length - 1) + "ies";
        } else {
            return str + "s";
        }
    };

    /**
     * Get available host list.
     *
     * @returns {HostAvailable[]}
     */
    static getAvailableHosts = ():HostAvailable[] => {
        let hostLists: any = JSON.parse(process.env.NEXT_PUBLIC_SIASAR3_API_HOST_LOCALIZED ?? '[]');
        let resp:HostAvailable[] = [];

        for (const hostListsKey in hostLists) {
            let item: HostAvailable = {
                label: hostListsKey,
                url: hostLists[hostListsKey],
            };
            resp.push(item);
        }

        return resp;
    }

    /**
     * Get current host.
     *
     * @returns {string}
     */
    static getCurrentHost = ():string => {
        let storedHost = typeof window !== 'undefined' ? localStorage.getItem('current_host') : null;
        // console.log('storedHost', storedHost);
        BASE_URL = process.env.NEXT_PUBLIC_SIASAR3_API_HOST;
        if (null !== storedHost) {
            BASE_URL = storedHost;
        }
        return BASE_URL ?? '??';
    }

    /**
     * Change current host.
     *
     * @param host {string} The new host.
     */
    static moveToHost = (host: string) => {
        if ('' === host) {
            host = process.env.NEXT_PUBLIC_SIASAR3_API_HOST ?? '';
        }
        BASE_URL = host;
        localStorage.setItem('current_host', host);
    }

    /**
     * Get form data.
     *
     * @param form_id Form type Id.
     * @param record_id Record Id.
     * @param page
     * @param prevData
     * @returns {Promise}
     */
    static getFormRecord = (form_id: string, record_id: string = "", page: number = 1, prevData: any = []): any => {
        let url = Api.URL_FORM_DATA + Api.getPlural(form_id) + "/" + record_id;
        const params: Record<string,any> = {
            page: page,
            field_deleted: "0",
        };
        return axios.get(url, params).then((res) => {
            prevData = prevData.concat(res.data);
            // Recursive load.
            let dataLen = res.data.length;
            if (dataLen > 0) {
                return Api.getFormRecord(form_id, record_id, page + 1, prevData);
            }
            return prevData;
        });
    };

    /**
     * Get unread mails count.
     *
     * @returns {Promise}
     */
    static getUnreadMailCount = ():any => {
        return axios.get(Api.URL_FORM_DATA + Api.getPlural('form.mail') + "/count_unread")
            .then((res) => {
                return res?.data.count;
            })
            .catch((info) => {
                return Promise.reject(info);
            });
    }

    /**
     * Search form data.
     *
     * @param parameters
     * @param parameters.form_id Form type Id.
     * @param parameters.unique {boolean} Get unique records.
     * @param parameters.filter
     * @param parameters.order
     * @param parameters.page {number} First page to get, if -1 then don't download other pages.
     * @param parameters.autopagination
     * @param parameters.prevData
     *
     * @returns {Promise}
     */
    static searchFormRecords = (
        parameters: {
            form_id: string,
            unique?: boolean,
            filter?: any,
            order?: any,
            page?: number,
            autopagination?: boolean,
            prevData?: any,
        }
    ): any => {
        // Default parameters.
        if (undefined === parameters.unique) {parameters.unique = false;}
        if (undefined === parameters.filter) {parameters.filter = {};}
        if (undefined === parameters.order) {parameters.order = {};}
        if (undefined === parameters.page) {parameters.page = 1;}
        if (undefined === parameters.autopagination) {parameters.autopagination = false;}
        if (undefined === parameters.prevData) {parameters.prevData = [];}

        let url = Api.URL_FORM_DATA + Api.getPlural(parameters.form_id);
        let originalFilter = JSON.parse(JSON.stringify(parameters.filter));
        // for (const filterKey in parameters.filter) {
        //     parameters.filter[filterKey+'[like]'] = parameters.filter[filterKey];
        //     parameters.filter[filterKey] = undefined;
        // }
        switch (parameters.form_id) {
            case 'form.tap':
            case 'form.community':
            case 'form.health.care':
            case 'form.school':
            case 'form.wsprovider':
            case 'form.wssystem':
                if (!parameters.filter["field_deleted"]) {
                    parameters.filter["field_deleted"] = 0;
                }
                break;
        }
        const params: Record<string,any> = parameters.filter;

        for (const filterKey in parameters.order) {
            params["order["+filterKey+"]"] = parameters.order[filterKey];
        }

        if (!parameters.autopagination) {
            params.page = parameters.page;
        }

        if (parameters.unique) {
            params.unique = parameters.unique;
        }

        return axios.get(url, {params: params}).then((res) => {
            parameters.prevData = parameters.prevData.concat(res.data);
            if (!parameters.autopagination) {
                // Stop auto-pagination and return results.
                return parameters.prevData;
            }
            // Recursive load.
            let dataLen = res.data.length;
            if (dataLen > 0) {
                return Api.searchFormRecords(
                    {
                        form_id: parameters.form_id,
                        unique: parameters.unique,
                        filter: originalFilter,
                        order: parameters.order,
                        page: parameters.page ? parameters.page + 1 : 1,
                        autopagination: parameters.autopagination,
                        prevData: parameters.prevData,
                    }
                );
            }
            return parameters.prevData;
        })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Insert form record.
     *
     * @param form_id
     * @param data The new record content.
     * @returns {Promise}
     */
    static addFormRecord = (form_id: string, data: any) => {
        return axios.post(Api.URL_FORM_DATA + Api.getPlural(form_id), data)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Update form record.
     *
     * @param form_id
     * @param record_id
     * @param data The new record content.
     * @returns {Promise}
     */
    static updateFormRecord = (form_id: string, record_id: string, data: any) => {
        return axios.put(Api.URL_FORM_DATA + Api.getPlural(form_id) + "/" + record_id, data)
                .then((res) => {
                    return res;
                })
                .catch((info) => {
                    return Api.displayErrorMessage(info);
                });
    };

    /**
     * Clone form record.
     *
     * @param form_id
     * @param record_id
     * @returns {Promise}
     */
    static cloneFormRecord = (form_id: string, record_id: string) => {
        return axios.put(Api.URL_FORM_DATA + Api.getPlural(form_id) + "/" + record_id + "/clone")
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * History of form record.
     *
     * @param form_id
     * @param record_id
     * @returns {Promise}
     */
    static getHistoryFormRecord = (form_id: string, record_id: string) => {
        return axios.get(Api.URL_FORM_DATA + Api.getPlural(form_id) + "/" + record_id + "/history")
            .then((res) => {
                return res.data;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Delete a form record.
     *
     * @param form_id Form ID.
     * @param id      Record ID.
     * @returns {Promise}
     */
    static removeFormRecord = (form_id: string, id: string) => {
        return axios.delete(Api.URL_FORM_DATA + Api.getPlural(form_id) + "/" + id)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get form data.
     *
     * @param page {number} List page number, default 1.
     * @param parameters {any}
     * @returns {Promise}
     */
    static getFormInquires = async (page: number = 1, parameters: any = {}) => {
        let url = Api.URL_FORM_INQUIRES;
        let config = {
            params: {
                page: page,
                ...parameters,
            },
        };

        return axios.get(url, config)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get form data.
     *
     * @param country {string} Country ID, code, without IRI.
     * @param page {number} List page number, default 1.
     * @param parameters {any}
     * @param allPages {boolean}
     * @param prevData {any}
     *
     * @returns {Promise}
     */
    static getFormPoints = async (country: string, page: number = 1, parameters: any = {}, allPages: boolean = false, prevData: any = []): Promise<any> => {
        let url = Api.URL_FORM_POINTS;
        if (-1 === page) {
            allPages = true;
            page = 1;
        }
        let config = {
            params: {
                page: page,
                country: country,
                ...parameters,
            },
        };

        return axios.get(url, config)
            .then((res) => {
                prevData = prevData.concat(res.data);
                if (allPages) {
                    // Recursive load.
                    let dataLen = res.data.length;
                    if (dataLen > 0) {
                        return Api.getFormPoints(country, page + 1, parameters, allPages, prevData);
                    }
                    return prevData;
                }
                // Paginated response.
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Add a new SIASAR Point.
     *
     * @param division {string} The initial division IRI.
     * @returns {Promise}
     */
    static addFormPoints = async (division: string) => {
        return axios.post(Api.URL_FORM_POINTS, {administrative_division: division})
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add a new WSP to SIASAR Point.
     *
     * @param id {string} Point ID.
     * @param data {any} Any init form data.
     * @returns {Promise}
     */
    static addFormWspToPoint = async (id: string, data: any) => {
        return axios.post(Api.URL_FORM_POINTS+'/'+id+'/form.wsprovider', data)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add a new Water system to SIASAR Point.
     *
     * @param id {string} Point ID.
     * @param data {any} Any init form data.
     * @returns {Promise}
     */
    static addFormWSystemToPoint = async (id: string, data: any) => {
        return axios.post(Api.URL_FORM_POINTS+'/'+id+'/form.wssystem', data)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Re-plan a SIASAR point to other SIASAR Point.
     *
     * @param id {string} Point ID.
     * @param data {ReplanPointInterface} Any init form data.
     * @returns {Promise}
     */
    static replanPoint = async (id: string, data: ReplanPointInterface) => {
        return axios.put(Api.URL_FORM_POINTS+'/'+id+'/replan', data)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete Water Supply System information.
     * 
     * @param pid {string} Point Record ID.
     * @param sid {string} WSS ID.
     * 
     * @returns {Promise}
     */
     static removeWSSystem = (pid: string, sid: string) => {
        return axios.delete(Api.URL_FORM_POINTS + "/" + pid + "/" + Api.FORM_WSSYSTEM_ID + "/" + sid)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Delete Community information.
     *
     * @param pid {string} Point Record ID.
     * @param cid {string} Community ID.
     *
     * @returns {Promise}
     */
    static removeCommunity = (pid: string, cid: string) => {
        return axios.delete(Api.URL_FORM_POINTS + "/" + pid + "/" + Api.FORM_COMMUNITY_ID + "/" + cid)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Delete Community households information.
     *
     * @param pid {string} Point Record ID.
     * @param cid {string} Community ID.
     *
     * @returns {Promise}
     */
    static removeCommunityHouseholds = (pid: string, cid: string) => {
        return axios.delete(Api.URL_FORM_POINTS + "/" + pid + "/" + Api.FORM_COMMUNITY_ID + "/" + cid + "/households")
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Delete Water Service Provider information.
     *
     * @param pid {string} Point Record ID.
     * @param wspid {string} WSP ID.
     *
     * @returns {Promise}
     */
    static removeWSProvder = (pid: string, wspid: string) => {
        return axios.delete(Api.URL_FORM_POINTS + "/" + pid + "/" + Api.FORM_WSPROVIDER_ID + "/" + wspid)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get SIASAR Point.
     *
     * @param id {string} Point ID.
     */
    static getFormPoint = async (id: string) => {
        return axios.get(Api.URL_FORM_POINTS+"/"+id, {})
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get SIASAR Point Inquiries.
     *
     * @param id {string} Point ID.
     */
    static getFormPointInquiries = async (id: string) => {
        return axios.get(Api.URL_FORM_POINTS+"/"+id+"/inquiries", {})
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update form point.
     *
     * @param record_id
     * @param data The new record content.
     * @returns {Promise}
     */
    static updateFormPoint = (record_id: string, data: any) => {
        return axios.put(Api.URL_FORM_POINTS + "/" + record_id, data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Clone form point.
     *
     * Clone a calculated point to a new point.
     *
     * @param record_id
     * @returns {Promise}
     */
    static cloneFormPoint = (record_id: string) => {
        return axios.put(Api.URL_FORM_POINTS + "/" + record_id + "/clone")
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Delete a form record.
     *
     * @param id      Record ID.
     * 
     * @returns {Promise}
     */
     static removeFormPoint = (id: string) => {
        return axios.delete(Api.URL_FORM_POINTS + "/" + id)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get form point history.
     *
     * @param pid {string} Point ID.
     * @param page {number} List page number, default 1.
     * @param parameters {any}
     * @returns {Promise}
     */
    static getFormPointHistory = async (pid: string, page: number = 1, parameters: any = {}) => {
        let url = Api.URL_FORM_POINTS+"/"+pid+"/history";
        let config = {
            params: {
                page: page,
                ...parameters,
            },
        };

        return axios.get(url, config)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Login user
     *
     * @param user UserLogin
     * @param okReturn
     * @returns {Promise}
     */
    static login = (user?: UserLogin, okReturn?: any): Promise<any> => {
        let payload: UserLogin = user ?? {refresh_token: smGetRefreshToken()};
        let isRefresh = (!user);
        return axios
            .post(Api.URL_LOGIN, payload)
            .then((res) => {
                if (res === undefined) {
                    return Promise.reject('Empty response');
                }
                if (res.status === 200) {
                    let token_data = JwtHelper.decodeToken(res.data.token);
                    let currentUser: any = {
                        id: token_data.id,
                        active: true,
                        roles: token_data.roles,
                        username: token_data.username,
                        gravatar: token_data.gravatar,
                        administrativeDivision: [],
                        permissions: [],
                        language: token_data.language_id,
                        timezone: token_data.timezone,
                        session: {
                            refresh_token: res.data.refresh_token,
                            refresh_token_expire: res.data.refresh_token_expire,
                            token: res.data.token,
                            token_expire: res.data.token_expire,
                        },
                        country: {
                            id: token_data.country.code,
                            code: token_data.country.code,
                            divisionTypes: token_data.country.divisionTypes,
                        },
                    };
                    if (isRefresh) {
                        // While refreshing token only change the session tokens.
                        currentUser = smGetSession();
                        currentUser.session = {
                            refresh_token: res.data.refresh_token,
                            refresh_token_expire: res.data.refresh_token_expire,
                            token: res.data.token,
                            token_expire: res.data.token_expire,
                        }
                    }
                    smSetSession(currentUser);
                    if (okReturn !== undefined) {
                        return axios(okReturn);
                    }
                }
            });
    };

    /**
     * Ping server.
     *
     * @returns {Promise}
     */
    static pingServer = () => {
        return axios.get(Api.URL_PING)
            .then((res) => {
                if (res.data.maintenance_mode) {
                    console.log('res.data.maintenance_mode', res.data.maintenance_mode);
                    // Maintenance mode.
                    eventManager.dispatch(Events.APP_MAINTENANCE, {});
                }
                return res.data;
            });
    };

    /**
     * Get i18n literals.
     *
     * @param isoCode {string}
     *
     * @returns {Promise}
     */
    static getI18n = (isoCode: string): Promise<any> => {
        return axios.get(Api.URL_I18N+isoCode)
            .then((res) => {return res.data;})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get system stats.
     *
     * @returns {Promise}
     */
    static getStats = (): Promise<any> => {
        return axios.get(Api.URL_STATS)
            .then((res) => {return res.data;})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Add i18n literals.
     *
     * @param literal {string}
     *
     * @returns {Promise}
     */
    static addI18nLiteral = (literal: string): Promise<any> => {
        let config = {
            params: {
                context: "SIASAR front app",
            },
        };
        const body = {literal: literal};
        return axios
            .post(Api.URL_I18N_ADD, body, config)
            .then((data: any) => data)
            .catch((onReject) => {});
    };

    /**
     * Get user list.
     *
     * @returns {Promise}
     */
    static getUserList = () => {
        let user = smGetSession();
        if (!user || !user.country) {
            return new Promise((resolve) => resolve([]));
        }
        let url = Api.URL_USERS_IN_COUNTRY + user.country.code;
        const config = {};

        return axios.get(url, config)
            .then((res) => res?.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get user roles.
     *
     * @returns {Promise}
     */
    static getRoles = () => {
        return axios.get(Api.URL_SYSTEM_ROLES)
            .then(response => response.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get user roles.
     *
     * @returns {Promise}
     */
    static getSystemPermissions = () => {
        return axios.get(Api.URL_SYSTEM_PERMISSIONS)
            .then(response => response.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get form structure.
     *
     * @param form
     * @param options
     * @returns {Promise}
     */
    static getStructure = (form?: string, options: any = {}) => {
        let url = Api.URL_FORM_STRUCTURE;

        const config = {
            params: {
                ...options
            },
        };

        if (form) {
            url = url + "/" + form;
        }

        return axios.get(url, config)
            .then((res) => {
                return res?.data
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get form empty record.
     *
     * @param form ID
     * @returns {Promise}
     */
    static getEmptyFormRecord = (form?: string) => {
        let url = Api.URL_FORM_STRUCTURE_EMPTY;

        const config = {};

        if (form) {
            url = url + "/" + form;
        }

        return axios.get(url, config)
            .then((res) => {
                return res?.data
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get file info.
     *
     * @param id
     * @returns {Promise}
     */
    static getFile = (id: string) => {
        return axios.get(Api.URL_FILES + '/' + id)
            .then(res => res.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove file.
     *
     * @param id
     * @returns {Promise}
     */
    static removeFile = (id: string) => {
        return axios.delete(Api.URL_FILES + '/' + id)
            .then(res => res.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Upload file
     *
     * @param country
     * @param file
     * @returns {Promise}
     */
    static uploadFile = (country: string, file: File): Promise<any> => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("country", country);
        return axios
            .post(Api.URL_FILES, formData)
            .then((d: { data: { id: string } }) => ({
                id: d.data.id,
                fileName: file.name,
            }))
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get file url
     *
     * @param id
     * @param withBaseUrl
     * @returns {string}
     */
    static getFileUrl = (id: string, withBaseUrl: boolean = true): string => {
        return (withBaseUrl ? BASE_URL : '') + Api.URL_FILES + '/' + id + '/download';
    }

    /**
     * Get file url
     *
     * @param entityName
     * @param withBaseUrl
     *
     * @returns {string}
     */
    static getParametricTemplateUrl = (entityName: string, withBaseUrl: boolean = true): string => {
        return (withBaseUrl ? BASE_URL : '') + Api.URL_PARAMETRIC_TEMPLATE + '/' + entityName;
    }

    /**
     * Massive parametric import.
     *
     * @param entityName
     * @param file
     *
     * @returns {Promise}
     */
    static uploadImportParametric = (entityName: string, file: File) => {
        const formData = new FormData();
        formData.append("file", file);
        return axios
            .post(Api.URL_PARAMETRIC_IMPORT + '/' + entityName, formData)
            .then((data: any) => {
                return data.data;
            })
            .catch((info) => {
                return Promise.reject(info);
            });
    }

    /**
     * Get configuration units.
     *
     * @param config Unit type.
     * @returns {Promise}
     */
    static getUnits = async (config: string) => {
        if (isMeasurementSystemUnitType(config)) {
            return axios
                .get(`${Api.URL_CONFIGURATION}/${config}`)
                .then((res) => res.data)
                .catch((info) => {
                    // Dispatch status event.
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.ERROR,
                            title: t("Error @code", {"@code": info.response.status}),
                            message: info.response.data.message,
                            isPublic: true,
                        }
                    );
                    return Promise.reject(info);
                });
        }
        // Dispatch status event.
        eventManager.dispatch(
            Events.STATUS_ADD,
            {
                level: StatusEventLevel.ERROR,
                title: "API getUnits error",
                message: "API not found '"+config+"' units",
                isPublic: false,
            }
        );
        return [];
    };

    /**
     * Get user languages.
     *
     * @returns {Promise}
     */
    static getUserLanguages = async () => {
        return axios
                .get(`${Api.URL_USER_LANGUAGES}`)
                .then((res) => res.data)
                .catch((info) => {
                    // Dispatch status event.
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.ERROR,
                            title: t("Error @code", {"@code": info.response.status}),
                            message: info.response.data.message,
                            isPublic: true,
                        }
                    );
                    return Promise.reject(info);
                });
    };

    /**
     * @deprecated
     *
     * @param id
     * @returns {Promise}
     */
    getSchool = (id: string) => {
        const config = {
            params: {id},
        };
        return axios.get(Api.URL_FORM_DATA + Api.SCHOOL_ID, config).then((res) => res.data);
    };

    /**
     * @deprecated
     *
     * @param id
     * @param data
     * @returns {Promise}
     */
    updateSchool = (id: string, data: SchoolInterface) => {
        const params = {
            params: {id},
        };
        // todo This config may be that don't work.
        return axios.put(Api.URL_FORM_DATA + Api.SCHOOL_ID + "/" + id, data, {params});
    };

    /**
     * @deprecated
     *
     * @param id
     * @returns {Promise}
     */
    removeSchool = (id: string) => {
        return axios.delete(Api.URL_FORM_DATA + Api.SCHOOL_ID + "/" + id);
    };

    /**
     * @deprecated
     *
     * @param id
     * @returns {Promise}
     */
    getSchoolContact = (id: string) => {
        const config = {
            params: {id},
        };
        return axios
            .get(Api.URL_FORM_DATA + Api.SCHOOL_CONTACT_ID, config)
            .then((res) => res.data);
    };

    /**
     * Get countries list.
     *
     * @param name {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getCountries = (name: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        return axios
            .get(Api.URL_COUNTRIES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getCountries(name, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get country data.
     *
     * @param code
     * @returns {Promise}
     */
    static getCountry = (code: string) => {
        return axios.get(`${Api.URL_COUNTRIES}/${code}`)
            .then((res) => {
                return res.data;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Update country data.
     *
     * @param code
     * @param data
     * @returns {Promise}
     */
    static updateCountry = (code: string, data: any) => {
        return axios.put(`${Api.URL_COUNTRIES}/${code}`, data)
            .then(res => {
                return res.data;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get registered users list.
     *
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getUsers = (page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        return axios
            .get(Api.URL_USERS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getUsers(page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }
   
    /**
     * Get inquiry form logs.
     *
     * @param page {number} List page number, default 1.
     * @param parameters {any}
     * @returns {Promise}
     */
     static getInquiryFormLogs = (page: number = 1, parameters: any = {}) => {
        let url = Api.URL_INQUIRY_FORM_LOGS + '?order%5Bmessage%5D=asc&order%5BcreatedAt%5D=desc';
        let config = {
            params: {
                page: page,
                ...parameters,
            },
        };

        return axios.get(url, config)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * add new inquiry form logs.
     *
     * @param inquiryFormLog {InquiryFormLog}
     * 
     * @returns {Promise}
     */
     static addInquiryFormLog = (inquiryFormLog: InquiryFormLog) => {
        return axios.post(Api.URL_INQUIRY_FORM_LOGS, inquiryFormLog)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get point form logs.
     *
     * @param page {number} List page number, default 1.
     * @param parameters {any}
     * @returns {Promise}
     */
    static getPointFormLogs = (page: number = 1, parameters: any = {}) => {
        let url = Api.URL_POINT_FORM_LOGS + '?order%5BcreatedAt%5D=desc';
        let config = {
            params: {
                page: page,
                ...parameters,
            },
        };

        return axios.get(url, config)
            .then((res) => {
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get institutionInterventions list.
     *
     * @param name {string}
     * @param address {string}
     * @param contact {string}
     * @param phone {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getInstitutionInterventions = (name: string = '', address: string = '', contact: string = '', phone: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (address !== '') {
            params.address = address;
        }
        if (contact !== '') {
            params.contact = contact;
        }
        if (phone !== '') {
            params.phone = phone;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_INSTITUTION_INTERVENTIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getInstitutionInterventions(name, address, contact, phone, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add institutionIntervention.
     *
     * @param institutionIntervention
     * @param institutionIntervention.country {string} Country IRI.
     * @param institutionIntervention.name {string} InstitutionIntervention name property.
     * @param institutionIntervention.address {string} InstitutionIntervention address property.
     * @param institutionIntervention.contact {string} InstitutionIntervention contact property.
     * @param institutionIntervention.phone {string} InstitutionIntervention phone property.
     * @returns {Promise}
     */
    static addInstitutionIntervention = (institutionIntervention: {country: string, name: string, address?: string, contact?: string, phone?: string}) => {
        return axios.post(Api.URL_INSTITUTION_INTERVENTIONS, institutionIntervention)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update institutionIntervention.
     *
     * @param institutionIntervention
     * @param institutionIntervention.id {string} InstitutionIntervention ID.
     * @param institutionIntervention.name {string} InstitutionIntervention name property.
     * @param institutionIntervention.address {string} InstitutionIntervention address property.
     * @param institutionIntervention.contact {string} InstitutionIntervention contact property.
     * @param institutionIntervention.phone {string} InstitutionIntervention phone property.
     * @returns {Promise}
     */
    static updateInstitutionIntervention = (institutionIntervention: {id: string, name: string, address?: string, contact?: string, phone?: string}) => {
        return axios.put(Api.URL_INSTITUTION_INTERVENTIONS+"/"+institutionIntervention.id, {name: institutionIntervention.name, address: institutionIntervention.address, contact: institutionIntervention.contact, phone: institutionIntervention.phone})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove institutionIntervention.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeInstitutionIntervention = (id: string) => {
        return axios.delete(Api.URL_INSTITUTION_INTERVENTIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get waterQualityEntities list.
     *
     * @param name {string}
     * @param address {string}
     * @param contact {string}
     * @param phone {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getWaterQualityEntities = (name: string = '', address: string = '', contact: string = '', phone: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (address !== '') {
            params.address = address;
        }
        if (contact !== '') {
            params.contact = contact;
        }
        if (phone !== '') {
            params.phone = phone;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_WATER_QUALITY_ENTITIES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getWaterQualityEntities(name, address, contact, phone, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add waterQualityEntity.
     *
     * @param waterQualityEntity
     * @param waterQualityEntity.country {string} Country IRI.
     * @param waterQualityEntity.name {string} WaterQualityEntity name property.
     * @param waterQualityEntity.address {string} WaterQualityEntity address property.
     * @param waterQualityEntity.contact {string} WaterQualityEntity contact property.
     * @param waterQualityEntity.phone {string} WaterQualityEntity phone property.
     * @returns {Promise}
     */
    static addWaterQualityEntity = (waterQualityEntity: {country: string, name: string, address?: string, contact?: string, phone?: string}) => {
        return axios.post(Api.URL_WATER_QUALITY_ENTITIES, waterQualityEntity)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update waterQualityEntity.
     *
     * @param waterQualityEntity
     * @param waterQualityEntity.id {string} WaterQualityEntity ID.
     * @param waterQualityEntity.name {string} WaterQualityEntity name property.
     * @param waterQualityEntity.address {string} WaterQualityEntity address property.
     * @param waterQualityEntity.contact {string} WaterQualityEntity contact property.
     * @param waterQualityEntity.phone {string} WaterQualityEntity phone property.
     * @returns {Promise}
     */
    static updateWaterQualityEntity = (waterQualityEntity: {id: string, name: string, address?: string, contact?: string, phone?: string}) => {
        return axios.put(Api.URL_WATER_QUALITY_ENTITIES+"/"+waterQualityEntity.id, {name: waterQualityEntity.name, address: waterQualityEntity.address, contact: waterQualityEntity.contact, phone: waterQualityEntity.phone})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove waterQualityEntity.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeWaterQualityEntity = (id: string) => {
        return axios.delete(Api.URL_WATER_QUALITY_ENTITIES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get funderInterventions list.
     *
     * @param name {string}
     * @param address {string}
     * @param contact {string}
     * @param phone {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getFunderInterventions = (name: string = '', address: string = '', contact: string = '', phone: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (address !== '') {
            params.address = address;
        }
        if (contact !== '') {
            params.contact = contact;
        }
        if (phone !== '') {
            params.phone = phone;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_FUNDER_INTERVENTIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getFunderInterventions(name, address, contact, phone, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add funderIntervention.
     *
     * @param funderIntervention
     * @param funderIntervention.country {string} Country IRI.
     * @param funderIntervention.name {string} FunderIntervention name property.
     * @param funderIntervention.address {string} FunderIntervention address property.
     * @param funderIntervention.contact {string} FunderIntervention contact property.
     * @param funderIntervention.phone {string} FunderIntervention phone property.
     * @returns {Promise}
     */
    static addFunderIntervention = (funderIntervention: {country: string, name: string, address?: string, contact?: string, phone?: string}) => {
        return axios.post(Api.URL_FUNDER_INTERVENTIONS, funderIntervention)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update funderIntervention.
     *
     * @param funderIntervention
     * @param funderIntervention.id {string} FunderIntervention ID.
     * @param funderIntervention.name {string} FunderIntervention name property.
     * @param funderIntervention.address {string} FunderIntervention address property.
     * @param funderIntervention.contact {string} FunderIntervention contact property.
     * @param funderIntervention.phone {string} FunderIntervention phone property.
     * @returns {Promise}
     */
    static updateFunderIntervention = (funderIntervention: {id: string, name: string, address?: string, contact?: string, phone?: string}) => {
        return axios.put(Api.URL_FUNDER_INTERVENTIONS+"/"+funderIntervention.id, {name: funderIntervention.name, address: funderIntervention.address, contact: funderIntervention.contact, phone: funderIntervention.phone})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove funderIntervention.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeFunderIntervention = (id: string) => {
        return axios.delete(Api.URL_FUNDER_INTERVENTIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get officialLanguages list.
     *
     * @param language {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getOfficialLanguages = (language: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (language !== '') {
            params.language = language;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_OFFICIAL_LANGUAGES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getOfficialLanguages(language, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add officialLanguage.
     *
     * @param officialLanguage
     * @param officialLanguage.country {string} Country IRI.
     * @param officialLanguage.language {string} OfficialLanguage language property.
     * @returns {Promise}
     */
    static addOfficialLanguage = (officialLanguage: {country: string, language: string}) => {
        return axios.post(Api.URL_OFFICIAL_LANGUAGES, officialLanguage)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update officialLanguage.
     *
     * @param officialLanguage
     * @param officialLanguage.id {string} OfficialLanguage ID.
     * @param officialLanguage.language {string} OfficialLanguage language property.
     * @returns {Promise}
     */
    static updateOfficialLanguage = (officialLanguage: {id: string, language: string}) => {
        return axios.put(Api.URL_OFFICIAL_LANGUAGES+"/"+officialLanguage.id, {language: officialLanguage.language})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove officialLanguage.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeOfficialLanguage = (id: string) => {
        return axios.delete(Api.URL_OFFICIAL_LANGUAGES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get user info.
     *
     * @param id
     * @returns {Promise}
     */
    static getUser = (id: string) => {
        return axios.get(Api.URL_USERS + '/' + id)
            .then(res => res.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add a new user.
     *
     * @param user
     * @returns {Promise}
     */
    static addUser = (user: any) => {
        return axios
            .post(Api.URL_USERS, user)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update user data.
     *
     * @param id
     * @param user
     * @returns {Promise}
     */
    static updateUser = (id: string | undefined, user: any) => {
        // Remove unnecessary fields.
        delete user.created;
        delete user.updated;
        delete user.id;
        delete user.gravatar;
        delete user.countryId;
        delete user.countryFlag;
        delete user.countryFlagId;

        return axios
            .put(Api.URL_USERS + '/' + id, user)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get special components list.
     *
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getSpecialComponents = (name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.type = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_SPECIAL_COMPONENTS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getSpecialComponents(name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add special component.
     *
     * @param specialComponent
     * @param specialComponent.country {string} Country IRI.
     * @param specialComponent.keyIndex {string} Special component keyIndex property.
     * @param specialComponent.name {string} Special component name property.
     * @returns {Promise}
     */
    static addSpecialComponent = (specialComponent: {country: string, keyIndex: string, name: string}) => {
        return axios.post(Api.URL_SPECIAL_COMPONENTS, specialComponent)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update special component.
     *
     * @param specialComponent
     * @param specialComponent.id {string} Special component ID.
     * @param specialComponent.keyIndex {string} Special component keyIndex property.
     * @param specialComponent.name {string} Special component name property.
     * @returns {Promise}
     */
    static updateSpecialComponent = (specialComponent: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_SPECIAL_COMPONENTS+"/"+specialComponent.id, {keyIndex: specialComponent.keyIndex, name: specialComponent.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove special component.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeSpecialComponent = (id: string) => {
        return axios.delete(Api.URL_SPECIAL_COMPONENTS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get ethnicities list.
     *
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getEthnicities = (name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_ETHNICITIES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getEthnicities(name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add ethnicities.
     *
     * @param ethnicities
     * @param ethnicities.country {string} Country IRI.
     * @param ethnicities.name {string} Ethnicities type property.
     * @returns {Promise}
     */
    static addEthnicity = (ethnicities: {country: string, name: string}) => {
        return axios.post(Api.URL_ETHNICITIES, ethnicities)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update ethnicities.
     *
     * @param ethnicities
     * @param ethnicities.id {string} Ethnicities ID.
     * @param ethnicities.name {string} Ethnicities name property.
     * @returns {Promise}
     */
    static updateEthnicity = (ethnicities: {id: string, name: string}) => {
        return axios.put(Api.URL_ETHNICITIES+"/"+ethnicities.id, {name: ethnicities.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove ethnicities.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeEthnicity = (id: string) => {
        return axios.delete(Api.URL_ETHNICITIES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get DefaultDiameters list.
     *
     * @param value {number}
     * @param unit {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getDefaultDiameters = (value: number = 0, unit: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (value !== 0) {
            params.value = value;
        }
        if (unit !== '') {
            params.unit = value;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_DEFAULT_DIAMETERS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getDefaultDiameters(value, unit, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add Default Diameter.
     *
     * @param defaultDiameter
     * @param defaultDiameter.country {string} Country IRI.
     * @param defaultDiameter.value {string} DefaultDiameter value property.
     * @param defaultDiameter.unit {string} DefaultDiameter unit property.
     * @returns {Promise}
     */
    static addDefaultDiameter = (defaultDiameter: {country: string, value: string, unit: string}) => {
        return axios.post(Api.URL_DEFAULT_DIAMETERS, defaultDiameter)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update Default Diameter.
     *
     * @param defaultDiameter
     * @param defaultDiameter.id {string} DefaultDiameter ID.
     * @param defaultDiameter.value {string} DefaultDiameter value property.
     * @param defaultDiameter.unit {string} DefaultDiameter unit property.
     * @returns {Promise}
     */
    static updateDefaultDiameter = (defaultDiameter: {id: string, value: string, unit: string}) => {
        return axios.put(Api.URL_DEFAULT_DIAMETERS+"/"+defaultDiameter.id, {value: defaultDiameter.value, unit: defaultDiameter.unit})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove default diameter.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeDefaultDiameter = (id: string) => {
        return axios.delete(Api.URL_DEFAULT_DIAMETERS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get materials list.
     *
     * @param type {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getMaterials = (type: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type = type;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_MATERIALS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getMaterials(type, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add material.
     *
     * @param material
     * @param material.country {string} Country IRI.
     * @param material.keyIndex {string} Material keyIndex property.
     * @param material.type {string} Material type property.
     * @returns {Promise}
     */
    static addMaterial = (material: {country: string, keyIndex: string, type: string}): Promise<any> => {
        return axios.post(Api.URL_MATERIALS, material)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update material.
     *
     * @param material
     * @param material.id {string} Material ID.
     * @param material.keyIndex {string} Material keyIndex property.
     * @param material.type {string} Material type property.
     * @returns {Promise}
     */
    static updateMaterial = (material: {id: string, keyIndex: string, type: string}) => {
        return axios.put(Api.URL_MATERIALS+"/"+material.id, {keyIndex: material.keyIndex, type: material.type})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove material.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeMaterial = (id: string) => {
        return axios.delete(Api.URL_MATERIALS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get pump_types list.
     *
     * @param type {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getPumpTypes = (type: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type = type;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_PUMP_TYPES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getPumpTypes(type, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get Functions carried out by TAP list.
     *
     * @param type {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getFunctionsCarriedOutTaps = (type: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type = type;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_FUNCTIONS_CARRIED_OUT_TAPS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getFunctionsCarriedOutTaps(type, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add Function carried out by TAP.
     *
     * @param data
     * @param data.country {string} country IRI.
     * @param data.keyIndex {string}.
     * @param data.type {string} type property.
     * @returns {Promise}
     */
    static addFunctionCarriedOutTap = (data: {country: string, keyIndex: string, type: string}) => {
        return axios.post(Api.URL_FUNCTIONS_CARRIED_OUT_TAPS, data)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete function carried out by TAP.
     *
     * @param id {string} id property.
     * @returns {Promise}
     */
    static removeFuncionCarriedOutTap = (id: string) => {
        return axios.delete(Api.URL_FUNCTIONS_CARRIED_OUT_TAPS + '/' + id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update function carried out by TAP.
     *
     * @param data
     * @param data.id {string}
     * @param data.keyIndex {string}
     * @param data.type {string}
     *
     * @returns {Promise}
     */
    static updateFunctionCarriedOutTap = (data: {id: string, keyIndex: string, type: string}) => {
        return axios.put(Api.URL_FUNCTIONS_CARRIED_OUT_TAPS + "/" + data.id, { keyIndex: data.keyIndex, type: data.type })
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get FunctionsCarriedOutWsps list.
     *
     * @param type {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getFunctionsCarriedOutWsps = (type: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type= type;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_FUNCTIONS_CARRIED_OUT_WSPS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getFunctionsCarriedOutWsps(type, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add functionsCarriedOutWsp.
     *
     * @param functionsCarriedOutWsp
     * @param functionsCarriedOutWsp.country {string} Country IRI.
     * @param functionsCarriedOutWsp.keyIndex {string} FunctionsCarriedOutWsp keyIndex property.
     * @param functionsCarriedOutWsp.type {string} FunctionsCarriedOutWsp type property.
     * @returns {Promise}
     */
    static addFunctionsCarriedOutWsp = (functionsCarriedOutWsp: {country: string, keyIndex: string, type: string}) => {
        return axios.post(Api.URL_FUNCTIONS_CARRIED_OUT_WSPS, functionsCarriedOutWsp)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update functionsCarriedOutWsp.
     *
     * @param functionsCarriedOutWsp
     * @param functionsCarriedOutWsp.id {string} FunctionsCarriedOutWsp ID.
     * @param functionsCarriedOutWsp.keyIndex {string} FunctionsCarriedOutWsp keyIndex property.
     * @param functionsCarriedOutWsp.type {string} FunctionsCarriedOutWsp type property.
     * @returns {Promise}
     */
    static updateFunctionsCarriedOutWsp = (functionsCarriedOutWsp: {id: string, keyIndex: string, type: string}) => {
        return axios.put(Api.URL_FUNCTIONS_CARRIED_OUT_WSPS+"/"+functionsCarriedOutWsp.id, {keyIndex: functionsCarriedOutWsp.keyIndex, type: functionsCarriedOutWsp.type})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove functionsCarriedOutWsp.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeFunctionsCarriedOutWsp = (id: string) => {
        return axios.delete(Api.URL_FUNCTIONS_CARRIED_OUT_WSPS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add pumpType.
     *
     * @param pumpType
     * @param pumpType.country {string} Country IRI.
     * @param pumpType.type {string} PumpType type property.
     * @returns {Promise}
     */
     static addPumpType = (pumpType: {country: string, type: string}) => {
        return axios.post(Api.URL_PUMP_TYPES, pumpType)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Send error message to notificate the user.
     *
     * @param info
     * @param isPublic
     *
     * @returns {Promise}
     *
     * @private
     */
    static displayErrorMessage(info: any, isPublic: boolean = true) {
        // Dispatch status event.
        let message = "Unknown";
        if (info.response) {
            message = info.response.data?.message ?? info.response.data?.detail ?? info.response.statusText;
        }
        eventManager.dispatch(
            Events.STATUS_ADD,
            {
                level: StatusEventLevel.ERROR,
                title: t("Error @code", {"@code": info.response ? info.response.status : "??"}),
                message: message,
                isPublic: isPublic,
            }
        );
        return Promise.reject(info);
    }

    /**
     * Update pumpType.
     *
     * @param pumpType
     * @param pumpType.id {string} PumpType ID.
     * @param pumpType.type {string} PumpType type property.
     * @returns {Promise}
     */
    static updatePumpType = (pumpType: {id: string, type: string}) => {
        return axios.put(Api.URL_PUMP_TYPES+"/"+pumpType.id, {type: pumpType.type})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove pumpType.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removePumpType = (id: string) => {
        return axios.delete(Api.URL_PUMP_TYPES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get technicalAssistanceProviders list.
     *
     * @param name {string}
     * @param description {string}
     * @param nationalId {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTechnicalAssistanceProviders = (name: string = '', description: string = '', nationalId: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (description !== '') {
            params.description = description;
        }
        if (nationalId !== '') {
            params.nationalId = nationalId;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TECHNICAL_ASSISTANCE_PROVIDERS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTechnicalAssistanceProviders(name, description, nationalId, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get technicalAssistanceProvider.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    static getTechnicalAssistanceProvider = (id: string): Promise<any> => {
        return axios
            .get(Api.URL_TECHNICAL_ASSISTANCE_PROVIDERS + "/" + id)
            .then((res) => {
                return res.data;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get program_interventions list.
     *
     * @param name {string}
     * @param description {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getProgramInterventions = (name: string = '', description: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (description !== '') {
            params.description = description;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_PROGRAM_INTERVENTIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getProgramInterventions(name, description, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add ProgramIntervention.
     *
     * @param programIntervention
     * @param programIntervention.country {string} Country IRI.
     * @param programIntervention.name {string} ProgramIntervention name property.
     * @param programIntervention.description {string} ProgramIntervention description property.
     * @returns {Promise}
     */
    static addProgramIntervention = (programIntervention: {country: string, name: string, description: string}): Promise<any> => {
        if (!programIntervention.description) {
            programIntervention.description = "";
        }
        return axios.post(Api.URL_PROGRAM_INTERVENTIONS, programIntervention)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update ProgramIntervention.
     *
     * @param programIntervention
     * @param programIntervention.id {string} ProgramIntervention ID.
     * @param programIntervention.name {string} ProgramIntervention name property.
     * @param programIntervention.description {string} ProgramIntervention description property.
     * @returns {Promise}
     */
    static updateProgramIntervention = (programIntervention: {id: string, name: string, description: string}) => {
        return axios.put(Api.URL_PROGRAM_INTERVENTIONS+"/"+programIntervention.id, {
            name: programIntervention.name,
            description: programIntervention.description
        })
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove ProgramIntervention.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeProgramIntervention = (id: string) => {
        return axios.delete(Api.URL_PROGRAM_INTERVENTIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get DisinfectingSubstances list.
     *
     * @param keyIndex {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getDisinfectingSubstances = (keyIndex: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (keyIndex !== '') {
            params.keyIndex = keyIndex;
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_DISINFECTING_SUBSTANCE, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getDisinfectingSubstances(keyIndex, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add DisinfectingSubstance.
     *
     * @param disinfectingSubstance
     * @param disinfectingSubstance.country {string} Country IRI.
     * @param disinfectingSubstance.keyIndex {string} DisinfectingSubstance keyIndex property.
     * @param disinfectingSubstance.name {string} DisinfectingSubstance name property.
     * @returns {Promise}
     */
    static addDisinfectingSubstance = (disinfectingSubstance: {country: string, keyIndex: string, name: string}): Promise<any> => {
        return axios.post(Api.URL_DISINFECTING_SUBSTANCE, disinfectingSubstance)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update DisinfectingSubstance.
     *
     * @param DisinfectingSubstance
     * @param DisinfectingSubstance.id {string} DisinfectingSubstance ID.
     * @param DisinfectingSubstance.keyIndex {string} DisinfectingSubstance keyIndex property.
     * @param DisinfectingSubstance.name {string} DisinfectingSubstance name property.
     * @returns {Promise}
     */
    static updateDisinfectingSubstance = (DisinfectingSubstance: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_DISINFECTING_SUBSTANCE+"/"+DisinfectingSubstance.id, {
            keyIndex: DisinfectingSubstance.keyIndex,
            name: DisinfectingSubstance.name
        })
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove DisinfectingSubstance.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeDisinfectingSubstance = (id: string) => {
        return axios.delete(Api.URL_DISINFECTING_SUBSTANCE+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get Distribution material list.
     *
     * @param type {string}
     * @param keyIndex {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getDistributionMaterials = (type: string = '', keyIndex: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type = type;
        }
        if (keyIndex !== '') {
            params.keyIndex = keyIndex;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_DISTRIBUTION_MATERIAL, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getDistributionMaterials(type, keyIndex, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add Distribution material.
     *
     * @param DistributionMaterial
     * @param DistributionMaterial.country {string} Country IRI.
     * @param DistributionMaterial.type {string} Distribution material type property.
     * @param DistributionMaterial.keyIndex {string} Distribution material keyIndex property.
     * @returns {Promise}
     */
    static addDistributionMaterial = (DistributionMaterial: {country: string, type: string, keyIndex: string}): Promise<any> => {
        return axios.post(Api.URL_DISTRIBUTION_MATERIAL, DistributionMaterial)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update Distribution material.
     *
     * @param DistributionMaterial
     * @param DistributionMaterial.id {string} Distribution material ID.
     * @param DistributionMaterial.keyIndex {string} Distribution material keyIndex property.
     * @param DistributionMaterial.type {string} Distribution material type property.
     * @returns {Promise}
     */
    static updateDistributionMaterial = (DistributionMaterial: {id: string, keyIndex: string, type: string}) => {
        return axios.put(Api.URL_DISTRIBUTION_MATERIAL+"/"+DistributionMaterial.id, {
            type: DistributionMaterial.type,
            keyIndex: DistributionMaterial.keyIndex
        })
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove Distribution material.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeDistributionMaterial = (id: string) => {
        return axios.delete(Api.URL_DISTRIBUTION_MATERIAL+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get typeInterventions list.
     *
     * @param type {string}
     * @param description {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTypeInterventions = (type: string = '', description: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type = type;
        }
        if (description !== '') {
            params.description = description;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TYPE_INTERVENTIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTypeInterventions(type, description, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add typeIntervention.
     *
     * @param typeIntervention
     * @param typeIntervention.country {string} Country IRI.
     * @param typeIntervention.type {string} TypeIntervention type property.
     * @param typeIntervention.description {string} TypeIntervention description property.
     * @returns {Promise}
     */
    static addTypeIntervention = (typeIntervention: {country: string, type: string, description: string}): Promise<any> => {
        if (!typeIntervention.description) {
            typeIntervention.description = "";
        }
        return axios.post(Api.URL_TYPE_INTERVENTIONS, typeIntervention)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update typeIntervention.
     *
     * @param typeIntervention
     * @param typeIntervention.id {string} TypeIntervention ID.
     * @param typeIntervention.type {string} TypeIntervention type property.
     * @param typeIntervention.description {string} TypeIntervention description property.
     * @returns {Promise}
     */
    static updateTypeIntervention = (typeIntervention: {id: string, type: string, description: string}) => {
        return axios.put(Api.URL_TYPE_INTERVENTIONS+"/"+typeIntervention.id, {
            type: typeIntervention.type,
            description: typeIntervention.description
        })
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove typeIntervention.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeTypeIntervention = (id: string) => {
        return axios.delete(Api.URL_TYPE_INTERVENTIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get typeInterventions list.
     *
     * @param type {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getCommunityServices = (type: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (type !== '') {
            params.type = type;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_COMMUNITY_SERVICES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getCommunityServices(type, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add communityService.
     *
     * @param communityService
     * @param communityService.country {string} Country IRI.
     * @param communityService.type {string} CommunityService type property.
     * @returns {Promise}
     */
    static addCommunityService = (communityService: {country: string, type: string}): Promise<any> => {
        return axios.post(Api.URL_COMMUNITY_SERVICES, communityService)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update communityService.
     *
     * @param communityService
     * @param communityService.id {string} CommunityService ID.
     * @param communityService.type {string} CommunityService type property.
     * @returns {Promise}
     */
    static updateCommunityService = (communityService: {id: string, type: string}) => {
        return axios.put(Api.URL_COMMUNITY_SERVICES+"/"+communityService.id, {
            type: communityService.type,
        })
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove communityService.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeCommunityService = (id: string) => {
        return axios.delete(Api.URL_COMMUNITY_SERVICES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get TypologyChlorinationInstallations list.
     *
     * @param id {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTypologyChlorinationInstallations = (id: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (id !== '') {
            params.id = id;
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TYPOLOGY_CHLORINATION_INSTALLATIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTypologyChlorinationInstallations(id, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add typologyChlorinationInstallation.
     *
     * @param typologyChlorinationInstallation
     * @param typologyChlorinationInstallation.country {string} Country IRI.
     * @param typologyChlorinationInstallation.keyIndex {string} TypologyChlorinationInstallation keyIndex property.
     * @param typologyChlorinationInstallation.name {string} TypologyChlorinationInstallation name property.
     * @returns {Promise}
     */
    static addTypologyChlorinationInstallation = (typologyChlorinationInstallation: {country: string, keyIndex: string, name: string}) => {
        return axios.post(Api.URL_TYPOLOGY_CHLORINATION_INSTALLATIONS, typologyChlorinationInstallation)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update typologyChlorinationInstallation.
     *
     * @param typologyChlorinationInstallation
     * @param typologyChlorinationInstallation.id {string} TypologyChlorinationInstallation ID.
     * @param typologyChlorinationInstallation.keyIndex {string} TypologyChlorinationInstallation keyIndex property.
     * @param typologyChlorinationInstallation.name {string} TypologyChlorinationInstallation name property.
     * @returns {Promise}
     */
    static updateTypologyChlorinationInstallation = (typologyChlorinationInstallation: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_TYPOLOGY_CHLORINATION_INSTALLATIONS+"/"+typologyChlorinationInstallation.id, {keyIndex: typologyChlorinationInstallation.keyIndex, name: typologyChlorinationInstallation.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove typologyChlorinationInstallation.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeTypologyChlorinationInstallation = (id: string) => {
        return axios.delete(Api.URL_TYPOLOGY_CHLORINATION_INSTALLATIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get TypeTaps list.
     *
     * @param id {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getTypeTaps = (id: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (id !== '') {
            params.id = id;
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TYPE_TAPS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTypeTaps(id, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add typeTap.
     *
     * @param typeTap
     * @param typeTap.country {string} Country IRI.
     * @param typeTap.keyIndex {string} TypeTap keyIndex property.
     * @param typeTap.name {string} TypeTap name property.
     * @returns {Promise}
     */
    static addTypeTap = (typeTap: {country: string, keyIndex: string, name: string}) => {
        return axios.post(Api.URL_TYPE_TAPS, typeTap)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update typeTap.
     *
     * @param typeTap
     * @param typeTap.id {string} TypeTap ID.
     * @param typeTap.keyIndex {string} TypeTap keyIndex property.
     * @param typeTap.name {string} TypeTap name property.
     * @returns {Promise}
     */
    static updateTypeTap = (typeTap: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_TYPE_TAPS+"/"+typeTap.id, {keyIndex: typeTap.keyIndex, name: typeTap.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove typeTap.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeTypeTap = (id: string) => {
        return axios.delete(Api.URL_TYPE_TAPS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get WaterQualityInterventionTypes list.
     *
     * @param id {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getWaterQualityInterventionTypes = (id: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (id !== '') {
            params.id = id;
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_WATER_QUALITY_INTERVENTION_TYPES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getWaterQualityInterventionTypes(id, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add waterQualityInterventionType.
     *
     * @param waterQualityInterventionType
     * @param waterQualityInterventionType.country {string} Country IRI.
     * @param waterQualityInterventionType.keyIndex {string} WaterQualityInterventionType keyIndex property.
     * @param waterQualityInterventionType.name {string} WaterQualityInterventionType name property.
     * @returns {Promise}
     */
    static addWaterQualityInterventionType = (waterQualityInterventionType: {country: string, keyIndex: string, name: string}) => {
        return axios.post(Api.URL_WATER_QUALITY_INTERVENTION_TYPES, waterQualityInterventionType)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update waterQualityInterventionType.
     *
     * @param waterQualityInterventionType
     * @param waterQualityInterventionType.id {string} WaterQualityInterventionType ID.
     * @param waterQualityInterventionType.keyIndex {string} WaterQualityInterventionType keyIndex property.
     * @param waterQualityInterventionType.name {string} WaterQualityInterventionType name property.
     * @returns {Promise}
     */
    static updateWaterQualityInterventionType = (waterQualityInterventionType: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_WATER_QUALITY_INTERVENTION_TYPES+"/"+waterQualityInterventionType.id, {keyIndex: waterQualityInterventionType.keyIndex, name: waterQualityInterventionType.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove waterQualityInterventionType.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeWaterQualityInterventionType = (id: string) => {
        return axios.delete(Api.URL_WATER_QUALITY_INTERVENTION_TYPES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get TypeHealthcareFacilities list.
     *
     * @param id {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getTypeHealthcareFacilities = (id: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (id !== '') {
            params.id = id;
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TYPE_HEALTHCARE_FACILITIES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTypeHealthcareFacilities(id, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add typeHealthcareFacility.
     *
     * @param typeHealthcareFacility
     * @param typeHealthcareFacility.country {string} Country IRI.
     * @param typeHealthcareFacility.keyIndex {string} TypeHealthcareFacility keyIndex property.
     * @param typeHealthcareFacility.name {string} TypeHealthcareFacility name property.
     * @returns {Promise}
     */
    static addTypeHealthcareFacility = (typeHealthcareFacility: {country: string, keyIndex: string, name: string}) => {
        return axios.post(Api.URL_TYPE_HEALTHCARE_FACILITIES, typeHealthcareFacility)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update typeHealthcareFacility.
     *
     * @param typeHealthcareFacility
     * @param typeHealthcareFacility.id {string} TypeHealthcareFacility ID.
     * @param typeHealthcareFacility.keyIndex {string} TypeHealthcareFacility keyIndex property.
     * @param typeHealthcareFacility.name {string} TypeHealthcareFacility name property.
     * @returns {Promise}
     */
    static updateTypeHealthcareFacility = (typeHealthcareFacility: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_TYPE_HEALTHCARE_FACILITIES+"/"+typeHealthcareFacility.id, {keyIndex: typeHealthcareFacility.keyIndex, name: typeHealthcareFacility.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove typeHealthcareFacility.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeTypeHealthcareFacility = (id: string) => {
        return axios.delete(Api.URL_TYPE_HEALTHCARE_FACILITIES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }
    /**
     * Get InterventionStatuses list.
     *
     * @param id {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getInterventionStatuses = (id: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (id !== '') {
            params.id = id;
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_INTERVENTION_STATUSES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getInterventionStatuses(id, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add interventionStatus.
     *
     * @param interventionStatus
     * @param interventionStatus.country {string} Country IRI.
     * @param interventionStatus.keyIndex {string} InterventionStatus keyIndex property.
     * @param interventionStatus.name {string} InterventionStatus name property.
     * @returns {Promise}
     */
    static addInterventionStatus = (interventionStatus: {country: string, keyIndex: string, name: string}) => {
        return axios.post(Api.URL_INTERVENTION_STATUSES, interventionStatus)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update interventionStatus.
     *
     * @param interventionStatus
     * @param interventionStatus.id {string} InterventionStatus ID.
     * @param interventionStatus.keyIndex {string} InterventionStatus keyIndex property.
     * @param interventionStatus.name {string} InterventionStatus name property.
     * @returns {Promise}
     */
    static updateInterventionStatus = (interventionStatus: {id: string, keyIndex: string, name: string}) => {
        return axios.put(Api.URL_INTERVENTION_STATUSES+"/"+interventionStatus.id, {keyIndex: interventionStatus.keyIndex, name: interventionStatus.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove interventionStatus.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeInterventionStatus = (id: string) => {
        return axios.delete(Api.URL_INTERVENTION_STATUSES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get treatmentTechnologyCoagFloccus list.
     *
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTreatmentTechnologyCoagFloccus = (name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TREATMENT_TECHNOLOGY_COAG_FLOCCUS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTreatmentTechnologyCoagFloccus(name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add treatmentTechnologyCoagFloccu.
     *
     * @param treatmentTechnologyCoagFloccu
     * @param treatmentTechnologyCoagFloccu.country {string} Country IRI.
     * @param treatmentTechnologyCoagFloccu.name {string} TreatmentTechnologyCoagFloccu name property.
     * @returns {Promise}
     */
    static addTreatmentTechnologyCoagFloccu = (treatmentTechnologyCoagFloccu: {country: string, name: string}) => {
        return axios.post(Api.URL_TREATMENT_TECHNOLOGY_COAG_FLOCCUS, treatmentTechnologyCoagFloccu)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update treatmentTechnologyCoagFloccu.
     *
     * @param treatmentTechnologyCoagFloccu
     * @param treatmentTechnologyCoagFloccu.id {string} TreatmentTechnologyCoagFloccu ID.
     * @param treatmentTechnologyCoagFloccu.name {string} TreatmentTechnologyCoagFloccu name property.
     * @returns {Promise}
     */
    static updateTreatmentTechnologyCoagFloccu = (treatmentTechnologyCoagFloccu: {id: string, name: string}) => {
        return axios.put(Api.URL_TREATMENT_TECHNOLOGY_COAG_FLOCCUS+"/"+treatmentTechnologyCoagFloccu.id, {name: treatmentTechnologyCoagFloccu.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove treatmentTechnologyCoagFloccu.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeTreatmentTechnologyCoagFloccu = (id: string) => {
        return axios.delete(Api.URL_TREATMENT_TECHNOLOGY_COAG_FLOCCUS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get treatmentTechnologySedimentations list.
     *
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTreatmentTechnologySedimentations = (name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_TREATMENT_TECHNOLOGY_SEDIMENTATIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTreatmentTechnologySedimentations(name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add treatmentTechnologySedimentation.
     *
     * @param treatmentTechnologySedimentation
     * @param treatmentTechnologySedimentation.country {string} Country IRI.
     * @param treatmentTechnologySedimentation.name {string} TreatmentTechnologySedimentation name property.
     * @returns {Promise}
     */
    static addTreatmentTechnologySedimentation = (treatmentTechnologySedimentation: {country: string, name: string}) => {
        return axios.post(Api.URL_TREATMENT_TECHNOLOGY_SEDIMENTATIONS, treatmentTechnologySedimentation)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update treatmentTechnologySedimentation.
     *
     * @param treatmentTechnologySedimentation
     * @param treatmentTechnologySedimentation.id {string} TreatmentTechnologySedimentation ID.
     * @param treatmentTechnologySedimentation.name {string} TreatmentTechnologySedimentation name property.
     * @returns {Promise}
     */
    static updateTreatmentTechnologySedimentation = (treatmentTechnologySedimentation: {id: string, name: string}) => {
        return axios.put(Api.URL_TREATMENT_TECHNOLOGY_SEDIMENTATIONS+"/"+treatmentTechnologySedimentation.id, {name: treatmentTechnologySedimentation.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove treatmentTechnologySedimentation.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeTreatmentTechnologySedimentation = (id: string) => {
        return axios.delete(Api.URL_TREATMENT_TECHNOLOGY_SEDIMENTATIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get currencies list.
     *
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
     static getCurrencies = (name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_CURRENCIES, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getCurrencies(name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add currency.
     *
     * @param currency
     * @param currency.country {string} Country IRI.
     * @param currency.name {string} Currency name property.
     * @returns {Promise}
     */
    static addCurrency = (currency: {country: string, name: string}) => {
        return axios.post(Api.URL_CURRENCIES, currency)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update currency.
     *
     * @param currency
     * @param currency.id {string} Currency ID.
     * @param currency.name {string} Currency name property.
     * @returns {Promise}
     */
    static updateCurrency = (currency: {id: string, name: string}) => {
        return axios.put(Api.URL_CURRENCIES+"/"+currency.id, {name: currency.name})
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove currency.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeCurrency = (id: string) => {
        return axios.delete(Api.URL_CURRENCIES+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get administrative divisions.
     *
     * @param name {string}
     * @param code {string}
     * @param parent {string|null}
     * @param country {string}
     * @param page {number|null}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getAdministrativeDivisions = (name: string = '', code: string = '', parent: string|null = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string,any> = {
            page: page,
        };
        if (name !== '') {
            params.name = name;
        }
        if (code !== '') {
            params.code = code;
        }
        if (parent !== '') {
            params.parent = parent !== null ? parent : 'null';
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_ADMINISTRATIVE_DIVISIONS, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                if (res.data[0] === undefined) {
                    return prevData;
                }
                return Api.getAdministrativeDivisions(name, code, parent, country, page + 1, prevData);
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get administrative divisions.
     *
     * @param code Country code.
     * @param filters Extra filters.
     * @returns {Promise}
     *
     * @deprecated Administrative divisions will be loaded with pagination, use getAdministrativeDivisions().
     */
    static getAdministrativeDivisionsOld = (code: string, filters?: any) => {
        const params = {...filters};
        if (code) {
            params["country"] = code;
        }

        return axios
            .get(Api.URL_ADMINISTRATIVE_DIVISIONS, {params})
            .then((res) => res.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get all administrative divisions.
     *
     * This use an ultra speed end-point with 500 items pages.
     *
     * @param code {string} Country code.
     * @param page {number} Internal use.
     * @param prevData {any} Internal use.
     * @param stepCallback {Function|null}
     *
     * @returns {Promise<any>}
     *     Complete administrative division after load all pages.
     */
    static getAllAdministrativeDivisions = (code: string, page: number = 1, prevData: any = [], stepCallback: Function|null = null): Promise<any> => {
        const params = {
            page: page,
        };
        return axios
            .get(Api.URL_ADMINISTRATIVE_DIVISIONS_BY_COUNTRY+code, {params})
            .then((res) => {
                prevData = prevData.concat(res.data);
                if (stepCallback) {
                    stepCallback(prevData.length);
                }
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getAllAdministrativeDivisions(code, page + 1, prevData, stepCallback);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get administrative division.
     *
     * @param id
     * @returns {Promise}
     */
    static getAdministrativeDivision = (id: string) => {
        if ("" === id) {
            return undefined;
        }
        return axios
            .get(`${Api.URL_ADMINISTRATIVE_DIVISIONS}/${id}`)
            .then((res) => {
                return res.data;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    };

    /**
     * Get all administrative division parents.
     *
     * @deprecated
     *
     * @todo Optimize this.
     *
     * @param division
     * @returns {Promise}
     * 
     * @deprecated Administrative divisions will be loaded from db, use dmGetAdministrativeDivisionParents().
     */
    static getAllAdministrativeDivisionParents = async (division: AdministrativeDivision) => {
        const arrParents = [];
        let parentIRI = division.parent;
        while (parentIRI !== undefined) {
            const parentDivision = await axios.get(parentIRI).then((res) => res.data);
            arrParents.push(parentDivision);
            parentIRI = parentDivision.parent;
        }
        return arrParents.reverse();
    };

    /**
     * Add AdministrativeDivision.
     *
     * @param administrativeDivision
     * @param administrativeDivision.country {string} Country IRI.
     * @param administrativeDivision.name {string} AdministrativeDivision name property.
     * @param administrativeDivision.code {string} AdministrativeDivision code property.
     * @param administrativeDivision.parent {string} AdministrativeDivision parent IRI property.
     * @returns {Promise}
     */
    static addAdministrativeDivision = (administrativeDivision: {country: string, name: string, code: string, parent: string|null}): Promise<any> => {
        return axios.post(Api.URL_ADMINISTRATIVE_DIVISIONS, administrativeDivision)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update AdministrativeDivision.
     *
     * @param administrativeDivision
     * @param administrativeDivision.id {string} AdministrativeDivision ID.
     * @param administrativeDivision.name {string} AdministrativeDivision name property.
     * @param administrativeDivision.code {string} AdministrativeDivision code property.
     * @param administrativeDivision.parent {string} AdministrativeDivision parent IRI property.
     * @returns {Promise}
     */
    static updateAdministrativeDivision = (administrativeDivision: {id: string, name: string, code: string, parent: string|null}) => {
        return axios.put(Api.URL_ADMINISTRATIVE_DIVISIONS+"/"+administrativeDivision.id, {
            name: administrativeDivision.name,
            code: administrativeDivision.code,
            parent: administrativeDivision.parent
        })
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Remove administrativeDivision.
     *
     * @param id {string}
     * @returns {Promise}
     */
    static removeAdministrativeDivision = (id: string) => {
        return axios.delete(Api.URL_ADMINISTRATIVE_DIVISIONS+"/"+id)
            .then((resp) => {})
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }
    
    /**
     * Get treatment technology aeration-oxidation list.
     *
     * @param country {string}
     * @param name {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTreatmentTechnologyAerationOxidation = (country: string = '', name: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string, any> = {
            page: page,
        };
        if (country !== '') {
            params.country = country;
        }
        if (name !== '') {
            params.name = name
        }
        return axios
            .get(Api.URL_TREATMENT_TECHNOLOGY_AERATION_OXIDATION, { params })
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTreatmentTechnologyAerationOxidation(country, name, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add treatment technology aeration-oxidation providers.
     *
     * @param data
     * @param data.country {string} country IRI.
     * @param data.name {string} type property.
     * @returns {Promise}
     */
    static addTreatmentTechnologyAerationOxidation = (data: { country: string, name: string }) => {
        return axios.post(Api.URL_TREATMENT_TECHNOLOGY_AERATION_OXIDATION, data)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete treatment technology aeration-oxidation providers.
     *
     * @param id {string} id property.
     * @returns {Promise}
     */
    static removeTreatmentTechnologyAerationOxidation = (id: string) => {
        return axios.delete(Api.URL_TREATMENT_TECHNOLOGY_AERATION_OXIDATION + '/' + id)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update treatment technology aeration-oxidation providers.
     *
     * @param data
     * @param data.id {string}
     * @param data.name {string}
     *
     * @returns {Promise}
     */
    static updateTreatmentTechnologyAerationOxidation = (data: { id: string, name: string }) => {
        return axios.put(Api.URL_TREATMENT_TECHNOLOGY_AERATION_OXIDATION + "/" + data.id, {name: data.name })
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get treatment technology desalination list.
     *
     * @param country {string}
     * @param name {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTreatmentTechnologyDesalination = (country: string = '', name: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string, any> = {
            page: page,
        };
        if (country !== '') {
            params.country = country;
        }
        if (name !== '') {
            params.name = name
        }
        return axios
            .get(Api.URL_TREATMENT_TECHNOLOGY_DESALINATION, { params })
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTreatmentTechnologyDesalination(country, name, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add treatment technology desalination providers.
     *
     * @param data
     * @param data.country {string} country IRI.
     * @param data.name {string} type property.
     * @returns {Promise}
     */
    static addTreatmentTechnologyDesalination = (data: { country: string, name: string }) => {
        return axios.post(Api.URL_TREATMENT_TECHNOLOGY_DESALINATION, data)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete treatment technology desalination providers.
     *
     * @param id {string} id property.
     * @returns {Promise}
     */
    static removeTreatmentTechnologyDesalination = (id: string) => {
        return axios.delete(Api.URL_TREATMENT_TECHNOLOGY_DESALINATION + '/' + id)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update treatment technology desalination providers.
     *
     * @param data
     * @param data.id {string}
     * @param data.name {string}
     *
     * @returns {Promise}
     */
    static updateTreatmentTechnologyDesalination = (data: { id: string, name: string }) => {
        return axios.put(Api.URL_TREATMENT_TECHNOLOGY_DESALINATION + "/" + data.id, {name: data.name })
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get treatment technology filtration list.
     *
     * @param country {string}
     * @param name {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getTreatmentTechnologyFiltration = (country: string = '', name: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string, any> = {
            page: page,
        };
        if (country !== '') {
            params.country = country;
        }
        if (name !== '') {
            params.name = name
        }
        return axios
            .get(Api.URL_TREATMENT_TECHNOLOGY_FILTRATION, { params })
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getTreatmentTechnologyFiltration(country, name, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add technical assistance providers.
     *
     * @param data
     * @param data.country {string} country IRI.
     * @param data.name {string} type property.
     * @returns {Promise}
     */
    static addTreatmentTechnologyFiltration = (data: { country: string, name: string }) => {
        return axios.post(Api.URL_TREATMENT_TECHNOLOGY_FILTRATION, data)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete technical assistance providers.
     *
     * @param id {string} id property.
     * @returns {Promise}
     */
    static removeTreatmentTechnologyFiltration = (id: string) => {
        return axios.delete(Api.URL_TREATMENT_TECHNOLOGY_FILTRATION + '/' + id)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update technical assistance providers.
     *
     * @param data
     * @param data.id {string}
     * @param data.name {string}
     *
     * @returns {Promise}
     */
    static updateTreatmentTechnologyFiltration = (data: { id: string, name: string }) => {
        return axios.put(Api.URL_TREATMENT_TECHNOLOGY_FILTRATION + "/" + data.id, {name: data.name })
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add technical assistance providers.
     *
     * @param data
     * @param data.country {string} country IRI.
     * @param data.name {string} type property.
     * @param data.description {string} type property.
     * @param data.nationalId {string} type property.
     * @returns {Promise}
     */
    static addTechnicalAssistanceProviders = (data: { country: string, name: string, description: string, nationalId: string }) => {
        return axios.post(Api.URL_TECHNICAL_ASSISTANCE_PROVIDERS, data)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete technical assistance providers.
     *
     * @param id {string} id property.
     * @returns {Promise}
     */
    static removeTechnicalAssistanceProviders = (id: string) => {
        return axios.delete(Api.URL_TECHNICAL_ASSISTANCE_PROVIDERS + '/' + id)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update technical assistance providers.
     *
     * @param data
     * @param data.id {string}
     * @param data.name {string}
     * @param data.description {string}
     * @param data.nationalId {string}
     *
     * @returns {Promise}
     */
    static updateTechnicalAssistanceProviders = (data: { id: string, name: string, description: string, nationalId: string }) => {
        return axios.put(Api.URL_TECHNICAL_ASSISTANCE_PROVIDERS + "/" + data.id, {name: data.name, description: data.description, nationalId: data.nationalId })
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get geographical scope list.
     *
     * @param keyIndex {string}
     * @param name {string}
     * @param country {string}
     * @param page {number}
     * @param prevData {any}
     *
     * @returns {Promise<any>}
     */
    static getGeographicalScope = (keyIndex: string = '', name: string = '', country: string = '', page: number = 1, prevData: any = []): Promise<any> => {
        const params: Record<string, any> = {
            page: page,
        };
        if (keyIndex !== '') {
            params.type = keyIndex
        }
        if (name !== '') {
            params.name = name;
        }
        if (country !== '') {
            params.country = country;
        }
        return axios
            .get(Api.URL_GEOGRAPHICAL_SCOPE, { params })
            .then((res) => {
                prevData = prevData.concat(res.data);
                // Recursive load.
                let dataLen = res.data.length;
                if (dataLen > 0) {
                    return Api.getGeographicalScope(keyIndex, name, country, page + 1, prevData);
                }
                return prevData;
            })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Add Function carried out by TAP.
     *
     * @param data
     * @param data.country {string} country IRI.
     * @param data.keyIndex {string}.
     * @param data.name {string} type property.
     * @returns {Promise}
     */
    static addGeographicalScope = (data: { country: string, keyIndex: string, name: string }) => {
        return axios.post(Api.URL_GEOGRAPHICAL_SCOPE, data)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Delete function carried out by TAP.
     *
     * @param id {string} id property.
     * @returns {Promise}
     */
    static removeGeographicalScope = (id: string) => {
        return axios.delete(Api.URL_GEOGRAPHICAL_SCOPE + '/' + id)
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Update Geographical scope.
     *
     * @param data
     * @param data.id {string}
     * @param data.keyIndex {string}
     * @param data.name {string}
     *
     * @returns {Promise}
     */
    static updateGeographicalScope = (data: { id: string, keyIndex: string, name: string }) => {
        return axios.put(Api.URL_GEOGRAPHICAL_SCOPE + "/" + data.id, { keyIndex: data.keyIndex, name: data.name })
            .then((resp) => { })
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get mutatebles fields.
     *
     * @returns {Promise}
     */
    static getMutatebles = () => {

        return axios.get(Api.URL_MUTATEBLE)
            .then(resp => resp.data)
            .catch((info) => {
                return Api.displayErrorMessage(info);
            });
    }

    /**
     * Get API version.
     *
     * @returns {Promise}
     */
    static getVersion = () => {
        return axios.get(Api.URL_VERSION)
            .then(resp => resp.data)
            .catch((info) => {
                return Promise.reject(info);
            });
    }


    /**
     * Get list of household processes.
     *
     * @param page {number}
     * @param open {boolean}
     * @param administrativeDivision {string}
     * @param allPages
     * @param prevData
     *
     * @returns {Promise<any>}
     */
    static getHouseholdProcesses = (
        page: number = 1,
        open: boolean | undefined = undefined,
        administrativeDivision: string | undefined = undefined,
        allPages:boolean = false,
        prevData: any = []
    ): Promise<any> => {
        if (-1 === page) {
            allPages = true;
            page = 1;
        }
        const params: Record<string, any> = {
            page: page,
        };
        if (undefined !== open) {
            params.open = open
        }
        if (administrativeDivision) {
            params.administrativeDivision = administrativeDivision;
        }
        return axios.get(Api.URL_HOUSEHOLD_PROCESSES, { params })
            .then((res) => {
                prevData = prevData.concat(res.data);
                if (allPages) {
                    // Recursive load.
                    let dataLen = res.data.length;
                    if (dataLen > 0) {
                        return Api.getHouseholdProcesses(page + 1, open, administrativeDivision, allPages, prevData);
                    }
                    return prevData;
                }
                // Paginated response.
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => { return Api.displayErrorMessage(info); });
    }

    /**
     * return all households a user has
     *
     * @param page
     * @param parameters
     * @param allPages
     * @param prevData
     *
     * @returns {Promise}
     */
    static getUserHouseholds = (page: number = 1, parameters: any = {}, allPages:boolean = false, prevData: any = []): Promise<any> => {
        if (-1 === page) {
            allPages = true;
            page = 1;
        }
        const params: Record<string, any> = {
            page: page,
            ...parameters
        };
        return axios.get(Api.URL_HOUSEHOLD_PROCESSES + '/my_households', { params })
            .then(res => {
                prevData = prevData.concat(res.data);
                if (allPages) {
                    // Recursive load.
                    let dataLen = res.data.length;
                    if (dataLen > 0) {
                        return Api.getUserHouseholds(page + 1, parameters, allPages, prevData);
                    }
                    return prevData;
                }
                // Paginated response.
                return res?.data ?? Promise.reject('Empty data');
            })
            .catch((info) => { return Api.displayErrorMessage(info); });
    }

    /**
     * Get a household process by id.
     *
     * @param id {string}
     *
     * @returns {Promise<any>}
     */
    static getHouseholdProcess = (id: string): Promise<any> => {
        return axios.get(Api.URL_HOUSEHOLD_PROCESSES + '/' + id)
            .then((res) => { return res.data; })
            .catch((info) => { return Api.displayErrorMessage(info); });
    }

    /**
     * creates a new household questionnaire for the given process
     *
     * @param processId {string}
     *
     * @returns {Promise}
     */
    static addHouseholdSurvey = (processId: string) => {
        return axios
            .post(Api.URL_HOUSEHOLD_PROCESSES + '/' + processId + '/add_household')
            .then(res => { return res.data })
            .catch((info) => { return Api.displayErrorMessage(info); });
    }

    /**
     * closes the specified process, preventing more household questionnaires to be created
     *
     * @param processId {string}
     *
     * @returns {Promise}
     */
    static closeHouseholdProcess = (processId: string) => {
        return axios
            .put(Api.URL_HOUSEHOLD_PROCESSES + '/' + processId + '/close')
            .then(res => { return res.data })
            .catch((info) => { return Api.displayErrorMessage(info); });
    }

    /**
     * filter translations
     * 
     * @param iso_code {string}
     * @param search_in {string}
     * @param contains {string}
     * @param page {number}
     * 
     * @returns {Promise}
     */
    static searchTranslationStrings = (
        iso_code: string,
        search_in: 'all' | 'untranslated' | 'translated',
        contains: string,
        page: number | undefined = undefined
    ) => {
        let params = { search_in, contains, page };

        return axios
            .get(Api.URL_I18N + iso_code + "/translator", { params })
            .then(res => {return res.data})
            .catch(info => Api.displayErrorMessage(info));
    };

    /**
     * save a new translation
     * 
     * @param iso_code {string}
     * @param source {string}
     * @param target {string}
     * 
     * @returns {Propmise}
     */
    static saveTranslationString = (iso_code: string, source: string, target: string) => {
        return axios
            .post(Api.URL_I18N + iso_code, { source, target })
            .then(res => {return res.data})
            .catch(info => Api.displayErrorMessage(info));
    }

    /**
     * gets translation progress
     * 
     * @param iso_code {string}
     * 
     * @returns {Promise}
     */
    static getTranslationProgress = (iso_code: string) => {
        return axios
            .get(Api.URL_I18N + iso_code + "/progress")
            .then(res => {return res.data})
            .catch(info => Api.displayErrorMessage(info));
    }
}

/**
 * Entity end-points by internal classes.
 */
const entityEndpointByClass = {
    "App\\Entity\\AdministrativeDivision": Api.URL_ADMINISTRATIVE_DIVISIONS,
    "App\\Entity\\User": Api.URL_USERS,
    "App\\Entity\\Ethnicity": Api.URL_ETHNICITIES,
    "App\\Entity\\InstitutionIntervention": Api.URL_INSTITUTION_INTERVENTIONS,
    "App\\Entity\\TypeIntervention": Api.URL_TYPE_INTERVENTIONS,
    "App\\Entity\\FunderIntervention": Api.URL_FUNDER_INTERVENTIONS,
    "App\\Entity\\ProgramIntervention": Api.URL_PROGRAM_INTERVENTIONS,
    "App\\Entity\\Currency": Api.URL_CURRENCIES,
    "App\\Entity\\PumpType": Api.URL_PUMP_TYPES,
    "App\\Entity\\OfficialLanguage": Api.URL_OFFICIAL_LANGUAGES,
};

/**
 * Known entity classes.
 */
type entityClasses = keyof typeof entityEndpointByClass;

/**
 * Is this key a valid entity class?
 *
 * @param key
 * @returns {boolean}
 */
const isEntityClass = (key: string): key is entityClasses => {
    return entityEndpointByClass.hasOwnProperty(key);
};

/**
 * Measurement system units.
 */
type measurementSystemUnits = {
    "system.units.length": string;
    "system.units.volume": string;
    "system.units.flow": string;
    "system.units.concentration": string;
};
/**
 * Measurement system units types.
 */
type measurementSystemUnitTypes = keyof measurementSystemUnits;

/**
 * Is this measurement unit type valid?
 *
 * @todo Replace array with measurementSystemUnitTypes?
 *
 * @param key
 * @returns {boolean}
 */
const isMeasurementSystemUnitType = (key: string): key is measurementSystemUnitTypes => {
    return [
        "system.units.length",
        "system.units.volume",
        "system.units.flow",
        "system.units.concentration",
    ].includes(key);
};

export default Api;
