/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Is required value defined?
 *
 * @param value
 * @returns {boolean}
 */
export const required = (value: any) => {
  return ![null, undefined, ""].includes(value);
};

/**
 * Is a valid month?
 *
 * @param str_month
 * @returns {boolean}
 */
export const validMonth = (str_month: string) => {
  const month = parseInt(str_month, 10);
  if (`${month}` !== str_month) return false;
  return month >= 1 && month <= 12;
};

/**
 * Is a valid year?
 *
 * @param str_year
 * @returns {boolean}
 */
export const validYear = (str_year: string) => {
  const current_year = new Date().getFullYear();
  const year = parseInt(str_year, 10);
  if (`${year}` !== str_year) return false;
  return year >= current_year - 50 && year <= current_year + 50;
};

/**
 * Is a valid number?
 *
 * @param str_num
 * @returns {boolean}
 */
export const validNumber = (str_num: string) => {
  return (str_num == '') ? true : `${parseFloat(str_num)}` === str_num;
}

/**
 * Is a valid phone number?
 *
 * @param str_num
 * @returns {boolean}
 */
export const validPhone = (str_num: string) => {
  return (str_num == '') ? true : /^(\+)?[0-9 ,]*[,~0-9]{2,3}$/.test(str_num);
}

/**
 * Is a valid email?
 *
 * @param str_num
 * @returns {boolean}
 */
export const validEmail = (str_num: string) => {
  return (str_num == '') ? true : /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(str_num);
}

/**
 * Is a valid input type?
 *
 * @param type
 * @returns {boolean}
 */
export const isValidInput = (type: string) => (value: string) => {
  if (type === 'number') return validNumber(value);
  if (type === 'phone') return validPhone(value);
  if (type === 'email') return validEmail(value);
  return true;
}
