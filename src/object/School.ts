/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {
    emptySiasarDate,
    SiasarDate,
    ReferenceForm,
    SiasarReference,
    emptySiasarCountry,
    emptySiasarAdministrativeDivision
} from "./Generic";

export interface SchoolContactInterface {
    id?: string;
    field_changed?: SiasarDate;
    field_created?: SiasarDate;
    field_name: string;
    field_occupation: string;
    field_phone: string;
    field_mail: string;
    field_consent_use: boolean;
}

/**
 * Get empty school contact data.
 *
 * @returns {Object}
 * @deprecated
 */
export function emptySchoolContact(): SchoolContactInterface {
    return {
        field_name: '',
        field_occupation: '',
        field_phone: '',
        field_mail: '',
        field_consent_use: false
    }
}

export interface SchoolInterface {
    id?: string,
    field_changed?: SiasarDate,
    field_created?: SiasarDate,
    field_creator?: SiasarReference,
    field_country: SiasarReference,
    field_region: SiasarReference,
    field_reference: string,
    field_status?: string,
    field_editors?: SiasarReference[],
    field_questionnaire_date: SiasarDate,
    field_questionnaire_interviewer: string,
    field_questionnaire_contacts: SchoolContactInterface | ReferenceForm[],
    field_location_lat: number,
    field_location_lon: number,
    field_location_alt: number,
    field_minor_local_entity: string,
    field_mayor_local_entity: string,
    field_regional_entity: string,
    field_school_name: string,
    field_school_code: string,
    field_school_type: string[],
    field_school_type_other: string[],
    field_staff_total_number_of_women: number,
    field_staff_total_number_of_men: number,
    field_student_total_number_of_female: number,
    field_student_total_number_of_male: number,
    field_rural_communities_served: string,
    field_national_classification: string,
    field_school_observations: string,
    field_have_water_supply_system: string[],
    field_name_water_supply_system: string[],
    field_functioning_of_water_system: string,
    field_water_for_drinking: boolean,
    field_main_source_for_drinking: string,
    field_reasons_not_using_water_system: string,
    field_reasons_not_using_water_system_other: string,
    field_main_source_is_water_available_at_school: boolean,
    field_school_have_toilets: boolean,
    field_type_toilets: string,
    field_girls_only_toilets_total: number,
    field_girls_only_toilets_usable: number,
    field_boys_only_toilets_total: number,
    field_boys_only_toilets_usable: number,
    field_common_use_toilets_total: number,
    field_common_use_toilets_usable: number,
    field_how_clean_student_toilets: string,
    field_limited_mobility_vision_toilet: boolean,
    field_handwashing_facilities: boolean,
    field_handwashing_facilities_soap_water: string,
    field_menstrual_facilities_soap_water: string,
    field_menstrual_covered_bins: boolean
}

/**
 * Get empty school data.
 *
 * @returns {Object}
 * @deprecated
 */
export function emptySchool(): SchoolInterface {
    return {
        field_country: emptySiasarCountry(),
        field_region: emptySiasarAdministrativeDivision(),
        field_reference: '',
        field_questionnaire_date: emptySiasarDate(),
        field_questionnaire_interviewer: '',
        field_questionnaire_contacts: emptySchoolContact(),
        field_location_lat: 0,
        field_location_lon: 0,
        field_location_alt: 0,
        field_minor_local_entity: '',
        field_mayor_local_entity: '',
        field_regional_entity: '',
        field_school_name: '',
        field_school_code: '',
        field_school_type: [],
        field_school_type_other: [''],
        field_staff_total_number_of_women: 0,
        field_staff_total_number_of_men: 0,
        field_student_total_number_of_female: 0,
        field_student_total_number_of_male: 0,
        field_rural_communities_served: '',
        field_national_classification: '',
        field_school_observations: '',
        field_have_water_supply_system: [],
        field_name_water_supply_system: [],
        field_functioning_of_water_system: '',
        field_water_for_drinking: false,
        field_main_source_for_drinking: '',
        field_reasons_not_using_water_system: '',
        field_reasons_not_using_water_system_other: '',
        field_main_source_is_water_available_at_school: false,
        field_school_have_toilets: false,
        field_type_toilets: '',
        field_girls_only_toilets_total: 0,
        field_girls_only_toilets_usable: 0,
        field_boys_only_toilets_total: 0,
        field_boys_only_toilets_usable: 0,
        field_common_use_toilets_total: 0,
        field_common_use_toilets_usable: 0,
        field_how_clean_student_toilets: '',
        field_limited_mobility_vision_toilet: false,
        field_handwashing_facilities: false,
        field_handwashing_facilities_soap_water: '',
        field_menstrual_facilities_soap_water: '',
        field_menstrual_covered_bins: false
    }
}
