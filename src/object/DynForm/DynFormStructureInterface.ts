/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {DynFormStructureMetaInterface} from "@/objects/DynForm/DynFormStructureMetaInterface";
import {DynFormStructureFieldInterface} from "@/objects/DynForm/DynFormStructureFieldInterface";

export interface DynFormStructureInterface {
    id: string,
    type?: string,
    requires?: Array<string>
    // The form must honor the field required properties.
    forceIsRequired?: boolean
    // The form use this form level. If null the form don't use any level.
    formLevel?: number
    // The form use SDG, if null or false the form don't use SDG.
    formSdg?: boolean
    // True if this form must render content without editable fields.
    readonly?: boolean
    // True if the form is subform.
    isSubForm?: boolean
    fields: {
        [key: string]: DynFormStructureFieldInterface
    }
    title: string,
    description?: string,
    endpoint?: Object,
    meta: DynFormStructureMetaInterface,
}
