/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

export interface DynFormStructureFieldInterface {
    id: string
    type: string
    label: string
    description: string
    indexable: boolean
    internal: boolean
    deprecated: boolean
    settings: {
        required: boolean
        multivalued: boolean
        disabled?: boolean
        _inMultivalued?: boolean
        _multiIndex?: number
        weight: number
        meta: {
            catalog_id?: string
            level?: number
            sdg?: boolean
            hidden?: boolean
            conditional?: any
            disabled?: boolean
            autocomplete?: boolean
            focus?: boolean
            displayId?: boolean
            filtered?: boolean
            // Only used by thread_mail_reference field type.
            parent_data?: any
        }
        unknowable?: boolean
        sort: boolean
        filter: boolean
        "max-length"?: number
        options?: {
            [key: string]: any
        }
        min?: number
        max?: number
        precision?: number
        scale?: number
        allow_extension?: string[]
        maximum_file_size?: number
        true_label?: string
        false_label?: string
        show_labels?: boolean
        subform?: string
        lat_field?: string
        lon_field?: string
        editor_field?: string
        update_field?: string
    }
}
