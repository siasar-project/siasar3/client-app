/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

export enum DynFormFieldInstanceTypes {
    Group = "group",
    Field = "field",
}

export interface DynFormFieldInstanceInterface {
    // Field ID.
    id: string
    // Instance type.
    type: DynFormFieldInstanceTypes
    conditional: any
    multivalued: boolean,
    // Component
    instance: any
    // Current visibility status.
    visible: boolean
    /**
     * Get current visibility usable value from Component.
     *
     * @param path {string} Completed field name path.
     * @returns {any}
     */
    getValue: Function
    /**
     * Change visibility to component.
     *
     * @param isVisible {boolean}
     */
    setVisible: Function
    /**
     * if declared, runs on dynform handleChange. 
     */
    onFormChange?: Function
    /**
     * if declared, runs on dynform processVisibility.
     */
    onVisibilityUpdate?: Function
    /**
     * if declared, runs on field_group handleAccordionHeadingClick.
     */
    onGroupToggle?: Function
}
