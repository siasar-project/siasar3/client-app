/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {DynFormStructureMetaFieldgroupInterface} from "@/objects/DynForm/DynFormStructureMetaFieldgroupInterface";

export interface DynFormStructureMetaInterface {
    title: string
    version: string
    allow_sdg?: boolean
    icon_field?: string
    title_field?: string
    table_fields?: Array<string>
    parent_form?: string
    field_groups: Array<DynFormStructureMetaFieldgroupInterface>
    point_associated?: boolean
}
