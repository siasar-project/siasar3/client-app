/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import {DynFormStructureFieldInterface} from "@/objects/DynForm/DynFormStructureFieldInterface";
import {DynFormFieldInstanceInterface} from "@/objects/DynForm/DynFormFieldInstanceInterface";

export interface DynFormComponentPropsInterface {
    id: string
    structure: DynFormStructureInterface
    definition: DynFormStructureFieldInterface
    value?: any
    index?: number
    formData: any
    // Store Component instances to do post-precess.
    formFields?: {[key: string]: DynFormFieldInstanceInterface};
    onFormatValue?: any
    onGetValuePropertyName?: any
    onGetDefaultValue?: any
    onGetValueMainProperty?: any
    onGetValueTargetProperty?: any
    onRenderHeader?: any
    onRemove?: any
    onChange?: Function
    triggerOpenModal?: boolean
}
