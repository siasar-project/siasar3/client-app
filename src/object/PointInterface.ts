/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {PointStatus} from "@/objects/PointStatus";
import {AdmDivisionMetaInterface} from "@/objects/AdmDivisionMetaInterface";
import {PointGraphInterface} from "@/objects/Graph/PointGraphInterface";

export interface PointInterface {
    id?: string
    status_code?: PointStatus
    status?: string
    version?: string
    updated?: string
    administrative_division?: AdmDivisionMetaInterface[]
    community?: string[]
    wsp?: string[]
    wsystem?: string[]
    team?: string[]
    allowed_actions?: string[]
    notes?: {
        warnings?: number
        errors?: number
    },
    image?: string
    inquiry_relations?: PointGraphInterface
}
