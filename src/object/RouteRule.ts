/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Route firewall rule.
 */
export interface RouteRule {
    name: string
    pattern: RegExp
    environment?: string
    // If no permissions defined, need session allow access or not by identified user.
    needSession?: boolean
    // If an option have permission, then needSession it's TRUE.
    permissions?: string[]
    // Allow this page in offline mode.
    offline?: boolean
}
