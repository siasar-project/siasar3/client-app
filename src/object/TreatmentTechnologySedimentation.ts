/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { Country } from './Country';

export interface TreatmentTechnologySedimentation {
    id?: string;
    // created?: Date;
    // updated?: Date;
    country?: Country | null;
    name: string;
}

/**
 * Get empty FunderIntervention.
 *
 * @returns {Object}
 */
export function emptyTreatmentTechnologySedimentationData(): TreatmentTechnologySedimentation {
    return {
        "country": null,
        "name": "",
    }
}
