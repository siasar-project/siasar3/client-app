/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { Country } from './Country';
import {SystemPermission} from "@/utils/permissionsManager";
import { getTimezone } from '@/utils/siasar';

export interface User {
    id?: string;
    active?: boolean;
    created?: Date;
    email?: string;
    roles?: string[];
    password?: string;
    updated?: Date;
    username?: string;
    country?: Country | null;
    administrativeDivisions: any;
    permissions:Array<SystemPermission>;
    session?: UserSession;
    gravatar?: string;
    language?: string;
    countryId?: string;
    countryFlag?: string;
    countryFlagId?: string;
    timezone: string;
}

export interface UserSession {
    refresh_token?: string;
    refresh_token_expire?: number;
    token?: string;
    token_expire?: number;
}

export interface UserLogin {
    username?: string;
    password?: string;
    refresh_token?: string;
}

export interface Roles {
    description: string
    label: string
    permissions: string[]
}

/**
 * Get empty user info.
 *
 * @returns {Object}
 */
export function emptyUserData(): User {
    return {
        permissions: [],
        "username": "",
        "password": "",
        "email": "",
        "country": null,
        "administrativeDivisions": [],
        "roles": [],
        "active": true,
        "timezone": getTimezone()
    }
}
