/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {smGetSession} from "@/utils/sessionManager";

export interface Mail {
    id?: string;
    from?: string,
    global: boolean,
    to: string[],
    subject: string,
    body: string,
    attachments: string[],
    thread: string[],
}

/**
 * Get empty mail info.
 *
 * @returns {Object}
 */
export function emptyMailData(): Mail {
    let session = smGetSession();
    return {
        from: session?.id,
        global: false,
        to: [],
        subject: "",
        body: "",
        attachments: [],
        thread: [],
    }
}
