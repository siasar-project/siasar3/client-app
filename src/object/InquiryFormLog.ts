/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

export interface InquiryFormLog {
    id?: string;
    // created?: Date;
    // updated?: Date;
    createdAt?: string;
    country?: string;
    message: string,
    context?: any,
    level: number,
    levelName?: string,
    extra?: any,
    formId: string,
    recordId: string,
}

/**
 * Get empty InstitutionIntervention.
 *
 * @returns {Object}
 */
export function emptyInquiryFormLogData(): InquiryFormLog {
    return {
        "createdAt": "",
        "message": "",
        "context": {},
        "level": 0,
        "levelName": "",
        "extra": {},
        "formId": "",
        "recordId": "",
    }
}
