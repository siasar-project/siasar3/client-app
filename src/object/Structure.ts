/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

export interface StructureFieldMeta {
  catalog_id: string;
  conditional?: any;
  level?: number;
  sdg?: boolean;
  field_other?:string;
}

export interface StructureFieldSettings {
  max:number
  meta: StructureFieldMeta
  min: number
  multivalued: boolean
  required: string
  weight: number
}

export interface StructureField {
  description: string
  id: string | number
  indexable: boolean
  internal: boolean
  label: string
  settings: StructureFieldSettings
  type: string
}

// export interface StructureField {
//   field_id?: string;
//   id: string | number;
//   title: string;
//   fields?: StructureField[];
//   weight?: number;
// }

export interface StructureRequestFieldGroups {
  id: number;
  title: string;
  help: string;
  children: StructureField;
}

export interface StructureRequestMeta {
  field_groups?: StructureRequestFieldGroups[];
  title: string;
  version: string;
  allow_sdg?: boolean;
}

export interface StructureRequest {
  description: string;
  fields: StructureField;
  id: string;
  meta: StructureRequestMeta;
  title: string;
  type: string;
}
