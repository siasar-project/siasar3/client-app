/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

export interface SiasarDate {
    value: string;
    timezone: string;
}

/**
 * Get empty SIASAR date.
 *
 * @returns {SiasarDate}
 */
export function emptySiasarDate() {
    return {
        value: '',
        timezone: ''
    }
}

export interface ReferenceForm {
    value: string
    form: string
}

export interface SiasarReference {
    value: string
    class: string
}

export interface SiasarCurrency {
    value: string;
    class: 'App\\Entity\\Currency';
    amount: number;
}

export interface SiasarUnit {
    value: string;
    key: string;
}

/**
 * Get empty SIASAR country.
 *
 * @returns {Object}
 */
export function emptySiasarCountry() {
    return {
        value: '',
        class: 'App\\Entity\\Country'
    }
}

/**
 * Get empty Siasar Administrative Division.
 *
 * @returns {Object}
 */
export function emptySiasarAdministrativeDivision() {
    return {
        value: '',
        class: 'App\\Entity\\AdministrativeDivision'
    }
}
