/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import {Material} from "@/objects/Material";
import {FunderIntervention} from "@/objects/FunderIntervention";
import {Currency} from "@/objects/Currency";
import {InstitutionIntervention} from "@/objects/InstitutionIntervention";
import {WaterQualityEntity} from "@/objects/WaterQualityEntity";
import {FunctionsCarriedOutTap} from "@/objects/FunctionsCarriedOutTap";
import {OfficialLanguage} from "@/objects/OfficialLanguage";
import {TypeIntervention} from "@/objects/TypeIntervention";
import {CommunityService} from "@/objects/CommunityService";
import { GeographicalScope } from '@/objects/GeographicalScope';
import {TypologyChlorinationInstallation} from "@/objects/TypologyChlorinationInstallation";
import {TypeTap} from "@/objects/TypeTap";
import {WaterQualityInterventionType} from "@/objects/WaterQualityInterventionType";
import {FunctionsCarriedOutWsp} from "@/objects/FunctionsCarriedOutWsp";
import {TypeHealthcareFacility} from "@/objects/TypeHealthcareFacility";
import {InterventionStatus} from "@/objects/InterventionStatus";
import {TreatmentTechnologyCoagFloccu} from "@/objects/TreatmentTechnologyCoagFloccu";
import {TreatmentTechnologySedimentation} from "@/objects/TreatmentTechnologySedimentation";
import {TechnicalAssistanceProvider} from "@/objects/TechnicalAssistanceProvider";
import {PumpType} from "@/objects/PumpType";
import {SpecialComponent} from "@/objects/SpecialComponent";
import {AdministrativeDivision} from "@/objects/AdministrativeDivision";
import {ProgramIntervention} from "@/objects/ProgramIntervention";
import {DisinfectingSubstance} from "@/objects/DisinfectingSubstance";
import {DistributionMaterial} from "@/objects/DistributionMaterial";
import {TreatmentTechnologyFiltration} from "@/objects/TreatmentTechnologyFiltration";
import {TreatmentTechnologyDesalination} from "@/objects/TreatmentTechnologyDesalination";
import {TreatmentTechnologyAerationOxidation} from "@/objects/TreatmentTechnologyAerationOxidation";
import {Ethnicity} from "@/objects/Ethnicity";
import {DefaultDiameter} from "@/objects/DefaultDiameter";
import {FileInterface} from "@/objects/FileInterface";

export interface Country {
  id: string;
  code: string;
  name: string;
  flag: FileInterface;
  mainOfficialLanguage: string;
  officialLanguages?: OfficialLanguage[];
  materials?: Material[];
  specialComponents?: SpecialComponent[];
  administrativeDivisions?: AdministrativeDivision[];
  distributionMaterials?: DistributionMaterial[];
  mainUnitLength: string;
  institutionInterventions?: InstitutionIntervention[];
  waterQualityEntities?: WaterQualityEntity[];
  funderInterventions?: FunderIntervention[];
  unitLengths: string;
  pumpTypes?: PumpType[];
  ethnicities?: Ethnicity[];
  programInterventions?: ProgramIntervention[];
  defaultDiameters?: DefaultDiameter[];
  functionsCarriedOutTap?: FunctionsCarriedOutTap[];
  typeInterventions?: TypeIntervention[];
  communityServices?: CommunityService[];
  typologyChlorinationInstallations?: TypologyChlorinationInstallation[];
  typeTaps?: TypeTap[];
  waterQualityInterventionTypes?: WaterQualityInterventionType[];
  functionsCarriedOutWsps?: FunctionsCarriedOutWsp[];
  typeHealthcareFacilities?: TypeHealthcareFacility[];
  interventionStatuses?: InterventionStatus[];
  treatmentTechnologyCoagFloccus?: TreatmentTechnologyCoagFloccu[];
  treatmentTechnologySedimentations?: TreatmentTechnologySedimentation[];
  technicalAssistanceProviders?: TechnicalAssistanceProvider[];
  treatmentTechnologyFiltration?: TreatmentTechnologyFiltration[];
  disinfectingSubstances?: DisinfectingSubstance[];
  mainUnitVolume: string;
  unitVolumes: string;
  mainUnitFlow: string;
  unitFlows: string;
  mainUnitConcentration: string;
  mutatebleFields: any;
  unitConcentrations: string;
  currencies?: Currency[];
  divisionTypes: any;
  formLevel: {[key: string]: {level:number; sdg?:boolean}};
  deep?: number;
  treatmentTechnologyAerationOxidation?: TreatmentTechnologyAerationOxidation[];
  treatmentTechnologyDesalination?: TreatmentTechnologyDesalination[];
  geographicalScopes?: GeographicalScope[];
}
