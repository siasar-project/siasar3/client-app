/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {i18nPlaceholderFormat} from "../utils/i18n";

/**
 * Test string @ replacement.
 */
test('Replace place holder "@" formats', () => {
    expect(i18nPlaceholderFormat("hola @world", {"@world": "mundo"})).toBe("hola mundo");
});

/**
 * Test string : replacement.
 */
test('Replace place holder ":" formats', () => {
    expect(i18nPlaceholderFormat("hola :world", {":world": "mundo"})).toBe("hola mundo");
});

/**
 * Test string % replacement.
 */
test('Replace place holder "%" formats', () => {
    expect(i18nPlaceholderFormat("hola %world", {"%world": "mundo"})).toBe("hola `mundo`");
});

/**
 * Test string multiple replacements.
 */
test('Replace place holder multiple formats', () => {
    expect(i18nPlaceholderFormat("@hi @world", {"@hi": "hola", "@world": "mundo"})).toBe("hola mundo");
});

/**
 * Test string replacement with html escaped.
 */
test('Test string replacement with html escaped.', () => {
    expect(i18nPlaceholderFormat("@hi @world", {"@hi": "<hola>", "@world": "mundo"})).toBe("&lt;hola&gt; mundo");
});
