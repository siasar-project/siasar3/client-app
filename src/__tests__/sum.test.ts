/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

export {}

/**
 * Example function.
 *
 * @param a {number}
 * @param b {number}
 * @returns {number}
 */
function sum(a: number, b: number) {
    return a + b;
}

/**
 * Test that the test environment is working.
 */
test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});
