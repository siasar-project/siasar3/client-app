/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import { useEffect, useState } from "react";
import { useRouter } from 'next/router';

/**
 * Resolves query or returns null
 *
 * @param defaultValue
 * @returns {any}
 */
export default function useQuery(defaultValue: any = {}) {
  const [query, setQuery] = useState(defaultValue);
  const router = useRouter();
  useEffect( () =>{
    const hasQueryParams =
      /\[.+\]/.test(router.route) || /\?./.test(router.asPath);
    const ready = !hasQueryParams || Object.keys(router.query).length > 0;
    if (ready){
      setQuery({...defaultValue, ...router.query});
    }
  },[router])
  return query;
}
