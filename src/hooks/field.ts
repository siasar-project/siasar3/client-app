/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { useState, useEffect } from 'react';

type Options = {
  start: boolean;
  message: string;
}

/**
 * extract the default field functionality
 *
 * @param initialValue
 * @param validation
 * @param options
 * @returns {Array}
 */
export function useField<T>(initialValue: T | (()=> T), validation: Function, options: Options) {
  const initial = toSupportedValue<T>(initialValue);

  const [ defaultValue, _ ] = useState(initial);
  const [ value, setValue ] = useState(initial);
  const [ error, setError ] = useState('');
  const message = options.message ?? 'Invalid Value'

  /**
   * On use effect.
   */
  useEffect( ()=>{
    if (options.start) {
      onChange(defaultValue)
    }
  },[]);

  /**
   * On change handler.
   *
   * @param val
   */
  const onChange = (val: T) => {
    setValue(val);
    if (!validation(val)) {
      setError(message);
    }else{
      setError('');
    }
  }

  return [defaultValue, value, error, onChange] as const;
}

/**
 * Initialize supported value.
 *
 * @param initialValue
 * @returns {any}
 */
function toSupportedValue<T>(initialValue:T | (()=> T) ): T {
  if (initialValue instanceof Function){
    return initialValue();
  }
  return initialValue;
}

