/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/**
 * Launch confirmable.
 *
 * @param message
 * @returns {any}
 *
 * @constructor
 */
export const Confirmable = (message: string) => (target: Object, key: string | symbol, descriptor: PropertyDescriptor) => {
    const fn = descriptor.value
    /**
     * Confirmable.
     *
     * @param args
     * @returns {any}
     */
    descriptor.value = function (...args: any[]) {
        const allow = confirm(message);
        if (allow) {
            return fn.apply(this, args)
        } else {
            return null;
        }
    }
    return descriptor;
}

/**
 * Confirmable.
 *
 * @param message
 * @returns {any}
 */
export const fnConfirmable = (message: string) => (fn: Function) => {

    /**
     * Execute confirmable action.
     *
     * @param args
     * @returns {any}
     */
    const hoc = function (...args: any[]) {
        const allow = confirm(message);
        if (allow) {
            return fn.apply(this, args);
        } else {
            return null;
        }
    }
    return hoc;
}
