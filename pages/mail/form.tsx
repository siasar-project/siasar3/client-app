/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {DynForm} from "@/components/DynForm/DynForm"
import MainLayout from "@/components/layout/MainLayout"
import {dmGetFormRecord, dmGetFormStructure} from "@/utils/dataManager"
import t from "@/utils/i18n"
import {faRotateLeft} from "@fortawesome/free-solid-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {Container} from "@material-ui/core"
import router, {useRouter} from "next/router"
import React, {ReactElement, useEffect, useState} from "react"
import {faPaperPlane} from "@fortawesome/free-solid-svg-icons/faPaperPlane";
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import {emptyMailData, Mail} from "@/objects/Mail";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {smHasPermission} from "@/utils/sessionManager";
import {dmAddFormRecord, dmUpdateFormRecord} from "@/utils/dataManager/FormRecordWrite";

interface DynFormMailInterface {
    field_id: {value: string | undefined},
    field_from: {value: string | undefined, class: string | undefined},
    field_global: {value: boolean | undefined},
    field_to?: any[] | null,
    field_subject: {value: string | undefined},
    field_body: {value: string | undefined},
    field_attachments?: any[] | null,
    field_thread?: any[] | null,
}

/**
 * User edit page.
 *
 * @constructor
 */
const MailForm = () => {

    const query = useRouter();
    const { id } = query.query;

    // Empty DynFormRecordInterface.
    const emptyDynFormMail: DynFormMailInterface = {
        field_id: {value: ''},
        field_from: {value: '', class: 'App\\Entity\\User'},
        field_global: {value: false},
        field_to: [],
        field_subject: {value: ''},
        field_body: {value: ''},
        field_attachments: [],
        field_thread: [],
    }
    // Empty DynFormStructureInterface.
    const emptyDynFormStructure: DynFormStructureInterface = {
        id: 'form.mail',
        fields: {},
        title: '',
        meta: {
            title: '',
            version: '',
            field_groups: []
        }
    }
    // User state.
    const [mail, setMail] = useState<Mail>(emptyMailData());
    // Form state.
    const [formMail, setFormMail] = useState(emptyDynFormMail);
    // Form structures.
    const [formStructure, setFormStructure] = useState<DynFormStructureInterface>(emptyDynFormStructure);
    // Loading state
    const [isLoading, setIsLoading] = useState(true);
    // Empty DynFormRecordInterface.
    const emptyDynFormRecord: DynFormRecordInterface = {
        formId: 'form.mail',
        id: id + "" || "",
    }

    // React to ID set.
    useEffect(() => {
        setIsLoading(true);
        dmGetFormStructure('form.mail')
            .then((structure: DynFormStructureInterface) => {
                if (id) {
                    dmGetFormRecord({formId: 'form.mail', id: id + ""})
                        .then((data:any) => {
                            let mailSource = {
                                id: data.id,
                                from: data.field_from.value,
                                global: data.field_global.value,
                                "to": data.field_to,
                                subject: data.field_subject.value,
                                body: data.field_body.value,
                                attachments: data.field_attachments,
                                thread: data.field_thread,
                            };
                            setFormMail(data);
                            setMail(mailSource);
                            updateFormStructure(structure, mailSource);
                            setIsLoading(false);
                        })
                        .catch(() => {
                            setIsLoading(false);
                        });
                } else {
                    let initData: Mail = emptyMailData();
                    setMail(initData);
                    structure.fields.field_global.settings.meta.disabled = !smHasPermission('can send global mail');
                    updateFormStructure(structure, initData);
                    setFormMail({
                        field_id: {value: initData.id},
                        field_from: {value: initData.from, class: 'App\\Entity\\User'},
                        field_global: {value: initData.global},
                        field_to: initData.to,
                        field_subject: {value: initData.subject},
                        field_body: {value: initData.body},
                        field_attachments: initData.attachments,
                        field_thread: initData.thread,
                    });
                    setIsLoading(false);
                }
            })
            .catch(() => {
                setIsLoading(false);
            });
    }, [id])

    /**
     * Update form display to the mail loaded.
     *
     * @param structure {DynFormStructureInterface}
     * @param mailSource {any}
     */
    const updateFormStructure = (structure: DynFormStructureInterface, mailSource:any) => {
        // Update form structure.
        if (isNewMail()) {
            // Form to new mail.
            structure.fields.field_thread.internal = true;
        } else {
            // Form to view mail.
            structure.fields.field_global.settings.meta.disabled = true;
            if ("0" === mailSource.global) {
                // Hide global field.
                structure.fields.field_global.deprecated = true;
            }
            structure.fields.field_to.settings.meta.disabled = true;
            structure.fields.field_subject.settings.meta.disabled = true;
            structure.fields.field_body.settings.meta.disabled = true;
            structure.fields.field_attachments.settings.meta.disabled = true;
            structure.fields.field_thread.settings.meta.parent_data = {
                subject: "Re: "+mailSource.subject,
                to: mailSource.to,
            };
        }
        setFormStructure(structure);
    }

    /**
     * Is this a new mail?
     *
     * @returns {boolean}
     */
    const isNewMail = () => {
        return undefined === id;
    }

    // Form buttons.
    let buttons: ReactElement[] = [
        (<button type="submit" key="button-update" className={"btn btn-primary green"} value={t("Send")} disabled={isLoading} hidden={!isNewMail()}><FontAwesomeIcon icon={faPaperPlane} /> {t("Send")}</button>),
        (<button
            type="button"
            key="button-return"
            onClick={() => router.push('/mail/list')}
            className={"btn btn-primary float-right"}
            value={(mail && mail.id) ? t("Return") : t("Cancel")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faRotateLeft} /> {(mail && mail.id) ? t("Return") : t("Cancel")}
        </button>),
    ];

    /**
     * Is visble a button?
     *
     * @param record {any}
     * @param button {ReactElement}
     * @returns {boolean}
     */
     const buttonsVisibility = (record: any, button:ReactElement) => {
        return button.key === "button-return" || !(mail && mail.id);
    }

    /**
     * Handle form submit.
     *
     * @param event
     *
     * @returns {any}
     */
     const handleButtonSubmit = (event: any) => {
        setIsLoading(true);
        // Is an edited mail?
        if(mail && mail.id) {
            // Yes, update it.
            // Dead code !!
            let sendToApi:DynFormRecordInterface = emptyDynFormRecord;
            sendToApi.data = {field_thread: formMail.field_thread};
            dmUpdateFormRecord(sendToApi)
                .then((res:any) => {
                    router.push('/mail/list');
                    setIsLoading(false);
                })
                .catch(() => {
                    setIsLoading(false);
                });
        }
        else {
            // No, add new mail.
            let sendToApi:DynFormRecordInterface = emptyDynFormRecord;
            sendToApi.data = formMail;
            delete(sendToApi.data.field_id);
            return dmAddFormRecord(sendToApi)
                .then(() => {
                    router.push('/mail/list');
                }).catch(() => setIsLoading(false));
        }
    }

    /**
     * Save form after thread update.
     *
     * @param event
     */
    const handleDynformChange = (event:any) => {
         if (event.field && event.field.startsWith('field_thread')) {
             setIsLoading(true);
             let sendToApi:DynFormRecordInterface = emptyDynFormRecord;
             sendToApi.data = {field_thread: formMail.field_thread};
             dmUpdateFormRecord(sendToApi)
                 .then((res:any) => {
                     setIsLoading(false);
                     // Load to force read mark to be set.
                     // If a user respond in the thread, he'll remove the read mark.
                     dmGetFormRecord({formId: 'form.mail', id: id + ""}).catch(() => {});
                 })
                 .catch(() => {
                     setIsLoading(false);
                 });
         }
    }

    return (
        <MainLayout>
            <div className={'dynform-edit'}>
                <Container>
                    <React.StrictMode>
                        <DynForm
                            key={new Date().getTime()}
                            structure={formStructure}
                            buttonsVisibility={buttonsVisibility}
                            buttons={buttons}
                            onSubmit={handleButtonSubmit}
                            onChange={handleDynformChange}
                            value={formMail}
                            isLoading={isLoading}
                        />
                    </React.StrictMode>
                </Container>
            </div>
        </MainLayout>
    )
}

export default MailForm
