/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {ReactElement, useEffect, useState} from "react";
import Container from "@material-ui/core/Container";
import MainLayout from "../src/components/layout/MainLayout";
import {smGetSession, smHasPermission, smIsValidSession} from "@/utils/sessionManager";
import t from "@/utils/i18n";
import BasicCreationButtons from "@/components/Buttons/BasicCreationButtons";
import router from "next/router";
import {dmGetInquires} from "@/utils/dataManager";
import Pager from "@/components/common/Pager";
import CardContainer from "@/components/Cards/CardContainer";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faEye} from "@fortawesome/free-solid-svg-icons";
import {User} from "@/objects/User";
import SystemStats from "@/components/layout/SystemStats";
import {getDestinationQueryParameter} from "@/utils/siasar";
import {DynForm} from "@/components/DynForm/DynForm";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import getOwnSurveysFilterSchema from "../src/Settings/dynforms/OwnSurveysFilterSchema";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";

/**
 * Dashboard page.
 *
 * @constructor
 */
const Dashboard = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState(1);
    const [records, setRecords] = useState([]);
    // Filter state.
    const [formState, setFormState] = useState(lcmGetItem(
        'own_surveys_filter',
        {"field_type": []}
    ));
    const [listedItems, setListedItems] = useState(0);

    /**
     * update record list
     */
    useEffect(() => {
        if (!isLoading) {
            return;
        }

        let currentUser:User | null = smGetSession();

        dmGetInquires({page: page, formType: formState.field_type.map((item:any) => {return item.value;}), user_id: currentUser?.id})
            .then((res) => {
                setRecords(res);
                setListedItems(res.length);
                setIsLoading(false);
            });
    }, [isLoading]);

    /**
     * Page changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (page === 0) {
            // Page 0 is not valid, must be 1.
            setPage(1);
        } else {
            setIsLoading(true);
        }
    }, [page]);

    /**
     * Save filter state.
     */
    useEffect(() => {
        lcmSetItem('own_surveys_filter', formState);
    }, [formState])

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginate = (state: { page: number }) => {
        setPage(state.page);
    };

    /**
     * Handle view item.
     *
     * @param data Item data to view.
     */
    const handleViewItem = (data: any) => {
        let destination = getDestinationQueryParameter({value: data.point, prefix:"/point/"});
        router.push(`/${data.type}/${data.id}${destination}`);
    };

    /**
     * Handle edit item.
     *
     * @param data Item data to edit.
     */
    const handleEditItem = (data: any) => {
        let destination = getDestinationQueryParameter({value: data.point, prefix:"/point/"});
        router.push(`/${data.type}/${data.id}/edit${destination}`);
    };

    /**
     * Handle component state.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFormState(event.value);
                setPage(0);
                break;
            case t("Reset"):
                // todo Implement form reset compatibility.
                setFormState({"field_type": []});
                break;
            case t("Refresh"):
                // Do nothing
                break;
        }
        // Force update.
    }

    const cardButtons = [
        {
            id: "btnView",
            label: t("View"),
            className: "btn-tertiary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                return smHasPermission("read inquiry record");
            },
            icon: <FontAwesomeIcon icon={faEye} />,
            onClick: handleViewItem,
        },
        {
            id: "btnEdit",
            label: t("Edit"),
            className: "btn-primary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                if (dmIsOfflineMode()) {
                    if ("draft" !== data.field_status) {
                        return false;
                    }
                }
                if ("calculating" === data.point_status || "calculated" === data.point_status) {
                    return false;
                }
                return smHasPermission("update inquiry record");
            },
            icon: <FontAwesomeIcon icon={faEdit} />,
            onClick: handleEditItem,
        },
    ];

    // Form buttons.
    let filterButtons: ReactElement[] = [
        (<input type="submit" value={t("Filter")} key="button-filter" className={"btn btn-primary"} disabled={isLoading}/>),
    ]

    if (!smIsValidSession()) {
        return <MainLayout />;
    }

    return (
        <MainLayout>
            <main className={"dashboard"}>
                <div className="root">
                    <Container maxWidth="md">
                        <React.StrictMode>
                            <div className="survey-management highlighted">
                                <h4>{t("Survey management")}</h4>
                                <div className={"buttons-container"}>
                                    {dmIsOfflineMode() ? (<></>) : (
                                        <button
                                            type="submit"
                                            className={"btn btn-primary"}
                                            onClick={() => router.push("/point/list")}
                                        >
                                            <span>{t("SIASAR Point")}</span>
                                        </button>
                                    )}
                                    <BasicCreationButtons formRedirectURL="/dashboard" />
                                </div>
                            </div>
                        </React.StrictMode>
                        {dmIsOfflineMode() ? (<></>) : (
                            <React.StrictMode>
                                {smHasPermission('can see user stats') || smHasPermission('can see inquiries stats') ? (
                                    <div className="stats">
                                        <SystemStats/>
                                    </div>
                                ) : (<></>)}
                            </React.StrictMode>
                        )}

                        <React.StrictMode>
                            <div className="surveys">
                                <div className="filters-wrapper">
                                    <DynForm
                                        key={"filter-form"}
                                        className={"filters-wrapper-form"}
                                        structure={getOwnSurveysFilterSchema()}
                                        buttons={filterButtons}
                                        onSubmit={handleButtonSubmit}
                                        value={formState}
                                        isLoading={false}
                                        withAccordion={false}
                                    />
                                </div>
                                <Pager page={page} itemsAmount={listedItems} onChange={paginate} isLoading={isLoading} />
                                <CardContainer
                                    enableList={true}
                                    cardType={"CardInquiry"}
                                    cards={records}
                                    isLoading={isLoading}
                                    buttons={cardButtons}
                                />
                            </div>
                        </React.StrictMode>
                    </Container>
                </div>
            </main>
        </MainLayout>
    );
};

export default Dashboard;
