/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from 'react';
import {createStyles, Theme, WithStyles, withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MainLayout from '../src/components/layout/MainLayout';

/**
 * Update theme.
 *
 * @param {Theme} theme
 * @returns {Theme}
 *   Updated theme.
 */
const styles = (theme: Theme) => createStyles({
    root: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    }
})

interface IProps { }
interface IState { }

/**
 * Base HTML document.
 */
class base extends React.Component<IProps & WithStyles<typeof styles>, IState> {

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props: IProps & WithStyles<typeof styles>) {
        super(props);
    }

    /**
     * todo Remove empty method.
     */
    componentDidMount() {
        // this.dbUtils.getUsers(this.db);
    }

    /**
     * Render HTML content.
     *
     * @returns {any}
     *   Structure.
     */
    render() {
        const { classes } = this.props;

        return (
            <MainLayout>
                <main>
                    <div className={classes.root}>
                        <Container maxWidth="md">
                            <Grid container spacing={4}   alignItems="center" >

                            </Grid>
                        </Container>
                    </div>
                </main>
            </MainLayout>
        )
    }
}

export default withStyles(styles)(base);
