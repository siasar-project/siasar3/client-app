/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {useEffect, useState} from "react";
import {Container} from "@material-ui/core";
import MainLayout from "@/components/layout/MainLayout";
import PageError from "@/components/layout/PageError";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleCheck, faRotateLeft} from "@fortawesome/free-solid-svg-icons";
import {PointStatus} from "@/objects/PointStatus";
import t from "@/utils/i18n";
import router from "next/router";
import {dmGetPoint} from "@/utils/dataManager";
import Loading from "@/components/common/Loading";
import PointStatusBreadcrumb from "@/components/Point/PointStatusBreadcrumb";
import PointInformation from "@/components/Point/PointInformation";
import PointBodyPlanning from "@/components/Point/PointBodyPlanning";
import PointBodyDigitizing from "@/components/Point/PointBodyDigitizing";
import PointBodyComplete from "@/components/Point/PointBodyComplete";
import PointBodyReviewing from "@/components/Point/PointBodyReviewing";
import ButtonsManager from "@/utils/buttonsManager";
import {faExclamationCircle} from "@fortawesome/free-solid-svg-icons/faExclamationCircle";
import {smHasPermission, smUserInRegion} from "@/utils/sessionManager";
import PointFormLogs from "@/components/DynForm/PointFormLogs";
import PointBodyViewOnly from "@/components/Point/PointBodyViewOnly";
import PointTeamList from "@/components/Point/PointTeamList";
import PointHistory from "@/components/Point/PointHistory";
import PointDefusion from "@/components/Point/PointDefusion";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {dmUpdateFormPoint} from "@/utils/dataManager/FormRecordWrite";
import {AmIInPointTeam} from "@/utils/siasar";
import {str_replace} from "locutus/php/strings";

/**
 * View form.
 *
 * @returns {any}
 *   Structure.
 */
export default function PointView() {
    // URL params.
    const {record_id} = router.query
    const urlDestination = router.query.destination;
    let recordId: string = record_id?.toString() ?? '';
    // Loading state.
    const [isLoading, setIsLoading] = useState(true);
    // Page error.
    const [pageError, setPageError] = useState({status: 0, message: ''});
    // SIASAR Point.
    const [point, setPoint] = useState({id: '', status_code: PointStatus.notdefined, allowed_actions: []});
    // Action buttons.
    let buttons: ButtonsManager = new ButtonsManager;

    /**
     * Load point data, first time.
     */
    useEffect(() => {
        dmGetPoint(recordId)
            .then((res: any) => {
                // Validate region, if the region is not in the user regions prohibit access.
                let middlePromise2 = [];
                for (let i = 0; i < res?.administrative_division.length; i++) {
                    let adId = str_replace('/api/v1/administrative_divisions/', '', res?.administrative_division[i].id);
                    middlePromise2.push(smUserInRegion(adId));
                }
                Promise.all(middlePromise2)
                    .then((results: boolean[]) => {
                        // results is an array of booleans corresponding to the resolutions of each promise.
                        // We check if any element is true with the .some() method.
                        let allow: boolean = results.some(result => result);
                        // Limit access by equip and region.
                        if (!allow || !AmIInPointTeam(res)) {
                            setPageError({status: 403, message: t('Forbidden')});
                        } else {
                            setPoint(res);
                        }
                        setIsLoading(false);
                    });
            })
            .catch((info: any) => {
                setPageError({status: info.response.status, message: info.response.statusText})
                setIsLoading(false);
            });
    }, [])

    /**
     * Redirect to point list page.
     */
    const handleReturn = () => {
        router.push(urlDestination as string || '/point/list');
    }

    /**
     * Generate page body by point status.
     *
     * @returns {ReactElement}
     */
    const bodyGenerator = () => {
        buttons.addButton({
            id: 'btn-return',
            icon: <FontAwesomeIcon icon={faRotateLeft} />,
            label: t("Return"),
            className: "btn-primary float-right",
            onClick: handleReturn,
        });

        switch (point.status_code.toLowerCase()) {
            case PointStatus.planning.toLowerCase():
                return (<PointBodyPlanning point={point} isLoading={isLoading} buttons={buttons}/>);
            case PointStatus.digitizing.toLowerCase():
                buttons.addButton({
                    id: 'btn-complete',
                    icon: <FontAwesomeIcon icon={faCircleCheck} />,
                    label: t("Complete"),
                    className: "btn-primary green",
                    /**
                     * Is this button visible?
                     *
                     * @returns {boolean}
                     */
                    show: () => {
                        if (dmIsOfflineMode()) {
                            return false;
                        }
                        for (let i = 0; i < point.allowed_actions.length; i++) {
                            if ('complete' === point.allowed_actions[i]) {
                                return smHasPermission("can do complete transition in workflow pointing");
                            }
                        }

                        return false;
                    },
                    /**
                     * On mouse click.
                     */
                    onClick: () => {
                        // Set point how complete.
                        dmUpdateFormPoint(
                            {
                                formId: "form.point",
                                id: point.id, data: {field_status: {value: "complete"}}
                            })
                            .then(() => {
                                // Reload page.
                                router.push(urlDestination as string || '/point/list');
                            })
                            .catch(() => {});
                    },
                });
                return (<PointBodyDigitizing point={point} isLoading={isLoading} buttons={buttons}/>);
            case PointStatus.complete.toLowerCase():
                return (<PointBodyComplete point={point} isLoading={isLoading} buttons={buttons}/>);
            case PointStatus.checking.toLowerCase():
                break;
            case PointStatus.reviewing.toLowerCase():
                buttons.addButton({
                    id: 'btn-reject',
                    icon: <FontAwesomeIcon icon={faExclamationCircle} />,
                    label: t("Reject"),
                    className: "btn-primary btn-red",
                    /**
                     * Is this button visible?
                     *
                     * @returns {boolean}
                     */
                    show: () => {
                        for (let i = 0; i < point.allowed_actions.length; i++) {
                            if ('reject' === point.allowed_actions[i]) {
                                return smHasPermission("can do reject transition in workflow pointing");
                            }
                        }
                        return false;
                    },
                    /**
                     * On mouse click.
                     */
                    onClick: () => {
                        // Set point how complete.
                        dmUpdateFormPoint(
                            {
                                formId: "form.point",
                                id: point.id, data: {field_status: {value: "digitizing"}}
                            })
                            .then(() => {
                                // Reload page.
                                router.push(urlDestination as string || '/point/list');
                            })
                            .catch(() => {});
                    },
                });
                buttons.addButton({
                    id: 'btn-complete',
                    icon: <FontAwesomeIcon icon={faCircleCheck} />,
                    label: t("Check"),
                    className: "btn-primary green",
                    /**
                     * Is this button visible?
                     *
                     * @returns {boolean}
                     */
                    show: () => {
                        if (dmIsOfflineMode()) {
                            return false;
                        }

                        for (let i = 0; i < point.allowed_actions.length; i++) {
                            if ('complete' === point.allowed_actions[i]) {
                                return smHasPermission("can do review transition in workflow pointing");
                            }
                        }

                        return false;
                    },
                    /**
                     * On mouse click.
                     */
                    onClick: () => {
                        // Set point how complete.
                        dmUpdateFormPoint(
                            {
                                formId: "form.point",
                                id: point.id, data: {field_status: {value: "complete"}}
                            })
                            .then(() => {
                                // Reload page.
                                router.push(urlDestination as string || '/point/list');
                            })
                            .catch(() => {});
                    },
                });
                buttons.addButton({
                    id: 'btn-calculate',
                    icon: <FontAwesomeIcon icon={faCircleCheck} />,
                    label: t("Calculate"),
                    className: "btn-primary orange",
                    /**
                     * Is this button visible?
                     *
                     * @returns {boolean}
                     */
                    show: () => {
                        for (let i = 0; i < point.allowed_actions.length; i++) {
                            if ('calculate' === point.allowed_actions[i]) {
                                return smHasPermission("can do calculate transition in workflow pointing");
                            }
                        }
                        return false;
                    },
                    /**
                     * On mouse click.
                     */
                    onClick: () => {
                        // Set point how complete.
                        dmUpdateFormPoint(
                            {
                                formId: "form.point",
                                id: point.id, data: {field_status: {value: "calculating"}}
                            })
                            .then(() => {
                                // Reload page.
                                router.push(urlDestination as string || '/point/list');
                            })
                            .catch(() => {});
                    },
                });
                return (<PointBodyReviewing point={point} isLoading={isLoading} buttons={buttons}/>);
            case PointStatus.calculating.toLowerCase():
            case PointStatus.calculated.toLowerCase():
                return (<PointBodyViewOnly point={point} isLoading={isLoading} buttons={buttons}/>);
            default:
                return (<span>{t("Point not found")}</span>);
        }
    }

    /**
     * Generate page foot by point status.
     *
     * @returns {ReactElement}
     */
    const footGenerator = () => {
        switch (point.status_code.toLowerCase()) {
            case PointStatus.planning.toLowerCase():
                break;
            case PointStatus.digitizing.toLowerCase():
                return (<PointFormLogs pointId={point.id} readonly={true} />);
            case PointStatus.complete.toLowerCase():
                break;
            case PointStatus.checking.toLowerCase():
                break;
            case PointStatus.reviewing.toLowerCase():
                return (<PointFormLogs pointId={point.id} readonly={true} />);
            case PointStatus.calculating.toLowerCase():
                break;
            case PointStatus.calculated.toLowerCase():
                return (<PointFormLogs pointId={point.id} readonly={true} />);
            default:
                return null;
        }
    }

    return (
        <MainLayout>
            <div className={'point-view'}>
                <Container>
                    {isLoading ? (
                            <Loading key={'loading_record'} />
                    ) : 
                        pageError.status > 0 ? (
                            <PageError status={pageError.status} message={pageError.message}/>
                        ) : (
                            <>
                                <h1>{t("SIASAR Point")}</h1>
                                {!dmIsOfflineMode() && PointStatus.planning.toLowerCase() !== point.status_code.toLowerCase() ? (<>
                                    <PointHistory point={point} buttons={buttons} isLoading={isLoading}/>
                                    <PointDefusion point={point} buttons={buttons} isLoading={isLoading}/>
                                </>) : (<></>)}
                                <PointStatusBreadcrumb status={point.status_code}/>
                                <PointInformation point={point}/>
                                <>{bodyGenerator()}</>
                                <PointTeamList point={point}/>
                                <>{footGenerator()}</>
                            </>
                        )
                    }
                </Container>
            </div>
        </MainLayout>
    );
}
