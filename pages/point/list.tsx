/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import {DynForm} from "@/components/DynForm/DynForm"
import React, {ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import { dmGetPoints } from "@/utils/dataManager";
import CardContainer from "@/components/Cards/CardContainer";
import Pager from "@/components/common/Pager";
import getPointFilterSchema from "../../src/Settings/dynforms/PointFilterSchema";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAdd} from "@fortawesome/free-solid-svg-icons/faAdd";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faEye} from "@fortawesome/free-solid-svg-icons/faEye";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import router from "next/router";
import Modal from "@/components/common/Modal";
import {faCalculator} from "@fortawesome/free-solid-svg-icons/faCalculator";
import {smHasPermission} from "@/utils/sessionManager";
import {AmIInPointTeam, getLuxonDate, getTimezone} from "@/utils/siasar";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {dmAddPoint, dmCloneFormPoint, dmRemoveFormPoint} from "@/utils/dataManager/FormRecordWrite";

/**
 * Surveys list.
 *
 * @returns {any}
 *   Structure.
 */
export default function PointList() {
    // List state.
    const [pointsListed, setPointsListed] = useState([]);
    // Pager state.
    const [page, setPage] = useState(1);
    // Filter state.
    const [formState, setFormState] = useState(lcmGetItem(
        'points_filter',
        {
                "field_status": [],
                "field_region": {"value": ""},
                "field_changed": {"value": ""},
                "field_order_changed": {"value": ""},
            }
        ));
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    // Unplan modal state
    const [isUnplanning, setIsUnplanning] = useState(false)
    const [pointToRemove, setPointToRemove] = useState<any>({})
    // Planning again modal state
    const [isPlaningAgain, setIsPlaningAgain] = useState(false)
    const [pointToPlaningAgain, setPointToPlaningAgain] = useState<any>({})

    const [listedItems, setListedItems] = useState(0);

    let buttons: ReactElement[] = [
        (<input type="submit" key="button-filter" className={"btn btn-primary"} value={t("Filter")} disabled={isLoading}/>),
        // (<input type="reset" key="button-reset" value={t("Reset")} disabled={isLoading}/>)
    ];

    /**
     * Page load Points listed.
     */
    useEffect(() => {
        if (!isLoading) {
            return;
        }
        let statusList:string[] = [];
        for (let i = 0; i < formState.field_status.length; i++) {
            statusList.push(formState.field_status[i]["value"]);
        }
        let changed = getLuxonDate(formState.field_changed.value, getTimezone());
        dmGetPoints(
            page,
            statusList.join(','),
            formState.field_region.value,
            changed?(changed?.year + '-' + changed?.month + '-' + changed.day):"",
            {
                changed: formState.field_order_changed.value
            }
        )
            .then(value => {
                setPointsListed(value);
                setListedItems(value.length);
            })
            .catch(() => {
                setIsLoading(false);
            });
    }, [isLoading])

    /**
     * Points loaded.
     */
    useEffect(() => {
        setIsLoading(false);
    }, [pointsListed])

    /**
     * Page changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (page === 0) {
            // Page 0 is not valid, must be 1.
            setPage(1);
        } else {
            setIsLoading(true);
        }
    }, [page])

    /**
     * Save filter state.
     */
    useEffect(() => {
        lcmSetItem('points_filter', formState);
    }, [formState])

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginate = (state: { page: number }) => {
        setPage(state.page);
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    const handleButtonSubmit = (event: any) => {
        if (event.button === t("Filter")) {
            setFormState(event.value);
        } else if (event.button === t("Reset")) {
            // todo Implement form reset compatibility.
            setFormState({"field_status": [], "field_region": {"value": ""}, "field_changed": {"value": ""}, "field_order_changed": {"value": ""}});
        }
        // Force update.
        setPage(0);
    }

    /**
     * handle remove siasar point
     * 
     * @param data {any}
     */
    const handleUnplanPoint = (data: any) => {
        dmRemoveFormPoint(data.id)
            .then(() => {
                setIsUnplanning(false)
                setPage(0)
            })
    }

    /**
     * handle planning again siasar point
     *
     * @param data {any}
     */
    const handlePlaningAgain = (data: any) => {
        dmCloneFormPoint({id: data.id, formId: 'form.point'})
            .then((newPoint: any) => {
                router.push('/point/' + newPoint.data.id);
            });
    }

    let cardButtons:CardButtonsInterface[] = [
        {
            // key: 'create',
            id: "to_plan",
            label: t("To plan"),
            className: "btn-primary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                return smHasPermission('create point record') && data.status_code == "without planning";
            },
            icon: <FontAwesomeIcon icon={faAdd} />,
            /**
             * Send user to create a new SIASAR Point.
             *
             * @param item {any}
             */
            onClick: (item:any) => {
                dmAddPoint(item.administrative_division[0].id)
                    .then((res:any) => {
                        // Go to edit the new point.
                        router.push(`/point/${res.id}`);
                    });
            },
        },
        {
            id: "unplan",
            label: t("Unplan"),
            className: "btn-primary btn-red",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                if (!AmIInPointTeam(data)) {
                    return false;
                }
                return data.status_code == "planning";
            },
            icon: <FontAwesomeIcon icon={faTrash} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item: any) => {
                setPointToRemove(item)
                setIsUnplanning(true)
            },
        },
        {
            id: "add_info",
            label: t("Add info"),
            className: "btn-primary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                if (!AmIInPointTeam(data)) {
                    return false;
                }
                return data.status_code == "planning" || data.status_code == "digitizing";
            },
            icon: <FontAwesomeIcon icon={faEdit} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {
                router.push('/point/'+item.id);
            },
        },
        {
            id: "check",
            label: t("Check"),
            className: "btn-tertiary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                if (!AmIInPointTeam(data)) {
                    return false;
                }
                return data.status_code == "reviewing";
            },
            icon: <FontAwesomeIcon icon={faCalculator} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {
                router.push('/point/'+item.id);
            },
        },
        {
            id: "calculate",
            label: t("Calculate"),
            className: "btn-tertiary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {return false},
            icon: <FontAwesomeIcon icon={faEdit} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {console.log('click ', this.id)},
        },
        {
            id: "plan_again",
            label: t("Plan again"),
            className: "btn-tertiary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                if (!smHasPermission('create point record')) {
                    return false;
                }
                return data.status_code == "calculated";
            },
            icon: <FontAwesomeIcon icon={faEdit} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {
                setPointToPlaningAgain(item)
                setIsPlaningAgain(true)
            },
        },
        {
            id: "remove",
            label: t("Remove"),
            className: "btn-tertiary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {return false},
            icon: <FontAwesomeIcon icon={faTrash} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {console.log('click ', this.id)},
        },
        {
            id: "View",
            label: t("View"),
            className: "btn-tertiary",
            /**
             * Is this button visible?
             *
             * @param data {any} Source data.
             * @param btn {CardButtonsInterface} Current button.
             *
             * @returns {boolean}
             */
            show: (data: any, btn: CardButtonsInterface) => {
                return ('calculated' === data.status_code || 'calculating' === data.status_code);
            },
            icon: <FontAwesomeIcon icon={faEye} />,
            /**
             * On mouse click.
             *
             * @param item {any}
             */
            onClick: (item:any) => {
                router.push('/point/'+item.id);
            },
        },
    ];

    return (
        <>
            <MainLayout>
                <div className={'surveys'}>
                    <Container>
                        <React.StrictMode>
                            {!dmIsOfflineMode() ? (
                                <div className="filters-wrapper">
                                    <DynForm
                                        className={"filters-wrapper-form"}
                                        structure={getPointFilterSchema()}
                                        buttons={buttons}
                                        onSubmit={handleButtonSubmit}
                                        value={formState}
                                        isLoading={false}
                                        withAccordion={false}
                                    />
                                </div>
                            ) : (<></>)}
                            <Pager page={page} itemsAmount={listedItems} onChange={paginate} isLoading={isLoading}/>
                            <CardContainer
                                enableList
                                cardType={"CardPoint"}
                                cards={pointsListed}
                                buttons={cardButtons}
                                isLoading={isLoading}
                            />
                        </React.StrictMode>
                    </Container>
                </div>
            </MainLayout>
            {/* Unplan Siasar Point modal */}
            <Modal
                title={t('Remove point')}
                onRequestClose={() => setIsUnplanning(false)}
                isOpen={isUnplanning}
            >
                {t('Surveys belonging to this Siasar Point')}
                <ul className="point-communities">
                    {
                        pointToRemove.administrative_division &&
                        pointToRemove.administrative_division
                            .filter((v: any, i: number, a: Array<any>) => a.findIndex((v2: any) => ( v2.id === v.id )) === i)
                            .map((c: any) => <li key={c.id}><strong>{t("Community")}</strong>{': '+c.name}</li>)
                    }
                    {
                        pointToRemove.inquiry_relations && pointToRemove.inquiry_relations.systems &&
                        pointToRemove.inquiry_relations.systems
                            .map((c: any) => <li key={c.id}><strong>{t("Water Supply System")}</strong>{': '+c.label}</li>)
                    }
                    {
                        pointToRemove.inquiry_relations && pointToRemove.inquiry_relations.providers &&
                        pointToRemove.inquiry_relations.providers
                            .map((c: any) => <li key={c.id}><strong>{t("Water Service Provider")}</strong>{': '+c.label}</li>)
                    }
                </ul>

                {t("Are you sure?")}
                <div className="card-administrative-division-actions">
                    <button 
                        type="button" 
                        className="btn btn-primary green" 
                        onClick={() => setIsUnplanning(false)}
                    >
                        {t("Cancel")}
                    </button>
                    <button 
                        type="button" 
                        className="btn btn-primary btn-red" 
                        onClick={() => handleUnplanPoint(pointToRemove)}
                    >
                        {t("Remove")}
                    </button>
                </div>
            </Modal>
            {/* Planning again Siasar Point modal */}
            <Modal
                title={t('Plan again')}
                onRequestClose={() => setIsPlaningAgain(false)}
                isOpen={isPlaningAgain}
            >
                {t('Surveys belonging to this Siasar Point')}
                <ul className="point-communities">
                    {
                        pointToPlaningAgain.administrative_division &&
                        pointToPlaningAgain.administrative_division
                            .filter((v: any, i: number, a: Array<any>) => a.findIndex((v2: any) => ( v2.id === v.id )) === i)
                            .map((c: any) => <li key={c.id}><strong>{t("Community")}</strong>{': '+c.name}</li>)
                    }
                    {
                        pointToPlaningAgain.inquiry_relations && pointToPlaningAgain.inquiry_relations.systems &&
                        pointToPlaningAgain.inquiry_relations.systems
                            .map((c: any) => <li key={c.id}><strong>{t("Water Supply System")}</strong>{': '+c.label}</li>)
                    }
                    {
                        pointToPlaningAgain.inquiry_relations && pointToPlaningAgain.inquiry_relations.providers &&
                        pointToPlaningAgain.inquiry_relations.providers
                            .map((c: any) => <li key={c.id}><strong>{t("Water Service Provider")}</strong>{': '+c.label}</li>)
                    }
                </ul>
                <p>{t("By accepting, this item and the surveys it contains will be locked and you will not be able to modify their content. Copies of these surveys will be created in draft state ready for planning.")}</p>
                {t("Are you sure?")}
                <div className="card-administrative-division-actions">
                    <button
                        type="button"
                        className="btn btn-primary btn-red"
                        onClick={() => setIsPlaningAgain(false)}
                    >
                        {t("Cancel")}
                    </button>
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={() => handlePlaningAgain(pointToPlaningAgain)}
                    >
                        {t("Plan again")}
                    </button>
                </div>
            </Modal>
        </>
    );
}
