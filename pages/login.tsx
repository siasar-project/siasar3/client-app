/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {SyntheticEvent, useState} from "react";
import router, {useRouter} from "next/router";
import {createStyles, Theme, withStyles,} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import MainLayout from "../src/components/layout/MainLayout";
import Typography from "@material-ui/core/Typography";
import {Button} from "@material-ui/core";
import {InputField} from "@/components/form/input";
import Api from "@/utils/api";
import {
  smAddPermissionToUser,
  smGetSession,
  smGetSessionAvatar,
  smGetSessionName,
  smIsValidSession,
  smSetLoading,
  smSetSession
} from "@/utils/sessionManager";
import LinearProgressWithLabel from "@/components/common/LinearProgressWithLabel";
import {addSystemPermission, addSystemRole, getSystemPermission, SystemPermission} from "@/utils/permissionsManager";
import {db} from "@/utils/db";
import {addFormType} from "@/utils/formTypes";
import t, {i18nInvalidateCurrentLanguage} from "@/utils/i18n";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {HostAvailable} from "@/objects/HostAvailable";
import {dmGetI18nStrings} from "@/utils/dataManager/Translation";
import {dmGetAvailableHosts, dmMoveToHost} from "@/utils/dataManager/Api";
import {loadStateStep} from "@/objects/loadStateStep";
import QueueProcessor from "@/utils/QueueProcessor";
import UserTag from "@/components/common/UserTag";

/**
 * Update theme.
 *
 * @param {Theme} theme
 *   Original theme.
 * @returns {Theme}
 *   Updated theme.
 */
const styles = (theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(8, 0, 6),
    },
    field: {
      padding: theme.spacing(2, 0, 2),
    },
  });

/**
 * Data to preload after login.
 */
const loadAppDataState:Array<loadStateStep> = [
  {key: 'start', label: "Loading...",
    /**
     * The queue process starting.
     *
     * @returns {Promise}
     */
    action: () => {
      return new Promise((resolve) => {
        smSetLoading(true);
        // Send event to notify that the Login is in state START.
        eventManager.dispatch(Events.LOGIN_START);
        resolve(true);
      });
    }},
  {key: 'load_translations', label: "Loading translations...",
    /**
     * Load translations.
     * 
     * @returns {Promise}
     */
    action: () => {
      i18nInvalidateCurrentLanguage();
      return dmGetI18nStrings()
        .catch((reason) => { 
             // Nothing to do.
        });
    }
  },
  {key: 'load_permissions', label: "Loading system permissions...",
    /**
     * Load permissions.
     *
     * @returns {Promise}
     */
    action: () => {
      return Api.getSystemPermissions().then(data => {
        for (const key in data) {
          let value = data[key];
          value.id = key;
          addSystemPermission(value);
        }
      });
    },
  },
  {key: 'load_roles', label: "Loading system roles...",
    /**
     * Load roles.
     *
     * @returns {Promise}
     */
    action: () => {
      let user = smGetSession();
      return Api.getRoles().then(data => {
        //let user = smGetSession();
        for (const roleKey in data) {
          // Add the new role.
          let value = data[roleKey];
          value.id = roleKey;
          let permissionsKeys = value.permissions;
          let permissions:Array<SystemPermission> = [];
          permissionsKeys.forEach(function (value: string) {
            let permissionInstance:SystemPermission | null = getSystemPermission(value);
            if (permissionInstance) {
              permissions.push(permissionInstance);
              // If the user have this role then we need add the permission to the user too.
              for (let i = 0; i < (user?.roles?.length ?? 0); ++i) {
                if (user?.roles && user?.roles[i] === roleKey) {
                  // Update user permission list.
                  smAddPermissionToUser(permissionInstance);
                  break;
                }
              }
            }
          });
          value.permissions = permissions;
          addSystemRole(value);
        }
      });
    },
  },
  {key: 'load_country', label: "Loading user country...",
    /**
     * Load user country.
     *
     * @returns {Promise}
     */
    action: () => {
      let user = smGetSession();
      let countryCode = user?.country?.code || '';
      return Api.getCountry(countryCode).then((data: any) => {
        if (user !== null) {
          // The user country must be into the user session data.
          user.country = data;
        }
        smSetSession(user);
      });
    },
  },
  {key: 'load_form_structures', label: 'Loading form structures...',
    /**
     * Load form types.
     *
     * @returns {Promise}
     */
    action: () => {
      // Get inquiry form list.
      return Api.getStructure(undefined, { type: 'App\\\\Forms\\\\InquiryFormManager' }).then(data => {
        for (const key in data) {
          let value = data[key];
          addFormType(value);
        }
        // Get subform form list.
        return Api.getStructure(undefined, { type: 'App\\\\Forms\\\\SubformFormManager' }).then(data => {
          for (const key in data) {
            let value = data[key];
            addFormType(value);
          }
          // Get mail forms.
          return Api.getStructure(undefined, { type: 'App\\\\Forms\\\\MailFormManager' }).then(data => {
            for (const key in data) {
              let value = data[key];
              addFormType(value);
            }
            // Get community household forms.
            return Api.getStructure(undefined, { type: 'App\\\\Forms\\\\HouseholdFormManager' }).then(data => {
              for (const key in data) {
                let value = data[key];
                addFormType(value);
              }
            });
          });
        });
      });
    },
  },
  {key: 'load_concentration_units', label: "Loading concentration units...",
    /**
     * Load units.
     *
     * @returns {Promise}
     */
    action: () => {
      return Api.getUnits('system.units.concentration').then(data => {
        return db.addSystemUnits(data[0], "concentration");
      });
    }
  },
  {key: 'load_flow_units', label: "Loading flow units...",
    /**
     * Load units.
     *
     * @returns {Promise}
     */
    action: () => {
      return Api.getUnits('system.units.flow').then(data => {
        return db.addSystemUnits(data[0], "flow");
      });
    }
  },
  {key: 'load_length_units', label: "Loading length units...",
    /**
     * Load units.
     *
     * @returns {Promise}
     */
    action: () => {
      return Api.getUnits('system.units.length').then(data => {
        return db.addSystemUnits(data[0], "length");
      });
    }
  },
  {key: 'load_volume_units', label: "Loading volume units...",
    /**
     * Load units.
     *
     * @returns {Promise}
     */
    action: () => {
      return Api.getUnits('system.units.volume').then(data => {
        return db.addSystemUnits(data[0], "volume");
      });
    }
  },
  {key: 'complete', label: "Complete",
    /**
     * The queue is finishing.
     *
     * @returns {Promise}
     */
    action: () => {
      return new Promise((resolve) => {
        eventManager.dispatch(Events.LOGIN_COMPLETE);
        smSetLoading(false);
        router.push("/dashboard");
        resolve(true);
      });
    }
  },
];

interface IProps {
  classes: any;
}

interface UserLogin {
  username?: string;
  password?: string;
  host?: string;
}

/**
 * Login page.
 *
 * @param props
 * @constructor
 */
const Login = (props: IProps) => {
  // Router.
  const router = useRouter();
  // Form states.
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [helperText, setHelperText] = useState("");
  const [isLogingIn, setIsLogingIn] = useState(false);
  // Load state: Must be a key in loadAppDataState.
  const [loadingData, setLoadingData] = useState(false);
  const [queuePercent, setQueuePercent] = useState(0);
  const [queueLabel, setQueueLabel] = useState('');
  const {
    getLoaderPercent: queueGetLoaderPercent,
    getLoaderLabel: queueGetLoaderLabel,
    init: queueInit,
    add: queueAdd
  } = QueueProcessor(() => {
    setQueuePercent(queueGetLoaderPercent());
    setQueueLabel(queueGetLoaderLabel());
  });

  // Load jobs to queue.
  for (let i = 0; i < loadAppDataState.length; i++) {
    queueAdd(loadAppDataState[i]);
  }

  /**
   * Do an API login.
   *
   * If ok load context data and redirect to dashboard.
   *
   * @param event
   */
  const onSubmit = (event: any) => {
    event.preventDefault();
    let data: UserLogin = { username, password, host };

    if (data) {
      setIsLogingIn(true);
      dmMoveToHost(data.host ?? '');
      Api.login(data)
        .then((data) => {
          setLoadingData(true);
          // Load context data.
          queueInit();
          // Dispatch status event.
          eventManager.dispatch(
              Events.STATUS_ADD,
              {
                level: StatusEventLevel.INFO,
                title: t("New user session"),
                message: 'The user create a new session.',
                isPublic: false,
              }
          );
        })
        .catch((error) => {
          setError(true);
          setHelperText(t("Invalid Credentials"));
          // Dispatch status event.
          eventManager.dispatch(
              Events.STATUS_ADD,
              {
                level: StatusEventLevel.ERROR,
                title: t("Invalid Credentials"),
                message: t('Username or password are invalid.'),
                isPublic: true,
              }
          );
        })
        .finally(() => {
          setIsLogingIn(false);
        });
    }
  };

  /**
   * On control change reset help value.
   */
  const handleChange = () => {
    setError(false);
    setHelperText("");
  };

  const { classes } = props;

  // Find current country by hostname.
  let availableHosts:HostAvailable[] = dmGetAvailableHosts();
  let domainParts: string[] = window.location.host.split('.');
  let forcedHost: string = '';
  for (let i = 0; i < availableHosts.length; i++) {
    let url = new URL(availableHosts[i].url);
    let hostParts = url.host.split('.');
    if (hostParts[0] === domainParts[0]) {
      forcedHost = availableHosts[i].url;
      break;
    }
  }
  const [host, setHost] = useState(forcedHost);

  return (
    <MainLayout>
      <main className={'login'}>
        <div className={classes.root}>
          <Container maxWidth="md">
            {loadingData ? (
                <div>
                  <LinearProgressWithLabel value={queuePercent} label={queueLabel}/>
                </div>
            ) : (
                <Grid>
                  <Grid item xs={12} sm={12}>
                    <Typography
                        component="h1"
                        variant="h2"
                        color="textPrimary"
                        gutterBottom
                    >
                      {t("Login")}
                    </Typography>
                  </Grid>

                  <Grid item xs={12} sm={12}>
                    {smIsValidSession() ? (
                        <div>
                          <span>{t("Logged in how")}</span>
                          <div>
                            <UserTag picture_url={smGetSessionAvatar()} username={smGetSessionName()}/>
                          </div>
                        </div>
                    ) : (
                        <form method="POST" onSubmit={onSubmit}>
                          <Grid container>
                            <InputField
                                id={"username"}
                                name={"username"}
                                label={t("Username")}
                                error={error}
                                onChange={(e: SyntheticEvent) => {
                                  setUsername((e.target as HTMLInputElement).value);
                                  handleChange();
                                }}
                            />

                            <InputField
                                type="password"
                                id={"password"}
                                name={"password"}
                                label={t("Password")}
                                error={error}
                                helperText={helperText}
                                onChange={(e: SyntheticEvent) => {
                                  setPassword((e.target as HTMLInputElement).value);
                                  handleChange();
                                }}
                            />

                            <Grid item xs={12} sm={12}>
                              <label htmlFor={"host"}>{t('API Host')}</label>
                              <select
                                  id={'host'}
                                  defaultValue={"" !== host ? host : undefined}
                                  value={"" !== host ? host : undefined}
                                  onChange={(e: SyntheticEvent) => {
                                    setHost((e.target as HTMLInputElement).value);
                                    handleChange();
                                  }}
                              >
                                <option value={''}>{t('Default')}</option>
                                {availableHosts.map((host: HostAvailable) => {
                                  return (<option value={host.url} key={Math.random().toString(36).substr(2, 9)}>{host.label}</option>);
                                })}
                              </select>
                            </Grid>

                            <Button disabled={isLogingIn} type="submit" className={'btn-primary'}>
                              {t("Login")}
                            </Button>
                          </Grid>
                        </form>
                    )}
                  </Grid>
                </Grid>
            )}
          </Container>
        </div>
      </main>
    </MainLayout>
  );
};

export default withStyles(styles)(Login);
