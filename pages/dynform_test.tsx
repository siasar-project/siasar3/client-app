/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import {DynForm} from "@/components/DynForm/DynForm"
import {JSXElementConstructor, ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import React from "react";
import getTestFormSchema from "../src/Settings/dynforms/TestFormSchema";
import getTestFormData, {dfTestForm} from "../src/Settings/dynforms/testsForms";
import {dmGetFormStructure, dmGetFormTypes} from "@/utils/dataManager";

/**
 * DynForm manual test tool.
 *
 * While coding, we must test single and multivalued fields,
 * with and without default values.
 * Try with/without required is a nice idea too.
 *
 * @returns {any}
 *   Structure.
 */
export default function DynformTest() {
    // Form structure.
    const [formStructure, setFormStructure] = useState(getTestFormSchema());
    // Form state.
    const [formState, setFormState] = useState(getTestFormData(dfTestForm.EMPTY_DATA));
    // Form structures.
    const [formStructures, setFormStructures] = useState([]);
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    // Is it in read only mode?
    const [isReadonly, setIsReadonly] = useState(false);
    // Is SDG form.
    const [isSDG, setIsSDG] = useState(false);
    // Form level.
    const [formLevel, setFormLevel] = useState(1);
    // Form buttons.
    let buttons: ReactElement[] = [
        (<input type="submit" key="button-add" className={"btn btn-primary"} value={t("Save")} disabled={isLoading}/>),
        // (<input type="reset" key="button-reset" value={t("Reset")} disabled={isLoading}/>)
    ];

    /**
     * Load structures.
     */
    useEffect(() => {
        setIsLoading(true);
        dmGetFormTypes()
            .then((data: any) => {
                setFormStructures(data);
                setIsLoading(false);
            })
            .catch(() => setIsLoading(false));
    }, [])

    /**
     * Redraw form.
     *
     * @param event {any}
     */
    const handleChangeForm = (event: any) => {
        if (event.target.value === '') {
            setFormStructure(getTestFormSchema());
        } else {
            setIsLoading(true);
            dmGetFormStructure(event.target.value)
                .then((struct: any) => {
                    struct.formLevel = formLevel;
                    struct.formSdg = isSDG;
                    setFormStructure(struct);
                    setFormState(getTestFormData(dfTestForm.EMPTY_DATA));
                    setIsLoading(false);
                })
                .catch(() => setIsLoading(false));
            ;
        }
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    const handleButtonSubmit = (event: any) => {
        console.log('Form submitted:', formState);
    }

    /**
     * Update form values with defaults.
     */
    const updateFormState = () => {
        // Add default ULIDs values to allow tests it.
        let updatedState: any = JSON.parse(JSON.stringify(formState));
        for (const fieldName in formStructure.fields) {
            let field = formStructure.fields[fieldName];
            if (field.type === 'ulid') {
                if (field.settings.multivalued) {
                    updatedState[fieldName] = [
                        {value: "01G5RBNN2G854AV0SJMK8AQ458"},
                        {value: "01G5RBNN38VTZWJ7R6X97D93E1"},
                        {value: "01G5RBNN4K00BS71SSMV0T2MFC"},
                    ];
                } else {
                    updatedState[fieldName] = {value: "ID01G5RBNN2G854AV0SJMK8AQ458"};
                }
            }
        }
        setFormState(updatedState);
    }

    /**
     * Toggle read only mode.
     *
     * @param event
     */
    const onIsReadonlyChange = (event: any) => {
        setIsReadonly(event.target.checked);
        formStructure.readonly = event.target.checked;
        setFormStructure(formStructure);
        updateFormState();
    }

    /**
     * Toggle read only mode.
     *
     * @param event
     */
    const onIsSDGChange = (event: any) => {
        setIsSDG(event.target.checked);
        formStructure.formSdg = event.target.checked;
        setFormStructure(formStructure);
        updateFormState();
    }

    /**
     * Toggle read only mode.
     *
     * @param event
     */
    const onLevelChange = (event: any) => {
        setFormLevel(event.target.value);
        formStructure.formLevel = event.target.value;
        setFormStructure(formStructure);
        updateFormState();
    }

    let options: Array<ReactElement<any, string | JSXElementConstructor<any>>> = formStructures.map((form: any) => {
        return (<option key={"form_type_" + form.id} value={form.id}>{"[" + form.type + "] " + form.title}</option>);
    });
    return (
        <MainLayout>
            <div className={'dynform_test'}>
                <Container>
                    <React.StrictMode>
                        <div className="dynform-filters-wrapper">
                            <div className="filter-item filter-item-select">
                                <label htmlFor={"form_type_list"}>Structure </label>
                                <select key={"form_type_list"} onChange={handleChangeForm}>
                                    <option key={"form_type_1"} value={""}>Demo</option>
                                    {options}
                                </select>
                            </div>
                            <div className="filter-item filter-item-checkbox">
                                <label className={'input-type-check'}>
                                    <input
                                        type="checkbox"
                                        name={"read_only_mode"}
                                        checked={isReadonly}
                                        onChange={onIsReadonlyChange}
                                        className="form-check-input"
                                    />
                                    <span>{"Read only mode"}</span>
                                </label>
                            </div>
                            <div className="filter-item filter-item-select">
                                <label htmlFor={"form_levels_list"}> </label>
                                <select key={"form_levels_list"} onChange={onLevelChange} value={formLevel}>
                                    <option key={"form_level_1"} value={"1"}>Level 1</option>
                                    <option key={"form_level_2"} value={"2"}>Level 2</option>
                                    <option key={"form_level_3"} value={"3"}>Level 3</option>
                                </select>
                            </div>
                            <div className="filter-item filter-item-checkbox">
                                <label className={'input-type-check'}>
                                    <input
                                        type="checkbox"
                                        name={"sdg_mode"}
                                        checked={isSDG}
                                        onChange={onIsSDGChange}
                                        className="form-check-input"
                                    />
                                    <span>{"SDG level"}</span>
                                </label>
                            </div>
                        </div>
                        <DynForm
                            key={new Date().getTime()}
                            structure={formStructure}
                            buttons={buttons}
                            onSubmit={handleButtonSubmit}
                            value={formState}
                            isLoading={isLoading}
                        />
                    </React.StrictMode>
                </Container>
            </div>
        </MainLayout>
    );
}
