/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React from "react";
import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import {Accordion, AccordionItem, AccordionItemButton, AccordionItemHeading} from "react-accessible-accordion";
import t from "@/utils/i18n";
import HouseholdProcessCards from "@/components/Household/HouseholdProcessCards";
import HouseholdSurveyCards from "@/components/Household/HouseholdSurveyCards";

/**
 * Households list.
 *
 * @returns {any}
 *   Structure.
 */
export default function Households() {
    /**
     * scroll when opening an accordion
     * 
     * @param event 
     */
    const handleAccordionHeadingClick = (event: any) => {
        setTimeout(() => {
            // Here the event.target is the accordion__button, but how their parent (accordion__heading) is sticky (floating) we can't use their offsetTop.
            // Instead, we need to use the firsts accordion__button parent that have static position (group) to get the offsetTop of AccordionItem.
            window.scrollTo({
                top: event.target.parentNode.parentNode.offsetTop,
                behavior: "smooth",
            });
        }, 200);
    }

    return (
        <MainLayout>
            <div className={'households'}>
                <Container>
                    <React.StrictMode>
                        <Accordion allowZeroExpanded allowMultipleExpanded preExpanded={['household_processes', 'user_households']}>

                            <AccordionItem uuid={'household_processes'}>
                                <AccordionItemHeading onClick={handleAccordionHeadingClick}>
                                    <AccordionItemButton>
                                        {t('Household Processes')}
                                    </AccordionItemButton>
                                </AccordionItemHeading>

                                <HouseholdProcessCards />
                                
                            </AccordionItem>

                            <AccordionItem uuid={'user_households'}>
                                <AccordionItemHeading onClick={handleAccordionHeadingClick}>
                                    <AccordionItemButton>
                                        {t('User households')}
                                    </AccordionItemButton>
                                </AccordionItemHeading>

                                <HouseholdSurveyCards />
                                
                            </AccordionItem>

                        </Accordion>
                    </React.StrictMode>
                </Container>
            </div>
        </MainLayout>
    );
}

