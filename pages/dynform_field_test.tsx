/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import {DynForm} from "@/components/DynForm/DynForm"
import React, {ReactElement, useState} from "react";
import t from "@/utils/i18n";
// import getTestFormSchema from "../src/dynforms/TestFormSchema";
import getTestFormData, {dfTestForm} from "../src/Settings/dynforms/testsForms";

/**
 * DynForm field test tool.
 *
 * While coding, we must test single and multivalued fields,
 * with and without default values.
 * Try with/without required is a nice idea too.
 *
 * @returns {any}
 *   Structure.
 */
export default function DynformFieldTest() {
    const emptyForm = {
        description: "",
        endpoint: undefined,
        fields: {},
        id: "form.sample",
        requires: [],
        title: "Test form field",
        type: "",
        meta: {
            "title": "Test form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                },
            ]
        }
    };
    // Form state.
    const [formState, setFormState] = useState(getTestFormData(dfTestForm.EMPTY_DATA));
    // Form result.
    const [formResult, setFormResult] = useState("");
    // Form structure.
    const [formStruct, setFormStruct] = useState(emptyForm);
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    // Form buttons.
    let buttons: ReactElement[] = [
        (<input type="submit" key="button-add" id="test-form-submit" className={"btn btn-primary"} value={t("Save")} disabled={isLoading}/>),
        // (<input type="reset" key="button-reset" value={t("Reset")} disabled={isLoading}/>)
    ];

    /**
     * Handle structure changes.
     *
     * @param event
     */
    const handleStructureChange = (event: any) => {
        setFormStruct(JSON.parse(event.target.value));
    }

    /**
     * Handle data changes.
     *
     * @param event
     */
    const handleDataChange = (event: any) => {
        setFormState(JSON.parse(event.target.value));
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    const handleButtonSubmit = (event: any) => {
        console.log('Form submitted:', formState);
        setFormResult(JSON.stringify(formState, undefined, 4));
    }

    return (
        <MainLayout>
            <div className={'dynform_test'}>
                <Container>
                    <p>
                        <label htmlFor={"txt_structure"}>Structure</label>
                    </p>
                    <p>
                        <textarea
                            id={"txt_structure"}
                            placeholder="Enter form structure here:"
                            value={JSON.stringify(formStruct, undefined, 4)}
                            onChange={handleStructureChange.bind(this)}
                            cols={80}
                            rows={10}
                        />
                    </p>
                    <p>
                        <label htmlFor={"txt_data"}>Data</label>
                    </p>
                    <p>
                        <textarea
                            id={"txt_data"}
                            placeholder="Enter form data here:"
                            value={JSON.stringify(formState, undefined, 4)}
                            onChange={handleDataChange.bind(this)}
                            cols={80}
                            rows={10}
                        />
                    </p>
                    <p>
                        <label htmlFor={"txt_result"}>Result</label>
                    </p>
                    <p>
                        <textarea
                            key={"txt_result"}
                            id={"txt_result"}
                            readOnly={true}
                            value={formResult}
                            cols={80}
                            rows={10}
                        />
                    </p>
                    <React.StrictMode>
                        <DynForm
                            key={new Date().getTime()}
                            structure={formStruct}
                            buttons={buttons}
                            onSubmit={handleButtonSubmit}
                            value={formState}
                            isLoading={isLoading}
                        />
                    </React.StrictMode>
                </Container>
            </div>
        </MainLayout>
    );
}
