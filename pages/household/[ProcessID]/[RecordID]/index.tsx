/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import { DynForm } from '@/components/DynForm/DynForm'
import MainLayout from '@/components/layout/MainLayout'
import Loading from "@/components/common/Loading"
import PageError from "@/components/layout/PageError"
import { DynFormRecordInterface } from '@/objects/DynForm/DynFormRecordInterface'
import { DynFormStructureInterface } from '@/objects/DynForm/DynFormStructureInterface'
import t from '@/utils/i18n'
import { faRotateLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Container } from '@material-ui/core'
import { useRouter } from 'next/router'
import React, { ReactElement, useEffect, useState } from 'react'
import { dmGetFormStructure, dmGetCountry, dmGetFormRecord } from "@/utils/dataManager";
import {dmGetHouseholdProcess} from "@/utils/dataManager/Household";

type Props = {}

/**
 * 
 * @param props 
 * @returns {any}
 */
const Index = (props: Props) => {
    const router = useRouter()
    const { ProcessID, RecordID } = router.query

    let formId: string = 'form.community.household';
    let processId: string = ProcessID?.toString() ?? '';
    let recordId: string = RecordID?.toString() ?? '';

    // Empty DynFormStructureInterface.
    const emptyDynFormStructure: DynFormStructureInterface = {
        id: formId,
        fields: {},
        title: '',
        meta: {
            title: '',
            version: '',
            field_groups: []
        }
    }
    // Empty DynFormRecordInterface.
    const emptyDynFormRecord: DynFormRecordInterface = {
        formId: formId,
        id: recordId,
    }

    // Form structure.
    const [formStructure, setFormStructure] = useState(emptyDynFormStructure);
    // Form state.
    const [formRecord, setFormRecord] = useState(emptyDynFormRecord);
    // Form structures.
    const [isLoading, setIsLoading] = useState(true);
    // Page error.
    const [pageError, setPageError] = useState({status: 0, message: ''});

    // Form buttons.
    let buttons: ReactElement[] = [
        (<button type="submit" key="button-return" className={"btn btn-primary float-right"} value={t("Return")} disabled={isLoading}>
            <FontAwesomeIcon icon={faRotateLeft} /> {t("Return")}
        </button>),
    ];

    /**
     * Load structures.
     */
    useEffect(() => {
        dmGetFormStructure(formId)
            .then((structure: any) => {
                return dmGetFormRecord(formRecord)
                    .then((data: DynFormRecordInterface) => {
                        return dmGetHouseholdProcess(processId)
                            .then((process: any) => {
                                //if (process.open) {
                                    return dmGetCountry().then((country: any) => {
                                        structure.readonly = true;
                                        structure.formLevel = country.formLevel['form.community'].level;
                                        structure.formSdg = country.formLevel['form.community'].sdg;

                                        setFormStructure(structure);
                                        setFormRecord({ formId: formId, id: recordId, data: data });
                                        setIsLoading(false);
                                    });
                                //}
                                // else {
                                //     setPageError({status: 403, message: t('The process is closed.')})
                                //     setIsLoading(false);
                                // }
                            })
                            .catch((info: any) => {
                                setPageError({status: info.response.status, message: info.response.statusText})
                                setIsLoading(false);
                            });
                    })
                    .catch((info: any) => {
                        setPageError({status: info.response.status, message: info.response.statusText})
                        setIsLoading(false);
                    });
            });
    }, [])

    /**
     * Handle form submit 
     * 
     * @param event 
     */
    const handleButtonSubmit = (event: any) => {
        // Redirect to survey page.
        router.push('/households');
    }

    return (
        <MainLayout>
            <div className={'dynform-view'}>
                <Container>
                    {isLoading ? (
                            <Loading key={'loading_record'} />
                    ) : 
                        pageError.status > 0 ? (
                            <PageError status={pageError.status} message={pageError.message}/>
                        ) : (
                            <React.StrictMode>
                                <DynForm
                                    key={new Date().getTime()}
                                    structure={formStructure}
                                    buttons={buttons}
                                    onSubmit={handleButtonSubmit}
                                    value={formRecord.data}
                                    isLoading={isLoading}
                                />
                            </React.StrictMode>
                        )
                    }
                </Container>
            </div>
        </MainLayout>
    )
}

export default Index
