/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
import { DynForm } from '@/components/DynForm/DynForm'
import MainLayout from '@/components/layout/MainLayout'
import Loading from "@/components/common/Loading"
import PageError from "@/components/layout/PageError";
import { DynFormRecordInterface } from '@/objects/DynForm/DynFormRecordInterface'
import { DynFormStructureInterface } from '@/objects/DynForm/DynFormStructureInterface'
import { dmGetCountry, dmGetFormRecord, dmGetFormStructure } from '@/utils/dataManager'
import t from '@/utils/i18n'
import { smHasPermission } from '@/utils/sessionManager'
import { faFloppyDisk, faRotateLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Container } from '@material-ui/core'
import { useRouter } from 'next/router'
import React, { ReactElement, useEffect, useState } from 'react'
import {dmGetHouseholdProcess} from "@/utils/dataManager/Household";
import {dmUpdateFormRecord} from "@/utils/dataManager/FormRecordWrite";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";

type Props = {}

/**
 * 
 * @param props 
 * @returns {any}
 */
const Edit = (props: Props) => {

    const router = useRouter()
    const { ProcessID, RecordID } = router.query

    let formId: string = 'form.community.household';
    let processId: string = ProcessID?.toString() ?? '';
    let recordId: string = RecordID?.toString() ?? '';

    // Empty DynFormStructureInterface.
    const emptyDynFormStructure: DynFormStructureInterface = {
        id: formId,
        fields: {},
        title: '',
        meta: {
            title: '',
            version: '',
            field_groups: []
        }
    }
    // Empty DynFormRecordInterface.
    const emptyDynFormRecord: DynFormRecordInterface = {
        formId: formId,
        id: recordId,
    }

    // Form structure.
    const [formStructure, setFormStructure] = useState(emptyDynFormStructure);
    // Form state.
    const [formRecord, setFormRecord] = useState(emptyDynFormRecord);
    // Form structures.
    const [isLoading, setIsLoading] = useState(true);
    // Page error.
    const [pageError, setPageError] = useState({status: 0, message: ''});

    // Form buttons.
    let buttons: ReactElement[] = [
        (<button type="submit" key="button-update" className={"btn btn-primary green"} value={t("Save")} disabled={isLoading}><FontAwesomeIcon icon={faFloppyDisk} /> {t("Save")}</button>),
        (<button type="submit" key="button-finish" className={"btn btn-primary orange"} value={t("Finish")} disabled={isLoading}><FontAwesomeIcon icon={faFloppyDisk} /> {t("Finish")}</button>),
        (<button type="submit" key="button-return" className={"btn btn-primary float-right"} value={t("Return")} disabled={isLoading}><FontAwesomeIcon icon={faRotateLeft} /> {t("Return")}</button>),
    ];

    /**
     * Load structures.
     */
    useEffect(() => {
        dmGetFormStructure(formId)
            .then((structure: any) => {
                dmGetFormRecord(formRecord)
                    .then((data: any) => {
                        dmGetHouseholdProcess(processId)
                            .then((process: any) => {
                                if (process.open) {
                                    dmGetCountry().then((country: any) => {
                                        formRecord.data = data;
                                        structure.formLevel = country.formLevel['form.community'].level;
                                        structure.formSdg = country.formLevel['form.community'].sdg;

                                        if (formRecord.data.field_finished.value === "1") {
                                            structure.readonly = true;
                                        }

                                        setFormStructure(structure);
                                        setFormRecord(formRecord);
                                        setIsLoading(false);
                                    });
                                }
                                else {
                                    setPageError({status: 403, message: t('The process is closed.')})
                                    setIsLoading(false);
                                }
                            })
                            .catch((info: any) => {
                                setPageError({status: info.response.status, message: info.response.statusText})
                                setIsLoading(false);
                            });
                    })
                    .catch((info: any) => {
                        setPageError({status: info.response.status, message: info.response.statusText})
                        setIsLoading(false);
                    });
            });
    }, [])

    /**
     * Is visble a button?
     *
     * @param record {any}
     * @param button {ReactElement}
     * @returns {boolean}
     */
    const buttonsVisibility = (record: any, button:ReactElement) => {
        if (button.key === "button-return") {
            return true;
        }
        // Set up buttons.
        switch (record.field_finished.value) {
            case '0':
                switch (button.key) {
                    case "button-update": return smHasPermission('update household record');
                    case "button-finish": return !dmIsOfflineMode() && smHasPermission('update household record');
                }
                break;

            case '1':
                switch (button.key) {
                    case "button-update": return smHasPermission('update household record');
                    case "button-finish": return false;
                }
                break;

            default:
                return false;
        }
    }
    
    /**
     * function to be executed by autosave
     * 
     * @param currentDynformState {any}
     * 
     * @returns {Promise}
     */
    const autosaveHandler = (currentDynformState: any) => {
        return dmUpdateFormRecord(currentDynformState)
    }

    /**
     * Handle form submit.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Save"):
                setIsLoading(true);
                dmUpdateFormRecord(formRecord)
                    .then(resp => {
                        setIsLoading(false);
                        // Alert user.
                        eventManager.dispatch(
                            Events.STATUS_ADD,
                            {
                                level: StatusEventLevel.SUCCESS,
                                title: t('Saved'),
                                message: t(`This questionnaire was saved.`),
                                isPublic: true
                            }
                        );
                        // Redirect to surveys page.
                        // router.push('/households');
                    })
                    .catch((info: any) => {
                        setPageError({status: info.response.status, message: info.response.statusText})
                        setIsLoading(false);
                    });
                break;

            case t("Finish"):
                dmUpdateFormRecord(formRecord).then(resp => {
                    const $preValue = formRecord.data.field_finished.value;
                    formRecord.data.field_finished.value = '1'
                    dmUpdateFormRecord(formRecord)
                        .then(resp => {
                            // Redirect to surveys page.
                            router.push('/households');
                        })
                        .finally(() => {
                            formRecord.data.field_finished.value = $preValue;
                        });
                });
                break;

            case t("Return"):
                // Redirect to survey page.
                router.push('/households');
                break;
        }
    }

    return (
        <MainLayout>
            <div className={'dynform-edit'}>
                <Container>
                    {isLoading ? (
                            <Loading key={'loading_record'} />
                    ) : 
                        pageError.status > 0 ? (
                            <PageError status={pageError.status} message={pageError.message}/>
                        ) : (
                            <React.StrictMode>
                                <DynForm
                                    key={new Date().getTime()}
                                    structure={formStructure}
                                    buttonsVisibility={buttonsVisibility}
                                    buttons={buttons}
                                    onSubmit={handleButtonSubmit}
                                    value={formRecord.data}
                                    isLoading={isLoading}
                                    autosave={formRecord.data["field_finished"].value !== 0 ? autosaveHandler : undefined}
                                />
                            </React.StrictMode>
                        )
                    }
                </Container>
            </div>
        </MainLayout>
    )
}

export default Edit
