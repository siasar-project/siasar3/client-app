/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import router from "next/router";
import {Container} from "@material-ui/core";
import MainLayout from "@/components/layout/MainLayout";
import Loading from "@/components/common/Loading";
import PageError from "@/components/layout/PageError";
import {DynForm} from "@/components/DynForm/DynForm"
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {dmGetCountry, dmGetFormRecord, dmGetFormStructure, dmGetHistoryFormRecord} from "@/utils/dataManager";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faRotateLeft, faScroll} from "@fortawesome/free-solid-svg-icons";
import InquiryFormLogs from "@/components/DynForm/InquiryFormLogs";
import {smHasPermission} from "@/utils/sessionManager";
import {getDestinationQueryParameter, routeTo} from "@/utils/siasar";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import Modal from "@/components/common/Modal";
import getInquiryHistorySchema from "../../src/Settings/dynforms/InquiryHistorySchema";

/**
 * View form.
 *
 * @returns {any}
 *   Structure.
 */
export default function SurveyView() {
    // URL params.
    const {form_id, record_id} = router.query;
    const urlDestination = router.query.destination;
    let formId: string = form_id?.toString() ?? '';
    let recordId: string = record_id?.toString() ?? '';
    // Empty DynFormStructureInterface.
    const emptyDynFormStructure: DynFormStructureInterface = {
        id: formId, 
        fields: {}, 
        title: '', 
        meta: {
            title: '', 
            version: '', 
            field_groups: []
        }
    }
    // Empty DynFormRecordInterface.
    const emptyDynFormRecord: DynFormRecordInterface = {
        formId: formId,
        id: recordId,
    }
    // Form structure.
    const [formStructure, setFormStructure] = useState(emptyDynFormStructure);
    // Form state.
    const [formRecord, setFormRecord] = useState(emptyDynFormRecord);
    // Form structures.
    const [isLoading, setIsLoading] = useState(true);
    // Page error.
    const [pageError, setPageError] = useState({status: 0, message: ''});
    // History open.
    const [historyOpen, setHistoryOpen] = useState(false);
    const [historyLoading, setHistoryLoading] = useState(false);
    const [historyState, setHistoryState] = useState({});
    // Form buttons.
    let buttons: ReactElement[] = [
        (<button
            type="submit"
            key="button-return"
            className={"btn btn-primary float-right"}
            value={t("Return")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faRotateLeft} /> {t("Return")}
        </button>),
        (<button
            type="submit"
            key="button-history"
            className={"btn btn-primary float-right"}
            value={t("History")}
            disabled={isLoading}
            hidden={dmIsOfflineMode()}>
            <FontAwesomeIcon icon={faScroll} /> {t("History")}
        </button>),
        (<button
            type="submit"
            key="button-edit"
            className={"btn btn-primary float-right green"}
            value={t("Edit | Validate")}
            disabled={isLoading}
            hidden={!smHasPermission("update inquiry record")}>
            <FontAwesomeIcon icon={faEdit} /> {t("Edit | Validate")}
        </button>),
    ];

    /**
     * Load structures.
     */
    useEffect(() => {
        dmGetFormStructure(formId)
            .then((structure: any) => {
                dmGetFormRecord({formId: formId, id: recordId})
                    .then((data: DynFormRecordInterface) => {
                        dmGetCountry().then((country: any) => {
                            structure.readonly = true;
                            structure.formLevel = country.formLevel[formId].level;
                            structure.formSdg = country.formLevel[formId].sdg;

                            setFormStructure(structure);
                            setFormRecord({formId: formId, id: recordId, data: data});
                            setIsLoading(false);
                        });
                    })
                    .catch((info: any) => {
                        setPageError({status: info.response.status, message: info.response.statusText})
                        setIsLoading(false);
                    });
            });
    }, [])

    /**
     * Handle form submit.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        let dest: string = '';
        if (router.query.destination) {
            dest = getDestinationQueryParameter({value: router.query.destination});
        }

        switch (event.button) {
            case t("Return"):
                routeTo(urlDestination, '/surveys', '/dashboard');
                break;

            case t("History"):
                dmGetHistoryFormRecord(formId, recordId)
                    .then((list:any) => {
                        let values = list.map((inquiry:any) => {
                            return {value: inquiry.id, meta: inquiry, destination: dest};
                        });
                        setHistoryState({field_inquiries: values});
                        setHistoryLoading(false);
                    })
                    .catch(() => {
                        setHistoryOpen(false);
                    });
                setHistoryLoading(true);
                setHistoryOpen(true);
                break;

            case t("Edit | Validate"):
                // Redirect to edit page.
                router.push("/" + form_id + "/" + record_id + "/edit" + dest);
                break;
        }
    }

    /**
     * Close modal with the edit point team.
     */
    const handleCloseModal = () => {
        setHistoryOpen(false);
    }

    let formHistoryButtons = [
        <input type="button" value={t("Close")} key="button-cancel-dialog" className={"btn btn-primary btn-red"} onClick={handleCloseModal}/>,
    ];
    
    return (
        <MainLayout>
            <div className={'dynform-view'}>
                <Container>
                    {isLoading ? (
                        <Loading key={'loading_record'} />
                    ) : 
                        pageError.status > 0 ? (
                            <PageError status={pageError.status} message={pageError.message}/>
                        ) : (
                            <>
                                <React.StrictMode>
                                    <DynForm
                                        key={new Date().getTime()}
                                        structure={formStructure}
                                        buttons={buttons}
                                        onSubmit={handleButtonSubmit}
                                        value={formRecord.data}
                                        isLoading={isLoading}
                                    />
                                </React.StrictMode>
                                {!dmIsOfflineMode() ? (<>
                                        <InquiryFormLogs formId={formId} recordId={recordId} readonly={formRecord.data?.field_status.value !== 'finished'}/>
                                        {/*{['draft', 'finished'].includes(formRecord.data?.field_status.value) ? (
                                            <InquiryFormLogs formId={formId} recordId={recordId} readonly={formRecord.data?.field_status.value !== 'finished'}/>
                                        ) : null }*/}
                                </>) : (<></>)}
                                <Modal
                                    isOpen={historyOpen}
                                    onRequestClose={handleCloseModal}
                                    title={t("Inquiry History")}
                                    size={"large"}
                                >
                                    <DynForm
                                        key={"edit-form"}
                                        structure={getInquiryHistorySchema()}
                                        buttons={formHistoryButtons}
                                        onSubmit={() => {}}
                                        value={historyState}
                                        isLoading={historyLoading}
                                        withAccordion={false}
                                    />
                                </Modal>
                            </>
                        )
                    }
                </Container>
            </div>
        </MainLayout>
    );
}
