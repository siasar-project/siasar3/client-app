/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import router from "next/router";
import {Container} from "@material-ui/core";
import MainLayout from "@/components/layout/MainLayout";
import Loading from "@/components/common/Loading";
import PageError from "@/components/layout/PageError";
import {DynForm} from "@/components/DynForm/DynForm"
import InquiryFormLogs from "@/components/DynForm/InquiryFormLogs"
import {DynFormStructureInterface} from "@/objects/DynForm/DynFormStructureInterface";
import {DynFormRecordInterface} from "@/objects/DynForm/DynFormRecordInterface";
import {dmGetCountry, dmGetFormRecord, dmGetFormStructure, dmGetHistoryFormRecord} from "@/utils/dataManager";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFloppyDisk, faRotateLeft, faScroll} from "@fortawesome/free-solid-svg-icons";
import {smHasPermission, smUserInRegion} from "@/utils/sessionManager";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {getDestinationQueryParameter, routeTo} from "@/utils/siasar";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {dmCloneFormRecord, dmUpdateFormRecord} from "@/utils/dataManager/FormRecordWrite";
import Modal from "@/components/common/Modal";
import getInquiryHistorySchema from "../../../src/Settings/dynforms/InquiryHistorySchema";

/**
 * Edit form.
 *
 * @returns {any}
 *   Structure.
 */
export default function SurveyEdit() {
    // URL params.
    const {form_id, record_id} = router.query;
    const urlDestination = router.query.destination;
    let formId: string = form_id?.toString() ?? '';
    let recordId: string = record_id?.toString() ?? '';
    // Empty DynFormStructureInterface.
    const emptyDynFormStructure: DynFormStructureInterface = {
        id: formId,
        fields: {},
        title: '',
        meta: {
            title: '',
            version: '',
            field_groups: []
        }
    }
    // Empty DynFormRecordInterface.
    const emptyDynFormRecord: DynFormRecordInterface = {
        formId: formId,
        id: recordId,
    }
    // Form structure.
    const [formStructure, setFormStructure] = useState(emptyDynFormStructure);
    // Form state.
    const [formRecord, setFormRecord] = useState(emptyDynFormRecord);
    // Form structures.
    const [isLoading, setIsLoading] = useState(true);
    // Page error.
    const [pageError, setPageError] = useState({status: 0, message: ''});
    // History open.
    const [historyOpen, setHistoryOpen] = useState(false);
    const [historyLoading, setHistoryLoading] = useState(false);
    const [historyState, setHistoryState] = useState({});
    // Finish and save modal.
    const [finishOpen, setFinishOpen] = useState(false);
    const [finishData, setFinishData] = useState({
        value: undefined
    });
    const [finishConfirmationMessage, setFinishConfirmationMessage] = useState("Message");
    // Update modal.
    const [updateOpen, setUpdateOpen] = useState(false);
    const [updateConfirmationMessage, setUpdateConfirmationMessage] = useState("Message");
    // Form buttons.
    let buttons: ReactElement[] = [
        (<button
            type="submit"
            key="button-update"
            className={"btn btn-primary green"}
            value={t("Save")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faFloppyDisk} /> {t("Save")}
        </button>),
        (<button
            type="submit"
            key="button-finish"
            className={"btn btn-primary orange"}
            value={t("Finish")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faFloppyDisk} /> {t("Finish")}
        </button>),
        (<button
            type="submit"
            key="button-invalidate"
            className={"btn btn-primary btn-red"}
            value={t("Back to draft")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faFloppyDisk} /> {t("Back to draft")}
        </button>),
        (<button
            type="submit"
            key="button-validate"
            className={"btn btn-primary orange"}
            value={t("Validate")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faFloppyDisk} /> {t("Validate")}
        </button>),
        (<button
            type="submit"
            key="button-clone"
            className={"btn btn-primary orange"}
            value={t("Update")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faFloppyDisk} /> {t("Update")}
        </button>),
        (<button
            type="submit"
            key="button-history"
            className={"btn btn-primary float-right"}
            value={t("History")}
            disabled={isLoading}
            hidden={dmIsOfflineMode()}>
            <FontAwesomeIcon icon={faScroll} /> {t("History")}
        </button>),
        (<button
            type="submit"
            key="button-return"
            className={"btn btn-primary float-right"}
            value={t("Return")}
            disabled={isLoading}>
            <FontAwesomeIcon icon={faRotateLeft} /> {t("Return")}
        </button>),
    ];

    /**
     * Load structures.
     */
    useEffect(() => {
        dmGetFormStructure(formStructure.id)
            .then((structure: any) => {
                dmGetFormRecord(formRecord)
                    .then((data: any) => {
                        setFormattedRecord(data, structure);
                    })
                    .catch((info: any) => {
                        setPageError({status: info.response.status, message: info.response.statusText});
                        setIsLoading(false);
                    });
            });
    }, [])

    /**
     * Format API record to be displayed in this page.
     *
     * @param data
     * @param structure
     */
    const setFormattedRecord = (data: any, structure: any) => {
        dmGetCountry().then((country: any) => {
            formRecord.data = data;
            structure.formLevel = country.formLevel[formId].level;
            structure.formSdg = country.formLevel[formId].sdg;

            if (formRecord.data.field_deleted.value === "1" || ['validated', 'locked', 'deleted'].includes(formRecord.data.field_status.value)) {
                structure.readonly = true;
                setFormStructure(structure);
                setFormRecord(formRecord);
                setIsLoading(false);
            }
            else {
                // Validate region, if the region is not in the user regions prohibit access.
                smUserInRegion(formRecord.data.field_region.value)
                    .then((allow: boolean) => {
                        if (!allow) {
                            setPageError({status: 403, message: t('Forbidden')});
                            setIsLoading(false);
                        } else {
                            switch (formStructure.id) {
                                case 'form.tap':
                                    structure.fields.field_provider_name.settings.meta.disabled = true;
                                    break;
                                case 'form.community':
                                    // If field_have_households is true, then disable it to avoid changing it again.
                                    if (formRecord.data.field_have_households.value === '1' || dmIsOfflineMode()) {
                                        structure.fields.field_have_households.settings.meta.disabled = true;
                                    }
                                    // This form never allows change field_region.
                                    structure.fields.field_region.settings.meta.disabled = true;
                                    break;
                            }
                            // if (!smHasPermission('region update inquiry record') &&
                            //     ('' !== formRecord.data.field_region.value || 'form.community' === formStructure.id)
                            // ) {
                            //     structure.fields.field_region.settings.meta.disabled = true;
                            // }

                            setFormStructure(structure);
                            setFormRecord(formRecord);
                            setIsLoading(false);
                        }
                    });
            }
        });
    }

    /**
     * Is a button visible?
     *
     * @param record {any}
     * @param button {ReactElement}
     * @returns {boolean}
     */
    const buttonsVisibility = (record: any, button:ReactElement) => {
        if (button.key === "button-return") {
            return true;
        }
        // Set up buttons.
        switch (record['field_status']['value']) {
            case 'draft':
                switch (button.key) {
                    case "button-update": return smHasPermission('update inquiry record');
                    case "button-finish": return !dmIsOfflineMode() && smHasPermission('can do finish transition in workflow inquiring');
                    case "button-invalidate": return false;
                    case "button-validate": return false;
                    case "button-clone": return false;
                    case "button-history": return true;
                }
                break;
            case 'finished':
                switch (button.key) {
                    case "button-update": return smHasPermission('update inquiry record');
                    case "button-finish": return false;
                    case "button-invalidate": return !dmIsOfflineMode() && smHasPermission('can do edit transition in workflow inquiring');
                    case "button-validate": return !dmIsOfflineMode() && smHasPermission('can do validate transition in workflow inquiring');
                    case "button-clone": return false;
                    case "button-history": return true;
                }
                break;
            case 'validated':
                switch (button.key) {
                    case "button-update": return false /*smHasPermission('can do fix transition in workflow inquiring')*/;
                    case "button-finish": return false;
                    case "button-invalidate": return !dmIsOfflineMode() && smHasPermission('can do fix transition in workflow inquiring');
                    case "button-validate": return false;
                    case "button-clone":
                        switch(record.form) {
                            case 'form.health.care':
                            case 'form.school':
                            case 'form.tap':
                            case 'form.wsprovider':
                                return !dmIsOfflineMode() && smHasPermission('can do update transition in workflow inquiring');
                            default:
                                return false;
                        }
                    case "button-history": return true;
                }
                break;
            case 'locked':
                switch (button.key) {
                    case "button-update": return false;
                    case "button-finish": return false;
                    case "button-invalidate": return false;
                    case "button-validate": return false;
                    case "button-clone":
                        switch(record.form) {
                            case 'form.health.care':
                            case 'form.school':
                            case 'form.tap':
                            case 'form.wsprovider':
                                return !dmIsOfflineMode() && smHasPermission('create inquiry record');
                            default:
                                return false;
                        }
                    case "button-history": return true;
                }
                break;

            default:
                return false;
        }
    }

    /**
     * function to be executed by autosave
     * 
     * @param currentDynformState {any}
     * 
     * @returns {Promise}
     */
    const autosaveHandler = (currentDynformState: any) => {
        let sendToApi = JSON.parse(JSON.stringify(formRecord));
        sendToApi.data = currentDynformState;
        return dmUpdateFormRecord(sendToApi);
    }

    /**
     * Handle form submit.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        let sendToApi: any;

        switch (event.button) {
            // Clone
            case t("Update"):
                setUpdateOpen(true);
                setUpdateConfirmationMessage(t(
                    'By accepting, this survey will be locked and cannot be edited again. A copy of the survey will be created in draft state ready for you to update.'
                ));
                break;
            case t("Save"):
                // event.value have the valid values to send to API.
                // formRecord.data have complete form value, but API don't accept it.
                sendToApi = JSON.parse(JSON.stringify(formRecord));
                sendToApi.data = event.value;
                setIsLoading(true);
                dmUpdateFormRecord(sendToApi)
                    .then(resp => {
                        // Alert user.
                        eventManager.dispatch(
                            Events.STATUS_ADD,
                            {
                                level: StatusEventLevel.SUCCESS,
                                title: t('Saved'),
                                message: t(`This questionnaire was saved.`),
                                isPublic: true
                            }
                        );

                        dmGetFormRecord(formRecord)
                            .then((data: any) => {
                                setFormattedRecord(data, formStructure);
                            });
                    })
                    .catch((info: any) => {
                        // setPageError({status: info.response.status, message: info.response.statusText});
                        setIsLoading(false);
                    });
                break;

            case t("Finish"):
                setFinishData(event);
                setFinishOpen(true);
                setFinishConfirmationMessage(t(
                    'The last time the survey was saved was on @date. Do you want to save it before finishing?',
                    { '@date': formRecord.data.field_changed.value}
                ));
                break;

            case t("Back to draft"):
                setIsLoading(true);
                sendToApi = JSON.parse(JSON.stringify(formRecord));
                sendToApi.data = {};
                sendToApi.data["field_status"] = {value: "draft"};
                dmUpdateFormRecord(sendToApi)
                    .then(resp => {
                        // routeTo(urlDestination, '/surveys', '/dashboard');
                        window.location.reload();
                    })
                    .catch(() => {
                        setIsLoading(false);
                    });
                break;

            case t("Validate"):
                setIsLoading(true);
                sendToApi = JSON.parse(JSON.stringify(formRecord));
                sendToApi.data = {};
                sendToApi.data["field_status"] = {value: "validated"};
                dmUpdateFormRecord(sendToApi)
                    .then(resp => {
                        routeTo(urlDestination, '/surveys', '/dashboard');
                    })
                    .catch(() => {
                        setIsLoading(false);
                    });
                break;

            case t("History"):
                let dest: string = '';
                if (router.query.destination) {
                    dest = getDestinationQueryParameter({value: router.query.destination});
                }
                dmGetHistoryFormRecord(formId, recordId)
                    .then((list:any) => {
                        let values = list.map((inquiry:any) => {
                            return {value: inquiry.id, meta: inquiry, destination: dest};
                        });
                        setHistoryState({field_inquiries: values});
                        setHistoryLoading(false);
                    })
                    .catch(() => {
                        setHistoryOpen(false);
                    });
                setHistoryLoading(true);
                setHistoryOpen(true);
                break;

            case t("Return"):
                routeTo(urlDestination, '/surveys', '/dashboard');
                break;
        }
    }

    /**
     * Close modal with the edit point team.
     */
    const handleCloseModal = () => {
        setHistoryOpen(false);
        setFinishOpen(false);
        setUpdateOpen(false);
    }

    /**
     * Save and finish.
     */
    const handleSaveFinishModal = () => {
        // Finish and save.
        let sendToApi: any;
        setIsLoading(true);
        setFinishOpen(false);

        sendToApi = JSON.parse(JSON.stringify(formRecord));
        sendToApi.data = finishData.value;
        dmUpdateFormRecord(sendToApi)
            .then(resp => {
                sendToApi.data = {};
                sendToApi.data["field_status"] = {value: "finished"};
                dmUpdateFormRecord(sendToApi)
                    .then(resp => {
                        // Redirect to surveys page.
                        // router.push(urlDestination as string || '/surveys');
                        window.location.reload();
                    })
                    .catch(() => {
                        setIsLoading(false);
                    });
            })
            .catch(() => {
                setIsLoading(false);
            });
    }

    /**
     * Finish only.
     */
    const handleFinishOnlyModal = () => {
        // Finish, only modify status.
        let sendToApi: any;
        setIsLoading(true);
        setFinishOpen(false);
        sendToApi = JSON.parse(JSON.stringify(formRecord));
        sendToApi.data = {};
        sendToApi.data["field_status"] = {value: "finished"};
        dmUpdateFormRecord(sendToApi)
            .then(resp => {
                // Redirect to surveys page.
                // router.push(urlDestination as string || '/surveys');
                window.location.reload();
            })
            .catch(() => {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.ERROR,
                        title: t("Validation at finish failed."),
                        message: t('Please, review this form logs'),
                        isPublic: true,
                    }
                );
                setIsLoading(false);
            });
    }

    /**
     * Finish only.
     */
    const handleUpdateModal = () => {
        setIsLoading(true);
        setUpdateOpen(false);

        let sendToApi: any;
        sendToApi = JSON.parse(JSON.stringify(formRecord));
        sendToApi.data = {};
        dmCloneFormRecord(sendToApi)
            .then(resp => {
                routeTo(urlDestination, '/surveys', '/dashboard');
            })
            .catch(() => {
                setIsLoading(false);
            });
    }

    let formHistoryButtons = [
        <input type="button" value={t("Close")} key="button-cancel-dialog" className={"btn btn-primary btn-red"} onClick={handleCloseModal}/>,
    ];

    return (
        <MainLayout>
            <div className={'dynform-edit'}>
                <Container>
                    <Modal
                        isOpen={finishOpen}
                        onRequestClose={handleCloseModal}
                        title={t("Finish inquiry")}
                        size={"default"}
                    >
                        <div className="surveys-modal">
                            <span>{finishConfirmationMessage}</span>
                            <div className="card-serveys-actions">
                                { "form.community" !== formRecord.data?.form ?
                                    (<button type="button" className="btn btn-primary btn-green" onClick={() => handleSaveFinishModal()} >{t("Save and finish")}</button>)
                                    : (<></>)}
                                <button type="button" className="btn btn-primary btn-orange" onClick={() => handleFinishOnlyModal()} >{t("Finish only")}</button>
                                <button type="button" className="btn btn-primary btn-red" onClick={() => handleCloseModal()}>{t("Cancel")}</button>
                            </div>
                        </div>
                    </Modal>
                    <Modal
                        isOpen={updateOpen}
                        onRequestClose={handleCloseModal}
                        title={t("Update")}
                        size={"default"}
                    >
                        <div className="surveys-modal">
                            <p>{updateConfirmationMessage}</p>
                            <p>{t("Are you sure?")}</p>
                            <div className="card-serveys-actions">
                                <button type="button" className="btn btn-primary btn-green" onClick={() => handleUpdateModal()} >{t("Update")}</button>
                                <button type="button" className="btn btn-primary btn-red" onClick={() => handleCloseModal()}>{t("Cancel")}</button>
                            </div>
                        </div>
                    </Modal>
                    {isLoading ? (
                        <Loading key={'loading_record'} />
                    ) : 
                        pageError.status > 0 ? (
                            <PageError status={pageError.status} message={pageError.message}/>
                        ) : (
                            <>
                                <React.StrictMode>
                                    <DynForm
                                        key={new Date().getTime()}
                                        structure={formStructure}
                                        buttonsVisibility={buttonsVisibility}
                                        buttons={buttons}
                                        onSubmit={handleButtonSubmit}
                                        value={formRecord.data}
                                        isLoading={isLoading}
                                        autosave={formRecord.data["field_status"].value === "draft" ? autosaveHandler : undefined}
                                    />
                                </React.StrictMode>
                                {!dmIsOfflineMode() ? (
                                    <>
                                        <InquiryFormLogs formId={formId} recordId={recordId} readonly={formRecord.data?.field_status.value !== 'finished'}/>
                                        {/*{['draft', 'finished'].includes(formRecord.data?.field_status.value) ? (
                                            <InquiryFormLogs formId={formId} recordId={recordId} readonly={formRecord.data?.field_status.value !== 'finished'}/>
                                        ) : null }*/}
                                    </>
                                ) : (<></>)}
                                <Modal
                                    isOpen={historyOpen}
                                    onRequestClose={handleCloseModal}
                                    title={t("Inquiry History")}
                                    size={"large"}
                                >
                                    <DynForm
                                        key={"edit-form"}
                                        structure={getInquiryHistorySchema()}
                                        buttons={formHistoryButtons}
                                        onSubmit={() => {}}
                                        value={historyState}
                                        isLoading={historyLoading}
                                        withAccordion={false}
                                    />
                                </Modal>
                            </>
                        )
                    }
                </Container>
            </div>
        </MainLayout>
    );
}
