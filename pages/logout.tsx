/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {useEffect} from "react";
import Container from "@material-ui/core/Container";
import MainLayout from "../src/components/layout/MainLayout";
import {smClearSession} from "@/utils/sessionManager";
import {i18nInvalidateCurrentLanguage} from "@/utils/i18n";
import Loading from "@/components/common/Loading";
import {dmIsOfflineMode} from "@/utils/dataManager/Offline";
import {useRouter} from "next/router";

/**
 * Logout page.
 * 
 * @constructor
 */
export default function Logout() {
  // Router.
  const router = useRouter();

  /**
   * Clear session Data.
   */
  useEffect(() => {
    if (!dmIsOfflineMode()) {
      smClearSession();
      i18nInvalidateCurrentLanguage();
    }

    router.push("/");
  }, []);

  return (
    <MainLayout>
      <main className={'logout'}>
        <Container maxWidth="md">
          <Loading />
        </Container>
      </main>
    </MainLayout>
  );
}
