/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import Modal from "@/components/common/Modal"
import {DynForm} from "@/components/DynForm/DynForm"
import React, {ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import {dmGetInquires} from "@/utils/dataManager";
import CardContainer from "@/components/Cards/CardContainer";
import Pager from "@/components/common/Pager";
import getSurveysFilterSchema from "../src/Settings/dynforms/SurveysFilterSchema";
import {confirmationFlow} from "@/utils/confirmationFlow";
import {useMachine} from "react-robot";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import router from "next/router";
import {faEye} from "@fortawesome/free-solid-svg-icons/faEye";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import BasicCreationButtons from "@/components/Buttons/BasicCreationButtons";
import {getLuxonDate, getTimezone} from "@/utils/siasar";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import {dmRemoveFormRecord} from "@/utils/dataManager/FormRecordWrite";

/**
 * Surveys list.
 *
 * @returns {any}
 *   Structure.
 */
export default function Surveys() {
    // List state.
    const [listItems, setListItems] = useState([]);
    // Pager state.
    const [page, setPage] = useState(1);
    // Filter state.
    const [formState, setFormState] = useState(lcmGetItem(
        'surveys_filter',
        {
                "field_status": [],
                "field_type": {"value": ""},
                "field_region": {"value": ""},
                "field_changed": {"value": ""},
                "field_order_changed": {"value": ""}
            }
    ));
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    // Confirmation flow.
    const [confirmationTitle, setConfirmationTitle] = useState("Remove");
    const [confirmationMessage, setConfirmationMessage] = useState("Message");
    const [itemToRemove, setItemToRemove] = useState();
    const [currentConfirmationState, updateConfirmationState] = useMachine(confirmationFlow);
    const [listedItems, setListedItems] = useState(0);

    /**
     * Handle view item.
     *
     * @param data Item data to view.
     */
     const handleViewItem = (data:any) => {
        router.push(`/${data.type}/${data.id}`);
    }

    /**
     * Handle edit item.
     *
     * @param data Item data to edit.
     */
    const handleEditItem = (data:any) => {
        router.push(`/${data.type}/${data.id}/edit`);
    }

    /**
     * Handle remove item.
     *
     * @param data Item data to remove.
     */
     const handleRemoveItem = (data:any) => {
         dmRemoveFormRecord({formId: data.type, id: data.id}).then((ok: any) => {          
            updateConfirmationState('confirm');
            // Force update.
            setPage(0);
            if (ok === true) {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.SUCCESS,
                        title: t('Removed'),
                        message: '',
                        isPublic: true,
                    }
                );
            }
        });
    }

    // Form buttons.
    let filterButtons: ReactElement[] = [
        (<input type="submit" value={t("Filter")} key="button-filter" className={"btn btn-primary"} disabled={isLoading}/>),
        (<BasicCreationButtons key="basic-creation-buttons" disabled={isLoading}/>),
   ]

    /**
     * Page load listItems.
     */
    useEffect(() => {
        if (isLoading) {
            let statusList:string[] = [];
            for (let i = 0; i < formState.field_status.length; i++) {
                statusList.push(formState.field_status[i]["value"]);
            }

            let changed = getLuxonDate(formState.field_changed.value, getTimezone());
            dmGetInquires(
                {
                    page: page,
                    formType: formState.field_type.value,
                    status: statusList,
                    division:formState.field_region.value,
                    changed: changed?(changed?.year + '-' + changed?.month + '-' + changed.day):""
                },
                {
                    changed: formState.field_order_changed.value
                }
            )
                .then(list => {
                    setListItems(list);
                    setListedItems(list.length);
                })
                .catch(() => { setIsLoading(false); });
        }
    }, [isLoading])

    /**
     * listItems loaded.
     */
     useEffect(() => {
        setIsLoading(false);
    }, [listItems])

    /**
     * Page changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (page === 0) {
            // Page 0 is not valid, must be 1.
            setPage(1);
        } else {
            setIsLoading(true);
        }
    }, [page])

    /**
     * Save filter state.
     */
    useEffect(() => {
        lcmSetItem('surveys_filter', formState);
    }, [formState])

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginate = (state: {page: number}) => {
        setPage(state.page);
    }

    /**
     * Handle component state.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFormState(event.value);
                setPage(0);
                break;
            case t("Reset"):
                // todo Implement form reset compatibility.
                setFormState({"field_status": [], "field_type": {"value": ""}, "field_region": {"value": ""}, "field_changed": {"value": ""}, "field_order_changed": {"value": ""}});
                break;
            case t("Refresh"):
                // Do nothing
                break;
        }
        // Force update.
    }

    return (
        <>
            <MainLayout>
                <div className={'surveys'}>
                    <Container>
                        <React.StrictMode>
                            <div className="filters-wrapper">
                                <DynForm
                                    key={"filter-form"}
                                    className={"filters-wrapper-form"}
                                    structure={getSurveysFilterSchema()}
                                    buttons={filterButtons}
                                    onSubmit={handleButtonSubmit}
                                    value={formState}
                                    isLoading={false}
                                    withAccordion={false}
                                />
                            </div>
                            <Pager page={page} itemsAmount={listedItems} onChange={paginate} isLoading={isLoading}/>
                            <CardContainer
                                enableList
                                cardType={"CardInquiry"}
                                cards={listItems}
                                isLoading={isLoading}
                                buttons={[
                                    {
                                        id: 'btnView',
                                        label: t("View"),
                                        className: "btn-tertiary",
                                        /**
                                         * Is this button visible?
                                         *
                                         * @param data {any} Source data.
                                         * @param btn {CardButtonsInterface} Current button.
                                         *
                                         * @returns {boolean}
                                         */
                                        show: (data: any, btn: CardButtonsInterface) => {return true},
                                        icon: <FontAwesomeIcon icon={faEye} />,
                                        onClick: handleViewItem
                                    },
                                    {
                                        id: 'btnEdit',
                                        label: t("Edit"),
                                        className: "btn-primary",
                                        /**
                                         * Is this button visible?
                                         *
                                         * @param data {any} Source data.
                                         * @param btn {CardButtonsInterface} Current button.
                                         *
                                         * @returns {boolean}
                                         */
                                        show: (data: any, btn: CardButtonsInterface) => {return true},
                                        icon: <FontAwesomeIcon icon={faEdit} />,
                                        onClick: handleEditItem
                                    },
                                    {
                                        id: 'btnRemove',
                                        label: t("Remove"),
                                        className: "btn-primary btn-red",
                                        /**
                                         * Is this button visible?
                                         *
                                         * @param data {any} Source data.
                                         * @param btn {CardButtonsInterface} Current button.
                                         *
                                         * @returns {boolean}
                                         */
                                        show: (data: any, btn: CardButtonsInterface) => {return true},
                                        icon: <FontAwesomeIcon icon={faTrash} />,
                                        /**
                                         * On mouse click.
                                         *
                                         * @param item {any}
                                         */
                                        onClick: (item:any) => {
                                            setConfirmationTitle(t('Remove @id "@label"', {"@id": item.id, "@label": item.type_title}));
                                            setConfirmationMessage(t('Are you sure?'));
                                            setItemToRemove(item);
                                            updateConfirmationState('begin');
                                        }
                                    }]}
                            />
                        </React.StrictMode>
                    </Container>
                </div>
            </MainLayout>
             {/* Remove survey modal */}
            <Modal
                title={confirmationTitle}
                onRequestClose={() => updateConfirmationState('cancel')}
                isOpen={currentConfirmationState.name === 'confirming'}
            >
                <div className="surveys-modal">
                    <span>{confirmationMessage}</span>
                    <div className="card-serveys-actions">
                        <button type="button" className="btn btn-primary" onClick={() => updateConfirmationState('cancel')}>{t("Cancel")}</button>
                        <button type="button" className="btn btn-primary btn-red" onClick={() => {
                            handleRemoveItem(itemToRemove);
                        }}>{t("Remove")}</button>
                    </div>
                </div>
            </Modal>
        </>
    );
}
