/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, {useEffect, useState} from 'react';
import Head from 'next/head';
import {AppProps} from 'next/app';
import {ThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import SiasarContextProvider from "../src/context/SiasarContext"
import theme from '@/styles/theme';
import '@/styles/globals.scss'
import ProgressBar from "@badrap/bar-of-progress";
import {useRouter} from "next/router";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {Modal} from "@material-ui/core";
import Loading from "@/components/common/Loading";
import RouterFirewall from "@/components/layout/RouterFirewall";
import StatusConsole from "@/components/layout/StatusConsole";
import StatusNotifications from "@/components/layout/StatusNotifications";
import t from "@/utils/i18n";
import Footer from "@/components/layout/Footer";
import {smHasPermission, smHaveSession} from "@/utils/sessionManager";
import {dmGetI18nStrings} from "@/utils/dataManager/Translation";

// Page load progress bar settings.
// @see https://akhilaariyachandra.com/blog/page-loading-progress-bar-in-nextjs
// @see https://github.com/badrap/bar-of-progress
const progress = new ProgressBar({
    size: 5,
    color: "#287fb8",
    className: "bar-of-progress",
    delay: 150,
});

interface SafeHydrateParameters {
    children: any;
}

/**
 * Allow disable SSR.
 *
 * @param props
 *
 * @constructor
 *
 * @see https://dev.to/apkoponen/how-to-disable-server-side-rendering-ssr-in-next-js-1563
 */
function SafeHydrateWithoutSSR(props:SafeHydrateParameters) {
    return (
        <div suppressHydrationWarning>
            {typeof window === 'undefined' ? null : props.children}
        </div>
    )
}

/**
 * The app.
 *
 * @param props
 * @returns {any}
 *   HTML head structure.
 */
export default function MyApp(props: AppProps) {
    const {Component, pageProps} = props;
    const router = useRouter();
    const [isMaintenanceOpen, setIsMaintenanceOpen] = useState(false);
    const [i18nLoaded, setI18nLoaded] = useState(false);

    /**
     * If the API set maintenance mode lock the user.
     */
    eventManager.on(Events.APP_MAINTENANCE, (data) => {setIsMaintenanceOpen(true);});

    if (typeof window !== 'undefined') {
        /**
         * Global error handler.
         *
         * @param message
         * @param source
         * @param lineno
         * @param colno
         * @param error
         */
        window.onerror = function(message, source, lineno, colno, error) {
            // Dispatch status event.
            eventManager.dispatch(
                Events.STATUS_ADD,
                {
                    level: StatusEventLevel.ERROR,
                    title: t('Error'),
                    message: message,
                    isPublic: false,
                }
            );
        }
    }

    /**
     * Load front translations.
     */
    useEffect(() => {
        if (i18nLoaded) {
            return;
        }
        dmGetI18nStrings()
            .then((data) => {
                setI18nLoaded(true);
            })
            .catch((reason) => {
                // Nothing to do.
            });
    }, [i18nLoaded]);

    // Remove the server-side injected CSS.
    useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement!.removeChild(jssStyles);
        }
    }, []);

    // Page loader bar.
    useEffect(() => {
        router.events.on("routeChangeStart", progress.start);
        router.events.on("routeChangeComplete", progress.finish);
        router.events.on("routeChangeError", progress.finish);

        return () => {
            router.events.off("routeChangeStart", progress.start);
            router.events.off("routeChangeComplete", progress.finish);
            router.events.off("routeChangeError", progress.finish);
        };
    }, [router]);

    return (
        <SafeHydrateWithoutSSR>
            <Head>
                <title>SIASAR</title>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width"/>
            </Head>
            <ThemeProvider theme={theme}>
                {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                <CssBaseline/>
                {!i18nLoaded ? (
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '400px',
                    }}>
                        <Loading />
                    </div>
                ) : (
                    <>
                        <StatusConsole/>
                        <StatusNotifications/>
                        <RouterFirewall>
                            <SiasarContextProvider>
                                <Component {...pageProps} />
                                <Modal open={isMaintenanceOpen && smHaveSession() && !smHasPermission('use the site in maintenance mode')} className={'maintenance-modal'}>
                                    <div className={'maintenance-modal-content'}>
                                        <div className={'maintenance-modal-text'}>{t("The remote server was entered in maintenance mode, please try to connect later.")}</div>
                                    </div>
                                </Modal>
                            </SiasarContextProvider>
                        </RouterFirewall>
                    </>
                )}
                <Footer></Footer>
            </ThemeProvider>
        </SafeHydrateWithoutSSR>
    );
}
