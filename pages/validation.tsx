/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import {DynForm} from "@/components/DynForm/DynForm"
import React, {ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import {dmGetInquires, dmGetPoints} from "@/utils/dataManager";
import CardContainer from "@/components/Cards/CardContainer";
import Pager from "@/components/common/Pager";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import router from "next/router";
import {faEye} from "@fortawesome/free-solid-svg-icons/faEye";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import {AmIInPointTeam, getDestinationQueryParameter, getLuxonDate, getTimezone} from "@/utils/siasar";
import {lcmGetItem, lcmSetItem} from "@/utils/localConfigManager";
import getValidationFilterSchema from "../src/Settings/dynforms/ValidationFilterSchema";
import {faCalculator} from "@fortawesome/free-solid-svg-icons/faCalculator";

/**
 * Surveys list.
 *
 * @returns {any}
 *   Structure.
 */
export default function Validation() {
    // List state.
    const [listItems, setListItems] = useState([]);
    const [pointsListed, setPointsListed] = useState([]);
    // Pager state.
    const [page, setPage] = useState(1);
    // Filter state.
    const [formState, setFormState] = useState(lcmGetItem(
        'validation_filter',
        {
                "field_region": {"value": ""},
                "field_validation_type": {"value": "2"},
                "field_changed": {"value": ""},
                "field_order_changed": {"value": ""}
            }
    ));
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    const [listedItems, setListedItems] = useState(0);

    /**
     * Handle view item.
     *
     * @param data Item data to view.
     */
     const handleViewItem = (data:any) => {
         let destination = getDestinationQueryParameter({value: "/validation"});
         router.push(`/${data.type}/${data.id}${destination}`);
    }

    /**
     * Handle edit item.
     *
     * @param data Item data to edit.
     */
    const handleEditItem = (data:any) => {
        let destination = getDestinationQueryParameter({value: "/validation"});
        router.push(`/${data.type}/${data.id}/edit${destination}`);
    }

    // Form buttons.
    let filterButtons: ReactElement[] = [
        (<input type="submit" value={t("Filter")} key="button-filter" className={"btn btn-primary"} disabled={isLoading}/>),
   ]

    /**
     * Page load listItems.
     */
    useEffect(() => {
        if (isLoading) {
            if (inSurveyMode()) {
                let changed = getLuxonDate(formState.field_changed.value, getTimezone());
                dmGetInquires(
                    {
                        page: page,
                        division:formState.field_region.value,
                        changed: changed?(changed?.year + '-' + changed?.month + '-' + changed.day):"",
                        status: ['finished'],
                    },
                    {
                        changed: formState.field_order_changed.value
                    }
                )
                    .then(list => {
                        setListItems(list);
                        setListedItems(list.length);
                    })
                    .catch(() => { setIsLoading(false); });
            } else {
                // SIASAR Point mode.
                if (!isLoading) {
                    return;
                }
                let changed = getLuxonDate(formState.field_changed.value, getTimezone());
                dmGetPoints(
                    page,
                    'reviewing',
                    formState.field_region.value,
                    changed?(changed?.year + '-' + changed?.month + '-' + changed.day):"",
                    {
                        changed: formState.field_order_changed.value
                    }
                )
                    .then(value => {
                        setPointsListed(value);
                    })
                    .catch(() => {
                        setIsLoading(false);
                    });
            }
        }
    }, [isLoading])

    /**
     * listItems loaded.
     */
     useEffect(() => {
        setIsLoading(false);
    }, [listItems])

    /**
     * Points loaded.
     */
    useEffect(() => {
        setIsLoading(false);
    }, [pointsListed])

    /**
     * Page changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (page === 0) {
            // Page 0 is not valid, must be 1.
            setPage(1);
        } else {
            setIsLoading(true);
        }
    }, [page])

    /**
     * Save filter state.
     */
    useEffect(() => {
        lcmSetItem('validation_filter', formState);
    }, [formState])

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginate = (state: {page: number}) => {
        setPage(state.page);
    }

    /**
     * Handle component state.
     *
     * @param event
     */
    const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFormState(event.value);
                setPage(0);
                break;
            case t("Reset"):
                // todo Implement form reset compatibility.
                setFormState({"field_region": {"value": ""}, "field_validation_type": {"value": "2"}, "field_changed": {"value": ""}, "field_order_changed": {"value": ""}});
                break;
            case t("Refresh"):
                // Do nothing
                break;
        }
        // Force update.
    }

    /**
     * Is the page in survey mode?
     *
     * @returns {boolean}
     */
    const inSurveyMode = () => {
        return "2" === formState.field_validation_type.value;
    }

    /**
     * Is the page in point mode?
     *
     * @returns {boolean}
     */
    const inPointMode = () => {
        return "1" === formState.field_validation_type.value;
    }

    return (
        <>
            <MainLayout>
                <div className={'surveys'}>
                    <Container>
                        <React.StrictMode>
                            <div className="filters-wrapper">
                                <DynForm
                                    key={"filter-form"}
                                    className={"filters-wrapper-form"}
                                    structure={getValidationFilterSchema()}
                                    buttons={filterButtons}
                                    onSubmit={handleButtonSubmit}
                                    value={formState}
                                    isLoading={false}
                                    withAccordion={false}
                                />
                            </div>
                            <Pager page={page} itemsAmount={listedItems} onChange={paginate} isLoading={isLoading}/>
                            {inSurveyMode() ? (<>
                                {/*Surveys mode.*/}
                                <h3>{t('Surveys')}</h3>
                                <CardContainer
                                    enableList
                                    cardType={"CardInquiry"}
                                    cards={listItems}
                                    isLoading={isLoading}
                                    buttons={[
                                        {
                                            id: 'btnView',
                                            label: t("View"),
                                            className: "btn-tertiary",
                                            /**
                                             * Is this button visible?
                                             *
                                             * @param data {any} Source data.
                                             * @param btn {CardButtonsInterface} Current button.
                                             *
                                             * @returns {boolean}
                                             */
                                            show: (data: any, btn: CardButtonsInterface) => {return true},
                                            icon: <FontAwesomeIcon icon={faEye} />,
                                            onClick: handleViewItem
                                        },
                                        {
                                            id: 'btnEdit',
                                            label: t("Edit"),
                                            className: "btn-primary",
                                            /**
                                             * Is this button visible?
                                             *
                                             * @param data {any} Source data.
                                             * @param btn {CardButtonsInterface} Current button.
                                             *
                                             * @returns {boolean}
                                             */
                                            show: (data: any, btn: CardButtonsInterface) => {return true},
                                            icon: <FontAwesomeIcon icon={faEdit} />,
                                            onClick: handleEditItem
                                        }
                                        ]}
                                />
                            </>) : (<>
                                {/*SIASAR Point mode.*/}
                                <h3>{t('SIASAR Point')}</h3>
                                <CardContainer
                                    enableList
                                    cardType={"CardPoint"}
                                    cards={pointsListed}
                                    buttons={[
                                        {
                                            id: "check",
                                            label: t("Check"),
                                            className: "btn-tertiary",
                                            /**
                                             * Is this button visible?
                                             *
                                             * @param data {any} Source data.
                                             * @param btn {CardButtonsInterface} Current button.
                                             *
                                             * @returns {boolean}
                                             */
                                            show: (data: any, btn: CardButtonsInterface) => {
                                                if (!AmIInPointTeam(data)) {
                                                    return false;
                                                }
                                                return data.status_code == "reviewing";
                                            },
                                            icon: <FontAwesomeIcon icon={faCalculator} />,
                                            /**
                                             * On mouse click.
                                             *
                                             * @param item {any}
                                             */
                                            onClick: (item:any) => {
                                                let destination = getDestinationQueryParameter({value: "/validation"});
                                                router.push('/point/'+item.id+destination);
                                            },
                                        }
                                    ]}
                                    isLoading={isLoading}
                                />
                            </>)}
                        </React.StrictMode>
                    </Container>
                </div>
            </MainLayout>
        </>
    );
}
