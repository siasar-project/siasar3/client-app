/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import React, { useEffect, useState } from "react";
import MainLayout from "@/components/layout/MainLayout";
import { Container } from "@material-ui/core";
import { DynForm } from "@/components/DynForm/DynForm";
import getTranslationFilterSchema from "src/Settings/dynforms/translationFielterSchema";
import t from "@/utils/i18n";
import CardContainer from "@/components/Cards/CardContainer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave } from "@fortawesome/free-solid-svg-icons";
import Pager from "@/components/common/Pager";
import { eventManager, Events, StatusEventLevel } from "@/utils/eventManager";
import {dmSaveTranslationString, dmSearchTranslationStrings} from "@/utils/dataManager/Translation";

/**
 * Tranlation tool
 * 
 * @returns {JSX.Element}
 */
const Translate = () => {
    const translationsFilterSchema = getTranslationFilterSchema();

    const [page, setPage] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [formData, setFormData] = useState<{[key: string]: any}>({});
    const [savedTranslations, setSavedTranslations] = useState<{[key: string]: any}[]>([]);
    const [listedItems, setListedItems] = useState(0);

    const buttons = [
        <input
            type="submit"
            value={t("Filter")}
            key="button-filter"
            className={"btn btn-primary"}
            disabled={isLoading}
        />
    ];

    useEffect(() => {
        if (0 === page) {
            setPage(1);
        }
        setIsLoading(true);
    }, [page])

    useEffect(() => {
        if (!isLoading) {
            return;
        }

        let iso_code = formData.field_language?.value;
        let search_in = formData.field_search_in?.value;
        let contains = formData.field_contains?.value;

        if (!iso_code || !search_in) {
            setIsLoading(false);
            return;
        }

        dmSearchTranslationStrings(iso_code, search_in, contains, page)
        .then((ts: any) => {
            let translations: {[key: string]: any}[] = [];
            Object.keys(ts.literals).forEach(lit => {
                translations.push({
                    source: lit,
                    target: ts.literals[lit],
                    translated: false
                });
            });
            setSavedTranslations(translations);
            setListedItems(translations.length);
        })
        .catch((info) => {console.warn(info)})
        .finally(() => {setIsLoading(false)});
    }, [isLoading])

    /**
     * save trnaslation
     * 
     * @param data {any}
     */
    const handleSaveTranslation = (data: any) => {
        dmSaveTranslationString(formData.field_language.value, data.source, data.target)
            .then(res => {
                setSavedTranslations(savedTranslations.map(t =>
                    (t.source === data.source) ? {...t, translated: true}: t)
                );
                
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.SUCCESS,
                        title: t('Saved translation'),
                        message: `correctly saved translation for "${data.source}"`,
                        isPublic: true,
                    }
                );
            })
            .catch((info) => {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.ERROR,
                        title: t('Error saving translation'),
                        message: info,
                        isPublic: true,
                    }
                );
            });
    }
    
    /**
     * Handle component state.
     *
     * @param event
     */
     const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFormData(event.value);
                setPage(0);
                break;
        }
    }

    /**
     * On change clear result list.
     *
     * @param event
     */
    const handleChange = (event: any) => {
        if ('field_language' === event.target.id) {
            setSavedTranslations([]);
        }
    }

    return (
        <MainLayout>
            <div className={'users'}>
                <Container>
                    <React.StrictMode>
                        <DynForm
                            key={'translations-filter-form'}
                            className={"filters-wrapper-form"}
                            structure={translationsFilterSchema}
                            onSubmit={handleButtonSubmit}
                            buttons={buttons}
                            value={formData}
                            isLoading={false}
                            onChange={handleChange}
                        />

                        <Pager 
                            page={page}
                            itemsAmount={listedItems}
                            onChange={(state: any) => setPage(state.page)} 
                            isLoading={isLoading} 
                        />
                        
                        <CardContainer
                            cards={savedTranslations}
                            cardType={'CardTranslationString'}
                            isLoading={isLoading}
                            forceList
                            buttons={[
                                {
                                    id: 'btnSave',
                                    label: t("Save"),
                                    className: "btn-primary",
                                    /**
                                     * show all buttons
                                     * 
                                     * @returns {true}
                                     */
                                    show: () => true,
                                    icon: <FontAwesomeIcon icon={faSave} />,
                                    onClick: handleSaveTranslation
                                },
                            ]}
                        />

                    </React.StrictMode>
                </Container>
            </div>
        </MainLayout>
    );
};

export default Translate;
