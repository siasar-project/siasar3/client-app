/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import t from "@/utils/i18n";
import Link from "next/link";
import * as React from "react";
import {useCallback, useEffect, useState} from "react";
import {Container} from "@material-ui/core";
import MainLayout from "@/components/layout/MainLayout";
import {dmGetCountry, dmGetFormStructure, dmGetMutatebles, dmGetUnits, dmUpdateCountry} from "@/utils/dataManager";
import {emptyMutatebleData} from "@/objects/Mutateble";
import {LevelSdgSetting} from "@/components/Settings/LevelSdgSetting";
import {MutatebleSetting} from "@/components/Settings/MutatebleSetting";
import {LanguageSetting} from "@/components/Settings/LanguageSetting";
import {UnitSetting} from "@/components/Settings/UnitSetting";
import Loading from "@/components/common/Loading";
import {
    Accordion,
    AccordionItem,
    AccordionItemButton,
    AccordionItemHeading,
    AccordionItemPanel
} from "react-accessible-accordion";
import Markdown from "@/components/common/Markdown";
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";

/**
 * Country setting form.
 *
 * @constructor
 */
const Country = () => {
    // List state.
    const [currentCountry, setCurrentCountry] = useState(null);
    // Country settings state.
    const [formLevelState, setFormLevelState] = useState<any>({})
    const [mutatebleState, setMutatebleState] = useState<any>({})
    const [languageState, setLanguageState] = useState<any>({})
    const [unitState, setUnitState] = useState<any>({})
    const [formStructures, setFormStructures] = useState<any>([])
    const [mutatebleFields, setMutatebleFields] = useState<any>([])
    const [languages, setLanguages] = useState<any>([])
    const [countryEditData, setCountryEditData] = useState<any>({})
    const [isLoading, setIsLoading] = useState(false);
    const [, updateState] = useState(0);
    const forceUpdate = useCallback(() => updateState(Math.random()), []);

    /**
     * Parametric list pages.
     */
    const parametricList = [
        { code: 5, label: t("Storage infrastructure materials"), url: "/admin/country/materials"},
        { code: 12, label: t("Distribution infrastructure materials"), url: "/admin/country/distribution-materials"},
        { code: 7, label: t("Funder interventions"), url: "/admin/country/funder-interventions"},
        { code: 16, label: t("Type interventions"), url: "/admin/country/type-interventions"},
        { code: 11, label: t("Institution interventions"), url: "/admin/country/institution-interventions"},
        { code: 22, label: t("Official languages"), url: "/admin/country/official-languages"},
        { code: 20, label: t("Ethnicities"), url: "/admin/country/ethnicities"},
        { code: 21, label: t("Currencies"), url: "/admin/country/currencies"},
        { code: 23, label: t("Program interventions"), url: "/admin/country/program-interventions"},
        { code: 2, label: t("Technical assistance providers"), url: "/admin/country/technical-assistance-providers"},
        { code: 1, label: t("Administrative divisions"), url: "/admin/country/administrative-divisions"},
        { code: 6, label: t("Pump types"), url: "/admin/country/pump-types"},
        { code: 15, label: t("Default diameters"), url: "/admin/country/default-diameters"},
        { code: 18, label: t("Special components"), url: "/admin/country/special-components"},
        { code: 4, label: t("Geographical scope"), url: "/admin/country/geographical-scope"},
        { code: 14, label: t("Functions carried out by TAP"), url: "/admin/country/functions-carried-out-tap"},
        { code: 17, label: t("Functions carried out by WSP"), url: "/admin/country/functions-carried-out-wsp"},
        { code: 24, label: t("Treatment Technology Aeration-Oxidation"), url: "/admin/country/treatment-technology-aera-oxida"},
        { code: 13, label: t("Treatment Technology Desalination"), url: "/admin/country/treatment-technology-desalination"},
        { code: 10, label: t("Treatment Technology Sedimentation"), url: "/admin/country/treatment-technology-sedimentation"},
        { code: 19, label: t("Treatment Technology Coagulation-Flocculation"), url: "/admin/country/treatment-technology-coag-floccus"},
        { code: 9, label: t("Treatment Technology Filtration"), url: "/admin/country/treatment-technology-filtration"},
        { code: 3, label: t("Disinfecting Substance"), url: "/admin/country/disinfecting-substance"},
        { code: 8, label: t("Typology Chlorination Installation"), url: "/admin/country/typology-chlorination-installations"},
        { code: 25, label: t("Type of Healthcare Facilities"), url: "/admin/country/type-healthcare-facilities"},
        { code: 29, label: t("Water Quality Entities"), url: "/admin/country/water-quality-entities"},
        { code: 30, label: t("Water Quality Intervention Types"), url: "/admin/country/water-quality-intervention-types"},
        { code: 26, label: t("Type of T.A.P."), url: "/admin/country/type-tap"},
        { code: 27, label: t("Community Services"), url: "/admin/country/community-services"},
        { code: 28, label: t("Intervention status"), url: "/admin/country/intervention-status"},
    ];

    const parametricListSortAsc = [...parametricList].sort((a, b) => a.code > b.code ? 1 : -1,);
    /**
     * Load data and strutures.
     */
    useEffect(() => {
        setIsLoading(true);

        dmGetCountry().then((country: any) => {
            setCurrentCountry(country);
            setLanguages(country!["officialLanguages"]);
            setFormLevelState(JSON.parse(JSON.stringify(country!["formLevel"])));
            setLanguageState(JSON.parse(JSON.stringify(country!["mainOfficialLanguage"])));

            dmGetMutatebles().then((mutatebles: any) => {
                // Mutatables
                let mutatebleStateTmp = JSON.parse(JSON.stringify(country!["mutatebleFields"]));

                for (let form_id in mutatebles) {
                    if (!mutatebleStateTmp[form_id]) {
                        mutatebleStateTmp[form_id] = {};
                    }

                    for (let i in mutatebles[form_id]) {
                        let field_id = mutatebles[form_id][i];

                        if (!mutatebleStateTmp[form_id][field_id]) {
                            mutatebleStateTmp[form_id][field_id] = emptyMutatebleData();
                        }

                        let options = [];
                        for (var key in mutatebleStateTmp[form_id][field_id]['options']) {
                            options.push({key: key, label: mutatebleStateTmp[form_id][field_id]['options'][key]});
                        }
                        mutatebleStateTmp[form_id][field_id]['options_key_label'] = options;
                    }
                }

                setMutatebleState(mutatebleStateTmp);
                setMutatebleFields(mutatebles);

                // Units
                let unitPromises: any[] = [];
                let unitIds = ['length', 'volume', 'flow', 'concentration'];

                unitIds.forEach(unitId => {
                    unitPromises.push(dmGetUnits(`system.units.${unitId}`));
                });

                Promise.all(unitPromises).then((units:any) => {
                    let unitStateTmp: any = {};
                    for (let i = 0; i < unitIds.length; i++) {
                        unitStateTmp[unitIds[i]] = units[i];
                    }

                    setUnitState(unitStateTmp);

                    // Form structures.
                    let formPromises: any[] = [];
                    Object.keys(country!["formLevel"]).map((form_key, key) => {
                        formPromises.push(dmGetFormStructure(form_key).catch(() => {}));
                    });

                    Promise.all(formPromises).then(structures => {
                        let formStructuresTmp: any[] = [];
                        structures.forEach((structure:any) => {
                            if (structure) {
                                formStructuresTmp[structure.id] = structure;
                            }
                        });

                        setFormStructures(formStructuresTmp);

                        setIsLoading(false);
                        forceUpdate();
                    });
                });
            });
        })
    }, []);

    /**
     * Handle level and sdg changes.
     *
     * @param event
     */
    const handleLevelChange = (event: any) => {
        // Split id by "__".
        const id_split = event.target.id.split('__');
        const form_id = id_split[1];

        // Get value.
        let value = event.target.value;
        let prop_id = 'level';

        // Initialize field edit data.
        let formLevelStateTmp: any = formLevelState;
        let countryEditDataTmp: any = countryEditData;

        if (!formLevelStateTmp[form_id]) {
            formLevelStateTmp[form_id] = {};
        }
        if (!countryEditDataTmp['formLevel']) {
            countryEditDataTmp['formLevel'] = {}
        }
        if (!countryEditDataTmp['formLevel'][form_id]) {
            countryEditDataTmp['formLevel'][form_id] = JSON.parse(JSON.stringify(formLevelStateTmp[form_id]));
        }

        if (id_split[0].includes('sdg')) {
            prop_id = 'sdg';
            value = event.target.checked;
        }
        else {
            // Setting label.
            switch (value) {
                case '1':
                    formLevelStateTmp[form_id].label = 'Level 1: Standard';
                    break;
                case '2':
                    formLevelStateTmp[form_id].label = 'Level 2: Intermediate';
                    break;
                case '3':
                    formLevelStateTmp[form_id].label = 'Level 3: Advanced';
                    break;
            }
        }

        // Setting value.
        formLevelStateTmp[form_id][prop_id] = value;

        countryEditDataTmp['formLevel'][form_id][prop_id] = value
        delete countryEditDataTmp['formLevel'][form_id].label;

        // Save states and edit data.
        setFormLevelState(formLevelStateTmp);
        setCountryEditData(countryEditDataTmp);

        forceUpdate();
    }

    /**
     * Handle mutatebles fields changes.
     *
     * @param event
     */
    const handleMutatebleChange = (event: any) => {
        // Remove error class if any when edit the value.
        event.target.parentElement.classList.remove('error-required', 'error-not-valid');

        // Split field id by "__" and get form, field and property ids.
        const field_split = event.target.id.split("__");
        const form_id = field_split[0];
        const field_id = field_split[1];
        const property_id = field_split[2];

        // Get value.
        let value = event.target.value;
        if (event.target.type === 'checkbox') {
            value = event.target.checked;
        }

        let mutatebleStateTmp: any = mutatebleState;
        let countryEditDataTmp: any = countryEditData;

        // Initialize state and edit data.
        // if (!mutatebleStateTmp[form_id]) {
        //     mutatebleStateTmp[form_id] = {};
        // }
        // if (!mutatebleStateTmp[form_id][field_id]) {
        //     mutatebleStateTmp[form_id][field_id] = emptyMutatebleData();
        //     mutatebleStateTmp[form_id][field_id]['options'] = [];
        // }
        if (!countryEditDataTmp['mutatebleFields']) {
            countryEditDataTmp['mutatebleFields'] = {};

            for (let form_id in mutatebleStateTmp) {
                countryEditDataTmp['mutatebleFields'][form_id] = JSON.parse(JSON.stringify(mutatebleStateTmp[form_id]));

                for (let field_id in countryEditDataTmp['mutatebleFields'][form_id]) {
                    delete countryEditDataTmp['mutatebleFields'][form_id][field_id]["options_key_label"];
                }
            }
        }

        if (['add', 'option_remove','option_key', 'option_value'].includes(property_id)) {
            let options_key_label = mutatebleStateTmp[form_id][field_id]['options_key_label'];
            let i = field_split[3];

            if (property_id === 'add') {
                event.preventDefault();
                options_key_label.push({key: '', label: ''});
            }
            else if (property_id === 'option_remove') {
                event.preventDefault();
                options_key_label.splice(i, 1);
            }
            else if (property_id === 'option_key') {
                options_key_label[i].key = value
            }
            else if (property_id === 'option_value') {
                options_key_label[i].label = value;
            }

            // Regenerate options data.
            let options: any = {};
            for (let j = 0; j < options_key_label.length; j++) {
                options[options_key_label[j].key] = options_key_label[j].label;
            }
            countryEditDataTmp['mutatebleFields'][form_id][field_id]['options'] = options;
        }
        else {
            mutatebleStateTmp[form_id][field_id][property_id] = value;
            countryEditDataTmp['mutatebleFields'][form_id][field_id][property_id] = value;
        }

        // Save state and edit field data.
        setMutatebleState(mutatebleStateTmp)
        setCountryEditData(countryEditDataTmp)

        forceUpdate();
    }

    /**
     * Handle language change.
     *
     * @param event
     */
    const handleLanguageChange = (event: any) => {
        let languageStateTmp: any = languageState;
        let value = event.target.value;

        languages.forEach((language:any) => {
            if (language.id === value) {
                languageStateTmp = language;
            }
        });

        // Initialize field edit data.
        let countryEditDataTmp: any = countryEditData;
        countryEditDataTmp.mainOfficialLanguage = value ? `/api/v1/official_languages/${value}` : null;

        // Save state and edit field data.
        setLanguageState(languageStateTmp);
        setCountryEditData(countryEditDataTmp);

        forceUpdate();
    }

    /**
     *  Handle main and others units changes.
     *
     * @param event
     */
    const handleUnitChange = (event: any) => {
        let prop_id = '';

        // Split id by "__".
        const id_split = event.target.id.split('__');
        const main_other = id_split[1];
        const unit_id = id_split[2];

        let value = event.target.value;

        // Initialize field edit data.
        let unitStateTmp: any = unitState;
        let countryEditDataTmp: any = countryEditData;

        if (main_other === 'main') {
            prop_id = 'mainUnit' + capitalize(unit_id);
            unitStateTmp[unit_id]['main'] = unitState[unit_id]['all'][value];
            countryEditDataTmp[`${prop_id}`] = unitStateTmp[unit_id]['main']['id'];
        }
        else {
            prop_id = 'unit' + capitalize(unit_id) + 's';
            let id = id_split[3];
            value = event.target.checked;

            if (value) {
                if (unitStateTmp[unit_id]['country'].filter((item:any) => {
                    if (!item) {
                        return false;
                    }

                    return item.id === id;
                }).length === 0) {
                    unitStateTmp[unit_id]['country'].push(unitStateTmp[unit_id]['all'][id]);
                }
            }
            else {
                unitStateTmp[unit_id]['country'] = unitStateTmp[unit_id]['country'].filter((item:any) => item.id !== id);
            }
            countryEditDataTmp[`${prop_id}`] = unitStateTmp[unit_id]['country'].filter((item:any) => {
                return !!item;
            }).map((item:any) => {
                if (!item) {
                    return null;
                }
                return item.id;
            });
        }

        // Save state and edit field data.
        setUnitState(unitStateTmp);
        setCountryEditData(countryEditDataTmp);

        forceUpdate();
    }

    /**
     * Handle AccordionItem onClick.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    const handleAccordionHeadingClick = (event: any) => {
        setTimeout(() => {
            // Here the event.target is the '.accordion__button' (or an inner node of this), but how their parent ('.accordion__heading') is sticky (floating) we can't use their offsetTop.
            // Instead we need to use the firts parent of the event.target that have static position ('.group') to get the offsetTop of AccordionItem.
            window.scrollTo({
                top: event.target.closest('.group').offsetTop,
                behavior: "smooth",
            });
        }, 400);
    }


    /**
     * Handle component invalid state.
     *
     * @param event
     */
    const handleMutatebleInvalid = (event: any) => {
        event.preventDefault();

        // Add error class to parent element (their wrapper).
        event.target.parentElement.classList.add('error-required');
        // Get error context.
        let formName = event.target.closest(".form-wrapper").children[0].textContent;
        let fieldName = event.target.closest("li").children[0].textContent;
        let propName = event.target.previousSibling.textContent;

        // Dispatch status event.
        eventManager.dispatch(
            Events.STATUS_ADD,
            {
                level: StatusEventLevel.ERROR,
                title: t("Survey '@form' - Mutateble field '@field'", {"@form": formName, "@field": fieldName}),
                message: t("'@prop' must have a value.", {"@prop": propName}),
                isPublic: true,
            }
        );
    }

    /**
     * Handle level button state.
     *
     * @param event
     *
     */
    const handleCountrySubmit = (event: any) => {
        event.preventDefault();
        setIsLoading(true);

        if (currentCountry !== null && currentCountry!["code"]) {
            // Check for key duplication in options.
            let errors = false;
            for (let form_id in mutatebleState) {
                for (let field_id in mutatebleState[form_id]) {
                    let options_key_label = mutatebleState[form_id][field_id]['options_key_label'];
                    if (!options_key_label) {
                        options_key_label = [];
                    }
                    for (let i = 0; i < (options_key_label.length - 1); i++) {
                        for (let j = i+1; j < options_key_label.length; j++) {
                            if (options_key_label[i].key === options_key_label[j].key) {
                                let formStructure:any = formStructures[form_id as keyof typeof formStructures];
                                let formName = formStructure.title;
                                let fieldName = formStructure.fields[field_id].label;
                                errors = true;

                                // Dispatch status event.
                                eventManager.dispatch(
                                    Events.STATUS_ADD,
                                    {
                                        level: StatusEventLevel.ERROR,
                                        title: t("Survey '@form' - Mutateble field '@field'", {"@form": formName, "@field": fieldName}),
                                        message: t("Can not repeat the keys in options."),
                                        isPublic: true,
                                    }
                                );
                                break;
                            }
                        }
                        if (errors) {
                            break;
                        }
                    }
                }
            }

            if (errors) {
                setIsLoading(false);
                return;
            }

            dmUpdateCountry(currentCountry!['code'], countryEditData)
                .then(res => {
                    setIsLoading(false)
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.SUCCESS,
                            title: t('Country settings saved'),
                            message: t('The data has been saved.'),
                            isPublic: true,
                        }
                    );
                    window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                    });
                })
                .catch(res => {
                    setIsLoading(false);
                })
        }
    }

    /**
     * Capitalize first char of a text.
     *
     * @param text {string} text to capitalize first char.
     *
     * @returns {string}
     */
    const capitalize = (text: string) => {
        return text.charAt(0).toUpperCase() + text.slice(1);
    }

    // We don't attach onClick to AccordionItemButton because it prevents another handler from executing on it.
    // But it doesn't prevent bubbling, so we can attach onClick to AccordionItemHeading.
    return (
        <MainLayout>
            <main className="country">
                <Container>
                    <form
                        className={"country-settings"}
                        key={"country-settings"}
                        id={"country-settings"}
                        onSubmit={handleCountrySubmit}>
                        <h1>{t("Country settings")}</h1>
                        <Accordion allowZeroExpanded preExpanded={["parametric-settings"]}>
                            <AccordionItem key={"parametric-settings"} uuid={"parametric-settings"} className={"accordion__item group group-parametric-settings"}>
                                <AccordionItemHeading onClick={handleAccordionHeadingClick}>
                                    <AccordionItemButton>
                                        {t("Parametric")}
                                    </AccordionItemButton>
                                </AccordionItemHeading>
                                <AccordionItemPanel>
                                    <div className="parametricListGrid">
                                        {parametricListSortAsc.map((value, index) => {
                                            return (
                                                <div key={"li_mnu_"+index} className="parametricListGridButton">
                                                    <Link href={value.url}>{value.code+' - '+value.label}</Link>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </AccordionItemPanel>
                            </AccordionItem>
                            <AccordionItem key={"form-settings"} uuid={"form-settings"} className={"accordion__item group group-form-settings"}>
                                <AccordionItemHeading onClick={handleAccordionHeadingClick}>
                                    <AccordionItemButton>
                                        {t("Surveys")}
                                    </AccordionItemButton>
                                </AccordionItemHeading>
                                <AccordionItemPanel>
                                    {isLoading ? (
                                        <Loading key={"form_loading"} />
                                    ) : (
                                        <>
                                            <ul className="level-sdg-list">
                                                {Object.keys(formLevelState)
                                                    .filter((value) => {
                                                        return !!formStructures[value as keyof typeof formStructures];
                                                    })
                                                    .map((form_key, key) => {
                                                    const form_structure:any = formStructures[form_key as keyof typeof formStructures];
                                                    const form_level_sdg = formLevelState[form_key as keyof typeof formLevelState];
                                                    const form_mutateble_fields = mutatebleFields![form_key as keyof typeof mutatebleFields];
                                                    const form_mutateble_data = mutatebleState[form_key as keyof typeof mutatebleState];

                                                    return (
                                                        <li key={key}>
                                                            <div className="form-wrapper">
                                                                <h2 className="form">{form_structure.title}</h2>
                                                                {form_structure.description !== '' ?
                                                                    (<Markdown text={t("Collect @form_title information in level:", {"@form_title": form_structure.title})}/>)
                                                                    : ''}
                                                                <LevelSdgSetting
                                                                    key={"level-sdg-"+form_key}
                                                                    form_id = {form_key}
                                                                    formStructure={form_structure}
                                                                    data={form_level_sdg}
                                                                    onChange={handleLevelChange}
                                                                />
                                                                {form_mutateble_fields ? (
                                                                    <MutatebleSetting
                                                                        key={"mutateble-"+form_key}
                                                                        form_id={form_key}
                                                                        fieldsMutateble={form_mutateble_fields}
                                                                        formStructure={form_structure}
                                                                        data={form_mutateble_data}
                                                                        onChange={handleMutatebleChange}
                                                                        onInvalid={handleMutatebleInvalid}
                                                                    />
                                                                ) : (<></>)}
                                                            </div>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </>
                                    )}
                                </AccordionItemPanel>
                            </AccordionItem>
                            <AccordionItem key={"lang-unit-settings"} uuid={"lang-unit-settings"} className={"accordion__item group group-lang-unit-settings"}>
                                <AccordionItemHeading onClick={handleAccordionHeadingClick}>
                                    <AccordionItemButton>
                                        {t("Language and Units")}
                                    </AccordionItemButton>
                                </AccordionItemHeading>
                                <AccordionItemPanel>
                                    {isLoading ? (
                                        <Loading key={"lang_unit_loading"} />
                                    ) : (
                                        <>
                                            <ul className="lang-unit-list">
                                                <li>
                                                    <div className="language-wrapper">
                                                        <h2>{t("Official Language")}</h2>
                                                        <LanguageSetting
                                                            languages = {languages}
                                                            data={languageState}
                                                            onChange={handleLanguageChange}
                                                        />
                                                    </div>
                                                </li>
                                                {Object.keys(unitState).map((unit_key, key) => {
                                                    const unit = unitState[unit_key as keyof typeof unitState];
                                                    return (
                                                        <li key={key}>
                                                            <div className="unit-wrapper">
                                                                <h2>{t(capitalize(unit_key))}</h2>
                                                                <UnitSetting
                                                                    key={"unit-"+unit_key}
                                                                    unit_id = {unit_key}
                                                                    data={unit}
                                                                    onChange={handleUnitChange}
                                                                />
                                                            </div>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </>
                                    )}
                                </AccordionItemPanel>
                            </AccordionItem>
                        </Accordion>
                        <div className="form-actions">
                            <button type="submit" className={"btn btn-primary btn-save"}>{t("Save")}</button>
                        </div>
                    </form>
                </Container>
            </main>
        </MainLayout>
    )
}

export default Country
