/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout";
import {Container} from "@material-ui/core";
import Modal from "@/components/common/Modal"
import {DynForm} from "@/components/DynForm/DynForm"
import {ReactElement, useEffect, useState} from "react";
import t from "@/utils/i18n";
import React from "react";
import {dmGetCountry} from "@/utils/dataManager";
import CardContainer from "@/components/Cards/CardContainer";
import Pager from "@/components/common/Pager";
import getPumpTypesFilterSchema from "../../../src/Settings/dynforms/PumpTypesFilterSchema";
import getPumpTypesEditSchema from "../../../src/Settings/dynforms/PumpTypesEditSchema";
import { confirmationFlow } from "@/utils/confirmationFlow";
import { useMachine } from "react-robot";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons/faPlusCircle";
import {faSync} from "@fortawesome/free-solid-svg-icons/faSync";
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import { eventManager, Events, StatusEventLevel } from "@/utils/eventManager";
import ImportParametric from "@/components/common/ImportParametric";
import {
    dmGetPumpTypes
} from "@/utils/dataManager/ParametricRead";
import {
    dmAddPumpType,
    dmInvalidatePumpTypes,
    dmRemovePumpType,
    dmUpdatePumpType
} from "@/utils/dataManager/ParametricWrite";

/**
 * Surveys list.
 *
 * @returns {any}
 *   Structure.
 */
export default function PumpTypes() {
    // List state.
    const [currentCountry, setCurrentCountry] = useState(null);
    // List state.
    const [listItems, setListItems] = useState([]);
    // Pager state.
    const [page, setPage] = useState(1);
    // Filter state.
    const [formState, setFormState] = useState({"field_type": { "value": "" }});
    // Editing mode.
    const [isEditing, setIsEditing] = useState(false);
    // Edit state.
    const formEditDefault = {"field_id": { "value": "" }, "field_type": { "value": "" }};
    const [formEditState, setFormEditState] = useState(formEditDefault);
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    // Confirmation flow.
    const [confirmationTitle, setConfirmationTitle] = useState("Remove");
    const [itemToRemove, setItemToRemove] = useState({});
    const [currentConfirmationState, updateConfirmationState] = useMachine(confirmationFlow);
    const [listedItems, setListedItems] = useState(0);

    /**
     * Handle add new item.
     */
    const handleAddItem = () => {
        setFormEditState(formEditDefault);
        setIsEditing(true);
    }

    /**
     * Handle refresh list.
     */
     const handleRefreshItem = () => {
        dmInvalidatePumpTypes().then(() => {
            // Force update.
            setPage(0);
        });
    }

    /**
     * Handle new item created.
     *
     * @param data Event data.
     */
    const handleNewItem = (data:any) => {
        if (currentCountry !== null) {
            let cuCountry: any = currentCountry;
            let code: string = cuCountry["code"];
            dmAddPumpType(`/api/v1/countries/${code}`, formEditState.field_type.value)
                .then(() => {
                    dmInvalidatePumpTypes().then(() => {
                        setIsEditing(false);
                        // Force update.
                        setPage(0);
                        eventManager.dispatch(
                            Events.STATUS_ADD,
                            {
                                level: StatusEventLevel.SUCCESS,
                                title: t('Added'),
                                message: '',
                                isPublic: true,
                            }
                        );
                    });
                })
                .catch(() => {});
        }
    }

    /**
     * Handle new item created.
     *
     * @param data Event data.
     */
    const handleUpdateItem = (data:any) => {
        dmUpdatePumpType(data.field_id.value, data.field_type.value)
            .then(() => {
                dmInvalidatePumpTypes().then(() => {
                    setIsEditing(false);
                    // Force update.
                    setPage(0);
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.SUCCESS,
                            title: t('Updated'),
                            message: '',
                            isPublic: true,
                        }
                    );
                });
            })
            .catch(() => {});
    }

    /**
     * Handle close modal request.
     */
    const handleRequestClose = () => {
        setIsEditing(false);
    }

    /**
     * Handle edit item.
     *
     * @param data Item data to remove.
     */
    const handleEditItem = (data:any) => {
        let instance = formEditDefault;
        instance.field_type.value = data.type;
        instance.field_id.value = data.id;
        setFormEditState(instance);
        setIsEditing(true);
    }

    /**
     * Handle remove item.
     *
     * @param data Item data to remove.
     */
    const handleRemoveItem = (data:any) => {
        dmRemovePumpType(data.id)
            .then(() => {
                dmInvalidatePumpTypes().then(() => {
                    updateConfirmationState('confirm');
                    // Force update.
                    setPage(0);
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.SUCCESS,
                            title: t('Removed'),
                            message: '',
                            isPublic: true,
                        }
                    );
                });
            })
            .catch(() => {})
    }

    // Form buttons.
    let filterButtons: ReactElement[] = [
        (<input type="submit" value={t("Filter")} key="button-filter" className={"btn btn-primary"} disabled={isLoading}/>),
        (<button
            type="submit"
            value={t("Add new")}
            className={"btn btn-secondary float-right"}
            onClick={handleAddItem}
            key="button-add"
            disabled={isLoading}
        >
            <FontAwesomeIcon icon={faPlusCircle} />&nbsp;{t("Add new")}
        </button>),
        (<button
            type="submit"
            value={t("Refresh")}
            key="button-refresh"
            className={"btn btn-tertiary"}
            disabled={isLoading}
            onClick={handleRefreshItem}
        >
            <FontAwesomeIcon icon={faSync} />&nbsp;{t("Refresh")}
        </button>),
        // (<input type="reset" value={t("Reset")} key="button-reset" disabled={isLoading}/>)
    ];
    let editButtons: ReactElement[] = [
        (<input type="submit" value={t("Save")} key="button-save" className={"btn btn-primary green"}/>),
        (<input type="button" value={t("Cancel")} key="button-cancel" className={"btn btn-primary btn-red"} onClick={handleRequestClose}/>),
    ];

    /**
     * Page load listItems.
     */
    useEffect(() => {
        if (!isLoading) {
            return;
        }
        // Load inquiry list.
        dmGetPumpTypes(formState.field_type.value, page).then(list => {
            dmGetCountry().then((country:any) => {
                setCurrentCountry(country);
                setListItems(list);
                setListedItems(list.length);
            });
        });
    }, [isLoading])

    /**
     * listItems loaded.
     */
    useEffect(() => {
        setIsLoading(false);
    }, [listItems])

    /**
     * Page changed.
     */
    useEffect(() => {
        // Page 0 force reload.
        if (page === 0) {
            // Page 0 is not valid, must be 1.
            setPage(1);
        } else {
            setIsLoading(true);
        }
    }, [page])

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginate = (state: {page: number}) => {
        setPage(state.page);
    }

    /**
     * Handle component state.
     *
     * @param event
     *
     * @see https://reactjs.org/docs/forms.html
     */
    const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFormState(event.value);
                setPage(0);
                break;
            case t("Save"):
                if (event.value.field_id.value === "") {
                    handleNewItem(event.value);
                } else {
                    handleUpdateItem(event.value);
                }
                break;
            case t("Reset"):
                // todo Implement form reset compatibility.
                setFormState({"field_type": { "value": "" }});
                break;
            default:
                console.log("Unknown button: ", event.button);
                break;
        }
    }

    return (
        <>
            <MainLayout>
                <div className={'pumpTypes'}>
                    <Container>
                        <React.StrictMode>
                            <div className="filters-wrapper">
                                <DynForm
                                    key={"filter-form"}
                                    className={"filters-wrapper-form"}
                                    structure={getPumpTypesFilterSchema()}
                                    buttons={filterButtons}
                                    onSubmit={handleButtonSubmit}
                                    value={formState}
                                    isLoading={false}
                                    withAccordion={false}
                                />
                            </div>
                            <Pager page={page} itemsAmount={listedItems} onChange={paginate} isLoading={isLoading}/>
                            <CardContainer
                                cardType={"CardPumpType"}
                                cards={listItems}
                                isLoading={isLoading}
                                buttons={[
                                    {
                                        id: 'btnEdit',
                                        label: t("Edit"),
                                        className: "btn-primary",
                                        /**
                                         * Is this button visible?
                                         *
                                         * @param data {any} Source data.
                                         * @param btn {CardButtonsInterface} Current button.
                                         *
                                         * @returns {boolean}
                                         */
                                        show: (data: any, btn: CardButtonsInterface) => {return true},
                                        icon: <FontAwesomeIcon icon={faEdit} />,
                                        onClick: handleEditItem
                                    },
                                    {
                                        id: 'btnRemove',
                                        label: t("Remove"),
                                        className: "btn-primary btn-red",
                                        /**
                                         * Is this button visible?
                                         *
                                         * @param data {any} Source data.
                                         * @param btn {CardButtonsInterface} Current button.
                                         *
                                         * @returns {boolean}
                                         */
                                        show: (data: any, btn: CardButtonsInterface) => {return true},
                                        icon: <FontAwesomeIcon icon={faTrash} />,
                                        /**
                                         * On mouse click.
                                         *
                                         * @param item {any}
                                         */
                                        onClick: (item:any) => {
                                            setConfirmationTitle(t('Remove "@label"', {"@label": item.name}));
                                            setItemToRemove(item);
                                            updateConfirmationState('begin');
                                        }
                                    }]}
                            />
                            <ImportParametric title={t("Pump Types")} entityName={"PumpType"} />
                        </React.StrictMode>
                    </Container>
                </div>
            </MainLayout>
            {/* Edit pumpType modal */}
            <Modal
                isOpen={isEditing}
                onRequestClose={handleRequestClose}
                title={formEditState.field_id.value !== "" ? (
                    t("Edit pump type")
                ) : (
                    t("Add pump type")
                )}
            >
                <DynForm
                    key={"edit-form"}
                    structure={getPumpTypesEditSchema()}
                    buttons={editButtons}
                    onSubmit={handleButtonSubmit}
                    value={formEditState}
                    isLoading={isLoading}
                    withAccordion={false}
                />
            </Modal>
            {/* Remove pumpType modal */}
            <Modal
                title={confirmationTitle}
                onRequestClose={() => updateConfirmationState('cancel')}
                isOpen={currentConfirmationState.name === 'confirming'}
            >
                {t("Are you sure?")}
                <div className="card-pump-type-actions">
                    <button type="button" className="btn btn-primary green" onClick={() => updateConfirmationState('cancel')}>{t("Cancel")}</button>
                    <button type="button" className="btn btn-primary btn-red" onClick={() => {
                        handleRemoveItem(itemToRemove);
                    }}>{t("Remove")}</button>
                </div>
            </Modal>
        </>
    );
}
