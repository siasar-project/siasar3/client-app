/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import MainLayout from "@/components/layout/MainLayout"
import Container from "@material-ui/core/Container"
import {useEffect, useState} from "react"
import {User} from "@/objects/User"
import router from "next/router";
import * as React from "react";
import {smHasPermission} from "@/utils/sessionManager";
import {dmGetRoles, dmGetUsers, dmUpdateUser, dmInvalidateUsers} from "@/utils/dataManager";
import {generatePassword, SystemRole, userHasPermission} from "@/utils/permissionsManager";
import Pager from "@/components/common/Pager"
import CardContainer from "@/components/Cards/CardContainer"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit, faPlusCircle, faSync, faKey } from "@fortawesome/free-solid-svg-icons"
import t from "@/utils/i18n"
import { DynForm } from "@/components/DynForm/DynForm";
import getUsersFilterSchema from "src/Settings/dynforms/UsersFilterSchema";
import Modal from "@/components/common/Modal"
import {eventManager, Events, StatusEventLevel} from "@/utils/eventManager";
import {smGetSession} from "@/utils/sessionManager"
import {CardButtonsInterface} from "@/objects/Cards/CardButtonsInterface";

/**
 * User list page.
 *
 * @returns {any}
 *   Structure.
 */
export default function Users() {

    // List state
    const [users, setUsers] = useState<User[] | []>([])
    const [roles, setRoles] = useState<SystemRole[] | undefined>(undefined)
    // Pager state.
    const [page, setPage] = useState(1);
    // Is loading, used to init data load.
    const [isLoading, setIsLoading] = useState(false);
    // Filter state.
    const [formState, setFormState] = useState({field_username: {'value': ''}, field_email: {'value': ''}});
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [formEditState, setFormEditState] = useState({id: '', password: ''});
    const [listedItems, setListedItems] = useState(0);

    /**
     * Load system roles.
     */
    useEffect(() => {
        setIsLoading(true)
        dmGetRoles().then((res: SystemRole[] | undefined) => {
            setRoles(res)
        })
    }, [])

    /**
     * Load defined users after load system roles.
     */
    useEffect(() => {
        if (roles && isLoading) {
            dmGetUsers(formState.field_username.value, formState.field_email.value, page).then((res:any) => {
                res = res.map((user: User) => ({
                    ...user,
                    roles: user.roles?.map((rol_id: string) => roles.find(rol => rol.id === rol_id))
                }))
                setUsers(res)
                setListedItems(res.length);
            })
        }
    }, [roles, isLoading])

    /**
     * Users loaded.
     */
    useEffect(() => {
        setIsLoading(false);
    }, [users])

    /**
     * Page changed.
     */
     useEffect(() => {
        // Page 0 force reload.
        if (page === 0) {
            // Page 0 is not valid, must be 1.
            setPage(1);
        } else {
            setIsLoading(true);
        }
    }, [page])

    /**
     * Update page from pager.
     *
     * @param state
     * @param state.page
     */
    const paginate = (state: {page: number}) => {
        setPage(state.page);
    }

    /**
     * Handle redirect to user edit form
     * 
     * @param data {any}
     */
    const handleEditUser = (data: any) => {
        router.push({
            pathname: "/admin/user/form",
            query: { id: data.id as string }
        })
    }

    /**
     * Handle delete user
     * 
     * @param data {any}
     */
     const handleDeleteUser = (data: any) => {
        console.log('user should be deleted, not implemented yet');
        // router.push({
        //     pathname: "/form",
        //     query: {
        //         id: form_id,
        //         entity_id: cellValues.id as string,
        //     },
        // });
    }

    /**
     * Handle component state.
     *
     * @param event
     */
     const handleButtonSubmit = (event: any) => {
        switch (event.button) {
            case t("Filter"):
                setFormState(event.value);
                setPage(0);
                break;
            default:
                console.log("Unknown button: ", event.button);
                break;
        }
    }

    /**
     * Handle refresh list.
     */
     const handleRefreshUser = () => {
        dmInvalidateUsers().then(() => {
            // Force update.
            setPage(0);
        });
    }

    /**
     * handle redirect to new user form
     */
    const handleAddNewUser = () => {
        router.push('/admin/user/form')
    }

    /**
     * Open dialog.
     * 
     * @param data {any}
     */
    const handleOpenModal = (data: any) => {
        if (smHasPermission('update doctrine user')) {
            if (smGetSession()?.id === data.id) {
                eventManager.dispatch(
                    Events.STATUS_ADD,
                    {
                        level: StatusEventLevel.WARNING,
                        title: t('Change user password'),
                        message: t('It is not possible to change the password of the logged in account.'),
                        isPublic: true,
                    }
                );
            } else {
                setFormEditState({id: data.id, password: generateNewPassword()});
                setIsOpenModal(true);
            }
        }
    }

    /**
     * Generate a new password.
     *
     * @returns {string}
     */
    const generateNewPassword = () => {
        return generatePassword(10);
        // return Math.random().toString(36).slice(2, 12);
    }

    /**
     * Close modal.
     */
    const handleCloseModal = () => {
        setFormEditState({id: '', password: ''});
        setIsOpenModal(false);
    }

    /**
     * Handle change user status.
     */
    const handleChangePassword = () => {
        const updatedUser: any = {
            password: formEditState.password,
        };
        dmUpdateUser(formEditState.id, updatedUser)
            .then(resp => {
                dmInvalidateUsers().then(() => {
                    eventManager.dispatch(
                        Events.STATUS_ADD,
                        {
                            level: StatusEventLevel.INFO,
                            title: t('Change user password'),
                            message: t('The user password was changed.'),
                            isPublic: true,
                        }
                    );

                    setFormEditState({id: '', password: ''});
                    setIsOpenModal(false);
                });
            })
            .catch(() => {
                setFormEditState({id: '', password: ''});
                setIsOpenModal(false);
            });
    }

    /**
     * returns an array with valid options depending on user permssions
     * 
     * @returns {JSX.Element[]}
     */
    const filterButtons = () => {
        let buttons = [<input type="submit" value={t("Filter")} key="button-filter" className={"btn btn-primary"} disabled={isLoading}/>]
        if(smHasPermission('create doctrine user')) {
            buttons.push(
                <button
                    type="submit"
                    value={"form.user"}
                    className={"btn btn-secondary float-right"}
                    onClick={handleAddNewUser}
                    key="button-add-user"
                    disabled={isLoading}
                >
                    <FontAwesomeIcon icon={faPlusCircle} />&nbsp;{t("Add User")}
                </button>
            )
        }
        buttons.push(
            <button
                type="button"
                value={t("Refresh")}
                key="button-refresh"
                className={"btn btn-tertiary"}
                disabled={isLoading}
                onClick={handleRefreshUser}
            >
                <FontAwesomeIcon icon={faSync} />&nbsp;{t("Refresh")}
            </button>);
        return buttons
    }

    return (
        <MainLayout>
            <div className={'users'}>
                <Container>
                    <React.StrictMode>
                        <div className="filters-wrapper">
                            <DynForm
                                key={"filter-form"}
                                structure={getUsersFilterSchema()}
                                buttons={filterButtons()}
                                onSubmit={handleButtonSubmit}
                                value={formState}
                                isLoading={false}
                                withAccordion={false}
                                className={"filters-wrapper-form"}
                            />
                        </div>

                        <div className={"field-description"}>
                            <p>{t("Users in API use Gravatar to user icons, any user can change his icon in:")} <a href="https://gravatar.com/" target={"_blank"} rel="noreferrer">https://gravatar.com/</a></p>
                        </div>

                        <Pager page={page} itemsAmount={listedItems} onChange={paginate} isLoading={isLoading}/>

                        <CardContainer
                            cardType={"CardUser"}
                            cards={users}
                            isLoading={isLoading}
                            buttons={[
                                {
                                    id: 'btnEdit',
                                    label: t("Edit"),
                                    className: "btn-primary",
                                    /**
                                     * Is this button visible?
                                     *
                                     * @param {any} data
                                     * @param {CardButtonsInterface} btn
                                     *
                                     * @returns {boolean}
                                     */
                                    show: (data: any, btn: CardButtonsInterface) => {
                                        let isAdmin = userHasPermission(data, "all permissions");
                                        if (isAdmin && !smHasPermission('all permissions')) {
                                            return false;
                                        }

                                        return smHasPermission('update doctrine user');
                                    },
                                    icon: <FontAwesomeIcon icon={faEdit} />,
                                    onClick: handleEditUser
                                },
                                {
                                    id: 'btnChangePassword',
                                    label: t("Password"),
                                    className: "btn-tertiary",
                                    /**
                                     * Is this button visible?
                                     *
                                     * @param {any} data
                                     * @param {CardButtonsInterface} btn
                                     *
                                     * @returns {boolean}
                                     */
                                    show: (data: any, btn: CardButtonsInterface) => {
                                        let isAdmin = userHasPermission(data, "all permissions");
                                        if (isAdmin && !smHasPermission('all permissions')) {
                                            return false;
                                        }

                                        return smGetSession()?.id !== data.id && smHasPermission('update doctrine user');
                                    },
                                    icon: <FontAwesomeIcon icon={faKey} />,
                                    onClick: handleOpenModal
                                },
                                // {
                                //     id: 'btnDelete',
                                //     label: t("Delete"),
                                //     className: "btn-primary btn-red",
                                //     /**
                                //      * Is this button visible?
                                //      *
                                //      * @returns {boolean}
                                //      */
                                //     show: () => smHasPermission('delete doctrine user'),
                                //     icon: <FontAwesomeIcon icon={faTrash} />,
                                //     onClick: handleDeleteUser
                                // }
                            ]}
                        />

                    </React.StrictMode>
                </Container>
            </div>
            { isOpenModal ? (
                <Modal
                    isOpen={isOpenModal}
                    onRequestClose={handleCloseModal}
                    title={t("Change user password")}
                    size={"small"}
                >
                    <p>{t('Would you like to change their password to ":password"?', {':password': formEditState.password})}</p>
                    <div className="form-actions">
                        <button type="button" className="btn btn-primary green" onClick={handleChangePassword}>{t("Change")}</button>
                        <button type="button" className="btn btn-primary btn-red" onClick={handleCloseModal}>{t("Cancel")}</button>
                    </div>
                </Modal>
            ) : null}
        </MainLayout>
    )
}
