/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import { DynForm } from "@/components/DynForm/DynForm"
import MainLayout from "@/components/layout/MainLayout"
import { Country } from "@/objects/Country"
import { emptyUserData, User } from "@/objects/User"
import { dmAddUser, dmGetCountry, dmGetUser, dmUpdateUser, dmInvalidateUsers } from "@/utils/dataManager"
import t from "@/utils/i18n"
import {smGetSession, smHasPermission} from "@/utils/sessionManager"
import { getTimezone } from "@/utils/siasar";
import { faFloppyDisk, faRotateLeft } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Container } from "@material-ui/core"
import router, { useRouter } from "next/router"
import React, { ReactElement, useEffect, useState } from "react"
import getUserEditSchema from "src/Settings/dynforms/UserEditSchema"
import {str_replace} from "locutus/php/strings";

interface DynFormUserInterface {
    field_id: {value: string | undefined},
    field_username: {value: string | undefined},
    field_email: {value: string | undefined},
    field_password: {value: string | undefined},
    field_roles: any[],
    field_language: {value: string | undefined},
    field_active: {value: string | undefined},
    field_country?: {value: string | undefined | null, class: string | undefined},
    field_administrative_division: any,
    field_timezone: {value: string},
}

/**
 * User edit page.
 *
 * @constructor
 */
const UserForm = () => {

    const query = useRouter();
    const { id } = query.query;

    // Empty DynFormRecordInterface.
    const emptyDynFormUser: DynFormUserInterface = {
        field_id: {value: ''},
        field_username: {value: ''},
        field_email: {value: ''},
        field_password: {value: ''},
        field_roles: [],
        field_language: {value: ''},
        field_active: {value: '1'},
        field_administrative_division: [],
        field_country: {value: '', class: ''},
        field_timezone: {value: getTimezone()},
    }

    // User state.
    const [user, setUser] = useState<User>()
    // Form state.
    const [formUser, setFormUser] = useState(emptyDynFormUser);
    // Form structures.
    const [formStructure, setFormStructure] = useState(getUserEditSchema())
    // Loading state
    const [isLoading, setIsLoading] = useState(true);
    let isCurrentUser: boolean = smGetSession()?.id === id ;

    // Component did mount.
    useEffect(() => {
        setIsLoading(true)
        if (id) {
            dmGetUser(id.toString())
                .then(user => {
                    // password not required for existing user
                    let updatedStructure = {...formStructure};
                    updatedStructure.fields.field_password.settings.required = false;
                    updatedStructure.fields.field_country.settings.meta.disabled = (!isCurrentUser || !smHasPermission("all permissions"));

                    setFormStructure(updatedStructure);
                    setUser(user);
                    setIsLoading(false);
                })
                .catch(() => {
                    setIsLoading(false);
                });
        }
        else {
            setFormStructure(getUserEditSchema())
            setUser(emptyUserData())
            setFormUser(emptyDynFormUser)
            setIsLoading(false)
        }
    }, [id])

    useEffect(() => {
        if(user) {
            setFormUser({
                field_id: {value: user.id},
                field_username: {value: user.username},
                field_email: {value: user.email},
                field_password: {value: undefined},
                field_roles: user.roles?.map((role: string) => ({value: role})) || [],
                field_language: {value: user.language?.replace('/api/v1/languages/', '')},
                field_active: {value: user.active ? '1' : '0'},
                field_administrative_division: user.administrativeDivisions?.map((admDiv:any) => {
                    let div = str_replace('/api/v1/administrative_divisions/', '', admDiv);
                    return {value: div, class:'App\\Entity\\AdministrativeDivision'}
                }) || [],
                field_country: {value: user.countryId, class: 'App\\Entity\\Country'},
                field_timezone: {value: user.timezone ?? getTimezone()},
            })
        }
    }, [user])
    
    // Form buttons.
    let buttons: ReactElement[] = [
        (<button type="submit" key="button-update" className={"btn btn-primary green"} value={t("Save")} disabled={isLoading}><FontAwesomeIcon icon={faFloppyDisk} /> {t("Save")}</button>),
        (<button type="button" key="button-return" onClick={() => router.push('/admin/users')} className={"btn btn-primary float-right"} value={t("Return")} disabled={isLoading}><FontAwesomeIcon icon={faRotateLeft} /> {t("Return")}</button>),
    ];

    /**
     * Is visble a button?
     *
     * @param record {any}
     * @param button {ReactElement}
     * @returns {boolean}
     */
     const buttonsVisibility = (record: any, button:ReactElement) => {
        if (button.key === "button-return") {
            return true;
        }
        if (button.key === "button-update") {
            return smHasPermission('update doctrine user');
        }
    }

    /**
     * Handle form submit.
     *
     * @param event
     */
     const handleButtonSubmit = (event: any) => {
        //  switch (event.button) {
        //      case t("Save"):
                setIsLoading(true)
                if(user) {
                    // Type User.
                    const updatedUser: any = {
                        ...user,
                        username: formUser.field_username.value,
                        email: formUser.field_email.value,
                        password: formUser.field_password.value?.length ? formUser.field_password.value : undefined,
                        roles: formUser.field_roles.map(role => role.value),
                        language: '/api/v1/languages/' + formUser.field_language.value,
                        active: formUser.field_active.value === '1',
                        administrativeDivisions: formUser.field_administrative_division.map((admDiv:any) => '/api/v1/administrative_divisions/' + admDiv.value),
                        country: '/api/v1/countries/' + formUser.field_country?.value ?? '',
                        timezone: formUser.field_timezone.value,
                    }
                    if(id) {
                        dmUpdateUser(id.toString(), updatedUser)
                            .then(resp => {
                                dmInvalidateUsers().then(() => {
                                    // Redirect to /admin/users page.
                                    setIsLoading(false);
                                    router.push('/admin/users');
                                });
                            })
                            .catch(() => {
                                setIsLoading(false);
                            });
                    }
                    else {
                        dmGetCountry().then((country: unknown) => {
                            updatedUser.country = '/api/v1/countries/' + (country as Country).code;
                            dmAddUser(updatedUser)
                                .then(res => {
                                    dmInvalidateUsers().then(() => {
                                        setIsLoading(false)
                                        router.push('/admin/users')
                                    });
                                })
                                .catch(() => {
                                    setIsLoading(false);
                                })
                        })
                    }
                }
            //     break;

            // case t("Return"):
            //     // Redirect to /admin/users page.
            //     router.push('/admin/users');
            //     break;
        // }
    }

    return (
        <MainLayout>
            <div className={'dynform-edit'}>
                <Container>
                    { isCurrentUser ? (
                        <div className="message orange">
                            <p>{t("Editing your account.")}</p>
                        </div>
                    ) : null }
                    <React.StrictMode>
                        <DynForm
                            key={new Date().getTime()}
                            structure={formStructure}
                            buttonsVisibility={buttonsVisibility}
                            buttons={buttons}
                            onSubmit={handleButtonSubmit}
                            value={formUser}
                            isLoading={isLoading}
                        />
                    </React.StrictMode>
                </Container>
            </div>
        </MainLayout>
    )
}

export default UserForm
