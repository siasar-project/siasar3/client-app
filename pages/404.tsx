/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {Container} from "@material-ui/core";
import PageError from "@/components/layout/PageError";
import t from "@/utils/i18n";
import MainLayout from "@/components/layout/MainLayout";
import React from "react";

/**
 * 404 page.
 *
 * @constructor
 */
export default function FourOhFour() {
    return <MainLayout>
            <div className={'error-page'}>
                <Container>
                    <PageError status={404} message={t("Page not found")}/>
                </Container>
            </div>
        </MainLayout>
}
