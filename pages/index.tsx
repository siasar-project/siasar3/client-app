/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

import {
  createStyles,
  Theme,
  WithStyles,
  withStyles,
} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import MainLayout from "../src/components/layout/MainLayout";
import React from "react";
import { Typography } from "@material-ui/core";
import { SiasarContext } from "@/context/SiasarContext";
import t from "@/utils/i18n";

/**
 * Update theme
 *
 * @param {Theme} theme
 * @returns {Theme}
 *   Updated theme.
 */
const styles = (theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(8, 0, 6),
    },
  });

/**
 * Index/home page.
 */
class Index extends React.Component<any & WithStyles<typeof styles>, any> {
  static contextType = SiasarContext;

  /**
   * Init page.
   *
   * @param props
   */
  constructor(props: any) {
    super(props);
  }

  /**
   * Render home page.
   *
   * @returns {any}
   *   Structure.
   */
  render() {
    const { classes } = this.props;
    const { isUser } = this.context;

    return (
      <MainLayout>
        <main>
          <div className={classes.root}>
            <Container maxWidth="md">
              <Typography variant="h4" component="h1" gutterBottom>
                {t("Rural Water and Sanitation Information System")}
              </Typography>
              <Typography variant="body1" component="p" gutterBottom>
                {t("SIASAR, the Rural Water and Sanitation Information System, is a joint initiative launched by the governments of Honduras, Nicaragua, and Panamá, that soon expanded to other countries and regions of Latin America, Asia and Africa. The strategic purpose of this initiative is to have a basic, updated, and comparable information on the sustainability of rural water supply, sanitation, and hygiene (WASH) services in a given country. In essence, SIASAR is intended to be a decision support tool for the rural WASH sector, and is defined on the basis of a set of indicators that are aggregated into a reduced number of indices and dimensions.")}
              </Typography>
              <br/>
              <Typography variant="body1" component="p" gutterBottom>
                {t("It can be used to better understand rural water supply and sanitation services in countries which are experiencing low levels of coverage, limited self-sustainability, and a lack of information to guide evidence-based decision-making. The aim is to support planning, coordination, and evaluation of the sector’s stakeholder activities; to monitor coverage, quality and sustainability of rural water supply and sanitation services; and to record performance of technical assistance providers, including their logistical limitations.")}
              </Typography>
            </Container>
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default withStyles(styles)(Index);
