#!/bin/bash
ps aux | grep node | grep -v grep | awk '{print$2}' | xargs kill 2> /dev/null
echo "Node server stopped"
exit 0
