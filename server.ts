/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
export {};

const next = require('next')
const app_routes = require('./routes')
const app = next({dev: process.env.NEXT_PUBLIC_ENV !== 'production'})
const handler = app_routes.getRequestHandler(app)
const port = process.env.PORT || 3000

const { createServer } = require('http')
app.prepare().then(() => {
    createServer(handler).listen(port)
})
