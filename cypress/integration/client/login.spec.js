/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})


/// see https://www.dolthub.com/blog/2020-10-23-cypress-login-tests/#logging-in-using-a-html-form

context('Window', () => {
    beforeEach(() => {
        cy.visit("/")
    })

    it('Login form - access the login form', () => {
        cy.intercept('/dashboard/*').as('getDashboard')
        // Visit login page on macbook 15
        cy.visit("login");
        //cy.visitViewport("macbook-15"); // `visitViewport` is a custom command

        // Enter username and password in form inputs
        cy.get("#username").type(Cypress.env("test_login_user"));
        cy.get("#password").type(Cypress.env("test_login_pass")).type("{enter}"); // '{enter}' submits the form

        cy.get(".MuiTypography-body2").should("be.visible");
    })
})
