/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
// returning false here prevents Cypress from
// failing the test
return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm currency field.
 */
context('DynForm currency field', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_currency": {
                "id": "field_currency",
                "type": "currency",
                "label": "Field currency",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Field currency",
        "type": "",
        "meta": {
            "title": "Field currency",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field currency",
                            "field_id": "field_currency",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4)
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_currency": {
                "id": "field_currency",
                "type": "currency",
                "label": "Field currency",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Field currency",
        "type": "",
        "meta": {
            "title": "Field currency",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field currency",
                            "field_id": "field_currency",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4)

    let fDataSingle = JSON.stringify({
        "field_currency": {
            "amount": "1111.22",
            "value": "01G0Y8TVF53603F1ZPXBG4Q8HR",
            "class": "App\\Entity\\Currency"
        }
    }, undefined, 4)
    let fDataMulti = JSON.stringify({
        "field_currency": [
            {
                "amount": "1111.22",
                "value": "01G0Y8TVF53603F1ZPXBG4Q8HR",
                "class": "App\\Entity\\Currency"
            },
            {
                "amount": "2222.33",
                "value": "01G0Y8TVF53603F1ZPXBG4Q8HP",
                "class": "App\\Entity\\Currency"
            },
            {
                "amount": "3333.44",
                "value": "01G0Y8TVF53603F1ZPXBG4Q8HJ",
                "class": "App\\Entity\\Currency"
            }
        ]
    }, undefined, 4)

    beforeEach(() => {
        // Mock get currencies from API.
        cy.intercept('GET', '/api/v1/currencies?page=2', []);
        cy.intercept('GET', '/api/v1/currencies?page=1', {fixture: 'currencies.json'});
    });

    it('DynForm Field - simple currency without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_currency_value").should("be.visible");
        cy.get("#field_currency_amount").should("be.visible");
        // Change values.
        cy.get("#field_currency_amount").clear()
        cy.get("#field_currency_amount").type("5555.66");
        cy.get("#field_currency_value").select('01G0Y8TVF53603F1ZPXBG4Q8HJ');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_currency": {
                    "amount": "5555.66",
                    "value": "01G0Y8TVF53603F1ZPXBG4Q8HJ",
                    "class": "App\\Entity\\Currency"
                }
            }, undefined, 4));
        // Change values.
        cy.get("#field_currency_amount").clear()
        cy.get("#field_currency_amount").type("6666.77");
        cy.get("#field_currency_value").select('01G0Y8TVF53603F1ZPXBG4Q8HM');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_currency": {
                    "amount": "6666.77",
                    "value": "01G0Y8TVF53603F1ZPXBG4Q8HM",
                    "class": "App\\Entity\\Currency"
                }
            }, undefined, 4));
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple currency with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_currency_amount").should("be.visible");
        cy.get("#field_currency_value").should("be.visible");
        cy.get("#field_currency_amount").should("have.value", "1111.22");
        cy.get("#field_currency_value").should("have.value", "01G0Y8TVF53603F1ZPXBG4Q8HR");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_currency": {
                    "amount": "1111.22",
                    "value": "01G0Y8TVF53603F1ZPXBG4Q8HR",
                    "class": "App\\Entity\\Currency"
                }
            }, undefined, 4));

        // Change values.
        cy.get("#field_currency_amount").clear()
        cy.get("#field_currency_amount").type("5555.66");
        cy.get("#field_currency_value").select('01G0Y8TVF53603F1ZPXBG4Q8HM');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
        .should("have.text",JSON.stringify({
            "field_currency": {
                "amount": "5555.66",
                "value": "01G0Y8TVF53603F1ZPXBG4Q8HM",
                "class": "App\\Entity\\Currency"
            }
        }, undefined, 4));
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued currency without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_currency__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_currency__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_currency__2_value").should("be.visible");

        // Change value.
        cy.get("#field_currency__0_amount").clear()
        cy.get("#field_currency__0_amount").type("5555.66");
        cy.get("#field_currency__0_value").select('01G0Y8TVF53603F1ZPXBG4Q8HJ');
        cy.get("#field_currency__1_amount").clear()
        cy.get("#field_currency__1_amount").type("6666.77");
        cy.get("#field_currency__1_value").select('01G0Y8TVF53603F1ZPXBG4Q8HM');
        cy.get("#field_currency__2_amount").clear()
        cy.get("#field_currency__2_amount").type("7777.88");
        cy.get("#field_currency__2_value").select('01G0Y8TVF53603F1ZPXBG4Q8HR');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_currency": [
                    {
                        "amount": "5555.66",
                        "value": "01G0Y8TVF53603F1ZPXBG4Q8HJ",
                        "class": "App\\Entity\\Currency"
                    },
                    {
                        "amount": "6666.77",
                        "value": "01G0Y8TVF53603F1ZPXBG4Q8HM",
                        "class": "App\\Entity\\Currency"
                    },
                    {
                        "amount": "7777.88",
                        "value": "01G0Y8TVF53603F1ZPXBG4Q8HR",
                        "class": "App\\Entity\\Currency"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_currency2-remove").click();
        cy.get('#field_currency__2_value').should('not.exist')
        cy.get("#field_currency1-remove").click();
        cy.get('#field_currency__1_value').should('not.exist')
        cy.get("#field_currency0-remove").click();
        cy.get('#field_currency__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_currency": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued currency with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");
        cy.get("#field_currency__0_amount").should("be.visible");
        cy.get("#field_currency__0_value").should("be.visible");
        cy.get("#field_currency__1_amount").should("be.visible");
        cy.get("#field_currency__1_value").should("be.visible");
        cy.get("#field_currency__2_amount").should("be.visible");
        cy.get("#field_currency__2_value").should("be.visible");
 
        // Validate values.
        cy.get("#field_currency__0_amount").should("have.value", "1111.22");
        cy.get("#field_currency__0_value").should("have.value", "01G0Y8TVF53603F1ZPXBG4Q8HR");
        cy.get("#field_currency__1_amount").should("have.value", "2222.33");
        cy.get("#field_currency__1_value").should("have.value", "01G0Y8TVF53603F1ZPXBG4Q8HP");
        cy.get("#field_currency__2_amount").should("have.value", "3333.44");
        cy.get("#field_currency__2_value").should("have.value", "01G0Y8TVF53603F1ZPXBG4Q8HJ");

        // Change value.
        cy.get("#field_currency__0_amount").clear()
        cy.get("#field_currency__0_amount").type("5555.66");
        cy.get("#field_currency__0_value").select('01G0Y8TVF53603F1ZPXBG4Q8HJ');
        cy.get("#field_currency__1_amount").clear()
        cy.get("#field_currency__1_amount").type("6666.77");
        cy.get("#field_currency__1_value").select('01G0Y8TVF53603F1ZPXBG4Q8HM');
        cy.get("#field_currency__2_amount").clear()
        cy.get("#field_currency__2_amount").type("7777.88");
        cy.get("#field_currency__2_value").select('01G0Y8TVF53603F1ZPXBG4Q8HR');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_currency": [
                    {
                        "amount": "5555.66",
                        "value": "01G0Y8TVF53603F1ZPXBG4Q8HJ",
                        "class": "App\\Entity\\Currency"
                    },
                    {
                        "amount": "6666.77",
                        "value": "01G0Y8TVF53603F1ZPXBG4Q8HM",
                        "class": "App\\Entity\\Currency"
                    },
                    {
                        "amount": "7777.88",
                        "value": "01G0Y8TVF53603F1ZPXBG4Q8HR",
                        "class": "App\\Entity\\Currency"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_currency2-remove").click();
        cy.get('#field_currency__2_value').should('not.exist')
        cy.get("#field_currency1-remove").click();
        cy.get('#field_currency__1_value').should('not.exist')
        cy.get("#field_currency0-remove").click();
        cy.get('#field_currency__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_currency": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
