/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm funder_intervention_reference field.
 */
context('Test DynForm funder_intervention_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_funder_intervention": {
                "id": "field_funder_intervention",
                "type": "funder_intervention_reference",
                "label": "Field FunderIntervention",
                "description": "Select a funder intervention",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Funder Intervention form field",
        "type": "",
        "meta": {
            "title": "Funder Intervention form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field funder_intervention",
                            "field_id": "field_funder_intervention",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_funder_intervention": {
                "id": "field_funder_intervention",
                "type": "funder_intervention_reference",
                "label": "Field FunderIntervention",
                "description": "Select a funder intervention",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Institutions form field",
        "type": "",
        "meta": {
            "title": "Institutions form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field funder_intervention",
                            "field_id": "field_funder_intervention",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_funder_intervention": {
            "value": "01G0Y8TVFAH3DS2DV4XGRY7AQZ",
            "class": "App\\Entity\\FunderIntervention"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_funder_intervention": [
            {
                "value": "01G0Y8TVFAH3DS2DV4XGRY7AQZ",
                "class": "App\\Entity\\FunderIntervention"
            },
            {
                "value": "01G0Y8TVFAH3DS2DV4XGRY7AQX",
                "class": "App\\Entity\\FunderIntervention"
            },
            {
                "value": "01G0Y8TVFAH3DS2DV4XGRY7AR3",
                "class": "App\\Entity\\FunderIntervention"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get funder_interventions_interventions from API.
        cy.intercept('GET', '/api/v1/funder_interventions?page=2', []);
        cy.intercept('GET', '/api/v1/funder_interventions?page=1', {fixture: 'funder_interventions.json'});
    })
    
    it('DynForm Field - simple funder_intervention_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_funder_intervention_value").should("be.visible");
        // Change value.
        cy.get("#field_funder_intervention_value").select('01G0Y8TVFAH3DS2DV4XGRY7AR1');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_funder_intervention": {
                    "value": "01G0Y8TVFAH3DS2DV4XGRY7AR1",
                    "class": "App\\Entity\\FunderIntervention"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_funder_intervention_value").select('01G0Y8TVFAH3DS2DV4XGRY7AR3');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_funder_intervention":
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AR3",
                        "class": "App\\Entity\\FunderIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple funder_intervention_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_funder_intervention_value").should("be.visible");
        cy.get("#field_funder_intervention_value").should("have.value", "01G0Y8TVFAH3DS2DV4XGRY7AQZ");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_funder_intervention": {
                    "value": "01G0Y8TVFAH3DS2DV4XGRY7AQZ",
                    "class": "App\\Entity\\FunderIntervention"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_funder_intervention_value").select('01G0Y8TVFAH3DS2DV4XGRY7AR1');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_funder_intervention":
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AR1",
                        "class": "App\\Entity\\FunderIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued funder_intervention_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_funder_intervention__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_funder_intervention__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_funder_intervention__2_value").should("be.visible");

        // Change value.
        cy.get("#field_funder_intervention__0_value").select('01G0Y8TVFAH3DS2DV4XGRY7AR1');
        cy.get("#field_funder_intervention__1_value").select('01G0Y8TVFAH3DS2DV4XGRY7AQX');
        cy.get("#field_funder_intervention__2_value").select('01G0Y8TVFAH3DS2DV4XGRY7AQZ');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_funder_intervention": [
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AR1",
                        "class": "App\\Entity\\FunderIntervention"
                    },
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AQX",
                        "class": "App\\Entity\\FunderIntervention"
                    },
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AQZ",
                        "class": "App\\Entity\\FunderIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_funder_intervention2-remove").click();
        cy.get('#field_funder_intervention__2_value').should('not.exist')
        cy.get("#field_funder_intervention1-remove").click();
        cy.get('#field_funder_intervention__1_value').should('not.exist')
        cy.get("#field_funder_intervention0-remove").click();
        cy.get('#field_funder_intervention__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_funder_intervention": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued funder_intervention_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_funder_intervention__0_value").should("be.visible");
        cy.get("#field_funder_intervention__0_value").should("have.value", "01G0Y8TVFAH3DS2DV4XGRY7AQZ");
        cy.get("#field_funder_intervention__1_value").should("be.visible");
        cy.get("#field_funder_intervention__1_value").should("have.value", "01G0Y8TVFAH3DS2DV4XGRY7AQX");
        cy.get("#field_funder_intervention__2_value").should("be.visible");
        cy.get("#field_funder_intervention__2_value").should("have.value", "01G0Y8TVFAH3DS2DV4XGRY7AR3");

        // Change value.
        cy.get("#field_funder_intervention__0_value").select("01G0Y8TVFAH3DS2DV4XGRY7AR1");
        cy.get("#field_funder_intervention__1_value").select("01G0Y8TVFAH3DS2DV4XGRY7AQX");
        cy.get("#field_funder_intervention__2_value").select("01G0Y8TVFAH3DS2DV4XGRY7AQZ");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_funder_intervention": [
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AR1",
                        "class": "App\\Entity\\FunderIntervention"
                    },
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AQX",
                        "class": "App\\Entity\\FunderIntervention"
                    },
                    {
                        "value": "01G0Y8TVFAH3DS2DV4XGRY7AQZ",
                        "class": "App\\Entity\\FunderIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_funder_intervention2-remove").click();
        cy.get('#field_funder_intervention__2_value').should('not.exist')
        cy.get("#field_funder_intervention1-remove").click();
        cy.get('#field_funder_intervention__1_value').should('not.exist')
        cy.get("#field_funder_intervention0-remove").click();
        cy.get('#field_funder_intervention__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_funder_intervention": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
