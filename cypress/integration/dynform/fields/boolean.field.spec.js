/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm boolean field.
 */
context('Test DynForm boolean field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_boolean": {
                "id": "field_boolean",
                "type": "boolean",
                "label": "Field boolean",
                "description": "Field boolean",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "true_label":"Yes",
                    "false_label": "No",
                    "show_labels": true
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "boolean form field",
        "type": "",
        "meta": {
            "title": "boolean form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field boolean",
                            "field_id": "field_boolean",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_boolean": {
                "id": "field_boolean",
                "type": "boolean",
                "label": "Field boolean",
                "description": "Field boolean",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "true_label": "Yes",
                    "false_label": "No",
                    "show_labels": true
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "boolean form field",
        "type": "",
        "meta": {
            "title": "boolean form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field boolean",
                            "field_id": "field_boolean",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_boolean": {
                "value": "1"
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_boolean": [
            {
                "value": "0"
            },
            {
                "value": "1"
            },
            {
                "value": "0"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple boolean without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".react-switch-button").should("be.visible");
        // Change value.
        cy.get(".react-switch-button").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", '{\n' +
                '    "field_boolean": {\n' +
                '        "value": "1"\n' +
                '    }\n' +
                '}');
        // Change value.
        cy.get(".react-switch-button").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", '{\n' +
                '    "field_boolean": {\n' +
                '        "value": "0"\n' +
                '    }\n' +
                '}');

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple boolean with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".react-switch-button").should("be.visible");
        cy.get("#form_sample_field_boolean").should("be.checked");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_boolean": {
                        "value": "1"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".react-switch-button").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_boolean": {
                    "value": "0"
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued boolean without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get(".field-field_boolean__0 .form-field-boolean").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get(".field-field_boolean__1 .form-field-boolean").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get(".field-field_boolean__2 .form-field-boolean").should("be.visible");

        // Change value.
        cy.get(".field-field_boolean__0 .react-switch-label").click();
        cy.get(".field-field_boolean__1 .react-switch-label").click();
        cy.get(".field-field_boolean__2 .react-switch-label").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_boolean": [
                        {
                            "value": "1"
                        },
                        {
                            "value": "1"
                        },
                        {
                            "value": "1"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_boolean2-remove").click();
        cy.get('.field-field_boolean__2').should('not.exist')
        cy.get("#field_boolean1-remove").click();
        cy.get('.field-field_boolean__1').should('not.exist')
        cy.get("#field_boolean0-remove").click();
        cy.get('.field-field_boolean__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_boolean": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued boolean with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get(".field-field_boolean__0 .form-field-boolean").should("be.visible");
        cy.get("#form_sample_field_boolean__0").should("not.be.checked");
        cy.get(".field-field_boolean__1 .form-field-boolean").should("be.visible");
        cy.get("#form_sample_field_boolean__1").should("be.checked");
        cy.get(".field-field_boolean__2 .form-field-boolean").should("be.visible");
        cy.get("#form_sample_field_boolean__2").should("not.be.checked");

        // Change value.
        cy.get(".field-field_boolean__0 .react-switch-label").click();
        cy.get(".field-field_boolean__1 .react-switch-label").click();
        cy.get(".field-field_boolean__2 .react-switch-label").click();

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_boolean": [
                    {
                        "value": "1"
                    },
                    {
                        "value": "0"
                    },
                    {
                        "value": "1"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_boolean2-remove").click();
        cy.get('.field-field_boolean__2 .form-field-boolean').should('not.exist')
        cy.get("#field_boolean1-remove").click();
        cy.get('.field-field_boolean__1 .form-field-boolean').should('not.exist')
        cy.get("#field_boolean0-remove").click();
        cy.get('.field-field_boolean__0 .form-field-boolean').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_boolean": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
