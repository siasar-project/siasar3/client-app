/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm mail field.
 */
context('Test DynForm mail field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_mail": {
                "id": "field_mail",
                "type": "mail",
                "label": "Field mail",
                "description": "Field mail",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "mail form field",
        "type": "",
        "meta": {
            "title": "mail form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field mail",
                            "field_id": "field_mail",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_mail": {
                "id": "field_mail",
                "type": "mail",
                "label": "Field mail",
                "description": "Field mail",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "mail form field",
        "type": "",
        "meta": {
            "title": "mail form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field mail",
                            "field_id": "field_mail",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_mail": {
                "value": "mail@example.com"
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_mail": [
            {
                "value": ""
            },
            {
                "value": "mail@example.com"
            },
            {
                "value": ""
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple mail without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_mail").should("be.visible");
        // Change value.
        cy.get("#field_mail").type('mail@example.com');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_mail": {
                        "value": "mail@example.com"
                    }
                }, undefined, 4));
        // Set empty value.
        cy.get("#field_mail").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_mail": {
                        "value": ""
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple mail with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_mail").should("be.visible");
        cy.get("#field_mail").should('have.value', 'mail@example.com');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_mail": {
                        "value": "mail@example.com"
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_mail").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_mail": {
                    "value": ""
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued mail without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_mail__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_mail__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_mail__2").should("be.visible");

        // Change value.
        cy.get("#field_mail__0").type('mail1@example.com');
        cy.get("#field_mail__1").type('mail2@example.com');
        cy.get("#field_mail__2").type('mail3@example.com');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_mail": [
                        {
                            "value": "mail1@example.com"
                        },
                        {
                            "value": "mail2@example.com"
                        },
                        {
                            "value": "mail3@example.com"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_mail2-remove").click();
        cy.get('#field_mail__2').should('not.exist')
        cy.get("#field_mail1-remove").click();
        cy.get('#field_mail__1').should('not.exist')
        cy.get("#field_mail0-remove").click();
        cy.get('#field_mail__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_mail": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued mail with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_mail__0").should("be.visible");
        cy.get("#field_mail__0").should('have.value', '');
        cy.get("#field_mail__1").should("be.visible");
        cy.get("#field_mail__1").should('have.value', 'mail@example.com');
        cy.get("#field_mail__2").should("be.visible");
        cy.get("#field_mail__2").should('have.value', '');

        // Change value.
        cy.get("#field_mail__0").type('mail1@example.com');
        cy.get("#field_mail__1").type('{selectAll}{del}');
        cy.get("#field_mail__2").type('mail2@example.com');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_mail": [
                    {
                        "value": "mail1@example.com"
                    },
                    {
                        "value": ""
                    },
                    {
                        "value": "mail2@example.com"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_mail2-remove").click();
        cy.get('#field_mail__2').should('not.exist')
        cy.get("#field_mail1-remove").click();
        cy.get('#field_mail__1').should('not.exist')
        cy.get("#field_mail0-remove").click();
        cy.get('#field_mail__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_mail": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
