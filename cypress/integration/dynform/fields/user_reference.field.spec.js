/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm user_reference field.
 */
context('Test DynForm user_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_user": {
                "id": "field_user",
                "type": "user_reference",
                "label": "Field User",
                "description": "Select a user",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "User form field",
        "type": "",
        "meta": {
            "title": "User form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field user",
                            "field_id": "field_user",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_user": {
                "id": "field_user",
                "type": "user_reference",
                "label": "Field User",
                "description": "Select a user",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "User form field",
        "type": "",
        "meta": {
            "title": "User form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field user",
                            "field_id": "field_user",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_user": {
            "value": "01FYBRJE90N9B37MABFDG1DD7S",
            "class": "App\\Entity\\User"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_user": [
            {
                "value": "01FYBRJE90N9B37MABFDG1DD7S",
                "class": "App\\Entity\\User"
            },
            {
                "value": "01FYBRJG4JVSHPEM8PH64NNDJ3",
                "class": "App\\Entity\\User"
            },
            {
                "value": "01FYBRJEWRTQ0S006F3WYMS7XK",
                "class": "App\\Entity\\User"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get users from API.
        cy.intercept('GET', '/api/v1/users?page=2', []);
        cy.intercept('GET', '/api/v1/users?page=1', {fixture: 'users.json'});
    })

    it('DynForm Field - simple user_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_user_value").should("be.visible");
        // Change value.
        cy.get("#field_user_value").select('01FYBRJE90N9B37MABFDG1DD7S');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_user": {
                    "value": "01FYBRJE90N9B37MABFDG1DD7S",
                    "class": "App\\Entity\\User"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_user_value").select('01FYBRJEJXWEYJTNHZR20X4892');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_user":
                    {
                        "value": "01FYBRJEJXWEYJTNHZR20X4892",
                        "class": "App\\Entity\\User"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple user_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_user_value").should("be.visible");
        cy.get("#field_user_value").should("have.value", "01FYBRJE90N9B37MABFDG1DD7S");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_user": {
                    "value": "01FYBRJE90N9B37MABFDG1DD7S",
                    "class": "App\\Entity\\User"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_user_value").select('01FYBRJEJXWEYJTNHZR20X4892');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_user":
                    {
                        "value": "01FYBRJEJXWEYJTNHZR20X4892",
                        "class": "App\\Entity\\User"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued user_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_user__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_user__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_user__2_value").should("be.visible");

        // Change value.
        //cy.get("#field_user_value").select('01FYBRJEJXWEYJTNHZR20X4892');
        cy.get("#field_user__0_value").select('01FYBRJE90N9B37MABFDG1DD7S');
        cy.get("#field_user__1_value").select('01FYBRJEJXWEYJTNHZR20X4892');
        cy.get("#field_user__2_value").select('01FYBRJEWRTQ0S006F3WYMS7XK');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_user": [
                    {
                        "value": "01FYBRJE90N9B37MABFDG1DD7S",
                        "class": "App\\Entity\\User"
                    },
                    {
                        "value": "01FYBRJEJXWEYJTNHZR20X4892",
                        "class": "App\\Entity\\User"
                    },
                    {
                        "value": "01FYBRJEWRTQ0S006F3WYMS7XK",
                        "class": "App\\Entity\\User"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_user2-remove").click();
        cy.get('#field_user__2_value').should('not.exist')
        cy.get("#field_user1-remove").click();
        cy.get('#field_user__1_value').should('not.exist')
        cy.get("#field_user0-remove").click();
        cy.get('#field_user__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_user": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued user_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_user__0_value").should("be.visible");
        cy.get("#field_user__0_value").should("have.value", "01FYBRJE90N9B37MABFDG1DD7S");
        cy.get("#field_user__1_value").should("be.visible");
        cy.get("#field_user__1_value").should("have.value", "01FYBRJG4JVSHPEM8PH64NNDJ3");
        cy.get("#field_user__2_value").should("be.visible");
        cy.get("#field_user__2_value").should("have.value", "01FYBRJEWRTQ0S006F3WYMS7XK");

        // Change value.
        cy.get("#field_user__0_value").select("01FYBRJEWRTQ0S006F3WYMS7XK");
        cy.get("#field_user__1_value").select("01FYBRJE90N9B37MABFDG1DD7S");
        cy.get("#field_user__2_value").select("01FYBRJG4JVSHPEM8PH64NNDJ3");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_user": [
                    {
                        "value": "01FYBRJEWRTQ0S006F3WYMS7XK",
                        "class": "App\\Entity\\User"
                    },
                    {
                        "value": "01FYBRJE90N9B37MABFDG1DD7S",
                        "class": "App\\Entity\\User"
                    },
                    {
                        "value": "01FYBRJG4JVSHPEM8PH64NNDJ3",
                        "class": "App\\Entity\\User"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_user2-remove").click();
        cy.get('#field_user__2_value').should('not.exist')
        cy.get("#field_user1-remove").click();
        cy.get('#field_user__1_value').should('not.exist')
        cy.get("#field_user0-remove").click();
        cy.get('#field_user__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_user": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
