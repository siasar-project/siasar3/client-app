/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm integer field.
 */
context('Test DynForm integer field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_integer": {
                "id": "field_integer",
                "type": "integer",
                "label": "Field integer",
                "description": "Field integer",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "integer form field",
        "type": "",
        "meta": {
            "title": "integer form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field integer",
                            "field_id": "field_integer",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_integer": {
                "id": "field_integer",
                "type": "integer",
                "label": "Field integer",
                "description": "Field integer",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "integer form field",
        "type": "",
        "meta": {
            "title": "integer form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field integer",
                            "field_id": "field_integer",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_integer": {
                "value": "123456"
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_integer": [
            {
                "value": ""
            },
            {
                "value": "123456"
            },
            {
                "value": ""
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple integer without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_integer").should("be.visible");
        // Change value.
        cy.get("#field_integer").type('123456');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_integer": {
                        "value": "123456"
                    }
                }, undefined, 4));
        // Set empty value.
        cy.get("#field_integer").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_integer": {
                        "value": ""
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple integer with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_integer").should("be.visible");
        cy.get("#field_integer").should('have.value', '123456');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_integer": {
                        "value": "123456"
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_integer").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_integer": {
                    "value": ""
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued integer without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_integer__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_integer__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_integer__2").should("be.visible");

        // Change value.
        cy.get("#field_integer__0").type('1234561');
        cy.get("#field_integer__1").type('1234562');
        cy.get("#field_integer__2").type('1234563');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_integer": [
                        {
                            "value": "1234561"
                        },
                        {
                            "value": "1234562"
                        },
                        {
                            "value": "1234563"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_integer2-remove").click();
        cy.get('#field_integer__2').should('not.exist')
        cy.get("#field_integer1-remove").click();
        cy.get('#field_integer__1').should('not.exist')
        cy.get("#field_integer0-remove").click();
        cy.get('#field_integer__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_integer": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued integer with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_integer__0").should("be.visible");
        cy.get("#field_integer__0").should('have.value', '');
        cy.get("#field_integer__1").should("be.visible");
        cy.get("#field_integer__1").should('have.value', '123456');
        cy.get("#field_integer__2").should("be.visible");
        cy.get("#field_integer__2").should('have.value', '');

        // Change value.
        cy.get("#field_integer__0").type('1234561');
        cy.get("#field_integer__1").type('{selectAll}{del}');
        cy.get("#field_integer__2").type('1234562');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_integer": [
                    {
                        "value": "1234561"
                    },
                    {
                        "value": ""
                    },
                    {
                        "value": "1234562"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_integer2-remove").click();
        cy.get('#field_integer__2').should('not.exist')
        cy.get("#field_integer1-remove").click();
        cy.get('#field_integer__1').should('not.exist')
        cy.get("#field_integer0-remove").click();
        cy.get('#field_integer__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_integer": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
