/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm pump_type_reference field.
 */
context('Test DynForm pump_type_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_pump_type": {
                "id": "field_pump_type",
                "type": "pump_type_reference",
                "label": "Field PumpType",
                "description": "Select a pump_type",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "PumpType form field",
        "type": "",
        "meta": {
            "title": "PumpType form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field pump_type",
                            "field_id": "field_pump_type",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_pump_type": {
                "id": "field_pump_type",
                "type": "pump_type_reference",
                "label": "Field PumpType",
                "description": "Select a pump_type",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "PumpType form field",
        "type": "",
        "meta": {
            "title": "PumpType form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field pump_type",
                            "field_id": "field_pump_type",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_pump_type": {
            "value": "01G0Y8TVGMCMADXKV6D12S291W",
            "class": "App\\Entity\\PumpType"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_pump_type": [
            {
                "value": "01G0Y8TVGMCMADXKV6D12S291W",
                "class": "App\\Entity\\PumpType"
            },
            {
                "value": "01G0Y8TVGMCMADXKV6D12S2922",
                "class": "App\\Entity\\PumpType"
            },
            {
                "value": "01G0Y8TVGMCMADXKV6D12S2926",
                "class": "App\\Entity\\PumpType"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get pump_types from API.
        cy.intercept('GET', '/api/v1/pump_types?page=2', []);
        cy.intercept('GET', '/api/v1/pump_types?page=1', {fixture: 'pump_types.json'});
    })

    
    it('DynForm Field - simple pump_type_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_pump_type_value").should("be.visible");
        // Change value.
        cy.get("#field_pump_type_value").select('01G0Y8TVGMCMADXKV6D12S291Y');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_pump_type": {
                    "value": "01G0Y8TVGMCMADXKV6D12S291Y",
                    "class": "App\\Entity\\PumpType"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_pump_type_value").select('01G0Y8TVGMCMADXKV6D12S291W');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_pump_type":
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S291W",
                        "class": "App\\Entity\\PumpType"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple pump_type_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_pump_type_value").should("be.visible");
        cy.get("#field_pump_type_value").should("have.value", "01G0Y8TVGMCMADXKV6D12S291W");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_pump_type": {
                    "value": "01G0Y8TVGMCMADXKV6D12S291W",
                    "class": "App\\Entity\\PumpType"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_pump_type_value").select('01G0Y8TVGMCMADXKV6D12S291Y');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_pump_type":
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S291Y",
                        "class": "App\\Entity\\PumpType"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued pump_type_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_pump_type__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_pump_type__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_pump_type__2_value").should("be.visible");

        // Change value.
        cy.get("#field_pump_type__0_value").select('01G0Y8TVGMCMADXKV6D12S291Y');
        cy.get("#field_pump_type__1_value").select('01G0Y8TVGMCMADXKV6D12S2920');
        cy.get("#field_pump_type__2_value").select('01G0Y8TVGMCMADXKV6D12S292A');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_pump_type": [
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S291Y",
                        "class": "App\\Entity\\PumpType"
                    },
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S2920",
                        "class": "App\\Entity\\PumpType"
                    },
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S292A",
                        "class": "App\\Entity\\PumpType"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_pump_type2-remove").click();
        cy.get('#field_pump_type__2_value').should('not.exist')
        cy.get("#field_pump_type1-remove").click();
        cy.get('#field_pump_type__1_value').should('not.exist')
        cy.get("#field_pump_type0-remove").click();
        cy.get('#field_pump_type__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_pump_type": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued pump_type_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_pump_type__0_value").should("be.visible");
        cy.get("#field_pump_type__0_value").should("have.value", "01G0Y8TVGMCMADXKV6D12S291W");
        cy.get("#field_pump_type__1_value").should("be.visible");
        cy.get("#field_pump_type__1_value").should("have.value", "01G0Y8TVGMCMADXKV6D12S2922");
        cy.get("#field_pump_type__2_value").should("be.visible");
        cy.get("#field_pump_type__2_value").should("have.value", "01G0Y8TVGMCMADXKV6D12S2926");

        // Change value.
        cy.get("#field_pump_type__0_value").select("01G0Y8TVGMCMADXKV6D12S291Y");
        cy.get("#field_pump_type__1_value").select("01G0Y8TVGMCMADXKV6D12S2920");
        cy.get("#field_pump_type__2_value").select("01G0Y8TVGMCMADXKV6D12S292A");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_pump_type": [
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S291Y",
                        "class": "App\\Entity\\PumpType"
                    },
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S2920",
                        "class": "App\\Entity\\PumpType"
                    },
                    {
                        "value": "01G0Y8TVGMCMADXKV6D12S292A",
                        "class": "App\\Entity\\PumpType"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_pump_type2-remove").click();
        cy.get('#field_pump_type__2_value').should('not.exist')
        cy.get("#field_pump_type1-remove").click();
        cy.get('#field_pump_type__1_value').should('not.exist')
        cy.get("#field_pump_type0-remove").click();
        cy.get('#field_pump_type__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_pump_type": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
