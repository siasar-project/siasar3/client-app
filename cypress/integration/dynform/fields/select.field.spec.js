/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm select field.
 */
context('Test DynForm select field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_select": {
                "id": "field_select",
                "type": "select",
                "label": "Field select",
                "description": "Field select",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": "One",
                        "2": "Two",
                        "3": "Three",
                        "4": "Four"
                    }
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Select form field",
        "type": "",
        "meta": {
            "title": "Select form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field select",
                            "field_id": "field_select",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_select": {
                "id": "field_select",
                "type": "select",
                "label": "Field select",
                "description": "Field select",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": "One",
                        "2": "Two",
                        "3": "Three",
                        "4": "Four"
                    }
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Select form field",
        "type": "",
        "meta": {
            "title": "Select form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field select",
                            "field_id": "field_select",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_select": {
                "value": "3"
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_select": [
                {
                    "value": "2"
                },
                {
                    "value": "3"
                },
                {
                    "value": "4"
                }
            ]
        }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple select without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_select").should("be.visible");
        // Change value.
        cy.get("#field_select").select('4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_select": {
                        "value": "4"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple select with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_select").should("be.visible");
        cy.get("#field_select").should("have.value", 3);

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_select": {
                        "value": "3"
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_select").select('4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_select": {
                    "value": "4"
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued select without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_select__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_select__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_select__2").should("be.visible");

        // Change value.
        cy.get("#field_select__0").select('2');
        cy.get("#field_select__1").select('3');
        cy.get("#field_select__2").select('4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_select": [
                        {
                            "value": "2"
                        },
                        {
                            "value": "3"
                        },
                        {
                            "value": "4"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_select2-remove").click();
        cy.get('#field_select__2').should('not.exist')
        cy.get("#field_select1-remove").click();
        cy.get('#field_select__1').should('not.exist')
        cy.get("#field_select0-remove").click();
        cy.get('#field_select__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_select": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued select with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_select__0").should("be.visible");
        cy.get("#field_select__0").should("have.value", 2);
        cy.get("#field_select__1").should("be.visible");
        cy.get("#field_select__1").should("have.value", 3);
        cy.get("#field_select__2").should("be.visible");
        cy.get("#field_select__2").should("have.value", 4);

        // Change value.
        cy.get("#field_select__0").select('1');
        cy.get("#field_select__1").select('2');
        cy.get("#field_select__2").select('3');

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_select": [
                        {
                            "value": "1"
                        },
                        {
                            "value": "2"
                        },
                        {
                            "value": "3"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_select2-remove").click();
        cy.get('#field_select__2').should('not.exist')
        cy.get("#field_select1-remove").click();
        cy.get('#field_select__1').should('not.exist')
        cy.get("#field_select0-remove").click();
        cy.get('#field_select__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_select": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
