/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm type_tap_reference field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_type_tap_reference": {
                "id": "field_type_tap_reference",
                "type": "type_tap_reference",
                "label": "Field type_tap_reference",
                "description": "Field type_tap_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "type_tap_reference form field",
        "type": "",
        "meta": {
            "title": "type_tap_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field type_tap_reference",
                            "field_id": "field_type_tap_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_type_tap_reference": {
                "id": "field_type_tap_reference",
                "type": "type_tap_reference",
                "label": "Field type_tap_reference",
                "description": "Field type_tap_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "type_tap_reference form field",
        "type": "",
        "meta": {
            "title": "type_tap_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field type_tap_reference",
                            "field_id": "field_type_tap_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_type_tap_reference": {
            "value": "01G3ZZ3NH04AN3A2K4V6H8C27N",
            "class": "App\\Entity\\TypeTap"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_type_tap_reference": [
            {
                "value": "01G3ZZ3NH04AN3A2K4V6H8C27N",
                "class": "App\\Entity\\TypeTap"
            },
            {
                "value": "01G3ZZ3NH04AN3A2K4V6H8C27P",
                "class": "App\\Entity\\TypeTap"
            },
            {
                "value": "01G3ZZ3NH04AN3A2K4V6H8C27Q",
                "class": "App\\Entity\\TypeTap"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/type_taps?page=2', []);
        cy.intercept('GET', '/api/v1/type_taps?page=1', {fixture: 'type_taps.json'});
    })

    it('DynForm Field - simple type_tap_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-type_tap_reference").should("be.visible");
        // Change value.
        cy.get(".form-field-type_tap_reference [type='radio']").check('01G3ZZ3NH04AN3A2K4V6H8C27Q');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_type_tap_reference": {
                        "value": "01G3ZZ3NH04AN3A2K4V6H8C27Q",
                        "class": "App\\Entity\\TypeTap"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple type_tap_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-type_tap_reference").should("be.visible");
        cy.get(".form-field-type_tap_reference :checked").should("be.checked").and('have.value', '01G3ZZ3NH04AN3A2K4V6H8C27N');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_type_tap_reference": {
                        "value": "01G3ZZ3NH04AN3A2K4V6H8C27N",
                        "class": "App\\Entity\\TypeTap"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-type_tap_reference [type='radio']").check('01G3ZZ3NH04AN3A2K4V6H8C27Q');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_type_tap_reference": {
                        "value": "01G3ZZ3NH04AN3A2K4V6H8C27Q",
                        "class": "App\\Entity\\TypeTap"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued type_tap_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK5]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK6]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK7]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27N]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27P]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27Q]').should("be.visible");

        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C27N');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C27P');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C27Q');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_type_tap_reference": [
                        {
                            "value": "01G3ZZ3NH04AN3A2K4V6H8C27N",
                            "class": "App\\Entity\\TypeTap"
                        },
                        {
                            "value": "01G3ZZ3NH04AN3A2K4V6H8C27P",
                            "class": "App\\Entity\\TypeTap"
                        },
                        {
                            "value": "01G3ZZ3NH04AN3A2K4V6H8C27Q",
                            "class": "App\\Entity\\TypeTap"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued type_tap_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK5]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK5]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK6]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK6]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK7]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3MGZEPSAGCNBYEA4HAK7]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27N]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27N]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27P]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27P]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27Q]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C27Q]').should('be.checked');
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3MGZEPSAGCNBYEA4HAK5');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3MGZEPSAGCNBYEA4HAK7');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3ZZ3NH04AN3A2K4V6H8C27N');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3ZZ3NH04AN3A2K4V6H8C27Q');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_type_tap_reference": [
                        {
                            "value": '01G3ZZ3MGZEPSAGCNBYEA4HAK5',
                            "class": "App\\Entity\\TypeTap"
                        },
                        {
                            "value": "01G3ZZ3MGZEPSAGCNBYEA4HAK7",
                            "class": "App\\Entity\\TypeTap"
                        },
                        {
                            "value": "01G3ZZ3NH04AN3A2K4V6H8C27P",
                            "class": "App\\Entity\\TypeTap"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
