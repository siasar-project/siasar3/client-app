/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm volume field.
 */
context('Test DynForm volume field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_volume": {
                "id": "field_volume",
                "type": "volume",
                "label": "Field volume",
                "description": "Field volume",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "volume form field",
        "type": "",
        "meta": {
            "title": "volume form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field volume",
                            "field_id": "field_volume",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_volume": {
                "id": "field_volume",
                "type": "volume",
                "label": "Field volume",
                "description": "Field volume",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "volume form field",
        "type": "",
        "meta": {
            "title": "volume form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field volume",
                            "field_id": "field_volume",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_volume": {
            "value": "1234.56",
            "unit": "cubic kilometre"
        }
    }, undefined, 4);

    let fDataMulti = JSON.stringify({
        "field_volume": [
            {
                "value": "",
                "unit": "cubic kilometre"
            },
            {
                "value": "1234.56",
                "unit": "cubic centímetre"
            },
            {
                "value": "",
                "unit": "barrel"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
        cy.intercept('GET', '/api/v1/configuration/system.units.volume', { fixture: 'unit_volume.json' })
    })

    it('Dynform Field - simple volume without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get("#field_volume_unit").should("be.visible");
        cy.get("#field_volume").should("be.visible");
        // Change value.
        cy.get("#field_volume").type('1234.56');
        cy.get("#field_volume_unit").select('cubic kilometre')
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": {
                    "value": "1234.56",
                    "unit": "cubic kilometre"
                }
            }, undefined, 4));
        // Set empty value.
        cy.get("#field_volume").type('{selectAll}{del}');
        cy.get("#field_volume_unit").select('cubic metre')

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": {
                    "value": "",
                    "unit": "cubic metre"
                }
            }, undefined, 4));
    })

    it('Dynform Field - simple volume with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_volume_unit").should("be.visible");
        cy.get("#field_volume").should("be.visible");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", fDataSingle);

        // Clear data.
        cy.get("#field_volume").type('{selectAll}{del}');
        cy.get("#field_volume_unit").select('cubic metre');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": {
                    "value": "",
                    "unit": "cubic metre"
                }
            }, undefined, 4));

    })

    it('Dynform Field - multivalued volume without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_volume__0").should("be.visible");
        cy.get("#field_volume__0_unit").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_volume__1").should("be.visible");
        cy.get("#field_volume__1_unit").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_volume__2").should("be.visible");
        cy.get("#field_volume__2_unit").should("be.visible");

        // Change values.
        cy.get("#field_volume__0").type(1234.56)
        cy.get("#field_volume__0_unit").select("litre")
        cy.get("#field_volume__1").type(2234.56)
        cy.get("#field_volume__1_unit").select("barrel")
        cy.get("#field_volume__2").type(3234.56)
        cy.get("#field_volume__2_unit").select("cubic decametre")

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": [
                    {
                        "value": "1234.56",
                        "unit": "litre"
                    },
                    {
                        "value": "2234.56",
                        "unit": "barrel"
                    },
                    {
                        "value": "3234.56",
                        "unit": "cubic decametre"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_volume2-remove").click();
        cy.get('#field_volume_2').should('not.exist')
        cy.get("#field_volume1-remove").click();
        cy.get('#field_volume_1').should('not.exist')
        cy.get("#field_volume0-remove").click();
        cy.get('#field_volume_0').should('not.exist')

        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": []
            }, undefined, 4));
    })

    it('DynForm Field - multivalued volume with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_volume__0").should("be.visible");
        cy.get("#field_volume__0_unit").should("be.visible");
        cy.get("#field_volume__0").should('have.value', '');
        cy.get("#field_volume__0_unit").should('have.value', 'cubic kilometre');
        cy.get("#field_volume__1").should("be.visible");
        cy.get("#field_volume__1_unit").should("be.visible");
        cy.get("#field_volume__1").should('have.value', '1234.56');
        cy.get("#field_volume__1_unit").should('have.value', 'cubic centímetre');
        cy.get("#field_volume__2").should("be.visible");
        cy.get("#field_volume__2_unit").should("be.visible");
        cy.get("#field_volume__2").should('have.value', '');
        cy.get("#field_volume__2_unit").should('have.value', 'barrel');

        // Change values.
        cy.get("#field_volume__0").type(1234.56)
        cy.get("#field_volume__1").type('{selectAll}{del}')
        cy.get("#field_volume__1_unit").select('litre')
        cy.get("#field_volume__2").type(2234.56)

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": [
                    {
                        "value": "1234.56",
                        "unit": "cubic kilometre"
                    },
                    {
                        "value": "",
                        "unit": "litre"
                    },
                    {
                        "value": "2234.56",
                        "unit": "barrel"
                    }
                ]
            },  undefined, 4))

        // Remove values.
        cy.get("#field_volume2-remove").click();
        cy.get('#field_volume_2').should('not.exist')
        cy.get("#field_volume1-remove").click();
        cy.get('#field_volume_1').should('not.exist')
        cy.get("#field_volume0-remove").click();
        cy.get('#field_volume_0').should('not.exist')

        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_volume": []
            }, undefined, 4));
    })
})
