/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm diameter field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_diameter": {
                "id": "field_diameter",
                "type": "diameter",
                "label": "Field diameter",
                "description": "Field diameter",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "diameter form field",
        "type": "",
        "meta": {
            "title": "diameter form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field diameter",
                            "field_id": "field_diameter",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_diameter": {
                "id": "field_diameter",
                "type": "diameter",
                "label": "Field diameter",
                "description": "Field diameter",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "diameter form field",
        "type": "",
        "meta": {
            "title": "diameter form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field diameter",
                            "field_id": "field_diameter",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_diameter": {
            "value": "58",
            "unit": "milimetre"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_diameter": [
            {
                "value": "81",
                "unit": "milimetre"
            },
            {
                "value": "22",
                "unit": "milimetre"
            },
            {
                "value": "66",
                "unit": "milimetre"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/default_diameters?page=2', []);
        cy.intercept('GET', '/api/v1/default_diameters?page=1', {fixture: 'default_diameters.json'});
        cy.intercept('GET', '/api/v1/configuration/system.units.length', { fixture: 'unit_length.json' })
    })

    it('DynForm Field - simple diameter without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-diameter").should("be.visible");
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Open diameter options.
        cy.get("#button-toggle-diameters").click()
        // Check modal visibility.
        cy.get(".modal-diameter").should("be.visible");
        // Change value.
        cy.get("#field_diameter-option-22-milimetre").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_diameter": {
                        "value": "22",
                        "unit": "milimetre"
                    }
                }, undefined, 4));
    })

    it('DynForm Field - simple diameter with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get("#field_diameter").should("be.visible").and('have.value', '58');
        cy.get("#field_diameter_unit").should("be.visible").and('have.value', 'milimetre');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_diameter": {
                        "value": "58",
                        "unit": "milimetre"
                    }
                }, undefined, 4));

        // Change value - Open diameter options.
        cy.get("#button-toggle-diameters").click()
        // Check modal visibility.
        cy.get(".modal-diameter").should("be.visible");
        // Change value.
        cy.get("#field_diameter-option-35-milimetre").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_diameter": {
                        "value": "35",
                        "unit": "milimetre"
                    }
                }, undefined, 4));

    })

    it('DynForm Field - multivalued diameter without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");
        // Add values.
        cy.get(".dynform-add button").click();
        cy.get(".dynform-add button").click();
        cy.get("#field_diameter__0").should("be.visible");
        cy.get("#field_diameter__1").should("be.visible");
        // Change value.
        cy.get("#button-toggle-diameters__0").click();
        cy.get("#field_diameter-option-22-milimetre__0").click();
        cy.get("#button-toggle-diameters__1").click();
        cy.get("#field_diameter-option-58-milimetre__1").click();
        // Check fields and values.
        cy.get("#field_diameter__0").should("be.visible").and('have.value', '22');
        cy.get("#field_diameter__0_unit").should("be.visible").and('have.value', 'milimetre');
        cy.get("#field_diameter__1").should("be.visible").and('have.value', '58');
        cy.get("#field_diameter__1_unit").should("be.visible").and('have.value', 'milimetre');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_diameter": [
                        {
                            "value": "22",
                            "unit": "milimetre"
                        },
                        {
                            "value": "58",
                            "unit": "milimetre"
                        }
                    ]
                }, undefined, 4));
    })

    it('DynForm Field - multivalued diameter with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");
        cy.get("#field_diameter__0").should("be.visible").and('have.value', '81');
        cy.get("#field_diameter__0_unit").should("be.visible").and('have.value', 'milimetre');
        cy.get("#field_diameter__1").should("be.visible").and('have.value', '22');
        cy.get("#field_diameter__1_unit").should("be.visible").and('have.value', 'milimetre');
        cy.get("#field_diameter__2").should("be.visible").and('have.value', '66');
        cy.get("#field_diameter__2_unit").should("be.visible").and('have.value', 'milimetre');
        // Submit form.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_diameter": [
                    {
                        "value": "81",
                        "unit": "milimetre"
                    },
                    {
                        "value": "22",
                        "unit": "milimetre"
                    },
                    {
                        "value": "66",
                        "unit": "milimetre"
                    }
                ]
            }, undefined, 4));
        // Change value.
        cy.get("#button-toggle-diameters__0").click();
        cy.get("#field_diameter-option-22-milimetre__0").click();
        cy.get("#button-toggle-diameters__1").click();
        cy.get("#field_diameter-option-58-milimetre__1").click();
        cy.get("#field_diameter2-remove").click();
        // Check fields and values.
        cy.get("#field_diameter__0").should("be.visible").and('have.value', '22');
        cy.get("#field_diameter__0_unit").should("be.visible").and('have.value', 'milimetre');
        cy.get("#field_diameter__1").should("be.visible").and('have.value', '58');
        cy.get("#field_diameter__1_unit").should("be.visible").and('have.value', 'milimetre');
        cy.get("#field_diameter__2").should("not.exist");
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_diameter": [
                    {
                        "value": "22",
                        "unit": "milimetre"
                    },
                    {
                        "value": "58",
                        "unit": "milimetre"
                    }
                ]
            }, undefined, 4));
        // Remove all fields.
        cy.get("#field_diameter1-remove").click();
        cy.get("#field_diameter0-remove").click();
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_diameter": []
            }, undefined, 4));
    })

})
