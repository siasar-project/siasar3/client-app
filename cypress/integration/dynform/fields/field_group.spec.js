/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * Test DynForm field group.
 */
context('Test DynForm field group', () => {
    // Form structure.
    let fStruct = JSON.stringify({
        "description": "",
        "fields": {},
        "id": "form.sample",
        "requires": [],
        "title": "Groups form field",
        "type": "",
        "meta": {
            "title": "Groups form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "QUESTIONNAIRE DATA",
                    "help": "Note for the interviewer: It’s recommended to take at least one photo that is as representative as possible of the service provider",
                    "children": {}
                },
                {
                    "id": "1",
                    "title": "GENERAL INFORMATION",
                    "help": "",
                    "children": {
                        "1.1": {
                            "id": "1.1",
                            "title": "Location of community",
                            "help": "suggested to use coordinates and location of the community",
                            "children": {
                                "1.1.1": {
                                    "id": "1.1.1",
                                    "title": "Location of community",
                                    "help": "suggested to use coordinates and location of the community",
                                    "children": {}
                                }
                            }
                        }
                    }
                },
                {
                    "id": "4",
                    "title": "HYGIENE",
                    "help": "",
                    "children": {}
                }
            ]
        }
    }, undefined, 4);
    let fStructWithFields = JSON.stringify({
        "description": "",
        "fields": {
            "field_text_1": {
                "id": "field_text_1",
                "type": "short_text",
                "label": "Text 1",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_text_2": {
                "id": "field_text_2",
                "type": "short_text",
                "label": "Text 1",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.0",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_text_3": {
                "id": "field_text_3",
                "type": "short_text",
                "label": "Text 1",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.1.0",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_text_4": {
                "id": "field_text_4",
                "type": "short_text",
                "label": "Text 1",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "1.1.1.0",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_text_5": {
                "id": "field_text_5",
                "type": "short_text",
                "label": "Text 1",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "4.0",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Groups form field",
        "type": "",
        "meta": {
            "title": "Groups form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "QUESTIONNAIRE DATA",
                    "help": "Note for the interviewer: It’s recommended to take at least one photo that is as representative as possible of the service provider",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Text 1",
                            "field_id": "field_text_1",
                            "weight": 0
                        }
                    }
                },
                {
                    "id": "1",
                    "title": "GENERAL INFORMATION",
                    "help": "",
                    "children": {
                        "1.0": {
                            "id": "1.0",
                            "title": "Text 1",
                            "field_id": "field_text_2",
                            "weight": 0
                        },
                        "1.1": {
                            "id": "1.1",
                            "title": "Location of community",
                            "help": "suggested to use coordinates and location of the community",
                            "children": {
                                "1.1.0": {
                                    "id": "1.1.0",
                                    "title": "Text 1",
                                    "field_id": "field_text_3",
                                    "weight": 0
                                },
                                "1.1.1": {
                                    "id": "1.1.1",
                                    "title": "Location of community",
                                    "help": "suggested to use coordinates and location of the community",
                                    "children": {
                                        "1.1.1.0": {
                                            "id": "1.1.1.0",
                                            "title": "Text 1",
                                            "field_id": "field_text_4",
                                            "weight": 0
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    "id": "4",
                    "title": "HYGIENE",
                    "help": "",
                    "children": {
                        "4.0": {
                            "id": "4.0",
                            "title": "Text 1",
                            "field_id": "field_text_5",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructGroupField = JSON.stringify({
        "description": "",
        "fields": {
            "field_text_1": {
                "id": "field_text_1",
                "type": "short_text",
                "label": "Text 1",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            },
            "field_text_2": {
                "id": "field_text_2",
                "type": "short_text",
                "label": "Text 2",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1.1",
                        "level": 1
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Groups form field",
        "type": "",
        "meta": {
            "title": "Groups form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "QUESTIONNAIRE DATA",
                    "help": "Note for the interviewer bla bla bla",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Text 1",
                            "field_id": "field_text_1",
                            "weight": 0,
                            "children": {
                                "0.1.1": {
                                    "id": "0.1.1",
                                    "title": "SUB Text 2",
                                    "field_id": "field_text_2",
                                    "weight": 0
                                }
                            }
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm field group & subgroups', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStruct);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Are groups visible?
        cy.get(".group-0").should("be.visible");
        cy.get(".group-0 .accordion__heading #accordion__heading-0").should("be.visible");
        cy.get(".group-0 .group-legend .catalog-id").should("not.exist");
        cy.get(".group-0 .group-legend .title").should("not.exist");
        cy.get(".group-0 .field-group-description").should("be.visible");

        cy.get(".group-1").should("be.visible");
        cy.get(".group-1").click();
        cy.get(".group-1 .accordion__heading #accordion__heading-1").should("be.visible");
        cy.get(".group-1 > legend .catalog-id").should("not.exist");
        cy.get(".group-1 > legend .title").should("not.exist");
        cy.get(".group-1 > .field-group-description").should("not.exist");

        cy.get(".group-1-1").should("be.visible");
        cy.get(".group-1-1 legend .catalog-id").should("be.visible");
        cy.get(".group-1-1 legend .title").should("be.visible");
        cy.get(".group-1-1 > .field-group-description").should("be.visible");

        cy.get(".group-1-1-1").should("be.visible");
        cy.get(".group-1-1-1 legend .catalog-id").should("be.visible");
        cy.get(".group-1-1-1 legend .title").should("be.visible");
        cy.get(".group-1-1-1 > .field-group-description").should("be.visible");

        cy.get(".group-4").should("be.visible");
        cy.get(".group-4").click();
        cy.get(".group-1 .accordion__heading #accordion__heading-1").should("be.visible");
        cy.get(".group-4 legend .catalog-id").should("not.exist");
        cy.get(".group-4 legend .title").should("not.exist");
        cy.get(".group-4 > .field-group-description").should("not.exist");

        cy.get(".group-0").click();

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify( {}, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm field group & subgroups with fields without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructWithFields);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Are groups visible?
        cy.get(".group-0").should("be.visible");
        cy.get(".group-0 .accordion__heading #accordion__heading-0").should("be.visible");
        cy.get(".group-0 .group-legend .catalog-id").should("not.exist");
        cy.get(".group-0 .group-legend .title").should("not.exist");
        cy.get(".group-0 .field-group-description").should("be.visible");
        cy.get(".group-0 #field_text_1").should("be.visible");

        cy.get(".group-1").should("be.visible");
        cy.get(".group-1").click();
        cy.get(".group-1 .accordion__heading #accordion__heading-1").should("be.visible");
        cy.get(".group-1 > legend .catalog-id").should("not.exist");
        cy.get(".group-1 > legend .title").should("not.exist");
        cy.get(".group-1 > .field-group-description").should("not.exist");
        cy.get(".group-1  #field_text_2").should("be.visible");

        cy.get(".group-1-1").should("be.visible");
        cy.get(".group-1-1 legend .catalog-id").should("be.visible");
        cy.get(".group-1-1 legend .title").should("be.visible");
        cy.get(".group-1-1 > .field-group-description").should("be.visible");
        cy.get(".group-1-1  #field_text_3").should("be.visible");

        cy.get(".group-1-1-1").should("be.visible");
        cy.get(".group-1-1-1 legend .catalog-id").should("be.visible");
        cy.get(".group-1-1-1 legend .title").should("be.visible");
        cy.get(".group-1-1-1 > .field-group-description").should("be.visible");
        cy.get(".group-1-1-1  #field_text_4").should("be.visible");

        cy.get(".group-4").should("be.visible");
        cy.get(".group-4").click();
        cy.get(".group-1 .accordion__heading #accordion__heading-1").should("be.visible");
        cy.get(".group-4 legend .catalog-id").should("not.exist");
        cy.get(".group-4 legend .title").should("not.exist");
        cy.get(".group-4 > .field-group-description").should("not.exist");
        cy.get(".group-4  #field_text_5").should("be.visible");

        cy.get(".group-0").click();

        cy.get("#txt_data").invoke('val', "{}");
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": ""
                },
                "field_text_2": {
                    "value": ""
                },
                "field_text_5": {
                    "value": ""
                },
                "field_text_3": {
                    "value": ""
                },
                "field_text_4": {
                    "value": ""
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm field group & subgroups with fields with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructWithFields);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Generated change event.
        cy.get("#txt_data").invoke('val', JSON.stringify({
            "field_text_1": {
                "value": "1"
            },
            "field_text_2": {
                "value": "2"
            },
            "field_text_5": {
                "value": "5"
            },
            "field_text_3": {
                "value": "3"
            },
            "field_text_4": {
                "value": "4"
            }
        }, undefined, 4));
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Are groups visible?
        cy.get(".group-0").should("be.visible");
        cy.get(".group-0 .accordion__heading #accordion__heading-0").should("be.visible");
        cy.get(".group-0 .group-legend .catalog-id").should("not.exist");
        cy.get(".group-0 .group-legend .title").should("not.exist");
        cy.get(".group-0 .field-group-description").should("be.visible");
        cy.get(".group-0 #field_text_1").should("be.visible");

        cy.get(".group-1").should("be.visible");
        cy.get(".group-1").click();
        cy.get(".group-1 .accordion__heading #accordion__heading-1").should("be.visible");
        cy.get(".group-1 > legend .catalog-id").should("not.exist");
        cy.get(".group-1 > legend .title").should("not.exist");
        cy.get(".group-1 > .field-group-description").should("not.exist");
        cy.get(".group-1  #field_text_2").should("be.visible");

        cy.get(".group-1-1").should("be.visible");
        cy.get(".group-1-1 legend .catalog-id").should("be.visible");
        cy.get(".group-1-1 legend .title").should("be.visible");
        cy.get(".group-1-1 > .field-group-description").should("be.visible");
        cy.get(".group-1-1  #field_text_3").should("be.visible");

        cy.get(".group-1-1-1").should("be.visible");
        cy.get(".group-1-1-1 legend .catalog-id").should("be.visible");
        cy.get(".group-1-1-1 legend .title").should("be.visible");
        cy.get(".group-1-1-1 > .field-group-description").should("be.visible");
        cy.get(".group-1-1-1  #field_text_4").should("be.visible");

        cy.get(".group-4").should("be.visible");
        cy.get(".group-4").click();
        cy.get(".group-1 .accordion__heading #accordion__heading-1").should("be.visible");
        cy.get(".group-4 legend .catalog-id").should("not.exist");
        cy.get(".group-4 legend .title").should("not.exist");
        cy.get(".group-4 > .field-group-description").should("not.exist");
        cy.get(".group-4  #field_text_5").should("be.visible");

        cy.get(".group-0").click();

        // Validate initial data.
        cy.get("#field_text_1").should("have.value", 1);
        cy.get("#field_text_2").should("have.value", 2);
        cy.get("#field_text_3").should("have.value", 3);
        cy.get("#field_text_4").should("have.value", 4);
        cy.get("#field_text_5").should("have.value", 5);

        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_result")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": "1"
                },
                "field_text_2": {
                    "value": "2"
                },
                "field_text_5": {
                    "value": "5"
                },
                "field_text_3": {
                    "value": "3"
                },
                "field_text_4": {
                    "value": "4"
                }
            }, undefined, 4));

        // Update values by hand.
        cy.get(".group-0").click();
        cy.get("#field_text_1").type("{selectAll}5");
        cy.get(".group-1").click();
        cy.get("#field_text_2").type("{selectAll}4");
        cy.get("#field_text_3").type("{selectAll}31");
        cy.get("#field_text_4").type("{selectAll}2");
        cy.get(".group-4").click();
        cy.get("#field_text_5").type("{selectAll}1");

        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_result")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": "5"
                },
                "field_text_2": {
                    "value": "4"
                },
                "field_text_5": {
                    "value": "1"
                },
                "field_text_3": {
                    "value": "31"
                },
                "field_text_4": {
                    "value": "2"
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm field that is a group without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructGroupField);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Are groups visible?
        cy.get(".group-0").should("be.visible");
        cy.get(".group-0 .accordion__heading #accordion__heading-0").should("be.visible");
        cy.get(".group-0 .group-legend .catalog-id").should("not.exist");
        cy.get(".group-0 .group-legend .title").should("not.exist");
        cy.get(".group-0 .field-group-description").should("be.visible");
        cy.get(".group-0 #field_text_1").should("be.visible");

        cy.get(".group-0-1").should("be.visible");
        cy.get(".group-0-1 .group-legend").should("not.exist");
        cy.get(".group-0-1 > .field-group-description").should("not.be.visible");
        cy.get(".group-0-1  #field_text_2").should("be.visible");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": ""
                },
                "field_text_2": {
                    "value": ""
                }
            }, undefined, 4));

        // Update values by hand.
        cy.get("#field_text_1").type("{selectAll}5");
        cy.get("#field_text_2").type("{selectAll}4");

        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_result")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": "5"
                },
                "field_text_2": {
                    "value": "4"
                },
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm field that is a group with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructGroupField);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        cy.get("#txt_data").invoke('val', JSON.stringify({
            "field_text_1": {
                "value": "1"
            },
            "field_text_2": {
                "value": "2"
            },
        }, undefined, 4));
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate initial data.
        cy.get("#field_text_1").should("have.value", "1");
        cy.get("#field_text_2").should("have.value", "2");

        // Are groups visible?
        cy.get(".group-0").should("be.visible");
        cy.get(".group-0 .accordion__heading #accordion__heading-0").should("be.visible");
        cy.get(".group-0 .group-legend .catalog-id").should("not.exist");
        cy.get(".group-0 .group-legend .title").should("not.exist");
        cy.get(".group-0 .field-group-description").should("be.visible");
        cy.get(".group-0 #field_text_1").should("be.visible");

        cy.get(".group-0-1").should("be.visible");
        cy.get(".group-0-1 .group-legend").should("not.exist");
        cy.get(".group-0-1 > .field-group-description").should("not.be.visible");
        cy.get(".group-0-1  #field_text_2").should("be.visible");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": "1"
                },
                "field_text_2": {
                    "value": "2"
                }
            }, undefined, 4));

        // Update values by hand.
        cy.get("#field_text_1").type("{selectAll}5");
        cy.get("#field_text_2").type("{selectAll}4");

        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_result")
            .should("have.text", JSON.stringify( {
                "field_text_1": {
                    "value": "5"
                },
                "field_text_2": {
                    "value": "4"
                },
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })
})
