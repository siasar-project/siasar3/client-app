/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm file_reference field.
 */
context('Test DynForm file_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_file": {
                "id": "field_file",
                "type": "file_reference",
                "label": "Field File",
                "description": "Select a file",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "allow_extension": ["png", "pdf"]
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "File form field",
        "type": "",
        "meta": {
            "title": "File form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field file",
                            "field_id": "field_file",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_file": {
                "id": "field_file",
                "type": "file_reference",
                "label": "Field File",
                "description": "Select a file",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "File form field",
        "type": "",
        "meta": {
            "title": "File form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field file",
                            "field_id": "field_file",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_file": {
            "value": "01G0Y8VGRPJ5TTAZ4F27WKC8QC",
            "class": "App\\Entity\\File",
            "filename": "hn.png"
        }
    } , undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_file": [
            {
                "value": "01G0Y8VGRPJ5TTAZ4F27WKC8QC",
                "class": "App\\Entity\\File",
                "filename": "hn.png"
            },
            {
                "value": "01G0Y8VGRRD9ANV0DMYXD7PGR6",
                "class": "App\\Entity\\File",
                "filename": "hn.png"
            },
            {
                "value": "01G0Y8VGPR8Y2W101E8V1S7SJ9",
                "class": "App\\Entity\\File",
                "filename": "hn.png"
            }
        ]
    }, undefined, 4);

    let countryCode = 'hn';
    let fileId = '12345678901234567890123456';
    let fileId2 = '12345678901234567890123457';
    let fileId3 = '12345678901234567890123458';
    let fileName = 'especias.png';
    let fileName2 = 'fondo.jpg';
    let fileName3 = 'chocolate.jpeg';

    let fDataSingleUpload = JSON.stringify({
        "field_file": {
            "value": fileId,
            "class": "App\\Entity\\File",
            "filename": fileName
        }
    }, undefined, 4);
    let fDataMultiUpload = JSON.stringify({
        "field_file": [
            {
                "value": fileId,
                "class": "App\\Entity\\File",
                "filename": fileName
            },
            {
                "value": fileId2,
                "class": "App\\Entity\\File",
                "filename": fileName2
            },
            {
                "value": fileId3,
                "class": "App\\Entity\\File",
                "filename": fileName3
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        window.localStorage.setItem("session", '{"country": {"code": "'+countryCode+'"}}');

        cy.intercept('GET', '/api/v1/files/'+fileId+'/download', {fixture: 'img/'+fileName})
            .as("loadApiV1Files_"+fileId+"_download");
        cy.intercept('POST', '/api/v1/files', {id: fileId, fileName: fileName})
            .as("loadApiV1Files");
        cy.intercept('DELETE', '/api/v1/files/*', '')
            .as("loadApiV1FilesAll");
    })

    it('DynForm Field - simple file_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate filename text not exist.
        cy.get("#field_file-0").should("not.exist");
        // Validate button add existence.
        cy.get("#field_file-add").should("be.visible");

        // Add file value.
        cy.get("#field_file-add").selectFile('cypress/fixtures/img/'+fileName);
        cy.wait("@loadApiV1Files");
        // Check filename value.
        cy.get("#field_file-0").should("have.text", fileName);
        // Validate add button not exist.
        cy.get("#field_file-add").should("not.exist");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text",fDataSingleUpload);
        // Check filename value.
        cy.get("#field_file-0").should("have.text", fileName);
        // Validate add button not exist.
        cy.get("#field_file-add").should("not.exist");
        
        // Remove file values.
        cy.get("#field_file0-remove").click();
        // Validate filename text not exist.
        cy.get("#field_file-"+fileId+"-0").should("not.exist");
        // Validate add button existence.
        cy.get("#field_file-add").should("be.visible");

        // // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple file_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Check filename value.
        cy.get("#field_file-0").should("have.text", 'hn.png');
        // Validate add button not exist.
        cy.get("#field_file-add").should("not.exist");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data").should("have.text", fDataSingle);

        // Remove file values.
        cy.get("#field_file0-remove").click();
        // Add file value.
        cy.get("#field_file-add").selectFile('cypress/fixtures/img/'+fileName);

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text",fDataSingleUpload);

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued file_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate filename text not exist.
        cy.get("#field_file-0").should("not.exist");
        // Validate button add existence.
        cy.get("#field_file-add").should("be.visible");

        // Add values.
        // Workaround, overwrite cy.intercept('POST', '/api/v1/files ...) with given file id and name. 
        cy.get("#field_file-add").selectFile('cypress/fixtures/img/'+fileName);
        cy.intercept('POST', '/api/v1/files', {id: fileId, fileName: fileName}).as("id_"+fileId);
        cy.wait("@id_"+fileId);
        // Workaround, overwrite cy.intercept('POST', '/api/v1/files ...) with given file id and name. 
        cy.get("#field_file-add").selectFile('cypress/fixtures/img/'+fileName2);
        cy.intercept('POST', '/api/v1/files', {id: fileId2, fileName: fileName2}).as("id_"+fileId2);
        cy.wait("@id_"+fileId2);
        // Workaround, overwrite cy.intercept('POST', '/api/v1/files ...) with given file id and name. 
        cy.get("#field_file-add").selectFile('cypress/fixtures/img/'+fileName3);
        cy.intercept('POST', '/api/v1/files', {id: fileId3, fileName: fileName3}).as("id_"+fileId3);
        cy.wait("@id_"+fileId3);

        // Check filename value.
        cy.get("#field_file-0").should("not.have.text", '');
        cy.get("#field_file-1").should("not.have.text", '');
        cy.get("#field_file-2").should("not.have.text", '');
        // Validate add button existence.
        cy.get("#field_file-add").should("be.visible");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text", fDataMultiUpload);
        // Check filename value.
        cy.get("#field_file-0").should("not.have.text", '');
        cy.get("#field_file-1").should("not.have.text", '');
        cy.get("#field_file-2").should("not.have.text", '');
        // Validate add button existence.
        cy.get("#field_file-add").should("be.visible");

        // Remove file values and Validate filename text not exist.
        cy.get("#field_file2-remove").click();
        cy.get("#field_file-2").should("not.exist");
        cy.get("#field_file1-remove").click();
        cy.get("#field_file-1").should("not.exist");
        cy.get("#field_file0-remove").click();
        cy.get("#field_file-0").should("not.exist");
        
        // Validate add button existence.
        cy.get("#field_file-add").should("be.visible");
        
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued file_reference with data', () => {
          // Visit test page.
          cy.visit("dynform_field_test");
          // Set configuration values in textarea.
          cy.get("#txt_structure").invoke('val', fStructMulti);
          // Generated change event.
          cy.get("#txt_structure").type('{moveToEnd}{enter}');
  
          // Set data values in textarea.
          cy.get("#txt_data").invoke('val', fDataMulti);
          // Generated change event.
          cy.get("#txt_data").type('{moveToEnd}{enter}');
  
          // Check filename value.
          cy.get("#field_file-0").should("have.text", 'hn.png');
          cy.get("#field_file-1").should("have.text", 'hn.png');
          cy.get("#field_file-2").should("have.text", 'hn.png');
          // Validate add button not exist.
          cy.get("#field_file-add").should("be.visible");
  
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text", fDataMulti);
        // Check filename value.
        cy.get("#field_file-0").should("have.text", 'hn.png');
        cy.get("#field_file-1").should("have.text", 'hn.png');
        cy.get("#field_file-2").should("have.text", 'hn.png');
        // Validate add button existence.
        cy.get("#field_file-add").should("be.visible");

        // Remove file values and Validate filename text not exist.
        cy.get("#field_file2-remove").click();
        cy.get("#field_file-2").should("not.exist");
        cy.get("#field_file1-remove").click();
        cy.get("#field_file-1").should("not.exist");
        cy.get("#field_file0-remove").click();
        cy.get("#field_file-0").should("not.exist");
        
        // Validate add button existence.
        cy.get("#field_file-add").should("be.visible");
        
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
