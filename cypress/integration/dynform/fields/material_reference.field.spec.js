/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm material_reference field.
 */
context('Test DynForm material_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_material": {
                "id": "field_material",
                "type": "material_reference",
                "label": "Field Material",
                "description": "Select a material",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Material form field",
        "type": "",
        "meta": {
            "title": "Material form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field material",
                            "field_id": "field_material",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_material": {
                "id": "field_material",
                "type": "material_reference",
                "label": "Field Material",
                "description": "Select a material",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Material form field",
        "type": "",
        "meta": {
            "title": "Material form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field material",
                            "field_id": "field_material",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_material": {
            "value": "01G0Y8TVFEBJMNR53YG4AQXY9P",
            "class": "App\\Entity\\Material"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_material": [
            {
                "value": "01G0Y8TVFEBJMNR53YG4AQXY9P",
                "class": "App\\Entity\\Material"
            },
            {
                "value": "01G0Y8TVFEBJMNR53YG4AQXY9Y",
                "class": "App\\Entity\\Material"
            },
            {
                "value": "01G0Y8TVFEBJMNR53YG4AQXYA4",
                "class": "App\\Entity\\Material"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get materials from API.
        cy.intercept('GET', '/api/v1/materials?page=2', []);
        cy.intercept('GET', '/api/v1/materials?page=1', {fixture: 'materials.json'});
    })

    it('DynForm Field - simple material_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_material_value").should("be.visible");
        // Change value.
        cy.get("#field_material_value").select('01G0Y8TVFEBJMNR53YG4AQXYA0');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_material": {
                    "value": "01G0Y8TVFEBJMNR53YG4AQXYA0",
                    "class": "App\\Entity\\Material"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_material_value").select('01G0Y8TVFEBJMNR53YG4AQXYA6');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_material":
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXYA6",
                        "class": "App\\Entity\\Material"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple material_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_material_value").should("be.visible");
        cy.get("#field_material_value").should("have.value", "01G0Y8TVFEBJMNR53YG4AQXY9P");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_material": {
                    "value": "01G0Y8TVFEBJMNR53YG4AQXY9P",
                    "class": "App\\Entity\\Material"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_material_value").select('01G0Y8TVFEBJMNR53YG4AQXYA6');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_material":
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXYA6",
                        "class": "App\\Entity\\Material"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued material_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_material__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_material__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_material__2_value").should("be.visible");

        // Change value.
        cy.get("#field_material__0_value").select('01G0Y8TVFEBJMNR53YG4AQXYA0');
        cy.get("#field_material__1_value").select('01G0Y8TVFEBJMNR53YG4AQXYA6');
        cy.get("#field_material__2_value").select('01G0Y8TVFEBJMNR53YG4AQXY9W');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_material": [
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXYA0",
                        "class": "App\\Entity\\Material"
                    },
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXYA6",
                        "class": "App\\Entity\\Material"
                    },
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXY9W",
                        "class": "App\\Entity\\Material"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_material2-remove").click();
        cy.get('#field_material__2_value').should('not.exist')
        cy.get("#field_material1-remove").click();
        cy.get('#field_material__1_value').should('not.exist')
        cy.get("#field_material0-remove").click();
        cy.get('#field_material__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_material": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued material_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_material__0_value").should("be.visible");
        cy.get("#field_material__0_value").should("have.value", "01G0Y8TVFEBJMNR53YG4AQXY9P");
        cy.get("#field_material__1_value").should("be.visible");
        cy.get("#field_material__1_value").should("have.value", "01G0Y8TVFEBJMNR53YG4AQXY9Y");
        cy.get("#field_material__2_value").should("be.visible");
        cy.get("#field_material__2_value").should("have.value", "01G0Y8TVFEBJMNR53YG4AQXYA4");

        // Change value.
        cy.get("#field_material__0_value").select("01G0Y8TVFEBJMNR53YG4AQXYA0");
        cy.get("#field_material__1_value").select("01G0Y8TVFEBJMNR53YG4AQXYA6");
        cy.get("#field_material__2_value").select("01G0Y8TVFEBJMNR53YG4AQXY9W");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_material": [
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXYA0",
                        "class": "App\\Entity\\Material"
                    },
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXYA6",
                        "class": "App\\Entity\\Material"
                    },
                    {
                        "value": "01G0Y8TVFEBJMNR53YG4AQXY9W",
                        "class": "App\\Entity\\Material"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_material2-remove").click();
        cy.get('#field_material__2_value').should('not.exist')
        cy.get("#field_material1-remove").click();
        cy.get('#field_material__1_value').should('not.exist')
        cy.get("#field_material0-remove").click();
        cy.get('#field_material__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_material": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
