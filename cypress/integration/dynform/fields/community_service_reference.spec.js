/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm community_service_reference field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_community_service_reference": {
                "id": "field_community_service_reference",
                "type": "community_service_reference",
                "label": "Field community_service_reference",
                "description": "Field community_service_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "community_service_reference form field",
        "type": "",
        "meta": {
            "title": "community_service_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field community_service_reference",
                            "field_id": "field_community_service_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_community_service_reference": {
                "id": "field_community_service_reference",
                "type": "community_service_reference",
                "label": "Field community_service_reference",
                "description": "Field community_service_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "community_service_reference form field",
        "type": "",
        "meta": {
            "title": "community_service_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field community_service_reference",
                            "field_id": "field_community_service_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_community_service_reference": {
            "value": "01G4D861V9GQXVC0Q29R7G44TY",
            "class": "App\\Entity\\CommunityService"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_community_service_reference": [
            {
                "value": "01G4D861V9GQXVC0Q29R7G44TY",
                "class": "App\\Entity\\CommunityService"
            },
            {
                "value": "01G4D86KAEMV4DJGAB8N3Z73YN",
                "class": "App\\Entity\\CommunityService"
            },
            {
                "value": "01G4D86R624ZDG59PV7VXJG3QN",
                "class": "App\\Entity\\CommunityService"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/community_services?page=2', []);
        cy.intercept('GET', '/api/v1/community_services?page=1', {fixture: 'community_services.json'});
    })

    it('DynForm Field - simple community_service_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-community_service_reference").should("be.visible");
        // Change value.
        cy.get(".form-field-community_service_reference [type='radio']").check('01G4D8198GEGMX7DAMHEYRV2YJ');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_community_service_reference": {
                        "value": "01G4D8198GEGMX7DAMHEYRV2YJ",
                        "class": "App\\Entity\\CommunityService"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple community_service_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-community_service_reference").should("be.visible");
        cy.get(".form-field-community_service_reference :checked").should("be.checked").and('have.value', '01G4D861V9GQXVC0Q29R7G44TY');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_community_service_reference": {
                        "value": "01G4D861V9GQXVC0Q29R7G44TY",
                        "class": "App\\Entity\\CommunityService"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-community_service_reference [type='radio']").check('01G4D8198GEGMX7DAMHEYRV2YJ');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_community_service_reference": {
                        "value": "01G4D8198GEGMX7DAMHEYRV2YJ",
                        "class": "App\\Entity\\CommunityService"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued community_service_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.

        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D8198GEGMX7DAMHEYRV2YJ]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D861V9GQXVC0Q29R7G44TY]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D8675DSS6VCJWFGVXCEM8P]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86CJEXAX2FN8VXTWHB60N]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86KAEMV4DJGAB8N3Z73YN]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86R624ZDG59PV7VXJG3QN]').should("be.visible");
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4D8198GEGMX7DAMHEYRV2YJ');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4D8675DSS6VCJWFGVXCEM8P');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4D86CJEXAX2FN8VXTWHB60N');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_community_service_reference": [
                        {
                            "value": '01G4D8198GEGMX7DAMHEYRV2YJ',
                            "class": "App\\Entity\\CommunityService"
                        },
                        {
                            "value": "01G4D8675DSS6VCJWFGVXCEM8P",
                            "class": "App\\Entity\\CommunityService"
                        },
                        {
                            "value": "01G4D86CJEXAX2FN8VXTWHB60N",
                            "class": "App\\Entity\\CommunityService"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued community_service_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D8198GEGMX7DAMHEYRV2YJ]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D8198GEGMX7DAMHEYRV2YJ]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D861V9GQXVC0Q29R7G44TY]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D861V9GQXVC0Q29R7G44TY]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D8675DSS6VCJWFGVXCEM8P]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D8675DSS6VCJWFGVXCEM8P]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86CJEXAX2FN8VXTWHB60N]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86CJEXAX2FN8VXTWHB60N]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86KAEMV4DJGAB8N3Z73YN]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86KAEMV4DJGAB8N3Z73YN]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86R624ZDG59PV7VXJG3QN]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D86R624ZDG59PV7VXJG3QN]').should('be.checked');
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4D8198GEGMX7DAMHEYRV2YJ');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4D861V9GQXVC0Q29R7G44TY');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4D8675DSS6VCJWFGVXCEM8P');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4D86CJEXAX2FN8VXTWHB60N');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4D86KAEMV4DJGAB8N3Z73YN');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4D86R624ZDG59PV7VXJG3QN');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_community_service_reference": [
                        {
                            "value": '01G4D8198GEGMX7DAMHEYRV2YJ',
                            "class": "App\\Entity\\CommunityService"
                        },
                        {
                            "value": "01G4D8675DSS6VCJWFGVXCEM8P",
                            "class": "App\\Entity\\CommunityService"
                        },
                        {
                            "value": "01G4D86CJEXAX2FN8VXTWHB60N",
                            "class": "App\\Entity\\CommunityService"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
