/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm type_intervention_reference field.
 */
context('Test DynForm type_intervention_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_type_intervention": {
                "id": "field_type_intervention",
                "type": "type_intervention_reference",
                "label": "Field Type Intervention",
                "description": "Select a Type Intervention",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Type Intervention form field",
        "type": "",
        "meta": {
            "title": "Type Intervention form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field type",
                            "field_id": "field_type_intervention",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_type_intervention": {
                "id": "field_type_intervention",
                "type": "type_intervention_reference",
                "label": "Field Programs",
                "description": "Select a type",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Type Interventions form field",
        "type": "",
        "meta": {
            "title": "Type Interventions form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field type",
                            "field_id": "field_type_intervention",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_type_intervention": {
            "value": "01G0Y8TVGFVH1HQDBD1VNSKSP1",
            "class": "App\\Entity\\TypeIntervention"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_type_intervention": [
            {
                "value": "01G0Y8TVGFVH1HQDBD1VNSKSP1",
                "class": "App\\Entity\\TypeIntervention"
            },
            {
                "value": "01G0Y8TVGFVH1HQDBD1VNSKSNZ",
                "class": "App\\Entity\\TypeIntervention"
            },
            {
                "value": "01G0Y8TVGFVH1HQDBD1VNSKSP3",
                "class": "App\\Entity\\TypeIntervention"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get types_interventions from API.
        cy.intercept('GET', '/api/v1/type_interventions?page=2', []);
        cy.intercept('GET', '/api/v1/type_interventions?page=1', [{ "id": "01G0Y8TVGFVH1HQDBD1VNSKSNX", "country": "/api/v1/countries/hn", "type": "Type Intervention 1", "description": "Type Intervention 1 description." }, { "id": "01G0Y8TVGFVH1HQDBD1VNSKSNZ", "country": "/api/v1/countries/hn", "type": "Type Intervention 2", "description": "Type Intervention 2 description." }, { "id": "01G0Y8TVGFVH1HQDBD1VNSKSP1", "country": "/api/v1/countries/hn", "type": "Type Intervention 3", "description": "Type Intervention 3 description." }, { "id": "01G0Y8TVGFVH1HQDBD1VNSKSP3", "country": "/api/v1/countries/hn", "type": "Type Intervention 4", "description": "Type Intervention 4 description." }, { "id": "01G0Y8TVGFVH1HQDBD1VNSKSP5", "country": "/api/v1/countries/hn", "type": "Type Intervention 5", "description": "Type Intervention 5 description." }]);
    })
    
    it('DynForm Field - simple type_intervention_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_type_intervention_value").should("be.visible");
        // Change value.
        cy.get("#field_type_intervention_value").select('01G0Y8TVGFVH1HQDBD1VNSKSP5');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_type_intervention": {
                    "value": "01G0Y8TVGFVH1HQDBD1VNSKSP5",
                    "class": "App\\Entity\\TypeIntervention"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_type_intervention_value").select('01G0Y8TVGFVH1HQDBD1VNSKSNX');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_type_intervention":
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSNX",
                        "class": "App\\Entity\\TypeIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple type_intervention_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_type_intervention_value").should("be.visible");
        cy.get("#field_type_intervention_value").should("have.value", "01G0Y8TVGFVH1HQDBD1VNSKSP1");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_type_intervention": {
                    "value": "01G0Y8TVGFVH1HQDBD1VNSKSP1",
                    "class": "App\\Entity\\TypeIntervention"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_type_intervention_value").select('01G0Y8TVGFVH1HQDBD1VNSKSNX');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_type_intervention":
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSNX",
                        "class": "App\\Entity\\TypeIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued type_intervention_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_type_intervention__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_type_intervention__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_type_intervention__2_value").should("be.visible");

        // Change value.
        cy.get("#field_type_intervention__0_value").select('01G0Y8TVGFVH1HQDBD1VNSKSP5');
        cy.get("#field_type_intervention__1_value").select('01G0Y8TVGFVH1HQDBD1VNSKSNX');
        cy.get("#field_type_intervention__2_value").select('01G0Y8TVGFVH1HQDBD1VNSKSNZ');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_type_intervention": [
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSP5",
                        "class": "App\\Entity\\TypeIntervention"
                    },
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSNX",
                        "class": "App\\Entity\\TypeIntervention"
                    },
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSNZ",
                        "class": "App\\Entity\\TypeIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_type_intervention2-remove").click();
        cy.get('#field_type_intervention__2_value').should('not.exist')
        cy.get("#field_type_intervention1-remove").click();
        cy.get('#field_type_intervention__1_value').should('not.exist')
        cy.get("#field_type_intervention0-remove").click();
        cy.get('#field_type_intervention__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_type_intervention": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued type_intervention_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_type_intervention__0_value").should("be.visible");
        cy.get("#field_type_intervention__0_value").should("have.value", "01G0Y8TVGFVH1HQDBD1VNSKSP1");
        cy.get("#field_type_intervention__1_value").should("be.visible");
        cy.get("#field_type_intervention__1_value").should("have.value", "01G0Y8TVGFVH1HQDBD1VNSKSNZ");
        cy.get("#field_type_intervention__2_value").should("be.visible");
        cy.get("#field_type_intervention__2_value").should("have.value", "01G0Y8TVGFVH1HQDBD1VNSKSP3");

        // Change value.
        cy.get("#field_type_intervention__0_value").select('01G0Y8TVGFVH1HQDBD1VNSKSP5');
        cy.get("#field_type_intervention__1_value").select('01G0Y8TVGFVH1HQDBD1VNSKSNX');
        cy.get("#field_type_intervention__2_value").select('01G0Y8TVGFVH1HQDBD1VNSKSNZ');

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_type_intervention": [
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSP5",
                        "class": "App\\Entity\\TypeIntervention"
                    },
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSNX",
                        "class": "App\\Entity\\TypeIntervention"
                    },
                    {
                        "value": "01G0Y8TVGFVH1HQDBD1VNSKSNZ",
                        "class": "App\\Entity\\TypeIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_type_intervention2-remove").click();
        cy.get('#field_type_intervention__2_value').should('not.exist')
        cy.get("#field_type_intervention1-remove").click();
        cy.get('#field_type_intervention__1_value').should('not.exist')
        cy.get("#field_type_intervention0-remove").click();
        cy.get('#field_type_intervention__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_type_intervention": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
