/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm radio_select field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_radio_select": {
                "id": "field_radio_select",
                "type": "radio_select",
                "label": "Field radio_select",
                "description": "Field radio_select",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": "One",
                        "2": "Two",
                        "3": "Three",
                        "4": "Four"
                    }
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "radio_select form field",
        "type": "",
        "meta": {
            "title": "radio_select form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field radio_select",
                            "field_id": "field_radio_select",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_radio_select": {
                "id": "field_radio_select",
                "type": "radio_select",
                "label": "Field radio_select",
                "description": "Field radio_select",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "options": {
                        "1": "One",
                        "2": "Two",
                        "3": "Three",
                        "4": "Four"
                    }
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "radio_select form field",
        "type": "",
        "meta": {
            "title": "radio_select form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field radio_select",
                            "field_id": "field_radio_select",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_radio_select": {
            "value": "3"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_radio_select": [
            {
                "value": "2"
            },
            {
                "value": "3"
            },
            {
                "value": "4"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple radio_select without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-radio_select").should("be.visible");
        // Change value.
        cy.get(".form-field-radio_select [type='radio']").check('4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_radio_select": {
                        "value": "4"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple radio_select with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-radio_select").should("be.visible");
        cy.get(".form-field-radio_select :checked").should("be.checked").and('have.value', '3');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_radio_select": {
                        "value": "3"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-radio_select [type='radio']").check('4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_radio_select": {
                        "value": "4"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued radio_select without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=1]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=2]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=3]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=4]').should("be.visible");

        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('2');
        cy.get(".form-multivalued-list [type='checkbox']").check('3');
        cy.get(".form-multivalued-list [type='checkbox']").check('4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_radio_select": [
                        {
                            "value": "2"
                        },
                        {
                            "value": "3"
                        },
                        {
                            "value": "4"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued radio_select with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=1]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=1]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=2]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=2]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=3]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=3]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=4]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=4]').should('be.checked');
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('1');
        cy.get(".form-multivalued-list [type='checkbox']").check('2');
        cy.get(".form-multivalued-list [type='checkbox']").check('3');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('4');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_radio_select": [
                        {
                            "value": "1"
                        },
                        {
                            "value": "2"
                        },
                        {
                            "value": "3"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
