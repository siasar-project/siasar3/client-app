/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm date field.
 */
context('Test DynForm date field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_date": {
                "id": "field_date",
                "type": "date",
                "label": "Field date",
                "description": "Field date",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "date form field",
        "type": "",
        "meta": {
            "title": "date form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field date",
                            "field_id": "field_date",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_date": {
                "id": "field_date",
                "type": "date",
                "label": "Field date",
                "description": "Field date",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "date form field",
        "type": "",
        "meta": {
            "title": "date form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field date",
                            "field_id": "field_date",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_date": {
                "value": "2022-01-20",
                "timezone": "UTC"
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_date": [
            {
                "value": "",
                "timezone": "UTC"
            },
            {
                "value": "2022-01-20",
                "timezone": "UTC"
            },
            {
                "value": "",
                "timezone": "UTC"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple date without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_date").should("be.visible");
        // Change value.
        cy.get("#field_date").type('2022-01-20');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_date": {
                        "value": "2022-01-20",
                        "timezone": "UTC"
                    }
                }, undefined, 4));
        // Set empty value.
        cy.get("#field_date").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_date": {
                        "value": "",
                        "timezone": "UTC"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple date with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_date").should("be.visible");
        cy.get("#field_date").should('have.value', '2022-01-20');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_date": {
                        "value": "2022-01-20",
                        "timezone": "UTC"
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_date").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_date": {
                    "value": "",
                    "timezone": "UTC"
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued date without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_date__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_date__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_date__2").should("be.visible");

        // Change value.
        cy.get("#field_date__0").type('2022-01-20');
        cy.get("#field_date__1").type('2022-01-21');
        cy.get("#field_date__2").type('2022-01-22');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_date": [
                        {
                            "value": "2022-01-20",
                            "timezone": "UTC"
                        },
                        {
                            "value": "2022-01-21",
                            "timezone": "UTC"
                        },
                        {
                            "value": "2022-01-22",
                            "timezone": "UTC"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_date2-remove").click();
        cy.get('#field_date__2').should('not.exist')
        cy.get("#field_date1-remove").click();
        cy.get('#field_date__1').should('not.exist')
        cy.get("#field_date0-remove").click();
        cy.get('#field_date__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_date": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued date with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_date__0").should("be.visible");
        cy.get("#field_date__0").should('have.value', '');
        cy.get("#field_date__1").should("be.visible");
        cy.get("#field_date__1").should('have.value', '2022-01-20');
        cy.get("#field_date__2").should("be.visible");
        cy.get("#field_date__2").should('have.value', '');

        // Change value.
        cy.get("#field_date__0").type('2022-01-20');
        cy.get("#field_date__1").type('{selectAll}{del}');
        cy.get("#field_date__2").type('2022-01-21');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_date": [
                    {
                        "value": "2022-01-20",
                        "timezone": "UTC"
                    },
                    {
                        "value": "",
                        "timezone": "UTC"
                    },
                    {
                        "value": "2022-01-21",
                        "timezone": "UTC"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_date2-remove").click();
        cy.get('#field_date__2').should('not.exist')
        cy.get("#field_date1-remove").click();
        cy.get('#field_date__1').should('not.exist')
        cy.get("#field_date0-remove").click();
        cy.get('#field_date__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_date": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
