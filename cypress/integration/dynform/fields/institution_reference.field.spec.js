/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm institution_reference field.
 */
context('Test DynForm institution_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_institution": {
                "id": "field_institution",
                "type": "institution_reference",
                "label": "Field Institution",
                "description": "Select a Institution",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Institution form field",
        "type": "",
        "meta": {
            "title": "Institution form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field institution",
                            "field_id": "field_institution",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_institution": {
                "id": "field_institution",
                "type": "institution_reference",
                "label": "Field Institutions",
                "description": "Select a institution",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Institutions form field",
        "type": "",
        "meta": {
            "title": "Institutions form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field institution",
                            "field_id": "field_institution",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_institution": {
            "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3G",
            "class": "App\\Entity\\InstitutionIntervention"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_institution": [
            {
                "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3G",
                "class": "App\\Entity\\InstitutionIntervention"
            },
            {
                "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3M",
                "class": "App\\Entity\\InstitutionIntervention"
            },
            {
                "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3J",
                "class": "App\\Entity\\InstitutionIntervention"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get institutions_interventions from API.
        cy.intercept('GET', '/api/v1/institution_interventions?page=2', []);
        cy.intercept('GET', '/api/v1/institution_interventions?page=1', {fixture: 'institution_interventions.json'});
    })
    
    it('DynForm Field - simple institution_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_institution_value").should("be.visible");
        // Change value.
        cy.get("#field_institution_value").select('01G0Y8TVFC0PT0F5AQFRC5RZ3E');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_institution": {
                    "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3E",
                    "class": "App\\Entity\\InstitutionIntervention"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_institution_value").select('01G0Y8TVFC0PT0F5AQFRC5RZ3J');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_institution":
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3J",
                        "class": "App\\Entity\\InstitutionIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple institution_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_institution_value").should("be.visible");
        cy.get("#field_institution_value").should("have.value", "01G0Y8TVFC0PT0F5AQFRC5RZ3G");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_institution": {
                    "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3G",
                    "class": "App\\Entity\\InstitutionIntervention"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_institution_value").select('01G0Y8TVFC0PT0F5AQFRC5RZ3E');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_institution":
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3E",
                        "class": "App\\Entity\\InstitutionIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued institution_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_institution__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_institution__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_institution__2_value").should("be.visible");

        // Change value.
        cy.get("#field_institution__0_value").select('01G0Y8TVFC0PT0F5AQFRC5RZ3E');
        cy.get("#field_institution__1_value").select('01G0Y8TVFC0PT0F5AQFRC5RZ3G');
        cy.get("#field_institution__2_value").select('01G0Y8TVFC0PT0F5AQFRC5RZ3M');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_institution": [
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3E",
                        "class": "App\\Entity\\InstitutionIntervention"
                    },
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3G",
                        "class": "App\\Entity\\InstitutionIntervention"
                    },
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3M",
                        "class": "App\\Entity\\InstitutionIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_institution2-remove").click();
        cy.get('#field_institution__2_value').should('not.exist')
        cy.get("#field_institution1-remove").click();
        cy.get('#field_institution__1_value').should('not.exist')
        cy.get("#field_institution0-remove").click();
        cy.get('#field_institution__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_institution": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued institution_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_institution__0_value").should("be.visible");
        cy.get("#field_institution__0_value").should("have.value", "01G0Y8TVFC0PT0F5AQFRC5RZ3G");
        cy.get("#field_institution__1_value").should("be.visible");
        cy.get("#field_institution__1_value").should("have.value", "01G0Y8TVFC0PT0F5AQFRC5RZ3M");
        cy.get("#field_institution__2_value").should("be.visible");
        cy.get("#field_institution__2_value").should("have.value", "01G0Y8TVFC0PT0F5AQFRC5RZ3J");

        // Change value.
        cy.get("#field_institution__0_value").select("01G0Y8TVFC0PT0F5AQFRC5RZ3E");
        cy.get("#field_institution__1_value").select("01G0Y8TVFC0PT0F5AQFRC5RZ3G");
        cy.get("#field_institution__2_value").select("01G0Y8TVFC0PT0F5AQFRC5RZ3M");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_institution": [
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3E",
                        "class": "App\\Entity\\InstitutionIntervention"
                    },
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3G",
                        "class": "App\\Entity\\InstitutionIntervention"
                    },
                    {
                        "value": "01G0Y8TVFC0PT0F5AQFRC5RZ3M",
                        "class": "App\\Entity\\InstitutionIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_institution2-remove").click();
        cy.get('#field_institution__2_value').should('not.exist')
        cy.get("#field_institution1-remove").click();
        cy.get('#field_institution__1_value').should('not.exist')
        cy.get("#field_institution0-remove").click();
        cy.get('#field_institution__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_institution": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
