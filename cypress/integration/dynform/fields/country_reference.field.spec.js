/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm country_reference field.
 */
context('Test DynForm country_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_country": {
                "id": "field_country",
                "type": "country_reference",
                "label": "Field Country",
                "description": "Select a country",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Country form field",
        "type": "",
        "meta": {
            "title": "Country form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field country",
                            "field_id": "field_country",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_country": {
                "id": "field_country",
                "type": "country_reference",
                "label": "Field Country",
                "description": "Select a country",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Country form field",
        "type": "",
        "meta": {
            "title": "Country form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field country",
                            "field_id": "field_country",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_country": {
            "value": "hn",
            "class": "App\\Entity\\Country"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_country": [
            {
                "value": "hn",
                "class": "App\\Entity\\Country"
            },
            {
                "value": "ni",
                "class": "App\\Entity\\Country"
            },
            {
                "value": "pa",
                "class": "App\\Entity\\Country"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get countries from API.
        cy.intercept('GET', '/api/v1/countries?page=2', [])
            .as("countriesLoaded");
        cy.intercept('GET', '/api/v1/countries?page=1', {fixture: 'countries.json'});
    })

    it('DynForm Field - simple country_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        cy.wait("@countriesLoaded");
        // Validate field existence.
        cy.get("#field_country_value").should("be.visible");
        // Change value.
        cy.get("#field_country_value").select('ni');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_country": {
                    "value": "ni",
                    "class": "App\\Entity\\Country"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_country_value").select('pa');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_country":
                    {
                        "value": "pa",
                        "class": "App\\Entity\\Country"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple country_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        cy.wait("@countriesLoaded");

        // Validate field existence.
        cy.get("#field_country_value").should("be.visible");
        cy.get("#field_country_value").should("have.value", "hn");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_country": {
                    "value": "hn",
                    "class": "App\\Entity\\Country"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_country_value").select('ni');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_country":
                    {
                        "value": "ni",
                        "class": "App\\Entity\\Country"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued country_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        cy.wait("@countriesLoaded");
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_country__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_country__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_country__2_value").should("be.visible");

        // Change value.
        cy.get("#field_country__0_value").select('pa');
        cy.get("#field_country__1_value").select('hn');
        cy.get("#field_country__2_value").select('ni');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_country": [
                    {
                        "value": "pa",
                        "class": "App\\Entity\\Country"
                    },
                    {
                        "value": "hn",
                        "class": "App\\Entity\\Country"
                    },
                    {
                        "value": "ni",
                        "class": "App\\Entity\\Country"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_country2-remove").click();
        cy.get('#field_country__2_value').should('not.exist')
        cy.get("#field_country1-remove").click();
        cy.get('#field_country__1_value').should('not.exist')
        cy.get("#field_country0-remove").click();
        cy.get('#field_country__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_country": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued country_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        cy.wait("@countriesLoaded");
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_country__0_value").should("be.visible");
        cy.get("#field_country__0_value").should("have.value", "hn");
        cy.get("#field_country__1_value").should("be.visible");
        cy.get("#field_country__1_value").should("have.value", "ni");
        cy.get("#field_country__2_value").should("be.visible");
        cy.get("#field_country__2_value").should("have.value", "pa");

        // Change value.
        cy.get("#field_country__0_value").select("pa");
        cy.get("#field_country__1_value").select("hn");
        cy.get("#field_country__2_value").select("ni");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_country": [
                    {
                        "value": "pa",
                        "class": "App\\Entity\\Country"
                    },
                    {
                        "value": "hn",
                        "class": "App\\Entity\\Country"
                    },
                    {
                        "value": "ni",
                        "class": "App\\Entity\\Country"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_country2-remove").click();
        cy.get('#field_country__2_value').should('not.exist')
        cy.get("#field_country1-remove").click();
        cy.get('#field_country__1_value').should('not.exist')
        cy.get("#field_country0-remove").click();
        cy.get('#field_country__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_country": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
