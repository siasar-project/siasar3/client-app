/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

/**
* @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
*/

/**
* Test DynForm water_quality_entity_reference field.
*/
context('Window', () => {
  // Form structure.
  let fStructSingle = JSON.stringify({
      "description": "",
      "fields": {
          "field_water_quality_entity_reference": {
              "id": "field_water_quality_entity_reference",
              "type": "water_quality_entity_reference",
              "label": "Field water_quality_entity_reference",
              "description": "Field water_quality_entity_reference",
              "indexable": false,
              "internal": false,
              "deprecated": false,
              "settings": {
                  "required": true,
                  "multivalued": false,
                  "weight": 0,
                  "meta": {
                      "catalog_id": "0.1"
                  },
                  "sort": false,
                  "filter": false
              }
          }
      },
      "id": "form.sample",
      "requires": [],
      "title": "water_quality_entity_reference form field",
      "type": "",
      "meta": {
          "title": "water_quality_entity_reference form field",
          "version": "Version 1.0",
          "field_groups": [
              {
                  "id": "0",
                  "title": "Group 0",
                  "children": {
                      "0.1": {
                          "id": "0.1",
                          "title": "Field water_quality_entity_reference",
                          "field_id": "field_water_quality_entity_reference",
                          "weight": 0
                      }
                  }
              }
          ]
      }
  }, undefined, 4);
  let fStructMulti = JSON.stringify({
      "description": "",
      "fields": {
          "field_water_quality_entity_reference": {
              "id": "field_water_quality_entity_reference",
              "type": "water_quality_entity_reference",
              "label": "Field water_quality_entity_reference",
              "description": "Field water_quality_entity_reference",
              "indexable": false,
              "internal": false,
              "deprecated": false,
              "settings": {
                  "required": true,
                  "multivalued": true,
                  "weight": 0,
                  "meta": {
                      "catalog_id": "0.1"
                  },
                  "sort": false,
                  "filter": false
              }
          }
      },
      "id": "form.sample",
      "requires": [],
      "title": "water_quality_entity_reference form field",
      "type": "",
      "meta": {
          "title": "water_quality_entity_reference form field",
          "version": "Version 1.0",
          "field_groups": [
              {
                  "id": "0",
                  "title": "Group 0",
                  "children": {
                      "0.1": {
                          "id": "0.1",
                          "title": "Field water_quality_entity_reference",
                          "field_id": "field_water_quality_entity_reference",
                          "weight": 0
                      }
                  }
              }
          ]
      }
  }, undefined, 4);

  let fDataSingle = JSON.stringify({
      "field_water_quality_entity_reference": {
          "value": "01G3ZZ3NH04AN3A2K4V6H8C271",
          "class": "App\\Entity\\WaterQualityEntity"
      }
  }, undefined, 4);
  let fDataMulti = JSON.stringify({
      "field_water_quality_entity_reference": [
          {
              "value": "01G3ZZ3NH04AN3A2K4V6H8C271",
              "class": "App\\Entity\\WaterQualityEntity"
          },
          {
              "value": "01G3ZZ3NH04AN3A2K4V6H8C272",
              "class": "App\\Entity\\WaterQualityEntity"
          },
          {
              "value": "01G3ZZ3NH04AN3A2K4V6H8C273",
              "class": "App\\Entity\\WaterQualityEntity"
          }
      ]
  }, undefined, 4);

  beforeEach(() => {
      // Mock get technical_assistance_providers from API.
      cy.intercept('GET', '/api/v1/water_quality_entities?page=2', []);
      cy.intercept('GET', '/api/v1/water_quality_entities?page=1', {fixture: 'water_quality_entities.json'});
  })

  it('DynForm Field - simple water_quality_entity_reference without data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructSingle);
      // Generated change event.
      cy.get("#txt_structure").type('{moveToEnd}{enter}');
      // Validate field existence.
      cy.get(".form-field-water_quality_entity_reference").should("be.visible");
      // Change value.
      cy.get(".form-field-water_quality_entity_reference [type='radio']").check('01G3ZZ3NH04AN3A2K4V6H8C273');
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_water_quality_entity_reference": {
                      "value": "01G3ZZ3NH04AN3A2K4V6H8C273",
                      "class": "App\\Entity\\WaterQualityEntity"
                  }
              }, undefined, 4));
      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - simple water_quality_entity_reference with data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructSingle);
      // Generated change event.
      cy.get("#txt_structure").type('{moveToEnd}{enter}');

      // Set data values in textarea.
      cy.get("#txt_data").invoke('val', fDataSingle);
      // Generated change event.
      cy.get("#txt_data").type('{moveToEnd}{enter}');

      // Validate field existence.
      cy.get(".form-field-water_quality_entity_reference").should("be.visible");
      cy.get(".form-field-water_quality_entity_reference :checked").should("be.checked").and('have.value', '01G3ZZ3NH04AN3A2K4V6H8C271');

      // Validate data value in field.
      cy.get("#test-form-submit").click();
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_water_quality_entity_reference": {
                      "value": "01G3ZZ3NH04AN3A2K4V6H8C271",
                      "class": "App\\Entity\\WaterQualityEntity"
                  }
              }, undefined, 4));

      // Change value.
      cy.get(".form-field-water_quality_entity_reference [type='radio']").check('01G3ZZ3NH04AN3A2K4V6H8C273');
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_water_quality_entity_reference": {
                      "value": "01G3ZZ3NH04AN3A2K4V6H8C273",
                      "class": "App\\Entity\\WaterQualityEntity"
                  }
              }, undefined, 4));

      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - multivalued water_quality_entity_reference without data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructMulti);
      // Generated change event.
      cy.get("#txt_structure").type('{moveToEnd}{enter}');
      // Validate field existence.
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C271]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C272]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C273]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C274]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C275]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C276]').should("be.visible");

      // Change value.
      cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C271');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C272');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C273');
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_water_quality_entity_reference": [
                      {
                          "value": "01G3ZZ3NH04AN3A2K4V6H8C271",
                          "class": "App\\Entity\\WaterQualityEntity"
                      },
                      {
                          "value": "01G3ZZ3NH04AN3A2K4V6H8C272",
                          "class": "App\\Entity\\WaterQualityEntity"
                      },
                      {
                          "value": "01G3ZZ3NH04AN3A2K4V6H8C273",
                          "class": "App\\Entity\\WaterQualityEntity"
                      }
                  ]
              }, undefined, 4));

      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - multivalued water_quality_entity_reference with data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructMulti);
      cy.get("#txt_structure").type('{moveToEnd}{enter}');
      // Set data values in textarea.
      cy.get("#txt_data").invoke('val', fDataMulti);
      cy.get("#txt_data").type('{moveToEnd}{enter}');
      // Validate field existence.

      // Validate field existence and value.
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C271]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C271]').should('be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C272]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C272]').should('be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C273]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C273]').should('be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C274]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C274]').should('not.be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C275]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C275]').should('not.be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C276]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3ZZ3NH04AN3A2K4V6H8C276]').should('not.be.checked');
      
      // Change value.
      cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3ZZ3NH04AN3A2K4V6H8C271');
      cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3ZZ3NH04AN3A2K4V6H8C273');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C274');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZZ3NH04AN3A2K4V6H8C276');
      
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_water_quality_entity_reference": [
                      {
                          "value": "01G3ZZ3NH04AN3A2K4V6H8C272",
                          "class": "App\\Entity\\WaterQualityEntity"
                      },
                      {
                          "value": '01G3ZZ3NH04AN3A2K4V6H8C274',
                          "class": "App\\Entity\\WaterQualityEntity"
                      },
                      {
                          "value": "01G3ZZ3NH04AN3A2K4V6H8C276",
                          "class": "App\\Entity\\WaterQualityEntity"
                      }
                  ]
              }, undefined, 4));

      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

})
