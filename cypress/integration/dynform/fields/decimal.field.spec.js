/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm decimal field.
 */
context('Test DynForm decimal field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_decimal": {
                "id": "field_decimal",
                "type": "decimal",
                "label": "Field decimal",
                "description": "Field decimal",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "decimal form field",
        "type": "",
        "meta": {
            "title": "decimal form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field decimal",
                            "field_id": "field_decimal",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_decimal": {
                "id": "field_decimal",
                "type": "decimal",
                "label": "Field decimal",
                "description": "Field decimal",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "decimal form field",
        "type": "",
        "meta": {
            "title": "decimal form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field decimal",
                            "field_id": "field_decimal",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructSinglePrecisionScale = JSON.stringify({
        "description": "",
        "fields": {
            "field_decimal": {
                "id": "field_decimal",
                "type": "decimal",
                "label": "Field decimal",
                "description": "Field decimal",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "precision": 20,
                    "scale": 4
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "decimal form field",
        "type": "",
        "meta": {
            "title": "decimal form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field decimal",
                            "field_id": "field_decimal",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_decimal": {
                "value": "1234.56"
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_decimal": [
            {
                "value": ""
            },
            {
                "value": "1234.56"
            },
            {
                "value": ""
            }
        ]
    }, undefined, 4);
    let fDataSinglePrecisionScale = JSON.stringify({
        "field_decimal": {
            "value": "1234567890123456.1234"
        }
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple decimal without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_decimal").should("be.visible");
        // Change value.
        cy.get("#field_decimal").type('1234.56');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": {
                        "value": "1234.56"
                    }
                }, undefined, 4));
        // Set empty value.
        cy.get("#field_decimal").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": {
                        "value": ""
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple decimal with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_decimal").should("be.visible");
        cy.get("#field_decimal").should('have.value', '1234.56');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": {
                        "value": "1234.56"
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_decimal").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_decimal": {
                    "value": ""
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued decimal without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_decimal__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_decimal__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_decimal__2").should("be.visible");

        // Change value.
        cy.get("#field_decimal__0").type('1234.56');
        cy.get("#field_decimal__1").type('2234.56');
        cy.get("#field_decimal__2").type('3234.56');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": [
                        {
                            "value": "1234.56"
                        },
                        {
                            "value": "2234.56"
                        },
                        {
                            "value": "3234.56"
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_decimal2-remove").click();
        cy.get('#field_decimal__2').should('not.exist')
        cy.get("#field_decimal1-remove").click();
        cy.get('#field_decimal__1').should('not.exist')
        cy.get("#field_decimal0-remove").click();
        cy.get('#field_decimal__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued decimal with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_decimal__0").should("be.visible");
        cy.get("#field_decimal__0").should('have.value', '');
        cy.get("#field_decimal__1").should("be.visible");
        cy.get("#field_decimal__1").should('have.value', '1234.56');
        cy.get("#field_decimal__2").should("be.visible");
        cy.get("#field_decimal__2").should('have.value', '');

        // Change value.
        cy.get("#field_decimal__0").type('1234.56');
        cy.get("#field_decimal__1").type('{selectAll}{del}');
        cy.get("#field_decimal__2").type('2234.56');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_decimal": [
                    {
                        "value": "1234.56"
                    },
                    {
                        "value": ""
                    },
                    {
                        "value": "2234.56"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_decimal2-remove").click();
        cy.get('#field_decimal__2').should('not.exist')
        cy.get("#field_decimal1-remove").click();
        cy.get('#field_decimal__1').should('not.exist')
        cy.get("#field_decimal0-remove").click();
        cy.get('#field_decimal__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_decimal": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple decimal setting precision and scale without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSinglePrecisionScale);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_decimal").should("be.visible");
        // Change value.
        cy.get("#field_decimal").type('1234567890123456.1234');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": {
                        "value": "1234567890123456.1234"
                    }
                }, undefined, 4));
        // Set empty value.
        cy.get("#field_decimal").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": {
                        "value": ""
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple decimal setting precision and scale with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSinglePrecisionScale);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSinglePrecisionScale);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_decimal").should("be.visible");
        cy.get("#field_decimal").should('have.value', '1234567890123456.1234');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_decimal": {
                        "value": "1234567890123456.1234"
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_decimal").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_decimal": {
                    "value": ""
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })
})
