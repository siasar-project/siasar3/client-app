// /// <reference types="cypress" />
// Cypress.on('uncaught:exception', (err, runnable) => {
//     // returning false here prevents Cypress from
//     // failing the test
//     return false
// })

// /**
//  * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
//  */

// /**
//  * Test DynForm administrative_division_reference field.
//  */
// context('Window', () => {
//     // Form structure.
//     let fStructSingle = JSON.stringify({
//         "description": "",
//         "fields": {
//             "field_administrative_division": {
//                 "id": "field_administrative_division",
//                 "type": "administrative_division_reference",
//                 "label": "Field administrative_division_reference",
//                 "description": "Field administrative_division_reference",
//                 "indexable": false,
//                 "internal": false,
//                 "deprecated": false,
//                 "settings": {
//                     "required": true,
//                     "multivalued": false,
//                     "weight": 0,
//                     "meta": {
//                         "catalog_id": "0.1"
//                     },
//                     "sort": false,
//                     "filter": false
//                 }
//             }
//         },
//         "id": "form.sample",
//         "requires": [],
//         "title": "administrative_division_reference form field",
//         "type": "",
//         "meta": {
//             "title": "administrative_division_reference form field",
//             "version": "Version 1.0",
//             "field_groups": [
//                 {
//                     "id": "0",
//                     "title": "Group 0",
//                     "children": {
//                         "0.1": {
//                             "id": "0.1",
//                             "title": "Field administrative_division_reference",
//                             "field_id": "field_administrative_division",
//                             "weight": 0
//                         }
//                     }
//                 }
//             ]
//         }
//     }, undefined, 4);
//     let fStructMulti = JSON.stringify({
//         "description": "",
//         "fields": {
//             "field_administrative_division": {
//                 "id": "field_administrative_division",
//                 "type": "administrative_division_reference",
//                 "label": "Field administrative_division_reference",
//                 "description": "Field administrative_division_reference",
//                 "indexable": false,
//                 "internal": false,
//                 "deprecated": false,
//                 "settings": {
//                     "required": true,
//                     "multivalued": true,
//                     "weight": 0,
//                     "meta": {
//                         "catalog_id": "0.1"
//                     },
//                     "sort": false,
//                     "filter": false
//                 }
//             }
//         },
//         "id": "form.sample",
//         "requires": [],
//         "title": "administrative_division_reference form field",
//         "type": "",
//         "meta": {
//             "title": "administrative_division_reference form field",
//             "version": "Version 1.0",
//             "field_groups": [
//                 {
//                     "id": "0",
//                     "title": "Group 0",
//                     "children": {
//                         "0.1": {
//                             "id": "0.1",
//                             "title": "Field administrative_division_reference",
//                             "field_id": "field_administrative_division",
//                             "weight": 0
//                         }
//                     }
//                 }
//             ]
//         }
//     }, undefined, 4);

//     let countryCode = 'hn';

//     let fDataSingle = JSON.stringify({
//         "field_administrative_division": {
//             "value": "01G4G4YC8RQ6JCH5ANS0B3A458",
//             "class": "App\\Entity\\AdministrativeDivision"
//         }
//     }, undefined, 4);
//     let fDataMulti = JSON.stringify({
//         "field_administrative_division": [
//             {
//                 "value": "01G4G4YC8RQ6JCH5ANS0B3A458",
//                 "class": "App\\Entity\\AdministrativeDivision"
//             },
//             {
//                 "value": "01G4G4YC8RQ6JCH5ANS0B3A45C",
//                 "class": "App\\Entity\\AdministrativeDivision"
//             },
//             {
//                 "value": "01G4G4YC8RQ6JCH5ANS0B3A45G",
//                 "class": "App\\Entity\\AdministrativeDivision"
//             }
//         ]
//     }, undefined, 4);

//     beforeEach(() => {
//         window.localStorage.setItem("session", JSON.stringify({
//             "country": {
//                 "code": countryCode, 
//                 "divisionTypes": [ { "id": "01G54ABRNJ2RMBCCT7ZV7Q7HZS", "name": "Departamento", "level": 1 }, { "id": "01G54ABRNJ2RMBCCT7ZV7Q7HZT", "name": "Municipio", "level": 2 }, { "id": "01G54ABRNJ2RMBCCT7ZV7Q7HZV", "name": "Aldea", "level": 3 }, { "id": "01G54ABRNJ2RMBCCT7ZV7Q7HZW", "name": "Comunidad", "level": 4 } ]
//             }
//         }));
        
//         // Mock get administrative_divisions from API.
//         cy.intercept('GET', '/api/v1/administrative_divisions?page=2', []).as("endLoad");
//         cy.intercept('GET', '/api/v1/administrative_divisions?page=1', {fixture: 'administrative_divisions.json'});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8Q58C5FHBTD5KXMX6P', {"id": "01G4G4YC8Q58C5FHBTD5KXMX6P", "name": "Division A, Level 1", "level": 1, "country": "/api/v1/countries/hn", "parent": null});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A452', {"id": "01G4G4YC8RQ6JCH5ANS0B3A452", "name": "Division A, Level 2", "level": 2, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8Q58C5FHBTD5KXMX6P"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454', {"id": "01G4G4YC8RQ6JCH5ANS0B3A454", "name": "Division A, Level 3", "level": 3, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A452"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A456', {"id": "01G4G4YC8RQ6JCH5ANS0B3A456", "name": "Division A, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A45A', {"id": "01G4G4YC8RQ6JCH5ANS0B3A45A", "name": "Division C, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A45E', {"id": "01G4G4YC8RQ6JCH5ANS0B3A45E", "name": "Division E, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A45M', {"id": "01G4G4YC8RQ6JCH5ANS0B3A45M", "name": "Division A1, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A458', {"id": "01G4G4YC8RQ6JCH5ANS0B3A458", "name": "Division B, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A45C', {"id": "01G4G4YC8RQ6JCH5ANS0B3A45C", "name": "Division D, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//         cy.intercept('GET', '/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A45G', {"id": "01G4G4YC8RQ6JCH5ANS0B3A45G", "name": "Division F, Level 4", "level": 4, "country": "/api/v1/countries/hn", "parent": "/api/v1/administrative_divisions/01G4G4YC8RQ6JCH5ANS0B3A454"});
//     })

//     it('DynForm Field - simple administrative_division_reference without data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructSingle);
//         // Generated change event.
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');
//         cy.wait("@endLoad");

//         // Validate field existence.
//         cy.get("#field_administrative_division").should("be.visible");
//         // Change value.
//         cy.get("#field_administrative_division").select('01G4G4YC8Q58C5FHBTD5KXMX6P');
//         cy.get("#field_administrative_division").select('01G4G4YC8RQ6JCH5ANS0B3A452');
//         cy.get("#field_administrative_division").select('01G4G4YC8RQ6JCH5ANS0B3A454');
//         cy.get("#field_administrative_division").select('01G4G4YC8RQ6JCH5ANS0B3A45A');
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": {
//                         "value": "01G4G4YC8RQ6JCH5ANS0B3A45A",
//                         "class": "App\\Entity\\AdministrativeDivision"
//                     }
//                 }, undefined, 4));

//         // Remove one parent.
//         // cy.get("#field_administrative_division-remove-one").click();
//         // cy.get(".form-field-administrative_division_reference .parents span.parent").should('have.length', 2);
//         // Remove all parents.
//         cy.get("#field_administrative_division-remove-all").click();
//         cy.get(".form-field-administrative_division_reference .parents span.parent").should('have.length', 0);
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": {
//                         "value": "",
//                         "class": "App\\Entity\\AdministrativeDivision"
//                     }
//                 }, undefined, 4));

//         // Change value.
//         cy.get("#field_administrative_division").select('01G4G4YC8Q58C5FHBTD5KXMX6P');
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": {
//                         "value": "",
//                         "class": "App\\Entity\\AdministrativeDivision"
//                     }
//                 }, undefined, 4));

//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//     })

//     it('DynForm Field - simple administrative_division_reference with data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructSingle);
//         // Generated change event.
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');
//         cy.wait("@endLoad");
//         // Set data values in textarea.
//         cy.get("#txt_data").invoke('val', fDataSingle);
//         // Generated change event.
//         cy.get("#txt_data").type('{moveToEnd}{enter}');
        
        
//         // Validate field existence.
//         cy.get("#field_administrative_division").should("be.visible");
//         cy.get("#field_administrative_division").should("have.value", "01G4G4YC8RQ6JCH5ANS0B3A458");
//         // Validate data value in field.
//         cy.get("#test-form-submit").click();
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": {
//                         "value": "01G4G4YC8RQ6JCH5ANS0B3A458",
//                         "class": "App\\Entity\\AdministrativeDivision"
//                     }
//                 }, undefined, 4));

//         // Change value.
//         cy.get("#field_administrative_division").select('01G4G4YC8RQ6JCH5ANS0B3A45A');
        
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                 "field_administrative_division": {
//                     "value": "01G4G4YC8RQ6JCH5ANS0B3A45A",
//                     "class": "App\\Entity\\AdministrativeDivision"
//                 }
//             }, undefined, 4));

//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//     })

//     it('DynForm Field - multivalued administrative_division_reference without data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructMulti);
//         // Generated change event.
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');
//         // Validate field existence.
//         cy.get(".dynform-add button").should("be.visible");
        
//         // Add values.
//         cy.get(".dynform-add button").click();
//         cy.wait("@endLoad");
//         cy.get("#field_administrative_division__0").should("be.visible");
//         cy.get(".dynform-add button").click();
//         cy.get("#field_administrative_division__1").should("be.visible");
//         cy.get(".dynform-add button").click();
//         cy.get("#field_administrative_division__2").should("be.visible");

//         // Change value.
//         cy.get("#field_administrative_division__0").select('01G4G4YC8Q58C5FHBTD5KXMX6P');
//         cy.get("#field_administrative_division__0").select('01G4G4YC8RQ6JCH5ANS0B3A452');
//         cy.get("#field_administrative_division__0").select('01G4G4YC8RQ6JCH5ANS0B3A454');
//         cy.get("#field_administrative_division__0").select('01G4G4YC8RQ6JCH5ANS0B3A45A');

//         cy.get("#field_administrative_division__1").select('01G4G4YC8Q58C5FHBTD5KXMX6P');
//         cy.get("#field_administrative_division__1").select('01G4G4YC8RQ6JCH5ANS0B3A452');
//         cy.get("#field_administrative_division__1").select('01G4G4YC8RQ6JCH5ANS0B3A454');
//         cy.get("#field_administrative_division__1").select('01G4G4YC8RQ6JCH5ANS0B3A45E');

//         cy.get("#field_administrative_division__2").select('01G4G4YC8Q58C5FHBTD5KXMX6P');
//         cy.get("#field_administrative_division__2").select('01G4G4YC8RQ6JCH5ANS0B3A452');
//         cy.get("#field_administrative_division__2").select('01G4G4YC8RQ6JCH5ANS0B3A454');
//         cy.get("#field_administrative_division__2").select('01G4G4YC8RQ6JCH5ANS0B3A45M');

//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": [
//                         {
//                             "value": "01G4G4YC8RQ6JCH5ANS0B3A45A",
//                             "class": "App\\Entity\\AdministrativeDivision"
//                         },
//                         {
//                             "value": "01G4G4YC8RQ6JCH5ANS0B3A45E",
//                             "class": "App\\Entity\\AdministrativeDivision"
//                         },
//                         {
//                             "value": "01G4G4YC8RQ6JCH5ANS0B3A45M",
//                             "class": "App\\Entity\\AdministrativeDivision"
//                         }
//                     ]
//                 }, undefined, 4));

//         // Remove values. We must remove from last to first.
//         cy.get("#field_administrative_division2-remove").click();
//         cy.get('#field_administrative_division__2').should('not.exist')
//         cy.get("#field_administrative_division1-remove").click();
//         cy.get('#field_administrative_division__1').should('not.exist')
//         cy.get("#field_administrative_division0-remove").click();
//         cy.get('#field_administrative_division__0').should('not.exist')
//         // Get form value.
//         cy.get("#test-form-submit").click();
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": []
//                 }, undefined, 4));

//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//     })

//     it('DynForm Field - multivalued administrative_division_reference with data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructMulti);
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');

//         // Set data values in textarea.
//         cy.get("#txt_data").invoke('val', fDataMulti);
//         cy.get("#txt_data").type('{moveToEnd}{enter}');
//         cy.wait("@endLoad");
//         // Validate field existence.
//         cy.get(".dynform-add button").should("be.visible");

//         // Validate values.
//         cy.get("#field_administrative_division__0").should("be.visible");
//         cy.get("#field_administrative_division__0").should("have.value", "01G4G4YC8RQ6JCH5ANS0B3A458");
//         cy.get("#field_administrative_division__1").should("be.visible");
//         cy.get("#field_administrative_division__1").should("have.value", "01G4G4YC8RQ6JCH5ANS0B3A45C");
//         cy.get("#field_administrative_division__2").should("be.visible");
//         cy.get("#field_administrative_division__2").should("have.value", "01G4G4YC8RQ6JCH5ANS0B3A45G");

//         // Change value.
//         cy.get("#field_administrative_division__0").select('01G4G4YC8RQ6JCH5ANS0B3A45A');
//         cy.get("#field_administrative_division__1").select('01G4G4YC8RQ6JCH5ANS0B3A45E');
//         cy.get("#field_administrative_division__2").select('01G4G4YC8RQ6JCH5ANS0B3A45M');

//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": [
//                         {
//                             "value": "01G4G4YC8RQ6JCH5ANS0B3A45A",
//                             "class": "App\\Entity\\AdministrativeDivision"
//                         },
//                         {
//                             "value": "01G4G4YC8RQ6JCH5ANS0B3A45E",
//                             "class": "App\\Entity\\AdministrativeDivision"
//                         },
//                         {
//                             "value": "01G4G4YC8RQ6JCH5ANS0B3A45M",
//                             "class": "App\\Entity\\AdministrativeDivision"
//                         }
//                     ]
//                 }, undefined, 4));

//         // Remove values. We must remove from last to first.
//         cy.get("#field_administrative_division2-remove").click();
//         cy.get('#field_administrative_division__2').should('not.exist')
//         cy.get("#field_administrative_division1-remove").click();
//         cy.get('#field_administrative_division__1').should('not.exist')
//         cy.get("#field_administrative_division0-remove").click();
//         cy.get('#field_administrative_division__0').should('not.exist')
//         // Get form value.
//         cy.get("#test-form-submit").click();
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_administrative_division": []
//                 }, undefined, 4));

//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//     })

// })
