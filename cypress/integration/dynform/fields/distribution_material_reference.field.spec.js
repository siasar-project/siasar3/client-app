/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm distribution_material_reference field.
 */
context('Test DynForm distribution_material_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_distribution_material_reference": {
                "id": "field_distribution_material_reference",
                "type": "distribution_material_reference",
                "label": "Field distribution_material_reference",
                "description": "Field distribution_material_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "distribution_material_reference form field",
        "type": "",
        "meta": {
            "title": "distribution_material_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field distribution_material_reference",
                            "field_id": "field_distribution_material_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_distribution_material_reference": {
                "id": "field_distribution_material_reference",
                "type": "distribution_material_reference",
                "label": "Field distribution_material_reference",
                "description": "Field distribution_material_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "distribution_material_reference form field",
        "type": "",
        "meta": {
            "title": "distribution_material_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field distribution_material_reference",
                            "field_id": "field_distribution_material_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_distribution_material_reference": {
            "value": "01G4A8Y62VB74M07HEHXMYX5TY",
            "class": "App\\Entity\\DistributionMaterial"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_distribution_material_reference": [
            {
                "value": "01G4A8Y62VB74M07HEHXMYX5TY",
                "class": "App\\Entity\\DistributionMaterial"
            },
            {
                "value": "01G4A8Y62VB74M07HEHXMYX503",
                "class": "App\\Entity\\DistributionMaterial"
            },
            {
                "value": "01G4A8Y62VB74M07HEHXMYX502",
                "class": "App\\Entity\\DistributionMaterial"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/distribution_materials?page=2', []);
        cy.intercept('GET', '/api/v1/distribution_materials?page=1', {fixture: 'distribution_material.json'});
    })

    it('DynForm Field - simple distribution_material_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-distribution_material_reference").should("be.visible");
        // Change value.
        cy.get(".form-field-distribution_material_reference [type='radio']").check('01G4A8Y62VB74M07HEHXMYX501');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_distribution_material_reference": {
                        "value": "01G4A8Y62VB74M07HEHXMYX501",
                        "class": "App\\Entity\\DistributionMaterial"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple distribution_material_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-distribution_material_reference").should("be.visible");
        cy.get(".form-field-distribution_material_reference :checked").should("be.checked").and('have.value', '01G4A8Y62VB74M07HEHXMYX5TY');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_distribution_material_reference": {
                        "value": "01G4A8Y62VB74M07HEHXMYX5TY",
                        "class": "App\\Entity\\DistributionMaterial"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-distribution_material_reference [type='radio']").check('01G4A8Y62VB74M07HEHXMYX501');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_distribution_material_reference": {
                        "value": "01G4A8Y62VB74M07HEHXMYX501",
                        "class": "App\\Entity\\DistributionMaterial"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued distribution_material_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y1NHF4EXG9W40F8N94ME]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y1P49Z9BXMAQR57W241T]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX5TY]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX501]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX502]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX503]').should("be.visible");

        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8Y62VB74M07HEHXMYX5TY');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8Y62VB74M07HEHXMYX501');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8Y62VB74M07HEHXMYX502');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_distribution_material_reference": [
                        {
                            "value": "01G4A8Y62VB74M07HEHXMYX5TY",
                            "class": "App\\Entity\\DistributionMaterial"
                        },
                        {
                            "value": "01G4A8Y62VB74M07HEHXMYX501",
                            "class": "App\\Entity\\DistributionMaterial"
                        },
                        {
                            "value": "01G4A8Y62VB74M07HEHXMYX502",
                            "class": "App\\Entity\\DistributionMaterial"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued distribution_material_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y1NHF4EXG9W40F8N94ME]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y1NHF4EXG9W40F8N94ME]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y1P49Z9BXMAQR57W241T]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y1P49Z9BXMAQR57W241T]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX5TY]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX5TY]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX501]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX501]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX502]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX502]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX503]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62VB74M07HEHXMYX503]').should('be.checked');
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8Y1NHF4EXG9W40F8N94ME');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8Y1P49Z9BXMAQR57W241T');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4A8Y62VB74M07HEHXMYX5TY');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8Y62VB74M07HEHXMYX501');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4A8Y62VB74M07HEHXMYX502');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4A8Y62VB74M07HEHXMYX503');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_distribution_material_reference": [
                        {
                            "value": '01G4A8Y1NHF4EXG9W40F8N94ME',
                            "class": "App\\Entity\\DistributionMaterial"
                        },
                        {
                            "value": "01G4A8Y1P49Z9BXMAQR57W241T",
                            "class": "App\\Entity\\DistributionMaterial"
                        },
                        {
                            "value": "01G4A8Y62VB74M07HEHXMYX501",
                            "class": "App\\Entity\\DistributionMaterial"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
