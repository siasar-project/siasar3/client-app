/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

/**
* @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
*/

/**
* Test DynForm type_health_facility_reference field.
*/
context('Window', () => {
  // Form structure.
  let fStructSingle = JSON.stringify({
      "description": "",
      "fields": {
          "field_type_health_facility_reference": {
              "id": "field_type_health_facility_reference",
              "type": "type_health_facility_reference",
              "label": "Field type_health_facility_reference",
              "description": "Field type_health_facility_reference",
              "indexable": false,
              "internal": false,
              "deprecated": false,
              "settings": {
                  "required": true,
                  "multivalued": false,
                  "weight": 0,
                  "meta": {
                      "catalog_id": "0.1"
                  },
                  "sort": false,
                  "filter": false
              }
          }
      },
      "id": "form.sample",
      "requires": [],
      "title": "type_health_facility_reference form field",
      "type": "",
      "meta": {
          "title": "type_health_facility_reference form field",
          "version": "Version 1.0",
          "field_groups": [
              {
                  "id": "0",
                  "title": "Group 0",
                  "children": {
                      "0.1": {
                          "id": "0.1",
                          "title": "Field type_health_facility_reference",
                          "field_id": "field_type_health_facility_reference",
                          "weight": 0
                      }
                  }
              }
          ]
      }
  }, undefined, 4);
  let fStructMulti = JSON.stringify({
      "description": "",
      "fields": {
          "field_type_health_facility_reference": {
              "id": "field_type_health_facility_reference",
              "type": "type_health_facility_reference",
              "label": "Field type_health_facility_reference",
              "description": "Field type_health_facility_reference",
              "indexable": false,
              "internal": false,
              "deprecated": false,
              "settings": {
                  "required": true,
                  "multivalued": true,
                  "weight": 0,
                  "meta": {
                      "catalog_id": "0.1"
                  },
                  "sort": false,
                  "filter": false
              }
          }
      },
      "id": "form.sample",
      "requires": [],
      "title": "type_health_facility_reference form field",
      "type": "",
      "meta": {
          "title": "type_health_facility_reference form field",
          "version": "Version 1.0",
          "field_groups": [
              {
                  "id": "0",
                  "title": "Group 0",
                  "children": {
                      "0.1": {
                          "id": "0.1",
                          "title": "Field type_health_facility_reference",
                          "field_id": "field_type_health_facility_reference",
                          "weight": 0
                      }
                  }
              }
          ]
      }
  }, undefined, 4);

  let fDataSingle = JSON.stringify({
      "field_type_health_facility_reference": {
          "value": "01G4FJQJKACXBTA7A4M8EDRF05",
          "class": "App\\Entity\\TypeHealthcareFacility"
      }
  }, undefined, 4);
  let fDataMulti = JSON.stringify({
      "field_type_health_facility_reference": [
          {
              "value": "01G4FJQJKACXBTA7A4M8EDRF05",
              "class": "App\\Entity\\TypeHealthcareFacility"
          },
          {
              "value": "01G4FJQJKACXBTA7A4M8EDRF02",
              "class": "App\\Entity\\TypeHealthcareFacility"
          },
          {
              "value": "01G4FJQJKACXBTA7A4M8EDRFVA",
              "class": "App\\Entity\\TypeHealthcareFacility"
          }
      ]
  }, undefined, 4);

  beforeEach(() => {
      // Mock get type_healthcare_facilities from API.
      cy.intercept('GET', '/api/v1/type_healthcare_facilities?page=2', []);
      cy.intercept('GET', '/api/v1/type_healthcare_facilities?page=1', {fixture: 'type_healthcare_facilities.json'});
  })

  it('DynForm Field - simple type_health_facility_reference without data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructSingle);
      // Generated change event.
      cy.get("#txt_structure").type('{moveToEnd}{enter}');
      // Validate field existence.
      cy.get(".form-field-type_health_facility_reference").should("be.visible");
      // Change value.
      cy.get(".form-field-type_health_facility_reference [type='radio']").check('01G4FJQJKACXBTA7A4M8EDRF03');
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_type_health_facility_reference": {
                      "value": "01G4FJQJKACXBTA7A4M8EDRF03",
                      "class": "App\\Entity\\TypeHealthcareFacility"
                  }
              }, undefined, 4));
      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - simple type_health_facility_reference with data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructSingle);
      // Generated change event.
      cy.get("#txt_structure").type('{moveToEnd}{enter}');

      // Set data values in textarea.
      cy.get("#txt_data").invoke('val', fDataSingle);
      // Generated change event.
      cy.get("#txt_data").type('{moveToEnd}{enter}');

      // Validate field existence.
      cy.get(".form-field-type_health_facility_reference").should("be.visible");
      cy.get(".form-field-type_health_facility_reference :checked").should("be.checked").and('have.value', '01G4FJQJKACXBTA7A4M8EDRF05');

      // Validate data value in field.
      cy.get("#test-form-submit").click();
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_type_health_facility_reference": {
                      "value": "01G4FJQJKACXBTA7A4M8EDRF05",
                      "class": "App\\Entity\\TypeHealthcareFacility"
                  }
              }, undefined, 4));

      // Change value.
      cy.get(".form-field-type_health_facility_reference [type='radio']").check('01G4FJQJKACXBTA7A4M8EDRF03');
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_type_health_facility_reference": {
                      "value": "01G4FJQJKACXBTA7A4M8EDRF03",
                      "class": "App\\Entity\\TypeHealthcareFacility"
                  }
              }, undefined, 4));

      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - multivalued type_health_facility_reference without data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructMulti);
      // Generated change event.
      cy.get("#txt_structure").type('{moveToEnd}{enter}');
      // Validate field existence.
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF01]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF02]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF03]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF04]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF05]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRFVA]').should("be.visible");

      // Change value.
      cy.get(".form-multivalued-list [type='checkbox']").check('01G4FJQJKACXBTA7A4M8EDRF01');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G4FJQJKACXBTA7A4M8EDRF03');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G4FJQJKACXBTA7A4M8EDRFVA');
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_type_health_facility_reference": [
                      {
                          "value": "01G4FJQJKACXBTA7A4M8EDRF01",
                          "class": "App\\Entity\\TypeHealthcareFacility"
                      },
                      {
                          "value": "01G4FJQJKACXBTA7A4M8EDRF03",
                          "class": "App\\Entity\\TypeHealthcareFacility"
                      },
                      {
                          "value": "01G4FJQJKACXBTA7A4M8EDRFVA",
                          "class": "App\\Entity\\TypeHealthcareFacility"
                      }
                  ]
              }, undefined, 4));

      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - multivalued type_health_facility_reference with data', () => {
      // Visit test page.
      cy.visit("dynform_field_test");
      // Set configuration values in textarea.
      cy.get("#txt_structure").invoke('val', fStructMulti);
      cy.get("#txt_structure").type('{moveToEnd}{enter}');
      // Set data values in textarea.
      cy.get("#txt_data").invoke('val', fDataMulti);
      cy.get("#txt_data").type('{moveToEnd}{enter}');
      // Validate field existence.

      // Validate field existence and value.
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF01]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF01]').should('not.be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF02]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF02]').should('be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF03]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF03]').should('not.be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF04]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF04]').should('not.be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF05]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRF05]').should('be.checked');
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRFVA]').should("be.visible");
      cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4FJQJKACXBTA7A4M8EDRFVA]').should('be.checked');
      
      // Change value.
      cy.get(".form-multivalued-list [type='checkbox']").check('01G4FJQJKACXBTA7A4M8EDRF01');
      cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4FJQJKACXBTA7A4M8EDRF02');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G4FJQJKACXBTA7A4M8EDRF03');
      cy.get(".form-multivalued-list [type='checkbox']").check('01G4FJQJKACXBTA7A4M8EDRF04');
      cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4FJQJKACXBTA7A4M8EDRF05');
      cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4FJQJKACXBTA7A4M8EDRFVA');
      
      // Submit form.
      cy.get("#test-form-submit").click();
      // Check data value.
      cy.get("#txt_data")
          .should("have.text", JSON.stringify({
                  "field_type_health_facility_reference": [
                      {
                          "value": '01G4FJQJKACXBTA7A4M8EDRF01',
                          "class": "App\\Entity\\TypeHealthcareFacility"
                      },
                      {
                          "value": "01G4FJQJKACXBTA7A4M8EDRF03",
                          "class": "App\\Entity\\TypeHealthcareFacility"
                      },
                      {
                          "value": "01G4FJQJKACXBTA7A4M8EDRF04",
                          "class": "App\\Entity\\TypeHealthcareFacility"
                      }
                  ]
              }, undefined, 4));

      // Scroll to form.
      cy.get(".form-sample").scrollIntoView();
  })

})
