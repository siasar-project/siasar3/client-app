/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />


Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm ulid_reference field.
 */
context('Test DynForm ulid_reference field.', () => {
  // Form structure.
  let fStructSingle = JSON.stringify({
    "description": "",
    "fields": {
      "field_ulid_reference": {
        "id": "field_ulid_reference",
        "type": "ulid",
        "label": "Internal reference ID",
        "description": "Internal management reference ID.",
        "indexable": false,
        "internal": true,
        "deprecated": false,
        "settings": {
          "required": false,
          "multivalued": false,
          "weight": 0,
          "meta": [],
          "sort": false,
          "filter": false
        }
      }
    },
    "id": "form.sample",
    "requires": [],
    "title": "ulid form field",
    "type": "",
    "meta": {
      "title": "ulid form field",
      "version": "Version 1.0",
      "field_groups": [
        {
          "id": "0",
          "title": "Group 0",
          "children": {
            "0.1": {
              "id": "0.1",
              "title": "Field ulid",
              "field_id": "field_ulid_reference",
              "weight": 0
            }
          }
        }
      ]
    }
  }, undefined, 4);

  let fDataEmpty = JSON.stringify({
    "field_ulid_reference": {
      "value": ""
    }
  }, undefined, 4);
  let fData = JSON.stringify({
    "field_ulid_reference": {
      "value": "01FX4W3QE7SYY2M2SK2SS7TQ07"
    }
  }, undefined, 4);

  beforeEach(() => {
    // cy.visit("/")
  })

  it('DynForm Field - simple ulid_reference without data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructSingle);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
    // Validate field existence.
    cy.get("#field_ulid_reference").should("be.visible");
    // Validate disabled field.
    cy.get("#field_ulid_reference").should("be.disabled");
  })

  it('DynForm Field - simple ulid_reference with data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructSingle);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();

    // Set data values in textarea.
    cy.get("#txt_data").invoke('val', fData);
    // Generated change event.
    cy.get("#txt_data").type('{moveToEnd}{enter}');

    // Validate field existence.
    cy.get("#field_ulid_reference").should("be.visible");
    cy.get("#field_ulid_reference").should("be.disabled");

    // Validate data value in field.
    cy.get("#test-form-submit").click();
    cy.get("#txt_data")
      .should("have.text", fData);
  })
})
