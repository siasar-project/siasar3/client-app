/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm tap_reference field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_tap_reference": {
                "id": "field_tap_reference",
                "type": "tap_reference",
                "label": "Field tap_reference",
                "description": "Field tap_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "tap_reference form field",
        "type": "",
        "meta": {
            "title": "tap_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field tap_reference",
                            "field_id": "field_tap_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_tap_reference": {
                "id": "field_tap_reference",
                "type": "tap_reference",
                "label": "Field tap_reference",
                "description": "Field tap_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "tap_reference form field",
        "type": "",
        "meta": {
            "title": "tap_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field tap_reference",
                            "field_id": "field_tap_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_tap_reference": {
            "value": "01G3RMMN0ARJE2S3TN2QHBMXQ1",
            "class": "App\\Entity\\TechnicalAssistanceProvider"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_tap_reference": [
            {
                "value": "01G3RMMN0ARJE2S3TN2QHBMXQ1",
                "class": "App\\Entity\\TechnicalAssistanceProvider"
            },
            {
                "value": "01G3RMMN0ARJE2S3TN2QHBMXPV",
                "class": "App\\Entity\\TechnicalAssistanceProvider"
            },
            {
                "value": "01G3RMMN0ARJE2S3TN2QHBMXQ3",
                "class": "App\\Entity\\TechnicalAssistanceProvider"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/technical_assistance_providers?page=2', []);
        cy.intercept('GET', '/api/v1/technical_assistance_providers?page=1', {fixture: 'technical_assistance_providers.json'});
    })

    it('DynForm Field - simple tap_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-tap_reference").should("be.visible");
        // Change value.
        cy.get(".form-field-tap_reference [type='radio']").check('01G3RMMN0ARJE2S3TN2QHBMXPX');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_tap_reference": {
                        "value": "01G3RMMN0ARJE2S3TN2QHBMXPX",
                        "class": "App\\Entity\\TechnicalAssistanceProvider"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple tap_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-tap_reference").should("be.visible");
        cy.get(".form-field-tap_reference :checked").should("be.checked").and('have.value', '01G3RMMN0ARJE2S3TN2QHBMXQ1');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_tap_reference": {
                        "value": "01G3RMMN0ARJE2S3TN2QHBMXQ1",
                        "class": "App\\Entity\\TechnicalAssistanceProvider"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-tap_reference [type='radio']").check('01G3RMMN0ARJE2S3TN2QHBMXPX');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_tap_reference": {
                        "value": "01G3RMMN0ARJE2S3TN2QHBMXPX",
                        "class": "App\\Entity\\TechnicalAssistanceProvider"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued tap_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPS]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPV]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPX]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPZ]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXQ1]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXQ3]').should("be.visible");

        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3RMMN0ARJE2S3TN2QHBMXPS');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3RMMN0ARJE2S3TN2QHBMXPX');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3RMMN0ARJE2S3TN2QHBMXQ3');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_tap_reference": [
                        {
                            "value": "01G3RMMN0ARJE2S3TN2QHBMXPS",
                            "class": "App\\Entity\\TechnicalAssistanceProvider"
                        },
                        {
                            "value": "01G3RMMN0ARJE2S3TN2QHBMXPX",
                            "class": "App\\Entity\\TechnicalAssistanceProvider"
                        },
                        {
                            "value": "01G3RMMN0ARJE2S3TN2QHBMXQ3",
                            "class": "App\\Entity\\TechnicalAssistanceProvider"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued tap_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPS]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPS]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPV]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPV]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPX]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPX]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPZ]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXPZ]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXQ1]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXQ1]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXQ3]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G3RMMN0ARJE2S3TN2QHBMXQ3]').should('be.checked');
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3RMMN0ARJE2S3TN2QHBMXPS');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3RMMN0ARJE2S3TN2QHBMXPV');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3RMMN0ARJE2S3TN2QHBMXPX');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G3RMMN0ARJE2S3TN2QHBMXPZ');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3RMMN0ARJE2S3TN2QHBMXQ1');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3RMMN0ARJE2S3TN2QHBMXQ3');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_tap_reference": [
                        {
                            "value": '01G3RMMN0ARJE2S3TN2QHBMXPS',
                            "class": "App\\Entity\\TechnicalAssistanceProvider"
                        },
                        {
                            "value": "01G3RMMN0ARJE2S3TN2QHBMXPX",
                            "class": "App\\Entity\\TechnicalAssistanceProvider"
                        },
                        {
                            "value": "01G3RMMN0ARJE2S3TN2QHBMXPZ",
                            "class": "App\\Entity\\TechnicalAssistanceProvider"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
