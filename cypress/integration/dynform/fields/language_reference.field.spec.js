/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm language_reference field.
 */
context('Test DynForm language_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_language": {
                "id": "field_language",
                "type": "language_reference",
                "label": "Field OfficialLanguage",
                "description": "Select a language",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "OfficialLanguage form field",
        "type": "",
        "meta": {
            "title": "OfficialLanguage form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field language",
                            "field_id": "field_language",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_language": {
                "id": "field_language",
                "type": "language_reference",
                "label": "Field OfficialLanguage",
                "description": "Select a language",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "OfficialLanguage form field",
        "type": "",
        "meta": {
            "title": "OfficialLanguage form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field language",
                            "field_id": "field_language",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_language": {
            "value": "01FYBRJA021FJXEYTKAE1K37AR",
            "class": "App\\Entity\\OfficialLanguage"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_language": [
            {
                "value": "01FYBRJA021FJXEYTKAE1K37AR",
                "class": "App\\Entity\\OfficialLanguage"
            },
            {
                "value": "01FYBRJA021FJXEYTKAE1K37AW",
                "class": "App\\Entity\\OfficialLanguage"
            },
            {
                "value": "01FYBRJA021FJXEYTKAE1K37AM",
                "class": "App\\Entity\\OfficialLanguage"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get officialLanguages from API.
        cy.intercept('GET', '/api/v1/official_languages?page=2', []);
        cy.intercept('GET', '/api/v1/official_languages?page=1', [{ "id": "01FYBRJA01Z9VD1ACBQ7MCQWEN", "country": "/api/v1/countries/hn", "language": "Language 1" }, { "id": "01FYBRJA021FJXEYTKAE1K37AM", "country": "/api/v1/countries/hn", "language": "Language 2" }, { "id": "01FYBRJA021FJXEYTKAE1K37AP", "country": "/api/v1/countries/hn", "language": "Language 3" }, { "id": "01FYBRJA021FJXEYTKAE1K37AR", "country": "/api/v1/countries/hn", "language": "Language 4" }, { "id": "01FYBRJA021FJXEYTKAE1K37AT", "country": "/api/v1/countries/hn", "language": "Language 5" }, { "id": "01FYBRJA021FJXEYTKAE1K37AW", "country": "/api/v1/countries/hn", "language": "Language 6" }, { "id": "01FYBRJA021FJXEYTKAE1K37AY", "country": "/api/v1/countries/hn", "language": "Language 7" }, { "id": "01FYBRJA021FJXEYTKAE1K37B0", "country": "/api/v1/countries/hn", "language": "Language 8" }, { "id": "01FYBRJA021FJXEYTKAE1K37B2", "country": "/api/v1/countries/hn", "language": "Language 9" }]);
    })    

    it('DynForm Field - simple language_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_language_value").should("be.visible");
        // Change value.
        cy.get("#field_language_value").select('01FYBRJA021FJXEYTKAE1K37B2');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_language": {
                    "value": "01FYBRJA021FJXEYTKAE1K37B2",
                    "class": "App\\Entity\\OfficialLanguage"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_language_value").select('01FYBRJA021FJXEYTKAE1K37AY');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_language":
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37AY",
                        "class": "App\\Entity\\OfficialLanguage"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple language_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_language_value").should("be.visible");
        cy.get("#field_language_value").should("have.value", "01FYBRJA021FJXEYTKAE1K37AR");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_language": {
                    "value": "01FYBRJA021FJXEYTKAE1K37AR",
                    "class": "App\\Entity\\OfficialLanguage"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_language_value").select('01FYBRJA021FJXEYTKAE1K37B2');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_language":
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37B2",
                        "class": "App\\Entity\\OfficialLanguage"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued language_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_language__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_language__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_language__2_value").should("be.visible");

        // Change value.
        cy.get("#field_language__0_value").select('01FYBRJA021FJXEYTKAE1K37B2');
        cy.get("#field_language__1_value").select('01FYBRJA021FJXEYTKAE1K37AY');
        cy.get("#field_language__2_value").select('01FYBRJA021FJXEYTKAE1K37AW');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_language": [
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37B2",
                        "class": "App\\Entity\\OfficialLanguage"
                    },
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37AY",
                        "class": "App\\Entity\\OfficialLanguage"
                    },
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37AW",
                        "class": "App\\Entity\\OfficialLanguage"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_language2-remove").click();
        cy.get('#field_language__2_value').should('not.exist')
        cy.get("#field_language1-remove").click();
        cy.get('#field_language__1_value').should('not.exist')
        cy.get("#field_language0-remove").click();
        cy.get('#field_language__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_language": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued language_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_language__0_value").should("be.visible");
        cy.get("#field_language__0_value").should("have.value", "01FYBRJA021FJXEYTKAE1K37AR");
        cy.get("#field_language__1_value").should("be.visible");
        cy.get("#field_language__1_value").should("have.value", "01FYBRJA021FJXEYTKAE1K37AW");
        cy.get("#field_language__2_value").should("be.visible");
        cy.get("#field_language__2_value").should("have.value", "01FYBRJA021FJXEYTKAE1K37AM");

        // Change value.
        cy.get("#field_language__0_value").select("01FYBRJA021FJXEYTKAE1K37B2");
        cy.get("#field_language__1_value").select("01FYBRJA021FJXEYTKAE1K37AY");
        cy.get("#field_language__2_value").select("01FYBRJA021FJXEYTKAE1K37AW");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_language": [
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37B2",
                        "class": "App\\Entity\\OfficialLanguage"
                    },
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37AY",
                        "class": "App\\Entity\\OfficialLanguage"
                    },
                    {
                        "value": "01FYBRJA021FJXEYTKAE1K37AW",
                        "class": "App\\Entity\\OfficialLanguage"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_language2-remove").click();
        cy.get('#field_language__2_value').should('not.exist')
        cy.get("#field_language1-remove").click();
        cy.get('#field_language__1_value').should('not.exist')
        cy.get("#field_language0-remove").click();
        cy.get('#field_language__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_language": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
