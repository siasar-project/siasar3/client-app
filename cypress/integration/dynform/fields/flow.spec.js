/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm flow field.
 */
context('Test DynForm field flow field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_flow": {
                "id": "field_flow",
                "type": "flow",
                "label": "Field flow",
                "description": "Field flow",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "flow form field",
        "type": "",
        "meta": {
            "title": "flow form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field flow",
                            "field_id": "field_flow",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_flow": {
                "id": "field_flow",
                "type": "flow",
                "label": "Field flow",
                "description": "Field flow",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "flow form field",
        "type": "",
        "meta": {
            "title": "flow form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field flow",
                            "field_id": "field_flow",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_flow": {
            "value": "1234.56",
            "unit": "cubic meter/day"
        }
    }, undefined, 4);

    let fDataMulti = JSON.stringify({
        "field_flow": [
            {
                "value": "12",
                "unit": "cubic meter/day"
            },
            {
                "value": "1234.56",
                "unit": "cubic meter/hour"
            },
            {
                "value": "",
                "unit": "cubic meter/day"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
        cy.intercept('GET', '/api/v1/configuration/system.units.flow', { fixture: 'unit_flow.json' })
    })

    it('Dynform Field - simple flow without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get("#field_flow_unit").should("be.visible");
        cy.get("#field_flow").should("be.visible");
        // Change value.
        cy.get("#field_flow").type('1234.56');
        cy.get("#field_flow_unit").select('cubic meter/second')
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": {
                    "value": "1234.56",
                    "unit": "cubic meter/second"
                }
            }, undefined, 4));
        // Set empty value.
        cy.get("#field_flow").type('{selectAll}{del}');
        cy.get("#field_flow_unit").select('liter/second')
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": {
                    "value": "",
                    "unit": "liter/second"
                }
            }, undefined, 4));
    })

    it('Dynform Field - simple flow with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_flow_unit").should("be.visible");
        cy.get("#field_flow").should("be.visible");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", fDataSingle);

        // Clear data.
        cy.get("#field_flow").type('{selectAll}{del}');
        cy.get("#field_flow_unit").select('liter/second');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": {
                    "value": "",
                    "unit": "liter/second"
                }
            }, undefined, 4));

    })

    it('Dynform Field - multivalued flow without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_flow__0").should("be.visible");
        cy.get("#field_flow__0_unit").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_flow__1").should("be.visible");
        cy.get("#field_flow__1_unit").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_flow__2").should("be.visible");
        cy.get("#field_flow__2_unit").should("be.visible");

        // Change values.
        cy.get("#field_flow__0").type(1234.56)
        cy.get("#field_flow__0_unit").select("cubic meter/second")
        cy.get("#field_flow__1").type(2234.56)
        cy.get("#field_flow__1_unit").select("cubic meter/day")
        cy.get("#field_flow__2").type(3234.56)
        cy.get("#field_flow__2_unit").select("cubic meter/hour")

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": [
                    {
                        "value": "1234.56",
                        "unit": "cubic meter/second"
                    },
                    {
                        "value": "2234.56",
                        "unit": "cubic meter/day"
                    },
                    {
                        "value": "3234.56",
                        "unit": "cubic meter/hour"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_flow2-remove").click();
        cy.get('#field_flow_2').should('not.exist')
        cy.get("#field_flow1-remove").click();
        cy.get('#field_flow_1').should('not.exist')
        cy.get("#field_flow0-remove").click();
        cy.get('#field_flow_0').should('not.exist')

        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": []
            }, undefined, 4));
    })

    it('DynForm Field - multivalued flow with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_flow__0").should("be.visible");
        cy.get("#field_flow__0_unit").should("be.visible");
        cy.get("#field_flow__0").should('have.value', '12');
        cy.get("#field_flow__0_unit").should('have.value', 'cubic meter/day');
        cy.get("#field_flow__1").should("be.visible");
        cy.get("#field_flow__1_unit").should("be.visible");
        cy.get("#field_flow__1").should('have.value', '1234.56');
        cy.get("#field_flow__1_unit").should('have.value', 'cubic meter/hour');
        cy.get("#field_flow__2").should("be.visible");
        cy.get("#field_flow__2_unit").should("be.visible");
        cy.get("#field_flow__2").should('have.value', '');
        cy.get("#field_flow__2_unit").should('have.value', 'cubic meter/day');

        // Change values.
        cy.get("#field_flow__0").type('{selectAll}{del}')
        cy.get("#field_flow__0").type(1234.56)
        cy.get("#field_flow__0_unit").select("liter/second")
        cy.get("#field_flow__1").type('{selectAll}{del}')
        cy.get("#field_flow__2").type(2234.56)

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": [
                    {
                        "value": "1234.56",
                        "unit": "liter/second"
                    },
                    {
                        "value": "",
                        "unit": "cubic meter/hour"
                    },
                    {
                        "value": "2234.56",
                        "unit": "cubic meter/day"
                    }
                ]
            }, undefined, 4))

        // Remove values.
        cy.get("#field_flow2-remove").click();
        cy.get('#field_flow_2').should('not.exist')
        cy.get("#field_flow1-remove").click();
        cy.get('#field_flow_1').should('not.exist')
        cy.get("#field_flow0-remove").click();
        cy.get('#field_flow_0').should('not.exist')

        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_flow": []
            }, undefined, 4));
    })
})
