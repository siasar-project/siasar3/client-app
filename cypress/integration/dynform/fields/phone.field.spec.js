/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm phone field.
 */
context('Test DynForm phone field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_phone": {
                "id": "field_phone",
                "type": "phone",
                "label": "Telephone number",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.3.3"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Phone form field",
        "type": "",
        "meta": {
            "title": "Phone form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field phone",
                            "field_id": "field_phone",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_phone": {
                "id": "field_phone",
                "type": "phone",
                "label": "Telephone number",
                "description": "",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": false,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.3.3"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Phone form field",
        "type": "",
        "meta": {
            "title": "Phone form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field phone",
                            "field_id": "field_phone",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_phone": {
            "value": "+34 666 66 66 66"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_phone": [
            {
                "value": "1"
            },
            {
                "value": "2"
            },
            {
                "value": "3"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple phone without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_phone").should("be.visible");
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": {
                    "value": ""
                }
            }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple phone with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_phone").should("be.visible");
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": {
                    "value": "+34 666 66 66 66"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_phone").clear();
        cy.get("#field_phone").type('+34 777 77 77 77', { force: true });
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": {
                    "value": "+34 777 77 77 77"
                }
            }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued phone without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_phone__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_phone__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_phone__2").should("be.visible");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": [
                    {
                        "value": ""
                    },
                    {
                        "value": ""
                    },
                    {
                        "value": ""
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_phone2-remove").click();
        cy.get('#field_phone__2').should('not.exist')
        cy.get("#field_phone1-remove").click();
        cy.get('#field_phone__1').should('not.exist')
        cy.get("#field_phone0-remove").click();
        cy.get('#field_phone__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued phone with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");
        // Validate.
        cy.get("#field_phone__0").should("be.visible");
        cy.get("#field_phone__0").should('have.value', '1');
        cy.get("#field_phone__1").should("be.visible");
        cy.get("#field_phone__1").should('have.value', '2');
        cy.get("#field_phone__2").should("be.visible");
        cy.get("#field_phone__2").should('have.value', '3');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": [
                    {
                        "value": "1"
                    },
                    {
                        "value": "2"
                    },
                    {
                        "value": "3"
                    }
                ]
            }, undefined, 4));

        // Add values.
        cy.get("#field_phone__0").clear();
        cy.get("#field_phone__0").type('4', { force: true })
        cy.get("#field_phone__1").clear();
        cy.get("#field_phone__1").type('5', { force: true })
        cy.get("#field_phone__2").clear();
        cy.get("#field_phone__2").type('6', { force: true })

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": [
                    {
                        "value": "4"
                    },
                    {
                        "value": "5"
                    },
                    {
                        "value": "6"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_phone2-remove").click();
        cy.get('#field_phone__2').should('not.exist')
        cy.get("#field_phone1-remove").click();
        cy.get('#field_phone__1').should('not.exist')
        cy.get("#field_phone0-remove").click();
        cy.get('#field_phone__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_phone": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
