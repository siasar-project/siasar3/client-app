/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm concentration field.
 */
context('Test DynForm concentration field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_concentration": {
                "id": "field_concentration",
                "type": "concentration",
                "label": "Field concentration",
                "description": "Field concentration",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "concentration form field",
        "type": "",
        "meta": {
            "title": "concentration form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field concentration",
                            "field_id": "field_concentration",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_concentration": {
                "id": "field_concentration",
                "type": "concentration",
                "label": "Field concentration",
                "description": "Field concentration",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "concentration form field",
        "type": "",
        "meta": {
            "title": "concentration form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field concentration",
                            "field_id": "field_concentration",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_concentration": {
            "value": "1234.56",
            "unit": "micrograms/liter"
        }
    }, undefined, 4);

    let fDataMulti = JSON.stringify({
        "field_concentration": [
            {
                "value": "12",
                "unit": "micrograms/liter"
            },
            {
                "value": "1234.56",
                "unit": "milligrams per liter (water at 5ºC)"
            },
            {
                "value": "",
                "unit": "particles per million"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
        cy.intercept('GET', '/api/v1/configuration/system.units.concentration', { fixture: 'unit_concentration.json' })
    })

    it('Dynform Field - simple concentration without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get("#field_concentration_unit").should("be.visible");
        cy.get("#field_concentration").should("be.visible");
        // Change value.
        cy.get("#field_concentration").type('1234.56');
        cy.get("#field_concentration_unit").select('milligrams per liter (water at 5ºC)')
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": {
                    "value": "1234.56",
                    "unit": "milligrams per liter (water at 5ºC)"
                }
            }, undefined, 4));
        // Set empty value.
        cy.get("#field_concentration").type('{selectAll}{del}');
        cy.get("#field_concentration_unit").select('particles per million')
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": {
                    "value": "",
                    "unit": "particles per million"
                }
            }, undefined, 4));
    })

    it('Dynform Field - simple concentration with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_concentration_unit").should("be.visible");
        cy.get("#field_concentration").should("be.visible");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", fDataSingle);

        // Clear data.
        cy.get("#field_concentration").type('{selectAll}{del}');
        cy.get("#field_concentration_unit").select('particles per million');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": {
                    "value": "",
                    "unit": "particles per million"
                }
            }, undefined, 4));

    })

    it('Dynform Field - multivalued concentration without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_concentration__0").should("be.visible");
        cy.get("#field_concentration__0_unit").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_concentration__1").should("be.visible");
        cy.get("#field_concentration__1_unit").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_concentration__2").should("be.visible");
        cy.get("#field_concentration__2_unit").should("be.visible");

        // Change values.
        cy.get("#field_concentration__0").type(1234.56)
        cy.get("#field_concentration__0_unit").select("micrograms/liter")
        cy.get("#field_concentration__1").type(2234.56)
        cy.get("#field_concentration__1_unit").select("milligrams per liter (water at 5ºC)")
        cy.get("#field_concentration__2").type(3234.56)
        cy.get("#field_concentration__2_unit").select("micrograms/liter")

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": [
                    {
                        "value": "1234.56",
                        "unit": "micrograms/liter"
                    },
                    {
                        "value": "2234.56",
                        "unit": "milligrams per liter (water at 5ºC)"
                    },
                    {
                        "value": "3234.56",
                        "unit": "micrograms/liter"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_concentration2-remove").click();
        cy.get('#field_concentration_2').should('not.exist')
        cy.get("#field_concentration1-remove").click();
        cy.get('#field_concentration_1').should('not.exist')
        cy.get("#field_concentration0-remove").click();
        cy.get('#field_concentration_0').should('not.exist')

        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": []
            }, undefined, 4));
    })

    it('DynForm Field - multivalued concentration with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_concentration__0").should("be.visible");
        cy.get("#field_concentration__0_unit").should("be.visible");
        cy.get("#field_concentration__0").should('have.value', '12');
        cy.get("#field_concentration__0_unit").should('have.value', 'micrograms/liter');
        cy.get("#field_concentration__1").should("be.visible");
        cy.get("#field_concentration__1_unit").should("be.visible");
        cy.get("#field_concentration__1").should('have.value', '1234.56');
        cy.get("#field_concentration__1_unit").should('have.value', 'milligrams per liter (water at 5ºC)');
        cy.get("#field_concentration__2").should("be.visible");
        cy.get("#field_concentration__2_unit").should("be.visible");
        cy.get("#field_concentration__2").should('have.value', '');
        cy.get("#field_concentration__2_unit").should('have.value', 'particles per million');

        // Change values.
        cy.get("#field_concentration__0").type('{selectAll}{del}')
        cy.get("#field_concentration__0").type(1234.56)
        cy.get("#field_concentration__0_unit").select("milligrams per liter (water at 5ºC)")
        cy.get("#field_concentration__1_unit").select("micrograms/liter")
        cy.get("#field_concentration__1").type('{selectAll}{del}')
        cy.get("#field_concentration__2").type(2234.56)

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": [
                    {
                        "value": "1234.56",
                        "unit": "milligrams per liter (water at 5ºC)"
                    },
                    {
                        "value": "",
                        "unit": "micrograms/liter"
                    },
                    {
                        "value": "2234.56",
                        "unit": "particles per million"
                    }
                ]
            },  undefined, 4))

        // Remove values.
        cy.get("#field_concentration2-remove").click();
        cy.get('#field_concentration_2').should('not.exist')
        cy.get("#field_concentration1-remove").click();
        cy.get('#field_concentration_1').should('not.exist')
        cy.get("#field_concentration0-remove").click();
        cy.get('#field_concentration_0').should('not.exist')

        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_concentration": []
            }, undefined, 4));
    })
})
