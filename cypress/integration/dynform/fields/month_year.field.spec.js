/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm month year field.
 */
context('DynForm month year field', () => {
  // Form structure.
  let fStructSingle = JSON.stringify({
    "description": "",
    "fields": {
      "field_month_year": {
        "id": "field_month_year",
        "type": "month_year",
        "label": "Month year number",
        "description": "",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": false,
          "multivalued": false,
          "weight": 0,
          "meta": {
            "catalog_id": "0.1"
          },
          "sort": false,
          "filter": false
        }
      }
    },
    "id": "form.sample",
    "requires": [],
    "title": "Month year form field",
    "type": "",
    "meta": {
      "title": "Month year form field",
      "version": "Version 1.0",
      "field_groups": [
        {
          "id": "0",
          "title": "Group 0",
          "children": {
            "0.1": {
              "id": "0.1",
              "title": "Field month year",
              "field_id": "field_month_year",
              "weight": 0
            }
          }
        }
      ]
    }
  }, undefined, 4)
  let fStructMulti = JSON.stringify({
    "description": "",
    "fields": {
      "field_month_year": {
        "id": "field_month_year",
        "type": "month_year",
        "label": "Month year number",
        "description": "",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": false,
          "multivalued": true,
          "weight": 0,
          "meta": {
            "catalog_id": "0.1"
          },
          "sort": false,
          "filter": false
        }
      }
    },
    "id": "form.sample",
    "requires": [],
    "title": "Month year form field",
    "type": "",
    "meta": {
      "title": "Month year form field",
      "version": "Version 1.0",
      "field_groups": [
        {
          "id": "0",
          "title": "Group 0",
          "children": {
            "0.1": {
              "id": "0.1",
              "title": "Field month year",
              "field_id": "field_month_year",
              "weight": 0
            }
          }
        }
      ]
    }
  }, undefined, 4)

  let date = new Date()
  let month = date.getMonth() + 1
  let year = date.getFullYear()
  let fDefaultDataSingle = JSON.stringify({
    "field_month_year": {
      "month": month.toString(),
      "year": year.toString()
    }
  }, undefined, 4)
  let fDataSingle = JSON.stringify({
    "field_month_year": {
      "month": "12",
      "year": "2025"
    }
  }, undefined, 4)
  let fDefaultDataMulti = JSON.stringify({
    "field_month_year": [
      {
        "month": month.toString(),
        "year": year.toString()
      },
      {
        "month": month.toString(),
        "year": year.toString()
      },
      {
        "month": month.toString(),
        "year": year.toString()
      }
    ]
  }, undefined, 4)
  let fDataMulti = JSON.stringify({
    "field_month_year": [
      {
        "month": "10",
        "year": "2025"
      },
      {
        "month": "11",
        "year": "2026"
      },
      {
        "month": "12",
        "year": "2024"
      }
    ]
  }, undefined, 4)

  beforeEach(() => {
    // cy.visit("/")
  })

  it('DynForm Field - simple month_year without data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructSingle);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
    // Validate field existence.
    cy.get("#field_month_year_month").should("be.visible");
    cy.get("#field_month_year_year").should("be.visible");
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data")
      .should("have.text", fDefaultDataSingle);
  })

  it('DynForm Field - simple month_year with data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructSingle);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
    // Validate field existence.
    cy.get("#field_month_year_month").should("be.visible");
    cy.get("#field_month_year_year").should("be.visible");
    // Change values.
    cy.get("#field_month_year_month").select(11);
    cy.get("#field_month_year_year").clear()
    cy.get("#field_month_year_year").type("2025");
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data").should("have.text", fDataSingle);
  })

  it('DynForm Field - multi month_year without data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructMulti);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Validate field existence.
    cy.get(".dynform-add button").should("be.visible");
    // Add fields.
    cy.get(".dynform-add button").click();
    cy.get("#field_month_year__0_month").should("be.visible");
    cy.get("#field_month_year__0_year").should("be.visible");
    cy.get(".dynform-add button").click();
    cy.get("#field_month_year__1_month").should("be.visible");
    cy.get("#field_month_year__1_year").should("be.visible");
    cy.get(".dynform-add button").click();
    cy.get("#field_month_year__2_month").should("be.visible");
    cy.get("#field_month_year__2_year").should("be.visible");
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data").should("have.text", fDefaultDataMulti);
    // Remove values. We must remove from last to first.
    cy.get("#field_month_year2-remove").click();
    cy.get("#field_month_year__2_month").should("not.exist");
    cy.get("#field_month_year__2_year").should("not.exist");
    cy.get("#field_month_year1-remove").click();
    cy.get("#field_month_year__1_month").should("not.exist");
    cy.get("#field_month_year__1_year").should("not.exist");
    cy.get("#field_month_year0-remove").click();
    cy.get("#field_month_year__0_month").should("not.exist");
    cy.get("#field_month_year__0_year").should("not.exist");
    // Get form value.
    cy.get("#test-form-submit").click();
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_month_year": []
        }, undefined, 4));
  })

  it('DynForm Field - multi month_year with data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructMulti);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Set data values in textarea.
    cy.get("#txt_data").invoke('val', fDataMulti);
    cy.get("#txt_data").type('{moveToEnd}{enter}');
    // Validate field existence.
    cy.get(".dynform-add button").should("be.visible");
    // Add fields.
    cy.get("#field_month_year__0_month").should("be.visible");
    cy.get("#field_month_year__0_month").should("have.value", 10);
    cy.get("#field_month_year__0_year").should("be.visible");
    cy.get("#field_month_year__0_year").should("have.value", "2025");

    cy.get("#field_month_year__1_month").should("be.visible");
    cy.get("#field_month_year__1_month").should("have.value", 11);
    cy.get("#field_month_year__1_year").should("be.visible");
    cy.get("#field_month_year__1_year").should("have.value", "2026");

    cy.get("#field_month_year__2_month").should("be.visible");
    cy.get("#field_month_year__2_month").should("have.value", 12);
    cy.get("#field_month_year__2_year").should("be.visible");
    cy.get("#field_month_year__2_year").should("have.value", "2024");
    // Check data value.
    cy.get("#txt_data").should("have.text", fDataMulti);

    // Change values.
    cy.get("#field_month_year__0_month").select("10");
    cy.get("#field_month_year__0_year").clear()
    cy.get("#field_month_year__0_year").type("2026");

    cy.get("#field_month_year__1_month").select("11");
    cy.get("#field_month_year__1_year").clear()
    cy.get("#field_month_year__1_year").type("2027");

    cy.get("#field_month_year__2_month").select("12");
    cy.get("#field_month_year__2_year").clear()
    cy.get("#field_month_year__2_year").type("2025");
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data").should("have.text", JSON.stringify({
      "field_month_year": [
        {
          "month": "10",
          "year": "2026"
        },
        {
          "month": "11",
          "year": "2027"
        },
        {
          "month": "12",
          "year": "2025"
        }
      ]
    }, undefined, 4));
    // Remove values. We must remove from last to first.
    cy.get("#field_month_year2-remove").click();
    cy.get("#field_month_year__2_month").should("not.exist");
    cy.get("#field_month_year__2_year").should("not.exist");
    cy.get("#field_month_year1-remove").click();
    cy.get("#field_month_year__1_month").should("not.exist");
    cy.get("#field_month_year__1_year").should("not.exist");
    cy.get("#field_month_year0-remove").click();
    cy.get("#field_month_year__0_month").should("not.exist");
    cy.get("#field_month_year__0_year").should("not.exist");
    // Get form value.
    cy.get("#test-form-submit").click();
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_month_year": []
        }, undefined, 4));
  })
})
