/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm ethnicity_reference field.
 */
context('Test DynForm ethnicity_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_ethnicity": {
                "id": "field_ethnicity",
                "type": "ethnicity_reference",
                "label": "Field Ethnicity",
                "description": "Select a ethnicity",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Ethnicity form field",
        "type": "",
        "meta": {
            "title": "Ethnicity form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field ethnicity",
                            "field_id": "field_ethnicity",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_ethnicity": {
                "id": "field_ethnicity",
                "type": "ethnicity_reference",
                "label": "Field Ethnicity",
                "description": "Select a ethnicity",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Ethnicity form field",
        "type": "",
        "meta": {
            "title": "Ethnicity form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field ethnicity",
                            "field_id": "field_ethnicity",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_ethnicity": {
            "value": "01G0Y8TVF7SD0C9NVWEHW9S134",
            "class": "App\\Entity\\Ethnicity"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_ethnicity": [
            {
                "value": "01G0Y8TVF7SD0C9NVWEHW9S134",
                "class": "App\\Entity\\Ethnicity"
            },
            {
                "value": "01G0Y8TVF8KVMNCA6KMTDKKZ1Y",
                "class": "App\\Entity\\Ethnicity"
            },
            {
                "value": "01G0Y8TVF7SD0C9NVWEHW9S132",
                "class": "App\\Entity\\Ethnicity"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get ethnicities from API.
        cy.intercept('GET', '/api/v1/ethnicities?page=2', []);
        cy.intercept('GET', '/api/v1/ethnicities?page=1', {fixture: 'ethnicities.json'});
    })    

    it('DynForm Field - simple ethnicity_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_ethnicity_value").should("be.visible");
        // Change value.
        cy.get("#field_ethnicity_value").select('01G0Y8TVF7SD0C9NVWEHW9S136');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_ethnicity": {
                    "value": "01G0Y8TVF7SD0C9NVWEHW9S136",
                    "class": "App\\Entity\\Ethnicity"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_ethnicity_value").select('01G0Y8TVF8KVMNCA6KMTDKKZ1W');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_ethnicity":
                    {
                        "value": "01G0Y8TVF8KVMNCA6KMTDKKZ1W",
                        "class": "App\\Entity\\Ethnicity"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple ethnicity_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_ethnicity_value").should("be.visible");
        cy.get("#field_ethnicity_value").should("have.value", "01G0Y8TVF7SD0C9NVWEHW9S134");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_ethnicity": {
                    "value": "01G0Y8TVF7SD0C9NVWEHW9S134",
                    "class": "App\\Entity\\Ethnicity"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_ethnicity_value").select('01G0Y8TVF8KVMNCA6KMTDKKZ1W');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_ethnicity":
                    {
                        "value": "01G0Y8TVF8KVMNCA6KMTDKKZ1W",
                        "class": "App\\Entity\\Ethnicity"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued ethnicity_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_ethnicity__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_ethnicity__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_ethnicity__2_value").should("be.visible");

        // Change value.
        cy.get("#field_ethnicity__0_value").select('01G0Y8TVF7SD0C9NVWEHW9S138');
        cy.get("#field_ethnicity__1_value").select('01G0Y8TVF8KVMNCA6KMTDKKZ1W');
        cy.get("#field_ethnicity__2_value").select('01G0Y8TVF7SD0C9NVWEHW9S136');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_ethnicity": [
                    {
                        "value": "01G0Y8TVF7SD0C9NVWEHW9S138",
                        "class": "App\\Entity\\Ethnicity"
                    },
                    {
                        "value": "01G0Y8TVF8KVMNCA6KMTDKKZ1W",
                        "class": "App\\Entity\\Ethnicity"
                    },
                    {
                        "value": "01G0Y8TVF7SD0C9NVWEHW9S136",
                        "class": "App\\Entity\\Ethnicity"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_ethnicity2-remove").click();
        cy.get('#field_ethnicity__2_value').should('not.exist')
        cy.get("#field_ethnicity1-remove").click();
        cy.get('#field_ethnicity__1_value').should('not.exist')
        cy.get("#field_ethnicity0-remove").click();
        cy.get('#field_ethnicity__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_ethnicity": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued ethnicity_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_ethnicity__0_value").should("be.visible");
        cy.get("#field_ethnicity__0_value").should("have.value", "01G0Y8TVF7SD0C9NVWEHW9S134");
        cy.get("#field_ethnicity__1_value").should("be.visible");
        cy.get("#field_ethnicity__1_value").should("have.value", "01G0Y8TVF8KVMNCA6KMTDKKZ1Y");
        cy.get("#field_ethnicity__2_value").should("be.visible");
        cy.get("#field_ethnicity__2_value").should("have.value", "01G0Y8TVF7SD0C9NVWEHW9S132");

        // Change value.
        cy.get("#field_ethnicity__0_value").select("01G0Y8TVF7SD0C9NVWEHW9S138");
        cy.get("#field_ethnicity__1_value").select("01G0Y8TVF8KVMNCA6KMTDKKZ1W");
        cy.get("#field_ethnicity__2_value").select("01G0Y8TVF7SD0C9NVWEHW9S136");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_ethnicity": [
                    {
                        "value": "01G0Y8TVF7SD0C9NVWEHW9S138",
                        "class": "App\\Entity\\Ethnicity"
                    },
                    {
                        "value": "01G0Y8TVF8KVMNCA6KMTDKKZ1W",
                        "class": "App\\Entity\\Ethnicity"
                    },
                    {
                        "value": "01G0Y8TVF7SD0C9NVWEHW9S136",
                        "class": "App\\Entity\\Ethnicity"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_ethnicity2-remove").click();
        cy.get('#field_ethnicity__2_value').should('not.exist')
        cy.get("#field_ethnicity1-remove").click();
        cy.get('#field_ethnicity__1_value').should('not.exist')
        cy.get("#field_ethnicity0-remove").click();
        cy.get('#field_ethnicity__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_ethnicity": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
