/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm image_reference field.
 */
context('Test DynForm image_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_image": {
                "id": "field_image",
                "type": "image_reference",
                "label": "Field Image",
                "description": "Select a image",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "allow_extension": ["jpg", "jpeg", "png"]
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Image form field",
        "type": "",
        "meta": {
            "title": "Image form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field image",
                            "field_id": "field_image",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_image": {
                "id": "field_image",
                "type": "image_reference",
                "label": "Field Image",
                "description": "Select a image",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false,
                    "allow_extension": ["jpg", "jpeg", "png"]
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Image form field",
        "type": "",
        "meta": {
            "title": "Image form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field image",
                            "field_id": "field_image",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_image": {
            "value": "01G0Y8VGRPJ5TTAZ4F27WKC8QC",
            "class": "App\\Entity\\File",
            "filename": "hn.png"
        }
    } , undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_image": [
            {
                "value": "01G0Y8VGRPJ5TTAZ4F27WKC8QC",
                "class": "App\\Entity\\File",
                "filename": "hn.png"
            },
            {
                "value": "01G0Y8VGRRD9ANV0DMYXD7PGR6",
                "class": "App\\Entity\\File",
                "filename": "hn.png"
            },
            {
                "value": "01G0Y8VGPR8Y2W101E8V1S7SJ9",
                "class": "App\\Entity\\File",
                "filename": "hn.png"
            }
        ]
    }, undefined, 4);

    let countryCode = 'hn';
    let fileId = '12345678901234567890123456';
    let fileId2 = '12345678901234567890123457';
    let fileId3 = '12345678901234567890123458';
    let fileName = 'especias.png';
    let fileName2 = 'fondo.jpg';
    let fileName3 = 'chocolate.jpeg';

    let fDataSingleUpload = JSON.stringify({
        "field_image": {
            "value": fileId,
            "class": "App\\Entity\\File",
            "filename": fileName
        }
    }, undefined, 4);
    let fDataMultiUpload = JSON.stringify({
        "field_image": [
            {
                "value": fileId,
                "class": "App\\Entity\\File",
                "filename": fileName
            },
            {
                "value": fileId2,
                "class": "App\\Entity\\File",
                "filename": fileName2
            },
            {
                "value": fileId3,
                "class": "App\\Entity\\File",
                "filename": fileName3
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        window.localStorage.setItem("session", '{"country": {"code": "'+countryCode+'"}}');

        cy.intercept('GET', '/api/v1/files/'+fileId+'/download', {fixture: 'img/'+fileName})
            .as("loadApiV1Files_"+fileId+"_download");
        cy.intercept('POST', '/api/v1/files', {id: fileId, fileName: fileName})
            .as("loadApiV1Files");
        cy.intercept('DELETE', '/api/v1/files/*', '')
            .as("loadApiV1FilesAll");
    })

    it('DynForm Field - simple image_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate filename text not exist.
        cy.get("#field_image-0").should("not.exist");
        // Validate button add existence.
        cy.get("#field_image-add").should("be.visible");

        // Add file value.
        cy.get("#field_image-add").selectFile('cypress/fixtures/img/'+fileName);
        cy.wait("@loadApiV1Files_"+fileId+"_download");
        // Check filename value.
        cy.get("#field_image-0").should("have.attr", "alt", fileName);
        // Validate add button not exist.
        cy.get("#field_image-add").should("not.exist");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text",fDataSingleUpload);
        // Check filename value.
        cy.get("#field_image-0").should("have.attr", "alt", fileName);
        // Validate add button not exist.
        cy.get("#field_image-add").should("not.exist");
        
        // Remove file values.
        cy.get("#field_image0-remove").click();
        // Validate filename text not exist.
        cy.get("#field_image-0").should("not.exist");
        // Validate add button existence.
        cy.get("#field_image-add").should("be.visible");

        // // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple image_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);

        cy.intercept('GET', '/api/v1/files/01G0Y8VGRPJ5TTAZ4F27WKC8QC/download', {
            "id": "01G0Y8VGRPJ5TTAZ4F27WKC8QC",
            "fileName": "hn.png",
            "country": "/api/v1/countries/hn",
            "created": "2022-04-18T12:02:45+00:00"
        })
            .as("loadApiV1Files_01G0Y8VGRPJ5TTAZ4F27WKC8QC_download");

        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        cy.wait("@loadApiV1Files_01G0Y8VGRPJ5TTAZ4F27WKC8QC_download")

        // Check filename value.
        cy.get("#field_image-0").should("have.attr", "alt", 'hn.png');
        // Validate add button not exist.
        cy.get("#field_image-add").should("not.exist");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data").should("have.text", fDataSingle);

        // Remove file values.
        cy.get("#field_image0-remove").click();
        // Add file value.
        cy.get("#field_image-add").selectFile('cypress/fixtures/img/'+fileName);

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text",fDataSingleUpload);

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued image_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate filename text not exist.
        cy.get("#field_image-0").should("not.exist");
        // Validate button add existence.
        cy.get("#field_image-add").should("be.visible");

        // Add values.
        // Workaround, overwrite cy.intercept('POST', '/api/v1/files ...) with given file id and name. 
        cy.get("#field_image-add").selectFile('cypress/fixtures/img/'+fileName);
        cy.intercept('POST', '/api/v1/files', {id: fileId, fileName: fileName}).as("id_"+fileId);
        cy.wait("@id_"+fileId);
        // Workaround, overwrite cy.intercept('POST', '/api/v1/files ...) with given file id and name. 
        cy.get("#field_image-add").selectFile('cypress/fixtures/img/'+fileName2);
        cy.intercept('POST', '/api/v1/files', {id: fileId2, fileName: fileName2}).as("id_"+fileId2);
        cy.wait("@id_"+fileId2);
        // Workaround, overwrite cy.intercept('POST', '/api/v1/files ...) with given file id and name. 
        cy.get("#field_image-add").selectFile('cypress/fixtures/img/'+fileName3);
        cy.intercept('POST', '/api/v1/files', {id: fileId3, fileName: fileName3}).as("id_"+fileId3);
        cy.wait("@id_"+fileId3);

        // Check filename value.
        cy.get("#field_image-0").should("have.attr", "alt", fileName);
        cy.get("#field_image-1").should("have.attr", "alt", fileName2);
        cy.get("#field_image-2").should("have.attr", "alt", fileName3);
        // Validate add button existence.
        cy.get("#field_image-add").should("be.visible");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text", fDataMultiUpload);
        // Check filename value.
        cy.get("#field_image-0").should("have.attr", "alt", fileName);
        cy.get("#field_image-1").should("have.attr", "alt", fileName2);
        cy.get("#field_image-2").should("have.attr", "alt", fileName3);
        // Validate add button existence.
        cy.get("#field_image-add").should("be.visible");

        // Remove file values and Validate filename text not exist.
        cy.get("#field_image2-remove").click();
        cy.get("#field_image-2").should("not.exist");
        cy.get("#field_image1-remove").click();
        cy.get("#field_image-1").should("not.exist");
        cy.get("#field_image0-remove").click();
        cy.get("#field_image-0").should("not.exist");
        
        // Validate add button existence.
        cy.get("#field_image-add").should("be.visible");
        
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued image_reference with data', () => {
          // Visit test page.
          cy.visit("dynform_field_test");
          // Set configuration values in textarea.
          cy.get("#txt_structure").invoke('val', fStructMulti);
          // Generated change event.
          cy.get("#txt_structure").type('{moveToEnd}{enter}');
  
          // Set data values in textarea.
          cy.get("#txt_data").invoke('val', fDataMulti);
          // Generated change event.
          cy.get("#txt_data").type('{moveToEnd}{enter}');
  
          // Check filename value.
          cy.get("#field_image-0").should("have.attr", "alt", "hn.png");
          cy.get("#field_image-1").should("have.attr", "alt", "hn.png");
          cy.get("#field_image-2").should("have.attr", "alt", "hn.png");
          // Validate add button not exist.
          cy.get("#field_image-add").should("be.visible");
  
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data").should("have.text", fDataMulti);
        // Check filename value.
        cy.get("#field_image-0").should("have.attr", "alt", "hn.png");
        cy.get("#field_image-1").should("have.attr", "alt", "hn.png");
        cy.get("#field_image-2").should("have.attr", "alt", "hn.png");
        // Validate add button existence.
        cy.get("#field_image-add").should("be.visible");

        // Remove file values and Validate filename text not exist.
        cy.get("#field_image2-remove").click();
        cy.get("#field_image-2").should("not.exist");
        cy.get("#field_image1-remove").click();
        cy.get("#field_image-1").should("not.exist");
        cy.get("#field_image0-remove").click();
        cy.get("#field_image-0").should("not.exist");
        
        // Validate add button existence.
        cy.get("#field_image-add").should("be.visible");
        
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
