/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm intervention_status_reference field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_intervention_status_reference": {
                "id": "field_intervention_status_reference",
                "type": "intervention_status_reference",
                "label": "Field intervention_status_reference",
                "description": "Field intervention_status_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "intervention_status_reference form field",
        "type": "",
        "meta": {
            "title": "intervention_status_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field intervention_status_reference",
                            "field_id": "field_intervention_status_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_intervention_status_reference": {
                "id": "field_intervention_status_reference",
                "type": "intervention_status_reference",
                "label": "Field intervention_status_reference",
                "description": "Field intervention_status_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "intervention_status_reference form field",
        "type": "",
        "meta": {
            "title": "intervention_status_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field intervention_status_reference",
                            "field_id": "field_intervention_status_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_intervention_status_reference": {
            "value": "01G4EDNNGS0HDVB265DJK76470",
            "class": "App\\Entity\\InterventionStatus"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_intervention_status_reference": [
            {
                "value": "01G4EDNNGS0HDVB265DJK76470",
                "class": "App\\Entity\\InterventionStatus"
            },
            {
                "value": "01G4EDNNGS0HDVB265DJK4X0B0",
                "class": "App\\Entity\\InterventionStatus"
            },
            {
                "value": "01G4EDP25FGHNA851NK2046DG9",
                "class": "App\\Entity\\InterventionStatus"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/intervention_statuses?page=2', []);
        cy.intercept('GET', '/api/v1/intervention_statuses?page=1', {fixture: 'intervention_statuses.json'});
    })

    it('DynForm Field - simple intervention_status_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-intervention_status_reference").should("be.visible");
        // Change value.
        cy.get(".form-field-intervention_status_reference [type='radio']").check('01G4EDP25FGHNA851NK20HRZDG');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_intervention_status_reference": {
                        "value": "01G4EDP25FGHNA851NK20HRZDG",
                        "class": "App\\Entity\\InterventionStatus"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple intervention_status_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-intervention_status_reference").should("be.visible");
        cy.get(".form-field-intervention_status_reference :checked").should("be.checked").and('have.value', '01G4EDNNGS0HDVB265DJK76470');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_intervention_status_reference": {
                        "value": "01G4EDNNGS0HDVB265DJK76470",
                        "class": "App\\Entity\\InterventionStatus"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-intervention_status_reference [type='radio']").check('01G4EDP25FGHNA851NK20HRZDG');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_intervention_status_reference": {
                        "value": "01G4EDP25FGHNA851NK20HRZDG",
                        "class": "App\\Entity\\InterventionStatus"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued intervention_status_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNCTWEBY9RK21Q5FWXYEH]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNNGS0HDVB265DJK4X0B0]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDP25FGHNA851NK20HRZDG]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNCTWEBY9RK21Q5FW345H]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNNGS0HDVB265DJK76470]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDP25FGHNA851NK2046DG9]').should("be.visible");

        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4EDNCTWEBY9RK21Q5FWXYEH');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4EDP25FGHNA851NK20HRZDG');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4EDP25FGHNA851NK2046DG9');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_intervention_status_reference": [
                        {
                            "value": "01G4EDNCTWEBY9RK21Q5FWXYEH",
                            "class": "App\\Entity\\InterventionStatus"
                        },
                        {
                            "value": "01G4EDP25FGHNA851NK20HRZDG",
                            "class": "App\\Entity\\InterventionStatus"
                        },
                        {
                            "value": "01G4EDP25FGHNA851NK2046DG9",
                            "class": "App\\Entity\\InterventionStatus"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued intervention_status_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNCTWEBY9RK21Q5FWXYEH]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNCTWEBY9RK21Q5FWXYEH]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNNGS0HDVB265DJK4X0B0]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNNGS0HDVB265DJK4X0B0]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDP25FGHNA851NK20HRZDG]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDP25FGHNA851NK20HRZDG]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNCTWEBY9RK21Q5FW345H]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNCTWEBY9RK21Q5FW345H]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNNGS0HDVB265DJK76470]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDNNGS0HDVB265DJK76470]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDP25FGHNA851NK2046DG9]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4EDP25FGHNA851NK2046DG9]').should('be.checked');
        
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4EDNCTWEBY9RK21Q5FWXYEH');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4EDNNGS0HDVB265DJK4X0B0');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4EDP25FGHNA851NK20HRZDG');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4EDNCTWEBY9RK21Q5FW345H');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4EDNNGS0HDVB265DJK76470');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4EDP25FGHNA851NK2046DG9');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_intervention_status_reference": [
                        {
                            "value": '01G4EDNCTWEBY9RK21Q5FWXYEH',
                            "class": "App\\Entity\\InterventionStatus"
                        },
                        {
                            "value": "01G4EDP25FGHNA851NK20HRZDG",
                            "class": "App\\Entity\\InterventionStatus"
                        },
                        {
                            "value": "01G4EDNCTWEBY9RK21Q5FW345H",
                            "class": "App\\Entity\\InterventionStatus"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
