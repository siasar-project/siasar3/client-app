/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm short_text field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_short_text": {
                "id": "field_short_text",
                "type": "short_text",
                "label": "Field short_text",
                "description": "Field short_text",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "short_text form field",
        "type": "",
        "meta": {
            "title": "short_text form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field short_text",
                            "field_id": "field_short_text",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_short_text": {
                "id": "field_short_text",
                "type": "short_text",
                "label": "Field short_text",
                "description": "Field short_text",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "short_text form field",
        "type": "",
        "meta": {
            "title": "short_text form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field short_text",
                            "field_id": "field_short_text",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
            "field_short_text": {
                "value": "foo bar baz."
            }
        }, undefined, 4);
    let fDataMulti = JSON.stringify({
            "field_short_text": [
            {
                "value": ""
            },
            {
                "value": "foo bar baz."
            },
            {
                "value": ""
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // cy.visit("/")
    })

    it('DynForm Field - simple short_text without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_short_text").should("be.visible");
        // Change value.
        cy.get("#field_short_text").type('foo bar baz.');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_short_text": {
                        "value": "foo bar baz."
                    }
                }, undefined, 4));
        // Set empty value.
        cy.get("#field_short_text").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_short_text": {
                        "value": ""
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple short_text with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_short_text").should("be.visible");
        cy.get("#field_short_text").should('have.value', 'foo bar baz.');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_short_text": {
                        "value": "foo bar baz."
                    }
                }, undefined, 4));

        // Change value.
        cy.get("#field_short_text").type('{selectAll}{del}');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_short_text": {
                    "value": ""
                }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued short_text without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_short_text__0").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_short_text__1").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_short_text__2").should("be.visible");

        // Change value.
        cy.get("#field_short_text__0").type('foo bar baz.');
        cy.get("#field_short_text__1").type('foo bar baz.');
        cy.get("#field_short_text__2").type('foo bar baz.');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_short_text": [
                        {
                            "value": "foo bar baz."
                        },
                        {
                            "value": "foo bar baz."
                        },
                        {
                            "value": "foo bar baz."
                        }
                    ]
                }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_short_text2-remove").click();
        cy.get('#field_short_text__2').should('not.exist')
        cy.get("#field_short_text1-remove").click();
        cy.get('#field_short_text__1').should('not.exist')
        cy.get("#field_short_text0-remove").click();
        cy.get('#field_short_text__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_short_text": []
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued short_text with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_short_text__0").should("be.visible");
        cy.get("#field_short_text__0").should('have.value', '');
        cy.get("#field_short_text__1").should("be.visible");
        cy.get("#field_short_text__1").should('have.value', 'foo bar baz.');
        cy.get("#field_short_text__2").should("be.visible");
        cy.get("#field_short_text__2").should('have.value', '');

        // Change value.
        cy.get("#field_short_text__0").type('foo bar baz.');
        cy.get("#field_short_text__1").type('{selectAll}{del}');
        cy.get("#field_short_text__2").type('foo bar baz.');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_short_text": [
                    {
                        "value": "foo bar baz."
                    },
                    {
                        "value": ""
                    },
                    {
                        "value": "foo bar baz."
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_short_text2-remove").click();
        cy.get('#field_short_text__2').should('not.exist')
        cy.get("#field_short_text1-remove").click();
        cy.get('#field_short_text__1').should('not.exist')
        cy.get("#field_short_text0-remove").click();
        cy.get('#field_short_text__0').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_short_text": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
