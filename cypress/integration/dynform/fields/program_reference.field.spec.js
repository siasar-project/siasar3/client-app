/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm program_reference field.
 */
context('Test DynForm program_reference field.', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_program": {
                "id": "field_program",
                "type": "program_reference",
                "label": "Field Program",
                "description": "Select a Program",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Program form field",
        "type": "",
        "meta": {
            "title": "Program form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field program",
                            "field_id": "field_program",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_program": {
                "id": "field_program",
                "type": "program_reference",
                "label": "Field Programs",
                "description": "Select a program",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "Programs form field",
        "type": "",
        "meta": {
            "title": "Programs form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field program",
                            "field_id": "field_program",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_program": {
            "value": "01G0Y8TVG594FAM1XSYGM805YK",
            "class": "App\\Entity\\ProgramIntervention"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_program": [
            {
                "value": "01G0Y8TVG594FAM1XSYGM805YK",
                "class": "App\\Entity\\ProgramIntervention"
            },
            {
                "value": "01G0Y8TVG60ETS4NR4Y0F8RV7J",
                "class": "App\\Entity\\ProgramIntervention"
            },
            {
                "value": "01G0Y8TVG60ETS4NR4Y0F8RV7G",
                "class": "App\\Entity\\ProgramIntervention"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get programs_interventions from API.
        cy.intercept('GET', '/api/v1/program_interventions?page=2', [])
            .as("endProgramLoad");
        cy.intercept('GET', '/api/v1/program_interventions?page=1', {fixture: 'program_interventions.json'})
            .as("page1ProgramLoad");
    })
    
    it('DynForm Field - simple program_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get("#field_program_value").should("be.visible");
        // Change value.
        cy.get("#field_program_value").select('01G0Y8TVG594FAM1XSYGM805YQ');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_program": {
                    "value": "01G0Y8TVG594FAM1XSYGM805YQ",
                    "class": "App\\Entity\\ProgramIntervention"
                }
            }, undefined, 4));
        // Change value.
        cy.get("#field_program_value").select('01G0Y8TVG594FAM1XSYGM805YN');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_program":
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YN",
                        "class": "App\\Entity\\ProgramIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple program_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get("#field_program_value").should("be.visible");
        cy.get("#field_program_value").should("have.value", "01G0Y8TVG594FAM1XSYGM805YK");

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text",JSON.stringify({
                "field_program": {
                    "value": "01G0Y8TVG594FAM1XSYGM805YK",
                    "class": "App\\Entity\\ProgramIntervention"
                }
            }, undefined, 4));

        // Change value.
        cy.get("#field_program_value").select('01G0Y8TVG594FAM1XSYGM805YN');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_program":
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YN",
                        "class": "App\\Entity\\ProgramIntervention"
                    }
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued program_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Add values.
        cy.get(".dynform-add button").click();
        cy.get("#field_program__0_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_program__1_value").should("be.visible");
        cy.get(".dynform-add button").click();
        cy.get("#field_program__2_value").should("be.visible");

        // Change value.
        cy.get("#field_program__0_value").select('01G0Y8TVG594FAM1XSYGM805YQ');
        cy.get("#field_program__1_value").select('01G0Y8TVG594FAM1XSYGM805YN');
        cy.get("#field_program__2_value").select('01G0Y8TVG594FAM1XSYGM805YH');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_program": [
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YQ",
                        "class": "App\\Entity\\ProgramIntervention"
                    },
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YN",
                        "class": "App\\Entity\\ProgramIntervention"
                    },
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YH",
                        "class": "App\\Entity\\ProgramIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_program2-remove").click();
        cy.get('#field_program__2_value').should('not.exist')
        cy.get("#field_program1-remove").click();
        cy.get('#field_program__1_value').should('not.exist')
        cy.get("#field_program0-remove").click();
        cy.get('#field_program__0_value').should('not.exist')
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_program": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued program_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        cy.wait("@page1ProgramLoad");
        cy.wait("@endProgramLoad");

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".dynform-add button").should("be.visible");

        // Validate values.
        cy.get("#field_program__0_value").should("be.visible");
        cy.get("#field_program__0_value").should("have.value", "01G0Y8TVG594FAM1XSYGM805YK");
        cy.get("#field_program__1_value").should("be.visible");
        cy.get("#field_program__1_value").should("have.value", "01G0Y8TVG60ETS4NR4Y0F8RV7J");
        cy.get("#field_program__2_value").should("be.visible");
        cy.get("#field_program__2_value").should("have.value", "01G0Y8TVG60ETS4NR4Y0F8RV7G");

        // Change value.
        cy.get("#field_program__0_value").select("01G0Y8TVG594FAM1XSYGM805YQ");
        cy.get("#field_program__1_value").select("01G0Y8TVG594FAM1XSYGM805YN");
        cy.get("#field_program__2_value").select("01G0Y8TVG594FAM1XSYGM805YH");

        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_program": [
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YQ",
                        "class": "App\\Entity\\ProgramIntervention"
                    },
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YN",
                        "class": "App\\Entity\\ProgramIntervention"
                    },
                    {
                        "value": "01G0Y8TVG594FAM1XSYGM805YH",
                        "class": "App\\Entity\\ProgramIntervention"
                    }
                ]
            }, undefined, 4));

        // Remove values. We must remove from last to first.
        cy.get("#field_program2-remove").click();
        cy.get('#field_program__2_value').should("not.exist")
        cy.get("#field_program1-remove").click();
        cy.get('#field_program__1_value').should("not.exist")
        cy.get("#field_program0-remove").click();
        cy.get('#field_program__0_value').should("not.exist")
        // Get form value.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                "field_program": []
            }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
