/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */
/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm publishing_status field.
 */
context('Test DynForm publishing_status field.', () => {
  // Form structure.
  let fStructSingle = JSON.stringify({
    "description": "",
    "fields": {
      "field_publishing_status": {
        "id": "field_publishing_status",
        "type": "publishing_status",
        "label": "Field publishing_status",
        "description": "Field publishing_status",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": true,
          "multivalued": false,
          "weight": 0,
          "meta": {
            "catalog_id": "0.1"
          },
          "sort": false,
          "filter": false
        }
      }
    },
    "id": "form.sample",
    "requires": [],
    "title": "publishing_status form field",
    "type": "",
    "meta": {
      "title": "publishing_status form field",
      "version": "Version 1.0",
      "field_groups": [
        {
          "id": "0",
          "title": "Group 0",
          "children": {
            "0.1": {
              "id": "0.1",
              "title": "Field publishing_status",
              "field_id": "field_publishing_status",
              "weight": 0
            }
          }
        }
      ]
    }
  }, undefined, 4);

  let fStructMulti = JSON.stringify({
    "description": "",
    "fields": {
      "field_publishing_status": {
        "id": "field_publishing_status",
        "type": "publishing_status",
        "label": "Field publishing_status",
        "description": "Field publishing_status",
        "indexable": false,
        "internal": false,
        "deprecated": false,
        "settings": {
          "required": true,
          "multivalued": true,
          "weight": 0,
          "meta": {
            "catalog_id": "0.1"
          },
          "sort": false,
          "filter": false
        }
      }
    },
    "id": "form.sample",
    "requires": [],
    "title": "publishing_status form field",
    "type": "",
    "meta": {
      "title": "publishing_status form field",
      "version": "Version 1.0",
      "field_groups": [
        {
          "id": "0",
          "title": "Group 0",
          "children": {
            "0.1": {
              "id": "0.1",
              "title": "Field publishing_status",
              "field_id": "field_publishing_status",
              "weight": 0
            }
          }
        }
      ]
    }
  }, undefined, 4);

  let fDataSingle = JSON.stringify({
    "field_publishing_status": {
      "value": "published"
    }
  }, undefined, 4);

  let fDataMulti = JSON.stringify({
    "field_publishing_status": [
      {
        "value": "published"
      },
      {
        "value": "draft"
      },
      {
        "value": "rejected"
      }
    ]
  }, undefined, 4);

  beforeEach(() => {
    // cy.visit("/")
  })

  it('DynForm Field - simple publishing_status without data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructSingle);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
    // Validate field existence.
    cy.get("#field_publishing_status").should("be.visible");
    // Change value.
    cy.get("#field_publishing_status").select('reviewed');
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": {
            "value": "reviewed"
          }
        }, undefined, 4));
  })

  it('DynForm Field - simple publishing_status with data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructSingle);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');

    // Set data values in textarea.
    cy.get("#txt_data").invoke('val', fDataSingle);
    // Generated change event.
    cy.get("#txt_data").type('{moveToEnd}{enter}');

    // Validate field existence.
    cy.get("#field_publishing_status").should("be.visible");
    cy.get("#field_publishing_status").should("have.value", "published");

    // Validate data value in field.
    cy.get("#test-form-submit").click();
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": {
            "value": "published"
          }
        }, undefined, 4));

    // Change value.
    cy.get("#field_publishing_status").select('reviewed');
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": {
            "value": "reviewed"
          }
        }, undefined, 4));

    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - multivalued publishing_status without data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructMulti);
    // Generated change event.
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Validate field existence.
    cy.get(".dynform-add button").should("be.visible");

    // Add values.
    cy.get(".dynform-add button").click();
    cy.get("#field_publishing_status__0").should("be.visible");
    cy.get(".dynform-add button").click();
    cy.get("#field_publishing_status__1").should("be.visible");
    cy.get(".dynform-add button").click();
    cy.get("#field_publishing_status__2").should("be.visible");

    // Change value.
    cy.get("#field_publishing_status__0").select('published');
    cy.get("#field_publishing_status__1").select('draft');
    cy.get("#field_publishing_status__2").select('rejected');
    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": [
            {
              "value": "published"
            },
            {
              "value": "draft"
            },
            {
              "value": "rejected"
            }
          ]
        }, undefined, 4));

    // Remove values. We must remove from last to first.
    cy.get("#field_publishing_status2-remove").click();
    cy.get('#field_publishing_status__2').should('not.exist')
    cy.get("#field_publishing_status1-remove").click();
    cy.get('#field_publishing_status__1').should('not.exist')
    cy.get("#field_publishing_status0-remove").click();
    cy.get('#field_publishing_status__0').should('not.exist')
    // Get form value.
    cy.get("#test-form-submit").click();
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": []
        }, undefined, 4));

    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
  })

  it('DynForm Field - multivalued publishing_status with data', () => {
    // Visit test page.
    cy.visit("dynform_field_test");
    // Set configuration values in textarea.
    cy.get("#txt_structure").invoke('val', fStructMulti);
    cy.get("#txt_structure").type('{moveToEnd}{enter}');
    // Set data values in textarea.
    cy.get("#txt_data").invoke('val', fDataMulti);
    cy.get("#txt_data").type('{moveToEnd}{enter}');
    // Validate field existence.
    cy.get(".dynform-add button").should("be.visible");

    // Validate values.
    cy.get("#field_publishing_status__0").should("be.visible");
    cy.get("#field_publishing_status__0").should("have.value", "published");
    cy.get("#field_publishing_status__1").should("be.visible");
    cy.get("#field_publishing_status__1").should("have.value", "draft");
    cy.get("#field_publishing_status__2").should("be.visible");
    cy.get("#field_publishing_status__2").should("have.value", "rejected");

    // Change value.
    cy.get("#field_publishing_status__0").select('rejected');
    cy.get("#field_publishing_status__1").select('reviewed');
    cy.get("#field_publishing_status__2").select('draft');

    // Submit form.
    cy.get("#test-form-submit").click();
    // Check data value.
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": [
            {
              "value": "rejected"
            },
            {
              "value": "reviewed"
            },
            {
              "value": "draft"
            }
          ]
        }, undefined, 4));

    // Remove values. We must remove from last to first.
    cy.get("#field_publishing_status2-remove").click();
    cy.get('#field_publishing_status__2').should('not.exist')
    cy.get("#field_publishing_status1-remove").click();
    cy.get('#field_publishing_status__1').should('not.exist')
    cy.get("#field_publishing_status0-remove").click();
    cy.get('#field_publishing_status__0').should('not.exist')
    // Get form value.
    cy.get("#test-form-submit").click();
    cy.get("#txt_data")
        .should("have.text", JSON.stringify({
          "field_publishing_status": []
        }, undefined, 4));

    // Scroll to form.
    cy.get(".form-sample").scrollIntoView();
  })

})
