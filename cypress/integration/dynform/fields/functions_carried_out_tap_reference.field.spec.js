// /// <reference types="cypress" />
// Cypress.on('uncaught:exception', (err, runnable) => {
//   // returning false here prevents Cypress from
//   // failing the test
//   return false
// })

// /**
// * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
// */

// /**
// * Test DynForm functions_carried_out_tap_reference field.
// */
// context('Window', () => {
//   // Form structure.
//   let fStructSingle = JSON.stringify({
//       "description": "",
//       "fields": {
//           "field_functions_carried_out_tap_reference": {
//               "id": "field_functions_carried_out_tap_reference",
//               "type": "functions_carried_out_tap_reference",
//               "label": "Field functions_carried_out_tap_reference",
//               "description": "Field functions_carried_out_tap_reference",
//               "indexable": false,
//               "internal": false,
//               "deprecated": false,
//               "settings": {
//                   "required": true,
//                   "multivalued": false,
//                   "weight": 0,
//                   "meta": {
//                       "catalog_id": "0.1"
//                   },
//                   "sort": false,
//                   "filter": false
//               }
//           }
//       },
//       "id": "form.sample",
//       "requires": [],
//       "title": "functions_carried_out_tap_reference form field",
//       "type": "",
//       "meta": {
//           "title": "functions_carried_out_tap_reference form field",
//           "version": "Version 1.0",
//           "field_groups": [
//               {
//                   "id": "0",
//                   "title": "Group 0",
//                   "children": {
//                       "0.1": {
//                           "id": "0.1",
//                           "title": "Field functions_carried_out_tap_reference",
//                           "field_id": "field_functions_carried_out_tap_reference",
//                           "weight": 0
//                       }
//                   }
//               }
//           ]
//       }
//   }, undefined, 4);
//   let fStructMulti = JSON.stringify({
//       "description": "",
//       "fields": {
//           "field_functions_carried_out_tap_reference": {
//               "id": "field_functions_carried_out_tap_reference",
//               "type": "functions_carried_out_tap_reference",
//               "label": "Field functions_carried_out_tap_reference",
//               "description": "Field functions_carried_out_tap_reference",
//               "indexable": false,
//               "internal": false,
//               "deprecated": false,
//               "settings": {
//                   "required": true,
//                   "multivalued": true,
//                   "weight": 0,
//                   "meta": {
//                       "catalog_id": "0.1"
//                   },
//                   "sort": false,
//                   "filter": false
//               }
//           }
//       },
//       "id": "form.sample",
//       "requires": [],
//       "title": "functions_carried_out_tap_reference form field",
//       "type": "",
//       "meta": {
//           "title": "functions_carried_out_tap_reference form field",
//           "version": "Version 1.0",
//           "field_groups": [
//               {
//                   "id": "0",
//                   "title": "Group 0",
//                   "children": {
//                       "0.1": {
//                           "id": "0.1",
//                           "title": "Field functions_carried_out_tap_reference",
//                           "field_id": "field_functions_carried_out_tap_reference",
//                           "weight": 0
//                       }
//                   }
//               }
//           ]
//       }
//   }, undefined, 4);

//   let fDataSingle = JSON.stringify({
//       "field_functions_carried_out_tap_reference": {
//           "value": "01G4D7X1P7P3QSS3CEAEGYVG02",
//           "class": "App\\Entity\\FunctionCarriedOutTap"
//       }
//   }, undefined, 4);
//   let fDataMulti = JSON.stringify({
//       "field_functions_carried_out_tap_reference": [
//           {
//               "value": "01G4D7X1P7P3QSS3CEAEGYVG02",
//               "class": "App\\Entity\\FunctionCarriedOutTap"
//           },
//           {
//               "value": "01G4D7X1P7P3QSS3CEAEGYVG03",
//               "class": "App\\Entity\\FunctionCarriedOutTap"
//           },
//           {
//               "value": "01G4D7X1P7P3QSS3CEAEGYVG3H",
//               "class": "App\\Entity\\FunctionCarriedOutTap"
//           }
//       ]
//   }, undefined, 4);

//   beforeEach(() => {
//       // Mock get technical_assistance_providers from API.
//       cy.intercept('GET', '/api/v1/functions_carried_out_taps?page=2', []);
//       cy.intercept('GET', '/api/v1/functions_carried_out_taps?page=1', {fixture: 'functions_carried_out_taps.json'});
//   })

//   it('DynForm Field - simple functions_carried_out_tap_reference without data', () => {
//       // Visit test page.
//       cy.visit("dynform_field_test");
//       // Set configuration values in textarea.
//       cy.get("#txt_structure").invoke('val', fStructSingle);
//       // Generated change event.
//       cy.get("#txt_structure").type('{moveToEnd}{enter}');
//       // Validate field existence.
//       cy.get(".form-field-functions_carried_out_tap_reference").should("be.visible");
//       // Change value.
//       cy.get(".form-field-functions_carried_out_tap_reference [type='radio']").check('01G4D7X1P7P3QSS3CEAEGYVG3H');
//       // Submit form.
//       cy.get("#test-form-submit").click();
//       // Check data value.
//       cy.get("#txt_data")
//           .should("have.text", JSON.stringify({
//                   "field_functions_carried_out_tap_reference": {
//                       "value": "01G4D7X1P7P3QSS3CEAEGYVG3H",
//                       "class": "App\\Entity\\FunctionCarriedOutTap"
//                   }
//               }, undefined, 4));
//       // Scroll to form.
//       cy.get(".form-sample").scrollIntoView();
//   })

//   it('DynForm Field - simple functions_carried_out_tap_reference with data', () => {
//       // Visit test page.
//       cy.visit("dynform_field_test");
//       // Set configuration values in textarea.
//       cy.get("#txt_structure").invoke('val', fStructSingle);
//       // Generated change event.
//       cy.get("#txt_structure").type('{moveToEnd}{enter}');

//       // Set data values in textarea.
//       cy.get("#txt_data").invoke('val', fDataSingle);
//       // Generated change event.
//       cy.get("#txt_data").type('{moveToEnd}{enter}');

//       // Validate field existence.
//       cy.get(".form-field-functions_carried_out_tap_reference").should("be.visible");
//       cy.get(".form-field-functions_carried_out_tap_reference :checked").should("be.checked").and('have.value', '01G4D7X1P7P3QSS3CEAEGYVG02');

//       // Validate data value in field.
//       cy.get("#test-form-submit").click();
//       cy.get("#txt_data")
//           .should("have.text", JSON.stringify({
//                   "field_functions_carried_out_tap_reference": {
//                       "value": "01G4D7X1P7P3QSS3CEAEGYVG02",
//                       "class": "App\\Entity\\FunctionCarriedOutTap"
//                   }
//               }, undefined, 4));

//       // Change value.
//       cy.get(".form-field-functions_carried_out_tap_reference [type='radio']").check('01G4D7X1P7P3QSS3CEAEGYVG3H');
//       // Submit form.
//       cy.get("#test-form-submit").click();
//       // Check data value.
//       cy.get("#txt_data")
//           .should("have.text", JSON.stringify({
//                   "field_functions_carried_out_tap_reference": {
//                       "value": "01G4D7X1P7P3QSS3CEAEGYVG3H",
//                       "class": "App\\Entity\\FunctionCarriedOutTap"
//                   }
//               }, undefined, 4));

//       // Scroll to form.
//       cy.get(".form-sample").scrollIntoView();
//   })

//   it('DynForm Field - multivalued functions_carried_out_tap_reference without data', () => {
//       // Visit test page.
//       cy.visit("dynform_field_test");
//       // Set configuration values in textarea.
//       cy.get("#txt_structure").invoke('val', fStructMulti);
//       // Generated change event.
//       cy.get("#txt_structure").type('{moveToEnd}{enter}');
//       // Validate field existence.
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7WW42YZ6P38GPKFCXS467]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7WW491HXQNVH76WPB0RXF]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG01]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG02]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG03]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG3H]').should("be.visible");

//       // Change value.
//       cy.get(".form-multivalued-list [type='checkbox']").check('01G4D7X1P7P3QSS3CEAEGYVG02');
//       cy.get(".form-multivalued-list [type='checkbox']").check('01G4D7X1P7P3QSS3CEAEGYVG03');
//       cy.get(".form-multivalued-list [type='checkbox']").check('01G4D7X1P7P3QSS3CEAEGYVG3H');
//       // Submit form.
//       cy.get("#test-form-submit").click();
//       // Check data value.
//       cy.get("#txt_data")
//           .should("have.text", JSON.stringify({
//                   "field_functions_carried_out_tap_reference": [
//                       {
//                           "value": "01G4D7X1P7P3QSS3CEAEGYVG02",
//                           "class": "App\\Entity\\FunctionCarriedOutTap"
//                       },
//                       {
//                           "value": "01G4D7X1P7P3QSS3CEAEGYVG03",
//                           "class": "App\\Entity\\FunctionCarriedOutTap"
//                       },
//                       {
//                           "value": "01G4D7X1P7P3QSS3CEAEGYVG3H",
//                           "class": "App\\Entity\\FunctionCarriedOutTap"
//                       }
//                   ]
//               }, undefined, 4));

//       // Scroll to form.
//       cy.get(".form-sample").scrollIntoView();
//   })

//   it('DynForm Field - multivalued functions_carried_out_tap_reference with data', () => {
//       // Visit test page.
//       cy.visit("dynform_field_test");
//       // Set configuration values in textarea.
//       cy.get("#txt_structure").invoke('val', fStructMulti);
//       cy.get("#txt_structure").type('{moveToEnd}{enter}');
//       // Set data values in textarea.
//       cy.get("#txt_data").invoke('val', fDataMulti);
//       cy.get("#txt_data").type('{moveToEnd}{enter}');
//       // Validate field existence.

//       // Validate field existence and value.
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7WW42YZ6P38GPKFCXS467]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7WW42YZ6P38GPKFCXS467]').should('not.be.checked');
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7WW491HXQNVH76WPB0RXF]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7WW491HXQNVH76WPB0RXF]').should('not.be.checked');
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG01]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG01]').should('not.be.checked');
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG02]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG02]').should('be.checked');
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG03]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG03]').should('be.checked');
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG3H]').should("be.visible");
//       cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4D7X1P7P3QSS3CEAEGYVG3H]').should('be.checked');
      
//       // Change value.
//       cy.get(".form-multivalued-list [type='checkbox']").check('01G4D7WW42YZ6P38GPKFCXS467');
//       cy.get(".form-multivalued-list [type='checkbox']").check('01G4D7X1P7P3QSS3CEAEGYVG01');
//       cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4D7X1P7P3QSS3CEAEGYVG02');
//       cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4D7X1P7P3QSS3CEAEGYVG3H');
      
//       // Submit form.
//       cy.get("#test-form-submit").click();
//       // Check data value.
//       cy.get("#txt_data")
//           .should("have.text", JSON.stringify({
//                   "field_functions_carried_out_tap_reference": [
//                       {
//                           "value": '01G4D7WW42YZ6P38GPKFCXS467',
//                           "class": "App\\Entity\\FunctionCarriedOutTap"
//                       },
//                       {
//                           "value": "01G4D7X1P7P3QSS3CEAEGYVG01",
//                           "class": "App\\Entity\\FunctionCarriedOutTap"
//                       },
//                       {
//                           "value": "01G4D7X1P7P3QSS3CEAEGYVG03",
//                           "class": "App\\Entity\\FunctionCarriedOutTap"
//                       }
//                   ]
//               }, undefined, 4));

//       // Scroll to form.
//       cy.get(".form-sample").scrollIntoView();
//   })

// })