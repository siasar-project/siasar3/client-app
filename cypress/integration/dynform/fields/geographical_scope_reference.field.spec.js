// /**
//  * This file is part of the SIASAR package.
//  *
//  * Next.js version 11.1.4
//  *
//  * SIASAR Global is a joint initiative launched by the governments of Honduras,
//  * Nicaragua and Panama that soon expanded to other regions. The strategic
//  * purpose of this initiative is to have a basic, updated and comparable
//  * information tool on the rural water supply and sanitation services in place
//  * in a given country.
//  *
//  * @summary SIASAR_3 Front-end
//  *
//  * @license GPL-3.0
//  *
//  * @see     http://globalsiasar.org/es/contact
//  */
// /// <reference types="cypress" />
// Cypress.on('uncaught:exception', (err, runnable) => {
//     // returning false here prevents Cypress from
//     // failing the test
//     return false
// })

// /**
//  * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
//  */

// /**
//  * Test DynForm geographical_scope field.
//  */
// context('Window', () => {
//     // Form structure.
//     let fStructSingle = JSON.stringify({
//         "description": "",
//         "fields": {
//             "field_geographical_scope": {
//                 "id": "field_geographical_scope",
//                 "type": "geographical_scope_reference",
//                 "label": "Field geographical_scope",
//                 "description": "Field geographical_scope",
//                 "indexable": false,
//                 "internal": false,
//                 "deprecated": false,
//                 "settings": {
//                     "required": true,
//                     "multivalued": false,
//                     "weight": 0,
//                     "meta": {
//                         "catalog_id": "0.1"
//                     },
//                     "sort": false,
//                     "filter": false
//                 }
//             }
//         },
//         "id": "form.sample",
//         "requires": [],
//         "title": "geographical_scope form field",
//         "type": "",
//         "meta": {
//             "title": "geographical_scope form field",
//             "version": "Version 1.0",
//             "field_groups": [
//                 {
//                     "id": "0",
//                     "title": "Group 0",
//                     "children": {
//                         "0.1": {
//                             "id": "0.1",
//                             "title": "Field geographical_scope",
//                             "field_id": "field_geographical_scope",
//                             "weight": 0
//                         }
//                     }
//                 }
//             ]
//         }
//     }, undefined, 4);
//     let fStructMulti = JSON.stringify({
//         "description": "",
//         "fields": {
//             "field_geographical_scope": {
//                 "id": "field_geographical_scope",
//                 "type": "geographical_scope_reference",
//                 "label": "Field geographical_scope",
//                 "description": "Field geographical_scope",
//                 "indexable": false,
//                 "internal": false,
//                 "deprecated": false,
//                 "settings": {
//                     "required": true,
//                     "multivalued": true,
//                     "weight": 0,
//                     "meta": {
//                         "catalog_id": "0.1"
//                     },
//                     "sort": false,
//                     "filter": false
//                 }
//             }
//         },
//         "id": "form.sample",
//         "requires": [],
//         "title": "geographical_scope form field",
//         "type": "",
//         "meta": {
//             "title": "geographical_scope form field",
//             "version": "Version 1.0",
//             "field_groups": [
//                 {
//                     "id": "0",
//                     "title": "Group 0",
//                     "children": {
//                         "0.1": {
//                             "id": "0.1",
//                             "title": "Field geographical_scope",
//                             "field_id": "field_geographical_scope",
//                             "weight": 0
//                         }
//                     }
//                 }
//             ]
//         }
//     }, undefined, 4);
//     let fDataSingle = JSON.stringify({
//         "field_geographical_scope": {
//             "value": "01G3TAYES1KTRREDXHVT404VAB",
//             "class": "App\\Entity\\GeographicalScope"
//         }
//     }, undefined, 4);
//     let fDataMulti = JSON.stringify({
//         "field_geographical_scope": [
//             {
//                 "value": "01G3TAYES1KTRREDXHVT404VAB",
//                 "class": "App\\Entity\\GeographicalScope"
//             },
//             {
//                 "value": "01G3TAYEY2BKZMZAS44VE084NX",
//                 "class": "App\\Entity\\GeographicalScope"
//             },
//             {
//                 "value": "01G3ZQXX309HZQFY23WVRFAVWH",
//                 "class": "App\\Entity\\GeographicalScope"
//             }
//         ]
//     }, undefined, 4);

//     beforeEach(() => {
//         // cy.visit("/")
//         cy.intercept('GET', '/api/v1/geographical_scopes?page=1', {fixture: 'geographical_scopes.json'});
//         cy.intercept('GET', '/api/v1/geographical_scopes?page=2', []);
//     })

//     it('DynForm Field - simple geographical_scope without data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructSingle);
//         // Generated change event.
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');
//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//         // Validate field existence.
//         cy.get(".form-field-geographical_scope_reference").should("be.visible");
//         // Change value.
//         cy.get(".form-field-geographical_scope_reference [type='radio']").check('01G3ZQY4YESDJFG19KVXHBW0SC');
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                 "field_geographical_scope": {
//                     "value": "01G3ZQY4YESDJFG19KVXHBW0SC",
//                     "class": "App\\Entity\\GeographicalScope"
//                 }
//             }, undefined, 4));
//     })

//     it('DynForm Field - simple geographical_scope with data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructSingle);
//         // Generated change event.
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');

//         // Set data values in textarea.
//         cy.get("#txt_data").invoke('val', fDataSingle);
//         // Generated change event.
//         cy.get("#txt_data").type('{moveToEnd}{enter}');

//         // Validate field existence.
//         cy.get(".form-field-geographical_scope_reference").should("be.visible");
//         cy.get(".form-field-geographical_scope_reference :checked")
//             .should("be.checked")
//             .and('have.value', '01G3TAYES1KTRREDXHVT404VAB');

//         // Validate data value in field.
//         cy.get("#test-form-submit").click();
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_geographical_scope": {
//                         "value": "01G3TAYES1KTRREDXHVT404VAB",
//                         "class": "App\\Entity\\GeographicalScope"
//                     }
//                 }, undefined, 4));

//         // Change value.
//         cy.get(".form-field-geographical_scope_reference [type='radio']").check('01G3ZQY4YESDJFG19KVXHBW0SC');
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                     "field_geographical_scope": {
//                         "value": "01G3ZQY4YESDJFG19KVXHBW0SC",
//                         "class": "App\\Entity\\GeographicalScope"
//                     }
//                 }, undefined, 4));

//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//     })

//     it('DynForm Field - multivalued geographical_scope without data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructMulti);
//         // Generated change event.
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');
//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//         // Validate field existence.
//         cy.get(".form-multivalued-list [type='checkbox']").filter('[value="01G3TAYEY2BKZMZAS44VE084NX"]').should("be.visible");
//         cy.get(".form-multivalued-list [type='checkbox']").filter('[value="01G3ZQXX309HZQFY23WVRFAVWH"]').should("be.visible");
//         cy.get(".form-multivalued-list [type='checkbox']").filter('[value="01G3ZQY4YESDJFG19KVXHBW0SC"]').should("be.visible");
//         cy.get(".form-multivalued-list [type='checkbox']").filter('[value="01G3TAYES1KTRREDXHVT404VAB"]').should("be.visible");
//         // Change value.
//         cy.get(".form-multivalued-list [type='checkbox']").check('01G3TAYEY2BKZMZAS44VE084NX');
//         cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZQXX309HZQFY23WVRFAVWH');
//         cy.get(".form-multivalued-list [type='checkbox']").check('01G3TAYES1KTRREDXHVT404VAB');
//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                 "field_geographical_scope": [
//                     {
//                         "value": "01G3TAYES1KTRREDXHVT404VAB",
//                         "class": "App\\Entity\\GeographicalScope"
//                     },
//                     {
//                         "value": "01G3ZQXX309HZQFY23WVRFAVWH",
//                         "class": "App\\Entity\\GeographicalScope"
//                     },
//                     {
//                         "value": "01G3TAYEY2BKZMZAS44VE084NX",
//                         "class": "App\\Entity\\GeographicalScope"
//                     }
//                 ]
//             }, undefined, 4));
//     })

//     it('DynForm Field - multivalued geographical_scope with data', () => {
//         // Visit test page.
//         cy.visit("dynform_field_test");
//         // Set configuration values in textarea.
//         cy.get("#txt_structure").invoke('val', fStructMulti);
//         cy.get("#txt_structure").type('{moveToEnd}{enter}');
//         // Scroll to form.
//         cy.get(".form-sample").scrollIntoView();
//         // Set data values in textarea.
//         cy.get("#txt_data").invoke('val', fDataMulti);
//         cy.get("#txt_data").type('{moveToEnd}{enter}');
//         // Validate field existence and value.
//         cy.get(".form-multivalued-list [type='checkbox']")
//             .filter('[value="01G3TAYEY2BKZMZAS44VE084NX"]')
//             .should("be.visible")
//             .and("be.checked");
//         cy.get(".form-multivalued-list [type='checkbox']")
//             .filter('[value="01G3ZQXX309HZQFY23WVRFAVWH"]')
//             .should("be.visible")
//             .and("be.checked");
//         cy.get(".form-multivalued-list [type='checkbox']")
//             .filter('[value="01G3ZQY4YESDJFG19KVXHBW0SC"]')
//             .should("be.visible")
//             .and("not.be.checked");
//         cy.get(".form-multivalued-list [type='checkbox']")
//             .filter('[value="01G3TAYES1KTRREDXHVT404VAB"]')
//             .should("be.visible")
//             .and("be.checked");
        
//         // Change value.
//         cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZQY4YESDJFG19KVXHBW0SC');
//         cy.get(".form-multivalued-list [type='checkbox']").check('01G3TAYEY2BKZMZAS44VE084NX');
//         cy.get(".form-multivalued-list [type='checkbox']").check('01G3ZQXX309HZQFY23WVRFAVWH');
//         cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G3TAYES1KTRREDXHVT404VAB');

//         // Submit form.
//         cy.get("#test-form-submit").click();
//         // Check data value.
//         cy.get("#txt_data")
//             .should("have.text", JSON.stringify({
//                 "field_geographical_scope": [
//                     {
//                         "value": "01G3ZQXX309HZQFY23WVRFAVWH",
//                         "class": "App\\Entity\\GeographicalScope"
//                     },
//                     {
//                         "value": "01G3ZQY4YESDJFG19KVXHBW0SC",
//                         "class": "App\\Entity\\GeographicalScope"
//                     },
//                     {
//                         "value": "01G3TAYEY2BKZMZAS44VE084NX",
//                         "class": "App\\Entity\\GeographicalScope"
//                     }
//                 ]
//             }, undefined, 4));
//     })
// })
