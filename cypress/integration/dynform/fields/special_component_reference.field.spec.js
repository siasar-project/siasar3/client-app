/// <reference types="cypress" />
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

/**
 * @see https://filiphric.com/cypress-basics-check-attributes-value-and-text
 */

/**
 * Test DynForm special_component_reference field.
 */
context('Window', () => {
    // Form structure.
    let fStructSingle = JSON.stringify({
        "description": "",
        "fields": {
            "field_special_component_reference": {
                "id": "field_special_component_reference",
                "type": "special_component_reference",
                "label": "Field special_component_reference",
                "description": "Field special_component_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": false,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "special_component_reference form field",
        "type": "",
        "meta": {
            "title": "special_component_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field special_component_reference",
                            "field_id": "field_special_component_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);
    let fStructMulti = JSON.stringify({
        "description": "",
        "fields": {
            "field_special_component_reference": {
                "id": "field_special_component_reference",
                "type": "special_component_reference",
                "label": "Field special_component_reference",
                "description": "Field special_component_reference",
                "indexable": false,
                "internal": false,
                "deprecated": false,
                "settings": {
                    "required": true,
                    "multivalued": true,
                    "weight": 0,
                    "meta": {
                        "catalog_id": "0.1"
                    },
                    "sort": false,
                    "filter": false
                }
            }
        },
        "id": "form.sample",
        "requires": [],
        "title": "special_component_reference form field",
        "type": "",
        "meta": {
            "title": "special_component_reference form field",
            "version": "Version 1.0",
            "field_groups": [
                {
                    "id": "0",
                    "title": "Group 0",
                    "children": {
                        "0.1": {
                            "id": "0.1",
                            "title": "Field special_component_reference",
                            "field_id": "field_special_component_reference",
                            "weight": 0
                        }
                    }
                }
            ]
        }
    }, undefined, 4);

    let fDataSingle = JSON.stringify({
        "field_special_component_reference": {
            "value": "01G4A8Y62WJSHF6E8ESRD25STB",
            "class": "App\\Entity\\SpecialComponent"
        }
    }, undefined, 4);
    let fDataMulti = JSON.stringify({
        "field_special_component_reference": [
            {
                "value": "01G4A8Y62WJSHF6E8ESRD25STB",
                "class": "App\\Entity\\SpecialComponent"
            },
            {
                "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J2",
                "class": "App\\Entity\\SpecialComponent"
            },
            {
                "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J6",
                "class": "App\\Entity\\SpecialComponent"
            }
        ]
    }, undefined, 4);

    beforeEach(() => {
        // Mock get technical_assistance_providers from API.
        cy.intercept('GET', '/api/v1/special_components?page=2', []);
        cy.intercept('GET', '/api/v1/special_components?page=1', {fixture: 'special_components.json'});
    })

    it('DynForm Field - simple special_component_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-field-special_component_reference").should("be.visible");
        // Change value.
        cy.get(".form-field-special_component_reference [type='radio']").check('01G4A8ZGB31Y1AX5NHPZ9QK1J4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_special_component_reference": {
                        "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J4",
                        "class": "App\\Entity\\SpecialComponent"
                    }
                }, undefined, 4));
        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - simple special_component_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructSingle);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');

        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataSingle);
        // Generated change event.
        cy.get("#txt_data").type('{moveToEnd}{enter}');

        // Validate field existence.
        cy.get(".form-field-special_component_reference").should("be.visible");
        cy.get(".form-field-special_component_reference :checked").should("be.checked").and('have.value', '01G4A8Y62WJSHF6E8ESRD25STB');

        // Validate data value in field.
        cy.get("#test-form-submit").click();
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_special_component_reference": {
                        "value": "01G4A8Y62WJSHF6E8ESRD25STB",
                        "class": "App\\Entity\\SpecialComponent"
                    }
                }, undefined, 4));

        // Change value.
        cy.get(".form-field-special_component_reference [type='radio']").check('01G4A8ZGB31Y1AX5NHPZ9QK1J4');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_special_component_reference": {
                        "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J4",
                        "class": "App\\Entity\\SpecialComponent"
                    }
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued special_component_reference without data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        // Generated change event.
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Validate field existence.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62WJSHF6E8ESRD25STB]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB20EBR3T6TCR0X9Q1M]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J2]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J4]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J6]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J8]').should("be.visible");

        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8ZGB20EBR3T6TCR0X9Q1M');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8ZGB31Y1AX5NHPZ9QK1J4');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8ZGB31Y1AX5NHPZ9QK1J6');
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_special_component_reference": [
                        {
                            "value": "01G4A8ZGB20EBR3T6TCR0X9Q1M",
                            "class": "App\\Entity\\SpecialComponent"
                        },
                        {
                            "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J4",
                            "class": "App\\Entity\\SpecialComponent"
                        },
                        {
                            "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J6",
                            "class": "App\\Entity\\SpecialComponent"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

    it('DynForm Field - multivalued special_component_reference with data', () => {
        // Visit test page.
        cy.visit("dynform_field_test");
        // Set configuration values in textarea.
        cy.get("#txt_structure").invoke('val', fStructMulti);
        cy.get("#txt_structure").type('{moveToEnd}{enter}');
        // Set data values in textarea.
        cy.get("#txt_data").invoke('val', fDataMulti);
        cy.get("#txt_data").type('{moveToEnd}{enter}');
        // Validate field existence.

        // Validate field existence and value.
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62WJSHF6E8ESRD25STB]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8Y62WJSHF6E8ESRD25STB]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB20EBR3T6TCR0X9Q1M]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB20EBR3T6TCR0X9Q1M]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J2]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J2]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J4]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J4]').should('not.be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J6]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J6]').should('be.checked');
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J8]').should("be.visible");
        cy.get(".form-multivalued-list [type='checkbox']").filter('[value=01G4A8ZGB31Y1AX5NHPZ9QK1J8]').should('not.be.checked');
              
        // Change value.
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4A8Y62WJSHF6E8ESRD25STB');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8ZGB20EBR3T6TCR0X9Q1M');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4A8ZGB31Y1AX5NHPZ9QK1J2');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8ZGB31Y1AX5NHPZ9QK1J4');
        cy.get(".form-multivalued-list [type='checkbox']").uncheck('01G4A8ZGB31Y1AX5NHPZ9QK1J6');
        cy.get(".form-multivalued-list [type='checkbox']").check('01G4A8ZGB31Y1AX5NHPZ9QK1J8');
        
        // Submit form.
        cy.get("#test-form-submit").click();
        // Check data value.
        cy.get("#txt_data")
            .should("have.text", JSON.stringify({
                    "field_special_component_reference": [
                        {
                            "value": '01G4A8ZGB20EBR3T6TCR0X9Q1M',
                            "class": "App\\Entity\\SpecialComponent"
                        },
                        {
                            "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J4",
                            "class": "App\\Entity\\SpecialComponent"
                        },
                        {
                            "value": "01G4A8ZGB31Y1AX5NHPZ9QK1J8",
                            "class": "App\\Entity\\SpecialComponent"
                        }
                    ]
                }, undefined, 4));

        // Scroll to form.
        cy.get(".form-sample").scrollIntoView();
    })

})
