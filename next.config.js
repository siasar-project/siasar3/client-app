/**
 * This file is part of the SIASAR package.
 *
 * Next.js version 11.1.4
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @summary SIASAR_3 Front-end
 *
 * @license GPL-3.0
 *
 * @see     http://globalsiasar.org/es/contact
 */

const withPWA = require('next-pwa');
const runtimeCaching = require('next-pwa/cache')
const prod = process.env.NEXT_PUBLIC_ENV === 'production'

module.exports = withPWA({
  reactStrictMode: false,
  trailingSlash: true,
  pwa: {
    // disable: !prod,
    dest: 'public'
  },
  images: {
    loader: 'imgix',
    path: '/',
  },
  eslint: {
    // Only run ESLint on the 'pages' and 'utils' directories during production builds (next build)
    dirs: ['pages', 'src'],
  },
  // Disable SSR.
  target: "serverless",
  async rewrites() {
    return [
      // Rewrite everything to `pages/index`
      {
        source: "/:any*",
        destination: "/",
      },
    ];
  },
});
