# docker-compose --verbose --env-file .docker.env up --exit-code-from cypress
## docker-compose --env-file .docker.env up --exit-code-from cypress


# Test if first parameter exists and begin with "e2e-" (i.e. is a cypress service).
if [[ "$1" =~ ^e2e-[a-z]+$ ]]; then
  service="$1"
  shift
else
  service="e2e-chrome"
fi

echo "Launching CYPRESS tests using "$service"..."

docker-compose --env-file .docker.env run $service "$@"
